package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="citydriver_status")
public class CityDriverStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idcityStatus")
	private int idCityStatus;

	@OneToOne
    @JoinColumn(name="idcityDriver")
	@Fetch(FetchMode.JOIN) 
	private CityDriver idCityDriver;

	@Column(name= "status_id")
	private BigInteger statusId;

    @Column(name="idsys_user_creator")
	private BigInteger sysUserCreator;
	
	private String comments;
	
	private BigInteger stops;
	
	@Column(name="price_stops")
	private String stopsPrice;
	
	private Date dateReg = new Date();


    public CityDriverStatus() {
    }

	public CityDriverStatus (int idCityStatus, CityDriver idCityDriver, 
			BigInteger statusId, BigInteger sysUserCreator, String comments, BigInteger stops, String stopsPrice) {
		super();
		this.idCityStatus = idCityStatus;
		this.idCityDriver = idCityDriver;
		this.statusId = statusId;
		this.sysUserCreator = sysUserCreator;
		this.comments = comments;
		this.stops = stops;
		this.stopsPrice = stopsPrice;
	}

	public int getIdCityStatus(){
		return this.idCityStatus;
	}
	public void setIdCityStatus(int idCityStatus){
		this.idCityStatus = idCityStatus;
	}

	public CityDriver getIdCityDriver(){
		return this.idCityDriver;
	}
	public void setIdCityDriver(CityDriver idCityDriver){
		this.idCityDriver = idCityDriver;
	}

	public BigInteger getStatusId(){
		return this.statusId;
	}
	public void setStatusId(BigInteger statusId){
		this.statusId = statusId;
	}

	public BigInteger getSysUserCreator(){
		return this.sysUserCreator;
	}
	public void setSysUserCreator(BigInteger sysUserCreator){
		this.sysUserCreator = sysUserCreator;
	}

	public String getComments(){
		return this.comments;
	}
	public void setComments(String comments){
		this.comments = comments;
	}
	
	public BigInteger getStops(){
		return this.stops;
	}
	public void setStops(BigInteger stops){
		this.stops = stops;
	}

	public String getStopsPrice(){
		return this.stopsPrice;
	}
	public void setStopsPrice(String stopsPrice){
		this.stopsPrice = stopsPrice;
	}
	
	public Date getDateReg(){
		return dateReg;
	}
	public void setDateReg(Date dateReg ){
		this.dateReg = dateReg;
	}

}
