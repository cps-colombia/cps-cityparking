package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the shared_account database table.
 * 
 */
@Entity
@Table(name="shared_account")
public class SharedAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="idsys_user_acc1")
	private String idsysUserAcc1;

	@Column(name="idsys_user_acc2")
	private String idsysUserAcc2;

	private int state;
	
    public SharedAccount() {
    }

	public String getIdsysUserAcc1() {
		return this.idsysUserAcc1;
	}

	public void setIdsysUserAcc1(String idsysUserAcc1) {
		this.idsysUserAcc1 = idsysUserAcc1;
	}

	public String getIdsysUserAcc2() {
		return this.idsysUserAcc2;
	}

	public void setIdsysUserAcc2(String idsysUserAcc2) {
		this.idsysUserAcc2 = idsysUserAcc2;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}