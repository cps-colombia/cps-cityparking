package com.cps.entity.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "cityparking_transaction")
public class CityParkingTransactionUser implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	
	@Id
	@Column(name="id_transaction")
	private String idTransaction;
	
	@Column(name="date_transaction")
	@Temporal( TemporalType.TIMESTAMP)
	private Date dateTransaction;
	
	/*@Column(name="id_sys_user")
	private String idSysUser;*/
	
	@Column(name="email_sys_user")
	private String emailSysUser;
	
	@Column(name="state_transaction")
	private String stateTransaction;
	
	
	@OneToOne
    @JoinColumn(name="id_sys_user")
	@Fetch(FetchMode.JOIN) 
	private SysUser sysUser;
	
	private double amount;
	
	private String currency;
	
	@Column(name="idzone")
	private String idZone;
	
	public CityParkingTransactionUser(){
		
	}
	public CityParkingTransactionUser(String idTransaction,Date dateTransaction,String idSysUser,String emailSysUser,
			String stateTransaction,double amount,String currency) {
		super();
		this.idTransaction = idTransaction;
		this.dateTransaction = dateTransaction;
		//this.idSysUser = idSysUser;
		this.emailSysUser = emailSysUser;
		this.stateTransaction = stateTransaction;
		this.amount = amount;
		this.currency = currency;

	}
	
	public String getIdTransaction() {
		return idTransaction;
	}

	public void setIdTransaction(String idTransaction) {
		this.idTransaction = idTransaction;
	}

	public Date getDateTransaction() {
		return dateTransaction;
	}

	public void setDateTransaction(Date dateTransaction) {
		this.dateTransaction = dateTransaction;
	}

	/*public String getIdSysUser() {
		return idSysUser;
	}

	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}*/

	public String getEmailSysUser() {
		return emailSysUser;
	}

	public void setEmailSysUser(String emailSysUser) {
		this.emailSysUser = emailSysUser;
	}

	public String getStateTransaction() {
		return stateTransaction;
	}

	public void setStateTransaction(String stateTransaction) {
		this.stateTransaction = stateTransaction;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public SysUser getSysUser() {
		return sysUser;
	}
	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}
	public String getIdZone() {
		return idZone;
	}
	public void setIdZone(String idZone) {
		this.idZone = idZone;
	}

		
	
	

}

