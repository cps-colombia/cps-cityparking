package com.cps.entity.bean;

import java.io.Serializable;
//import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "paypal_transaction")
public class PayPalTransaction implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="id_transaction")
	private String idTransaction;
	
	@Column(name="date_transaction")
	@Temporal( TemporalType.TIMESTAMP)
	private Date dateTransaction;
	
	@Column(name="id_sys_user")
	private String idSysUser;
	
	@Column(name="email_sys_user")
	private String emailSysUser;
	
	@Column(name="state_transaction")
	private String stateTransaction;
	
	private double amount;
	
	private String currency;

	/*public PaypalTransaction(){
		
	}
	public PaypalTransaction(String idTransaction,Date dateTransaction,String idSysUser,String emailSysUser,
			String stateTransaction,double amount,String currency) {
		super();
		this.idTransaction = idTransaction;
		this.dateTransaction = dateTransaction;
		this.idSysUser = idSysUser;
		this.emailSysUser = emailSysUser;
		this.stateTransaction = stateTransaction;
		this.amount = amount;
		this.currency = currency;

	}*/
	
	public String getIdTransaction() {
		return idTransaction;
	}

	public void setIdTransaction(String idTransaction) {
		this.idTransaction = idTransaction;
	}

	public Date getDateTransaction() {
		return dateTransaction;
	}

	public void setDateTransaction(Date dateTransaction) {
		this.dateTransaction = dateTransaction;
	}

	public String getIdSysUser() {
		return idSysUser;
	}

	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}

	public String getEmailSysUser() {
		return emailSysUser;
	}

	public void setEmailSysUser(String emailSysUser) {
		this.emailSysUser = emailSysUser;
	}

	public String getStateTransaction() {
		return stateTransaction;
	}

	public void setStateTransaction(String stateTransaction) {
		this.stateTransaction = stateTransaction;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

		
	
	

}
