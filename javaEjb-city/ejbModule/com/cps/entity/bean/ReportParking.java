package com.cps.entity.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the parking_history_view database table.
 * 
 */
@Entity
@Table(name="parking_history_view")
public class ReportParking implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idhistory;

    @Temporal( TemporalType.TIMESTAMP)
	private Date endTime;

	private double minuteValue;

	private String msisdn;

	@Column(name="platform_idplatform")
	private int platformIdplatform;

    @Temporal( TemporalType.TIMESTAMP)
	private Date startTime;

    @OneToOne 
    @JoinColumn(name="zone_idzone") 
    private Zone zone;

	@Column(name="vehicle_plate")
	private String vehiclePlate;
	
	/*@Column(name="idsys_user")
	private String idSysUser;*/
	
	private double minuts;
	
	private double minutsRound;
	
	private double totalParking;
	
	@Column(name="place_idplace")
	private String placeIdplace;
	
	@Column(name="pay_id")
	private String payId;
	
	private Double fee;
	
	@OneToOne 
    @JoinColumn(name="idsys_user" )
	private SysUser sysUser;
	
    public SysUser getSysUser() {
		return sysUser;
	}


	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}


	public ReportParking() {
    }

	
	public String getIdhistory() {
		return this.idhistory;
	}

	public void setIdhistory(String idhistory) {
		this.idhistory = idhistory;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public double getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(double minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getPlatformIdplatform() {
		return this.platformIdplatform;
	}

	public void setPlatformIdplatform(int platformIdplatform) {
		this.platformIdplatform = platformIdplatform;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getVehiclePlate() {
		return vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

	/*public String getIdSysUser() {
		return idSysUser;
	}


	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}*/

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}


	public void setMinuts(double minuts) {
		this.minuts = minuts;
	}


	public double getMinuts() {
		return minuts;
	}


	public void setMinutsRound(double minutsRound) {
		this.minutsRound = minutsRound;
	}


	public double getMinutsRound() {
		return minutsRound;
	}


	public void setTotalParking(double totalParking) {
		this.totalParking = totalParking;
	}


	public double getTotalParking() {
		return totalParking;
	}


	public void setPlaceIdplace(String placeIdplace) {
		this.placeIdplace = placeIdplace;
	}


	public String getPlaceIdplace() {
		return placeIdplace;
	}

	public String getPayId() {
		return this.payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}
	
	public Double getFee() {
		return fee;
	}


	public void setFee(Double fee) {
		this.fee = fee;
	}
	
}