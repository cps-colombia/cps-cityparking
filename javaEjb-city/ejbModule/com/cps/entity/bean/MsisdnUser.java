package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the msisdn_user database table.
 * 
 */
@Entity
@Table(name="msisdn_user")
public class MsisdnUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idmsisdn_user")
	private String idmsisdnUser;

	@Column(name="idsys_user")
	private String idsysUser;

	private String msisdn;

    public MsisdnUser() {
    }

	public String getIdmsisdnUser() {
		return this.idmsisdnUser;
	}

	public void setIdmsisdnUser(String idmsisdnUser) {
		this.idmsisdnUser = idmsisdnUser;
	}

	public String getIdsysUser() {
		return this.idsysUser;
	}

	public void setIdsysUser(String idsysUser) {
		this.idsysUser = idsysUser;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

}