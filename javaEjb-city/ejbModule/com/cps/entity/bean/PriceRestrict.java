package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;


/**
 * The persistent class for the price_restrict database table.
 * 
 */
@Entity
@Table(name="price_restrict")
@NamedQuery(name="PriceRestrict.findAll", query="SELECT p FROM PriceRestrict p")
public class PriceRestrict implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idtimePriceRestriction;

	private int day;

	@Lob
	private String description;

	private Time endTime;
	
	@Column(name="rate_time_minutes")
	
	private BigInteger rateTimeMinutes;

	private BigDecimal minuteValue;

	private Time startTime;

	//uni-directional many-to-one association to RateType
	@ManyToOne
	@JoinColumn(name="idrate_type")
	private RateType rateType;

	//uni-directional many-to-one association to VehicleType
	@ManyToOne
	@JoinColumn(name="vtype_id")
	private VehicleType vehicleType;

	//uni-directional many-to-one association to Zone
	@ManyToOne
	private Zone zone;

	public PriceRestrict() {
	}

	public String getIdtimePriceRestriction() {
		return this.idtimePriceRestriction;
	}

	public void setIdtimePriceRestriction(String idtimePriceRestriction) {
		this.idtimePriceRestriction = idtimePriceRestriction;
	}

	public int getDay() {
		return this.day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Time getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public BigDecimal getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(BigDecimal minuteValue) {
		this.minuteValue = minuteValue;
	}

	public Time getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public RateType getRateType() {
		return this.rateType;
	}

	public void setRateType(RateType rateType) {
		this.rateType = rateType;
	}

	public VehicleType getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Zone getZone() {
		return this.zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public BigInteger getRateTimeMinutes() {
		return rateTimeMinutes;
	}

	public void setRateTimeMinutes(BigInteger rateTimeMinutes) {
		this.rateTimeMinutes = rateTimeMinutes;
	}

}