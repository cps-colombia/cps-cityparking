package com.cps.entity.bean;

import java.io.Serializable;



public class ClientCountryId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idclient;

	private String idcountry;
	public String getIdclient() {
		return idclient;
	}
	public void setIdclient(String idclient) {
		this.idclient = idclient;
	}
	public String getIdcountry() {
		return idcountry;
	}
	public void setIdcountry(String idcountry) {
		this.idcountry = idcountry;
	}	
}
