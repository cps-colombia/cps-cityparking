package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the rate_type database table.
 * 
 */
@Entity
@Table(name="rate_type")
@NamedQueries({
	@NamedQuery(name="RateType.findAll", query="SELECT r FROM RateType r"),
	@NamedQuery(name="RateType.findById", query="SELECT r FROM RateType r where r.idrateType=:idRtype")
})

public class RateType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idrate_type")
	private int idrateType;

	private String description;

	private String name;

	public RateType() {
	}

	public int getIdrateType() {
		return this.idrateType;
	}

	public void setIdrateType(int idrateType) {
		this.idrateType = idrateType;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}