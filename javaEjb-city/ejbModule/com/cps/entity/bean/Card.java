package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the card database table.
 * 
 */
@Entity
@Table(name="card")
public class Card implements Serializable {
	private static final long serialVersionUID = 1L;

	//@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	//private String id;
	@Id	
	private String pin;

    @Temporal( TemporalType.TIMESTAMP)
	private Date activationDate;
    
    @Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal price;

	private int used;

	private String idsys_user;
	
	private String currency;

    public Card() {
    }

	public String getPin() {
		return this.pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public Date getActivationDate() {
		return this.activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getUsed() {
		return this.used;
	}

	public void setUsed(int used) {
		this.used = used;
	}

	public String getUser() {
		return this.idsys_user;
	}

	public void setUser(String userId) {
		this.idsys_user = userId;
	}
	
	public boolean isUsed(){
		return used == 1;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrency() {
		return currency;
	}
/*
	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
*/
	
}