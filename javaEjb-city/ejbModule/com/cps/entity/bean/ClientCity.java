package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="client_city")
@IdClass(ClientCityId.class)

public class ClientCity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String idclient;
	@Id
	
	private String idcountry;

	@Id
	
	private String idcity;
	
	
	
	
	public ClientCity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public ClientCity(String idclient, String idcountry, String idcity) {
		super();
		this.idclient = idclient;
		this.idcountry = idcountry;
		this.idcity = idcity;
	}


	public String getIdclient() {
		return idclient;
	}
	public void setIdclient(String idclient) {
		this.idclient = idclient;
	}
	public String getIdcity() {
		return idcity;
	}
	public void setIdcity(String idcity) {
		this.idcity = idcity;
	}
	public String getIdcountry() {
		return idcountry;
	}
	public void setIdcountry(String idcountry) {
		this.idcountry = idcountry;
	}

}
