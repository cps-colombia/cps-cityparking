package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="operator")
public class Operator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name = "operator_id")
	private int operatorId;
	
	@Column(name = "idsys_user")
	private String idSysUser;

	
	
    public Operator() {
    }
    
	public Operator (int operatorId, String idSysUser) {
		super();
		this.operatorId = operatorId;
		this.idSysUser = idSysUser;
	}
	
	public int getOperatorId(){
		return this.operatorId;
	}
	public void setOperatorId(int operatorId){
		this.operatorId = operatorId;
	}
	
	public String getIdSysUser(){
		return this.idSysUser;
	}
	public void setIdSysUser(String idSysUser){
		this.idSysUser = idSysUser;
	}

}
