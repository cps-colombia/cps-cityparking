package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the agent database table.
 * 
 */
@Entity
@Table(name="agent")
public class Agent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idagent;

	private String address;

	@Column(name="country_idcountry")
	private String countryIdcountry;

	private String email;

	private String lastName;

	private String login;

	private String msisdn;

	private String name;

	private String pass;
	
	private String status;

    public Agent() {
    }

	public String getIdagent() {
		return this.idagent;
	}

	public void setIdagent(String idagent) {
		this.idagent = idagent;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountryIdcountry() {
		return this.countryIdcountry;
	}

	public void setCountryIdcountry(String countryIdcountry) {
		this.countryIdcountry = countryIdcountry;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

}