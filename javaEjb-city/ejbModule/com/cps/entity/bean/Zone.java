package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


/**
 * The persistent class for the zone database table.
 * 
 */
@Entity
@Table(name="zone")
public class Zone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String idzone;

	@Column(name="client_idclient")
	private String clientIdclient;

	private Time closeTime;

	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal minuteValue;

	private String latitude;

	private String longitude;

	private String name;

	private Time openTime;
	
	private String gmt;
	
	private String url;
	private String address;
	private String schedule;
	@Column(nullable = false,insertable=false)
	private Integer online;
	@Column(nullable = false,insertable=false)
	private Integer active=0;
	private String email;
	
	@OneToOne
	@Fetch(FetchMode.JOIN)
	 @JoinTable(name="zone_location", 
	    joinColumns=@JoinColumn(name="idzone"),
	    inverseJoinColumns=@JoinColumn(name="idzone"))
	private ZoneLocation zoneLocation;

    public ZoneLocation getZoneLocation() {
		return zoneLocation;
	}

	public void setZoneLocation(ZoneLocation zoneLocation) {
		this.zoneLocation = zoneLocation;
	}

	public Zone() {
    }
    
	public Zone(String idzone, String clientIdclient, Time closeTime,
			BigDecimal minuteValue, String latitude, String longitude, 
			String name, Time openTime) {
		super();
		this.idzone = idzone;
		this.clientIdclient = clientIdclient;
		this.closeTime = closeTime;
		this.minuteValue = minuteValue;
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
		this.openTime = openTime;
	}

	public String getIdzone() {
		return this.idzone;
	}

	public void setIdzone(String idzone) {
		this.idzone = idzone;
	}

	public String getClientIdclient() {
		return this.clientIdclient;
	}

	public void setClientIdclient(String clientIdclient) {
		this.clientIdclient = clientIdclient;
	}

	public Time getCloseTime() {
		return this.closeTime;
	}

	public void setCloseTime(Time closeTime) {
		this.closeTime = closeTime;
	}

	public BigDecimal getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(BigDecimal minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Time getOpenTime() {
		return this.openTime;
	}

	public void setOpenTime(Time openTime) {
		this.openTime = openTime;
	}

	public String getGmt() {
		return gmt;
	}

	public void setGmt(String gmt) {
		this.gmt = gmt;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getOnline() {
		return online;
	}

	public void setOnline(Integer online) {
		this.online = online;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}
	
}