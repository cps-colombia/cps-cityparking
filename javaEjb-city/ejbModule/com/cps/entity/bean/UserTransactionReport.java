package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


//@Entity
public class UserTransactionReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 private String idsysUserOrigin;
	 private String userOriginName;

	 private String idsysUserDest;
	 private String userDestName;

	/*@Id
	@Column(name="transaction_date")*/
	private Date transactionDate;

	private BigDecimal amount;

	
	
	
	public UserTransactionReport(String idsysUserOrigin, String userOriginName,
			String idsysUserDest, String userDestName, Date transactionDate,
			BigDecimal amount) {
		super();
		this.idsysUserOrigin = idsysUserOrigin;
		this.userOriginName = userOriginName;
		this.idsysUserDest = idsysUserDest;
		this.userDestName = userDestName;
		this.transactionDate = transactionDate;
		this.amount = amount;
	}




	public UserTransactionReport() {
		super();
		// TODO Auto-generated constructor stub
	}




	public String getIdsysUserOrigin() {
		return idsysUserOrigin;
	}




	public void setIdsysUserOrigin(String idsysUserOrigin) {
		this.idsysUserOrigin = idsysUserOrigin;
	}




	public String getUserOriginName() {
		return userOriginName;
	}




	public void setUserOriginName(String userOriginName) {
		this.userOriginName = userOriginName;
	}




	public String getIdsysUserDest() {
		return idsysUserDest;
	}




	public void setIdsysUserDest(String idsysUserDest) {
		this.idsysUserDest = idsysUserDest;
	}




	public String getUserDestName() {
		return userDestName;
	}




	public void setUserDestName(String userDestName) {
		this.userDestName = userDestName;
	}




	public Date getTransactionDate() {
		return transactionDate;
	}




	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}




	public BigDecimal getAmount() {
		return amount;
	}




	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}




	






	
	
	
	
}
