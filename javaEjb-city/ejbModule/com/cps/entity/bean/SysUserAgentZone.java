package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * The persistent class for the sys_user_agent_zone database table.
 * 
 */
@Entity
@Table(name="sys_user_agent_zone")
public class SysUserAgentZone implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SysUserAgentZonePK id;

    public SysUserAgentZone() {
    }

	public SysUserAgentZonePK getId() {
		return this.id;
	}

	public void setId(SysUserAgentZonePK id) {
		this.id = id;
	}
	
}