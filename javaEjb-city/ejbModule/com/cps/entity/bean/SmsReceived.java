package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the sms_received database table.
 * 
 */
@Entity
@Table(name="sms_received")
public class SmsReceived implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String smsID;

    @Temporal( TemporalType.TIMESTAMP)
	private Date date;

	private String min;

	private String msisdn;

	private String operator;

	private String opt;

	private String shortCode;

	private String text;

    public SmsReceived() {
    }
    
	public SmsReceived(Date date, String min, String msisdn, String operator,
			String option, String shortCode, String text) {
		super();
		this.date = date;
		this.min = min;
		this.msisdn = msisdn;
		this.operator = operator;
		this.opt = option;
		this.shortCode = shortCode;
		this.text = text;
	}

	public String getSmsID() {
		return this.smsID;
	}

	public void setSmsID(String smsID) {
		this.smsID = smsID;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMin() {
		return this.min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOption() {
		return this.opt;
	}

	public void setOption(String opt) {
		this.opt = opt;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

}