package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Column;

public class CityId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idcity;
	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private String idcountry;

	public String getIdcity() {
		return idcity;
	}

	public void setIdcity(String idcity) {
		this.idcity = idcity;
	}

	public String getIdcountry() {
		return idcountry;
	}

	public void setIdcountry(String idcountry) {
		this.idcountry = idcountry;
	}
	
	
}
