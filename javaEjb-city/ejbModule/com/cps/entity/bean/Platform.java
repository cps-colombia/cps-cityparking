package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the platform database table.
 * 
 */
@Entity
@Table(name="platform")
public class Platform implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int SMS = 1;	
	public static final int MOBILE = 2;
	public static final int TOWER = 3;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idplatform;

	private String name;

    public Platform() {
    }

	public int getIdplatform() {
		return this.idplatform;
	}

	public void setIdplatform(int idplatform) {
		this.idplatform = idplatform;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}