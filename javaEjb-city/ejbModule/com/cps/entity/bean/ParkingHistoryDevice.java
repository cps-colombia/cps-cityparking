package com.cps.entity.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "parking_history_device")
//@Inheritance(strategy = InheritanceType.JOINED)
public class ParkingHistoryDevice {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String idhistory;
	@Column(name="device_id")
	private int deviceId;
	public String getIdhistory() {
		return idhistory;
	}
	public void setIdhistory(String idhistory) {
		this.idhistory = idhistory;
	}
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

  
}
