package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


/**
 * The persistent class for the driver database table.
 * 
 */
@Entity
@Table(name="citydriver")
public class CityDriver implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name = "idcityDriver")
	private int idCityDriver;

	@OneToOne
    @JoinColumn(name="idsys_user")
	@Fetch(FetchMode.JOIN) 
	private SysUser sysUser;
	
	@Column(name = "contact_name")
	private String contactName;
	
	@Column(name = "contact_lastname")
	private String contactLastName;
	
	@Column(name = "contact_phone")
	private String contactPhone;
	
	@Column(name = "address_origin")
	private String addressOrigin;
	
	@Column(name = "address_dest")
	private String addressDest;
	
	private String plate;
	
	private BigInteger documents;	
	
	@Column(name = "date_service")
	private Date dateService;

	@Column(name = "status_id")
	private BigInteger state;
	
	@OneToOne
    @JoinColumn(name="idcityStatus")
	@Fetch(FetchMode.JOIN) 
	private CityDriverStatus idCityStatus;
	
	private String comments;

    @Column(name="idsys_user_creator")
	private BigInteger sysUserCreator;
    
    private Date dateReg;
    
    public CityDriver() {
    }
    
	public CityDriver(int idCityDriver, SysUser sysUser, String contactPhone, String contactName, String contactLastName, 
			String addressOrigin, String addressDest, String plate, Date dateService, BigInteger documents, BigInteger state, String comments,
			CityDriverStatus idCityStatus) {
		super();
		this.idCityDriver = idCityDriver;
		this.sysUser = sysUser;
		this.contactName = contactName;
		this.contactLastName = contactLastName;
		this.contactPhone = contactPhone;
		this.addressOrigin = addressOrigin;
		this.addressDest = addressDest; 
		this.plate = plate;
		this.documents = documents;
		this.dateService = dateService;
		this.state = state;
		this.idCityStatus = idCityStatus;
		this.comments = comments;
	}
	
	public int getIdCityDriver(){
		return this.idCityDriver;
	}
	public void setIdCityDriver(int idCityDriver){
		this.idCityDriver = idCityDriver;
	}
	
	public String getContactPhone() {
		return this.contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	public String getContactName() {
		return this.contactName;
	}	
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
	public String getContactLastName() {
		return this.contactLastName;
	}
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}
	
	public String getAddressOrigin() {
		return this.addressOrigin;
	}	
	public void setAddressOrigin(String addressOrigin) {
		this.addressOrigin = addressOrigin;
	}
	
	public String getAddressDest() {
		return this.addressDest;
	}
	public void setAddressDest(String addressDest) {
		this.addressDest = addressDest;
	}

	public String getPlate() {
		return this.plate;
	}
	public void setPlate(String plate) {
		this.plate = plate;
	}
	
	public Date getDateService() {
		return this.dateService;
	}
	public void setDateService(Date dateService) {
		this.dateService = dateService;
	}

	public BigInteger getDocuments() {
		return this.documents;
	}
	public void setDocuments(BigInteger documents) {
		this.documents = documents;
	}
	
	public BigInteger getState() {
		return this.state;
	}
	public void setState(BigInteger state) {
		this.state = state;
	}	

	public String getComments() {
		return this.comments;
	}	
	public void setComments(String comments) {
		this.comments = comments;
	}	
	
	public SysUser getSysUser() {
		return sysUser;
	}
	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}
	
	public CityDriverStatus getIdCityStatus(){
		return idCityStatus;
	}
	public void setIdCityStatus(CityDriverStatus idCityStatus){
		this.idCityStatus = idCityStatus;
	}
	
	public BigInteger getSysUserCreator(){
		return this.sysUserCreator;
	}
	public void setSysUserCreator(BigInteger sysUserCreator){
		this.sysUserCreator = sysUserCreator;
	}
	
	@PrePersist
	protected void onCreate() {
		dateReg = new Date();
	}
	public Date getDateReg(){
		return dateReg;
	}
	public void setDateReg(Date dateReg ){
		this.dateReg = dateReg;
	}
}