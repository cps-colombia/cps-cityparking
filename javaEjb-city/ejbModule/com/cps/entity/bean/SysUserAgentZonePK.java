package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the sys_user_agent_zone database table.
 * 
 */
@Embeddable
public class SysUserAgentZonePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="sys_user_idsys_user")
	private String sysUserIdsysUser;

	@Column(name="zone_idzone")
	private String zoneIdzone;

    public SysUserAgentZonePK() {
    }
	public String getSysUserIdsysUser() {
		return this.sysUserIdsysUser;
	}
	public void setSysUserIdsysUser(String sysUserIdsysUser) {
		this.sysUserIdsysUser = sysUserIdsysUser;
	}
	public String getZoneIdzone() {
		return this.zoneIdzone;
	}
	public void setZoneIdzone(String zoneIdzone) {
		this.zoneIdzone = zoneIdzone;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SysUserAgentZonePK)) {
			return false;
		}
		SysUserAgentZonePK castOther = (SysUserAgentZonePK)other;
		return 
			this.sysUserIdsysUser.equals(castOther.sysUserIdsysUser)
			&& this.zoneIdzone.equals(castOther.zoneIdzone);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.sysUserIdsysUser.hashCode();
		hash = hash * prime + this.zoneIdzone.hashCode();
		
		return hash;
    }
}