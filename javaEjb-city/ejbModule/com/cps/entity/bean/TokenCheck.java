package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "token_password_check")
public class TokenCheck implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column( name="idtoken_password_check")
	private String id;
	@Column(name="expiration_time")
	private Long expirationTime;
	public TokenCheck() {

	}
	public TokenCheck(String id, Long expirationTime) {
		super();
		this.id = id;
		this.expirationTime = expirationTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(Long expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	
	
}
