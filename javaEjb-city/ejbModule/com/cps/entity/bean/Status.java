package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="status")
public class Status implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name = "status_id")
	private int statusId;
	
	@Column(name = "status_name")
	private String statusName;
	
	@Column(name = "status_description")
	private String statusDescription;
	
	
    public Status() {
    }
    
	public Status (int statusId, String statusName, String statusDescription) {
		super();
		this.statusId = statusId;
		this.statusName = statusName;
		this.statusDescription = statusDescription;
	}
	
	public int getStatusId(){
		return this.statusId;
	}
	public void setStatusId(int statusId){
		this.statusId = statusId;
	}
	
	public String getStatusName(){
		return this.statusName;
	}
	public void setStatusName(String statusName){
		this.statusName = statusName;
	}
	
	public String getStatusDescription(){
		return this.statusDescription;
	}
	public void setStatusId(String statusDescription){
		this.statusDescription = statusDescription;
	}	


}
