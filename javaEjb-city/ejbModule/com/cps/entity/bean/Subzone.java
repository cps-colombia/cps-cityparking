/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cps.entity.bean;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 *
 * @author jmanzur
 */
@Entity
@Table(name = "subzone")
@NamedQueries({
    @NamedQuery(name = "Subzone.findAll", query = "SELECT s FROM Subzone s"),
    @NamedQuery(name = "Subzone.findByIdsubzone", query = "SELECT s FROM Subzone s WHERE s.idsubzone = :idsubzone"),
    @NamedQuery(name = "Subzone.findAllByZone", query = "SELECT s FROM Subzone s WHERE s.idzone.idzone = :idzone"),
    @NamedQuery(name = "Subzone.findByName", query = "SELECT s FROM Subzone s WHERE s.name = :name")})
public class Subzone implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idsubzone")
    private Long idsubzone;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "latitude")
    private Double latitude;
    @Column(name = "longitude")
    private Double longitude;
    @JoinColumn(name = "idzone", referencedColumnName = "idzone")
    @ManyToOne
    private Zone idzone;

    public Subzone() {
    }

    public Subzone(Long idsubzone) {
        this.idsubzone = idsubzone;
    }

    public Subzone(Long idsubzone, String name) {
        this.idsubzone = idsubzone;
        this.name = name;
    }

    public Long getIdsubzone() {
        return idsubzone;
    }

    public void setIdsubzone(Long idsubzone) {
        this.idsubzone = idsubzone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Zone getIdzone() {
        return idzone;
    }

    public void setIdzone(Zone idzone) {
        this.idzone = idzone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsubzone != null ? idsubzone.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subzone)) {
            return false;
        }
        Subzone other = (Subzone) object;
        if ((this.idsubzone == null && other.idsubzone != null) || (this.idsubzone != null && !this.idsubzone.equals(other.idsubzone))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Subzone[ id=" + idsubzone + " name="+name+"]";
    }
    

}