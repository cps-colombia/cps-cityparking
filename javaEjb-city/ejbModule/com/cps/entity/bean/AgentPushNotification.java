package com.cps.entity.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the agent database table.
 * 
 */
@Entity
@Table(name="agent_push_notification")
public class AgentPushNotification implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idagent_push_notification;
	
	private String message;
	
	private String idagent;
	
	private Date dateNotif;
	
	private int isSended;
	
	public AgentPushNotification(){
		
	}

	public String getIdagent_push_notification() {
		return idagent_push_notification;
	}

	public void setIdagent_push_notification(String idagent_push_notification) {
		this.idagent_push_notification = idagent_push_notification;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getIdagent() {
		return idagent;
	}

	public void setIdagent(String idagent) {
		this.idagent = idagent;
	}

	public Date getDateNotif() {
		return dateNotif;
	}

	public void setDateNotif(Date dateNotif) {
		this.dateNotif = dateNotif;
	}

	public int getIsSended() {
		return isSended;
	}

	public void setIsSended(int isSended) {
		this.isSended = isSended;
	}
	
}
