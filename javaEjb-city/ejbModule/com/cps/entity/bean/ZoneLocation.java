package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="zone_location")
public class ZoneLocation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String idzone;
	private BigInteger idcountry;
	private BigInteger idcity;
	public String getIdzone() {
		return idzone;
	}
	public void setIdzone(String idzone) {
		this.idzone = idzone;
	}
	public BigInteger getIdcountry() {
		return idcountry;
	}
	public void setIdcountry(BigInteger idcountry) {
		this.idcountry = idcountry;
	}
	public BigInteger getIdcity() {
		return idcity;
	}
	public void setIdcity(BigInteger idcity) {
		this.idcity = idcity;
	}

	
	
	

}
