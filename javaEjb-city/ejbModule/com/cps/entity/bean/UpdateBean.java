package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class UpdateBean implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	private String idactiveParking;

	private int currentMins;

	private int enableSMSNotif;

	private int maxMins;
	
	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal minuteValue;

	private String msisdn;

	@Column(name="platform_idplatform")
	private int platformIdplatform;

    @Temporal( TemporalType.TIMESTAMP)
	private Date startTime;

	@Column(name="place_idplace")
	private String placeIdplace;
	
	@Column(name="zone_idzone")
	private String zoneIdzone;

	@Column(name="idhistory")
	private String idHistory;

	@Column(name="vehicle_plate")
	private String vehiclePlate;
	 
	@Column(name="idsys_user")
	private String idSysUser;
	
	@Column(name="system_date")
	private Date systemDate;
	
	private int balance;
	
	private String countryLang;
	
	private String zoneName;
	
	private String gmt;

    public UpdateBean() {
    }
    
	public String getIdactiveParking() {
		return this.idactiveParking;
	}

	public void setIdactiveParking(String idactiveParking) {
		this.idactiveParking = idactiveParking;
	}

	public int getCurrentMins() {
		return this.currentMins;
	}

	public void setCurrentMins(int currentMins) {
		this.currentMins = currentMins;
	}

	public int getEnableSMSNotif() {
		return this.enableSMSNotif;
	}

	public void setEnableSMSNotif(int enableSMSNotif) {
		this.enableSMSNotif = enableSMSNotif;
	}

	public int getMaxMins() {
		return this.maxMins;
	}

	public void setMaxMins(int maxMins) {
		this.maxMins = maxMins;
	}

	public BigDecimal getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(BigDecimal minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getPlatformIdplatform() {
		return this.platformIdplatform;
	}

	public void setPlatformIdplatform(int platformIdplatform) {
		this.platformIdplatform = platformIdplatform;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getPlaceIdplace() {
		return this.placeIdplace;
	}

	public void setPlaceIdplace(String placeIdplace) {
		this.placeIdplace = placeIdplace;
	}

	public String getVehiclePlate() {
		return vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

	public String getIdSysUser() {
		return idSysUser;
	}

	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}

	public String getIdHistory() {
		return idHistory;
	}

	public void setIdHistory(String idHistory) {
		this.idHistory = idHistory;
	}

	public String getZoneIdzone() {
		return zoneIdzone;
	}

	public void setZoneIdzone(String zoneIdzone) {
		this.zoneIdzone = zoneIdzone;
	}

	public Date getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getCountryLang() {
		return countryLang;
	}

	public void setCountryLang(String countryLang) {
		this.countryLang = countryLang;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getGmt() {
		return gmt;
	}

	public void setGmt(String gmt) {
		this.gmt = gmt;
	}
	
}
