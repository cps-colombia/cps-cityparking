package com.cps.entity.bean;

import java.io.Serializable;
/**
 * Pojo class to manage parking payments responses
 * @author juan otero
 *
 */
public class PaymentResponse  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3663888631016516659L;
	public static class InfoPayment  implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6890358261301421051L;
		private String id;
		private String validationNumber;
		private String operationResult;
		private String token;
		private String parkingInitDate;
		private String parkingEndDate;
        private double paymentValue;
        private int placeId;
		private int parkingTime;
	
		public InfoPayment(String id, String validationNumber,
				String operationResult, String token, String parkingInitDate,
				String parkingEndDate, double paymentValue, int placeId, int parkingTime) {
			super();
			this.id = id;
			this.validationNumber = validationNumber;
			this.operationResult = operationResult;
			this.token = token;
			this.parkingInitDate = parkingInitDate;
			this.parkingEndDate = parkingEndDate;
			this.paymentValue = paymentValue;
			this.setPlaceId(placeId);
			this.parkingTime= parkingTime;
		}
		public String getParkingInitDate() {
			return parkingInitDate;
		}
		public void setParkingInitDate(String parkingInitDate) {
			this.parkingInitDate = parkingInitDate;
		}
		public String getParkingEndDate() {
			return parkingEndDate;
		}
		public void setParkingEndDate(String parkingEndDate) {
			this.parkingEndDate = parkingEndDate;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getValidationNumber() {
			return validationNumber;
		}
		public void setValidationNumber(String validationNumber) {
			this.validationNumber = validationNumber;
		}
		public String getOperationResult() {
			return operationResult;
		}
		public void setOperationResult(String operationResult) {
			this.operationResult = operationResult;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public double getPaymentValue() {
			return paymentValue;
		}
		public void setPaymentValue(double paymentValue) {
			this.paymentValue = paymentValue;
		}
		public int getPlaceId() {
			return placeId;
		}
		public void setPlaceId(int placeId) {
			this.placeId = placeId;
		}
		public int getParkingTime() {
			return parkingTime;
		}
		public void setParkingTime(int parkingTime) {
			this.parkingTime = parkingTime;
		}
		
		
	}
	private int value;
	private InfoPayment info;
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public InfoPayment getInfo() {
		return info;
	}
	public void setInfo(InfoPayment info) {
		this.info = info;
	}
	

}
