package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the agent_zone database table.
 * 
 */
@Entity
@Table(name="agent_zone")
public class AgentZone implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AgentZonePK id;

    public AgentZone() {
    }

	public AgentZonePK getId() {
		return this.id;
	}

	public void setId(AgentZonePK id) {
		this.id = id;
	}
	
}