package com.cps.entity.bean;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_transaction")
public class UserTransaction {

	@EmbeddedId
	private UserTransactionPK id;
	
	private BigDecimal amount;


	
	public UserTransaction() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public UserTransaction( String userOriginId, String userDestId, Date transDate, BigDecimal amount ) {
		
		super();
		this.id= new UserTransactionPK();
		this.id.setIdsysUserOrigin(userOriginId);
		this.id.setIdsysUserDest(userDestId);
		this.id.setTransactionDate(transDate);
	 	this.amount=amount;
		
		// TODO Auto-generated constructor stub
	}
	
	

	public UserTransactionPK getId() {
		return id;
	}

	public void setId(UserTransactionPK id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	/*public SysUser getUserOrigin() {
		return userOrigin;
	}


	public SysUser getUserDest() {
		return userDest;
	}*/
	
}
