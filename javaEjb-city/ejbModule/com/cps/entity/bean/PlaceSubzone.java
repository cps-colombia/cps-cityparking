/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author jmanzur
 */
@Entity
@Table(name = "place_subzone")
@NamedQueries({
    @NamedQuery(name = "PlaceSubzone.findAll", query = "SELECT p FROM PlaceSubzone p"),
    @NamedQuery(name = "PlaceSubzone.findByIdplace", query = "SELECT p FROM PlaceSubzone p WHERE p.idplace = :idplace")})
public class PlaceSubzone implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idplace")
    private String idplace;
    @JoinColumn(name = "idsubzone", referencedColumnName = "idsubzone")
    @ManyToOne(optional = false)
    private Subzone subzone;

    public PlaceSubzone() {
    }

    public PlaceSubzone(String idplace) {
        this.idplace = idplace;
    }

    public String getIdplace() {
        return idplace;
    }

    public void setIdplace(String idplace) {
        this.idplace = idplace;
    }

    public Subzone getSubzone() {
        return subzone;
    }

    public void setSubzone(Subzone subzone) {
        this.subzone = subzone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idplace != null ? idplace.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlaceSubzone)) {
            return false;
        }
        PlaceSubzone other = (PlaceSubzone) object;
        if ((this.idplace == null && other.idplace != null) || (this.idplace != null && !this.idplace.equals(other.idplace))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PlaceSubzone[ ID=" + idplace + " - IDSubZone="+subzone.getIdsubzone()+" - NameSubzone="+subzone.getName()+" ]";
    }
    
}
