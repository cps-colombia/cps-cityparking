package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the active_parking database table.
 * 
 */
@Entity
@Table(name="active_parking")
public class ActiveParking implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idactiveParking;

	private int currentMins;

	private int enableSMSNotif;

	private int maxMins;
	
	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal minuteValue;

	private String msisdn;

	@Column(name="platform_idplatform")
	private int platformIdplatform;

    @Temporal( TemporalType.TIMESTAMP)
	private Date startTime;

	@Column(name="place_idplace")
	private String placeIdplace;
	
	@Column(name="zone_idzone")
	private String zoneIdzone;

	@Column(name="idhistory")
	private String idHistory;

	@Column(name="vehicle_plate")
	private String vehiclePlate;
	 
	@Column(name="idsys_user")
	private String idSysUser;
	
	@Column(name="system_date")
	private Date systemDate;

    public ActiveParking() {
    }
    
	public ActiveParking(int currentMins, int enableSMSNotif, int maxMins,
			BigDecimal minuteValue, String msisdn, int platformIdplatform,
			Date startTime, String placeIdplace, String idHistory,
			String vehiclePlate, String idSysUser, String zoneIdzone) {
		super();
		this.currentMins = currentMins;
		this.enableSMSNotif = enableSMSNotif;
		this.maxMins = maxMins;
		this.minuteValue = minuteValue;
		this.msisdn = msisdn;
		this.platformIdplatform = platformIdplatform;
		this.startTime = startTime;
		this.placeIdplace = placeIdplace;
		this.idHistory = idHistory;
		this.vehiclePlate = vehiclePlate;
		this.idSysUser = idSysUser;
		this.zoneIdzone = zoneIdzone;
	}
	
	public ActiveParking(int currentMins, int enableSMSNotif, int maxMins,
			BigDecimal minuteValue, String msisdn, int platformIdplatform,
			Date startTime, String placeIdplace, String idHistory,
			String vehiclePlate, String idSysUser, String zoneIdzone, Date systemDate) {
		super();
		this.currentMins = currentMins;
		this.enableSMSNotif = enableSMSNotif;
		this.maxMins = maxMins;
		this.minuteValue = minuteValue;
		this.msisdn = msisdn;
		this.platformIdplatform = platformIdplatform;
		this.startTime = startTime;
		this.placeIdplace = placeIdplace;
		this.idHistory = idHistory;
		this.vehiclePlate = vehiclePlate;
		this.idSysUser = idSysUser;
		this.zoneIdzone = zoneIdzone;
		this.systemDate = systemDate;
	}

	public String getIdactiveParking() {
		return this.idactiveParking;
	}

	public void setIdactiveParking(String idactiveParking) {
		this.idactiveParking = idactiveParking;
	}

	public int getCurrentMins() {
		return this.currentMins;
	}

	public void setCurrentMins(int currentMins) {
		this.currentMins = currentMins;
	}

	public int getEnableSMSNotif() {
		return this.enableSMSNotif;
	}

	public void setEnableSMSNotif(int enableSMSNotif) {
		this.enableSMSNotif = enableSMSNotif;
	}

	public int getMaxMins() {
		return this.maxMins;
	}

	public void setMaxMins(int maxMins) {
		this.maxMins = maxMins;
	}

	public BigDecimal getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(BigDecimal minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getPlatformIdplatform() {
		return this.platformIdplatform;
	}

	public void setPlatformIdplatform(int platformIdplatform) {
		this.platformIdplatform = platformIdplatform;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getPlaceIdplace() {
		return this.placeIdplace;
	}

	public void setPlaceIdplace(String placeIdplace) {
		this.placeIdplace = placeIdplace;
	}

	public String getVehiclePlate() {
		return vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

	public String getIdSysUser() {
		return idSysUser;
	}

	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}

	public String getIdHistory() {
		return idHistory;
	}

	public void setIdHistory(String idHistory) {
		this.idHistory = idHistory;
	}

	public String getZoneIdzone() {
		return zoneIdzone;
	}

	public void setZoneIdzone(String zoneIdzone) {
		this.zoneIdzone = zoneIdzone;
	}

	public Date getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	
}