package com.cps.entity.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the notification database table.
 * 
 */
@Entity
@Table(name="notification")
public class Notification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idnotification;

	private int isread;

	private String message;

	private String msisdn;

	@Column(name="platform_idplatform")
	private int platformIdplatform;

	@Column(name="vehicle_plate")
	private String vehiclePlate;
	
	@Column(name="idsys_user")
	private String idSysUser;
	
	private Date dateNotif;
	
    public Notification() {
    }

	public Notification(int isread, String message, String msisdn,
			int platformIdplatform, String vehiclePlate, String idSysUser) {
		super();
		this.isread = isread;
		this.message = message;
		this.msisdn = msisdn;
		this.platformIdplatform = platformIdplatform;
		this.vehiclePlate = vehiclePlate;
		this.idSysUser = idSysUser;
	}
	
	public Notification(int isread, String message, String msisdn,
			int platformIdplatform, String vehiclePlate, String idSysUser,
			Date dateNotif) {
		super();
		this.isread = isread;
		this.message = message;
		this.msisdn = msisdn;
		this.platformIdplatform = platformIdplatform;
		this.vehiclePlate = vehiclePlate;
		this.idSysUser = idSysUser;
		this.dateNotif = dateNotif;
	}

	public String getIdnotification() {
		return this.idnotification;
	}

	public void setIdnotification(String idnotification) {
		this.idnotification = idnotification;
	}

	public int getIsread() {
		return this.isread;
	}

	public void setIsread(int isread) {
		this.isread = isread;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getPlatformIdplatform() {
		return this.platformIdplatform;
	}

	public void setPlatformIdplatform(int platformIdplatform) {
		this.platformIdplatform = platformIdplatform;
	}
	
	public String getVehiclePlate() {
		return vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

	public String getIdSysUser() {
		return idSysUser;
	}

	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}

	public boolean isRead(){
		return isread == 1;
	}

	public Date getDateNotif() {
		return dateNotif;
	}

	public void setDateNotif(Date dateNotif) {
		this.dateNotif = dateNotif;
	}		
	
}