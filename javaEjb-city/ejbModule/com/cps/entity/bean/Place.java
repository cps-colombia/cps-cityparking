package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


/**
 * The persistent class for the place database table.
 * 
 */
@Entity(name="place")
public class Place implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String idplace;

	@OneToOne
	@JoinColumn(name="idzone")
	private Zone zone;

    public Place() {
    }

	public String getIdplace() {
		return this.idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}

	public Zone getZone() {
		return this.zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}
	
}