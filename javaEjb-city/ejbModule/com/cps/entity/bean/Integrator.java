package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the integrator database table.
 * 
 */
@Entity
@Table(name="integrator")
public class Integrator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idintegrator;

	@Column(name="country_idcountry")
	private String countryIdcountry;

	private String name;

	private String shortCode;

    public Integrator() {
    }

	public String getIdintegrator() {
		return this.idintegrator;
	}

	public void setIdintegrator(String idintegrator) {
		this.idintegrator = idintegrator;
	}

	public String getCountryIdcountry() {
		return this.countryIdcountry;
	}

	public void setCountryIdcountry(String countryIdcountry) {
		this.countryIdcountry = countryIdcountry;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

}