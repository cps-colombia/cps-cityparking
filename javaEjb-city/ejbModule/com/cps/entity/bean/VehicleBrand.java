package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vehicle_brand")
public class VehicleBrand implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name = "vbrand_id")
	private int vBrandId;
	
	@Column(name = "vbrand_name")
	private String vBrandName;
	
	@Column(name = "vbrand_description")
	private String vBrandDescription;
	
	
    public VehicleBrand() {
    }
    
	public VehicleBrand (int vBrandId, String vBrandName, String vBrandDescription) {
		super();
		this.vBrandId = vBrandId;
		this.vBrandName = vBrandName;
		this.vBrandDescription = vBrandDescription;
	}
	
	public int getVBrandId(){
		return this.vBrandId;
	}
	public void setVBrandId(int vBrandId){
		this.vBrandId = vBrandId;
	}
	
	public String getVBrandName(){
		return this.vBrandName;
	}
	public void setVBrandName(String vBrandName){
		this.vBrandName = vBrandName;
	}
	
	public String getVBrandDescription(){
		return this.vBrandDescription;
	}
	public void setVBrandId(String vBrandDescription){
		this.vBrandDescription = vBrandDescription;
	}	

}
