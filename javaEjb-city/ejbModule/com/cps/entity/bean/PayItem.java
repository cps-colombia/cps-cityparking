package com.cps.entity.bean;

import java.io.Serializable;

import javax.xml.ws.Holder;

import com.cps.wservice.payment.verification.ArrayOfPagosV3;

public class PayItem implements Serializable {

	/**
	 * 
	 */
	
	private ArrayOfPagosV3 resPagosV3; 
	private Integer intError ;
	private String strError ; 
	private Integer verificarPagoV3Result;


	public ArrayOfPagosV3 getResPagosV3() {
		return resPagosV3;
	}


	public PayItem(ArrayOfPagosV3 resPagosV3, Integer intError,
			String strError, Integer verificarPagoV3Result) {
		super();
		this.resPagosV3 = resPagosV3;
		this.intError = intError;
		this.strError = strError;
		this.verificarPagoV3Result = verificarPagoV3Result;
	}


	public void setResPagosV3(ArrayOfPagosV3 resPagosV3) {
		this.resPagosV3 = resPagosV3;
	}


	public Integer getIntError() {
		return intError;
	}


	public void setIntError(Integer intError) {
		this.intError = intError;
	}


	public String getStrError() {
		return strError;
	}


	public void setStrError(String strError) {
		this.strError = strError;
	}


	public Integer getVerificarPagoV3Result() {
		return verificarPagoV3Result;
	}


	public void setVerificarPagoV3Result(Integer verificarPagoV3Result) {
		this.verificarPagoV3Result = verificarPagoV3Result;
	}


	private static final long serialVersionUID = -3403708523357442036L;

}
