package com.cps.entity.bean;

import com.cps.wservice.payment.ArrayOfDouble;
import com.cps.wservice.payment.ArrayOfString;

public class PaymentZone {

	 private int _inicioPagoV3_idTienda = 3531;
     private String _inicioPagoV3_clave = "City3531";
     private double _inicioPagoV3_totalConIva = 10800.0;
     private double _inicioPagoV3_valorIva = 800.0;
     private String _inicioPagoV3_idPago = "2701";
     private String _inicioPagoV3_descripcionPago = "City Parking saldo";
     private String _inicioPagoV3_email = "test@mail.com";
     private String _inicioPagoV3_idCliente = "808172";
     private String _inicioPagoV3_tipoId = "";
     private String _inicioPagoV3_nombreCliente = "test";
     private String _inicioPagoV3_apellidoCliente = "test";
     private String _inicioPagoV3_telefonoCliente = "123456";
     private String _inicioPagoV3_infoOpcional1 = "";
     private String _inicioPagoV3_infoOpcional2 = "";
     private String _inicioPagoV3_infoOpcional3 = "";
     private String _inicioPagoV3_codigoServicioPrincipal = "2701";
     private ArrayOfString _inicioPagoV3_listaCodigosServicioMulticredito = null;
     private ArrayOfString _inicioPagoV3_listaNitCodigosServicioMulticredito = null;
     private ArrayOfDouble _inicioPagoV3_listaValoresConIva = null;
     private ArrayOfDouble _inicioPagoV3_listaValoresIva = null;
     private int _inicioPagoV3_totalCodigosServicio = 0;
     private String _inicioPagoV3_archivoAdjuntoNombreOriginal = "";
     private String _inicioPagoV3_archivoAdjuntoNombreGuardado = "";
     private ArrayOfString _inicioPagoV3_listaInfoOpcional = null;
	public PaymentZone() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PaymentZone(int _inicioPagoV3_idTienda, String _inicioPagoV3_clave,
			double _inicioPagoV3_totalConIva, double _inicioPagoV3_valorIva,
			String _inicioPagoV3_idPago, String _inicioPagoV3_descripcionPago,
			String _inicioPagoV3_email, String _inicioPagoV3_idCliente,
			String _inicioPagoV3_tipoId, String _inicioPagoV3_nombreCliente,
			String _inicioPagoV3_apellidoCliente,
			String _inicioPagoV3_telefonoCliente,
			String _inicioPagoV3_infoOpcional1,
			String _inicioPagoV3_infoOpcional2,
			String _inicioPagoV3_infoOpcional3,
			String _inicioPagoV3_codigoServicioPrincipal,
			ArrayOfString _inicioPagoV3_listaCodigosServicioMulticredito,
			ArrayOfString _inicioPagoV3_listaNitCodigosServicioMulticredito,
			ArrayOfDouble _inicioPagoV3_listaValoresConIva,
			ArrayOfDouble _inicioPagoV3_listaValoresIva,
			int _inicioPagoV3_totalCodigosServicio,
			String _inicioPagoV3_archivoAdjuntoNombreOriginal,
			String _inicioPagoV3_archivoAdjuntoNombreGuardado,
			ArrayOfString _inicioPagoV3_listaInfoOpcional) {
		super();
		this._inicioPagoV3_idTienda = _inicioPagoV3_idTienda;
		this._inicioPagoV3_clave = _inicioPagoV3_clave;
		this._inicioPagoV3_totalConIva = _inicioPagoV3_totalConIva;
		this._inicioPagoV3_valorIva = _inicioPagoV3_valorIva;
		this._inicioPagoV3_idPago = _inicioPagoV3_idPago;
		this._inicioPagoV3_descripcionPago = _inicioPagoV3_descripcionPago;
		this._inicioPagoV3_email = _inicioPagoV3_email;
		this._inicioPagoV3_idCliente = _inicioPagoV3_idCliente;
		this._inicioPagoV3_tipoId = _inicioPagoV3_tipoId;
		this._inicioPagoV3_nombreCliente = _inicioPagoV3_nombreCliente;
		this._inicioPagoV3_apellidoCliente = _inicioPagoV3_apellidoCliente;
		this._inicioPagoV3_telefonoCliente = _inicioPagoV3_telefonoCliente;
		this._inicioPagoV3_infoOpcional1 = _inicioPagoV3_infoOpcional1;
		this._inicioPagoV3_infoOpcional2 = _inicioPagoV3_infoOpcional2;
		this._inicioPagoV3_infoOpcional3 = _inicioPagoV3_infoOpcional3;
		this._inicioPagoV3_codigoServicioPrincipal = _inicioPagoV3_codigoServicioPrincipal;
		this._inicioPagoV3_listaCodigosServicioMulticredito = _inicioPagoV3_listaCodigosServicioMulticredito;
		this._inicioPagoV3_listaNitCodigosServicioMulticredito = _inicioPagoV3_listaNitCodigosServicioMulticredito;
		this._inicioPagoV3_listaValoresConIva = _inicioPagoV3_listaValoresConIva;
		this._inicioPagoV3_listaValoresIva = _inicioPagoV3_listaValoresIva;
		this._inicioPagoV3_totalCodigosServicio = _inicioPagoV3_totalCodigosServicio;
		this._inicioPagoV3_archivoAdjuntoNombreOriginal = _inicioPagoV3_archivoAdjuntoNombreOriginal;
		this._inicioPagoV3_archivoAdjuntoNombreGuardado = _inicioPagoV3_archivoAdjuntoNombreGuardado;
		this._inicioPagoV3_listaInfoOpcional = _inicioPagoV3_listaInfoOpcional;
	}
	public int get_inicioPagoV3_idTienda() {
		return _inicioPagoV3_idTienda;
	}
	public void set_inicioPagoV3_idTienda(int _inicioPagoV3_idTienda) {
		this._inicioPagoV3_idTienda = _inicioPagoV3_idTienda;
	}
	public String get_inicioPagoV3_clave() {
		return _inicioPagoV3_clave;
	}
	public void set_inicioPagoV3_clave(String _inicioPagoV3_clave) {
		this._inicioPagoV3_clave = _inicioPagoV3_clave;
	}
	public double get_inicioPagoV3_totalConIva() {
		return _inicioPagoV3_totalConIva;
	}
	public void set_inicioPagoV3_totalConIva(double _inicioPagoV3_totalConIva) {
		this._inicioPagoV3_totalConIva = _inicioPagoV3_totalConIva;
	}
	public double get_inicioPagoV3_valorIva() {
		return _inicioPagoV3_valorIva;
	}
	public void set_inicioPagoV3_valorIva(double _inicioPagoV3_valorIva) {
		this._inicioPagoV3_valorIva = _inicioPagoV3_valorIva;
	}
	public String get_inicioPagoV3_idPago() {
		return _inicioPagoV3_idPago;
	}
	public void set_inicioPagoV3_idPago(String _inicioPagoV3_idPago) {
		this._inicioPagoV3_idPago = _inicioPagoV3_idPago;
	}
	public String get_inicioPagoV3_descripcionPago() {
		return _inicioPagoV3_descripcionPago;
	}
	public void set_inicioPagoV3_descripcionPago(
			String _inicioPagoV3_descripcionPago) {
		this._inicioPagoV3_descripcionPago = _inicioPagoV3_descripcionPago;
	}
	public String get_inicioPagoV3_email() {
		return _inicioPagoV3_email;
	}
	public void set_inicioPagoV3_email(String _inicioPagoV3_email) {
		this._inicioPagoV3_email = _inicioPagoV3_email;
	}
	public String get_inicioPagoV3_idCliente() {
		return _inicioPagoV3_idCliente;
	}
	public void set_inicioPagoV3_idCliente(String _inicioPagoV3_idCliente) {
		this._inicioPagoV3_idCliente = _inicioPagoV3_idCliente;
	}
	public String get_inicioPagoV3_tipoId() {
		return _inicioPagoV3_tipoId;
	}
	public void set_inicioPagoV3_tipoId(String _inicioPagoV3_tipoId) {
		this._inicioPagoV3_tipoId = _inicioPagoV3_tipoId;
	}
	public String get_inicioPagoV3_nombreCliente() {
		return _inicioPagoV3_nombreCliente;
	}
	public void set_inicioPagoV3_nombreCliente(String _inicioPagoV3_nombreCliente) {
		this._inicioPagoV3_nombreCliente = _inicioPagoV3_nombreCliente;
	}
	public String get_inicioPagoV3_apellidoCliente() {
		return _inicioPagoV3_apellidoCliente;
	}
	public void set_inicioPagoV3_apellidoCliente(
			String _inicioPagoV3_apellidoCliente) {
		this._inicioPagoV3_apellidoCliente = _inicioPagoV3_apellidoCliente;
	}
	public String get_inicioPagoV3_telefonoCliente() {
		return _inicioPagoV3_telefonoCliente;
	}
	public void set_inicioPagoV3_telefonoCliente(
			String _inicioPagoV3_telefonoCliente) {
		this._inicioPagoV3_telefonoCliente = _inicioPagoV3_telefonoCliente;
	}
	public String get_inicioPagoV3_infoOpcional1() {
		return _inicioPagoV3_infoOpcional1;
	}
	public void set_inicioPagoV3_infoOpcional1(String _inicioPagoV3_infoOpcional1) {
		this._inicioPagoV3_infoOpcional1 = _inicioPagoV3_infoOpcional1;
	}
	public String get_inicioPagoV3_infoOpcional2() {
		return _inicioPagoV3_infoOpcional2;
	}
	public void set_inicioPagoV3_infoOpcional2(String _inicioPagoV3_infoOpcional2) {
		this._inicioPagoV3_infoOpcional2 = _inicioPagoV3_infoOpcional2;
	}
	public String get_inicioPagoV3_infoOpcional3() {
		return _inicioPagoV3_infoOpcional3;
	}
	public void set_inicioPagoV3_infoOpcional3(String _inicioPagoV3_infoOpcional3) {
		this._inicioPagoV3_infoOpcional3 = _inicioPagoV3_infoOpcional3;
	}
	public String get_inicioPagoV3_codigoServicioPrincipal() {
		return _inicioPagoV3_codigoServicioPrincipal;
	}
	public void set_inicioPagoV3_codigoServicioPrincipal(
			String _inicioPagoV3_codigoServicioPrincipal) {
		this._inicioPagoV3_codigoServicioPrincipal = _inicioPagoV3_codigoServicioPrincipal;
	}
	public ArrayOfString get_inicioPagoV3_listaCodigosServicioMulticredito() {
		return _inicioPagoV3_listaCodigosServicioMulticredito;
	}
	public void set_inicioPagoV3_listaCodigosServicioMulticredito(
			ArrayOfString _inicioPagoV3_listaCodigosServicioMulticredito) {
		this._inicioPagoV3_listaCodigosServicioMulticredito = _inicioPagoV3_listaCodigosServicioMulticredito;
	}
	public ArrayOfString get_inicioPagoV3_listaNitCodigosServicioMulticredito() {
		return _inicioPagoV3_listaNitCodigosServicioMulticredito;
	}
	public void set_inicioPagoV3_listaNitCodigosServicioMulticredito(
			ArrayOfString _inicioPagoV3_listaNitCodigosServicioMulticredito) {
		this._inicioPagoV3_listaNitCodigosServicioMulticredito = _inicioPagoV3_listaNitCodigosServicioMulticredito;
	}
	public ArrayOfDouble get_inicioPagoV3_listaValoresConIva() {
		return _inicioPagoV3_listaValoresConIva;
	}
	public void set_inicioPagoV3_listaValoresConIva(
			ArrayOfDouble _inicioPagoV3_listaValoresConIva) {
		this._inicioPagoV3_listaValoresConIva = _inicioPagoV3_listaValoresConIva;
	}
	public ArrayOfDouble get_inicioPagoV3_listaValoresIva() {
		return _inicioPagoV3_listaValoresIva;
	}
	public void set_inicioPagoV3_listaValoresIva(
			ArrayOfDouble _inicioPagoV3_listaValoresIva) {
		this._inicioPagoV3_listaValoresIva = _inicioPagoV3_listaValoresIva;
	}
	public int get_inicioPagoV3_totalCodigosServicio() {
		return _inicioPagoV3_totalCodigosServicio;
	}
	public void set_inicioPagoV3_totalCodigosServicio(
			int _inicioPagoV3_totalCodigosServicio) {
		this._inicioPagoV3_totalCodigosServicio = _inicioPagoV3_totalCodigosServicio;
	}
	public String get_inicioPagoV3_archivoAdjuntoNombreOriginal() {
		return _inicioPagoV3_archivoAdjuntoNombreOriginal;
	}
	public void set_inicioPagoV3_archivoAdjuntoNombreOriginal(
			String _inicioPagoV3_archivoAdjuntoNombreOriginal) {
		this._inicioPagoV3_archivoAdjuntoNombreOriginal = _inicioPagoV3_archivoAdjuntoNombreOriginal;
	}
	public String get_inicioPagoV3_archivoAdjuntoNombreGuardado() {
		return _inicioPagoV3_archivoAdjuntoNombreGuardado;
	}
	public void set_inicioPagoV3_archivoAdjuntoNombreGuardado(
			String _inicioPagoV3_archivoAdjuntoNombreGuardado) {
		this._inicioPagoV3_archivoAdjuntoNombreGuardado = _inicioPagoV3_archivoAdjuntoNombreGuardado;
	}
	public ArrayOfString get_inicioPagoV3_listaInfoOpcional() {
		return _inicioPagoV3_listaInfoOpcional;
	}
	public void set_inicioPagoV3_listaInfoOpcional(
			ArrayOfString _inicioPagoV3_listaInfoOpcional) {
		this._inicioPagoV3_listaInfoOpcional = _inicioPagoV3_listaInfoOpcional;
	}
     
     

     
     
}
