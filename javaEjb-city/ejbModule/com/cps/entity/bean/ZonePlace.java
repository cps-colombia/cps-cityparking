package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ZonePlace implements Serializable{
	
	private static final long serialVersionUID = 1L;
		
	private String idzone;
	
	@Id
	private String idplace;

	private Time closeTime;

	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal minuteValue;

	private String latitude;

	private String longitude;

	private String name;

	private Time openTime;
	
	public ZonePlace(){
		
	}

	public String getIdzone() {
		return idzone;
	}

	public void setIdzone(String idzone) {
		this.idzone = idzone;
	}

	public Time getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Time closeTime) {
		this.closeTime = closeTime;
	}

	public BigDecimal getMinuteValue() {
		return minuteValue;
	}

	public void setMinuteValue(BigDecimal minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Time getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Time openTime) {
		this.openTime = openTime;
	}

	public String getIdplace() {
		return idplace;
	}

	public void setIdplace(String idplace) {
		this.idplace = idplace;
	}
	
}
