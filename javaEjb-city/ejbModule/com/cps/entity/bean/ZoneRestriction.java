package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;

import javax.persistence.Column;

public class ZoneRestriction implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Time startTime;
	
	private Time endTime;

	private int maxMinutes;
	
	private String zoneIdzone;
	
	private String zoneName;

	private int day;
	
	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal price;
	
	public ZoneRestriction(){
		
	}

	public ZoneRestriction(Time startTime, Time endTime, int maxMinutes,
			String zoneIdzone, int day, BigDecimal price, String zoneName) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.maxMinutes = maxMinutes;
		this.zoneIdzone = zoneIdzone;
		this.day = day;
		this.price = price;
		this.zoneName = zoneName;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public int getMaxMinutes() {
		return maxMinutes;
	}

	public void setMaxMinutes(int maxMinutes) {
		this.maxMinutes = maxMinutes;
	}

	public String getZoneIdzone() {
		return zoneIdzone;
	}

	public void setZoneIdzone(String zoneIdzone) {
		this.zoneIdzone = zoneIdzone;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	
}
