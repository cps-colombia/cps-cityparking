package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sys_user_vehicle database table.
 * 
 */
@Entity
@Table(name="sys_user_vehicle")
public class SysUserVehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idsys_user_vehicle")
	private String idsysUserVehicle;

	@Column(name="idsys_user")
	private String idsysUser;

	private int owner;

	@Column(name="vehicle_plate")
	private String vehiclePlate;

    public SysUserVehicle() {
    }

	public String getIdsysUserVehicle() {
		return this.idsysUserVehicle;
	}

	public void setIdsysUserVehicle(String idsysUserVehicle) {
		this.idsysUserVehicle = idsysUserVehicle;
	}

	public String getIdsysUser() {
		return this.idsysUser;
	}

	public void setIdsysUser(String idsysUser) {
		this.idsysUser = idsysUser;
	}

	public int getOwner() {
		return this.owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public String getVehiclePlate() {
		return this.vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

}