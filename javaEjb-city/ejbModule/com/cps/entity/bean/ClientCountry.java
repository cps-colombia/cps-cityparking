package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="client_country")
@IdClass(ClientCountryId.class)
public class ClientCountry implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String idclient;
	@Id
	
	
	private String idcountry;
	
	
	public ClientCountry() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ClientCountry(String idclient, String idcountry) {
		super();
		this.idclient = idclient;
		this.idcountry = idcountry;
	}

	public String getIdclient() {
		return idclient;
	}
	public void setIdclient(String idclient) {
		this.idclient = idclient;
	}
	public String getIdcountry() {
		return idcountry;
	}
	public void setIdcountry(String idcountry) {
		this.idcountry = idcountry;
	}	
}
