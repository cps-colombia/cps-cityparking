package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the vehicle database table.
 *
 */
@Entity
@Table(name="vehicle")
public class Vehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String plate;

	private BigInteger type;

	private BigInteger brand;

	private String description;

	private int state;

	@Temporal( TemporalType.TIMESTAMP)
	private Date dateReg;


    public Vehicle() {
    }

	public String getPlate() {
		return this.plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public BigInteger getType(){
		return this.type;
	}
	public void setType(BigInteger type){
		this.type = type;
	}

	public BigInteger getBrand(){
		return this.brand;
	}
	public void setBrand(BigInteger brand){
		this.brand = brand;
	}

	public int getState() {
		return this.state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getDateReg() {
		return this.dateReg;
	}

	public void setDateReg(Date dateReg) {
		this.dateReg = dateReg;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;

	}

}
