package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="campaign_transaction_log")
public class CampaignLogReport implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idcampaign_transaction_log")
	BigInteger  idCampaignLog;
	@Column


	//@Column(name="range_amount_start")
	private BigDecimal amount ;
	


	@Column(name="amount_percent")
	private BigDecimal amountPercent ;
	//@Column(name="campaign_state")
	@Column(name="log_date")
	private Date logDate ; 
	
	
	@OneToOne 
    @JoinColumn(name="id_sys_user" )
	private SysUser sysUser;
	
	
	
	public CampaignLogReport() {
		
	}
	
	public CampaignLogReport(BigInteger idCampaignLog, BigDecimal amount,
			BigDecimal amountPercent, Date logDate, SysUser sysUser) {
		super();
		this.idCampaignLog = idCampaignLog;
		this.amount = amount;
		this.amountPercent = amountPercent;
		this.logDate = logDate;
		this.sysUser = sysUser;
	}

	public BigInteger getIdCampaignLog() {
		return idCampaignLog;
	}

	public void setIdCampaignLog(BigInteger idCampaignLog) {
		this.idCampaignLog = idCampaignLog;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountPercent() {
		return amountPercent;
	}

	public void setAmountPercent(BigDecimal amountPercent) {
		this.amountPercent = amountPercent;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public SysUser getSysUser() {
		return sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}
	
	


	
}
