package com.cps.entity.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserTransactionPK implements Serializable {

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "idsysuser_origen")
	private String idsysUserOrigin;
	@Column(name = "idsysuser_dest")
	private String idsysUserDest;

	@Column(name = "transaction_date")
	private Date transactionDate;

	public String getIdsysUserOrigin() {
		return idsysUserOrigin;
	}

	public void setIdsysUserOrigin(String idsysUserOrigin) {
		this.idsysUserOrigin = idsysUserOrigin;
	}

	public String getIdsysUserDest() {
		return idsysUserDest;
	}

	public void setIdsysUserDest(String idsysUserDest) {
		this.idsysUserDest = idsysUserDest;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

}
