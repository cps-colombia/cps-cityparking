package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sys_user_type database table.
 * 
 */
@Entity
@Table(name="sys_user_type")
public class SysUserType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final int ADMIN = 1;
	
	public static final int USER = 2;
	
	public static final int TEMP_USER = 3;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idsys_user_type")
	private int idsysUserType;

	private String name;

    public SysUserType() {
    }

	public int getIdsysUserType() {
		return this.idsysUserType;
	}

	public void setIdsysUserType(int idsysUserType) {
		this.idsysUserType = idsysUserType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}