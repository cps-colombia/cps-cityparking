package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tower database table.
 * 
 */
@Entity(name="tower")
public class Tower implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String towerId;

	private String idZone;

    public Tower() {
    }

	public String getTowerId() {
		return this.towerId;
	}

	public void setTowerId(String towerId) {
		this.towerId = towerId;
	}

	public String getIdZone() {
		return this.idZone;
	}

	public void setIdZone(String idZone) {
		this.idZone = idZone;
	}

}