package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the agent_zone database table.
 * 
 */
@Embeddable
public class AgentZonePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String idZone;

	private String idAgent;

    public AgentZonePK() {
    }
	public String getIdZone() {
		return this.idZone;
	}
	public void setIdZone(String idZone) {
		this.idZone = idZone;
	}
	public String getIdAgent() {
		return this.idAgent;
	}
	public void setIdAgent(String idAgent) {
		this.idAgent = idAgent;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AgentZonePK)) {
			return false;
		}
		AgentZonePK castOther = (AgentZonePK)other;
		return 
			this.idZone.equals(castOther.idZone)
			&& this.idAgent.equals(castOther.idAgent);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idZone.hashCode();
		hash = hash * prime + this.idAgent.hashCode();
		
		return hash;
    }
}