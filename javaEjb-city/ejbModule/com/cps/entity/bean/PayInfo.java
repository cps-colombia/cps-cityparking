
package com.cps.entity.bean;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


public class PayInfo {

    private String token;
    private long identificador;
    private int plazaId;
    private XMLGregorianCalendar fechaInicioParqueo;
    private XMLGregorianCalendar fechaFinParqueo;
    private BigDecimal importeParqueo;
    private int tiempoParqueo;
    private int codigoRespuesta;
    private int codigoRespuestaPago;

    
    
    
    public PayInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

    
	public PayInfo(String token, long identificador, int plazaId,
			XMLGregorianCalendar fechaInicioParqueo,
			XMLGregorianCalendar fechaFinParqueo, BigDecimal importeParqueo,
			int tiempoParqueo, int codigoRespuesta, int codigoRespuestaPago) {
		super();
		this.token = token;
		this.identificador = identificador;
		this.plazaId = plazaId;
		this.fechaInicioParqueo = fechaInicioParqueo;
		this.fechaFinParqueo = fechaFinParqueo;
		this.importeParqueo = importeParqueo;
		this.tiempoParqueo = tiempoParqueo;
		this.codigoRespuesta = codigoRespuesta;
		this.codigoRespuestaPago = codigoRespuestaPago;
	}


	/**
     * Obtiene el valor de la propiedad token.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     */
    public long getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     */
    public void setIdentificador(long value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad plazaId.
     * 
     */
    public int getPlazaId() {
        return plazaId;
    }

    /**
     * Define el valor de la propiedad plazaId.
     * 
     */
    public void setPlazaId(int value) {
        this.plazaId = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicioParqueo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicioParqueo() {
        return fechaInicioParqueo;
    }

    /**
     * Define el valor de la propiedad fechaInicioParqueo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicioParqueo(XMLGregorianCalendar value) {
        this.fechaInicioParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFinParqueo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaFinParqueo() {
        return fechaFinParqueo;
    }

    /**
     * Define el valor de la propiedad fechaFinParqueo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaFinParqueo(XMLGregorianCalendar value) {
        this.fechaFinParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad importeParqueo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getImporteParqueo() {
        return importeParqueo;
    }

    /**
     * Define el valor de la propiedad importeParqueo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setImporteParqueo(BigDecimal value) {
        this.importeParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad tiempoParqueo.
     * 
     */
    public int getTiempoParqueo() {
        return tiempoParqueo;
    }

    /**
     * Define el valor de la propiedad tiempoParqueo.
     * 
     */
    public void setTiempoParqueo(int value) {
        this.tiempoParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoRespuesta.
     * 
     */
    public int getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Define el valor de la propiedad codigoRespuesta.
     * 
     */
    public void setCodigoRespuesta(int value) {
        this.codigoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoRespuestaPago.
     * 
     */
    public int getCodigoRespuestaPago() {
        return codigoRespuestaPago;
    }

    /**
     * Define el valor de la propiedad codigoRespuestaPago.
     * 
     */
    public void setCodigoRespuestaPago(int value) {
        this.codigoRespuestaPago = value;
    }

}
