package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the agent_zone database table.
 * 
 */
@Entity
@Table(name="agent_subzone")
public class AgentSubZone implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AgentSubZonePK id;
	
	public AgentSubZone() {
    }

    public AgentSubZonePK getId() {
		return id;
	}

	public void setId(AgentSubZonePK id) {
		this.id = id;
	}



}