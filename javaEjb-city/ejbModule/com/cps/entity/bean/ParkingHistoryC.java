package com.cps.entity.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the parking_history database table.
 * 
 */
@Entity
@Table(name="parking_history")
public class ParkingHistoryC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idhistory;

    @Temporal( TemporalType.TIMESTAMP)
	private Date endTime;

	private double minuteValue;

	private String msisdn;

	@Column(name="platform_idplatform")
	private int platformIdplatform;

    @Temporal( TemporalType.TIMESTAMP)
	private Date startTime;

    @OneToOne 
    @JoinColumn(name="place_idplace") 
    private Place place;
    
    @OneToOne 
    @JoinColumn(name="zone_idzone") 
    private Zone zone;

	@Column(name="vehicle_plate")
	private String vehiclePlate;
	
	@Column(name="idsys_user")
	private String idSysUser;
	
	
	@Column(name="pay_id")
	private String payId;
	
	@Column(name="fee")
	private double fee;
	
	
	
    public ParkingHistoryC() {
    }

	public ParkingHistoryC(Date endTime, double minuteValue, String msisdn,
			int platformIdplatform, Date startTime
			,String vehiclePlate, String idSysUser) {
		super();
		this.endTime = endTime;
		this.minuteValue = minuteValue;
		this.msisdn = msisdn;
		this.platformIdplatform = platformIdplatform;
		this.startTime = startTime;
		this.vehiclePlate = vehiclePlate;
		this.idSysUser = idSysUser;				
	}

	public String getIdhistory() {
		return this.idhistory;
	}

	public void setIdhistory(String idhistory) {
		this.idhistory = idhistory;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public double getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(double minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getPlatformIdplatform() {
		return this.platformIdplatform;
	}

	public void setPlatformIdplatform(int platformIdplatform) {
		this.platformIdplatform = platformIdplatform;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getVehiclePlate() {
		return vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

	public String getIdSysUser() {
		return idSysUser;
	}

	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}

	/*public Place getPlace() {
		return place;
	}

	public void setZone(Place place) {
		this.place = place;
	}*/

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}
	
	
	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}
}