package com.cps.entity.bean;

import java.io.Serializable;


public class ClientCityId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idclient;
	private String idcity;
	private String idcountry;
	public String getIdclient() {
		return idclient;
	}
	public void setIdclient(String idclient) {
		this.idclient = idclient;
	}
	public String getIdcity() {
		return idcity;
	}
	public void setIdcity(String idcity) {
		this.idcity = idcity;
	}
	public String getIdcountry() {
		return idcountry;
	}
	public void setIdcountry(String idcountry) {
		this.idcountry = idcountry;
	}

}
