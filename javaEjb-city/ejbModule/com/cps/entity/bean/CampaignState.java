package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="campaign_state")
public class CampaignState implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idcampaign_state")
	int  idCampaignState;
	@Column(name="state_name")
	private String stateName; 
	
	
	
	
	public CampaignState() {
		
	}




	public CampaignState(int idCampaignState, String stateName) {
		super();
		this.idCampaignState = idCampaignState;
		this.stateName = stateName;
	}




	public int getIdCampaignState() {
		return idCampaignState;
	}




	public void setIdCampaignState(int idCampaignState) {
		this.idCampaignState = idCampaignState;
	}




	public String getStateName() {
		return stateName;
	}




	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	

	

}
