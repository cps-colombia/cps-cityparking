package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class NotificationMQString implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private String notifications;
	
	public NotificationMQString(){
		
	}

	public String getNotifications() {
		return notifications;
	}

	public void setNotifications(String notifications) {
		this.notifications = notifications;
	}
	
}
