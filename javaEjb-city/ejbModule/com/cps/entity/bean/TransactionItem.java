package com.cps.entity.bean;

import java.io.Serializable;

public class TransactionItem implements Serializable {

	public TransactionItem(String transactionType, Integer quantity,
			Float totalAmount) {
		super();
		this.transactionType = transactionType;
		this.quantity = quantity;
		this.totalAmount = totalAmount;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String transactionType;
	private Integer quantity;
	private Float totalAmount;

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Float totalAmount) {
		this.totalAmount = totalAmount;
	}

}
