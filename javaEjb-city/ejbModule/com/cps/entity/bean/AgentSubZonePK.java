package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the agent_zone database table.
 * 
 */
@Embeddable
public class AgentSubZonePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String idSubZone;

	private String idAgent;

    public AgentSubZonePK() {
    }

    public String getIdAgent() {
		return this.idAgent;
	}
	public void setIdAgent(String idAgent) {
		this.idAgent = idAgent;
	}

	public String getIdSubZone() {
		return idSubZone;
	}

	public void setIdSubZone(String idSubZone) {
		this.idSubZone = idSubZone;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AgentSubZonePK)) {
			return false;
		}
		AgentSubZonePK castOther = (AgentSubZonePK)other;
		return 
			this.idSubZone.equals(castOther.idSubZone)
			&& this.idAgent.equals(castOther.idAgent);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idSubZone.hashCode();
		hash = hash * prime + this.idAgent.hashCode();
		
		return hash;
    }
}