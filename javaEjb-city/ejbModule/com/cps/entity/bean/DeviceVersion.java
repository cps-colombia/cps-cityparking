package com.cps.entity.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="device_app_version")
public class DeviceVersion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="device_id")
	private int deviceId;
	
	@Column(name="device_name")
	private String deviceName;
	
	@Column(name="current_version")
	private String currVersion;
	
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getCurrVersion() {
		return currVersion;
	}
	public void setCurrVersion(String currVersion) {
		this.currVersion = currVersion;
	}
	
	

}
