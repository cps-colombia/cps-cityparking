package com.cps.entity.bean;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;


/**
 * The persistent class for the time_restrict database table.
 * 
 */
@Entity
@Table(name="time_restrict")
public class TimeRestrict implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idtime_restrict")
	private String idtimeRestrict;

	private Time endTime;

	private int maxMinutes;

	private Time startTime;

	@Column(name="zone_idzone")
	private String zoneIdzone;

	private int day;
	
    public TimeRestrict() {
    }

	public String getIdtimeRestrict() {
		return this.idtimeRestrict;
	}

	public void setIdtimeRestrict(String idtimeRestrict) {
		this.idtimeRestrict = idtimeRestrict;
	}

	public Time getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public int getMaxMinutes() {
		return this.maxMinutes;
	}

	public void setMaxMinutes(int maxMinutes) {
		this.maxMinutes = maxMinutes;
	}

	public Time getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public String getZoneIdzone() {
		return this.zoneIdzone;
	}

	public void setZoneIdzone(String zoneIdzone) {
		this.zoneIdzone = zoneIdzone;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

}