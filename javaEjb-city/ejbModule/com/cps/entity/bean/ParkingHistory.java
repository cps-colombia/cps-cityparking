package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the parking_history database table.
 * 
 */
@Entity
@Table(name="parking_history")
public class ParkingHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idhistory;

    @Temporal( TemporalType.TIMESTAMP)
	private Date endTime;
    
    @Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal minuteValue;

	private String msisdn;

	@Column(name="platform_idplatform")
	private int platformIdplatform;

    @Temporal( TemporalType.TIMESTAMP)
	private Date startTime;

	@Column(name="place_idplace")
	private String placeIdplace;
	
	@Column(name="zone_idzone")
	private String zoneIdzone;

	@Column(name="vehicle_plate")
	private String vehiclePlate;
	
	@Column(name="idsys_user")
	private String idSysUser;
	@Column(name="pay_id")
	private String payId;
	@Column(name="fee")
	private double fee;
	
	
	@Column(name="confirm_number")
	private String confirmationNumber;
	
    public ParkingHistory() {
    }

	public ParkingHistory(Date endTime, BigDecimal minuteValue, String msisdn,
			int platformIdplatform, Date startTime, String placeIdplace
			,String vehiclePlate, String idSysUser, String zoneIdzone) {
		super();
		this.endTime = endTime;
		this.minuteValue = minuteValue;
		this.msisdn = msisdn;
		this.platformIdplatform = platformIdplatform;
		this.startTime = startTime;
		this.placeIdplace = placeIdplace;
		this.vehiclePlate = vehiclePlate;
		this.idSysUser = idSysUser;				
		this.zoneIdzone = zoneIdzone;
	
	}

	
	public ParkingHistory(Date endTime, BigDecimal minuteValue, String msisdn,
			int platformIdplatform, Date startTime, String placeIdplace
			,String vehiclePlate, String idSysUser, String zoneIdzone, String payId, double fee,
			String confirmationNumber) {
		super();
		this.endTime = endTime;
		this.minuteValue = minuteValue;
		this.msisdn = msisdn;
		this.platformIdplatform = platformIdplatform;
		this.startTime = startTime;
		this.placeIdplace = placeIdplace;
		this.vehiclePlate = vehiclePlate;
		this.idSysUser = idSysUser;				
		this.zoneIdzone = zoneIdzone;
		this.payId=payId;
		this.fee=fee;
		this.confirmationNumber=confirmationNumber;
	}
	public String getIdhistory() {
		return this.idhistory;
	}

	public void setIdhistory(String idhistory) {
		this.idhistory = idhistory;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public BigDecimal getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(BigDecimal minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getPlatformIdplatform() {
		return this.platformIdplatform;
	}

	public void setPlatformIdplatform(int platformIdplatform) {
		this.platformIdplatform = platformIdplatform;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getPlaceIdplace() {
		return this.placeIdplace;
	}

	public void setPlaceIdplace(String placeIdplace) {
		this.placeIdplace = placeIdplace;
	}

	public String getVehiclePlate() {
		return vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

	public String getIdSysUser() {
		return idSysUser;
	}

	public void setIdSysUser(String idSysUser) {
		this.idSysUser = idSysUser;
	}

	public String getZoneIdzone() {
		return zoneIdzone;
	}

	public void setZoneIdzone(String zoneIdzone) {
		this.zoneIdzone = zoneIdzone;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	
}