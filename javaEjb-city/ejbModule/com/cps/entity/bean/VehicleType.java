package com.cps.entity.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vehicle_type")
public class VehicleType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name = "vtype_id")
	private int vTypeId;
	
	@Column(name = "vtype_name")
	private String vTypeName;
	
	@Column(name = "vtype_description")
	private String vTypeDescription;
	
	
    public VehicleType() {
    }
    
	public VehicleType (int vTypeId, String vTypeName, String vTypeDescription) {
		super();
		this.vTypeId = vTypeId;
		this.vTypeName = vTypeName;
		this.vTypeDescription = vTypeDescription;
	}

	public int getvTypeId() {
		return vTypeId;
	}

	public void setvTypeId(int vTypeId) {
		this.vTypeId = vTypeId;
	}

	public String getvTypeName() {
		return vTypeName;
	}

	public void setvTypeName(String vTypeName) {
		this.vTypeName = vTypeName;
	}

	public String getvTypeDescription() {
		return vTypeDescription;
	}

	public void setvTypeDescription(String vTypeDescription) {
		this.vTypeDescription = vTypeDescription;
	}
	

	
	
}
