package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the sys_user database table.
 */
@Entity
@Table(name = "sys_user")
public class SysUser implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idsys_user")
	private String idsysUser;

	private String address;

	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal balance;

	@Temporal(TemporalType.DATE)
	private Date birthDate;

	@Column(name = "country_idcountry")
	private String countryIdcountry;

	private String cu;

	private String email;

	@Column(name = "idsys_user_type")
	private int idsysUserType;

	private String lastName;

	private String login;

	private String name;

	private String pass;

	private String favoriteMsisdn;

	@OneToOne
	@JoinColumns({
		@JoinColumn(name="city_idcity", referencedColumnName="idcity", nullable=false),
		
		})
	private City city_idcity;
	@Column(name = "origin")
	private String origin;
	@Column(name = "notificationmsj")
	private int notificationMsj;
	
	private Date dateReg;
	
	@Column(name = "email_notification")

	private int emailNotification;


	public SysUser() {
	}

	public SysUser( String address, BigDecimal balance, Date birthDate, String countryIdcountry, String cu,
			String email, int idsysUserType, String lastName, String login, String name, String pass,
			String favoriteMsisdn ) {
		super();
		this.address = address;
		this.balance = balance;
		this.birthDate = birthDate;
		this.countryIdcountry = countryIdcountry;
		this.cu = cu;
		this.email = email;
		this.idsysUserType = idsysUserType;
		this.lastName = lastName;
		this.login = login;
		this.name = name;
		this.pass = pass;
		this.favoriteMsisdn = favoriteMsisdn;
	}

	public SysUser( String idsysUser, String address, BigDecimal balance, Date birthDate, String countryIdcountry,
			String cu, String email, int idsysUserType, String lastName, String login, String name, String pass,
			String favoriteMsisdn, City city, int emailNotification ) {
		super();
		
		
		this.idsysUser = idsysUser;
		this.address = address;
		this.balance = balance;
		this.birthDate = birthDate;
		this.countryIdcountry = countryIdcountry;
		this.cu = cu;
		this.email = email;
		this.idsysUserType = idsysUserType;
		this.lastName = lastName;
		this.login = login;
		this.name = name;
		this.pass = pass;
		this.favoriteMsisdn = favoriteMsisdn;
		this.city_idcity = city;
		this.emailNotification=emailNotification;
	}

	public String getIdsysUser()
	{
		return idsysUser;
	}

	public void setIdsysUser( String idsysUser )
	{
		this.idsysUser = idsysUser;
	}

	public String getAddress()
	{
		return this.address;
	}

	public void setAddress( String address )
	{
		this.address = address;
	}

	public BigDecimal getBalance()
	{
		return this.balance;
	}

	public void setBalance( BigDecimal balance )
	{
		this.balance = balance;
	}

	public Date getBirthDate()
	{
		return this.birthDate;
	}

	public void setBirthDate( Date birthDate )
	{
		this.birthDate = birthDate;
	}

	public String getCountryIdcountry()
	{
		return this.countryIdcountry;
	}

	public void setCountryIdcountry( String countryIdcountry )
	{
		this.countryIdcountry = countryIdcountry;
	}

	public String getCu()
	{
		return this.cu;
	}

	public void setCu( String cu )
	{
		this.cu = cu;
	}

	public String getEmail()
	{
		return this.email;
	}

	public void setEmail( String email )
	{
		this.email = email;
	}

	public int getIdsysUserType()
	{
		return this.idsysUserType;
	}

	public void setIdsysUserType( int idsysUserType )
	{
		this.idsysUserType = idsysUserType;
	}

	public String getLastName()
	{
		return this.lastName;
	}

	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}

	public String getLogin()
	{
		return this.login;
	}

	public void setLogin( String login )
	{
		this.login = login;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName( String name )
	{
		this.name = name;
	}

	public String getPass()
	{
		return this.pass;
	}

	public void setPass( String pass )
	{
		this.pass = pass;
	}

	public String getFavoriteMsisdn()
	{
		return favoriteMsisdn;
	}

	public void setFavoriteMsisdn( String favoriteMsisdn )
	{
		this.favoriteMsisdn = favoriteMsisdn;
	}

	public City getCity()
	{
		return city_idcity;
	}

	public void setCity( City city )
	{
		this.city_idcity = city;
	}

	public int getNotificationMsj()
	{
		return notificationMsj;
	}

	public void setNotificationMsj( int notificationMsj )
	{
		this.notificationMsj = notificationMsj;
	}

	public boolean isTempUser()
	{
		return idsysUserType == SysUserType.TEMP_USER;
	}

	public Date getDateReg()
	{
		return dateReg;
	}

	public void setDateReg( Date dateReg )
	{
		this.dateReg = dateReg;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public int getEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(int emailNotification) {
		this.emailNotification = emailNotification;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name+","+email+","+favoriteMsisdn;
	}


	
	
}