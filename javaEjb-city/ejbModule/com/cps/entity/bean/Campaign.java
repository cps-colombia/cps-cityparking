package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="campaign")
public class Campaign implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_campaign")
	int  idCampaign;
	@Column
	private String description; 
	@Column(name="start_date")
	private Date startDate ; 
	@Column(name="end_date")
	private Date endDate ; 
	@Column(name="range_amount_start")
	private BigDecimal rangeAmountStart ;
	@Column(name="range_amount_end")
	private BigDecimal rangeAmountEnd ;
	@Column(name="amount_percent")
	private BigDecimal amountPercent ;
	//@Column(name="campaign_state")
	@OneToOne 
    @JoinColumn(name="campaign_state")
	private CampaignState campaignState;
	//private  int campaignState;
	
	
	
	
	public Campaign() {
		
	}
	
	

	public Campaign(int idCampaign, String description, Date startDate,
			Date endDate, BigDecimal rangeAmountEnd,
			BigDecimal rangeAmountStart, BigDecimal amountPercent,
			CampaignState campaignState) {
		super();
		this.idCampaign = idCampaign;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.rangeAmountEnd = rangeAmountEnd;
		this.rangeAmountStart = rangeAmountStart;
		this.amountPercent = amountPercent;
		this.campaignState = campaignState;
	}



	public BigDecimal getRangeAmountEnd() {
		return rangeAmountEnd;
	}



	public void setRangeAmountEnd(BigDecimal rangeAmountEnd) {
		this.rangeAmountEnd = rangeAmountEnd;
	}



	public BigDecimal getRangeAmountStart() {
		return rangeAmountStart;
	}



	public void setRangeAmountStart(BigDecimal rangeAmountStart) {
		this.rangeAmountStart = rangeAmountStart;
	}



	public BigDecimal getAmountPercent() {
		return amountPercent;
	}



	public void setAmountPercent(BigDecimal amountPercent) {
		this.amountPercent = amountPercent;
	}



	public int getIdCampaign() {
		return idCampaign;
	}
	public void setIdCampaign(int idCampaign) {
		this.idCampaign = idCampaign;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public CampaignState getCampaignState() {
		return campaignState;
	}
	public void setCampaignState(CampaignState campaignState) {
		this.campaignState = campaignState;
	}
	
	

}
