package com.cps.entity.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


/**
 * The persistent class for the client database table.
 * 
 */
@Entity
@Table(name="client")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String idclient;

//	@OneToOne 
//    @JoinColumn(name="country_idcountry") 
//    private Country country;

	@OneToMany
	 @JoinTable(name="client_country", 
     joinColumns=@JoinColumn(name="idclient"),
     inverseJoinColumns=@JoinColumn(name="idcountry"))
	@Fetch(FetchMode.JOIN) 
	private Set<Country> clientCountries;
	
	@OneToMany (fetch = FetchType.EAGER)
    @JoinTable(name="client_city", 
    joinColumns=@JoinColumn(name="idclient"),
    		 inverseJoinColumns={@JoinColumn(name="idcity" , referencedColumnName="idcity" )})
	private Set<City> clientCities;

	private String name;

	/*@OneToOne
	@JoinColumn(name="city_idcity")
	private City city_idcity;
	*/
	
	private String status;
	
    public Client() {
    }

	public String getIdclient() {
		return this.idclient;
	}

	public void setIdclient(String idclient) {
		this.idclient = idclient;
	}

//	public Country getCountry() {
//		return this.country;
//	}
//
//	public void setCountryIdcountry(Country country) {
//		this.country = country;
//	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
//
//	public City getCity() {
//		return city_idcity;
//	}
//
//	public void setCity(City city) {
//		this.city_idcity = city;
//	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public Set<Country> getClientCountries() {
		return clientCountries;
	}

	public void setClientCountries(Set<Country>  clientCountries) {
		this.clientCountries = clientCountries;
	}

	public Set<City>  getClientCities() {
		return clientCities;
	}

	public void setClientCities(Set<City> clientCities) {
		this.clientCities = clientCities;
	}


}