package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the suggestion database table.
 * 
 */
@Entity
@Table(name="suggestion")
public class Suggestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String id;

    @Temporal( TemporalType.TIMESTAMP)
	private Date date;

	private String description;
	
	@Column(name="sys_user_id")
	private BigInteger sysUserId;

    public Suggestion() {
    }

	public Suggestion(String id, Date date, String description) {
		super();
		this.id = id;
		this.date = date;
		this.description = description;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigInteger getSysUserId() {
		return sysUserId;
	}

	public void setSysUserId(BigInteger sysUserId) {
		this.sysUserId = sysUserId;
	}

}