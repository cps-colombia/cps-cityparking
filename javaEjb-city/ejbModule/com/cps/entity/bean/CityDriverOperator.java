package com.cps.entity.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="citydriver_operator")
public class CityDriverOperator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name = "idcityOperator")
	private int idCityOperator;
	
	@OneToOne
    @JoinColumn(name="idcityStatus")
	@Fetch(FetchMode.JOIN) 
	private CityDriverStatus idCityStatus;

	@OneToOne
    @JoinColumn(name="idsys_user")
	@Fetch(FetchMode.JOIN) 
	private SysUser sysUser;

    @Column(name="idsys_user_creator")
	private BigInteger sysUserCreator;
	
    private Date dateReg = new Date();
	
    public CityDriverOperator() {
    }
    
	public CityDriverOperator (int idCityOperator, CityDriverStatus idCityStatus, SysUser sysUser) {
		super();
		this.idCityOperator = idCityOperator;
		this.idCityStatus = idCityStatus;
		this.sysUser = sysUser;
	}
	
	public int getIdCityOperator(){
		return idCityOperator;
	}
	public void setIdCityOperator(int idCityOperator){
		this.idCityOperator = idCityOperator;
	}
	
	public SysUser getSysUser() {
		return sysUser;
	}
	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}
	
	public CityDriverStatus getIdCityDriver(){
		return this.idCityStatus;
	}
	public void setIdCityStatus(CityDriverStatus idCityStatus){
		this.idCityStatus = idCityStatus;
	}
	
	public BigInteger getSysUserCreator(){
		return this.sysUserCreator;
	}
	public void setSysUserCreator(BigInteger sysUserCreator){
		this.sysUserCreator = sysUserCreator;
	}

	public Date getDateReg(){
		return dateReg;
	}
	public void setDateReg(Date dateReg ){
		this.dateReg = dateReg;
	}
	
}
