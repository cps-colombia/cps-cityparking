
package com.cps.wservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ValorWSResult" type="{http://tempuri.org/}Cvalorubicaws" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "valorWSResult"
})
@XmlRootElement(name = "ValorWSResponse")
public class ValorWSResponse {

    @XmlElement(name = "ValorWSResult")
    protected Cvalorubicaws valorWSResult;

    /**
     * Obtiene el valor de la propiedad valorWSResult.
     * 
     * @return
     *     possible object is
     *     {@link Cvalorubicaws }
     *     
     */
    public Cvalorubicaws getValorWSResult() {
        return valorWSResult;
    }

    /**
     * Define el valor de la propiedad valorWSResult.
     * 
     * @param value
     *     allowed object is
     *     {@link Cvalorubicaws }
     *     
     */
    public void setValorWSResult(Cvalorubicaws value) {
        this.valorWSResult = value;
    }

}
