
package com.cps.wservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cps.wservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Cvalorubicaws_QNAME = new QName("http://tempuri.org/", "Cvalorubicaws");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cps.wservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Cvalorubicaws }
     * 
     */
    public Cvalorubicaws createCvalorubicaws() {
        return new Cvalorubicaws();
    }

    /**
     * Create an instance of {@link ValorWSResponse }
     * 
     */
    public ValorWSResponse createValorWSResponse() {
        return new ValorWSResponse();
    }

    /**
     * Create an instance of {@link ValorWS }
     * 
     */
    public ValorWS createValorWS() {
        return new ValorWS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cvalorubicaws }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Cvalorubicaws")
    public JAXBElement<Cvalorubicaws> createCvalorubicaws(Cvalorubicaws value) {
        return new JAXBElement<Cvalorubicaws>(_Cvalorubicaws_QNAME, Cvalorubicaws.class, null, value);
    }

}
