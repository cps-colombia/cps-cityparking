
package com.cps.wservice.payment.verification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ds_verificar_pagoResult" type="{http://www.zonapagos.com/ws_verificar_pagos}Respuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dsVerificarPagoResult"
})
@XmlRootElement(name = "ds_verificar_pagoResponse")
public class DsVerificarPagoResponse {

    @XmlElement(name = "ds_verificar_pagoResult")
    protected Respuesta dsVerificarPagoResult;

    /**
     * Obtiene el valor de la propiedad dsVerificarPagoResult.
     * 
     * @return
     *     possible object is
     *     {@link Respuesta }
     *     
     */
    public Respuesta getDsVerificarPagoResult() {
        return dsVerificarPagoResult;
    }

    /**
     * Define el valor de la propiedad dsVerificarPagoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link Respuesta }
     *     
     */
    public void setDsVerificarPagoResult(Respuesta value) {
        this.dsVerificarPagoResult = value;
    }

}
