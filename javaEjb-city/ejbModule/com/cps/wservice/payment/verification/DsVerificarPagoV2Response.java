
package com.cps.wservice.payment.verification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ds_verificar_pago_v2Result" type="{http://www.zonapagos.com/ws_verificar_pagos}Respuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dsVerificarPagoV2Result"
})
@XmlRootElement(name = "ds_verificar_pago_v2Response")
public class DsVerificarPagoV2Response {

    @XmlElement(name = "ds_verificar_pago_v2Result")
    protected Respuesta dsVerificarPagoV2Result;

    /**
     * Obtiene el valor de la propiedad dsVerificarPagoV2Result.
     * 
     * @return
     *     possible object is
     *     {@link Respuesta }
     *     
     */
    public Respuesta getDsVerificarPagoV2Result() {
        return dsVerificarPagoV2Result;
    }

    /**
     * Define el valor de la propiedad dsVerificarPagoV2Result.
     * 
     * @param value
     *     allowed object is
     *     {@link Respuesta }
     *     
     */
    public void setDsVerificarPagoV2Result(Respuesta value) {
        this.dsVerificarPagoV2Result = value;
    }

}
