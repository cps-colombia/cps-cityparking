
package com.cps.wservice.payment.verification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfPagos_v3 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPagos_v3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pagos_v3" type="{http://www.zonapagos.com/ws_verificar_pagos}pagos_v3" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPagos_v3", propOrder = {
    "pagosV3"
})
public class ArrayOfPagosV3 implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement(name = "pagos_v3", nillable = true)
    protected List<PagosV3> pagosV3;

    /**
     * Gets the value of the pagosV3 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pagosV3 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPagosV3().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PagosV3 }
     * 
     * 
     */
    public List<PagosV3> getPagosV3() {
        if (pagosV3 == null) {
            pagosV3 = new ArrayList<PagosV3>();
        }
        return this.pagosV3;
    }

}
