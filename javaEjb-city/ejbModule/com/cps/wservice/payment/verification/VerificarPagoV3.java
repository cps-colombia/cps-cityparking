
package com.cps.wservice.payment.verification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="str_id_pago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="int_id_tienda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="str_id_clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="res_pagos_v3" type="{http://www.zonapagos.com/ws_verificar_pagos}ArrayOfPagos_v3" minOccurs="0"/>
 *         &lt;element name="int_error" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="str_error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "strIdPago",
    "intIdTienda",
    "strIdClave",
    "resPagosV3",
    "intError",
    "strError"
})
@XmlRootElement(name = "verificar_pago_v3")
public class VerificarPagoV3 {

    @XmlElement(name = "str_id_pago")
    protected String strIdPago;
    @XmlElement(name = "int_id_tienda")
    protected int intIdTienda;
    @XmlElement(name = "str_id_clave")
    protected String strIdClave;
    @XmlElement(name = "res_pagos_v3")
    protected ArrayOfPagosV3 resPagosV3;
    @XmlElement(name = "int_error")
    protected int intError;
    @XmlElement(name = "str_error")
    protected String strError;

    /**
     * Obtiene el valor de la propiedad strIdPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrIdPago() {
        return strIdPago;
    }

    /**
     * Define el valor de la propiedad strIdPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrIdPago(String value) {
        this.strIdPago = value;
    }

    /**
     * Obtiene el valor de la propiedad intIdTienda.
     * 
     */
    public int getIntIdTienda() {
        return intIdTienda;
    }

    /**
     * Define el valor de la propiedad intIdTienda.
     * 
     */
    public void setIntIdTienda(int value) {
        this.intIdTienda = value;
    }

    /**
     * Obtiene el valor de la propiedad strIdClave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrIdClave() {
        return strIdClave;
    }

    /**
     * Define el valor de la propiedad strIdClave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrIdClave(String value) {
        this.strIdClave = value;
    }

    /**
     * Obtiene el valor de la propiedad resPagosV3.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPagosV3 }
     *     
     */
    public ArrayOfPagosV3 getResPagosV3() {
        return resPagosV3;
    }

    /**
     * Define el valor de la propiedad resPagosV3.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPagosV3 }
     *     
     */
    public void setResPagosV3(ArrayOfPagosV3 value) {
        this.resPagosV3 = value;
    }

    /**
     * Obtiene el valor de la propiedad intError.
     * 
     */
    public int getIntError() {
        return intError;
    }

    /**
     * Define el valor de la propiedad intError.
     * 
     */
    public void setIntError(int value) {
        this.intError = value;
    }

    /**
     * Obtiene el valor de la propiedad strError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrError() {
        return strError;
    }

    /**
     * Define el valor de la propiedad strError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrError(String value) {
        this.strError = value;
    }

}
