
package com.cps.wservice.payment.verification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="verificar_pago_v3Result" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="res_pagos_v3" type="{http://www.zonapagos.com/ws_verificar_pagos}ArrayOfPagos_v3" minOccurs="0"/>
 *         &lt;element name="int_error" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="str_error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "verificarPagoV3Result",
    "resPagosV3",
    "intError",
    "strError"
})
@XmlRootElement(name = "verificar_pago_v3Response")
public class VerificarPagoV3Response {

    @XmlElement(name = "verificar_pago_v3Result")
    protected int verificarPagoV3Result;
    @XmlElement(name = "res_pagos_v3")
    protected ArrayOfPagosV3 resPagosV3;
    @XmlElement(name = "int_error")
    protected int intError;
    @XmlElement(name = "str_error")
    protected String strError;

    /**
     * Obtiene el valor de la propiedad verificarPagoV3Result.
     * 
     */
    public int getVerificarPagoV3Result() {
        return verificarPagoV3Result;
    }

    /**
     * Define el valor de la propiedad verificarPagoV3Result.
     * 
     */
    public void setVerificarPagoV3Result(int value) {
        this.verificarPagoV3Result = value;
    }

    /**
     * Obtiene el valor de la propiedad resPagosV3.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPagosV3 }
     *     
     */
    public ArrayOfPagosV3 getResPagosV3() {
        return resPagosV3;
    }

    /**
     * Define el valor de la propiedad resPagosV3.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPagosV3 }
     *     
     */
    public void setResPagosV3(ArrayOfPagosV3 value) {
        this.resPagosV3 = value;
    }

    /**
     * Obtiene el valor de la propiedad intError.
     * 
     */
    public int getIntError() {
        return intError;
    }

    /**
     * Define el valor de la propiedad intError.
     * 
     */
    public void setIntError(int value) {
        this.intError = value;
    }

    /**
     * Obtiene el valor de la propiedad strError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrError() {
        return strError;
    }

    /**
     * Define el valor de la propiedad strError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrError(String value) {
        this.strError = value;
    }

}
