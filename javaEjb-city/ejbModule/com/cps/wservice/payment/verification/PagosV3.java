
package com.cps.wservice.payment.verification;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para pagos_v3 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="pagos_v3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="str_id_pago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="int_estado_pago" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="int_id_forma_pago" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dbl_valor_pagado" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="str_ticketID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="str_id_clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="str_id_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="str_franquicia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="int_cod_aprobacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="int_codigo_servico" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="int_codigo_banco" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="str_nombre_banco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="str_codigo_transaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="int_ciclo_transaccion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="str_campo1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="str_campo2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="str_campo3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dat_fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pagos_v3", propOrder = {
    "strIdPago",
    "intEstadoPago",
    "intIdFormaPago",
    "dblValorPagado",
    "strTicketID",
    "strIdClave",
    "strIdCliente",
    "strFranquicia",
    "intCodAprobacion",
    "intCodigoServico",
    "intCodigoBanco",
    "strNombreBanco",
    "strCodigoTransaccion",
    "intCicloTransaccion",
    "strCampo1",
    "strCampo2",
    "strCampo3",
    "datFecha"
})
public class PagosV3 implements Serializable  {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement(name = "str_id_pago")
    protected String strIdPago;
    @XmlElement(name = "int_estado_pago")
    protected int intEstadoPago;
    @XmlElement(name = "int_id_forma_pago")
    protected int intIdFormaPago;
    @XmlElement(name = "dbl_valor_pagado")
    protected double dblValorPagado;
    @XmlElement(name = "str_ticketID")
    protected String strTicketID;
    @XmlElement(name = "str_id_clave")
    protected String strIdClave;
    @XmlElement(name = "str_id_cliente")
    protected String strIdCliente;
    @XmlElement(name = "str_franquicia")
    protected String strFranquicia;
    @XmlElement(name = "int_cod_aprobacion")
    protected int intCodAprobacion;
    @XmlElement(name = "int_codigo_servico")
    protected int intCodigoServico;
    @XmlElement(name = "int_codigo_banco")
    protected int intCodigoBanco;
    @XmlElement(name = "str_nombre_banco")
    protected String strNombreBanco;
    @XmlElement(name = "str_codigo_transaccion")
    protected String strCodigoTransaccion;
    @XmlElement(name = "int_ciclo_transaccion")
    protected int intCicloTransaccion;
    @XmlElement(name = "str_campo1")
    protected String strCampo1;
    @XmlElement(name = "str_campo2")
    protected String strCampo2;
    @XmlElement(name = "str_campo3")
    protected String strCampo3;
    @XmlElement(name = "dat_fecha", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datFecha;

    /**
     * Obtiene el valor de la propiedad strIdPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrIdPago() {
        return strIdPago;
    }

    /**
     * Define el valor de la propiedad strIdPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrIdPago(String value) {
        this.strIdPago = value;
    }

    /**
     * Obtiene el valor de la propiedad intEstadoPago.
     * 
     */
    public int getIntEstadoPago() {
        return intEstadoPago;
    }

    /**
     * Define el valor de la propiedad intEstadoPago.
     * 
     */
    public void setIntEstadoPago(int value) {
        this.intEstadoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad intIdFormaPago.
     * 
     */
    public int getIntIdFormaPago() {
        return intIdFormaPago;
    }

    /**
     * Define el valor de la propiedad intIdFormaPago.
     * 
     */
    public void setIntIdFormaPago(int value) {
        this.intIdFormaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad dblValorPagado.
     * 
     */
    public double getDblValorPagado() {
        return dblValorPagado;
    }

    /**
     * Define el valor de la propiedad dblValorPagado.
     * 
     */
    public void setDblValorPagado(double value) {
        this.dblValorPagado = value;
    }

    /**
     * Obtiene el valor de la propiedad strTicketID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrTicketID() {
        return strTicketID;
    }

    /**
     * Define el valor de la propiedad strTicketID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrTicketID(String value) {
        this.strTicketID = value;
    }

    /**
     * Obtiene el valor de la propiedad strIdClave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrIdClave() {
        return strIdClave;
    }

    /**
     * Define el valor de la propiedad strIdClave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrIdClave(String value) {
        this.strIdClave = value;
    }

    /**
     * Obtiene el valor de la propiedad strIdCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrIdCliente() {
        return strIdCliente;
    }

    /**
     * Define el valor de la propiedad strIdCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrIdCliente(String value) {
        this.strIdCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad strFranquicia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrFranquicia() {
        return strFranquicia;
    }

    /**
     * Define el valor de la propiedad strFranquicia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrFranquicia(String value) {
        this.strFranquicia = value;
    }

    /**
     * Obtiene el valor de la propiedad intCodAprobacion.
     * 
     */
    public int getIntCodAprobacion() {
        return intCodAprobacion;
    }

    /**
     * Define el valor de la propiedad intCodAprobacion.
     * 
     */
    public void setIntCodAprobacion(int value) {
        this.intCodAprobacion = value;
    }

    /**
     * Obtiene el valor de la propiedad intCodigoServico.
     * 
     */
    public int getIntCodigoServico() {
        return intCodigoServico;
    }

    /**
     * Define el valor de la propiedad intCodigoServico.
     * 
     */
    public void setIntCodigoServico(int value) {
        this.intCodigoServico = value;
    }

    /**
     * Obtiene el valor de la propiedad intCodigoBanco.
     * 
     */
    public int getIntCodigoBanco() {
        return intCodigoBanco;
    }

    /**
     * Define el valor de la propiedad intCodigoBanco.
     * 
     */
    public void setIntCodigoBanco(int value) {
        this.intCodigoBanco = value;
    }

    /**
     * Obtiene el valor de la propiedad strNombreBanco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrNombreBanco() {
        return strNombreBanco;
    }

    /**
     * Define el valor de la propiedad strNombreBanco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrNombreBanco(String value) {
        this.strNombreBanco = value;
    }

    /**
     * Obtiene el valor de la propiedad strCodigoTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrCodigoTransaccion() {
        return strCodigoTransaccion;
    }

    /**
     * Define el valor de la propiedad strCodigoTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrCodigoTransaccion(String value) {
        this.strCodigoTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad intCicloTransaccion.
     * 
     */
    public int getIntCicloTransaccion() {
        return intCicloTransaccion;
    }

    /**
     * Define el valor de la propiedad intCicloTransaccion.
     * 
     */
    public void setIntCicloTransaccion(int value) {
        this.intCicloTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad strCampo1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrCampo1() {
        return strCampo1;
    }

    /**
     * Define el valor de la propiedad strCampo1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrCampo1(String value) {
        this.strCampo1 = value;
    }

    /**
     * Obtiene el valor de la propiedad strCampo2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrCampo2() {
        return strCampo2;
    }

    /**
     * Define el valor de la propiedad strCampo2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrCampo2(String value) {
        this.strCampo2 = value;
    }

    /**
     * Obtiene el valor de la propiedad strCampo3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrCampo3() {
        return strCampo3;
    }

    /**
     * Define el valor de la propiedad strCampo3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrCampo3(String value) {
        this.strCampo3 = value;
    }

    /**
     * Obtiene el valor de la propiedad datFecha.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatFecha() {
        return datFecha;
    }

    /**
     * Define el valor de la propiedad datFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatFecha(XMLGregorianCalendar value) {
        this.datFecha = value;
    }

}
