
package com.cps.wservice.payment.verification;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cps.wservice.payment.verification package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cps.wservice.payment.verification
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link VerificarPagoV3 }
     * 
     */
    public VerificarPagoV3 createVerificarPagoV3() {
        return new VerificarPagoV3();
    }

    /**
     * Create an instance of {@link ArrayOfPagosV3 }
     * 
     */
    public ArrayOfPagosV3 createArrayOfPagosV3() {
        return new ArrayOfPagosV3();
    }

    /**
     * Create an instance of {@link DsVerificarPagoV2Response }
     * 
     */
    public DsVerificarPagoV2Response createDsVerificarPagoV2Response() {
        return new DsVerificarPagoV2Response();
    }

    /**
     * Create an instance of {@link DsVerificarPagoV2 }
     * 
     */
    public DsVerificarPagoV2 createDsVerificarPagoV2() {
        return new DsVerificarPagoV2();
    }

    /**
     * Create an instance of {@link com.cps.wservice.payment.verification.DsVerificarPago }
     * 
     */
    public com.cps.wservice.payment.verification.DsVerificarPago createDsVerificarPago() {
        return new com.cps.wservice.payment.verification.DsVerificarPago();
    }

    /**
     * Create an instance of {@link DsVerificarPagoResponse }
     * 
     */
    public DsVerificarPagoResponse createDsVerificarPagoResponse() {
        return new DsVerificarPagoResponse();
    }

    /**
     * Create an instance of {@link VerificarPagoV3Response }
     * 
     */
    public VerificarPagoV3Response createVerificarPagoV3Response() {
        return new VerificarPagoV3Response();
    }

    /**
     * Create an instance of {@link PagosV3 }
     * 
     */
    public PagosV3 createPagosV3() {
        return new PagosV3();
    }

    /**
     * Create an instance of {@link Respuesta.DsVerificarPago }
     * 
     */
    public Respuesta.DsVerificarPago createRespuestaDsVerificarPago() {
        return new Respuesta.DsVerificarPago();
    }

}
