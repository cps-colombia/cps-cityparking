
package com.cps.wservice.payment.verification;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_pago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id_tienda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idPago",
    "idTienda",
    "idClave"
})
@XmlRootElement(name = "ds_verificar_pago_v2")
public class DsVerificarPagoV2 {

    @XmlElement(name = "id_pago")
    protected String idPago;
    @XmlElement(name = "id_tienda")
    protected int idTienda;
    @XmlElement(name = "id_clave")
    protected String idClave;

    /**
     * Obtiene el valor de la propiedad idPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPago() {
        return idPago;
    }

    /**
     * Define el valor de la propiedad idPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPago(String value) {
        this.idPago = value;
    }

    /**
     * Obtiene el valor de la propiedad idTienda.
     * 
     */
    public int getIdTienda() {
        return idTienda;
    }

    /**
     * Define el valor de la propiedad idTienda.
     * 
     */
    public void setIdTienda(int value) {
        this.idTienda = value;
    }

    /**
     * Obtiene el valor de la propiedad idClave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdClave() {
        return idClave;
    }

    /**
     * Define el valor de la propiedad idClave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdClave(String value) {
        this.idClave = value;
    }

}
