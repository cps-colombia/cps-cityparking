
package com.cps.wservice.payment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_tienda" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="total_con_iva" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="valor_iva" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="id_pago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcion_pago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apellido_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono_cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info_opcional1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info_opcional2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info_opcional3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigo_servicio_principal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lista_codigos_servicio_multicredito" type="{http://www.zonapagos.com}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="lista_nit_codigos_servicio_multicredito" type="{http://www.zonapagos.com}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="lista_valores_con_iva" type="{http://www.zonapagos.com}ArrayOfDouble" minOccurs="0"/>
 *         &lt;element name="lista_valores_iva" type="{http://www.zonapagos.com}ArrayOfDouble" minOccurs="0"/>
 *         &lt;element name="total_codigos_servicio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idTienda",
    "clave",
    "totalConIva",
    "valorIva",
    "idPago",
    "descripcionPago",
    "email",
    "idCliente",
    "tipoId",
    "nombreCliente",
    "apellidoCliente",
    "telefonoCliente",
    "infoOpcional1",
    "infoOpcional2",
    "infoOpcional3",
    "codigoServicioPrincipal",
    "listaCodigosServicioMulticredito",
    "listaNitCodigosServicioMulticredito",
    "listaValoresConIva",
    "listaValoresIva",
    "totalCodigosServicio"
})
@XmlRootElement(name = "inicio_pagoV2")
public class InicioPagoV2 {

    @XmlElement(name = "id_tienda")
    protected int idTienda;
    protected String clave;
    @XmlElement(name = "total_con_iva")
    protected double totalConIva;
    @XmlElement(name = "valor_iva")
    protected double valorIva;
    @XmlElement(name = "id_pago")
    protected String idPago;
    @XmlElement(name = "descripcion_pago")
    protected String descripcionPago;
    protected String email;
    @XmlElement(name = "id_cliente")
    protected String idCliente;
    @XmlElement(name = "tipo_id")
    protected String tipoId;
    @XmlElement(name = "nombre_cliente")
    protected String nombreCliente;
    @XmlElement(name = "apellido_cliente")
    protected String apellidoCliente;
    @XmlElement(name = "telefono_cliente")
    protected String telefonoCliente;
    @XmlElement(name = "info_opcional1")
    protected String infoOpcional1;
    @XmlElement(name = "info_opcional2")
    protected String infoOpcional2;
    @XmlElement(name = "info_opcional3")
    protected String infoOpcional3;
    @XmlElement(name = "codigo_servicio_principal")
    protected String codigoServicioPrincipal;
    @XmlElement(name = "lista_codigos_servicio_multicredito")
    protected ArrayOfString listaCodigosServicioMulticredito;
    @XmlElement(name = "lista_nit_codigos_servicio_multicredito")
    protected ArrayOfString listaNitCodigosServicioMulticredito;
    @XmlElement(name = "lista_valores_con_iva")
    protected ArrayOfDouble listaValoresConIva;
    @XmlElement(name = "lista_valores_iva")
    protected ArrayOfDouble listaValoresIva;
    @XmlElement(name = "total_codigos_servicio")
    protected int totalCodigosServicio;

    /**
     * Obtiene el valor de la propiedad idTienda.
     * 
     */
    public int getIdTienda() {
        return idTienda;
    }

    /**
     * Define el valor de la propiedad idTienda.
     * 
     */
    public void setIdTienda(int value) {
        this.idTienda = value;
    }

    /**
     * Obtiene el valor de la propiedad clave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClave() {
        return clave;
    }

    /**
     * Define el valor de la propiedad clave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClave(String value) {
        this.clave = value;
    }

    /**
     * Obtiene el valor de la propiedad totalConIva.
     * 
     */
    public double getTotalConIva() {
        return totalConIva;
    }

    /**
     * Define el valor de la propiedad totalConIva.
     * 
     */
    public void setTotalConIva(double value) {
        this.totalConIva = value;
    }

    /**
     * Obtiene el valor de la propiedad valorIva.
     * 
     */
    public double getValorIva() {
        return valorIva;
    }

    /**
     * Define el valor de la propiedad valorIva.
     * 
     */
    public void setValorIva(double value) {
        this.valorIva = value;
    }

    /**
     * Obtiene el valor de la propiedad idPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPago() {
        return idPago;
    }

    /**
     * Define el valor de la propiedad idPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPago(String value) {
        this.idPago = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionPago() {
        return descripcionPago;
    }

    /**
     * Define el valor de la propiedad descripcionPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionPago(String value) {
        this.descripcionPago = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad idCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * Define el valor de la propiedad idCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCliente(String value) {
        this.idCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoId() {
        return tipoId;
    }

    /**
     * Define el valor de la propiedad tipoId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoId(String value) {
        this.tipoId = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Define el valor de la propiedad nombreCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCliente(String value) {
        this.nombreCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoCliente() {
        return apellidoCliente;
    }

    /**
     * Define el valor de la propiedad apellidoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoCliente(String value) {
        this.apellidoCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad telefonoCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    /**
     * Define el valor de la propiedad telefonoCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefonoCliente(String value) {
        this.telefonoCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad infoOpcional1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoOpcional1() {
        return infoOpcional1;
    }

    /**
     * Define el valor de la propiedad infoOpcional1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoOpcional1(String value) {
        this.infoOpcional1 = value;
    }

    /**
     * Obtiene el valor de la propiedad infoOpcional2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoOpcional2() {
        return infoOpcional2;
    }

    /**
     * Define el valor de la propiedad infoOpcional2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoOpcional2(String value) {
        this.infoOpcional2 = value;
    }

    /**
     * Obtiene el valor de la propiedad infoOpcional3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoOpcional3() {
        return infoOpcional3;
    }

    /**
     * Define el valor de la propiedad infoOpcional3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoOpcional3(String value) {
        this.infoOpcional3 = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoServicioPrincipal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoServicioPrincipal() {
        return codigoServicioPrincipal;
    }

    /**
     * Define el valor de la propiedad codigoServicioPrincipal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoServicioPrincipal(String value) {
        this.codigoServicioPrincipal = value;
    }

    /**
     * Obtiene el valor de la propiedad listaCodigosServicioMulticredito.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getListaCodigosServicioMulticredito() {
        return listaCodigosServicioMulticredito;
    }

    /**
     * Define el valor de la propiedad listaCodigosServicioMulticredito.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setListaCodigosServicioMulticredito(ArrayOfString value) {
        this.listaCodigosServicioMulticredito = value;
    }

    /**
     * Obtiene el valor de la propiedad listaNitCodigosServicioMulticredito.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getListaNitCodigosServicioMulticredito() {
        return listaNitCodigosServicioMulticredito;
    }

    /**
     * Define el valor de la propiedad listaNitCodigosServicioMulticredito.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setListaNitCodigosServicioMulticredito(ArrayOfString value) {
        this.listaNitCodigosServicioMulticredito = value;
    }

    /**
     * Obtiene el valor de la propiedad listaValoresConIva.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDouble }
     *     
     */
    public ArrayOfDouble getListaValoresConIva() {
        return listaValoresConIva;
    }

    /**
     * Define el valor de la propiedad listaValoresConIva.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDouble }
     *     
     */
    public void setListaValoresConIva(ArrayOfDouble value) {
        this.listaValoresConIva = value;
    }

    /**
     * Obtiene el valor de la propiedad listaValoresIva.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDouble }
     *     
     */
    public ArrayOfDouble getListaValoresIva() {
        return listaValoresIva;
    }

    /**
     * Define el valor de la propiedad listaValoresIva.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDouble }
     *     
     */
    public void setListaValoresIva(ArrayOfDouble value) {
        this.listaValoresIva = value;
    }

    /**
     * Obtiene el valor de la propiedad totalCodigosServicio.
     * 
     */
    public int getTotalCodigosServicio() {
        return totalCodigosServicio;
    }

    /**
     * Define el valor de la propiedad totalCodigosServicio.
     * 
     */
    public void setTotalCodigosServicio(int value) {
        this.totalCodigosServicio = value;
    }

}
