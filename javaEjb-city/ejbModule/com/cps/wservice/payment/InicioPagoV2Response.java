
package com.cps.wservice.payment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inicio_pagoV2Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inicioPagoV2Result"
})
@XmlRootElement(name = "inicio_pagoV2Response")
public class InicioPagoV2Response {

    @XmlElement(name = "inicio_pagoV2Result")
    protected String inicioPagoV2Result;

    /**
     * Obtiene el valor de la propiedad inicioPagoV2Result.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioPagoV2Result() {
        return inicioPagoV2Result;
    }

    /**
     * Define el valor de la propiedad inicioPagoV2Result.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioPagoV2Result(String value) {
        this.inicioPagoV2Result = value;
    }

}
