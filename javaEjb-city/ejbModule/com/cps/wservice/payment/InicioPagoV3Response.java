
package com.cps.wservice.payment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inicio_pagoV3Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inicioPagoV3Result"
})
@XmlRootElement(name = "inicio_pagoV3Response")
public class InicioPagoV3Response {

    @XmlElement(name = "inicio_pagoV3Result")
    protected String inicioPagoV3Result;

    /**
     * Obtiene el valor de la propiedad inicioPagoV3Result.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioPagoV3Result() {
        return inicioPagoV3Result;
    }

    /**
     * Define el valor de la propiedad inicioPagoV3Result.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioPagoV3Result(String value) {
        this.inicioPagoV3Result = value;
    }

}
