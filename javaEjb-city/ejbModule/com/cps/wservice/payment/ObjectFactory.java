
package com.cps.wservice.payment;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cps.wservice.payment package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cps.wservice.payment
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InicioPagoV3Response }
     * 
     */
    public InicioPagoV3Response createInicioPagoV3Response() {
        return new InicioPagoV3Response();
    }

    /**
     * Create an instance of {@link InicioPagoV3 }
     * 
     */
    public InicioPagoV3 createInicioPagoV3() {
        return new InicioPagoV3();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link ArrayOfDouble }
     * 
     */
    public ArrayOfDouble createArrayOfDouble() {
        return new ArrayOfDouble();
    }

    /**
     * Create an instance of {@link InicioPagoV4 }
     * 
     */
    public InicioPagoV4 createInicioPagoV4() {
        return new InicioPagoV4();
    }

    /**
     * Create an instance of {@link InicioPagoV4Response }
     * 
     */
    public InicioPagoV4Response createInicioPagoV4Response() {
        return new InicioPagoV4Response();
    }

    /**
     * Create an instance of {@link InicioPagoV2Response }
     * 
     */
    public InicioPagoV2Response createInicioPagoV2Response() {
        return new InicioPagoV2Response();
    }

    /**
     * Create an instance of {@link InicioPagoV2 }
     * 
     */
    public InicioPagoV2 createInicioPagoV2() {
        return new InicioPagoV2();
    }

}
