
package com.cps.wservice.payment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inicio_pagoV4Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lista_respuesta" type="{http://www.zonapagos.com}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inicioPagoV4Result",
    "listaRespuesta"
})
@XmlRootElement(name = "inicio_pagoV4Response")
public class InicioPagoV4Response {

    @XmlElement(name = "inicio_pagoV4Result")
    protected String inicioPagoV4Result;
    @XmlElement(name = "lista_respuesta")
    protected ArrayOfString listaRespuesta;

    /**
     * Obtiene el valor de la propiedad inicioPagoV4Result.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioPagoV4Result() {
        return inicioPagoV4Result;
    }

    /**
     * Define el valor de la propiedad inicioPagoV4Result.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioPagoV4Result(String value) {
        this.inicioPagoV4Result = value;
    }

    /**
     * Obtiene el valor de la propiedad listaRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getListaRespuesta() {
        return listaRespuesta;
    }

    /**
     * Define el valor de la propiedad listaRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setListaRespuesta(ArrayOfString value) {
        this.listaRespuesta = value;
    }

}
