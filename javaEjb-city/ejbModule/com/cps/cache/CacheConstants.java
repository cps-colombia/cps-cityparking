package com.cps.cache;

public interface CacheConstants {

	boolean ENABLE_CACHE = true;
	
	final String COUNTRYS = "countrys";
	
	final String USER_NOTIFICATIONS = "usn";
	
	final String AGENT_NOTIFICATIONS = "agn";
	
	final String ACTIVE_PARKING = "activeParking";
	
	final String USERS = "users";
	
	final int MAX_MINS_IN_CACHE = 90;
	
}
