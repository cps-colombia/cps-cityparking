package com.cps.cache;

//import com.opensymphony.oscache.general.GeneralCacheAdministrator;
//import com.opensymphony.oscache.web.filter.ExpiresRefreshPolicy;

public class CacheManager {
	
	private static CacheManager cacheManager;

	 //DummyClass
	public class GeneralCacheAdministrator 
	{

		public Object getFromCache( String key )
		{
			// TODO Auto-generated method stub
			return null;
		}

		public void putInCache( String key, Object obj )
		{
			// TODO Auto-generated method stub
			
		}

		public void removeEntry( String key )
		{
			// TODO Auto-generated method stub
			
		}

		public void cancelUpdate( String key )
		{
			// TODO Auto-generated method stub
			
		}

		public void flushAll()
		{
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private static GeneralCacheAdministrator cache;
	
	private CacheManager(){
		cache = new GeneralCacheAdministrator();
	}
	
	public static CacheManager getInstance() {
		if( cacheManager == null ){
			cacheManager = new CacheManager();
		}
		return cacheManager;
	}
	
	public Object get(String key)throws Exception{
		return cache.getFromCache(key);
	}

	public void put(String key, Object obj)throws Exception{
		cache.putInCache(key, obj);
	}
	
	public void put(String key, Object obj, int refreshMins)throws Exception{		
		//cache.putInCache(key, obj, new ExpiresRefreshPolicy(refreshMins*60));
	}
	
	public void remove(String key)throws Exception{		
		cache.removeEntry(key);
	}
	
	public void cancelUpdate(String key){
		cache.cancelUpdate(key);
	}
	
	public void flushAll(){
		cache.flushAll();
	}
	
}
