package com.cps.session;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import javax.xml.ws.Service;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import com.cps.business.ZoneBusiness;
import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.PayItem;
import com.cps.entity.bean.PaymentZone;
import com.cps.entity.bean.Zone;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.service.Cvalorws;
import com.cps.service.WSpagoCPSWS;
import com.cps.service.WSpagoCPSWSSoap;
import com.cps.service.WSvalidadato;
import com.cps.service.WSvalidadatoSoap;
import com.cps.util.TaskUtil;
import com.cps.wservice.Cvalorubicaws;
import com.cps.wservice.WsConsultapuntopark;
import com.cps.wservice.WsConsultapuntoparkSoap;
import com.cps.wservice.payment.ZPagos;
import com.cps.wservice.payment.ZPagosSoap;
import com.cps.wservice.payment.verification.ArrayOfPagosV3;
import com.cps.wservice.payment.verification.ServiceSoap;
//import org.apache.log4j.Logger;
//import org.jboss.beans.metadata.api.annotations.Inject;
//import org.jboss.ejb3.annotation.Management;
//import org.jboss.ejb3.annotation.Service;

@Singleton(name = "ParkingWSManagerService")
@Startup
public class ParkingWSManager implements BeanLauncherManagement {
	private static final org.jboss.logging.Logger LOGGER = org.jboss.logging.Logger
			.getLogger(ParkingWSManager.class);

	private static final org.jboss.logging.Logger LOGGERPAY = org.jboss.logging.Logger
			.getLogger("com.cps.log.paylog");

	public static ParkingWSManager instance;

	private static boolean debug = true;
	@PersistenceContext
	EntityManager em;
	@EJB
	private CityParkingBeanRemote cityParkingBean;

	@EJB
	private ZoneBeanRemote zoneBean;

	private HashMap<String, Cvalorubicaws> parkingCuotaCache = new HashMap<String, Cvalorubicaws>();
	private ArrayList<String> cvalorQueue = new ArrayList<String>();

	private static final QName SERVICE_CONSULTA_PUNTO_NAME = new QName(
			"http://tempuri.org/", "wsConsultapuntopark");

	private static final QName SERVICE_PAGO_NAME = new QName(
			"http://tempuri.org/", "WSpagoCPSWS");
	private static final QName SERVICE_VALIDA_NAME = new QName(
			"http://tempuri.org/", "WSvalidadato");

	private static final QName SERVICE_ZONE_NAME = new QName(
			"http://www.zonapagos.com", "ZPagos");

	private static final QName SERVICE_CHECKPAYMENT_NAME = new QName(
			"http://www.zonapagos.com/ws_verificar_pagos", "Service");
	private Map<String, ParkingItem> parkingItems = new HashMap<String, ParkingWSManager.ParkingItem>();

	private ZPagos ssZone;
	private ZPagosSoap zonePort;
	private com.cps.wservice.payment.verification.Service ssVerify;
	private ServiceSoap verifyPort;

	// Map<String,String>
	// Class to keep cityparking web services.
	private static class ParkingItem {
		private String url;
		WSpagoCPSWS ssPago;
		private WSpagoCPSWSSoap wsPago;
		WSvalidadato ssValidaDato;
		private WSvalidadatoSoap wsValida;

		private WsConsultapuntopark ss = null;
		private WsConsultapuntoparkSoap wsPuntoParq = null;

		public ParkingItem(Zone zone) throws Exception {
			this.url = zone.getUrl();
			// try {
			URL wsdlURL = new URL(url + "/WSpagoCPS.asmx?WSDL");
			// wsdlURL.openConnection().connect();
			ssPago = new WSpagoCPSWS(wsdlURL, SERVICE_PAGO_NAME);
			setWsPago(ssPago.getWSpagoCPSWSSoap());

			wsdlURL = new URL(url + "/WSvalidadato.asmx?WSDL");
			// wsdlURL.openConnection().connect();
			ssValidaDato = new WSvalidadato(wsdlURL, SERVICE_VALIDA_NAME);
			setWsValida(ssValidaDato.getWSvalidadatoSoap());
			wsdlURL = new URL(url + "/wsConsultapuntopark.asmx?WSDL");
			// wsdlURL.openConnection().connect();
			ss = new WsConsultapuntopark(wsdlURL, SERVICE_CONSULTA_PUNTO_NAME);
			setWsPuntoParq(ss.getWsConsultapuntoparkSoap());

			/*
			 * } catch (Exception e) { e.printStackTrace(); }
			 */

		}

		public ParkingItem(String url) throws Exception {

			// try {
			URL wsdlURL = new URL(url + "/WSpagoCPS.asmx?WSDL");
			HttpURLConnection connection = null;

			try {
				connection = (HttpURLConnection) wsdlURL.openConnection();
				connection.setReadTimeout(5000);
				connection.setConnectTimeout(5000);
				connection.connect();
				
			} catch (Exception e) {
				LOGGER.error("server 5 seconds connection exception ");
			
				throw e;
			}finally
			{
				if(connection!= null)
					connection.disconnect();
			}
			// wsdlURL.openConnection().connect();
			ssPago = new WSpagoCPSWS(wsdlURL, SERVICE_PAGO_NAME);
			WSpagoCPSWSSoap sp = ssPago.getWSpagoCPSWSSoap();
			setWsPago(sp);
			// createservice(ssPago);
			//configureService(sp);

			wsdlURL = new URL(url + "/WSvalidadato.asmx?WSDL");
			// wsdlURL.openConnection().connect();
			ssValidaDato = new WSvalidadato(wsdlURL, SERVICE_VALIDA_NAME);
			WSvalidadatoSoap vd = ssValidaDato.getWSvalidadatoSoap();
			setWsValida(vd);
			//configureService(vd);

			wsdlURL = new URL(url + "/wsConsultapuntopark.asmx?WSDL");
			// wsdlURL.openConnection().connect();
			ss = new WsConsultapuntopark(wsdlURL, SERVICE_CONSULTA_PUNTO_NAME);
			WsConsultapuntoparkSoap pps = ss.getWsConsultapuntoparkSoap();
			setWsPuntoParq(pps);
			//configureService(pps);

			/*
			 * } catch (Exception e) { e.printStackTrace(); }
			 */

		}

		private void configureService(Object ss) throws Exception {
			// Service ss = new Service(url, name);//new
			// WsConsultapuntopark(wsdlURL, SERVICE_CONSULTA_PUNTO_NAME);
			Client client = ClientProxy.getClient(ss);

			HTTPConduit httpConduit = (HTTPConduit) client.getConduit();

			HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
			httpClientPolicy.setConnectionTimeout(5);
			httpClientPolicy.setReceiveTimeout(5000);

			httpConduit.setClient(httpClientPolicy);
			// return ss;
		}

		public String getUrl() {
			return url;
		}

		public WSpagoCPSWSSoap getWsPago() {
			return wsPago;
		}

		public void setWsPago(WSpagoCPSWSSoap wSpagoCPSWSSoap) {
			this.wsPago = wSpagoCPSWSSoap;
		}

		public WSvalidadatoSoap getWsValida() {
			return wsValida;
		}

		public void setWsValida(WSvalidadatoSoap wsValida) {
			this.wsValida = wsValida;
		}

		public WsConsultapuntoparkSoap getWsPuntoParq() {
			return wsPuntoParq;
		}

		public void setWsPuntoParq(WsConsultapuntoparkSoap wsPuntoParq) {
			this.wsPuntoParq = wsPuntoParq;
		}

		@Override
		public int hashCode() {
			int hash = url.hashCode() + ssPago.hashCode()
					+ ssValidaDato.hashCode();
			return hash;
		}

		@Override
		public boolean equals(Object obj) {

			if (obj == null)
				return false;

			if (obj instanceof ParkingItem) {
				ParkingItem other = (ParkingItem) obj;
				return other.getUrl().equals(this.url);
			}
			return false;

		}

	}

	public ParkingWSManager() {

		instance = this;
	}

	//@PostConstruct
	public void startService() throws Exception {

		try {

			URL url = new URL(
					"https://www.zonapagos.com/ws_verificar_pagos/Service.asmx?WSDL");

			ssVerify = new com.cps.wservice.payment.verification.Service(url,
					SERVICE_CHECKPAYMENT_NAME);
			verifyPort = ssVerify.getServiceSoap();

//			TaskUtil.startTask(new Runnable() {
//
//				@Override
//				public void run() {
//					ArrayList<CityParkingTransaction> result = cityParkingBean
//							.checkTransactionStatus();
//					// if(debug)LOGGER.info("Check transactions");
//					for (CityParkingTransaction tr : result) {
//						PayItem item = ParkingWSManager.instance
//								.checkPayment(tr.getIdTransaction());
//						ArrayOfPagosV3 array = item.getResPagosV3();
//						String estado = "Error, Transacci�n rechazada";
//
//						if (debug)
//							LOGGER.info("Check items- result="
//									+ item.getVerificarPagoV3Result());
//						if (item.getVerificarPagoV3Result() > 0
//								&& array != null) {
//							int payStatement = array.getPagosV3().get(0)
//									.getIntEstadoPago();
//							String status = tr.getStateTransaction();
//							if (payStatement == 999) {
//								status = "Pending to end";
//								estado = "Transacci�n pendiente por terminar";
//							} else if (payStatement == 888) {
//								estado = "Transacci�n pendiente por empezar";
//								status = "Pending to begin";
//							} else if (payStatement == 1) {
//								status = "Completed";
//								estado = "Transacci�n Completa";
//							} else if (payStatement == 0) {
//								status = "Error";
//							}
//
//							LOGGER.warn("Checking item with transaction number="
//									+ tr.getIdTransaction()
//									+ " status="
//									+ status);
//							if (!tr.getStateTransaction().equals(status)) {
//								boolean updated = cityParkingBean
//										.updateTransactionStatus(
//												String.valueOf(tr
//														.getIdTransaction()),
//												status);
//								if (debug)
//									LOGGER.info("Check items- updated="
//											+ updated);
//							}
//
//						} else if (item.getVerificarPagoV3Result() == 0
//								&& item.getIntError() == 1) {
//							cityParkingBean.updateTransactionStatus(
//									String.valueOf(tr.getIdTransaction()),
//									"Rejected");
//						}
//					}
//
//				}
//			}, 120);

			url = new URL(
					"https://www.zonapagos.com/ws_inicio_pagov2/Zpagos.asmx?WSDL");
			ssZone = new ZPagos(url, SERVICE_ZONE_NAME);
			zonePort = ssZone.getZPagosSoap();
			TaskUtil.enqueTask(new Runnable() {

				@Override
				public void run() {

					ArrayList<Zone> zones;

					try {
						zones = new ZoneBusiness()
								.getZonesByState(em, "472", 1);
						LOGGER.info("inicialización de servicios=" + new Date());
						for (Zone zone : zones) {
							ParkingItem pi = null;
							if (zone.getUrl() != null
									&& zone.getUrl().toLowerCase()
											.contains("http")) {
								try {
									if (debug)
										LOGGER.info("trying with zone id="
												+ zone.getIdzone() + ", url = "
												+ zone.getUrl());
									pi = new ParkingItem(zone.getUrl());
									Cvalorubicaws value = pi
											.getWsPuntoParq()
											.valorWS(
													"EBCAA96BA64393C6959A81EE8B2A9389",
													zone.getIdzone());
									parkingCuotaCache.put(zone.getIdzone(),
											value);
								} catch (Exception e) {
									// System.err.print("error url: "
									// + e.getMessage());
									pi = null;
									zoneBean.setZoneState(zone.getIdzone(), 0);
								}

								if (pi != null)
									parkingItems.put(zone.getIdzone(), pi);
							}
						}

						LOGGER.info("final de inicialización de servicios="
								+ new Date());

						TaskUtil.startTask(zonetask, 180);
						TaskUtil.startTask(cacheRefreshingTask, 120);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void create() throws Exception {
		instance = this;

	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	public void invalidateItems() {
		parkingItems.clear();
		try {
			start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Cvalorws validateData(String parkingCode, String id) {
		LOGGERPAY.info("Validando ID=" + id + ",  y codigo de parqueo ="
				+ parkingCode);
		ParkingItem item = parkingItems.get("COL" + parkingCode);

		LOGGERPAY.info("Validacion: parking item is null=" + (item == null));
		if (item != null) {
			LOGGERPAY.info("Validacion Pago: URL:" + item.getUrl());
			// long startTime = System.currentTimeMillis();
			try {

				return item.getWsValida().valorWS(
						"EBCAA96BA64393C6959A81EE8B2A9389", id);
			} catch (Exception e) {
				LOGGERPAY.error("Validacion error: ", e);

			} finally {
				// if(debug)LOGGER.info("request elapsed time for zone: " +id
				// +" time: " +
				// + (System.currentTimeMillis() - startTime));
			}
		}
		return null;
	}

	public int payParking(String id, String place, String validationNumber,
			String operationResult, String token, String cu, BigDecimal balance) {

		LOGGERPAY.info("Confirmacion Pago: intentando con ID=" + id
				+ ",  y plaza =" + place);

		ParkingItem item = parkingItems.get("COL" + place);
		LOGGERPAY.info("Confirmacion Pago: parking item is null="
				+ (item == null));
		try {
			if (item != null) {
				int res = item.getWsPago().registrarAprobacion(id,
						validationNumber, operationResult, token, cu, balance);

				LOGGERPAY.info("Confirmacion Pago: ID=" + id
						+ ", Numero validaci�n=" + validationNumber
						+ ", operationResult=" + operationResult + ", valor="
						+ res);
				LOGGERPAY.info("Confirmacion Pago: URL:" + item.getUrl());

				return res;
			}
		} catch (Exception e) {
			LOGGERPAY.error("Confirmacion Pago:", e);
		}

		return 0;
	}

	/**
	 * 
	 * @param id
	 * @return Parking place info
	 */
	public Cvalorubicaws getParkingInfo(String id) {
		Cvalorubicaws value = null;
		if (parkingCuotaCache.containsKey(id)) {
			// if(debug)LOGGER.info("cache contains key=" + id);
			value = parkingCuotaCache.get(id);
		} else {
			// TODO: Speed up query to cty parking.
			// value = getParkingInfoUrl(id);
		}
		return value;
	}

	public Map<String, Cvalorubicaws> getParkingsInfo(List<Zone> zones) {
		Map<String, Cvalorubicaws> values = new HashMap<String, Cvalorubicaws>();

		for (Zone zone : zones) {
			String id = zone.getIdzone();
			if (parkingCuotaCache.containsKey(id)) {
				// if(debug)LOGGER.info("cache contains key=" + id);
				values.put(id, parkingCuotaCache.get(id));
			} else {
				// TODO: Speed up query to cty parking.
				// value = getParkingInfoUrl(id);
			}
		}
		return values;
	}

	public Cvalorubicaws getParkingInfoUrl(String id) {
		ParkingItem item = parkingItems.get(id);
		Cvalorubicaws value = null;
		if (item != null) {

			try {

				// long startTime = System.currentTimeMillis();
				value = item.getWsPuntoParq().valorWS(
						"EBCAA96BA64393C6959A81EE8B2A9389", id.substring(3));
				if (debug)
					LOGGER.info("getting info for id= " + id + " name="
							+ value.getNombreparqueadero() + " places="
							+ value.getCupos());
				// if(debug)LOGGER.info("info request elapsed time for zone: "
				// +id
				// +" time: " +
				// + (System.currentTimeMillis() - startTime));
			} catch (Exception e) {
				// e.printStackTrace();
				try {
					zoneBean.setZoneState(id, 0);
					parkingCuotaCache.remove(id);
					if (debug)
						LOGGER.info("removing id= " + id);

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					LOGGER.error("Error trying to remove item from cache");
					e1.printStackTrace();
				}

			} finally {

			}
		}
		return value;
	}

	public String initPayment(PaymentZone pz) {
		String response = zonePort.inicioPagoV3(pz.get_inicioPagoV3_idTienda(),
				pz.get_inicioPagoV3_clave(), pz.get_inicioPagoV3_totalConIva(),
				pz.get_inicioPagoV3_valorIva(), pz.get_inicioPagoV3_idPago(),
				pz.get_inicioPagoV3_descripcionPago(),
				pz.get_inicioPagoV3_email(), pz.get_inicioPagoV3_idCliente(),
				pz.get_inicioPagoV3_tipoId(),
				pz.get_inicioPagoV3_nombreCliente(),
				pz.get_inicioPagoV3_apellidoCliente(),
				pz.get_inicioPagoV3_telefonoCliente(),
				pz.get_inicioPagoV3_infoOpcional1(),
				pz.get_inicioPagoV3_infoOpcional2(),
				pz.get_inicioPagoV3_infoOpcional3(),
				pz.get_inicioPagoV3_codigoServicioPrincipal(),
				pz.get_inicioPagoV3_listaCodigosServicioMulticredito(),
				pz.get_inicioPagoV3_listaNitCodigosServicioMulticredito(),
				pz.get_inicioPagoV3_listaValoresConIva(),
				pz.get_inicioPagoV3_listaValoresIva(),
				pz.get_inicioPagoV3_totalCodigosServicio(),
				pz.get_inicioPagoV3_archivoAdjuntoNombreOriginal(),
				pz.get_inicioPagoV3_archivoAdjuntoNombreGuardado(),
				pz.get_inicioPagoV3_listaInfoOpcional());
		return response;
	}

	public PayItem checkPayment(String strIdPago)// , resPagosV3, intError,
													// strError,
	// verificarPagoV3Result)

	{

		int intIdTienda = 3531;
		String strIdClave = "City3531";
		try {

			ArrayOfPagosV3 _verificarPagoV3_resPagosV3Val = new ArrayOfPagosV3();
			Holder<ArrayOfPagosV3> resPagosV3 = new Holder<ArrayOfPagosV3>(
					_verificarPagoV3_resPagosV3Val);
			Integer intErrorval = 0;
			Holder<Integer> intError = new Holder<Integer>(intErrorval);
			String strErrorVal = "";
			Holder<String> strError = new Holder<String>(strErrorVal);
			Integer verificarPagoV3Resultval = 0;
			Holder<Integer> verificarPagoV3Result = new Holder<Integer>(
					verificarPagoV3Resultval);
			verifyPort.verificarPagoV3(strIdPago, intIdTienda, strIdClave,
					resPagosV3, intError, strError, verificarPagoV3Result);

			return new PayItem(resPagosV3.value, intError.value,
					strError.value, verificarPagoV3Result.value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void removeItemsNoExistIncache(ArrayList<Zone> zones) {

		for (Zone zone : zones) {
			if (parkingCuotaCache.containsKey(zone.getIdzone())) {
				LOGGER.warn("removed not active item");

				parkingCuotaCache.remove(zone.getIdzone());
			}
		}

		/*
		 * for(String key : keyscache ) { boolean contains=false; for(Zone zone
		 * : zones) { if(zone.getIdzone().equals(key)) { contains=true; break; }
		 * } if(!contains) { LOGGER.warn("removed not active item");
		 * 
		 * parkingCuotaCache.remove(key); }
		 * 
		 * }
		 */

	}

	/**
	 * Check for period of times whether a zone online or not and store its
	 * value in database online zone column
	 */
	private Runnable zonetask = new Runnable() {

		@Override
		public void run() {

			if (debug)
				LOGGER.info("starting zone check");
			ArrayList<Zone> zones;
			try {

				zones = zoneBean.getZonesByState("472", 0);
				removeItemsNoExistIncache(zoneBean.getZonesByActivation("472",
						0));

				if (debug)
					LOGGER.info("from zone task, number of offline zones="
							+ zones.size());

				for (Zone zone1 : zones) {
					final Zone zone = zone1;
					TaskUtil.enqueTask(new Runnable() {

						@Override
						public void run() {
							ParkingItem pi = null;
							try {
								if (parkingItems.containsKey(zone.getIdzone()))
									parkingItems.remove(zone.getIdzone());
								if (parkingCuotaCache.containsKey(zone
										.getIdzone()))
									parkingCuotaCache.remove(zone.getIdzone());
								if (zone.getUrl() != null
										&& zone.getUrl().toLowerCase()
												.contains("http")) {
									try {
										if (debug)
											LOGGER.info("from zone task, trying with zone id="
													+ zone.getIdzone()
													+ ", url = "
													+ zone.getUrl());
										pi = new ParkingItem(zone.getUrl());
									} catch (Exception e) {
										LOGGER.error("error item zone id : "
												+ zone.getIdzone() + " "
												+ e.getMessage());

									}

									if (pi != null) {

										if (debug)
											LOGGER.info("Item online, addding back id= "
													+ zone.getIdzone());

										zoneBean.setZoneState(zone.getIdzone(),
												1);
										parkingItems.put(zone.getIdzone(), pi);
										TaskUtil.executeCacheTask(new PopulateCache(
												zone.getIdzone()));

									}
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								// LOGGER.error(e);
								e.printStackTrace();
							}

						}

					});

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// LOGGER.error(e);
				e.printStackTrace();
			} catch (Throwable t) {
				System.err.println("The method zone  failed" + t);

			} finally {
				// try {
				// Thread.sleep(120000);
				// } catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// TaskUtil.enqueTask(zonetask);
			}

		}
	};

	/**
	 * Task to refresh items online
	 */
	private Runnable cacheRefreshingTask = new Runnable() {

		@Override
		public void run() {

			if (debug)
				LOGGER.info("refreshing cache");
			ArrayList<Zone> zones;
			try {
				// if(debug)LOGGER.info("refreshing cache"+
				// parkingCuotaCache.keySet().toString());
				zones = zoneBean.getZonesByState("472", 1);
				if (debug)
					LOGGER.info(" cache size= " + parkingCuotaCache.size());

				/*
				 * Iterator<String> zonesCache = parkingCuotaCache.keySet()
				 * .iterator();
				 * 
				 * 
				 * for ( String zz = zonesCache.next(); zonesCache.hasNext(); zz
				 * = zonesCache.next()) { System.out.println(zz); boolean found
				 * = false; for (Zone z : zones) { if (found =
				 * z.getIdzone().equals(zz) == true) { break; }
				 * 
				 * }
				 * 
				 * if (!found) { System.out.println("Removing from cache: "+
				 * zz);
				 * 
				 * parkingCuotaCache.remove(zz); }
				 * 
				 * }
				 */

				/*
				 * for (Zone zone : zones) {
				 * 
				 * Cvalorubicaws value;
				 * 
				 * if (parkingCuotaCache.containsKey(zone.getIdzone())) { value
				 * = parkingCuotaCache.get(zone.getIdzone()); if (debug)
				 * LOGGER.info("zone=" + zone.getName() + " cache item " +
				 * zone.getIdzone() + " parking value =" + value.getCupos()); }
				 * }
				 */
				int i = 0;
				for (Zone zone : zones) {
					i++;

					// if (!cvalorQueue.contains(zone.getIdzone()))
					// {

					// cvalorQueue.add(zone.getIdzone());
					TaskUtil.executeCacheTask(new PopulateCache(zone
							.getIdzone()));
					// }

				}
				if (debug)
					LOGGER.info("index zone=" + i);
			} catch (Exception e) {
				if (debug)
					e.printStackTrace();
			} catch (Throwable t) {
				System.err.println("The refresh cache  failed" + t);

			} finally {
				// try {
				// Thread.sleep(180000);
				// } catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// TaskUtil.enqueTask(cacheRefreshingTask);
			}

		}
	};

	public synchronized void evictFromCache(String zoneId) {
		LOGGER.info("Evict from cache zoneid=" + zoneId);
		parkingCuotaCache.remove(zoneId);
	}

	private synchronized void putInCache(String zoneId, Cvalorubicaws value) {
		// if(parkingCuotaCache.containsKey(zoneId) )
		// parkingCuotaCache.remove(zoneId);
		if (debug)
			LOGGER.info("put in cache zoneid=" + zoneId);
		parkingCuotaCache.put(zoneId, value);
	}

	/**
	 * inner class to handle parking items
	 * 
	 * @author cpsdevquilla
	 *
	 */
	public class PopulateCache implements Runnable {
		private String zoneId;

		public PopulateCache(String zoneId) {
			super();
			this.zoneId = zoneId;
		}

		@Override
		public void run() {
			try {

				Cvalorubicaws value;
				// value= parkingCuotaCache.get(zone.getIdzone());
				// if(debug)LOGGER.info("zone="+zone.getName()+" cache item "+zone.getIdzone()+" parking value pushed ="+
				// value.getCupos());

				/*
				 * if(parkingCuotaCache.containsKey(zone.getIdzone())) {
				 * parkingCuotaCache.remove(zone.getIdzone()); }
				 */

				value = getParkingInfoUrl(zoneId);
				// if(debug)LOGGER.info(" cache item "+zone.getIdzone()+" parking value before put ="+
				// value.getCupos());

				if (value != null)
					putInCache(zoneId, value);

				// value= parkingCuotaCache.get(zone.getIdzone());
				// if(debug)LOGGER.info(" cache item "+zone.getIdzone()+" parking value after put="+
				// value.getCupos());

			} catch (Exception e1) {
				e1.printStackTrace();
			} catch (Throwable t) {
				System.err.println("The method failed" + t);

			}

			finally {
				// cvalorQueue.remove(zoneId);
			}

		}

	}

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) throws Exception {

		String desiredUrl = "http://186.30.243.70:70/wsPagoXCelular/wsConsultapuntopark.asmx";
		URL url = new URL(desiredUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// just want to do an HTTP GET here
		// connection.setRequestMethod("GET");

		// uncomment this if you want to write output to this url
		// connection.setDoOutput(true);

		// give it 15 seconds to respond
		connection.setReadTimeout(5 * 1000);
		connection.setConnectTimeout(5000);
		connection.connect();
		connection.disconnect();
		// ParkingItem item = new ParkingItem(url);
		// String id = "136";
		// Cvalorws res = item.getWsValida().valorWS(
		// "EBCAA96BA64393C6959A81EE8B2A9389", id);
		//
		// int result = item.getWsPago().registrarAprobacion(
		// String.valueOf(res.getIdentificador()),
		// String.valueOf(res.getCodigoRespuesta()),
		// String.valueOf(res.getCodigoRespuestaPago()),
		// "EBCAA96BA64393C6959A81EE8B2A9389","5234",new BigDecimal(0.0));
		// System.out.println("result value=" + result);
		// URL wsdlURL = new URL(url + "/WSvalidadato.asmx?WSDL");

		// wsdlURL = new URL(url + "/WSpagoCPS.asmx?WSDL");
		// QName SERVICE_NAME = new
		// QName("http://tempuri.org/","wsConsultapuntopark");
		// javax.xml.ws.Service service = javax.xml.ws.Service.create(wsdlURL,
		// SERVICE_PAGO_NAME);
		/*
		 * WsConsultapuntoparkSoap value = service
		 * .getPort(WsConsultapuntoparkSoap.class);
		 */
		// WSpagoCPSWSSoap value = new WSpagoCPSWS(wsdlURL,
		// SERVICE_PAGO_NAME).getWSpagoCPSWSSoap();

		// / value.registrarAprobacion(identificadors, numeroAprobacion1,
		// resultadoOperacion1, token1)

		// long startTime = System.currentTimeMillis();
		// // Cvalorubicaws resp =
		// // value.valorWS("EBCAA96BA64393C6959A81EE8B2A9389", "101");
		// if (debug)
		// LOGGER.info("info request elapsed time for zone: 101 time: "
		// + +(System.currentTimeMillis() - startTime));
		// if(debug)LOGGER.info("cupos=" + resp.getCupos());
	}

}
