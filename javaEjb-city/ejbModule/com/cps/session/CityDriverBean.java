package com.cps.session;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.CityDriver;
import com.cps.entity.bean.CityDriverStatus;
import com.cps.entity.bean.CityDriverOperator;
import com.cps.remote.CityDriverBeanRemote;
import com.cps.business.CityDriverBusiness;

@Stateless(name="CityDriverBeanRemote")
public class CityDriverBean implements CityDriverBeanRemote {

	@PersistenceContext
	EntityManager em;
	

	@Override
	public ArrayList<CityDriver> getCityDrivers() throws Exception {
		return new CityDriverBusiness().getCityDrivers(em);
	}
	
	@Override
	public ArrayList<SysUser> getCityDriversOperators(Integer SysUserType) throws Exception {
		return new CityDriverBusiness().getCityDriversOperators(em, SysUserType);
	}
	
	@Override
	public ArrayList<CityDriverOperator> currentCityDriversOperators(CityDriverStatus cityDriverStatus)throws Exception{
		return new CityDriverBusiness().currentCityDriversOperators(em, cityDriverStatus);
	}
	
	@Override
	public CityDriverStatus getCityDriverStatus(CityDriver cityDriver) throws Exception{
		return new CityDriverBusiness().getCityDriverStatus(em, cityDriver);
	}
	
	public CityDriver registerCityDriver(CityDriver cityDriver)throws Exception{
		return new CityDriverBusiness().registerCityDriver(em, cityDriver);
	}

	@Override
	public void updateCityDriver(CityDriver cityDriver) throws Exception {
		new CityDriverBusiness().updateCityDriver(em, cityDriver);	
	}

	@Override
	public void updateCityDriverStatus(CityDriverStatus cityDriverStatus) throws Exception {
		new CityDriverBusiness().updateCityDriverStatus(em, cityDriverStatus);	
	}
	
	@Override
	public void updateCityDriverOperator(CityDriverOperator cityDriverStatus) throws Exception {
		new CityDriverBusiness().updateCityDriverOperator(em, cityDriverStatus);	
	}
	
	@Override
	public void deleteCityDriverOperator(SysUser sysUser, CityDriverStatus cityDriverStatus) throws Exception{
		new CityDriverBusiness().deleteCityDriverOperator(em, sysUser, cityDriverStatus);	
	}
	
	@Override
	public ArrayList<CityDriver> getCityDriversById(String idCityDriver) throws Exception {
		return new CityDriverBusiness().getCityDriversById(em, idCityDriver);
	}

	@Override
	public void setCityDriverState(String id, int state)  throws Exception {	
		 new CityDriverBusiness().setCityDriverState(em, id, state);	
	}

	@Override
	public void toggleActivate(String id, Integer activate) {
		 new CityDriverBusiness().toggleActivate(em, id, activate);	
	}

}
