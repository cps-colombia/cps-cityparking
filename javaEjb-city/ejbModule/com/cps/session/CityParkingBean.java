package com.cps.session;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.Holder;

//import org.jboss.ws.extensions.security.Util;



import com.cps.business.CityParkingBusiness;
import com.cps.business.CountryBusiness;
import com.cps.business.ParkingBusiness;
import com.cps.business.SysUserBusiness;
import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.CityParkingTransactionUser;
import com.cps.entity.bean.PayItem;
import com.cps.entity.bean.PaymentResponse;
import com.cps.entity.bean.PaymentResponse.InfoPayment;
import com.cps.entity.bean.PaymentZone;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.Zone;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.service.Cvalorws;
import com.cps.wservice.Cvalorubicaws;
import com.cps.wservice.payment.verification.ArrayOfPagosV3;

@Stateless(name = "CityParkingBeanRemote")
public class CityParkingBean implements CityParkingBeanRemote {

	@PersistenceContext
	EntityManager em;
	

	/*
	 * @Override public Cvalorws validateData( String zoneid, String token,
	 * String id) { return ParkingWSManager.instance.validateData(zoneid, token,
	 * id);
	 * 
	 * }
	 */

	@Override
	public PaymentResponse validateParking(String parkingCode, String payCode) {

		Cvalorws data = ParkingWSManager.instance.validateData(parkingCode,
				payCode);
		PaymentResponse payrespn = new PaymentResponse();
		payrespn.setValue(-301);
		if (data != null) {
			payrespn.setValue(data.getCodigoRespuesta());

			if (data.getCodigoRespuesta() == 1) {

				InfoPayment info = new InfoPayment(String.valueOf(data
						.getIdentificador()), String.valueOf(data
						.getCodigoRespuesta()), String.valueOf(data
						.getCodigoRespuestaPago()), data.getToken(), data
						.getFechaInicioParqueo().toString(), data
						.getFechaFinParqueo().toString(), data
						.getImporteParqueo().doubleValue(), data.getPlazaId(),
						(int) Math.ceil(data.getTiempoParqueo() / 60.0));

				payrespn.setInfo(info);

			}

		}

		return payrespn;

	}

	@Override
	public int payParking(SysUser user, double value, String id,
			String validationNumber, String operationResult, String token,
			String zone, String initparkingdate, String endparkingdate, int deviceId) {
		SysUserBusiness sub = new SysUserBusiness();
		BigDecimal balance;
		String confirmationNumber = zone + id;
		try {
			balance = sub.getBalance(em, user);

			int res = 12;
			if (balance.doubleValue() >= value) {
				String prefix = new CountryBusiness().getCountryPrefix(em,
						user.getCountryIdcountry());
				res = ParkingWSManager.instance.payParking(id, zone, confirmationNumber, "8",
						 token,  user.getCu(), user.getBalance());
				if (res == 12) {
					balance = balance.subtract(BigDecimal.valueOf(value));
					sub.setBalance(em, user, balance);
					new ParkingBusiness()
							.addHistoryEvent(em, BigDecimal.valueOf(value),
									user.getFavoriteMsisdn(), 2, id,
									user.getCountryIdcountry(), user, prefix
											+ zone, confirmationNumber,
									initparkingdate, endparkingdate,deviceId);
					return res;

				}
			} else {

				// int _registrarAprobacion__return =
				// port.registrarAprobacion(_registrarAprobacion_identificadors,
				// _registrarAprobacion_numeroAprobacion1,
				// _registrarAprobacion_resultadoOperacion1,
				// _registrarAprobacion_token1);

				/*
				 * ParkingWSManager.instance.payParking(
				 * String.valueOf(data.getIdentificador()),
				 * String.valueOf(data.getCodigoRespuesta()),
				 * String.valueOf(data.getCodigoRespuestaPago()),
				 * data.getToken());
				 */

				res = ParkingWSManager.instance.payParking(id, zone, "9",
						confirmationNumber, token,user.getCu(), user.getBalance());
				return 9;
			}
			return res;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			ParkingWSManager.instance.payParking(id, zone, "11",
					confirmationNumber, token, user.getCu(), user.getBalance());
			e.printStackTrace();
		}
		return 13;
	}

	
	@Override
	public Cvalorubicaws getParkingInfo(String id) {
		try {
			return ParkingWSManager.instance.getParkingInfo(id);
		} catch (Exception e) {
            e.printStackTrace();
		}
		return null;
	}

	private final int STORE_CODE = 3531;
	private final double taxValue = 0;
    private final String serviceCode="5600";
    // private final String serviceCode="2701";//testcode

	@Override
	public String[] initPayment(SysUser user, double ammount) {
		// TODO Auto-generated method stub
		double amountTax = (ammount * taxValue / 100);

		String id = com.cps.util.Util.getNextId();
		PaymentZone pz = new PaymentZone(STORE_CODE, "City3531", ammount
				+ amountTax, amountTax, id, "Pago Cps", user.getEmail(),
				user.getCu(), "11", user.getName(), user.getLastName(),
				user.getFavoriteMsisdn(), "", "", "", serviceCode/*"2701"*/, null, null, null,
				null, 0, "", "", null);
		String response = ParkingWSManager.instance.initPayment(pz);

		return new String[] { response, id };
	}

	/**
	 * check remote payment status
	 * 
	 */
	@Override
	public PayItem checkPayment(String strIdPago) {

		return ParkingWSManager.instance.checkPayment(strIdPago);
		// return resPagosV3.value.toString()+ ","+intError.value+
		// ","+strError.value+ ","+verificarPagoV3Result.value;

	}

	@Override
	public CityParkingTransaction checkTransaction(String id) {

		return new CityParkingBusiness().getTransaccionById(em, id);
	}

	@Override
	public void registryTransaction(CityParkingTransaction transaction) {

		new CityParkingBusiness().registryTransaccion(em, transaction);
	}

	@Override
	public boolean updateTransactionStatus(String id, String status) {
		return new CityParkingBusiness().updateTransaccionById(em, id, status);
	}

	@Override
	public ArrayList<CityParkingTransaction> getTransactionsByUserId(
			long user_sys_id) {
		return new CityParkingBusiness().getTransactionsByUserId(em,
				user_sys_id);
	}

	@Override
	public CityParkingTransactionUser getTransactionByUserId(long user_sys_id) {
		return new CityParkingBusiness()
				.getTransactionByUserId(em, user_sys_id);
	}

	@Override
	public ArrayList<CityParkingTransaction> checkTransactionStatus() {
		return new CityParkingBusiness().checkTransactionStatus(em);
	}

	@Override
	public ArrayList<CityParkingTransactionUser> getReportSales(String fechaIni,
			String fechaFin, String email) throws Exception {
		// TODO Auto-generated method stub
		return new CityParkingBusiness().getReportSales(em, fechaIni, fechaFin, email);
	}

	
	@Override
	public ArrayList<CityParkingTransactionUser> getReportSalesByUser( SysUser sUser,String fechaIni,
			String fechaFin) throws Exception {
		// TODO Auto-generated method stub
		return new CityParkingBusiness().getReportSalesByUser(em,sUser, fechaIni, fechaFin);
	}
	
	public static void main(String[] args) {
		
		double dvalue= 1000000;
		
		System.out.println( BigDecimal.valueOf(dvalue));
		System.out.println( new BigDecimal(String.valueOf(dvalue)));
		
	}

	@Override
	public Map<String, Cvalorubicaws> getParkingsInfo(List<Zone> zones) {
		// TODO Auto-generated method stub
		return ParkingWSManager.instance.getParkingsInfo(zones) ;
	}

	@Override
	public ArrayList<CityParkingTransactionUser> getReportSales(
			String fechaIni, String fechaFin, String email, String method)
			throws Exception {
		// TODO Auto-generated method stub
		return new CityParkingBusiness().getReportSales(em, fechaIni, fechaFin, email,method); 
	}

}
