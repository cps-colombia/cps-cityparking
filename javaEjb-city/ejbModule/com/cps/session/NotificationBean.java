package com.cps.session;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.NotificationBusiness;
import com.cps.entity.bean.Notification;
import com.cps.entity.bean.SysUser;
import com.cps.remote.NotificationBeanRemote;

@Stateless(name="NotificationBeanRemote")
public class NotificationBean implements NotificationBeanRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public void addNotification(Notification notification) throws Exception {
		new NotificationBusiness().addNotification(em, notification);
	}

	@Override
	public ArrayList<Notification> getNotifications() throws Exception {
		return new NotificationBusiness().getNotifications(em);
	}

	@Override
	public ArrayList<Notification> getNotificationsByPlatform(int platform)
			throws Exception {
		return new NotificationBusiness().getNotificationsByPlatform(em, platform);
	}

	@Override
	public void setRead(String id) throws Exception {
		new NotificationBusiness().setRead(em, id);
	}

	@Override
	public ArrayList<Notification> getUserNotifications(SysUser user)
			throws Exception {
		return new NotificationBusiness().getUserNotifications(em, user);
	}

}
