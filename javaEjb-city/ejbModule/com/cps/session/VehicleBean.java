package com.cps.session;

import java.math.BigInteger;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.VehicleBusiness;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.SysUserVehicle;
import com.cps.entity.bean.Vehicle;
import com.cps.entity.bean.VehicleBrand;
import com.cps.entity.bean.VehicleType;
import com.cps.remote.VehicleBeanRemote;

/**
 * Session Bean implementation class VehicleBean
 */
@Stateless(name="VehicleBeanRemote")
public class VehicleBean implements VehicleBeanRemote {
	
	@PersistenceContext
	EntityManager em;
   

	@Override
	public int registerVehicle(String plate, SysUser user) throws Exception {
		return new VehicleBusiness().registerVehicle(em, plate, user);
	}

	@Override
	public int registerVehicleFull(String plate, BigInteger type, BigInteger brand, String description, SysUser user) throws Exception {
		return new VehicleBusiness().registerVehicleFull(em, plate, type, brand, description, user);
	}
	
	@Override
	public Vehicle getVehicle(String plate, SysUser user) throws Exception {
		return new VehicleBusiness().getVehicle(em, plate, user);
	}	

	@Override
	public Vehicle getVehicleByMobile(String msisdn) throws Exception {
		return new VehicleBusiness().getVehicleByMobile(em, msisdn);
	}

	@Override
	public ArrayList<Vehicle> getVehicles(SysUser user) throws Exception {
		return new VehicleBusiness().getVehicles(em, user);
	}

	@Override
	public int shareVehicle(SysUser user, String plate) throws Exception {
		return new VehicleBusiness().shareVehicle(em, user, plate);
	}


	@Override
	public ArrayList<Vehicle> getListVehicles(SysUser user) throws Exception {
		return new VehicleBusiness().getListVehicles(em, user);
	}

	@Override
	public void updateVehicle(Vehicle vehicle) throws Exception {
		new VehicleBusiness().updateVehicle(em, vehicle);		
	}
	
	@Override
	public int deleteVehicle(String plate, SysUser user) throws Exception {
		return new VehicleBusiness().deleteVehicle(em, plate, user);
	}

	@Override
	public boolean isOwnerVehicle(SysUser user, String plate) throws Exception {
		return new VehicleBusiness().isOwnerVehicle(em, user, plate);
	}

	@Override
	public boolean existsVehicle(String plate) throws Exception {
		return new VehicleBusiness().existsVehicle(em, plate);
	}

	@Override
	public ArrayList<SysUserVehicle> getSysUserVehicles(SysUser user)
			throws Exception {
		return new VehicleBusiness().getSysUserVehicles(em, user);
	}
	
	@Override
	public ArrayList<VehicleBrand> getVehicleBrand() throws Exception {
		return new VehicleBusiness().getVehicleBrand(em);
	}
	
	@Override
	public ArrayList<VehicleType> getVehicleType() throws Exception {
		return new VehicleBusiness().getVehicleType(em);
	}

	@Override
	public VehicleType getVehicleTypeById(int vType) throws Exception {
		// TODO Auto-generated method stub
		return new VehicleBusiness().getVehicleTypeById(em, vType);
	}

}
