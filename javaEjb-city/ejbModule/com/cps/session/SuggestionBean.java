package com.cps.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.SuggestionBusiness;
import com.cps.entity.bean.SysUser;
import com.cps.remote.SuggestionBeanRemote;

@Stateless(name="SuggestionBeanRemote")
public class SuggestionBean implements SuggestionBeanRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public void addSuggestion(String suggestion,SysUser sysuser) throws Exception {
		new SuggestionBusiness().addSuggestion(em, suggestion,  sysuser);
	}

}
