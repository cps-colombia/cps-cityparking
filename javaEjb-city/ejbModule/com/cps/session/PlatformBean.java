package com.cps.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.PlatformBusiness;
import com.cps.entity.bean.Platform;
import com.cps.remote.PlatformBeanRemote;

@Stateless(name="PlatformBeanRemote")
public class PlatformBean implements PlatformBeanRemote
{
	@PersistenceContext
	EntityManager em;

	@Override
	public Platform getPlatForm( int idplatform )
	{
		// TODO Auto-generated method stub
		return new PlatformBusiness().getPlatForm( em, idplatform );
	}

	@Override
	public List<Platform> getPlatforms()
	{
		// TODO Auto-generated method stub
		return new PlatformBusiness().getPlatforms( em );
	}

}
