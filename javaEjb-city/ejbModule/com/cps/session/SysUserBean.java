package com.cps.session;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.SysUserBusiness;
import com.cps.entity.bean.MsisdnUser;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.TokenCheck;
import com.cps.entity.bean.UserTransactionReport;
import com.cps.remote.SysUserBeanRemote;

/**
 * Session Bean implementation class VehicleBean
 */
@Stateless(name="SysUserBeanRemote")
public class SysUserBean implements SysUserBeanRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@Override
	public SysUser getUser(String user) throws Exception {
		return new SysUserBusiness().getUser(em, user);
	}

	@Override
	public boolean isValidCredentials(String user, String pass)
			throws Exception {
		return new SysUserBusiness().isValidCredentials(em, user, pass);
	}

	@Override
	public int setPassword(String user, String oldPass, String pass)
			throws Exception {
		return new SysUserBusiness().setPassword(em, user, oldPass, pass);
	}

	@Override
	public int updateUser(SysUser user) throws Exception {
		return new SysUserBusiness().updateUser(em, user);
	}

	@Override
	public SysUser registerUser(SysUser user) throws Exception {		
		return new SysUserBusiness().registerUser(em, user);
	}

	@Override
	public BigDecimal getBalance(SysUser user) throws Exception {
		return new SysUserBusiness().getBalance(em, user);
	}

	@Override
	public int reloadBalance(SysUser user, String PIN) throws Exception {
		
		try
		{
		int sysys = new SysUserBusiness().reloadBalance(em, user, PIN);
		return sysys;
		}catch( Exception e)
		{
			throw e;
		}
	}

	@Override
	public int reloadBalance(SysUser user, BigDecimal balance) throws Exception {
		return new SysUserBusiness().reloadBalance(em, user, balance);
	}
	
	@Override
	public int setBalance(SysUser user, BigDecimal balance) throws Exception {
		// TODO Auto-generated method stub
		return new SysUserBusiness().setBalance(em, user, balance);
	}
	

	@Override
	public SysUser getUserByMobile(String msisdn) throws Exception {
		return new SysUserBusiness().getUserByMobile(em, msisdn);
	}

	@Override
	public SysUser getUserByCU(String CU) throws Exception {
		return new SysUserBusiness().getUserByCU(em, CU);
	}

	@Override
	public boolean isCU(String cu) throws Exception {
		return new SysUserBusiness().isCU(em, cu);
	}

	@Override
	public void addMobile(String msisdn, SysUser user) throws Exception {
		new SysUserBusiness().addMobile(em, msisdn, user);
	}

	@Override
	public ArrayList<MsisdnUser> getMobileList(SysUser user) throws Exception {
		return new SysUserBusiness().getMobileList(em, user);
	}

	@Override
	public void removeMobile(String msisdn, SysUser user) throws Exception {
		new SysUserBusiness().removeMobile(em, msisdn, user);
	}

	@Override
	public void addAccount(SysUser user, SysUser userAdded) throws Exception {
		new SysUserBusiness().addAccount(em, user, userAdded);
	}

	@Override
	public void acceptAccount(SysUser user, SysUser userAdded) throws Exception {
		new SysUserBusiness().acceptAccount(em, user, userAdded);		
	}

	@Override
	public ArrayList<SysUser> getAccountUsersList(SysUser user)
			throws Exception {
		return new SysUserBusiness().getAccountUsersList(em, user);
	}

	@Override
	public int updateUserNoBalance(SysUser user) throws Exception {
		return new SysUserBusiness().updateUser(em, user);
	}
	
	@Override
	public Object createUserBySMS(String plate, String msisdn, String PIN, String idCountry)
			throws Exception {
		return new SysUserBusiness().createUserBySMS(em, plate, msisdn, PIN, idCountry);
	}

	@Override
	public ArrayList<SysUser> getRequestAccountUsersList(SysUser user) throws Exception {
		return  new SysUserBusiness().getRequestAccountUsersList(em, user);
	}

	@Override
	public ArrayList<SysUser> getTempUsers() throws Exception {
		return new SysUserBusiness().getTempUsers(em);
	}

	@Override
	public ArrayList<SysUser> getTempUsersEmptyBalance() throws Exception {
		return new SysUserBusiness().getTempUsersEmptyBalance(em);
	}

	@Override
	public Object createTempUser(String plate, String PIN, String idCountry)
			throws Exception {
		return new SysUserBusiness().createTempUser(em, plate, PIN, idCountry);
	}

	@Override
	public SysUser getUserByEmail(String email) throws Exception {
		return new SysUserBusiness().getUserByEmail(em, email);
	}

	@Override
	public int setPassword(SysUser user, String pass) throws Exception {
		
		return new SysUserBusiness().setPassword(em, user, pass);
	}

	@Override
	public ArrayList<SysUser> getReportUsersList( String fechaIni, String fechaFin ) throws Exception
	{
		return new SysUserBusiness().getReportUsersList( em, fechaIni, fechaFin );
	}

	@Override
	public boolean registryUserTransaction(String userOriginId,
			String userDestId, BigDecimal amount) throws Exception {
		// TODO Auto-generated method stub
		return new SysUserBusiness().registryUserTransaction(em, userOriginId, userDestId, amount);
	}

	@Override
	public List<UserTransactionReport> getTransactionForUser( String userId, String fechaIni, String fechaFin ) {
		// TODO Auto-generated method stub
		return new SysUserBusiness().getTransactionForUser(em, userId, fechaIni, fechaFin );
	}

	@Override
	public List<UserTransactionReport> getTransactionForAllUsers(
			String fechaIni, String fechaFin) throws Exception {
		// TODO Auto-generated method stub
		return  new SysUserBusiness().getTransactionForAllUsers(em, fechaIni, fechaFin);
	}

	@Override
	public boolean tokenExist(String token) throws Exception {
		// TODO Auto-generated method stub
		return new SysUserBusiness().tokenExist(em, token) ;
	}

	@Override
	public void addToken(TokenCheck check) throws Exception {
		new SysUserBusiness().addToken(em, check);
	}

	@Override
	public ArrayList<SysUser> getUsers(String criterial) throws Exception {
		return new SysUserBusiness().getUsers(em, criterial);
	}

	@Override
	public ArrayList<SysUser> getUsersAndExclude(String criterial,
			String[] excludes) throws Exception {
		// TODO Auto-generated method stub
		return new SysUserBusiness().getUsersAndExclude(em, criterial, excludes);
	}

	
	
	

}
