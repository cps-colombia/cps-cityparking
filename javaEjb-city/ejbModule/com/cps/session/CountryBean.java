package com.cps.session;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.CountryBusiness;
import com.cps.entity.bean.City;
import com.cps.entity.bean.Country;
import com.cps.remote.CountryBeanRemote;

@Stateless(name="CountryBeanRemote")
public class CountryBean implements CountryBeanRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public String getCountryPrefix(String idCountry) throws Exception {
		return new CountryBusiness().getCountryPrefix(em, idCountry);
	}

	@Override
	public ArrayList<Country> getCountrys() throws Exception {
		return new CountryBusiness().getCountrys(em);
	}

	@Override
	public Country getCountry(String idCountry) throws Exception {
		return new CountryBusiness().getCountry(em, idCountry);
	}

	@Override
	public ArrayList<City> getCities(String country)
			throws Exception {
		return new CountryBusiness().getCities(em, country);
	}

	@Override
	public City getCity(String idCity) throws Exception {
		return new CountryBusiness().getCity(em, idCity);
	}

}
