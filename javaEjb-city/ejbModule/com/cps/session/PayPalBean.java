/**
 * 
 */
package com.cps.session;

//import java.util.ArrayList;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

//import com.cps.business.ParkingBusiness;
import com.cps.business.PayPalBusiness;
import com.cps.entity.bean.PayPalTransaction;
//import com.cps.entity.bean.ReportParking;
import com.cps.remote.PayPalBeanRemote;

/**
 * @author Jorge
 *
 */
@Stateless(name="PayPalBeanRemote")
public class PayPalBean implements PayPalBeanRemote {

	@PersistenceContext
	 EntityManager em;
	
	/* (non-Javadoc)
	 * @see com.cps.remote.PayPalBeanRemote#registrarTransaccion(com.cps.entity.bean.PayPalTransaction)
	 */
	@Override
	public void registrarTransaccion(PayPalTransaction palTransaction) {
		new PayPalBusiness().registrarTransaccion(em, palTransaction);
	}

	@Override
	public PayPalTransaction getTransaccionById(String idTransaccion) {
    	return new PayPalBusiness().getTransaccionById(em, idTransaccion);
	}
	
	
	 /**
	 * Retorna todas las ventas en un rango de fecha dado.
	 * @param fechaIni
	 * @param fechaFin
	 * @return
	 * @throws Exception
	 */
	@Override
	public ArrayList<PayPalTransaction> getReportSales(String fechaIni,
			String fechaFin, String method) throws Exception {
		return new PayPalBusiness().getReportSales(em, fechaIni, fechaFin, method);
	}
}

/*	@Override
	public ArrayList<ReportSales> getReportSales(String fechaIni,String fechaFin){
    	return new PayPalBusiness().getReportSales(em,String fechaIni,String fechaFin);
	}*/

