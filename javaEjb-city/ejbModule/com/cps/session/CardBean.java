package com.cps.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.cps.business.CardBusiness;
import com.cps.entity.bean.Card;
import com.cps.entity.bean.SysUser;
import com.cps.remote.CardBeanRemote;

@Stateless(name="CardBeanRemote")
public class CardBean implements CardBeanRemote{

	@PersistenceContext
	EntityManager em;
	

	@Override
	public Card getCard(String PIN)throws Exception{
		return new CardBusiness().getCard(em, PIN);
	}
	
	@Override
	public boolean isCard(String PIN)throws Exception{
		return new CardBusiness().isCard(em, PIN);
	}
	
	@Override
	public void setUsed(SysUser user, String PIN)throws Exception{
		new CardBusiness().setUsed(em, user, PIN);
	}

	@Override
	public void importCards(List<Card> lisCards) throws Exception {
		for (Card card : lisCards) {
		new CardBusiness().registerCard(em, card);
		}
	}
	
}
