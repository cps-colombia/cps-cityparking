package com.cps.session;

//import org.jboss.ejb3.annotation.Management;


public interface BeanLauncherManagement
{
    public void start() throws Exception;
    public void create () throws Exception; 
    public void destroy () throws Exception; 
}
