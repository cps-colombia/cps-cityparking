package com.cps.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.SubZoneBusiness;
import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentSubZone;
import com.cps.entity.bean.AgentSubZonePK;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.PlaceSubzone;
import com.cps.entity.bean.Subzone;
import com.cps.remote.SubZoneBeanRemote;

@Stateless(name="SubZoneBeanRemote")
public class SubZoneBean implements SubZoneBeanRemote {

	@PersistenceContext
	EntityManager em;
	
	private SubZoneBusiness subZoneBusiness = new SubZoneBusiness();

	@Override
	public void create(Subzone subzone) throws Exception {
		subZoneBusiness.create(subzone, em);
	}

	@Override
	public void edit(Subzone subzone) throws Exception {
		subZoneBusiness.edit(subzone, em);
		
	}

	@Override
	public void destroy(Long id) throws Exception {
		subZoneBusiness.destroy(id, em);
		
	}

	@Override
	public List<Subzone> findSubzones() throws Exception {
		return subZoneBusiness.findSubzoneEntities(em);
	}

	@Override
	public List<Subzone> findSubzones(int maxResults, int firstResult)
			throws Exception {
		return subZoneBusiness.findSubzoneEntities(maxResults, firstResult, em);
	}

	@Override
	public Subzone findSubzone(Long id) throws Exception {
		return subZoneBusiness.findSubzone(id, em);
	}

	@Override
	public int getSubzoneCount() throws Exception {
		return subZoneBusiness.getSubzoneCount(em);
	}

	@Override
	public List<Subzone> findAllByZone(String idZone) throws Exception {
		return subZoneBusiness.findAllByZone(idZone, em);
	}

	@Override
	public void createPlaceSubzone(PlaceSubzone placeSubzone) throws Exception {
		subZoneBusiness.createPlaceSubzone(placeSubzone, em);
	}

	@Override
	public void associateAgentSubzone(AgentSubZone agentSubZone)
			throws Exception {
		subZoneBusiness.associateAgentSubzone(agentSubZone, em);
		
	}

	@Override
	public void disassociateAgentSubzone(AgentSubZonePK agentSubZone) throws Exception {
		subZoneBusiness.disassociateAgentSubzone(agentSubZone, em);
		
	}

	@Override
	public List<Agent> findAgentsBySubZone(String idSubZone) throws Exception {
		return subZoneBusiness.findAgentsBySubZone(idSubZone, em);
	}

	@Override
	public List<Place> findPlacesAvailablesByZone(String idZone)
			throws Exception {
		return subZoneBusiness.findPlacesAvailablesByZone(idZone, em);
	}

	@Override
	public List<PlaceSubzone> findPlacesBySubZone(String idSubZone)
			throws Exception {
		return subZoneBusiness.findPlacesBySubZone(idSubZone, em);
	}

	@Override
	public void destroyPlaceSubzone(PlaceSubzone placeSubzoneTem)
			throws Exception {
		subZoneBusiness.destroyPlaceSubzone(placeSubzoneTem, em);
	}


}
