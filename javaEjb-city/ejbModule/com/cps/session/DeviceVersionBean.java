package com.cps.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.DeviceVersionBussines;
import com.cps.remote.DeviceVersionRemote;
@Stateless(name="DeviceVersionRemote")
public class DeviceVersionBean implements DeviceVersionRemote {
	@PersistenceContext
	EntityManager em;
	@Override
	public boolean checklastDeviceVersion(Integer id, String currDeviceVersion) {
		// TODO Auto-generated method stub
		return new DeviceVersionBussines().checklastDeviceVersion(em, id, currDeviceVersion);
	}

}
