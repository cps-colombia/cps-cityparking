package com.cps.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.SocialNetworkBusiness;
import com.cps.remote.SocialNetworkRemote;

@Stateless(name="SocialNetworkRemote")
public class SocialNetworkBean implements SocialNetworkRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public String getSNUrl(String name) throws Exception {
		return new SocialNetworkBusiness().getSNUrl(em, name);
	}

}
