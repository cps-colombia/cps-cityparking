package com.cps.session;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.AdministrativeReportBusiness;
import com.cps.entity.bean.TransactionItem;
import com.cps.remote.AdministrativeReportBeanRemote;

@Stateless(name = "AdministrativeReportBeanRemote")
public class AdministrativeReportBean implements AdministrativeReportBeanRemote {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<TransactionItem> getTransactionsByDateRange(Date startDate,
			Date endDate, String[] transactionTypeText) {
		// TODO Auto-generated method stub
		return new AdministrativeReportBusiness().getTransactionsByDateRange(
				em, startDate, endDate,transactionTypeText);
	}

	@Override
	public List<TransactionItem> getUserTransactionsByDateRange(Date startDate,
			Date endDate,String[] transactionTypeText) {
		// TODO Auto-generated method stub
		return new AdministrativeReportBusiness().getUserTransactionsByDateRange(
				em,startDate,endDate,transactionTypeText) ; 
	}

}
