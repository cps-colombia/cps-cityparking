package com.cps.session;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.PlaceBusiness;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.PlaceSubzone;
import com.cps.entity.bean.Zone;
import com.cps.remote.PlaceBeanRemote;

@Stateless(name="PlaceBeanRemote")
public class PlaceBean implements PlaceBeanRemote{

	@PersistenceContext
	EntityManager em;
	
	@Override
	public boolean isPlace(String place, String idCountry) throws Exception {
		return new PlaceBusiness().isPlace(em, place, idCountry);
	}

	@Override
	public boolean isPlaceAvailable(String place, String idCountry) throws Exception {
		return new PlaceBusiness().isPlaceAvailable(em, place, idCountry);
	}

	@Override
	public Zone getPlaceZone(String place, String idCountry) throws Exception {
		return new PlaceBusiness().getPlaceZone(em, place, idCountry);
	}

	@Override
	public Place getPlace(String place, String idCountry) throws Exception {
		return new PlaceBusiness().getPlace(em, place, idCountry);
	}

	@Override
	public void registerPlace(Place place) throws Exception {
		new PlaceBusiness().registerPlace(em, place);
	}

	@Override
	public ArrayList<Place> getPlacesZone(String idZone) throws Exception {
		return new PlaceBusiness().getPlacesZone(em, idZone);
	}
	@Override
	public ArrayList<PlaceSubzone> getPlacesSubZone( Long idSubZone ) throws Exception
	{
		// TODO Auto-generated method stub
		return  new PlaceBusiness().getPlacesSubZone(em, idSubZone);
	}

	@Override
	public void deletePlace(Place place) {
		new PlaceBusiness().deletePlace(em, place);
	}

	
	
}
