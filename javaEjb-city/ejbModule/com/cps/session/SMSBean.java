package com.cps.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.SMSBusiness;
import com.cps.remote.SMSBeanRemote;

@Stateless(name="SMSBeanRemote")
public class SMSBean implements SMSBeanRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@Override
	public void insertInbox(String min, String msisdn, String operator,
			String option, String shortCode, String text) throws Exception {
		new SMSBusiness().insertInbox(em, min, msisdn, operator, option, shortCode, text);
	}

	@Override
	public void insertOutbox(String min, String msisdn, String operator,
			String option, String shortCode, String text) throws Exception {
		new SMSBusiness().insertOutbox(em, min, msisdn, operator, option, shortCode, text);
	}

}
