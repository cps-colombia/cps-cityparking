package com.cps.session;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.StatusBusiness;
import com.cps.entity.bean.Status;
import com.cps.remote.StatusBeanRemote;

/**
 * Session Bean implementation class StatusBean
 */
@Stateless(name="StatusBeanRemote")
public class StatusBean implements StatusBeanRemote {

	@PersistenceContext
	EntityManager em;
	

	@Override
	public ArrayList<Status> getStatus() throws Exception {
		return new StatusBusiness().getStatus(em);
	}

}
