package com.cps.session;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.ZoneBusiness;
import com.cps.entity.bean.PriceRestrict;
import com.cps.entity.bean.RateType;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.TimeRestrict;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZoneLocation;
import com.cps.entity.bean.ZoneRestriction;
import com.cps.remote.ZoneBeanRemote;

@Stateless(name = "ZoneBeanRemote")
public class ZoneBean implements ZoneBeanRemote
{

    @PersistenceContext
    EntityManager em;

    @Override
    public int getMaxMinutes( String idZone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().getMaxMinutes(em, idZone, idCountry);
    }

    @Override
    public BigDecimal getMinuteValue( String idZone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().getMinuteValue(em, idZone, idCountry);
    }

    @Override
    public boolean isAvailableZone( String idZone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().isAvailableZone(em, idZone, idCountry);
    }

    @Override
    public Zone getZone( String idZone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().getZone(em, idZone, idCountry);
    }

    @Override
    public ArrayList<Zone> getZones( String idCountry ) throws Exception
    {
	return new ZoneBusiness().getZones(em, idCountry);
    }

    @Override
    public ArrayList<Zone> getAgentZones( String idagent ) throws Exception
    {
	return new ZoneBusiness().getAgentZones(em, idagent);
    }

    @Override
    public int getAvaliableSpaces( String idZone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().getAvaliableSpaces(em, idZone, idCountry);
    }

    @Override
    public int getMinsLeftAvaliableSpace( String idZone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().getMinsLeftAvaliableSpace(em, idZone, idCountry);
    }

    @Override
    public boolean isZone( String zone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().isZone(em, zone, idCountry);
    }

    @Override
    public ArrayList<Zone> getZonesByCity( SysUser su, String idCountry, String idCity ) throws Exception
    {

	return new ZoneBusiness().getZonesByCity(em, su, idCountry, idCity);
    }

    @Override
    public ArrayList<ZoneRestriction> getTimeRestrictOfWeek( String idZone, String idCountry ) throws Exception
    {
	return new ZoneBusiness().getTimeRestrictOfWeek(em, idZone, idCountry);
    }

    @Override
    public ArrayList<Zone> getZonasCercanas( String lat, String lon ) throws Exception
    {
	return new ZoneBusiness().getZonasCercanas(em, lat, lon);
    }

    public void registerZone( Zone zone, ZoneLocation zoneLocation ) throws Exception
    {
	new ZoneBusiness().registerZone(em, zone, zoneLocation);
    }

    @Override
    public void registerZoneLocation( ZoneLocation zoneLocation ) throws Exception
    {
	new ZoneBusiness().registerZoneLocation(em, zoneLocation);

    }

    @Override
    public ArrayList<Zone> getZonesByIdClient( String idClient ) throws Exception
    {
	return new ZoneBusiness().getZonesByIdClient(em, idClient);
    }

    @Override
    public void updateZone( Zone zone ) throws Exception
    {
	new ZoneBusiness().updateZone(em, zone);

    }

    @Override
    public ArrayList<PriceRestrict> getPriceRestrictByZone( String idZone ) throws Exception
    {
	return new ZoneBusiness().getPriceRestrictByZone(em, idZone);

    }

    @Override
    public ArrayList<TimeRestrict> getTimeRestrictByZone( String idZone ) throws Exception
    {
	return new ZoneBusiness().getTimeRestrictByZone(em, idZone);
    }

    @Override
    public void registerTimeRestrict( TimeRestrict timeRestrict ) throws Exception
    {
	new ZoneBusiness().registerTimeRestrict(em, timeRestrict);

    }

    @Override
    public void registerPriceRestrict( PriceRestrict priceRestrict ) throws Exception
    {
	new ZoneBusiness().registerPriceRestrict(em, priceRestrict);

    }

    @Override
    public void modifyPriceRestrict( PriceRestrict priceRestrict ) throws Exception
    {
	new ZoneBusiness().modifyPriceRestrict(em, priceRestrict);

    }

    @Override
    public void deleteTimeRestrict( String idTimeRestrict )
    {
	new ZoneBusiness().deleteTimeRestrict(em, idTimeRestrict);

    }

    @Override
    public void deletePriceRestrict( String idPriceRestrict )
    {
	new ZoneBusiness().deletePriceRestrict(em, idPriceRestrict);
    }

    @Override
    public ArrayList<Zone> getZonesClient( SysUser sysUser ) throws Exception
    {
	return new ZoneBusiness().getZonesClient(em, sysUser);
    }

    @Override
    public Zone getZoneByActiveParking( String id ) throws Exception
    {
	return new ZoneBusiness().getZoneByActiveParking(em, id);
    }

    @Override
    public void setZoneState( String id, int state ) throws Exception
    {

	new ZoneBusiness().setZoneState(em, id, state);

    }

    @Override
    public ArrayList<Zone> getZonesByState( String idCountry, int state ) throws Exception
    {
	// TODO Auto-generated method stub
	return new ZoneBusiness().getZonesByState(em, idCountry, state);
    }

    @Override
    public void toggleActivate( String id, Integer activate )
    {

	new ZoneBusiness().toggleActivate(em, id, activate);

    }

    @Override
    public ArrayList<Zone> getZonesByActivation( String idCountry, int activeState ) throws Exception
    {
	// TODO Auto-generated method stub
	return new ZoneBusiness().getZonesByActivation(em, idCountry, activeState);
    }

    @Override
    public ArrayList<PriceRestrict> getPriceRestrictionList() throws Exception
    {
	// TODO Auto-generated method stub
	return new ZoneBusiness().getPriceRestrictionList(em);
    }

    @Override
    public List<RateType> getRateTypes() throws Exception
    {
	// TODO Auto-generated method stub
	return new ZoneBusiness().getRateTypes(em);
    }

    @Override
    public RateType getRateTypeById( int rateId ) throws Exception
    {
	// TODO Auto-generated method stub
	return new ZoneBusiness().getRateTypeById(em, rateId);
    }

    @Override
    public List<PriceRestrict> checkValidPriceRestrictItem( PriceRestrict pr ) throws Exception
    {
	// TODO Auto-generated method stub
	return new ZoneBusiness().checkValidPriceRestrictItem(em, pr);
    }

}
