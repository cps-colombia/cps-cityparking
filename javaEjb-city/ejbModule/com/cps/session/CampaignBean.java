package com.cps.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.CampaignBusiness;
import com.cps.entity.bean.Campaign;
import com.cps.entity.bean.CampaignLogReport;
import com.cps.remote.CampaignBeanRemote;
@Stateless(name = "CampaignBeanRemote")

public class CampaignBean implements CampaignBeanRemote {

	@PersistenceContext
	EntityManager em;
	@Override
	public boolean addCampaign(Campaign item) throws Exception {
		
		return new CampaignBusiness().addCampaign(em, item);
		
	}
	
	@Override
	public void deleteCampaign(Campaign item) throws Exception {
		
		new CampaignBusiness().deleteCampaign(em, item);
		
	}

	@Override
	public void deleteCampaign(int id) throws Exception {
		new CampaignBusiness().deleteCampaign(em, id);		
	}

	@Override
	public void modifyCampaign(Campaign item) throws Exception {
		new CampaignBusiness().modifyCampaign(em, item);		
	}

	@Override
	public List<Campaign> listCampaigns(int state) throws Exception {
		// TODO Auto-generated method stub
		return new CampaignBusiness().listCampaigns(em, state);
	}

	@Override
	public List<Campaign> listAllCampaigns() throws Exception {
		// TODO Auto-generated method stub
		return  new CampaignBusiness().listAllCampaigns(em);
	}

	@Override
	public List<CampaignLogReport> listCampaignsLog( String fechaIni,
			String fechaFin,  String email) throws Exception {
		// TODO Auto-generated method stub
		return new CampaignBusiness().listCampaignsLog(em, fechaIni, fechaFin, email);
	}

}
