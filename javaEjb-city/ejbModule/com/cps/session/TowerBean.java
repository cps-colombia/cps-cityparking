package com.cps.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.TowerBusiness;
import com.cps.remote.TowerBeanRemote;

@Stateless(name="TowerBeanRemote")
public class TowerBean implements TowerBeanRemote{

	@PersistenceContext
	EntityManager em;
	
	@Override
	public boolean isValidTowerID(String id) {
		return new TowerBusiness().isValidTowerID(em, id);
	}
	
}
