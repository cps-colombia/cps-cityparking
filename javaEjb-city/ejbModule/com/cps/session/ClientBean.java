package com.cps.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.ClientBusiness;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.ClientCity;
import com.cps.entity.bean.ClientCountry;
import com.cps.entity.bean.SysUser;
import com.cps.remote.ClientBeanRemote;

/**
 * Session Bean implementation class ClientBeanRemote
 */
@Stateless(name="ClientBeanRemote")
public class ClientBean implements ClientBeanRemote {

	@PersistenceContext
	EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ClientBean() {}

	@Override
	public List<Client> getListClientByCountry(String idCountry)
			throws Exception {
      return new ClientBusiness().getListClientByCountry(em, idCountry);
	}

	@Override
	public List<Client> getListClients()  {
		return new ClientBusiness().getListClients(em);
	}

	@Override
	public Client getClientById(String idClient) throws Exception {
		return new ClientBusiness().getClientById(em, idClient);
	}

	@Override
	public void registerClient(Client client) throws Exception {
		new ClientBusiness().registerClient(em, client);		
	}

	@Override
	public List<SysUser> getListUsersClient(String idClient) throws Exception {
		return new ClientBusiness().getListUsersClient(em, idClient);
	}

	@Override
	public Client getClient(String idClient) {
		return new ClientBusiness().getClient(em, idClient);
	}

	@Override
	public void updateClient(Client client) throws Exception {
		new ClientBusiness().updateClient(em, client);		
	}

	@Override
	public void registerClientCountry(ClientCountry clientCountry )
			throws Exception {
		new ClientBusiness().registerClientCountry(em, clientCountry);
		
	}

	@Override
	public void registerClientCity(ClientCity client) throws Exception {
		new ClientBusiness().registerClientCity(em, client);
		
	}

	@Override
	public int deleteClientCity(String cliendId, String countryId, String cityId)
			throws Exception {
		// TODO Auto-generated method stub
		return new ClientBusiness().deleteClientCity(em, cliendId, countryId, cityId);
	}

	@Override
	public int deleteClientCountry(String cliendId, String countryId)
			throws Exception {
		// TODO Auto-generated method stub
		return new ClientBusiness().deleteClientCountry(em, cliendId, countryId);
	}

}
