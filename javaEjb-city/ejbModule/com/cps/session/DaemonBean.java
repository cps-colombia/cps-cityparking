package com.cps.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.DaemonBusiness;
import com.cps.remote.DaemonBeanRemote;

@Stateless(name="DaemonBeanRemote")
public class DaemonBean implements DaemonBeanRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@Override
	public void update() throws Exception {
		new DaemonBusiness().update(em);		
	}

}
