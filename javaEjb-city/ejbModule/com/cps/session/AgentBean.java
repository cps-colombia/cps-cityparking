package com.cps.session;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.AgentBusiness;
import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentPushNotification;
import com.cps.entity.bean.AgentZone;
import com.cps.entity.bean.Subzone;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZonePlace;
import com.cps.remote.AgentBeanRemote;

@Stateless(name="AgentBeanRemote")
public class AgentBean implements AgentBeanRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public boolean isValidCredentials(String user, String pass)
			throws Exception {
		return new AgentBusiness().isValidCredentials(em, user, pass);
	}

	@Override
	public ArrayList<Zone> getZones(String idAgent) throws Exception {
		return new AgentBusiness().getZones(em, idAgent);
	}
	
	@Override
	public ArrayList<Subzone> getSubZones( String idAgent ) throws Exception
	{
		
		return new AgentBusiness().getSubZones( em, idAgent );
	}

	@Override
	public Agent getAgent(String username) throws Exception {
		return new AgentBusiness().getAgent(em, username);
	}

	@Override
	public void registerAgent(Agent agent) throws Exception {
		new AgentBusiness().registerAgent(em, agent);
	}

	@Override
	public ArrayList<Agent> getAgents() throws Exception {
		return new AgentBusiness().getAgents(em);
	}

	@Override
	public void registerAgentZone(AgentZone agentZone) throws Exception {
		new AgentBusiness().registerAgentZone(em, agentZone);
	}

	@Override
	public ArrayList<Agent> getAgentsZone(String idZone) throws Exception {
		return new AgentBusiness().getAgentsZone(em, idZone);
	}

	@Override
	public void updateAgent(Agent agent) throws Exception {
		new AgentBusiness().updateAgent(em, agent);
		
	}

	public ArrayList<ZonePlace> getAllZonesAndPlacesByAgent(String idAgent) throws Exception{
		return new AgentBusiness().getAllZonesAndPlacesByAgent(em, idAgent);
	}

	@Override
	public ArrayList<AgentPushNotification> getPendingPushNotifications()
			throws Exception {
		return new AgentBusiness().getPendingPushNotifications(em);
	}

	@Override
	public void setNotificationPushSended(String id) throws Exception {
		new AgentBusiness().setNotificationPushSended(em, id);
	}

	@Override
	public void deleteNotificationPush(String id) throws Exception {
		new AgentBusiness().deleteNotificationPush(em, id);
	}


	
}
