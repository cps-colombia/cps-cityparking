package com.cps.session;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cps.business.ParkingBusiness;
import com.cps.entity.bean.ActiveParking;
import com.cps.entity.bean.ParkingHistory;
import com.cps.entity.bean.ParkingHistoryC;
import com.cps.entity.bean.ParkingHistoryCity;
import com.cps.entity.bean.ReportParking;
import com.cps.entity.bean.SysUser;
import com.cps.remote.ParkingBeanRemote;

@Stateless(name="ParkingBeanRemote")
public class ParkingBean implements ParkingBeanRemote {
	
	@PersistenceContext
	EntityManager em;

	@Override
	public Object endParking(String plate, SysUser user) throws Exception {
		return new ParkingBusiness().endParking(em, plate, user);
	}

	@Override
	public Object startParking(int platform, String plate, String msisdn, String idCountry,
			String idPlace, SysUser user, int enableNotifSMS) throws Exception {
		return new ParkingBusiness().startParking(em, platform, plate, msisdn, idCountry, idPlace, user, enableNotifSMS);
	}

	@Override
	public String getTimeParking(String plate) throws Exception {
		return new ParkingBusiness().getTimeParking(em, plate);
	}

	@Override
	public ActiveParking getActiveParkingVehicle(String plate) throws Exception {
		return new ParkingBusiness().getActiveParkingVehicle(em, plate);
	}

	@Override
	public ArrayList<ActiveParking> getActiveParkingByZone(String idCountry,
			String idZone) throws Exception {
		return new ParkingBusiness().getActiveParkingByZone(em, idCountry, idZone);
	}

	@Override
	public ArrayList<ActiveParking> getActiveParkingByPlace(String placeFrom,
			String placeTo) throws Exception {
		return new ParkingBusiness().getActiveParkingByPlace(em, placeFrom, placeTo);
	}

	@Override
	public ArrayList<ActiveParking> getActiveParking() throws Exception {
		return new ParkingBusiness().getActiveParking(em);
	}

	@Override
	public ArrayList<ActiveParking> getActiveParkingByZone(String idZone)
			throws Exception {
		return new ParkingBusiness().getActiveParkingByZone(em, idZone);
	}


	@Override
	public ArrayList<ActiveParking> getActiveParkingBySubZone( Long idSubZone ) throws Exception
	{
		// TODO Auto-generated method stub
		return new ParkingBusiness().getActiveParkingBySubZone(em, idSubZone);
	}
	
	@Override
	public int countActiveParkingExceed(String idZone) throws Exception {
		return new ParkingBusiness().countActiveParkingExceed(em, idZone);
	}

	@Override
	public ArrayList<ActiveParking> getTimeExceedActiveParking(String idZone)
			throws Exception {
		return new ParkingBusiness().getTimeExceedActiveParking(em, idZone);
	}

	@Override
	public ArrayList<ParkingHistory> getParkingHistory(String zone)
			throws Exception {
		return new ParkingBusiness().getParkingHistory(em, zone);
	}

	@Override
	public ParkingHistory getParkingHistoryById(String idHistory)
			throws Exception {
		return new ParkingBusiness().getParkingHistoryById(em, idHistory);
	}

	@Override
	public ActiveParking getActiveParkingSMS(String zone,
			String cu, String idCountry) throws Exception {
		return new ParkingBusiness().getActiveParkingSMS(em, zone, cu, idCountry);
	}

	@Override
	public ArrayList<ActiveParking> getActiveParkingSMS(String cu)
			throws Exception {
		return new ParkingBusiness().getActiveParkingSMS(em, cu);
	}
	
	@Override
	public ArrayList<ParkingHistoryC> getHistoryParking(String idSysUser)
			throws Exception {
		return new ParkingBusiness().getHistoryParking(em,idSysUser);
	}
	
	@Override
	public ArrayList<ParkingHistoryCity> getHistoryCityParking(String idSysUser,  String startDate, String  endDate)
			throws Exception {
		return new ParkingBusiness().getHistoryParking(em,idSysUser,startDate, endDate, ParkingHistoryCity.class );
	}


	@Override
	public ArrayList<ReportParking> getReportParking(String idZone,
			String fechaIni, String fechaFin) throws Exception {
		return new ParkingBusiness().getReportParking(em, idZone, fechaIni, fechaFin);
	}

	@Override
	public ArrayList<ReportParking> getReportParking( String idZone, String fechaIni, String fechaFin, int platformId )
			throws Exception
	{
		// TODO Auto-generated method stub
		return  new ParkingBusiness().getReportParking(em, idZone, fechaIni, fechaFin, platformId);
	}
	@Override
	public ArrayList<ActiveParking> getActiveParkingByUser(String idUser)
			throws Exception {
		return new ParkingBusiness().getActiveParkingByUser(em, idUser);
	}

	@Override
	public ArrayList<ActiveParking> getAllActiveParkingByZone(String idZone) throws Exception {
		return new ParkingBusiness().getAllActiveParkingByZone(em, idZone);
	}
	
	@Override
	public ArrayList<ActiveParking> getActiveParkingByZoneAndPlaceRange(String idZone,String placeFrom,String placeTo) throws Exception {
		return new ParkingBusiness().getActiveParkingByZoneAndPlaceRange(em, idZone,  placeFrom,  placeTo);
	}

	@Override
	public ArrayList<ActiveParking> getAllActiveParkingByAgent(String idAgent)
			throws Exception {
		return new ParkingBusiness().getAllActiveParkingByAgent(em, idAgent);
	}

	@Override
	public ArrayList<ActiveParking> searchVehicleInAP(String idAgent,
			String plate) throws Exception {
		return new ParkingBusiness().searchVehicleInAP(em, idAgent, plate);
	}



		
}
