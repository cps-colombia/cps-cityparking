package com.cps.remote;

import java.util.List;

import javax.ejb.Remote;
import com.cps.entity.bean.Card;
import com.cps.entity.bean.SysUser;

@Remote
public interface CardBeanRemote {
	
	/**
	 * Obtiene la tarjeta a traves de el numero pin
	 * Generalmente este metodo es usado para transacciones con SMS
	 * @param
	 * 	PIN - Numero pin de la tarjeta prepago
	 * @return Card
	 * 
	 * */
	public Card getCard(String PIN)throws Exception;
	
	/**
	 * Valida si el parametro recibido es un PIN de tarjeta registrado
	 * Generalmente este metodo es usado para transacciones con SMS
	 * @param
	 * 	PIN - Numero pin de la tarjeta prepago
	 * @return boolean
	 * 
	 * */
	public boolean isCard(String PIN)throws Exception;
	
	/**
	 * Setea la tarjeta como usada
	 * @param
	 * 	cu - codigo unico
	 * 	PIN - Numero pin de la tarjeta prepago
	 * */
	public void setUsed(SysUser user, String PIN)throws Exception;
	
	/**
	 * 
	 * @param lisCards
	 * @throws Exception
	 */
	public void importCards(List<Card> lisCards)throws Exception;
}
