package com.cps.remote;

import javax.ejb.Remote;

import com.cps.entity.bean.SysUser;

@Remote
public interface SuggestionBeanRemote {

	public void addSuggestion(String suggestion, SysUser sysuser)throws Exception;
	
}