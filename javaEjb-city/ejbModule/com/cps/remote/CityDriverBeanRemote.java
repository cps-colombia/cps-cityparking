package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.CityDriver;
import com.cps.entity.bean.CityDriverStatus;
import com.cps.entity.bean.CityDriverOperator;

@Remote
public interface CityDriverBeanRemote {

	
	/**
	 * Devuelve todos los driver
	 * @param em
	 * @return ArrayList<CityDriver>
	 * */
	public ArrayList<CityDriver> getCityDrivers()throws Exception;
	
	/**
	 * Devuelve todos los operadores driver
	 * @param em, SysUserType
	 * @return ArrayList<SysUser>
	 * */
	public ArrayList<SysUser> getCityDriversOperators(Integer SysUserType)throws Exception;

	/**
	 * Devuelve todos los operadores por servicio driver
	 * @param em, CityDriverStatus
	 * @return ArrayList<CityDriverOperator>
	 * */
	public ArrayList<CityDriverOperator> currentCityDriversOperators(CityDriverStatus cityDriverStatus)throws Exception;
	
	/**
	 * Abtiene el estado actual en asociación con cityDriver
	 * @param em, cityDriver
	 * @return CityDriverStatus
	 * */
	public CityDriverStatus getCityDriverStatus(CityDriver cityDriver)throws Exception;
	
	/**
	 * Registra driver
	 * @param em, cityDriver
	 * @param cityDriver
	 */
	public CityDriver registerCityDriver(CityDriver cityDriver)throws Exception;
	
	/**
	 * Actualiza driver
	 * @param em, cityDriver
	 * @param cityDriver
	 */
	public void updateCityDriver(CityDriver cityDriver) throws Exception;

	/**
	 * Actualiza estado driver
	 * @param em, cityDriverStatus
	 * @param cityDriverStatus
	 */
	public void updateCityDriverStatus(CityDriverStatus cityDriverStatus) throws Exception;
	
	/**
	 * Actualiza operadores driver
	 * @param em, cityDriverOperator
	 * @param cityDriverOperator
	 */
	public void updateCityDriverOperator(CityDriverOperator cityDriverOperator) throws Exception;
	public void deleteCityDriverOperator(SysUser sysUser, CityDriverStatus cityDriverStatus) throws Exception;
	
	/**
	 * 
	 * 
	 * @param idCityDriver
	 * @return em, idCityDriver
	 * @throws Exception
	 */
	public ArrayList<CityDriver> getCityDriversById(String idCityDriver) throws Exception;

	public void setCityDriverState( String id, int state) throws Exception;
	public void toggleActivate( String id,Integer activate);

}
