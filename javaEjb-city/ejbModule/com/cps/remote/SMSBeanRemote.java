package com.cps.remote;

import javax.ejb.Remote;

@Remote
public interface SMSBeanRemote {

	/**
	 * Guarda el sms recibido
	 * @param
	 * 	min - numero movil sin prefijo
	 *  msisdn - numero movil con prefijo
	 *  operator - nombre del operador movil
	 *  option - parametro opcional
	 *  shortCode - codigo corto
	 *  text - contenido del mensaje
	 * */
	public void insertInbox(String min, String msisdn,
			String operator, String option, String shortCode, String text) throws Exception;
	
	/**
	 * Guarda el sms enviado
	 * @param
	 * 	min - numero movil sin prefijo
	 *  msisdn - numero movil con prefijo
	 *  operator - nombre del operador movil
	 *  option - parametro opcional
	 *  shortCode - codigo corto
	 *  text - contenido del mensaje
	 * */
	public void insertOutbox(String min, String msisdn,
			String operator, String option, String shortCode, String text) throws Exception;
	
}
