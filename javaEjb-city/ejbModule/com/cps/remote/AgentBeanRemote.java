package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentPushNotification;
import com.cps.entity.bean.AgentZone;
import com.cps.entity.bean.Subzone;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZonePlace;

@Remote
public interface AgentBeanRemote {

	/**
	 * Valida las credenciales de autenticacion del usuario
	 * @param
	 * 	user - usuario
	 *  pass - contrasena
	 * @return boolean
	 * */
	public boolean isValidCredentials(String user, String pass)throws Exception;
	
	/**
	 * Devuelve las zonas pertenecientes al agente
	 * @param
	 * 	idAgent - id del agente
	 * @return ArrayList<Zone>
	 * */
	public ArrayList<Zone> getZones(String idAgent) throws Exception;
	public ArrayList<Subzone> getSubZones( String idAgent) throws Exception;
	/**
	 * Devuelve el agente
	 * @param
	 * 	username - username del agente
	 * @return Agent
	 * */
	public Agent getAgent(String username) throws Exception;
	
	public void registerAgent(Agent agent)throws Exception;
	
	public ArrayList<Agent> getAgents()throws Exception;
	
	public void registerAgentZone(AgentZone agentZone)throws Exception;
	
	public ArrayList<Agent> getAgentsZone(String idZone)throws Exception;
	
	public void updateAgent(Agent agent)throws Exception;
	
	public ArrayList<ZonePlace> getAllZonesAndPlacesByAgent(String idAgent) throws Exception;
	
	public ArrayList<AgentPushNotification> getPendingPushNotifications()throws Exception;
	
	public void setNotificationPushSended(String id)throws Exception;
	
	public void deleteNotificationPush(String id)throws Exception;	
	
}
