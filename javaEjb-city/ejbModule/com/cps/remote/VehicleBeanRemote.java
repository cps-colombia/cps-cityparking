package com.cps.remote;
import java.math.BigInteger;
import java.util.ArrayList;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.SysUserVehicle;
import com.cps.entity.bean.Vehicle;
import com.cps.entity.bean.VehicleBrand;
import com.cps.entity.bean.VehicleType;

@Remote
public interface VehicleBeanRemote {

	/**
	 * Registra un vehiculo en el sistema con su placa y lo asocia a un usuario
	 * Generalmente este metodo es usado para transacciones desde la App movil o de la Web
	 * @param
	 * 	plate - Placa del vehiculo
	 * 	user - usuario
	 * @return int
	 * */
	public int registerVehicle(String plate, SysUser user)throws Exception;

    /**
	 * Registra un vehiculo en el sistema con su placa y lo asocia a un usuario con parametros adicionales
	 * Generalmente este metodo es usado para transacciones desde la App movil o de la Web
	 * @param
	 * 	plate - Placa del vehiculo
     *  type - ID Tipo de vehiculo como particular, publico, moto, cicla etc.
     *  brand - ID Marca como chevrolet, ford etc
     *  description - Descripción detalladas donde puede ir el modelo o color
	 * 	user - usuario
	 * @return int
	 * */
	public int registerVehicleFull(String plate, BigInteger type, BigInteger brand, String description, SysUser user)throws Exception;

	/**
	 * Obtiene información del vehiculo a traves de su placa
	 * @param
	 * 	plate - placa
	 * @return Vehicle
	 * */
	public Vehicle getVehicle(String plate, SysUser user)throws Exception;

	/**
	 * Obtiene los vehiculos asociados a un usuario
	 * @param
	 * 	user - usuario
	 * @return ArrayList<Vehicle>
	 * */
	public ArrayList<Vehicle> getVehicles(SysUser user)throws Exception;

	/**
	 * Obtiene información del vehiculo a traves de su numero movil
	 * @param
	 * 	msisdn - numero movil
	 * @return Vehicle
	 * */
	public Vehicle getVehicleByMobile(String msisdn)throws Exception;

	/**
	 * Compartir un vehiculo entre cuentas
	 * @param
	 * 	user - usuario al que le van a compartir
	 * 	plate - placa del vehiculo que sera compartido
	 * @return Vehicle
	 * */
	public int shareVehicle(SysUser user, String plate)throws Exception;

	/**
	 * Obtiene un listado de vehiculos asociados a un usuario.
	 * @param em
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Vehicle> getListVehicles(SysUser user) throws Exception;

	public void updateVehicle(Vehicle vehicle) throws Exception;

	public int deleteVehicle(String plate, SysUser user) throws Exception;

	public boolean isOwnerVehicle(SysUser user, String plate)throws Exception;

	public boolean existsVehicle(String plate) throws Exception;

	public ArrayList<SysUserVehicle> getSysUserVehicles(SysUser user) throws Exception;

	public ArrayList<VehicleBrand> getVehicleBrand()throws Exception;
	public ArrayList<VehicleType> getVehicleType()throws Exception;
	
	public VehicleType getVehicleTypeById( int vType ) throws Exception ;


}
