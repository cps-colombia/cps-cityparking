package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.cps.entity.bean.City;
import com.cps.entity.bean.Country;

@Remote
public interface CountryBeanRemote {

	/**
	 * Obtiene el prefijo de un pais
	 * @param
	 * 	idCountry - id del pais, Ej: CAN, COL, COS, USA, etc...
	 * @return String
	 * */
	public String getCountryPrefix(String idCountry)throws Exception;
	
	/**
	 * Obtiene la lista de paises donde esta CPS 	
	 * @return ArrayList<Country>
	 * */
	public ArrayList<Country> getCountrys()throws Exception;
	
	/**
	 * Obtiene el pais
	 * @param
	 * idCountry - id del pais
	 * @return Country
	 * */
	public Country getCountry(String idCountry)throws Exception;
	
	public ArrayList<City> getCities(String country) throws Exception;
	
	public City getCity(String idCity) throws Exception;
	
}
