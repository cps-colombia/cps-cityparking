package com.cps.remote;

import javax.ejb.Remote;

@Remote
public interface SocialNetworkRemote {

	public String getSNUrl(String name)throws Exception;
	
}
