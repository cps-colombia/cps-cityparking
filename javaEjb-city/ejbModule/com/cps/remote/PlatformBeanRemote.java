package com.cps.remote;

import java.util.List;

import javax.ejb.Remote;

import com.cps.entity.bean.Platform;

/**
 * 
 * @author juan carlos otero
 *
 */
@Remote
public interface PlatformBeanRemote
{

	/**
	 * 
	 * @param idplatform
	 * @return platform object
	 */
	public Platform getPlatForm( int idplatform);
	/**
	 * 
	 * @return platform list
	 */
	public  List<Platform> getPlatforms();
}
