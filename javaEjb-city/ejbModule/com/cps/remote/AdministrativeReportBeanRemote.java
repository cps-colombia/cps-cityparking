package com.cps.remote;

import java.util.Date;
import java.util.List;
import javax.ejb.Remote;
import com.cps.entity.bean.TransactionItem;

@Remote
public interface AdministrativeReportBeanRemote {
	public List<TransactionItem> getTransactionsByDateRange(Date startDate, Date endDate,String[] transactionTypeText) ;
	public List<TransactionItem> getUserTransactionsByDateRange(Date startDate, Date endDate,String[] transactionTypeText) ; 
}
