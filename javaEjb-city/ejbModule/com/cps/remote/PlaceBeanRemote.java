package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

import com.cps.entity.bean.Place;
import com.cps.entity.bean.PlaceSubzone;
import com.cps.entity.bean.Zone;

@Remote
public interface PlaceBeanRemote {

	public boolean isPlace(String place, String idCountry)throws Exception;
	
	public boolean isPlaceAvailable(String place, String idCountry)throws Exception;
	
	public Zone getPlaceZone(String place, String idCountry)throws Exception;
	
	public Place getPlace(String place, String idCountry)throws Exception;
	
	public void registerPlace(Place place)throws Exception;
	
	public ArrayList<Place> getPlacesZone(String idZone) throws Exception;
	
	public ArrayList<PlaceSubzone> getPlacesSubZone( Long idSubZone) throws Exception;
	
	public void deletePlace(Place place);
}
