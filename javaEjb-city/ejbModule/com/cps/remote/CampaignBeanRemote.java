package com.cps.remote;

import java.util.List;

import javax.ejb.Remote;




import com.cps.entity.bean.Campaign;
import com.cps.entity.bean.CampaignLogReport;
@Remote
public interface CampaignBeanRemote {
	
	  public boolean addCampaign(  Campaign item )throws Exception;
	  public void deleteCampaign(  Campaign item )throws Exception;

	  public void deleteCampaign(  int id )throws Exception;
	  
	  public void modifyCampaign( Campaign item )throws Exception;
	
	  public List<Campaign> listCampaigns( int state)throws Exception;
	  public List<CampaignLogReport> listCampaignsLog(  String fechaIni, String fechaFin,  String email)throws Exception;
	  public List<Campaign> listAllCampaigns( )throws Exception;


}
