package com.cps.remote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

import com.cps.entity.bean.PriceRestrict;
import com.cps.entity.bean.RateType;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.TimeRestrict;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZoneLocation;
import com.cps.entity.bean.ZoneRestriction;

@Remote
public interface ZoneBeanRemote {
	
	/**
	 * Obtiene el valor maximo de tiempo (Minutos) de una zona
	 * @param
	 * 	idZone - id de la zona
	 * @return int
	 * */
	public int getMaxMinutes(String idZone, String idCountry)throws Exception;
	
	/**
	 * Obtiene el valor de la hora de parqueo en la zona especificada
	 * @param
	 * 	idZone - id de la zona
	 * @return double
	 * */
	public BigDecimal getMinuteValue(String idZone, String idCountry)throws Exception;
	
	/**
	 * Verifica que la zona esta en su horario de atencion
	 * @param
	 * 	idZone - id de la zona
	 * @return boolean
	 * */
	public boolean isAvailableZone(String idZone, String idCountry)throws Exception;
	
	/**
	 * Devuelve la zona especifica correspondiente al id de la zona
	 * @param
	 * 	idZone - id de la zona
	 * @return Zone
	 * */
	public Zone getZone(String idZone, String idCountry)throws Exception;
	
	/**
	 * Devuelve las zonas pertenecientes a un pais especifico
	 * @param
	 * 	idCountry - id del pais
	 * @return ArrayList<Zone>
	 * */
	public ArrayList<Zone> getZones(String idCountry)throws Exception;
	
	/**
	 * Devuelve las zonas pertenecientes a un agente
	 * @param
	 * 	idagent - id del agente
	 * @return ArrayList<Zone>
	 * */
	public ArrayList<Zone> getAgentZones(String idagent)throws Exception;
	
	/**
	 * Devuelve la cantidad de espacios disponibles de una zona
	 * @param
	 * 	idZone - id de la zona
	 * @return int
	 * */
	public int getAvaliableSpaces(String idZone, String idCountry) throws Exception;
	
	/**
	 * Devuelve el minimo tiempo (Minutos) en que se desocupara un espacio en la zona
	 * @param
	 * 	idZone - id de la zona
	 * @return int
	 * */
	public int getMinsLeftAvaliableSpace(String idZone, String idCountry) throws Exception;
	
	/**
	 * Valida si el parametro recibido es una zona
	 * Generalmente este metodo es usado para transacciones con SMS
	 * @param
	 * 	zone - id de la zona a verificar
	 * @return boolean
	 * */	
	public boolean isZone(String zone, String idCountry)throws Exception;
	
	public void toggleActivate( String id,Integer activate);


	public ArrayList<Zone> getZonesByCity(SysUser su,String idCountry, String idCity) throws Exception;
	
	public ArrayList<ZoneRestriction> getTimeRestrictOfWeek(
			String idZone, String idCountry
			)throws Exception;
	
	public ArrayList<Zone> getZonasCercanas(String lat, String lon)throws Exception;
	
	/**
	 * Obtiene las zonas asocidas a un cliente
	 * @param sysUser
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Zone> getZonesClient(SysUser sysUser) throws Exception;
	
	/**
	 * Registra una nueva zona
	 * @param em
	 * @param zone
	 * @return
	 */
	public void registerZone(Zone zone, ZoneLocation zoneLocation)throws Exception;
	public void registerZoneLocation(  ZoneLocation zoneLocation)throws Exception;
	
	/**
	 * 
	 * 
	 * @param idClient
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Zone> getZonesByIdClient(String idClient) throws Exception;
	
	public void updateZone(Zone zone) throws Exception;
	
	public ArrayList<PriceRestrict> getPriceRestrictByZone(String idZone) throws Exception;
	
	
	ArrayList<PriceRestrict> getPriceRestrictionList() throws Exception;
	
	public ArrayList<TimeRestrict> getTimeRestrictByZone(String idZone) throws Exception;

	public void registerTimeRestrict(TimeRestrict timeRestrict)throws Exception;
	
	public void registerPriceRestrict(PriceRestrict priceRestrict)throws Exception;
	public void modifyPriceRestrict(  PriceRestrict priceRestrict ) throws Exception;

	
	public void deleteTimeRestrict(String idTimeRestrict);
	
	public void deletePriceRestrict(String idPriceRestrict);
	
	public Zone getZoneByActiveParking(String id)throws Exception;
	
	public void setZoneState( String id, int state) throws Exception ;
	
	public ArrayList<Zone> getZonesByState(String idCountry, int state ) throws Exception;
	
	public ArrayList<Zone> getZonesByActivation(String idCountry, int activeState ) throws Exception ;
	
	
	public List<RateType> getRateTypes( )  throws Exception ;
	
	public RateType getRateTypeById(  int rateId) throws Exception ;
	
	public List<PriceRestrict> checkValidPriceRestrictItem(PriceRestrict pr) throws Exception;


}
