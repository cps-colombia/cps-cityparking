package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.cps.entity.bean.Status;

@Remote
public interface StatusBeanRemote {

	public ArrayList<Status> getStatus()throws Exception;
}
