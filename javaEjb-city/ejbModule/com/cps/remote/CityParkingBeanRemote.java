package com.cps.remote;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.CityParkingTransactionUser;
import com.cps.entity.bean.PayItem;
import com.cps.entity.bean.PaymentResponse;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.Zone;
import com.cps.wservice.Cvalorubicaws;
import com.cps.wservice.payment.verification.ArrayOfPagosV3;
/**
 * 
 * @author Juan Carlos Otero
 *
 */
@Remote
public interface CityParkingBeanRemote {
	/*
    java.lang.String _valorWS_token = "7";
    java.lang.String _valorWS_identificadors = "7";
    com.cps.service.Cvalorws _valorWS__return = port.valorWS(_valorWS_token, _valorWS_identificadors);*/
	//public Cvalorws validateData(String zoneid, String token, String id);
	 /* java.lang.String _registrarAprobacion_identificadors = "7";
      java.lang.String _registrarAprobacion_numeroAprobacion1 = "24";
      java.lang.String _registrarAprobacion_resultadoOperacion1 = "4545";
      java.lang.String _registrarAprobacion_token1 = "7";
      int _registrarAprobacion__return = port.registrarAprobacion(*/
	//public int payParking( String zoneid,String id, String validationNumber, String operationResult, String token);
	/**
	 * checks if codes provided by parking place are valid
	 * @param parkingCode parking place code
	 * @param payCode is the current code to pay
	 * @return response with validated info.
	 */
	public PaymentResponse validateParking( String parkingCode ,String payCode);
	
	/**
	 * Pay parking previously validated
	 * @param user, current user whom is making the transaction
	 * @param value, transaction ammount
	 * @param id
	 * @param validationNumber
	 * @param operationResult
	 * @param token
	 * @param zone
	 * @param initparkingdate
	 * @param endparkingdate
	 * @param deviceId
	 * @return
	 */
	public int payParking(SysUser  user,double value,String id, String validationNumber,
			String operationResult, String token,String zone, String initparkingdate, String endparkingdate, int deviceId);

	
	public  Cvalorubicaws getParkingInfo( String id );
	 public Map< String,Cvalorubicaws> getParkingsInfo( List<Zone> zones);
	/**
	 * begins  a  recharge transaction with zona virtual 
	 * @param user current user for transaction
	 * @param ammount, 
	 * @return
	 */
	public String[]  initPayment( SysUser user ,double ammount );
	
	public PayItem checkPayment(String strIdPago) ;
	
//	public PayItem checkPayment(String strIdPago,  int intIdTienda, 
//			String strIdClave) ;
	
	public CityParkingTransaction  checkTransaction( String id );

	
	public void registryTransaction( CityParkingTransaction transaction );
	
	public boolean  updateTransactionStatus( String id, String status );
	
	public ArrayList<CityParkingTransaction> getTransactionsByUserId(long user_sys_id );
	
	public CityParkingTransactionUser getTransactionByUserId( long user_sys_id );


	public ArrayList<CityParkingTransaction> checkTransactionStatus();
	public ArrayList<CityParkingTransactionUser> getReportSales( String fechaIni, String fechaFin, String email) throws Exception;
	public ArrayList<CityParkingTransactionUser> getReportSales( String fechaIni, String fechaFin, String email,String method) throws Exception;
	public ArrayList<CityParkingTransactionUser> getReportSalesByUser(SysUser sUser,
			String fechaIni, String fechaFin) throws Exception;
	
}
