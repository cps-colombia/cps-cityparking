package com.cps.remote;

import java.util.List;

import javax.ejb.Remote;

import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentSubZone;
import com.cps.entity.bean.AgentSubZonePK;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.PlaceSubzone;
import com.cps.entity.bean.Subzone;

@Remote
public interface SubZoneBeanRemote {
	
	 public void create(Subzone subzone ) throws Exception;

	 public void edit(Subzone subzone) throws  Exception;
	 
	 public void destroy(Long id) throws Exception;
	 
	 public List<Subzone> findSubzones() throws Exception;
	 
	 public List<Subzone> findSubzones(int maxResults, int firstResult) throws Exception;
	 
	 public Subzone findSubzone(Long id) throws Exception;
	    
	 public int getSubzoneCount() throws Exception;
    
	 public List<Subzone> findAllByZone(String idZone) throws Exception;
	 
	 public void createPlaceSubzone(PlaceSubzone placeSubzone) throws Exception;
	 
	 public void associateAgentSubzone(AgentSubZone agentSubZone) throws Exception;
	 
	 public void disassociateAgentSubzone(AgentSubZonePK agentSubZone) throws Exception;
	 
	 public List<Agent> findAgentsBySubZone(String idSubZone) throws Exception;
	 
	 public List<Place> findPlacesAvailablesByZone(String idZone) throws Exception;
	 
	 public List<PlaceSubzone> findPlacesBySubZone(String idSubZone) throws Exception;
	 
	 public void destroyPlaceSubzone(PlaceSubzone placeSubzoneTem) throws Exception;
	    
}