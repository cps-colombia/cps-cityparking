package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;

//import com.cps.business.ParkingBusiness;
import com.cps.entity.bean.PayPalTransaction;

@Remote
public interface PayPalBeanRemote {

	public void registrarTransaccion(PayPalTransaction palTransaction);
	
	public PayPalTransaction getTransaccionById(String idTransaccion);
	
	 /**
	 * Retorna todas las ventas en un rango de fecha dado.
	 * @param fechaIni
	 * @param fechaFin
	 * @return
	 * @throws Exception
	 */
	public ArrayList<PayPalTransaction> getReportSales(String fechaIni,String fechaFin, String method)
			throws Exception;

	
}

	
	/*public ArrayList<ReportSales> getReportSales(String fechaIni,String fechaFin)
			throws Exception;
*/
	

