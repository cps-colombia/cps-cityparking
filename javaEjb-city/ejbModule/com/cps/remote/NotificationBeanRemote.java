package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.cps.entity.bean.Notification;
import com.cps.entity.bean.SysUser;

@Remote
public interface NotificationBeanRemote {
	
	/**
	 * Agrega una notificacion
	 * @param
	 * 	notification - objeto notificacion
	 * */
	public void addNotification(Notification notification)throws Exception;
	
	/**
	 * obtiene la lista de notificaciones
	 * 	@return ArrayList<Notification>
	 * */
	public ArrayList<Notification> getNotifications()throws Exception;
	
	/**
	 * Obtiene la lista de notificaciones discriminando por plataforma
	 * 	@param 
	 * 	platform - id de plataforma, Ej: Platform.WEB, Platform.MOBILE, Platform.SMS, ...
	 * 	@return ArrayList<Notification>
	 * */
	public ArrayList<Notification> getNotificationsByPlatform(int platform)throws Exception;
	
	/**
	 * Obtiene la lista de notificaciones discriminando por el cu
	 * 	@param 
	 * 	cu - codigo unico
	 * 	@return ArrayList<Notification>
	 * */
	public ArrayList<Notification> getUserNotifications(SysUser user)throws Exception;
	
	/**
	 * Establece como leida una notificacion
	 * 	@param
	 * 	id - id de la notificacion
	 * */
	public void setRead(String id)throws Exception;
	
}
