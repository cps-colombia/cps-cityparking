package com.cps.remote;

import java.util.ArrayList;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

import com.cps.entity.bean.ActiveParking;
import com.cps.entity.bean.ParkingHistory;
import com.cps.entity.bean.ParkingHistoryC;
import com.cps.entity.bean.ParkingHistoryCity;
import com.cps.entity.bean.ReportParking;
import com.cps.entity.bean.SysUser;

@Remote
public interface ParkingBeanRemote {

	/**
	 * termina un parqueo
	 * 
	 * @param plate_CU
	 *            - placa o codigo unico
	 * @return int
	 * */
	public Object endParking(String plate, SysUser user) throws Exception;

	/**
	 * parquea un vehiculo Generalmente este metodo es usado para transacciones
	 * con la App Movil
	 * 
	 * @param plate_CU
	 *            - placa o codigo unico idCountry - id del pais idPlace - id
	 *            del espacio user - usuario enableNotifSMS - desea o no recibir
	 *            notificaciones por sms
	 * @return int
	 * */
	public Object startParking(int platform, String plate, String msisdn,
			String idCountry, String idPlace, SysUser user, int enableNotifSMS)
			throws Exception;

	/**
	 * Devuelve el tiempo que lleva parqueado un vehiculo en formato hh:mm:ss
	 * 
	 * @param plate_CU
	 *            - placa o codigo unico
	 * @return String
	 * */
	public String getTimeParking(String plate) throws Exception;

	/**
	 * Devuelve una instancia de ActiveParking del vehiculo especificado
	 * 
	 * @param plate_CU
	 *            - placa o codigo unico
	 * @return ActiveParking
	 * */
	public ActiveParking getActiveParkingVehicle(String plate) throws Exception;

	/**
	 * 
	 */
	public ArrayList<ActiveParking> getActiveParkingByPlace(String placeFrom,
			String placeTo) throws Exception;

	/**
	 * Devuelve una lista de parqueos activos por zona
	 * 
	 * @param idCountry
	 *            - id del pais idZone - id de la zona
	 * @return ArrayList<ActiveParking>
	 * */
	public ArrayList<ActiveParking> getActiveParkingByZone(String idCountry,
			String idZone) throws Exception;

	public int countActiveParkingExceed(String idZone) throws Exception;

	public ArrayList<ActiveParking> getActiveParkingByZone(String idZone)
			throws Exception;

	public ArrayList<ActiveParking> getActiveParkingBySubZone(Long idSubZone)
			throws Exception;

	public ArrayList<ActiveParking> getActiveParkingByUser(String idUser)
			throws Exception;

	public ArrayList<ActiveParking> getTimeExceedActiveParking(String idZone)
			throws Exception;

	public ArrayList<ParkingHistory> getParkingHistory(String zone)
			throws Exception;

	public ParkingHistory getParkingHistoryById(String idHistory)
			throws Exception;

	/**
	 * Devuelve una lista de parqueo activo
	 * 
	 * @return plate_CU - placa o codigo unico
	 * */
	public ArrayList<ActiveParking> getActiveParking() throws Exception;

	public ActiveParking getActiveParkingSMS(String zone, String cu,
			String idCountry) throws Exception;

	public ArrayList<ActiveParking> getActiveParkingSMS(String cu)
			throws Exception;

	/**
	 * Devuelve una lista de parqueos activos por zona
	 * 
	 * @param idCountry
	 *            - id del pais idZone - id de la zona
	 * @return ArrayList<ActiveParking>
	 * */
	public ArrayList<ParkingHistoryC> getHistoryParking(String idSysUser)
			throws Exception;

	public ArrayList<ParkingHistoryCity> getHistoryCityParking(
			String idSysUser, String startDate, String endDate)
			throws Exception;

	public ArrayList<ReportParking> getReportParking(String idZone,
			String fechaIni, String fechaFin) throws Exception;

	public ArrayList<ReportParking> getReportParking(String idZone,
			String fechaIni, String fechaFin, int platformId) throws Exception;

	public ArrayList<ActiveParking> getAllActiveParkingByZone(String idZone)
			throws Exception;

	public ArrayList<ActiveParking> getActiveParkingByZoneAndPlaceRange(
			String idZone, String placeFrom, String placeTo) throws Exception;

	public ArrayList<ActiveParking> getAllActiveParkingByAgent(String idAgent)
			throws Exception;

	public ArrayList<ActiveParking> searchVehicleInAP(String idAgent,
			String plate) throws Exception;

}
