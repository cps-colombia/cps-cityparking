package com.cps.remote;

import javax.ejb.Remote;

@Remote
public interface TowerBeanRemote {

	public boolean isValidTowerID(String id);
	
}
