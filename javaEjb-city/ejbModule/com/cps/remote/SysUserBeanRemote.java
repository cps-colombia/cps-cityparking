package com.cps.remote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

import com.cps.entity.bean.MsisdnUser;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.TokenCheck;
import com.cps.entity.bean.UserTransactionReport;

@Remote
public interface SysUserBeanRemote {
	
	/**
	 * Obtiene los datos de usuario
	 * @param
	 * 	user - usuario
	 * @return SysUser
	 * */
	public SysUser getUser(String user)throws Exception;
	
	/**
	 * Obtiene los datos de usuario por el CU
	 * @param
	 * 	CU - codigo unico
	 * @return SysUser
	 * */
	public SysUser getUserByCU(String CU)throws Exception;
	
	/**
	 * Obtiene los datos de usuario
	 * Generalmente este metodo es usado para transacciones con SMS
	 * @param
	 * 	msisdn - numero movil
	 * @return SysUser
	 * */
	public SysUser getUserByMobile(String msisdn)throws Exception;
	
	/**
	 * Valida las credenciales de autenticacion del usuario
	 * @param
	 * 	user - usuario
	 *  pass - contrasena
	 * @return boolean
	 * */
	public boolean isValidCredentials(String user, String pass)throws Exception;
	
	/**
	 * Actualiza el password del usuario
	 * @param
	 * 	user - usuario
	 *  oldPass - antigua contrasena
	 *  pass - contrasena
	 * @return boolean
	 * */
	public int setPassword(String user, String oldPass, String pass)throws Exception;
	
	/**
	 * Actualiza los datos de usuario
	 * @param
	 * 	user - usuario
	 * @return int
	 * */
	public int updateUser(SysUser user)throws Exception;
	
	/**
	 * Actualiza los datos del usuario, menos el balance
	 * @param em
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int updateUserNoBalance(SysUser user) throws Exception;
	
	/**
	 * Registra el usuario
	 * @param
	 * 	user - usuario
	 * @return int
	 * */
	public SysUser registerUser(SysUser user)throws Exception;
	
	/**
	 * Obtiene el saldo del usuario a traves de sus datos de usuario
	 * @param
	 * 	user - usuario
	 * @return double
	 * */
	public BigDecimal getBalance(SysUser user)throws Exception;
	
	/**
	 * Recarga el saldo del usuario a traves de su usuario y tarjeta prepagada
	 * @param
	 * 	user - usuario 
	 * 	PIN - pin de la tarjeta prepago
	 * @return int
	 * */	
	public int reloadBalance( SysUser user, String PIN )throws Exception;
	
	/**
	 * Recarga el saldo del usuario a traves de su usuario y el valor de la recarga
	 * @param
	 * 	user - usuario 
	 * 	balance - valor de la recarga
	 * @return int
	 * */	
	public int reloadBalance( SysUser user, BigDecimal balance )throws Exception;
	
	
	public int setBalance( SysUser user, BigDecimal balance ) throws Exception;
	/**
	 * Valida si el parametro recibido es un CU registrado
	 * Generalmente este metodo es usado para transacciones con SMS
	 * @param
	 * 	cu - Codigo unico 
	 * @return boolean
	 * */	
	public boolean isCU(String cu)throws Exception;
	
	/**
	 * Registra un movil al usuario
	 * @param
	 * 	msisdn - Numero movil
	 * 	user - usuario 
	 * */	
	public void addMobile(String msisdn, SysUser user)throws Exception;
	
	/**
	 * Relaciona dos cuentas
	 * @param
	 * 	user - Usuario que extiende la invitacion
	 * 	userAdded - usuario invitado 
	 * */	
	public void addAccount(SysUser user, SysUser userAdded)throws Exception;
	
	/**
	 * Aceptar cuenta
	 * @param
	 * 	user - Usuario que extiende la invitacion
	 * 	userAdded - usuario invitado 
	 * */	
	public void acceptAccount(SysUser user, SysUser userAdded)throws Exception;
	
	/**
	 * Borra un numero del usuario
	 * @param
	 * 	msisdn - Numero movil
	 * */	
	public void removeMobile(String msisdn, SysUser user)throws Exception;
	
	/**
	 * Devuelve el listado de usuarios asociados
	 * @param
	 * 	user - usuario 
	 * */	
	public ArrayList<SysUser> getAccountUsersList(SysUser user)throws Exception;
	
	/**
	 * Devuelve el listado de moviles registrados por el usuario
	 * @param
	 * 	user - usuario 
	 * */	
	public ArrayList<MsisdnUser> getMobileList(SysUser user)throws Exception;
	
	public Object createUserBySMS(
			String plate, String msisdn, String PIN, String idCountry
			) throws Exception;
	
	public Object createTempUser(
			String plate, String PIN, String idCountry
			) throws Exception;
	
	public ArrayList<SysUser> getRequestAccountUsersList(SysUser user)
	throws Exception;
	
	public ArrayList<SysUser> getTempUsers()throws Exception;
	
	public ArrayList<SysUser> getTempUsersEmptyBalance()throws Exception;
	
	public SysUser getUserByEmail(String email)throws Exception;
	
	public int setPassword(SysUser user, String pass)throws Exception;
	
	public ArrayList<SysUser> getReportUsersList( String fechaIni, String fechaFin ) throws Exception;
	
	public boolean registryUserTransaction(String userOriginId, String userDestId, BigDecimal amount) throws Exception;
	 
	public List<UserTransactionReport> getTransactionForUser( String userId ,  String fechaIni, String fechaFin);

	public List<UserTransactionReport> getTransactionForAllUsers (   String fechaIni, String fechaFin)  throws Exception;
	
	public boolean tokenExist(  String token ) throws Exception;
	
	
	public void addToken( TokenCheck check  )  throws Exception;
	
	/**
	 * 
	 * @param criterial could be , login name, email, mobile number or CU
	 * @return filtered users
	 * @throws Exception
	 */
	public ArrayList<SysUser> getUsers(String criterial) throws Exception;
	
	public ArrayList<SysUser> getUsersAndExclude( String criterial, String [] excludes)
			throws Exception;
	
	
}
