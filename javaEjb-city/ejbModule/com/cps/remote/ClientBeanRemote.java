package com.cps.remote;
import java.util.List;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

import com.cps.entity.bean.Client;
import com.cps.entity.bean.ClientCity;
import com.cps.entity.bean.ClientCountry;
import com.cps.entity.bean.SysUser;

@Remote
public interface ClientBeanRemote{
	
	public Client getClient(String idClient);
	
	public List<Client> getListClientByCountry(String idCountry)throws Exception;

	public List<Client> getListClients();
	
	public Client getClientById(String idClient)throws Exception;
	
	public void registerClient(Client client)throws Exception;
	public void registerClientCountry( ClientCountry clientCountry)throws Exception;
	public void registerClientCity(ClientCity client)throws Exception;
	public int deleteClientCity(  String cliendId, String countryId ,String cityId )throws Exception;
	public int  deleteClientCountry( String cliendId, String countryId )throws Exception;
	public List<SysUser> getListUsersClient(String idClient)throws Exception;

	public void updateClient(Client client)throws Exception;

}
