package com.cps.remote;

import javax.ejb.Remote;

@Remote
public interface DaemonBeanRemote {

	/**
	 * Este metodo es invocado desde el thread de CPS, este metodo actualiza el estado de parqueo,
	 * descontando saldo y aumentando el numero de minutos de parqueo.
	 * */
	public void update()throws Exception;
	
}
