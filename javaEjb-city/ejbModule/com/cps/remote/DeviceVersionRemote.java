package com.cps.remote;

import javax.ejb.Remote;

@Remote
public interface DeviceVersionRemote {

	 public boolean checklastDeviceVersion(  Integer id, String currDeviceVersion );
}
