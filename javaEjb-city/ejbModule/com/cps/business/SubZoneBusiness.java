/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cps.business;

import java.util.List;

import javax.ejb.NoSuchEntityException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentSubZone;
import com.cps.entity.bean.AgentSubZonePK;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.PlaceSubzone;
import com.cps.entity.bean.Subzone;
import com.cps.entity.bean.Zone;

/**
 *
 * @author jmanzur
 */
public class SubZoneBusiness  {


    public void create(Subzone subzone , EntityManager em) throws Exception {
        try {

            Zone zona = subzone.getIdzone();
            if (zona != null) {
            	zona = em.getReference(zona.getClass(), zona.getIdzone());
                subzone.setIdzone(zona);
            }
            
            em.persist(subzone);
            em.flush();
            
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex); 
        } 
    }

    public void edit(Subzone subzone, EntityManager em) throws  Exception {
        try {
            
            subzone = em.merge(subzone);
           
        } catch (Exception ex) {
        	ex.printStackTrace();
           throw ex;
        }
    }

    public void destroy(Long id, EntityManager em) throws Exception {
        try {
            
            Subzone subzone;
            try {
                subzone = em.getReference(Subzone.class, id);
                subzone.getIdsubzone();
            } catch (EntityNotFoundException enfe) {
                throw new NoSuchEntityException("The subzone with id " + id + " no longer exists.");
            }
            
            em.remove(subzone);
            em.flush();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        	throw ex;
        } 
    }
    
    public void destroyPlaceSubzone(PlaceSubzone placeSubzoneTem, EntityManager em) throws Exception {
        try {
            
        	PlaceSubzone placeSubzone;
            try {
            	placeSubzone = em.getReference(PlaceSubzone.class, placeSubzoneTem.getIdplace());
            } catch (EntityNotFoundException enfe) {
                throw new NoSuchEntityException("The PlaceSubzone with id " + placeSubzoneTem.getIdplace() + " no longer exists.");
            }
            
            em.remove(placeSubzone);
            em.flush();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        	throw ex;
        } 
    }

    public List<Subzone> findSubzoneEntities(EntityManager em) throws Exception {
        return findSubzoneEntities(true, -1, -1, em);
    }

    public List<Subzone> findSubzoneEntities(int maxResults, int firstResult, EntityManager em) throws Exception {
        return findSubzoneEntities(false, maxResults, firstResult, em);
    }

    @SuppressWarnings("unchecked")
	private List<Subzone> findSubzoneEntities(boolean all, int maxResults, int firstResult , EntityManager em) throws Exception {
         
        try {
            Query q = em.createQuery("select object(o) from Subzone as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }  catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
    }

    public Subzone findSubzone(Long id, EntityManager  em) throws Exception {
        try {
            return em.find(Subzone.class, id);
        } catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
    }

    public int getSubzoneCount(EntityManager em) throws Exception {
        try {
            Query q = em.createQuery("select count(o) from Subzone as o");
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
    }
    
    
    @SuppressWarnings("unchecked")
	public List<Subzone> findAllByZone(String idZone, EntityManager em) throws Exception{
        try {
            Query q = em.createNamedQuery("Subzone.findAllByZone");
            q.setParameter("idzone", idZone);
            return q.getResultList();
        } catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
    	
    }
    
    @SuppressWarnings("unchecked")
	public List<Agent> findAgentsBySubZone(String idSubZone, EntityManager em) throws Exception{
        try {
            Query q = em.createNativeQuery("select a.*  from agent a join agent_subzone asz on(a.idagent = asz.idagent) where asz.idsubzone = :idSubZone", Agent.class);
            q.setParameter("idSubZone", idSubZone);
            return q.getResultList();
        } catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
    	
    }
    
    @SuppressWarnings("unchecked")
	public List<Place> findPlacesAvailablesByZone(String idZone, EntityManager em) throws Exception{
        try {
            Query q = em.createNativeQuery("select p.* from place p" +
            		"    left join place_subzone ps on(p.idplace = ps.idplace)" +
            		"    where p.idzone = :idZone" +
            		"    and ps.idplace is null", Place.class);
            q.setParameter("idZone", idZone);
            return q.getResultList();
        } catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
    	
    }
    
    @SuppressWarnings("unchecked")
	public List<PlaceSubzone> findPlacesBySubZone(String idSubZone, EntityManager em) throws Exception{
        try {
            Query q = em.createNativeQuery("select * from place_subzone where idsubzone = :idSubZone", PlaceSubzone.class);
            q.setParameter("idSubZone", idSubZone);
            return q.getResultList();
        } catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
    	
    }
    
    
    
    
    public void createPlaceSubzone(PlaceSubzone placeSubzone, EntityManager em) throws Exception {
         try {
            Subzone subzone = placeSubzone.getSubzone();
            if (subzone != null) {
                subzone = em.getReference(subzone.getClass(), subzone.getIdsubzone());
                placeSubzone.setSubzone(subzone);
            }
            em.persist(placeSubzone);
           
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } 
    }
    
    public void associateAgentSubzone(AgentSubZone agentSubZone, EntityManager em) throws Exception {
        try {
           
        	em.persist(agentSubZone);
        	em.flush();
          
       } catch (Exception ex) {
           ex.printStackTrace();
           throw ex;
       } 
   }
    
    public void disassociateAgentSubzone(AgentSubZonePK agentSubZonePk, EntityManager em) throws Exception {
        try {
        	AgentSubZone agentSubZone = em.find(AgentSubZone.class, agentSubZonePk);
        	em.remove(agentSubZone);
        	em.flush();
          
       } catch (Exception ex) {
           ex.printStackTrace();
           throw ex;
       } 
   } 
    
}
