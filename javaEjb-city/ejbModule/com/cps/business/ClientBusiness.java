package com.cps.business;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.Client;
import com.cps.entity.bean.ClientCity;
import com.cps.entity.bean.ClientCountry;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.SysUserVehicle;

public class ClientBusiness {
	
	public Client getClient(EntityManager em, String idClient){
		return em.find(Client.class, idClient);
	}
	
	@SuppressWarnings("unchecked")
	public List<Client> getListClientByCountry(EntityManager em, String idCountry)throws Exception{
		try{
			Query q = em.createQuery("select o from Client o where o.country.idcountry=:id_country and o.status=:status");
			q.setParameter("id_country", idCountry);
			q.setParameter("status", "");
			return q.getResultList();
		}catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Client> getListClients(EntityManager em){
		try{
			Query q = em.createQuery("select o from Client o where o.status=:status",Client.class);
			q.setParameter("status", "");
			List<Client> list = q.getResultList();
			return list;
		}catch (Exception e) {
			return null;
		}
	}
	
	
	public Client getClientById(EntityManager em, String idClient)throws Exception{
		return em.find(Client.class, idClient);
	}
	
	public void registerClient(EntityManager em, Client client)throws Exception{
		em.persist(client);
		em.flush();
	}
	
	public void registerClientCountry(EntityManager em,  ClientCountry clientCountry)throws Exception{
		em.persist(clientCountry);
		em.flush();
	}
	
	public int  deleteClientCountry(EntityManager em,  String cliendId, String countryId )throws Exception{
		
		int res = em.createNativeQuery(
					"DELETE FROM client_city  WHERE " +
					"idclient = '"+cliendId+"' AND " +
					"idcountry = "+countryId, 
					ClientCity.class
					).executeUpdate();
					
					
		 res = em.createNativeQuery(
				"DELETE FROM client_country  WHERE " +
				"idclient = '"+cliendId+"' AND " +
				"idcountry = "+countryId, 
				ClientCountry.class
				).executeUpdate();
		 
		 em.flush();
				return res;
	}
	
	
	
	public void registerClientCity(EntityManager em, ClientCity client)throws Exception{
		em.persist(client);
		em.flush();
	}
	
	public int  deleteClientCity(EntityManager em,  String cliendId, String countryId ,String cityId )throws Exception{
		
		int res = em.createNativeQuery(
					"DELETE FROM client_city  WHERE " +
					"idclient = '"+cliendId+"' AND " +
					"idcountry = '"+countryId +"' AND " +
							"idcity = '"+cityId+"'", 
					ClientCity.class
					).executeUpdate();
					
		em.flush();
		
				return res;
	}
	
	@SuppressWarnings("unchecked")
	public List<SysUser> getListUsersClient(EntityManager em, String idClient)throws Exception{
		try{
			Query q = em.createNativeQuery("select * from sys_user s " +
					"join sys_user_client suc on(s.idsys_user = suc.idsys_user)  " +
					"where suc.idclient = '"+idClient+"'",SysUser.class);
			return q.getResultList();
		}catch (Exception e) {
			return null;
		}
	}
	
	public void updateClient(EntityManager em, Client client)throws Exception{
		em.merge(client);
		em.flush();
	}
	
	
	
}
