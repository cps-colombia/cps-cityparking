package com.cps.business;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;

import com.cps.entity.bean.Notification;
import com.cps.entity.bean.SysUser;

public class NotificationBusiness {
	
	public void addNotification(EntityManager em, Notification notification) throws Exception {
		
		int count = 0;
		
		if( notification.getDateNotif() != null ){
			count = ((BigInteger)em.createNativeQuery(
					"SELECT count(idnotification) from notification "+
					"WHERE message = '"+notification.getMessage()+"' AND vehicle_plate = '"+notification.getVehiclePlate()+"' "+
					"AND platform_idplatform = "+notification.getPlatformIdplatform()+" AND "+
					"(HOUR(TIMEDIFF(now(),dateNotif))*60 + MINUTE(TIMEDIFF(now(),dateNotif))) = 0").getSingleResult()
					).intValue();
		}
		else{
			notification.setDateNotif(new Date());
		}
		
		if( count == 0 ){
			em.persist(notification);
			em.flush();
		}
		
	}
		
	@SuppressWarnings("unchecked")
	public ArrayList<Notification> getNotifications(EntityManager em) throws Exception {
		return new ArrayList<Notification>(em.createNativeQuery("SELECT * FROM notification WHERE isread = 0", Notification.class).getResultList());
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Notification> getNotificationsByPlatform(EntityManager em, int platform)
			throws Exception {
		return new ArrayList<Notification>(em.createNativeQuery("SELECT * FROM notification WHERE isread = 0 AND platform_idplatform = "+platform, Notification.class).getResultList());
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Notification> getUserNotifications(EntityManager em, SysUser user)
			throws Exception {		
		return new ArrayList<Notification>(em.createNativeQuery("SELECT * FROM notification WHERE isread = 0 AND idsys_user = "+user.getIdsysUser(), Notification.class).getResultList());
	}
	
	public void setRead(EntityManager em, String id) throws Exception {
		Notification notif = em.find(Notification.class, id);
		notif.setIsread(1);
		em.merge(notif);
		em.flush();
	}
	
}
