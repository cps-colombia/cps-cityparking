package com.cps.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.Campaign;
import com.cps.entity.bean.CampaignLogReport;

public class CampaignBusiness {

	public boolean addCampaign(EntityManager em, Campaign item) {
		try {

			String sstartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(item.getStartDate());
			String sendDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(item.getEndDate());
			
			/**
			 * 06:00 <=20:00
			 * 23:00 >=08:00
			 * 
			 */

			String sql = "SELECT  *,if(start_date <= '" + sendDate
					+ "' and  end_date >= ' " + sstartDate
					+ "' ,if(range_amount_start <= " + item.getRangeAmountEnd()
					+ " and  range_amount_end>=" + item.getRangeAmountStart()
					+ " ,true,false),false ) as valueif "
					+ "FROM cps_city.campaign having valueif=true";
			Query query = em.createNativeQuery(sql);
			@SuppressWarnings("unchecked")
			List<Object> objects = query.getResultList();

			if (objects.size() == 0) {
				em.merge(item);
				em.flush();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return false;
	}

	public void deleteCampaign(EntityManager em, Campaign item) {

		try {
			if (item != null) {
				em.remove(em.contains(item) ? item : em.merge(item));
				em.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void deleteCampaign(EntityManager em, int id) {

		Campaign item = em.find(Campaign.class, id);
		if (item != null) {
			em.remove(item);
			em.flush();
		}

	}

	public void modifyCampaign(EntityManager em, Campaign item) {
		em.merge(item);
		em.flush();

	}

	@SuppressWarnings("unchecked")
	public List<Campaign> listCampaigns(EntityManager em, int state) {

		List<Campaign> listItems = new ArrayList<Campaign>();
		Query q = em.createNativeQuery(
				"Select * from campaign where campaign_state like ?",
				Campaign.class);
		if (state == -1)
			q.setParameter(1, "%");
		else
			q.setParameter(1, state);

		listItems = q.getResultList();

		return listItems;

	}

	public List<CampaignLogReport> listCampaignsLog(EntityManager em,
			String fechaIni, String fechaFin, String email) {
		List<CampaignLogReport> listItems = new ArrayList<CampaignLogReport>();
		//14871 cu
		try
		{
		Query q = em.createQuery(
				"select clog from CampaignLogReport "
				+ "clog join fetch clog.sysUser "
				+ "where clog.logDate between '"+fechaIni +"' and '" +fechaFin+"'and "+
				"clog.sysUser.email like '"+email+"'",
				CampaignLogReport.class);
		/*if (state == -1)
			q.setParameter(1, "%");
		else
			q.setParameter(1, state);*/
		
		listItems = (List<CampaignLogReport>)q.getResultList();
		}catch( Exception e)
		{
			
		}
		
		

		return listItems;
	}

	@SuppressWarnings("unchecked")
	public List<Campaign> listAllCampaigns(EntityManager em) {

		List<Campaign> listItems = new ArrayList<Campaign>();
		Query q = em.createNativeQuery("Select * from campaign ",
				Campaign.class);

		listItems = q.getResultList();

		return listItems;

	}

}
