package com.cps.business;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.CityDriver;
import com.cps.entity.bean.CityDriverStatus;
import com.cps.entity.bean.CityDriverOperator;

public class CityDriverBusiness {
	
	/**
	 * Devuelve todos los driver registrados
	 * @param em
	 * @return ArrayList<CityDriver>
	 * */
	@SuppressWarnings("unchecked")
	public ArrayList<CityDriver> getCityDrivers(EntityManager em) throws Exception {
		Query q = em.createQuery(
				"SELECT cd FROM CityDriver cd JOIN FETCH cd.sysUser ORDER BY FIELD(cd.state, 1) DESC", 	
				CityDriver.class);		
		return (ArrayList<CityDriver>) q.getResultList();
	}	
	
	/**
	 * Devuelve todos los operadores driver
	 * @param em, SysUserType
	 * @return ArrayList<SysUser>
	 * */
	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getCityDriversOperators(EntityManager em, Integer SysUserType){
		Query q = em.createQuery(
				"SELECT cdo FROM SysUser cdo WHERE cdo.idsysUserType LIKE :SysUserType", 	
				SysUser.class);	
		q.setParameter("SysUserType", SysUserType);
		return (ArrayList<SysUser>) q.getResultList();		
	}
	
	/**
	 * Devuelve todos los operadores por servicio driver
	 * @param em, CityDriverStatus
	 * @return ArrayList<CityDriverOperator>
	 * */
	@SuppressWarnings("unchecked")
	public ArrayList<CityDriverOperator> currentCityDriversOperators(EntityManager em, CityDriverStatus cityDriverStatus)throws Exception {
		Query q = em.createQuery(
				"SELECT cdo FROM CityDriverOperator cdo WHERE cdo.idCityStatus LIKE :cityDriverStatus", 	
				CityDriverOperator.class);	
		q.setParameter("cityDriverStatus", cityDriverStatus);
		return (ArrayList<CityDriverOperator>) q.getResultList();	
	}
	
	/**
	 * @param em
	 * @param cityDriver
	 */
	public CityDriver registerCityDriver(EntityManager em, CityDriver cityDriver)throws Exception{
		try{
			em.persist(cityDriver);
			em.flush();
			return cityDriver;
		}catch (Exception e) {
			throw new Exception(e);
		}
	}
	public void updateCityDriver(EntityManager em, CityDriver cityDriver) throws Exception{
		try{
			em.merge(cityDriver);
			em.flush();
		}catch (Exception e) {
			throw new Exception(e);
		}			
	}
	/**
	 * @param em
	 * @param cityDriverStatus
	 */
	public void updateCityDriverStatus(EntityManager em, CityDriverStatus cityDriverStatus) throws Exception{
		try {
			em.merge(cityDriverStatus);
			em.flush();
		}catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * @param em
	 * @param cityDriverOperator
	 */
	public void updateCityDriverOperator(EntityManager em, CityDriverOperator cityDriverOperator) throws Exception{
		try {
			em.merge(cityDriverOperator);
			em.flush();
		}catch (Exception e) {
			throw new Exception(e);
		}			
	}
	public void deleteCityDriverOperator(EntityManager em, SysUser sysUser, CityDriverStatus cityDriverStatus) throws Exception{
		try {
			Query q = em.createQuery(
					"DELETE FROM CityDriverOperator cdo WHERE cdo.sysUser LIKE :sysUser and cdo.idCityStatus LIKE :cityDriverStatus");
			q.setParameter("sysUser", sysUser).setParameter("cityDriverStatus", cityDriverStatus).executeUpdate();
		}catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * @param em
	 * @param cityDriver
	 * @return
	 */
	public CityDriverStatus getCityDriverStatus(EntityManager em, CityDriver cityDriver) throws Exception {
		try {
			Query q = em.createQuery(
					"SELECT cds FROM CityDriverStatus cds WHERE cds.idCityDriver LIKE :cityDriver", 	
					CityDriverStatus.class);	
			q.setParameter("cityDriver", cityDriver);
			return (CityDriverStatus) q.getSingleResult();
	    } catch(NoResultException e) {
	        return null;
	    }
	}
	
	public void setCityDriverState(EntityManager em, String id, int state){
		Query query = em.createNativeQuery("UPDATE citydriver SET state="+ state +" WHERE idcityDriver='"+id+"'" , CityDriver.class);
		query.executeUpdate();			 
	}
	
	public void toggleActivate(EntityManager em, String id,Integer activate){	
		Integer act= (activate==1?0:1);
		Query query = em.createNativeQuery("UPDATE citydriver set status="+ act +", WHERE idcityDriver='"+id+"'" , CityDriver.class);
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<CityDriver> getCityDriversById(EntityManager em, String idCityDriver) throws Exception {
		Query q = em.createQuery(
				"SELECT cd FROM CityDriver cd join fetch cd.sysUser WHERE cd.idCityDriver like :idCityDriver",
				CityDriver.class);
		q.setParameter("idCityDriver", idCityDriver);
	
		return (ArrayList<CityDriver>) q.getResultList();
	}
}
