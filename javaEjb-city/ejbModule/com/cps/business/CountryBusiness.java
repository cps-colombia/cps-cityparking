package com.cps.business;

import java.util.ArrayList;

import javax.persistence.EntityManager;


import com.cps.entity.bean.City;
import com.cps.entity.bean.Country;


public class CountryBusiness {
	
	public String getCountryPrefix(EntityManager em, String idCountry) throws Exception {
		/*try{
			String prefix = getCountryFromCacheById(idCountry).getCountryPrefix();
			if( prefix != null ){
				return prefix;
			}
		}catch (NeedsRefreshException e) {
			loadCountrysOnCache(em);
		}*/	
		return getCountry(em, idCountry).getCountryPrefix();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Country> getCountrys(EntityManager em) throws Exception {
		/*try{
			return (ArrayList<Country>)CacheManager.getInstance().get(CacheConstants.COUNTRYS);
		}catch (NeedsRefreshException e) {			
			return loadCountrysOnCache(em);
		}*/
		try
		{
		return new ArrayList<Country>(em.createNativeQuery("SELECT * FROM country ORDER BY name", Country.class).getResultList());
		}catch(Exception e)
		{
			e.printStackTrace();
		throw e;
		}
	}
		
	
	@SuppressWarnings("unchecked")
	public ArrayList<City> getCities(EntityManager em, String country) throws Exception {		
		return new ArrayList<City>(em.createNativeQuery("SELECT * FROM city WHERE idcountry = '"+country+"' ORDER BY name", City.class).getResultList());
	}

	public Country getCountry(EntityManager em, String idCountry) throws Exception {
		/*try{
			return getCountryFromCacheById(idCountry);
		}catch (NeedsRefreshException e) {
			loadCountrysOnCache(em);
		}*/
		return em.find(Country.class, idCountry);
	}
	
	public City getCity(EntityManager em, String idCity) throws Exception {
		return em.find(City.class, idCity);
	}
	
}
