package com.cps.business;

import java.math.BigInteger;
import java.util.ArrayList;

import javax.persistence.EntityManager;

import com.cps.entity.bean.Country;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.PlaceSubzone;
import com.cps.entity.bean.Zone;
import com.cps.util.Util;

public class PlaceBusiness {
	
	public boolean isPlace(EntityManager em, String place, String idCountry){
		try{
			Country cn = new CountryBusiness().getCountry(em, idCountry);	
			String idPlace = !place.startsWith(cn.getCountryPrefix()) ? cn.getCountryPrefix().concat(place) : place;
			Place pl = em.find(Place.class, idPlace);
			return pl != null;
		}catch (Exception e) {}
		return false;
	}
	
	public boolean isPlaceAvailable(EntityManager em, String place, String idCountry){
		try{
			Country cn = new CountryBusiness().getCountry(em, idCountry);	
			String idPlace = !place.startsWith(cn.getCountryPrefix()) ? cn.getCountryPrefix().concat(place) : place;
			BigInteger mp = Util.getBigIntValue("SELECT COUNT(*) FROM active_parking WHERE place_idplace = '"+idPlace+"'", em);			
			return mp.intValue() == 0;
		}catch (Exception e) {}
		return false;
	}

	
	public Zone getPlaceZone(EntityManager em, String place, String idCountry){
		Place pl = getPlace(em, place, idCountry);
		if( pl != null ){
			return pl.getZone();
		}
		return null;
	}
	
	public Place getPlace(EntityManager em, String place, String idCountry){
		try{
			Country cn = new CountryBusiness().getCountry(em, idCountry);	
			String idPlace = !place.startsWith(cn.getCountryPrefix()) ? cn.getCountryPrefix().concat(place) : place;
			return em.find(Place.class, idPlace);
		}catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Place> getPlacesZone(EntityManager em, String idZone) throws Exception {
		return new ArrayList<Place>(
				em.createNativeQuery(
						"select * from place " +
						"where  idzone = '"+idZone+"'", 
						Place.class
						).getResultList()
				);
	}
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<PlaceSubzone> getPlacesSubZone(EntityManager em, Long idSubZone) throws Exception {
		return new ArrayList<PlaceSubzone>(
				em.createNativeQuery(
						"select * from place_subzone " +
						"where  idsubzone = '"+idSubZone+"'", 
						PlaceSubzone.class
						).getResultList()
				);
	}
	
	
	public void registerPlace(EntityManager em, Place place){
		em.persist(place);
	}
	
	public void deletePlace(EntityManager em, Place place){
		Place placeOld = em.find(Place.class, place.getIdplace());
		em.remove(placeOld);
	}
	
}
