package com.cps.business;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.CityDriver;
import com.cps.entity.bean.MsisdnUser;
import com.cps.entity.bean.SharedAccount;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.SysUserVehicle;
import com.cps.entity.bean.Vehicle;
import com.cps.entity.bean.VehicleBrand;
import com.cps.entity.bean.VehicleType;
import com.cps.util.CodeResponse;
import com.cps.util.Util;

public class VehicleBusiness {

	public boolean existsVehicle(EntityManager em, String plate) throws Exception {
		try{
			Vehicle vh = em.find(Vehicle.class, plate);
			return vh != null;
		}catch (Exception e) {
			return false;
		}
	}

	public int registerVehicle(EntityManager em, String plate, SysUser user) throws Exception {

		if( !existsVehicle(em, plate) ){

			Vehicle vh = new Vehicle();
			vh.setPlate(plate);
			vh.setState(1);
			vh.setDateReg(new Date());
			em.persist(vh);
			em.flush();

			SysUserVehicle suv = new SysUserVehicle();
			suv.setIdsysUser(user.getIdsysUser());
			suv.setVehiclePlate(plate);
			suv.setOwner(1);
			em.persist(suv);
			em.flush();

			return CodeResponse.REGISTER_VEHICLE_OK;
		}
		else{
			return CodeResponse.VEHICLE_ALREADY_REGISTRED;
		}

	}

	public int registerVehicleFull(EntityManager em, String plate, BigInteger type, BigInteger brand, String description, SysUser user) throws Exception {

		if( !existsVehicle(em, plate) ){

			Vehicle vh = new Vehicle();
			vh.setPlate(plate);
			vh.setState(1);
			if(type != null){
				vh.setType(type);
			}
			if(brand != null){
				vh.setBrand(brand);
			}
			if(description != null){
				vh.setDescription(description);
			}
			vh.setDateReg(new Date());
			em.persist(vh);
			em.flush();

			SysUserVehicle suv = new SysUserVehicle();
			suv.setIdsysUser(user.getIdsysUser());
			suv.setVehiclePlate(plate);
			suv.setOwner(1);
			em.persist(suv);
			em.flush();

			return CodeResponse.REGISTER_VEHICLE_OK;
		}
		else{
			return CodeResponse.VEHICLE_ALREADY_REGISTRED;
		}

	}

	public int deleteVehicle(EntityManager em, String plate, SysUser user) throws Exception {

		int res = em.createNativeQuery(
				"DELETE FROM sys_user_vehicle WHERE " +
				"vehicle_plate = '"+plate+"' AND " +
				"idsys_user = "+user.getIdsysUser()+ " AND owner = 1",
				SysUserVehicle.class
				).executeUpdate();

		if( res > 0 ){
			res = em.createNativeQuery(
					"DELETE FROM vehicle WHERE " +
					"plate = '"+plate+"'",
					SysUserVehicle.class).executeUpdate();
			if( res > 0 ){
				return CodeResponse.VEHICLE_DELETED_OK;
			}
		}

		return CodeResponse.VEHICLE_DELETED_FAIL;

	}

	public boolean isOwnerVehicle(EntityManager em, SysUser user, String plate)throws Exception{
		try{
			SysUserVehicle suv = (SysUserVehicle)em.createNativeQuery(
					"SELECT * FROM sys_user_vehicle WHERE idsys_user = "+user.getIdsysUser()+ " " +
							"AND vehicle_plate = '"+plate+"' and owner = 1",
					SysUserVehicle.class
					).getSingleResult();
			return suv != null;
		}catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public int shareVehicle(EntityManager em, SysUser user, String plate)throws Exception{

		SysUserVehicle suv = (SysUserVehicle)em.createNativeQuery(
				"SELECT * FROM sys_user_vehicle WHERE vehicle_plate = '"+plate+"' and owner = 1",
				SysUserVehicle.class
				).getSingleResult();

		SysUser us = new SysUserBusiness().getUser(em, suv.getIdsysUser());

		ArrayList<SharedAccount> sh = (ArrayList<SharedAccount>)em.createNativeQuery(
				"SELECT * FROM shared_account WHERE (idsys_user_acc1 = "+us.getIdsysUser()+" " +
						"AND idsys_user_acc2 = "+user.getIdsysUser()+") OR " +
						" (idsys_user_acc2 = "+us.getIdsysUser()+" AND idsys_user_acc1 = "+user.getIdsysUser()+")",
				SharedAccount.class
				).getResultList();

		if( sh != null && !sh.isEmpty()){

			ArrayList<SysUserVehicle> suv2 = (ArrayList<SysUserVehicle>)em.createNativeQuery(
					"SELECT * FROM sys_user_vehicle WHERE vehicle_plate = '"+plate+"' AND idsys_user = "+user.getIdsysUser(),
					SysUserVehicle.class
					).getResultList();

			if( suv2 != null && !suv2.isEmpty() ){
				return CodeResponse.VEHICLE_ALREADY_SHARED;
			}

			suv = new SysUserVehicle();
			suv.setIdsysUser(user.getIdsysUser());
			suv.setOwner(0);
			suv.setVehiclePlate(plate);
			em.persist(suv);

			return CodeResponse.VEHICLE_SHARED_SUCCESSFULLY;

		}

		return CodeResponse.ACCOUNT_NOT_SHARED;

	}

	public Vehicle getVehicle(EntityManager em, String plate, SysUser user) throws Exception {
		try{
			SysUserVehicle suv = (SysUserVehicle)em.createNativeQuery(
					"SELECT * FROM sys_user_vehicle WHERE vehicle_plate = '"+plate+"' AND idsys_user = "+user.getIdsysUser(),
					SysUserVehicle.class
					).getSingleResult();
			return (Vehicle)em.find(Vehicle.class, suv.getVehiclePlate());
		}catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Vehicle> getVehicles(EntityManager em, SysUser user) throws Exception {

		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

		ArrayList<SysUserVehicle> suv = new ArrayList<SysUserVehicle>(
				em.createNativeQuery(
						"SELECT * FROM sys_user_vehicle WHERE idsys_user = "+user.getIdsysUser()+" order by owner desc",
						SysUserVehicle.class
						).getResultList()
				);

		for( SysUserVehicle s : suv ){
			Vehicle vh = em.find(Vehicle.class, s.getVehiclePlate());
			if( vh.getState() == 1 ){
				vehicles.add(vh);
			}
		}

		return vehicles;

	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysUserVehicle> getSysUserVehicles(EntityManager em, SysUser user) throws Exception {
		return new ArrayList<SysUserVehicle>(
			em.createNativeQuery(
					"SELECT sys_user_vehicle.idsys_user_vehicle as idsys_user_vehicle, " +
					"sys_user_vehicle.vehicle_plate as vehicle_plate, sys_user_vehicle.idsys_user as idsys_user," +
					"sys_user_vehicle.owner as owner FROM sys_user_vehicle, vehicle " +
					"WHERE sys_user_vehicle.idsys_user = "+user.getIdsysUser()+" and sys_user_vehicle.vehicle_plate = vehicle.plate and " +
					"vehicle.state = 1 order by sys_user_vehicle.owner desc",
					SysUserVehicle.class
			).getResultList());
	}

	public Vehicle getVehicleByMobile(EntityManager em, String msisdn) throws Exception {

		MsisdnUser us = (MsisdnUser)em.createNativeQuery("SELECT * FROM msisdn_user WHERE msisdn = '"+msisdn+"'", MsisdnUser.class).getSingleResult();
		BigInteger vhs = Util.getBigIntValue("SELECT COUNT(*) FROM sys_user_vehicle WHERE idsys_user = "+us.getIdsysUser(), em);

		if( vhs.intValue() == 1 ){
			SysUserVehicle suv = (SysUserVehicle)em.createNativeQuery(
					"SELECT * FROM sys_user_vehicle WHERE idsys_user = "+us.getIdsysUser(),
					SysUserVehicle.class
					).getSingleResult();
			return (Vehicle)em.find(Vehicle.class, suv.getVehiclePlate());
		}

		return null;

	}

	@SuppressWarnings("unchecked")
	public ArrayList<Vehicle> getListVehicles(EntityManager em, SysUser user) throws Exception {
		return new ArrayList<Vehicle>(
				em.createNativeQuery(
						"select v.* from vehicle v " +
						"join sys_user_vehicle s on(v.cu = s.vehicle_cu) " +
						"where s.sys_user_idsys_user = "+user.getIdsysUser(),
						Vehicle.class
						).getResultList()
				);
	}

	public void updateVehicle(EntityManager em, Vehicle vehicle) throws Exception{
		try{
			em.merge(vehicle);
			em.flush();
		}catch (Exception e) {
			throw new Exception(e);
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<VehicleBrand> getVehicleBrand(EntityManager em) throws Exception {
		Query q = em.createQuery(
				"SELECT vb FROM VehicleBrand vb",
				VehicleBrand.class);
		return (ArrayList<VehicleBrand>) q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<VehicleType> getVehicleType(EntityManager em) throws Exception {
		Query q = em.createQuery(
				"SELECT vt FROM VehicleType vt",
				VehicleType.class);
		return (ArrayList<VehicleType>) q.getResultList();
	}
	
	public VehicleType getVehicleTypeById(EntityManager em, int vType ) throws Exception {
		Query q = em.createQuery(
				"SELECT vt FROM VehicleType vt where vt.vTypeId=:vtype",
				VehicleType.class);
		 q.setParameter("vtype", vType);
		return  (VehicleType) q.getSingleResult();
	}

}
