package com.cps.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.Card;
import com.cps.entity.bean.MsisdnUser;
import com.cps.entity.bean.SharedAccount;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.SysUserType;
import com.cps.entity.bean.TokenCheck;
import com.cps.entity.bean.UserTransaction;
import com.cps.entity.bean.UserTransactionReport;
import com.cps.util.CodeResponse;
import com.cps.util.Util;

public class SysUserBusiness {

	private String chars[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

	public String generatePassword() {
		int k = 0;
		String password = "";
		while (k < 6) {
			password += String
					.valueOf(chars[(int) (Math.random() * chars.length)]);
			k++;
		}
		return password;
	}

	private String generateCU(EntityManager em) {
		while (true) {
			String code = "";
			int len = 5;
			for (int i = 0; i < len; i++) {
				code += String.valueOf(
						chars[(int) (Math.random() * chars.length)])
						.toUpperCase();
			}
			Query query = em.createNativeQuery(
					"SELECT * FROM sys_user WHERE cu = '" + code + "'",
					SysUser.class);
			try {
				query.getSingleResult();
			} catch (javax.persistence.NoResultException e) {
				return code;
			}
		}
	}

	public String generateCU(EntityManager em, String plate) {
		while (true) {
			String code = plate;
			code += String.valueOf(chars[(int) (Math.random() * chars.length)])
					.toUpperCase();
			Query query = em.createNativeQuery(
					"SELECT * FROM sys_user WHERE cu = '" + code
							+ "' or login = '" + code + "'", SysUser.class);
			try {
				query.getSingleResult();
			} catch (javax.persistence.NoResultException e) {
				return code;
			}
		}
	}

	public SysUser getUser(EntityManager em, String user) throws Exception {
		try {
			SysUser us = (SysUser) em
					.createNativeQuery(
							"SELECT * FROM sys_user WHERE login = '" + user
									+ "' OR idsys_user = '" + user + "'",
							SysUser.class).getSingleResult();
			em.flush();
			return us;
		} catch (Exception e) {
			return null;
		}
	}

	public SysUser getUserByMobile(EntityManager em, String msisdn)
			throws Exception {
		try {

			try {
				SysUser sys = (SysUser) em.createNativeQuery(
						"SELECT * FROM sys_user WHERE favoriteMsisdn = '"
								+ msisdn + "'", SysUser.class)
						.getSingleResult();
				return sys;
			} catch (Exception e) {
			}

			return getUser(
					em,
					((MsisdnUser) em.createNativeQuery(
							"SELECT * FROM msisdn_user WHERE msisdn = '"
									+ msisdn + "'", MsisdnUser.class)
							.getSingleResult()).getIdsysUser());
		} catch (Exception e) {
			return null;
		}
	}

	public boolean isValidCredentials(EntityManager em, String user, String pass)
			throws Exception {
		try {

			SysUser us = null;

			try {
				us = (SysUser) em.createNativeQuery(
						"SELECT * FROM sys_user WHERE login = '" + user
								+ "' AND pass = '" + Util.SHA1(pass) + "'",
						SysUser.class).getSingleResult();
				if (us == null) {
					String fl = String.valueOf(pass.charAt(0));
					String np = fl.toLowerCase().concat(pass.substring(1));
					us = (SysUser) em.createNativeQuery(
							"SELECT * FROM sys_user WHERE login = '" + user
									+ "' AND pass = '" + Util.SHA1(np) + "'",
							SysUser.class).getSingleResult();
				}
			} catch (Exception e) {
				String fl = String.valueOf(pass.charAt(0));
				String np = fl.toLowerCase().concat(pass.substring(1));
				us = (SysUser) em.createNativeQuery(
						"SELECT * FROM sys_user WHERE login = '" + user
								+ "' AND pass = '" + Util.SHA1(np) + "'",
						SysUser.class).getSingleResult();
			}

			return us != null;

		} catch (Exception e) {

		}
		return false;
	}

	public int setPassword(EntityManager em, String user, String oldPass,
			String pass) throws Exception {

		try {
			SysUser us = (SysUser) em.createNativeQuery(
					"SELECT * FROM sys_user WHERE login = '" + user
							+ "' AND pass = '" + Util.SHA1(oldPass) + "'",
					SysUser.class).getSingleResult();
			us.setPass(Util.SHA1(pass));
			em.merge(us);
			em.flush();
			return CodeResponse.PASSWORD_SET_OK;
		} catch (Exception e) {
		}
		return CodeResponse.PASSWORD_SET_INVALID_PASS;
	}

	public int setPassword(EntityManager em, SysUser user, String pass)
			throws Exception {

		try {
			user.setPass(Util.SHA1(pass));
			em.merge(user);
			em.flush();
			return CodeResponse.PASSWORD_SET_OK;
		} catch (Exception e) {
		}
		return CodeResponse.PASSWORD_SET_INVALID_PASS;
	}

	public int updateUser(EntityManager em, SysUser user) throws Exception {
		em.merge(user);
		em.flush();
		return CodeResponse.UPDATE_USER_OK;
	}

	public Object createUserBySMS(EntityManager em, String plate,
			String msisdn, String PIN, String idCountry) throws Exception {

		VehicleBusiness vhb = new VehicleBusiness();

		if (vhb.existsVehicle(em, plate)) {
			return new Integer(CodeResponse.VEHICLE_ALREADY_REGISTRED);
		}

		SysUser sys = getUserByMobile(em, msisdn);

		if (sys == null) {

			Card card = new CardBusiness().getCard(em, PIN);

			if (card != null && !card.isUsed()) {
				String CU = generateCU(em);

				SysUser user = new SysUser();
				user.setCu(CU);
				user.setLogin(CU);
				user.setPass(generatePassword());
				user.setBalance(card.getPrice());
				user.setFavoriteMsisdn(msisdn);
				user.setCountryIdcountry(idCountry);
				user.setIdsysUserType(SysUserType.USER);
				em.persist(user);
				em.flush();

				int resp = vhb.registerVehicle(em, plate, user);
				if (resp == CodeResponse.VEHICLE_ALREADY_REGISTRED) {
					em.remove(user);
					em.flush();
					return new Integer(resp);
				}

				card.setUsed(1);
				card.setUser(user.getIdsysUser());
				card.setActivationDate(new Date());
				em.merge(card);
				em.flush();

				return user;
			} else {
				return new Integer(CodeResponse.INVALID_PIN);
			}

		} else {
			return new Integer(new VehicleBusiness().registerVehicle(em, plate,
					sys));
		}

	}

	public SysUser registerUser(EntityManager em, SysUser user)
			throws Exception {
		try {
			String CU = generateCU(em);
			user.setCu(CU);
			em.persist(user);
			em.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return user;
	}

	public BigDecimal getBalance(EntityManager em, SysUser user)
			throws Exception {
		return em.find(SysUser.class, user.getIdsysUser()).getBalance();
	}

	public int reloadBalance(EntityManager em, SysUser user, String PIN)
			throws Exception {

		try {

			Card card = new CardBusiness().getCard(em, PIN);
			SysUser parkingUser = getUser(em, card.getUser());

			boolean parkare = ((parkingUser != null) && "PARKARE"
					.equals(parkingUser.getOrigin()));

			if (card != null && card.isUsed() && parkare) {
				// em.createNativeQuery("UPDATE sys_user SET balance=balance+balanceparkimetro WHERE idsys_user =usuariomovil ");
				// em.createNativeQuery("UPDATE sys_user SET balance=0 WHERE idsys_user =usuarioparkimetro ");
				BigDecimal newbalance = user.getBalance().add(
						parkingUser.getBalance());
				user.setBalance(newbalance);
				em.merge(user);
				em.flush();
				parkingUser.setBalance(new BigDecimal(0));
				em.merge(parkingUser);
				em.flush();
				card.setUser(user.getIdsysUser());
				em.merge(card);
				em.flush();

				return CodeResponse.RELOAD_RESIDUE_OK;

			} else if (card != null && (!card.isUsed())) {
				int res = reloadBalance(em, user, card.getPrice());
				if (res == CodeResponse.RELOAD_RESIDUE_OK) {
					card.setUsed(1);
					card.setUser(user.getIdsysUser());
					card.setActivationDate(new Date());
					em.merge(card);
					em.flush();
				}
				return res;
			}

			return CodeResponse.INVALID_PIN;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int reloadBalance(EntityManager em, SysUser user, BigDecimal balance)
			throws Exception {
		user.setBalance(user.getBalance().add(balance));
		em.merge(user);
		em.flush();
		return CodeResponse.RELOAD_RESIDUE_OK;
	}

	public int setBalance(EntityManager em, SysUser user, BigDecimal balance)
			throws Exception {
		user.setBalance(balance);
		em.merge(user);
		em.flush();
		return CodeResponse.RELOAD_RESIDUE_OK;
	}

	public SysUser getUserByCU(EntityManager em, String CU) throws Exception {

		try {
			return (SysUser) em.createNativeQuery(
					"SELECT * FROM sys_user WHERE cu = '" + CU + "'",
					SysUser.class).getSingleResult();
		} catch (Exception e) {
		}
		return null;
	}

	public boolean isCU(EntityManager em, String cu) throws Exception {
		try {
			SysUser sysUser = (SysUser) em.createNativeQuery(
					"SELECT * FROM sys_user WHERE cu = '" + cu + "'",
					SysUser.class).getSingleResult();
			if (sysUser != null) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	public void addMobile(EntityManager em, String msisdn, SysUser user)
			throws Exception {
		MsisdnUser usermsisdn = new MsisdnUser();
		usermsisdn.setIdsysUser(user.getIdsysUser());
		usermsisdn.setMsisdn(msisdn);
		em.persist(usermsisdn);
		em.flush();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<MsisdnUser> getMobileList(EntityManager em, SysUser user)
			throws Exception {
		return new ArrayList<MsisdnUser>(em.createNativeQuery(
				"SELECT * FROM msisdn_user WHERE idsys_user = "
						+ user.getIdsysUser(), MsisdnUser.class)
				.getResultList());
	}

	public void removeMobile(EntityManager em, String msisdn, SysUser user)
			throws Exception {
		MsisdnUser msis = (MsisdnUser) em.createNativeQuery(
				"SELECT * FROM msisdn_user WHERE idsys_user = "
						+ user.getIdsysUser() + " " + "and msisdn = '" + msisdn
						+ "'", MsisdnUser.class).getSingleResult();
		em.remove(msis);
		em.flush();

	}

	public void addAccount(EntityManager em, SysUser user, SysUser userAdded)
			throws Exception {
		SharedAccount sh = new SharedAccount();
		sh.setIdsysUserAcc1(user.getIdsysUser());
		sh.setIdsysUserAcc2(userAdded.getIdsysUser());
		sh.setState(0);
		em.persist(sh);
		em.flush();
	}

	public void acceptAccount(EntityManager em, SysUser user, SysUser userAdded)
			throws Exception {
		SharedAccount sh = (SharedAccount) em.createNativeQuery(
				"SELECT * FROM shared_account WHERE " + "idsys_user_acc1 = "
						+ user.getIdsysUser() + " AND idsys_user_acc2 = "
						+ userAdded.getIdsysUser(), SharedAccount.class)
				.getSingleResult();
		sh.setState(1);
		em.merge(sh);
		em.flush();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getAccountUsersList(EntityManager em, SysUser user)
			throws Exception {

		ArrayList<SharedAccount> arr = new ArrayList<SharedAccount>(em
				.createNativeQuery(
						"SELECT * FROM shared_account WHERE "
								+ "idsys_user_acc1 = " + user.getIdsysUser()
								+ " OR idsys_user_acc2 = "
								+ user.getIdsysUser() + " AND state = 1",
						SharedAccount.class).getResultList());

		ArrayList<SysUser> sys = new ArrayList<SysUser>();

		for (SharedAccount sh : arr) {
			String id = user.getIdsysUser();
			SysUser us = em
					.find(SysUser.class,
							sh.getIdsysUserAcc1().equals(id) ? sh
									.getIdsysUserAcc2() : sh.getIdsysUserAcc1());
			sys.add(us);
		}

		return sys;

	}

	/**
	 * Listado de Usuarios q han hecho solicitudes de cuenta
	 * 
	 * @param em
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getRequestAccountUsersList(EntityManager em,
			SysUser user) throws Exception {

		ArrayList<SharedAccount> arr = new ArrayList<SharedAccount>(em
				.createNativeQuery(
						"SELECT * FROM shared_account WHERE "
								+ "idsys_user_acc2 = " + user.getIdsysUser()
								+ " AND state = 0", SharedAccount.class)
				.getResultList());

		ArrayList<SysUser> sys = new ArrayList<SysUser>();

		for (SharedAccount sh : arr) {
			SysUser us = em.find(SysUser.class, sh.getIdsysUserAcc1());
			sys.add(us);
		}

		return sys;

	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getTempUsers(EntityManager em) {
		try {
			ArrayList<SysUser> arr = new ArrayList<SysUser>(em
					.createNativeQuery(
							"SELECT * FROM sys_user WHERE "
									+ "idsys_user_type = "
									+ SysUserType.TEMP_USER, SysUser.class)
					.getResultList());
			return arr;
		} catch (Exception e) {
		}
		return new ArrayList<SysUser>();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getTempUsersEmptyBalance(EntityManager em) {
		try {
			ArrayList<SysUser> arr = new ArrayList<SysUser>(em
					.createNativeQuery(
							"SELECT * FROM sys_user WHERE "
									+ "idsys_user_type = "
									+ SysUserType.TEMP_USER
									+ " AND balance <= 0", SysUser.class)
					.getResultList());
			return arr;
		} catch (Exception e) {
		}
		return new ArrayList<SysUser>();
	}

	public Object createTempUser(EntityManager em, String plate, String PIN,
			String idCountry) throws Exception {

		SysUser sys = getUser(em, PIN);
		VehicleBusiness vhb = new VehicleBusiness();

		if (vhb.existsVehicle(em, plate)) {
			return new Integer(CodeResponse.VEHICLE_ALREADY_REGISTRED);
		}

		if (sys == null) {

			Card card = new CardBusiness().getCard(em, PIN);

			if (card != null && !card.isUsed()) {

				SysUser user = new SysUser();
				user.setCu(PIN);
				user.setLogin(PIN);
				user.setPass(generatePassword());
				user.setBalance(card.getPrice());
				user.setCountryIdcountry(idCountry);
				user.setIdsysUserType(SysUserType.TEMP_USER);
				em.persist(user);
				em.flush();

				int resp = vhb.registerVehicle(em, plate, user);
				if (resp == CodeResponse.VEHICLE_ALREADY_REGISTRED) {
					em.remove(user);
					em.flush();
					return new Integer(resp);
				}

				card.setUsed(1);
				card.setActivationDate(new Date());
				em.merge(card);
				em.flush();

				return user;

			} else {
				return new Integer(CodeResponse.INVALID_PIN);
			}

		}

		return null;

	}

	public SysUser getUserByEmail(EntityManager em, String email)
			throws Exception {
		try {
			SysUser sys = (SysUser) em.createNativeQuery(
					"SELECT * FROM sys_user WHERE email = '" + email + "'",
					SysUser.class).getSingleResult();
			return sys;
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getReportUsersList(EntityManager em,
			String fechaIni, String fechaFin) throws Exception {

		ArrayList<SysUser> arr = new ArrayList<SysUser>(em.createNativeQuery(
				"SELECT * FROM sys_user WHERE dateReg between '" + fechaIni
						+ "' and '" + fechaFin + "' order by dateReg desc",
				SysUser.class).getResultList());

		return arr;

	}

	public boolean registryUserTransaction(EntityManager em,
			String userOriginId, String userDestId, BigDecimal amount) {

		UserTransaction ut = new UserTransaction(userOriginId, userDestId,
				new Date(), amount);
		/*
		 * sh.setIdsysUserAcc1( user.getIdsysUser() ); sh.setIdsysUserAcc2(
		 * userAdded.getIdsysUser() ); sh.setState( 0 );
		 */
		em.persist(ut);
		em.flush();
		return true;

	}

	/**
	 * 
	 * @param em
	 * @param criterial
	 *            could be , login name, email, mobile number or CU
	 * @return filtered users
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getUsers(EntityManager em, String criterial)
			throws Exception {

		ArrayList<SysUser> arr = new ArrayList<SysUser>(em.createNativeQuery(
				"SELECT * FROM sys_user WHERE login like '%" + criterial
						+ "%' or " + " email like '%" + criterial + "%'  "
						+ "or favoriteMsisdn like '%" + criterial + "%'"
										+ " or name like '%" + criterial + "%'"

						+ "or CU like '%" + criterial + "%'", SysUser.class)
				.getResultList());

		return arr;

	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SysUser> getUsersAndExclude(EntityManager em, String criterial, String [] excludes)
			throws Exception {

		
		String sql= 	"SELECT * FROM sys_user WHERE (login like '%" + criterial
				+ "%' or " + " email like '%" + criterial + "%'  "
				+ "or favoriteMsisdn like '%" + criterial + "%'"
				+ " or name like '%" + criterial + "%'"
				+ "or CU like '%" + criterial + "%')";
		if(excludes!=null&&excludes.length>0)
		{
			
			  
			for(int i=0 ;i<excludes.length;i++ )
			{
				excludes[i]="'"+excludes[i]+"'";
			}
			
			String excFormatted=Arrays.toString(excludes).replace("[", "").replace("]", "");
			String inExclusion=" and login not in("+excFormatted+" )";
			sql=sql + inExclusion;
		}
		
		
		ArrayList<SysUser> arr = new ArrayList<SysUser>(em.createNativeQuery(
			sql, SysUser.class)
				.getResultList());

		return arr;

	}

	public List<UserTransactionReport> getTransactionForUser(EntityManager em,
			String userId, String fechaIni, String fechaFin) {
		List<UserTransactionReport> result = new ArrayList<UserTransactionReport>();

		try {
			String sql = "SELECT suo.cu as suocu,  suo.login as suologin,"
					+ " sud.cu as sudcu, sud.login as sudlogin, IF(ut.idsysuser_dest= "
					+ userId
					+ ", amount, amount * -1), transaction_date"
					+ " FROM cps_city.user_transaction ut "
					+ " inner join sys_user suo on ut.idsysuser_origen=suo.idsys_user "
					+ " inner join sys_user sud on ut.idsysuser_dest=sud.idsys_user"
					+ " where (ut.idsysuser_origen= " + userId
					+ " or ut.idsysuser_dest = " + userId
					+ ") and  transaction_date between '" + fechaIni
					+ "' and '" + fechaFin + "' order by transaction_date desc";

			System.out.println(sql);
			Query query = em.createNativeQuery(sql);
			List<Object> objects = query.getResultList();

			for (Object object : objects) {
				Object[] resultArray = (Object[]) object;
				result.add(new UserTransactionReport(resultArray[0].toString(),
						resultArray[1].toString(), resultArray[2].toString(),
						resultArray[3].toString(), (Date) (resultArray[5]),
						(BigDecimal) resultArray[4]));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

	public List<UserTransactionReport> getTransactionForAllUsers(
			EntityManager em, String fechaIni, String fechaFin) {

		List<UserTransactionReport> result = new ArrayList<UserTransactionReport>();

		try {
			String sql = "SELECT suo.cu as suocu,  suo.login as suologin,"
					+ " sud.cu as sudcu, sud.login as sudlogin, amount, transaction_date"
					+ " FROM cps_city.user_transaction ut "
					+ " inner join sys_user suo on ut.idsysuser_origen=suo.idsys_user "
					+ " inner join sys_user sud on ut.idsysuser_dest=sud.idsys_user"
					+ " where (transaction_date between '" + fechaIni
					+ "' and '" + fechaFin
					+ "') order by transaction_date desc";
			Query query = em.createNativeQuery(sql);
			List<Object> objects = query.getResultList();

			for (Object object : objects) {
				Object[] resultArray = (Object[]) object;
				result.add(new UserTransactionReport(resultArray[0].toString(),
						resultArray[1].toString(), resultArray[2].toString(),
						resultArray[3].toString(), (Date) (resultArray[5]),
						(BigDecimal) resultArray[4]));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public boolean tokenExist(EntityManager em, String token) {
		clearExpiredtokens(em);

		return em.find(TokenCheck.class, token) != null;
	}

	public void addToken(EntityManager em, TokenCheck check) {

		em.persist(check);
		em.flush();

	}

	private void clearExpiredtokens(EntityManager em) {
		long currtime = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
				.getTimeInMillis();

		Query q = em
				.createNativeQuery("DELETE from token_password_check where expiration_time<=  "
						+ currtime);
		q.executeUpdate();
	}
	/*
	 * public List<UserTransactionReport>
	 * getTransactionForAllUsers(EntityManager em, String fechaIni, String
	 * fechaFin ) {
	 * 
	 * List<UserTransactionReport> result = new
	 * ArrayList<UserTransactionReport>(); List<SysUser> users = (List<SysUser>
	 * ) em.createNativeQuery( "SELECT * FROM sys_user", SysUser.class );
	 * 
	 * for( SysUser user: users) { List<UserTransactionReport> transResult =
	 * getTransactionForUser(em,user.getIdsysUser(),fechaIni,fechaFin);
	 * result.addAll(transResult); }
	 * 
	 * 
	 * return result; }
	 */

	/*
	 * public List<UserTransactionReport> getTransactionForUser(EntityManager
	 * em, String userId ) { List<UserTransactionReport> result = new
	 * ArrayList<UserTransactionReport>();
	 * 
	 * try { String sql=
	 * "SELECT NEW com.cps.entity.bean.UserTransactionReport(ut.id.idsysUserOrigin,  suo.name,"
	 * +
	 * " ut.id.idsysUserDest, sud.name, ut.id.transactionDate,IF(ut.id.idsysUserDest= "
	 * +userId+", amount, amount * -1))" + " FROM UserTransaction ut" +
	 * " inner join ut.userOrigin suo" + " inner join ut.userDest sud " +
	 * " where (ut.id.idsysUserOrigin= "
	 * +userId+" or ut.id.idsysUserDest = "+userId+
	 * ") and ut.id.idsysUserOrigin=suo.idsysUser and ut.id.idsysUserDest=sud.idsysUser"
	 * ; Query query = em.createQuery(sql); result=
	 * (List<UserTransactionReport>)query.getResultList(); }catch(Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * return result;
	 * 
	 * }
	 */

	
	
	public static void main(String[] args) {
		
	    String []excludes={"'12345'","'78'","'juan'"};
	    String excFormatted=Arrays.toString(excludes).replace("[", "").replace("]", "");
		String inExclusion=" and login not in("+excFormatted+" )";
		System.out.println(inExclusion);

	}
}
