package com.cps.business;

import java.math.BigDecimal;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.Client;
import com.cps.entity.bean.Country;
import com.cps.entity.bean.PriceRestrict;
import com.cps.entity.bean.RateType;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.TimeRestrict;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZoneLocation;
import com.cps.entity.bean.ZoneRestriction;
import com.cps.session.ParkingWSManager;
import com.cps.util.Util;

public class ZoneBusiness
{

    @EJB
    private ParkingWSManager parkingws;

    public ArrayList<ZoneRestriction> getTimeRestrictOfWeek( EntityManager em, String idZone, String idCountry )
	                                                                                                        throws Exception
    {

	Zone zn = getZone(em, idZone, idCountry);
	ArrayList<ZoneRestriction> tmr = new ArrayList<ZoneRestriction>();

	try
	{

	    String tm = zn.getOpenTime().toString();
	    int start = Integer.parseInt(tm.substring(0, 2));

	    String tmf = zn.getCloseTime().toString();
	    int end = Integer.parseInt(tmf.substring(0, 2));

	    String tm2 = start + 1 < 10 ? "0" + (start + 1) + ":00:00" : (start + 1) + ":00:00";
	    int next = Integer.parseInt(tm2.substring(0, 2));

	    int days = 1;

	    int str = start;
	    int str2 = next;

	    while (days <= 7)
	    {

		TimeRestrict tr = null;
		PriceRestrict pr = null;

		try
		{
		    tr = (TimeRestrict) em.createNativeQuery("SELECT * FROM time_restrict where zone_idzone = " + "'"
			                                             + zn.getIdzone() + "' and day = " + days
			                                             + " and '" + tm + "' >= startTime and '" + tm2
			                                             + "' <= endTime", TimeRestrict.class)
			                  .getSingleResult();
		}
		catch (Exception e)
		{
		}

		try
		{
		    pr = (PriceRestrict) em.createNativeQuery("SELECT * FROM price_restrict where zone_idzone = " + "'"
			                                              + zn.getIdzone() + "' and day = " + days
			                                              + " and '" + tm + "' >= startTime and '" + tm2
			                                              + "' <= endTime", PriceRestrict.class)
			                   .getSingleResult();
		}
		catch (Exception e)
		{
		}

		DateFormat sdf = new SimpleDateFormat("hh:mm:ss");

		if (tr != null && pr != null)
		{
		    ZoneRestriction restrict = new ZoneRestriction();
		    restrict.setDay(days);
		    restrict.setStartTime(new Time(sdf.parse(tm).getTime()));
		    restrict.setEndTime(new Time(sdf.parse(tm2).getTime()));
		    restrict.setMaxMinutes(tr.getMaxMinutes());
		    restrict.setZoneIdzone(zn.getIdzone());
		    restrict.setPrice(pr.getMinuteValue());
		    restrict.setZoneName(zn.getName());
		    tmr.add(restrict);
		}
		else if (tr != null && pr == null)
		{
		    ZoneRestriction restrict = new ZoneRestriction();
		    restrict.setDay(days);
		    restrict.setStartTime(new Time(sdf.parse(tm).getTime()));
		    restrict.setEndTime(new Time(sdf.parse(tm2).getTime()));
		    restrict.setMaxMinutes(tr.getMaxMinutes());
		    restrict.setZoneIdzone(zn.getIdzone());
		    restrict.setPrice(zn.getMinuteValue());
		    restrict.setZoneName(zn.getName());
		    tmr.add(restrict);
		}
		else if (tr == null && pr != null)
		{
		    ZoneRestriction restrict = new ZoneRestriction();
		    restrict.setDay(days);
		    restrict.setStartTime(new Time(sdf.parse(tm).getTime()));
		    restrict.setEndTime(new Time(sdf.parse(tm2).getTime()));
		    restrict.setMaxMinutes(-1);
		    restrict.setZoneIdzone(zn.getIdzone());
		    restrict.setPrice(pr.getMinuteValue());
		    restrict.setZoneName(zn.getName());
		    tmr.add(restrict);
		}
		else
		{
		    ZoneRestriction restrict = new ZoneRestriction();
		    restrict.setDay(days);
		    restrict.setStartTime(new Time(sdf.parse(tm).getTime()));
		    restrict.setEndTime(new Time(sdf.parse(tm2).getTime()));
		    restrict.setMaxMinutes(-1);
		    restrict.setZoneIdzone(zn.getIdzone());
		    restrict.setPrice(zn.getMinuteValue());
		    restrict.setZoneName(zn.getName());
		    tmr.add(restrict);
		}

		if (str + 1 > end)
		{
		    str = start;
		    str2 = next;
		    days++;
		}
		else
		{
		    str++;
		}

		tm = str < 10 ? "0" + str + ":00:00" : str + ":00:00";
		tm2 = str2 < 10 ? "0" + (str + 1) + ":00:00" : (str2 + 1) + ":00:00";

	    }

	}
	catch (Exception e)
	{

	}

	return tmr;

    }

    public int getMaxMinutes( EntityManager em, String idZone, String idCountry ) throws Exception
    {

	Zone zn = getZone(em, idZone, idCountry);
	TimeRestrict tr = null;

	try
	{
	    tr = (TimeRestrict) em.createNativeQuery("SELECT * FROM time_restrict WHERE (day = "
		                                             + Util.getTodayFromGMT(zn.getGmt()) + " OR day = -1) "
		                                             + " AND zone_idzone = '" + zn.getIdzone() + "' AND '"
		                                             + Util.getStringNowFromGMT(zn.getGmt()) + "' "
		                                             + "BETWEEN startTime AND endTime", TimeRestrict.class)
		                  .getSingleResult();
	}
	catch (Exception e)
	{
	}

	if (tr != null)
	{
	    return tr.getMaxMinutes();
	}

	return -1;
    }

    public BigDecimal getMinuteValue( EntityManager em, String idZone, String idCountry ) throws Exception
    {

	Zone zn = getZone(em, idZone, idCountry);
	PriceRestrict pr = null;

	try
	{
	    pr = (PriceRestrict) em.createNativeQuery("SELECT * FROM price_restrict WHERE (day = "
		                                              + Util.getTodayFromGMT(zn.getGmt()) + " OR day = -1) "
		                                              + " AND zone_idzone = '" + zn.getIdzone() + "' AND '"
		                                              + Util.getStringNowFromGMT(zn.getGmt()) + "' "
		                                              + "BETWEEN startTime AND endTime AND ",
		                                      PriceRestrict.class).getSingleResult();
	}
	catch (Exception e)
	{
	}

	if (pr != null)
	{
	    return pr.getMinuteValue();
	}

	return zn.getMinuteValue();

    }

    public boolean isAvailableZone( EntityManager em, String idZone, String idCountry ) throws Exception
    {
	Zone zn = getZone(em, idZone, idCountry);
	return Util.gmtIsBetween(zn.getGmt(), zn.getOpenTime(), zn.getCloseTime());
	// TEMPORAL, MIENTRAS FERIA DE ARIZONA !!
	// return true;
    }

    public Zone getZone( EntityManager em, String idZone, String idCountry ) throws Exception
    {
	String zone = idZone;
	if (idCountry != null)
	{
	    Country cnt = new CountryBusiness().getCountry(em, idCountry);
	    zone = !idZone.startsWith(cnt.getCountryPrefix()) ? cnt.getCountryPrefix() + idZone : idZone;
	}
	return em.find(Zone.class, zone);
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Zone> getZones( EntityManager em, String idCountry ) throws Exception
    {
	Country cnt = new CountryBusiness().getCountry(em, idCountry);

	ArrayList<Zone> list = new ArrayList<Zone>();
	try
	{
	    list = new ArrayList<Zone>(em.createNativeQuery("SELECT * FROM zone WHERE  LOCATE('"
		                                                    + cnt.getCountryPrefix()
		                                                    + "', idzone) = 1 order by name", Zone.class)
		                         .getResultList());
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	return list;

    }

    @SuppressWarnings("unchecked")
    public ArrayList<Zone> getZonesByState( EntityManager em, String idCountry, int state ) throws Exception
    {
	Country cnt = new CountryBusiness().getCountry(em, idCountry);

	ArrayList<Zone> list = new ArrayList<Zone>();
	try
	{
	    list = new ArrayList<Zone>(em.createNativeQuery("SELECT * FROM zone WHERE online=" + state
		                                                    + " and  LOCATE('" + cnt.getCountryPrefix()
		                                                    + "', idzone) = 1 and active=1 order by name",
		                                            Zone.class).getResultList());
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	return list;

    }

    @SuppressWarnings("unchecked")
    public ArrayList<Zone> getZonesByActivation( EntityManager em, String idCountry, int activeState ) throws Exception
    {
	Country cnt = new CountryBusiness().getCountry(em, idCountry);

	ArrayList<Zone> list = new ArrayList<Zone>();
	try
	{
	    list = new ArrayList<Zone>(em.createNativeQuery("SELECT * FROM zone WHERE active=" + activeState
		                                                    + " and  LOCATE('" + cnt.getCountryPrefix()
		                                                    + "', idzone) = 1 order by name", Zone.class)
		                         .getResultList());
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	return list;

    }

    public void setZoneState( EntityManager em, String id, int state )
    {

	Query query = em.createNativeQuery("Update zone set online=" + state + " where idzone='" + id + "'", Zone.class);
	query.executeUpdate();

    }

    public void toggleActivate( EntityManager em, String id, Integer activate )
    {

	Integer act = (activate == 1 ? 0 : 1);
	Query query = em.createNativeQuery("update zone set active=" + act + ", online=0 where idzone='" + id + "'",
	                                   Zone.class);
	query.executeUpdate();

	if (act == 0 && ParkingWSManager.instance != null) ParkingWSManager.instance.evictFromCache(id);
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Zone> getZonesByCity( EntityManager em, SysUser su, String idCountry, String idCity )
	                                                                                                  throws Exception
    {

	Country cnt = new CountryBusiness().getCountry(em, idCountry);

	ArrayList<Zone> zones = new ArrayList<Zone>(em.createNativeQuery("SELECT * FROM zone WHERE LOCATE('"
	                                                                         + cnt.getCountryPrefix()
	                                                                         + "', idzone) = 1  order by name",
	                                                                 Zone.class).getResultList());

	ArrayList<Zone> filteredZones = new ArrayList<Zone>();

	for (Zone z : zones)
	{
	    // Client cl = new ClientBusiness().getClient(em,
	    // z.getClientIdclient());
	    // filteredZones.add(z);

	    ZoneLocation zlocation = z.getZoneLocation();

	    if (zlocation != null && zlocation.getIdcountry().toString().equals(idCountry)
		    && zlocation.getIdcity().toString().equals(idCity))
	    {
		filteredZones.add(z);
	    }

	    // if( cl.getCity().getIdcity().equals(idCity) ){
	    // filteredZones.add(z);
	    // }
	}

	return filteredZones;

    }

    public ArrayList<Zone> getAgentZones( EntityManager em, String idagent ) throws Exception
    {

	return null;
    }

    public int getAvaliableSpaces( EntityManager em, String idZone, String idCountry ) throws Exception
    {
	/*
	 * Zone zn = getZone(em, idZone, idCountry); ArrayList<ActiveParking>
	 * act = new ParkingBusiness().getActiveParkingByZone(em, idCountry,
	 * idZone);
	 */
	return 0;// zn.getPlaces() - act.size();
    }

    public int getMinsLeftAvaliableSpace( EntityManager em, String idZone, String idCountry ) throws Exception
    {
	// TODO Auto-generated method stub
	return 0;
    }

    public boolean isZone( EntityManager em, String zone, String idCountry ) throws Exception
    {
	Zone zn = getZone(em, zone, idCountry);
	return zn != null;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Zone> getZonasCercanas( EntityManager em, String lat, String lon ) throws Exception
    {
	try
	{

	    /*
	     * 
	     * "SELECT * FROM taxi HAVING (( 3959 * acos( cos( radians("+lat+
	     * ") ) * "+
	     * "cos( radians( latitude ) ) * cos( radians( longitude ) - radians("
	     * +lon+") ) + "+ "sin( radians("+lat+
	     * ") ) * sin( radians( latitude ) ) ) )) < 10 ORDER BY "+
	     * "((( 3959 * acos( cos( radians("
	     * +lat+") ) * cos( radians( latitude ) ) * "+
	     * "cos( radians( longitude ) - radians("+lon+") ) + "+
	     * "sin( radians("+lat+") ) * "+
	     * "sin( radians( latitude ) ) ) ))) LIMIT 0 , 10"
	     */

	    /*
	     * return new ArrayList<Zone>(em.createNativeQuery(
	     * "SELECT * FROM zone HAVING (( 3959 * acos( cos( radians("
	     * +lat+") ) * cos( radians( latitude ) ) " +
	     * "* cos( radians( longitude ) - radians("
	     * +lon+") ) + sin( radians("+lat+") ) * " +
	     * "sin( radians( latitude ) ) ) )) < 25 ORDER BY idzone LIMIT 0 , 20"
	     * , Zone.class ).getResultList());
	     */

	    return new ArrayList<Zone>(
		                       em.createNativeQuery("SELECT * FROM zone HAVING (( 3959 * acos( cos( radians("
		                                                    + lat
		                                                    + ") ) * "
		                                                    + "cos( radians( latitude ) ) * cos( radians( longitude ) - radians("
		                                                    + lon
		                                                    + ") ) + "
		                                                    + "sin( radians("
		                                                    + lat
		                                                    + ") ) * sin( radians( latitude ) ) ) )) < 10 ORDER BY "
		                                                    + "((( 3959 * acos( cos( radians(" + lat
		                                                    + ") ) * cos( radians( latitude ) ) * "
		                                                    + "cos( radians( longitude ) - radians(" + lon
		                                                    + ") ) + " + "sin( radians(" + lat + ") ) * "
		                                                    + "sin( radians( latitude ) ) ) ))) LIMIT 0 , 10",
		                                            Zone.class).getResultList());

	}
	catch (Exception e)
	{
	    return new ArrayList<Zone>();
	}
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Zone> getZonesClient( EntityManager em, SysUser sysUser ) throws Exception
    {
	return new ArrayList<Zone>(
	                           em.createNativeQuery("select * from zone z "
	                                                        + "join sys_user_client uc on(z.client_idclient = uc.idclient) "
	                                                        + "where uc.idsys_user = '" + sysUser.getIdsysUser()
	                                                        + "' order by z.name asc", Zone.class).getResultList());
    }

    /**
     * 
     * @param em
     * @param zone
     * @return
     */
    public void registerZone( EntityManager em, Zone zone, ZoneLocation zoneLocation ) throws Exception
    {
	try
	{

	    em.merge(zone);
	    em.flush();
	    registerZoneLocation(em, zoneLocation);

	}
	catch (Exception e)
	{
	    throw new Exception(e);
	}
    }

    /**
     * 
     * @param em
     * @param zone
     * @return
     */
    public void registerZoneLocation( EntityManager em, ZoneLocation zoneLocation ) throws Exception
    {
	try
	{

	    em.merge(zoneLocation);
	    em.flush();

	}
	catch (Exception e)
	{
	    throw new Exception(e);
	}
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Zone> getZonesByIdClient( EntityManager em, String idClient ) throws Exception
    {
	return new ArrayList<Zone>(em.createNativeQuery("select * from zone z " + "where  z.client_idclient = '"
	                                                        + idClient + "'  order by name ", Zone.class).getResultList());
    }

    public void updateZone( EntityManager em, Zone zone ) throws Exception
    {
	em.merge(zone);
	em.flush();
    }

    @SuppressWarnings("unchecked")
    public ArrayList<PriceRestrict> getPriceRestrictByZone( EntityManager em, String idZone ) throws Exception
    {
	return new ArrayList<PriceRestrict>(em.createQuery("select pr from PriceRestrict  pr "
	                                                                 + "where  pr.zone.idzone = '" + idZone + "'",
	                                                         PriceRestrict.class).getResultList());
    }

    @SuppressWarnings("unchecked")
    public ArrayList<PriceRestrict> getPriceRestrictionList( EntityManager em ) throws Exception
    {
	return new ArrayList<PriceRestrict>(em.createQuery("select pr from PriceRestrict  pr order by pr.zone.idzone, pr.rateType",
	                                                         PriceRestrict.class).getResultList());
    }

    @SuppressWarnings("unchecked")
    public ArrayList<TimeRestrict> getTimeRestrictByZone( EntityManager em, String idZone ) throws Exception
    {
	return new ArrayList<TimeRestrict>(em.createNativeQuery("select * from time_restrict "
	                                                                + "where  zone_idzone = '" + idZone + "'",
	                                                        TimeRestrict.class).getResultList());
    }

    public void registerTimeRestrict( EntityManager em, TimeRestrict timeRestrict ) throws Exception
    {
	try
	{
	    em.persist(timeRestrict);
	    em.flush();
	}
	catch (Exception e)
	{
	}
    }

    @SuppressWarnings("unchecked")
    public List<PriceRestrict> checkValidPriceRestrictItem( EntityManager em, PriceRestrict pr ) throws Exception
    {

	boolean updating=pr.getIdtimePriceRestriction()!=null;
	
	String sql = "Select pr from  PriceRestrict  pr " + "where ( pr.startTime <=:endt and pr.endTime >=:startt)"
	        + " and (pr.day=-1 or (pr.day=-2 and  (:day<>7 and :day<>6)) "
	        + "or pr.day=:day) and pr.vehicleType.vTypeId=:vtypeid" + " and pr.zone.idzone=:idzone and pr.rateType.idrateType=:ratet"
	        +" and  pr.minuteValue=:minv";

	
	
	String sqlnativo = "select pr.* from price_restrict  pr where " + "( pr.startTime <='" + pr.getEndTime()
	        + "' and pr.endTime >= '" + pr.getStartTime() + "')"
	        + " and (pr.day=-1 or (pr.day=-2 and  ("+pr.getDay()+"<>7 and "+pr.getDay()+"<>6)) or pr.day=" + pr.getDay() + ") "
	        + "and pr.vtype_id=" + pr.getVehicleType().getvTypeId() + " and pr.zone_idzone='"
	        + pr.getZone().getIdzone() + "' and pr.minuteValue="+ pr.getMinuteValue() + "' and pr.idrate_type="+ pr.getRateType().getIdrateType();
	
	if(updating)
	{
	    sql+=" and  pr.idtimePriceRestriction<>:prid";
	    
	    sqlnativo+=" and pr.idtimePriceRestriction <>"+pr.getIdtimePriceRestriction();
	}

	System.out.println(sqlnativo);
	Query q = em.createQuery(sql);
	q.setParameter("endt", pr.getEndTime()).setParameter("startt", pr.getStartTime())
	 .setParameter("day", pr.getDay()).setParameter("vtypeid", pr.getVehicleType().getvTypeId())
	 .setParameter("idzone", pr.getZone().getIdzone())
	 .setParameter("minv", pr.getZone().getMinuteValue())
	 .setParameter("ratet", pr.getRateType().getIdrateType());
	if(updating)
	    q.setParameter("prid", pr.getIdtimePriceRestriction());

	return q.getResultList();

    }

    public void registerPriceRestrict( EntityManager em, PriceRestrict priceRestrict ) throws Exception
    {
	try
	{
	    em.persist(priceRestrict);
	    em.flush();
	}
	catch (Exception e)
	{
	}
    }

    public void modifyPriceRestrict( EntityManager em, PriceRestrict priceRestrict ) throws Exception
    {
	try
	{
	    em.merge(priceRestrict);
	    em.flush();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    public void deleteTimeRestrict( EntityManager em, String idTimeRestrict )
    {
	TimeRestrict tr = em.find(TimeRestrict.class, idTimeRestrict);
	em.remove(tr);
	em.flush();
    }

    public void deletePriceRestrict( EntityManager em, String idPriceRestrict )
    {
	PriceRestrict pr = em.find(PriceRestrict.class, idPriceRestrict);
	em.remove(pr);
	em.flush();
    }

    public Zone getZoneByActiveParking( EntityManager em, String id ) throws Exception
    {
	return (Zone) em.createNativeQuery("select zone.*  from zone, active_parking where active_parking.idactiveParking = "
	                                           + id + " " + "and active_parking.zone_idzone = zone.idzone",
	                                   Zone.class).getSingleResult();
    }

    public List<RateType> getRateTypes( EntityManager em )
    {

	return em.createNamedQuery("RateType.findAll", RateType.class).getResultList();

    }

    public RateType getRateTypeById( EntityManager em, int rateId )
    {

	return em.createNamedQuery("RateType.findById", RateType.class).setParameter("idRtype", rateId)
	         .getSingleResult();

    }

}
