package com.cps.business;

import java.util.Date;

import javax.persistence.EntityManager;

import com.cps.entity.bean.SmsReceived;
import com.cps.entity.bean.SmsSent;

public class SMSBusiness {

	public void insertInbox(EntityManager em, String min, String msisdn, String operator,
			String option, String shortCode, String text) throws Exception {
		em.persist(new SmsReceived(new Date(), min, msisdn, operator, option, shortCode, text));
	}

	public void insertOutbox(EntityManager em, String min, String msisdn, String operator,
			String option, String shortCode, String text) throws Exception {
		em.persist(new SmsSent(new Date(), min, msisdn, operator, option, shortCode, text));
	}
	
}
