package com.cps.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.cps.entity.bean.Platform;

public class PlatformBusiness
{
	
	public Platform getPlatForm(EntityManager em, int idplatform )
	{

		return em.find(Platform.class, idplatform);
	}

	@SuppressWarnings("unchecked")
	public List<Platform> getPlatforms(EntityManager em)
	{
		// TODO Auto-generated method stub
		return  new ArrayList<Platform>(em.createNativeQuery("SELECT * FROM platform ORDER BY name", Platform.class).getResultList());
	}

}
