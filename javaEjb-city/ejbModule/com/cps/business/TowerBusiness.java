package com.cps.business;

import javax.persistence.EntityManager;

import com.cps.entity.bean.Tower;

public class TowerBusiness {

	public TowerBusiness(){
		
	}
	
	public boolean isValidTowerID(EntityManager em, String id){
		try{
			Tower tw = em.find(Tower.class, id);
			return tw != null;
		}catch (Exception e) {}
		return false;
	}
	
}
