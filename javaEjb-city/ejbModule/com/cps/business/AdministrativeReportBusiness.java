package com.cps.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.TransactionItem;

public class AdministrativeReportBusiness {

	public List<TransactionItem> getTransactionsByDateRange(EntityManager em,
			Date startDate, Date endDate, String[] transactionTypeText) {

		List<TransactionItem> result = new ArrayList<TransactionItem>();

		try {
			String sql = "SELECT '"+transactionTypeText[0]+"' AS estadistica, "
					+ "       Count(*)                    AS cantidadZP, "
					+ "       CASE "
					+ "         WHEN Sum(amount) IS NULL THEN 0 "
					+ "         ELSE Sum(amount) "
					+ "       end                         AS SUMA "
					+ "FROM   cityparking_transaction "
					+ "WHERE  ( date_transaction >= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(startDate)
					+ " 00:00:00'"
					+ "         AND date_transaction <= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(endDate)
					+ " 23:59:59') "
					+ "       AND state_transaction = 'Completed' "
					+ "       AND idzone IS NULL "
					+ "UNION "
					+ "SELECT '"+transactionTypeText[1]+"', "
					+ "       Count(*) AS cantidadPunto, "
					+ "       CASE "
					+ "         WHEN Sum(amount) IS NULL THEN 0 "
					+ "         ELSE Sum(amount) "
					+ "       end      AS SUMA "
					+ "FROM   cityparking_transaction "
					+ "WHERE  ( date_transaction >= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(startDate)
					+ " 00:00:00'"
					+ "         AND date_transaction <='"
					+ new SimpleDateFormat("YYYY-MM-dd").format(endDate)
					+ " 23:59:59' ) "
					+ "       AND state_transaction = 'Completed' "
					+ "       AND idzone IS NOT NULL "
					+ "UNION "
					+ "select '"+transactionTypeText[2]+"', COUNT(*) as cantidadTrns,SUM(amount)  as SUMA from user_transaction "
					+ "WHERE  ( "
					+ "              transaction_date >= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(startDate)
					+ " 00:00:00'  "
					+ "AND transaction_date<='"
					+ new SimpleDateFormat("YYYY-MM-dd").format(endDate)
					+ " 23:59:59' ) "
					+ " "
					+ "union select '"+transactionTypeText[3]+"', COUNT(*) as cantidadPagos, SUM(fee) as SUMA from parking_history "
					+ "  where (startTime >= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(startDate)
					+ " 00:00:00' "
					+ " "
					+ "AND startTime<= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(endDate)
					+ " 23:59:59') ";

			Query query = em.createNativeQuery(sql);
			List<Object> objects = query.getResultList();
			Integer totalQuantity = 0;
			Float totalAmount = 0F;
			for (Object object : objects) {
				Object[] resultArray = (Object[]) object;
				totalQuantity += Integer.valueOf(resultArray[1].toString());
				totalAmount += Float.valueOf(resultArray[2].toString());
				result.add(new TransactionItem(resultArray[0].toString(),
						Integer.valueOf(resultArray[1].toString()), Float
								.valueOf(resultArray[2].toString())));
			}
			result.add(new TransactionItem(transactionTypeText[4], totalQuantity, totalAmount));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public List<TransactionItem> getUserTransactionsByDateRange(
			EntityManager em, Date startDate, Date endDate,String[] transactionTypeText ) {
		// TODO Auto-generated method stub
		List<TransactionItem> result = new ArrayList<TransactionItem>();
		try {
			String sql = "select '"+transactionTypeText[0]+"' as tipotrans,  COUNT(*) as cantidad, 0 as valor from sys_user "
					+ "  where (dateReg >= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(startDate)
					+ " 00:00:00' AND "
					+ "dateReg<='"
					+ new SimpleDateFormat("YYYY-MM-dd").format(endDate)
					+ " 23:59:59') "
					+ "union "
					+ "select '"+transactionTypeText[1]+"' , COUNT(*) as cantidadRecZP, SUM(amount) as SUMA from cityparking_transaction "
					+ "  where (date_transaction >= '"
					+ new SimpleDateFormat("YYYY-MM-dd").format(startDate)
					+ " 00:00:00'  "
					+ "AND date_transaction<=' "
					+ new SimpleDateFormat("YYYY-MM-dd").format(endDate)
					+ " 23:59:59' ) "
					+ "  AND state_transaction='Rejected' AND idzone IS NULL";

			Query query = em.createNativeQuery(sql);
			List<Object> objects = query.getResultList();
			Integer totalQuantity = 0;
			Float totalAmount = 0F;
			for (Object object : objects) {
				Object[] resultArray = (Object[]) object;
				result.add(new TransactionItem(resultArray[0].toString(),
						Integer.valueOf(resultArray[1].toString()), Float
								.valueOf(resultArray[2].toString())));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
