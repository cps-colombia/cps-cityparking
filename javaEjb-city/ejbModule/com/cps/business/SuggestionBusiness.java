package com.cps.business;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.EntityManager;

import com.cps.entity.bean.Suggestion;
import com.cps.entity.bean.SysUser;

public class SuggestionBusiness {

	public void addSuggestion(EntityManager em, String suggestion, SysUser sysuser )throws Exception{
		Suggestion sgg = new Suggestion();
		sgg.setDate(new Date());
		sgg.setDescription(suggestion);
		sgg.setSysUserId(BigInteger.valueOf(Long.parseLong(sysuser.getIdsysUser())));
		em.persist(sgg);
		em.flush();
	}
	
}
