package com.cps.business;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import com.cps.entity.bean.PayPalTransaction;
import javax.persistence.Query;

public class PayPalBusiness {
	
	public void registrarTransaccion(EntityManager em, PayPalTransaction palTransaction){
		em.persist(palTransaction);
		em.flush();
	}
	
	public PayPalTransaction getTransaccionById(EntityManager em, String idTransaccion){
		return em.find(PayPalTransaction.class, idTransaccion);
	}
	
	/**
	 * Retorna todas las ventas en un rango de fecha dado.
	 * @param em
	 * @param fechaIni
	 * @param fechaFin
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<PayPalTransaction> getReportSales(EntityManager em, String fechaIni, String fechaFin, String method) throws Exception {
		try {
			Query q = em.createNativeQuery("{ CALL pro_reportsales(?,?,?) }", PayPalTransaction.class);
			q.setParameter(1, fechaIni);
			q.setParameter(2, fechaFin);
			q.setParameter(3, method);
			
			return (ArrayList<PayPalTransaction>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}

	
	/*	@SuppressWarnings("unchecked")
	public ArrayList<ReportSales> getReportSales(EntityManager em, String fechaIni, String fechaFin) throws Exception {
		try {
			Query q = em.createNativeQuery(
					"select * from paypal_transaction where (date_transaction between '"
							+ fechaIni + "' and '" + fechaFin + "')", ReportSales.class);
			return (ArrayList<ReportSales>) q.getResultList();
		} catch (Exception e) {
			return null;
		}
	}*/
	