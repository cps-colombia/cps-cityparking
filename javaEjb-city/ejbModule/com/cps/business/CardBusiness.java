package com.cps.business;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.cps.entity.bean.Card;
import com.cps.entity.bean.SysUser;
import com.cps.util.Util;

public class CardBusiness {

	public Card getCard(EntityManager em, String PIN)throws Exception{
		try{
			return em.find(Card.class, Util.SHA1(PIN));
		}catch (Exception e) {
			return null;
		}
	}
		
	public boolean isCard(EntityManager em, String PIN)throws Exception{
		try{
			Card c = em.find(Card.class, Util.SHA1(PIN));
			if( c != null ){
				return true;
			}
		}catch (Exception e) {}
		return false;
	}
	
	public void setUsed(EntityManager em, SysUser user, String PIN)throws Exception{
		Card c = em.find(Card.class, Util.SHA1(PIN));
		c.setUsed(1);
		c.setUser(user.getIdsysUser());
		c.setActivationDate(new Date());
		em.merge(c);
		em.flush();
	}
	
	public void registerCard(EntityManager em, Card card){
		em.persist(card);
	}
	
	public void importCards(EntityManager em, List<Card> lisCards)throws Exception{
		for (Card card : lisCards) {
		em.persist(card);
		}
	}
		
}
