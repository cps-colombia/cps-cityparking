package com.cps.business;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.ActiveParking;
import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentPushNotification;
import com.cps.entity.bean.AgentSubZone;
import com.cps.entity.bean.AgentZone;
import com.cps.entity.bean.Subzone;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZonePlace;
import com.cps.util.JSonUtil;
import com.cps.util.Util;

public class AgentBusiness {

	public boolean isValidCredentials(EntityManager em, String user, String pass)
			throws Exception {
		try {
			Agent ag = (Agent) em.createNativeQuery(
					"SELECT * FROM agent WHERE login = '" + user
							+ "' AND pass = '" + Util.SHA1(pass) + "'",
					Agent.class).getSingleResult();
			return ag != null;
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Zone> getZones(EntityManager em, String idAgent)
			throws Exception {
		ArrayList<AgentZone> az = new ArrayList<AgentZone>(em
				.createNativeQuery(
						"SELECT * FROM agent_zone WHERE idAgent = " + idAgent,
						AgentZone.class).getResultList());
		ArrayList<Zone> zones = new ArrayList<Zone>();
		for (AgentZone zn : az) {
			zones.add(em.find(Zone.class, zn.getId().getIdZone()));
		}
		return zones;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Subzone> getSubZones(EntityManager em, String idAgent)
			throws Exception {
		ArrayList<Subzone> zones = new ArrayList<Subzone>();
		try {
			ArrayList<AgentSubZone> az = new ArrayList<AgentSubZone>(em
					.createNativeQuery(
							"SELECT * FROM agent_subzone WHERE idAgent = "
									+ idAgent, AgentSubZone.class)
					.getResultList());

			for (AgentSubZone zn : az) {

				Subzone z = em.find(Subzone.class,
						Long.parseLong(zn.getId().getIdSubZone()));
				zones.add(z);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}

		return zones;
	}

	public Agent getAgent(EntityManager em, String username) throws Exception {
		return (Agent) em.createNativeQuery(
				"SELECT * FROM agent WHERE login = '" + username
						+ "' or idagent = '" + username + "'", Agent.class)
				.getSingleResult();
	}

	public void registerAgent(EntityManager em, Agent agent) throws Exception {
		em.persist(agent);
		em.flush();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Agent> getAgents(EntityManager em) throws Exception {
		ArrayList<Agent> az;
		Query q = em
				.createQuery("SELECT o FROM Agent o where o.status=:status");
		q.setParameter("status", "");
		az = (ArrayList<Agent>) q.getResultList();
		return az;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Agent> getAgentsZone(EntityManager em, String idZone)
			throws Exception {
		ArrayList<Agent> az = new ArrayList<Agent>(em.createNativeQuery(
				"SELECT * FROM agent a " + "inner join agent_zone az "
						+ "on(a.idagent = az.idAgent) " + "where az.idZone = '"
						+ idZone + "' " + "and a.status = ''", Agent.class)
				.getResultList());

		return az;
	}

	public void registerAgentZone(EntityManager em, AgentZone agentZone)
			throws Exception {
		em.persist(agentZone);
		em.flush();
	}

	public void updateAgent(EntityManager em, Agent agent) throws Exception {
		em.merge(agent);
		em.flush();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<ZonePlace> getAllZonesAndPlacesByAgent(EntityManager em,
			String idAgent) throws Exception {
		try {
			Query query = em.createNativeQuery(
					"call getAllZonesAndPlacesByAgent('" + idAgent + "')",
					ZonePlace.class);
			return new ArrayList<ZonePlace>(query.getResultList());
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<AgentPushNotification> getPendingPushNotifications(
			EntityManager em) throws Exception {
		return new ArrayList<AgentPushNotification>(em.createNativeQuery(
				"SELECT * FROM agent_push_notification WHERE isSended = 0",
				AgentPushNotification.class).getResultList());

	}

	public void setNotificationPushSended(EntityManager em, String id)
			throws Exception {
		AgentPushNotification push = em.find(AgentPushNotification.class, id);
		push.setIsSended(1);
		em.merge(push);
		em.flush();
	}

	public void deleteNotificationPush(EntityManager em, String id)
			throws Exception {
		AgentPushNotification push = em.find(AgentPushNotification.class, id);
		em.remove(push);
		em.flush();
	}

	public void addNotificationPush(EntityManager em, String message,
			String idagent) throws Exception {
		AgentPushNotification apn = new AgentPushNotification();
		apn.setDateNotif(new Date());
		apn.setIdagent(idagent);
		apn.setIsSended(0);
		apn.setMessage(message);
		em.persist(apn);
		em.flush();
	}

	public void addPushActiveParking(EntityManager em, String idZone)
			throws Exception {
		ParkingBusiness pb = new ParkingBusiness();
		ArrayList<Agent> agents = getAgentsZone(em, idZone);
		for (Agent ag : agents) {
			ArrayList<ActiveParking> acts = pb.getAllActiveParkingByAgent(em,
					ag.getIdagent());
			addNotificationPush(em, JSonUtil.getInstance().toJson(acts),
					ag.getIdagent());
		}
	}

	public void addPushToAgentsByZone(EntityManager em, String message,
			String idZone) throws Exception {
		ArrayList<Agent> agents = getAgentsZone(em, idZone);
		for (Agent ag : agents) {
			addNotificationPush(em, message, ag.getIdagent());
		}
	}

}