package com.cps.business;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.Notification;
import com.cps.entity.bean.Platform;
import com.cps.entity.bean.UpdateBean;
import com.cps.util.PropertiesUtil;
import com.cps.util.Util;

public class DaemonBusiness {
	
	@SuppressWarnings("unchecked")
	public void _update(EntityManager em)throws Exception{
		
		Query query = em.createNativeQuery("call updateAPN()", UpdateBean.class);		
		ArrayList<UpdateBean> notifiable = new ArrayList<UpdateBean>(query.getResultList());
		NotificationBusiness nb = new NotificationBusiness();
		
		for( UpdateBean ub : notifiable ){
			if( ub.getPlatformIdplatform() == Platform.SMS ){
				if( ub.getMaxMins() == -1 ){
					if( ub.getBalance() == 5 || ub.getBalance() == 1 || ub.getBalance() == 0 ){
						nb.addNotification(
								em, new Notification(
										0,
										PropertiesUtil.getInstance().getMessage(
												(ub.getCountryLang() != null ? ub.getCountryLang() : "en")+"."+ub.getBalance(), 
												new String[]{
														Util.getDateStringFromGMT(ub.getGmt())
												}
										)
										, ub.getMsisdn(), 
										Platform.SMS, 
										ub.getVehiclePlate(), ub.getIdSysUser(),
										new Date()
								)
						);			
						
						if( ub.getBalance() <= 0 ){
							new AgentBusiness().addPushToAgentsByZone(em, "3|"+ub.getZoneIdzone()+"|"+ub.getZoneName()+"|"+ub.getPlaceIdplace()+"|"+ub.getVehiclePlate()+"|"+ub.getCurrentMins()+"|"+ub.getMaxMins(), ub.getZoneIdzone());
						}
						
					}
				}else{
					
					int diff = (ub.getMaxMins()-ub.getCurrentMins());
					
					nb.addNotification(
							em, new Notification(
									0,
									PropertiesUtil.getInstance().getMessage(
											(ub.getCountryLang() != null ? ub.getCountryLang() : "en")+"."+diff, 
											new String[]{
													Util.getDateStringFromGMT(ub.getGmt())
											}
									)
									, ub.getMsisdn(), 
									Platform.SMS, 
									ub.getVehiclePlate(), ub.getIdSysUser(),
									new Date()
							)
					);							
					if( diff == 0 ){
						new AgentBusiness().addPushToAgentsByZone(em, "3|"+ub.getZoneIdzone()+"|"+ub.getZoneName()+"|"+ub.getPlaceIdplace()+"|"+ub.getVehiclePlate()+"|"+ub.getCurrentMins()+"|"+ub.getMaxMins(), ub.getZoneIdzone());
					}
				}
			}else{
				if( ub.getMaxMins() == -1 ){
					if( ub.getBalance() == 5 || ub.getBalance() == 1 || ub.getBalance() == 0 ){
						nb.addNotification(
								em, new Notification(
										0, String.valueOf(ub.getBalance()), ub.getMsisdn(), 
										ub.getPlatformIdplatform(), 
										ub.getVehiclePlate(), ub.getIdSysUser(),
										new Date()
								)
						);						
						
						if( ub.getEnableSMSNotif() == 1 ){
							nb.addNotification(
									em, new Notification(
											0,
											PropertiesUtil.getInstance().getMessage(
													(ub.getCountryLang() != null ? ub.getCountryLang() : "en")+"."+ub.getBalance(), 
													new String[]{
															Util.getDateStringFromGMT(ub.getGmt())
													}
											)
											, ub.getMsisdn(), 
											Platform.SMS, 
											ub.getVehiclePlate(), ub.getIdSysUser(),
											new Date()
									)
							);			
						}
						
						if( ub.getBalance() <= 0 ){
							new AgentBusiness().addPushToAgentsByZone(em, "3|"+ub.getZoneIdzone()+"|"+ub.getZoneName()+"|"+ub.getPlaceIdplace()+"|"+ub.getVehiclePlate()+"|"+ub.getCurrentMins()+"|"+ub.getMaxMins(), ub.getZoneIdzone());
						}
					}
				}else{
					
					int diff = (ub.getMaxMins()-ub.getCurrentMins());
					nb.addNotification(
							em, new Notification(
									0, String.valueOf(diff), ub.getMsisdn(), 
									ub.getPlatformIdplatform(), 
									ub.getVehiclePlate(), ub.getIdSysUser(),
									new Date()
							)
					);
					
					if( ub.getEnableSMSNotif() == 1 ){
						nb.addNotification(
								em, new Notification(
										0,
										PropertiesUtil.getInstance().getMessage(
												(ub.getCountryLang() != null ? ub.getCountryLang() : "en")+"."+diff, 
												new String[]{
														Util.getDateStringFromGMT(ub.getGmt())
												}
										)
										, ub.getMsisdn(), 
										Platform.SMS, 
										ub.getVehiclePlate(), ub.getIdSysUser(),
										new Date()
								)
						);			
					}
					
					if( diff == 0 ){
						new AgentBusiness().addPushToAgentsByZone(em, "3|"+ub.getZoneIdzone()+"|"+ub.getZoneName()+"|"+ub.getPlaceIdplace()+"|"+ub.getVehiclePlate()+"|"+ub.getCurrentMins()+"|"+ub.getMaxMins(), ub.getZoneIdzone());
					}
				}				
			}
		}
				
	}		
		
	public void update(EntityManager em) throws Exception {
//		if( CacheConstants.ENABLE_CACHE ){		
			_update(em);
//		}else{			
//			ArrayList<ActiveParking> arr = new ParkingBusiness().getActiveParking(em);			
//			for( ActiveParking act : arr ){				
//				Zone z = new ZoneBusiness().getZone(em, act.getZoneIdzone(), null);				
//				long t1 = act.getStartTime().getTime();
//				long t2 = Util.getDateFromGMT(z.getGmt()).getTime();				
//				int CM = (int)((t2-t1)/1000)/60;				
//				if( act.getCurrentMins() < CM ){				
//					SysUser user = new SysUserBusiness().getUser(em, act.getIdSysUser());						
//					BigDecimal bal = user.getBalance().subtract(act.getMinuteValue());									
//					act.setCurrentMins(CM);
//					user.setBalance(user.getBalance().subtract(act.getMinuteValue()));
//					em.merge(act);
//					em.merge(user);
//					em.flush();
//					
//					int diff = -1;
//					int suppMins = (bal.divide(act.getMinuteValue(), RoundingMode.HALF_UP)).intValue();
//					
//					if( act.getMaxMins() > 0 ){
//						diff = act.getMaxMins() - act.getCurrentMins();
//					}
//					else{
//						diff = suppMins; 
//					}
//					
//					String idCountry = user.getCountryIdcountry();
//					Country cnt = null;
//					
//					if( idCountry != null && !idCountry.equals("") ){
//						cnt = new CountryBusiness().getCountry(em, user.getCountryIdcountry());
//					}
//					
//					SysUser us = new SysUserBusiness().getUser(em, act.getIdSysUser());
//					Vehicle vh = new VehicleBusiness().getVehicle(em, act.getVehiclePlate(), us);
//					
//					switch( diff ){
//					
//					case 5:
//						
//						NotificationBusiness nb = new NotificationBusiness();
//						
//						if( act.getPlatformIdplatform() == Platform.MOBILE ){
//							
//							nb.addNotification(
//									em, 
//									new Notification(
//											0, "5", act.getMsisdn(), 
//											act.getPlatformIdplatform(), 
//											vh.getPlate(), us.getIdsysUser()
//									)
//							);
//												
//							
//						}
//						
//						if( act.getEnableSMSNotif() == 1 || act.getPlatformIdplatform() == Platform.SMS  ){
//																
//							nb.addNotification(
//									em, 
//									new Notification(
//											0,
//											PropertiesUtil.getInstance().getMessage(
//													(cnt != null ? cnt.getLang() : "en")+".fiveMinsNotif", 
//													new String[]{
//															Util.getDateStringFromGMT(z.getGmt())
//													}
//											)
//											, act.getMsisdn(), 
//											Platform.SMS, 
//											vh.getPlate(), us.getIdsysUser()
//									)
//							);					
//						}
//														
//						break;
//						
//					case 1:
//						
//						nb = new NotificationBusiness();
//										
//						if( act.getPlatformIdplatform() == Platform.MOBILE ){
//							
//							nb.addNotification(
//									em, 
//									new Notification(
//											0, "1", act.getMsisdn(), 
//											act.getPlatformIdplatform(), 
//											vh.getPlate(), us.getIdsysUser()
//									)
//							);
//							
//												
//						}
//						
//						if( act.getEnableSMSNotif() == 1 || act.getPlatformIdplatform() == Platform.SMS  ){
//																
//							nb.addNotification(
//									em, 
//									new Notification(
//											0,
//											PropertiesUtil.getInstance().getMessage(
//													(cnt != null ? cnt.getLang() : "en")+".oneMinsNotif", 
//													new String[]{
//															Util.getDateStringFromGMT(z.getGmt())
//													}
//											)
//											, act.getMsisdn(), 
//											Platform.SMS, 
//											vh.getPlate(), us.getIdsysUser()
//									)
//							);					
//						}
//										
//						break;
//						
//					case -1:
//						
//						nb = new NotificationBusiness();
//						
//						if( act.getPlatformIdplatform() == Platform.MOBILE ){
//							
//							nb.addNotification(
//									em, 
//									new Notification(
//											0, "0", act.getMsisdn(), 
//											act.getPlatformIdplatform(), 
//											vh.getPlate(), us.getIdsysUser()
//									)
//							);
//							
//							
//							
//						}
//						
//						if( act.getEnableSMSNotif() == 1 || act.getPlatformIdplatform() == Platform.SMS  ){
//																
//							nb.addNotification(
//									em, 
//									new Notification(
//											0,
//											PropertiesUtil.getInstance().getMessage(
//													(cnt != null ? cnt.getLang() : "en")+".zeroMinsNotif", 
//													new String[]{
//															Util.getDateStringFromGMT(z.getGmt())
//													}
//											)
//											, act.getMsisdn(), 
//											Platform.SMS, 
//											vh.getPlate(), us.getIdsysUser()
//									)
//							);					
//						}
//										
//						new AgentBusiness().addPushToAgentsByZone(em, "3|"+z.getIdzone()+"|"+z.getName()+"|"+act.getPlaceIdplace()+"|"+vh.getPlate()+"|"+act.getCurrentMins()+"|"+act.getMaxMins(), z.getIdzone());
//						
//						break;
//						
//					}
//						
//				}
//				
//			}//END FOR
//		
//		}
		
	}
	
}
