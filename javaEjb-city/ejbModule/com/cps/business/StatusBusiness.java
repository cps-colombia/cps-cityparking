package com.cps.business;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.Status;

public class StatusBusiness {

	@SuppressWarnings("unchecked")
	public ArrayList<Status> getStatus(EntityManager em) throws Exception {
		Query q = em.createQuery(
				"SELECT st FROM Status st", 
				Status.class);		
		return (ArrayList<Status>) q.getResultList();
	}
	
}
