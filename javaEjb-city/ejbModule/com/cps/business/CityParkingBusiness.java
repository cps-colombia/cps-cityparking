package com.cps.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;

import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.CityParkingTransactionUser;
import com.cps.entity.bean.PayPalTransaction;
import com.cps.entity.bean.SysUser;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class CityParkingBusiness {
	
	public void registryTransaccion(EntityManager em, CityParkingTransaction palTransaction){
		em.persist(palTransaction);
		em.flush();
	}
	
	public CityParkingTransaction getTransaccionById(EntityManager em, String idTransaccion){
		return em.find(CityParkingTransaction.class, idTransaccion);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean  updateTransaccionById(EntityManager em, String idTransaccion, String status){
		
	
		CityParkingTransaction tr =getTransaccionById(em,idTransaccion);
		if(tr==null) return false;
		tr.setStateTransaction(status);
		try
		{
			em.merge(tr);
			em.flush();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		 return true;
	}
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<CityParkingTransaction> getTransactionsByUserId(EntityManager em, long user_sys_id )
	{
		ArrayList<CityParkingTransaction>   transactions = new ArrayList<CityParkingTransaction>();
		
		Query q =  em.createNativeQuery("SELECT * FROM cityparking_transaction where id_sys_user=?",CityParkingTransaction.class);
		q.setParameter(1, user_sys_id);
		transactions=(ArrayList<CityParkingTransaction>) q.getResultList();
		return transactions;
	}
	
	public CityParkingTransactionUser getTransactionByUserId(EntityManager em, long user_sys_id )
	{
		CityParkingTransactionUser transaction;
		
		Query q =  em.createNativeQuery("SELECT * FROM cityparking_transaction where id_sys_user=? limit 1",CityParkingTransactionUser.class);
		q.setParameter(1, user_sys_id);
		transaction = (CityParkingTransactionUser) q.getSingleResult();
		return transaction;
	}
	/**
	 * Retorna todas las ventas en un rango de fecha dado.
	 * @param em
	 * @param fechaIni
	 * @param fechaFin
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<CityParkingTransactionUser> getReportSales(EntityManager em, String fechaIni, String fechaFin, String email) throws Exception {
		try {
			//			Query q = em.createNativeQuery("SELECT * FROM cityparking_transaction ct join sys_user as su on( ct.id_sys_user=su.idsys_user) where ct.email_sys_user like ? and ct.date_transaction between ? and ?", CityParkingTransactionUser.class);

			/*CriteriaBuilder qb = em.getCriteriaBuilder();
			 *

			CriteriaQuery<CityParkingTransactionUser> qu=	qb.createQuery(CityParkingTransactionUser.class);
			Root<CityParkingTransactionUser> r =  qu.from(CityParkingTransactionUser.class);
		    qu.where(qb.like(qb.upper(r.get(CityParkingTransactionUser_.)), arg1))*/
			Query q = em.createQuery("SELECT ct FROM CityParkingTransactionUser ct join fetch ct.sysUser where ct.emailSysUser like ? and ct.dateTransaction between ? and ?", CityParkingTransactionUser.class);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			q.setParameter(1, email);
			q.setParameter(2, sdf.parse(fechaIni));
			q.setParameter(3, sdf.parse(fechaFin));
			
			System.out.println("SELECT * FROM cityparking_transaction where email_sys_user like'"+email+"' and date_transaction between '"+fechaIni+"' and '"+fechaFin+"';");
			return (ArrayList<CityParkingTransactionUser>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	/**
	 * Retorna todas las ventas en un rango de fecha dado.
	 * @param em
	 * @param fechaIni
	 * @param fechaFin
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<CityParkingTransactionUser> getReportSales(EntityManager em, String fechaIni, String fechaFin, String cu , String method) throws Exception {
		try {
			//			Query q = em.createNativeQuery("SELECT * FROM cityparking_transaction ct join sys_user as su on( ct.id_sys_user=su.idsys_user) where ct.email_sys_user like ? and ct.date_transaction between ? and ?", CityParkingTransactionUser.class);

			/*CriteriaBuilder qb = em.getCriteriaBuilder();
			 *

			CriteriaQuery<CityParkingTransactionUser> qu=	qb.createQuery(CityParkingTransactionUser.class);
			Root<CityParkingTransactionUser> r =  qu.from(CityParkingTransactionUser.class);
		    qu.where(qb.like(qb.upper(r.get(CityParkingTransactionUser_.)), arg1))*/
			
			String paymentZIdQuery="";
		
			if("ZP".equals(method))
			{
			
				paymentZIdQuery=" and idzone is null";
			}else if("PK".equals(method))
			{
				paymentZIdQuery=" and idzone is not null";
			}
			Query q = em.createQuery("SELECT ct FROM CityParkingTransactionUser ct join fetch ct.sysUser where ct.sysUser.cu like ? and (ct.dateTransaction between ? and ?)  "+paymentZIdQuery, CityParkingTransactionUser.class);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			q.setParameter(1, cu);
			q.setParameter(2, sdf.parse(fechaIni));
			q.setParameter(3, sdf.parse(fechaFin));
			
			//System.out.println("SELECT * FROM cityparking_transaction where email_sys_user like'"+ciu+"' and date_transaction between '"+fechaIni+"' and '"+fechaFin+"';");
			return (ArrayList<CityParkingTransactionUser>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<CityParkingTransactionUser> getReportSalesByUser(EntityManager em, SysUser su , String fechaIni, String fechaFin) throws Exception {
		try {
		
			Query q = em.createNativeQuery("SELECT * FROM cityparking_transaction ct join sys_user as su on( ct.id_sys_user=su.idsys_user) "
					+ " where ct.id_sys_user= ? and ct.date_transaction between ? and ?", CityParkingTransactionUser.class);
			q.setParameter(1, su.getIdsysUser());
			q.setParameter(2, fechaIni);
			q.setParameter(3, fechaFin);
			
			//System.out.println("SELECT * FROM cityparking_transaction where email_sys_user="+userid+" and date_transaction between "+fechaIni+" and "+fechaFin);
			return (ArrayList<CityParkingTransactionUser>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public ArrayList<CityParkingTransaction> checkTransactionStatus(EntityManager em) {
	
	ArrayList<CityParkingTransaction>   transactions = new ArrayList<CityParkingTransaction>();
		
		Query q =  em.createNativeQuery("SELECT * FROM cityparking_transaction where state_transaction  not like '%Completed%' and state_transaction not like '%Rejected%'",CityParkingTransaction.class);
	
		transactions=(ArrayList<CityParkingTransaction>) q.getResultList();
		return transactions;
	}

}





	
	/*	@SuppressWarnings("unchecked")
	public ArrayList<ReportSales> getReportSales(EntityManager em, String fechaIni, String fechaFin) throws Exception {
		try {
			Query q = em.createNativeQuery(
					"select * from paypal_transaction where (date_transaction between '"
							+ fechaIni + "' and '" + fechaFin + "')", ReportSales.class);
			return (ArrayList<ReportSales>) q.getResultList();
		} catch (Exception e) {
			return null;
		}
	}*/
	