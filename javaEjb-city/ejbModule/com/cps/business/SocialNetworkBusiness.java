package com.cps.business;

import javax.persistence.EntityManager;

import com.cps.entity.bean.SocialNetwork;

public class SocialNetworkBusiness {

	public SocialNetworkBusiness(){
		
	}
	
	public String getSNUrl(EntityManager em, String name){
		try{
			SocialNetwork sn = (SocialNetwork)
					em.createNativeQuery(
							"SELECT * FROM social_networks WHERE name = '"+name+"'", 
							SocialNetwork.class
							).getSingleResult();		
			return sn.getUrl();
		}catch (Exception e) {
			return null;
		}
	}
	
}
