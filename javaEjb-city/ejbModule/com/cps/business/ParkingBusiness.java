package com.cps.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cps.entity.bean.ActiveParking;
import com.cps.entity.bean.ParkingHistory;
import com.cps.entity.bean.ParkingHistoryC;
import com.cps.entity.bean.ParkingHistoryCity;
import com.cps.entity.bean.ParkingHistoryDevice;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.ReportParking;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.Vehicle;
import com.cps.entity.bean.Zone;
import com.cps.util.CodeResponse;
import com.cps.util.Util;

public class ParkingBusiness {

	public Object endParking(EntityManager em, String plate, SysUser user) throws Exception {
		
		try{					
			
			Vehicle vh = new VehicleBusiness().getVehicle(em, plate, user);
			
			if( vh == null ){
				return CodeResponse.VEHICLE_DOES_NOT_EXISTS;
			}
			
			ActiveParking act = getActiveParkingVehicle(em, plate);			
			ParkingHistory ph = em.find(ParkingHistory.class, act.getIdHistory());
			Zone z = new ZoneBusiness().getZone(em, ph.getZoneIdzone(), null);
			ph.setEndTime(Util.getDateFromGMT(z.getGmt()));
			if( act.getCurrentMins() == 0 ){
				act.setCurrentMins(1);
				user.setBalance( user.getBalance().subtract(act.getMinuteValue()) );
			}
			em.merge(ph);
			em.merge(user);
			em.remove(act);
			em.flush();		
			
			new AgentBusiness().addPushToAgentsByZone(em, "2|"+z.getIdzone()+"|"+z.getName(), z.getIdzone());
			
			return act;
		
		}catch (Exception e) {
			return CodeResponse.NO_PARKING_ACTIVE;
		}
		
	}
	
	public Object startParking(
			EntityManager em, int platform, String plate, String msisdn, 
			String idCountry, String place,
			SysUser user, int enableNotifSMS) throws Exception {
				
		PlaceBusiness pb = new PlaceBusiness();		
		Zone zone = pb.getPlaceZone(em, place, idCountry);		
		String idZone = zone.getIdzone();
		
		Vehicle vh = new VehicleBusiness().getVehicle(em, plate, user);						
		if( vh == null ){
			return CodeResponse.VEHICLE_DOES_NOT_EXISTS;
		}
		
		if( getActiveParkingVehicle(em, plate) == null ){
		
			ZoneBusiness zb = new ZoneBusiness();		
			
			boolean available = false;
			
			try{
				 available = zb.isAvailableZone(em, idZone, idCountry);
			}catch (Exception e) {
				return CodeResponse.ZONE_IS_CLOSED;
			}
			
			if( available ){
				
				if( pb.isPlaceAvailable(em, place, idCountry) ){
									
					BigDecimal minV = zb.getMinuteValue(em, idZone, idCountry);
					
					if( user.getBalance().compareTo(minV) == 1  ){
						
						int max = zb.getMaxMinutes(em, idZone, idCountry);
						Date startTime = Util.getDateFromGMT(zone.getGmt());
						Date systemDate = new Date();
						
						int suppMins = (user.getBalance().divide(minV, RoundingMode.HALF_UP)).intValue();
						
						if( max > 0 ){
							if( suppMins > 1 && suppMins < max ){																
								max = suppMins;
							}							
						}
						
						ParkingHistory ph = new ParkingHistory(
								null, minV, msisdn, platform, startTime, 
								pb.getPlace(em, place, idCountry).getIdplace(), 
								vh.getPlate(), user.getIdsysUser(), idZone
								);
						
						em.persist(ph);
						em.flush();

						ActiveParking act = new ActiveParking(
								0, enableNotifSMS, max, minV, msisdn, platform, Util.getDateFromGMT(zone.getGmt()), 
								pb.getPlace(em, place, idCountry).getIdplace(), 
								ph.getIdhistory(), vh.getPlate(), user.getIdsysUser(), idZone, systemDate
								);
						
						em.persist(act);
						em.flush();
						
						//PUSH TO AGENT
						new AgentBusiness().addPushToAgentsByZone(em, "1|"+zone.getIdzone()+"|"+zone.getName(), zone.getIdzone());
						
						return act;
												
					}
					else{
						return CodeResponse.EMPTY_RESIDUE;
					}
					
				}
				else{
					return CodeResponse.PLACE_IS_OCCUPIED;
				}
				
			}
			else{
				return CodeResponse.ZONE_IS_CLOSED;
			}
		
		}
		else{
			return CodeResponse.VEHICLE_ALREADY_PARKING;
		}				
		
	}
			
	public String getTimeParking(EntityManager em, String plate) throws Exception {
		return null;
	}

	public ActiveParking getActiveParkingVehicle(EntityManager em, String plate)
			throws Exception {
		try{
			
			return (ActiveParking)em.createNativeQuery(
					"SELECT * FROM active_parking WHERE vehicle_plate = '"+plate+"'", 
					ActiveParking.class
					).getSingleResult();
			
		}catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParkingByUser(EntityManager em, String idUser)
			throws Exception {
		try{
			
			return new ArrayList<ActiveParking>(em.createNativeQuery(
					"SELECT * FROM active_parking WHERE idsys_user = "+idUser, 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ParkingHistory> getParkingHistory(EntityManager em, String zone)
			throws Exception {
		try{			
			Zone z = new ZoneBusiness().getZone(em, zone, null);
			return new ArrayList<ParkingHistory>(
					em.createNativeQuery(
					"SELECT * FROM parking_history WHERE DATE(startTime) = DATE('"+Util.getDateStringFromGMT(z.getGmt())+"') AND zone_idzone = '"+zone+"'", 
					ParkingHistory.class
					).getResultList());						
		}catch (Exception e) {
			return null;
		}
	}
	
	public ParkingHistory getParkingHistoryById(EntityManager em, String idHistory)
			throws Exception {
		try{			
			return em.find(ParkingHistory.class, idHistory);
		}catch (Exception e) {
			return null;
		}
	}
	
	public int countActiveParkingExceed(EntityManager em, String idZone)
			throws Exception {
		try{
			return getTimeExceedActiveParking(em, idZone).size();			
		}catch (Exception e) {
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getTimeExceedActiveParking(
			EntityManager em, String idZone) throws Exception {
		
		try{
			
			 return new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"SELECT * FROM active_parking WHERE currentMins > maxMins AND zone_idzone = '"+idZone+"' AND maxMins > 0", 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			return new ArrayList<ActiveParking>();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParkingByZone(
			EntityManager em, String idCountry,
			String idZone) throws Exception {
		
		try{
		
			Zone zn = new ZoneBusiness().getZone(em, idZone, idCountry);
						
			return new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"SELECT * FROM active_parking WHERE zone_idzone = '"+zn.getIdzone()+"'", 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			return new ArrayList<ActiveParking>();
		}
		
	}
		
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParkingByPlace(
			EntityManager em, String placeFrom,
			String placeTo) throws Exception {
		
		try{
		
			return new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"SELECT * FROM active_parking WHERE place_idplace BETWEEN 'COS"+placeFrom+"' AND 'COS"+placeTo+"'", 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			return new ArrayList<ActiveParking>();
		}
		
	}
	
	public ActiveParking getActiveParkingSMS(
			EntityManager em, String place, String cu, String idCountry) throws Exception {
		
		try{
			
			SysUserBusiness sub = new SysUserBusiness();			
			SysUser sys = sub.getUserByCU(em, cu);
			Place pl = new PlaceBusiness().getPlace(em, place, idCountry);
			
			return (ActiveParking)(
					em.createNativeQuery(
					"SELECT * FROM active_parking WHERE idsys_user = "+sys.getIdsysUser()+" AND place_idplace = '"+pl.getIdplace()+"'", 
					ActiveParking.class
					).getSingleResult());
			
		}catch (Exception e) {
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParkingSMS(
			EntityManager em, String cu) throws Exception {
		
		try{
			
			SysUserBusiness sub = new SysUserBusiness();			
			SysUser sys = sub.getUserByCU(em, cu);
			
			return new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"SELECT * FROM active_parking WHERE idsys_user = "+sys.getIdsysUser()+"", 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParkingByZone(
			EntityManager em, String idZone) throws Exception {
		
		try{
			
			return new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"SELECT * FROM active_parking WHERE zone_idzone = '"+idZone+"'", 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			return new ArrayList<ActiveParking>();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParkingBySubZone(
			EntityManager em, 
			Long idSubZone) throws Exception {
		
		try{
		
			//Zone zn = new ZoneBusiness().getZone(em, idZone, idCountry);
						
			return new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"SELECT * FROM active_parking WHERE place_idplace In(  select idplace from place_subzone where idsubzone="+idSubZone+")", 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			return new ArrayList<ActiveParking>();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParking(EntityManager em) throws Exception {
		try{
			
			return new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"SELECT * FROM active_parking", 
					ActiveParking.class
					).getResultList());
			
		}catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ParkingHistoryC> getHistoryParking(EntityManager em,String idSysUser )
	throws Exception {
		try{
			return new ArrayList<ParkingHistoryC>(
				em.createNativeQuery(
				"SELECT * FROM parking_history WHERE idsys_user = '"+idSysUser+"'", 
				ParkingHistoryC.class
				).getResultList());
	   
		}catch(Exception e){
			return null;
		}
	}
	
	

	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> getHistoryParking(EntityManager em,String idSysUser , Class<T> clazz)
	throws Exception {
		  String sql="SELECT * FROM parking_history WHERE idsys_user = '"+idSysUser+"'";
		  System.out.println(sql);
			return new ArrayList<T>(
				em.createNativeQuery(sql
				, 
				clazz
				).getResultList());
	   
	
	}
	
	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> getHistoryParking(EntityManager em,String idSysUser , String startDate, String  endDate,  Class<T> clazz)
	throws Exception {
		  String sql="SELECT * FROM parking_history WHERE idsys_user = '"+idSysUser+"' and "
	+" (startTime between '"+startDate+"' and '"+endDate+"'" + 
                  " and  endTime between '"+startDate+"' and '"+endDate+"')";
		  System.out.println(sql);
			return new ArrayList<T>(
				em.createNativeQuery(sql
				, 
				clazz
				).getResultList());
	   
	
	}
	
  @SuppressWarnings("unchecked")
  public ArrayList<ReportParking> getReportParking(EntityManager em,String idZone, String fechaIni,String fechaFin)
	throws Exception {    
	  
	  /*
	   * 	  Query q = em.createNativeQuery(
				"select phv.*, su.idsys_user from parking_history_view  phv join sys_user su on(phv.idsys_user=su.idsys_user) where zone_idzone  like'%"+idZone+"%' " +
						" and " +
						" (startTime between '"+fechaIni+"' and '"+fechaFin+"'" + 
                        " and  endTime between '"+fechaIni+"' and '"+fechaFin+"')", 
				ReportParking.class);	   	   
	   * */
	  try{	   
		  Query q = em.createQuery(
				"select phv from ReportParking phv join fetch phv.sysUser  where zone_idzone like '%"+idZone+"' " +
						" and " +
						" (phv.startTime between '"+fechaIni+"' and '"+fechaFin+"'" + 
                        " and  phv.endTime between '"+fechaIni+"' and '"+fechaFin+"')", 
				ReportParking.class);	   	   
		  return (ArrayList<ReportParking>)q.getResultList();	      
	  }catch(Exception e){
		  return null;   
	  }
  }	
	
  
  @SuppressWarnings("unchecked")
  public ArrayList<ReportParking> getReportParking(EntityManager em,String idZone, String fechaIni,String fechaFin, int platformId)
	throws Exception {    
	  try{	   
		  Query q = em.createNativeQuery(
				"select * from parking_history_view where zone_idzone = '"+idZone+"' " +
						" and " +
						" (startTime between '"+fechaIni+"' and '"+fechaFin+"'" + 
                        " and  endTime between '"+fechaIni+"' and '"+fechaFin+"') and  platform_idplatform="+ platformId, 
				ReportParking.class);	   	   
		  return (ArrayList<ReportParking>)q.getResultList();	      
	  }catch(Exception e){
		  return null;   
	  }
  }	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getAllActiveParkingByZone(
			EntityManager em, String idZone) throws Exception {
		
		try{
		
			ArrayList<ActiveParking> ap = new ArrayList<ActiveParking>(
					em.createNativeQuery(
					"select coalesce(idactiveparking,TRUNCATE(RAND() *100000,0)) as idactiveparking,startTime,coalesce(currentMins,0) as currentMins,coalesce(maxMins,0) as maxMins," +
					"msisdn,coalesce(platform_idplatform,0) as platform_idplatform,coalesce(enableSMSNotif,0) as enableSMSNotif," +
					"coalesce(minuteValue,0) as minuteValue,idplace as place_idplace,idhistory," +
					"vehicle_plate,idsys_user,idzone as zone_idzone, '2012-01-01 00:00:00' as system_date" +
					" from (select * from place p" +
					" left join active_parking a ON(p.idplace = a.place_idplace and p.idzone = a.zone_idzone)) a" +
					" where a.idzone = '"+idZone+"' order by place_idplace", 
					  ActiveParking.class
					).getResultList());
						
			return ap;
			
		}catch (Exception e) {			
			return new ArrayList<ActiveParking>();
		}
		
	}	
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getActiveParkingByZoneAndPlaceRange(
			EntityManager em, String idZone, String placeFrom, String placeTo) throws Exception {
		
		try{
		
			ArrayList<ActiveParking> ap = new ArrayList<ActiveParking>(
					em.createNativeQuery(
							"select " +
									"    coalesce(idactiveparking, " +
									"            TRUNCATE(RAND() * 100000, 0)) as idactiveparking, " +
									"    startTime, " +
									"    coalesce(currentMins, 0) as currentMins, " +
									"    coalesce(maxMins, 0) as maxMins, " +
									"    msisdn, " +
									"    coalesce(platform_idplatform, 0) as platform_idplatform, " +
									"    coalesce(enableSMSNotif, 0) as enableSMSNotif, " +
									"    coalesce(minuteValue, 0) as minuteValue, " +
									"    idplace as place_idplace, " +
									"    idhistory, " +
									"    vehicle_plate, " +
									"    idsys_user, " +
									"    idzone as zone_idzone, " +
									"    '2012-01-01 00:00:00' as system_date " +
									"from " +
									"    (select " +
									"        * " +
									"    from " +
									"        place p " +
									"    left join active_parking a ON (p.idplace = a.place_idplace " +
									"        and p.idzone = a.zone_idzone)) a " +
									"where " +
									"    a.idzone = 'COS04' and " +
									"	cast(substring(idplace,4,length(idplace)) as unsigned) between " +
									"	"+ placeFrom +" and " + placeTo  + 
									" " +
									"order by place_idplace ", 
					  ActiveParking.class
					).getResultList());
						
			return ap;
			
		}catch (Exception e) {			
			return new ArrayList<ActiveParking>();
		}
		
	}	
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> getAllActiveParkingByAgent(EntityManager em, String idAgent) throws Exception {
		try{
			
			Query query = em.createNativeQuery("call getAllAPByAgent('"+idAgent+"')", ActiveParking.class);
			return new ArrayList<ActiveParking>(query.getResultList());			
			
		}catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<ActiveParking> searchVehicleInAP(EntityManager em, String idAgent, String plate) throws Exception {
		try{		
						
			Query query = em.createNativeQuery(
					"SELECT * FROM active_parking WHERE " +
					"vehicle_plate = '"+plate+"' AND zone_idzone = " +
							"(select idzone from agent_zone where idagent = "+idAgent+" " +
									"and idzone = zone_idzone)", ActiveParking.class);
			return new ArrayList<ActiveParking>(query.getResultList());			
		}catch (Exception e) {			
			return null;
		}
	}
	private  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	public void addHistoryEvent( EntityManager em, BigDecimal minV, String msisdn, int platform, String place, String idCountry, SysUser user, String idZone, String confirmationNumber, String initparkingdate, String endparkingdate, int deviceId )
	{
	//	PlaceBusiness pb = new PlaceBusiness();	
		//Place plc = pb.getPlace(em, place, idCountry);
		//String idplace = plc.getIdplace();
		
		 try {
			 //2014-01-16T12:13:35
			 
			
			
			 Date dateinit= sdf.parse(initparkingdate);
			Date dateend= sdf.parse(endparkingdate);
			ParkingHistory ph = new ParkingHistory(
					dateend, BigDecimal.valueOf(0), msisdn, platform, dateinit,"0" 
					, 
					"", user.getIdsysUser(), idZone,place,minV.doubleValue(),confirmationNumber
					);
			
			
			em.persist(ph);

			em.flush();
			ParkingHistoryDevice phd= new ParkingHistoryDevice();
			phd.setDeviceId(deviceId);
			phd.setIdhistory(ph.getIdhistory());
			em.persist(phd);
			em.flush();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
}
