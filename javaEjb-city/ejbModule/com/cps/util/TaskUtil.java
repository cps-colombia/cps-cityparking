package com.cps.util;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

//import org.hibernate.dialect.Sybase11Dialect;

public class TaskUtil {

	private volatile static ScheduledExecutorService executor = null;
	private volatile static ThreadPoolExecutor poolExecutor= null;
	public volatile static ThreadPoolExecutor poolExecutorCache= null;

	
	/**
	 * 
	 * @param task  Current task to execute
	 * @param periodInsecconds Seconds
	 * @return
	 */
	public static ScheduledFuture<?> startTask(Runnable task, long periodInsecconds) {
		if (executor == null)
		{
		  
		    
			executor = new CPSExecutorService(10); //Executors.newScheduledThreadPool(10);
		}
		return executor.scheduleAtFixedRate(task, 0, periodInsecconds, TimeUnit.SECONDS);
	}
	
	public static void enqueTask( Runnable task )
	{
		if(poolExecutor==null)
		{
			poolExecutor  = new ThreadPoolExecutor(50,100, 60, TimeUnit.SECONDS,  new LinkedBlockingQueue<Runnable>());
		}
		poolExecutor.execute(task);
	}
	
	public static void enqueTask( Callable<?> task )
	{
		if(poolExecutor==null)
		{
			poolExecutor  = new ThreadPoolExecutor(50,50, 10, TimeUnit.SECONDS,  new LinkedBlockingQueue<Runnable>());
		}
		Future<?> future = poolExecutor.submit(task);
		try {
			future.get(10,  TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public synchronized static  void executeCacheTask( Runnable task )
	{
		if(poolExecutorCache==null)
		{
			poolExecutorCache  = new ThreadPoolExecutor(50,100, 100, TimeUnit.SECONDS,  new ArrayBlockingQueue<Runnable>(200));
		}
		poolExecutorCache.execute(task);
	}

	
	
	
	public static void main(String[] args) {
		
		
		System.out.println("starting tasks");
		
		for( int i=0;i<10000;i++)
		{
			System.out.println("starting task "+i);
			final int t=i;
		TaskUtil.enqueTask( new Runnable() {
			
			@Override
			public void run() {
			     int tt=t; 
				try {
					long time=randInt(1000,25000);
					System.out.println("time t task " + time);

					Thread.sleep(time);
					System.out.println("finishing t task " + tt);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		}
	}
	
	
	public static int randInt(int min, int max) {

	    // NOTE: Usually this should be a field rather than a method
	    // variable so that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}
