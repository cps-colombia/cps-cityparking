package com.cps.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class PropertiesUtil {

	private static PropertiesUtil instance;
	
	private Properties messages;
	
	private static String DEV_MESSAGES_PROPERTY = "D:\\repositories\\cps\\web\\properties\\cps.messages.properties";
	
	private static String PROD_MESSAGES_PROPERTY = "/root/properties/cps.messages.properties";
	
	private PropertiesUtil(){
		try{
						messages = new Properties();
			messages.load(new FileInputStream(new File(DEV_MESSAGES_PROPERTY)));
		}catch (Exception e) {
			try{
				messages.load(new FileInputStream(new File(PROD_MESSAGES_PROPERTY)));
			}catch (Exception ex) {}
		}		
	}
	
	public static final PropertiesUtil getInstance(){
		if( instance == null ){
			instance = new PropertiesUtil();
		}
		return instance;
	}
	
	public String getMessage(String key, String[] params)throws Exception{
		String message = messages.getProperty("cps.messages."+key);
		for( int i=0;i<params.length;i++ ){
			message = message.replace("{"+i+"}", params[i]);
		}
		return message;
	}
	
}
