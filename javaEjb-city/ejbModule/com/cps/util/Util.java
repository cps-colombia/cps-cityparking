package com.cps.util;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class Util { 

	public static boolean nowIsBetweenX(Time start, Time end){		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Time now = Time.valueOf(dateFormat.format(cal.getTime()));		
		return now.after(start) && now.before(end);		
	}
	
	public static boolean gmtIsBetween(String gmt, Time start, Time end){		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");		
		Time now = Time.valueOf(dateFormat.format(Util.getDateFromGMT(gmt)));
		return now.after(start) && now.before(end);		
	}
	
	public static int getToday(){
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DAY_OF_WEEK);
	}
	
	public static int getTodayFromGMT(String place){		
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone(place);
		java.util.Calendar calendar = java.util.Calendar.getInstance(zone);		
		return calendar.get(Calendar.DAY_OF_WEEK);		
	}
	
	public static Time getNow(){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Time now = Time.valueOf(dateFormat.format(cal.getTime()));		
		return now;		
	}
	
	public static String getStringNow(){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Time now = Time.valueOf(dateFormat.format(cal.getTime()));		
		return now.toString();		
	}
	
	public static String getStringNowFromGMT(String place){
		Date d = getDateFromGMT(place);
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Time now = Time.valueOf(dateFormat.format(d.getTime()));		
		return now.toString();		
	}
	
	public static String getDate(){
		Calendar c = Calendar.getInstance();
		String day = String.valueOf( c.get( Calendar.DAY_OF_MONTH ) );
		String month = String.valueOf( c.get(Calendar.MONTH)+1 );
		String year = String.valueOf( c.get(Calendar.YEAR) );
		return  year+"-"+(month.length() < 2 ? "0"+month : month)+"-"+(day.length() < 2 ? "0"+day : day);
	}
	
	public static String getTime(){
		Calendar c = Calendar.getInstance();
		String hour = String.valueOf( c.get( Calendar.HOUR_OF_DAY ));
		String mins = String.valueOf( c.get( Calendar.MINUTE ));
		String secs = String.valueOf( c.get( Calendar.SECOND ));
		return (hour.length() < 2 ? "0"+hour : hour)+":"+(mins.length() < 2 ? "0"+mins : mins)+":"+(secs.length() < 2 ? "0"+secs : secs);
	}
	
	public static BigInteger getBigIntValue(String sql, EntityManager em){
		Query query = em.createNativeQuery(sql);
		return (BigInteger)query.getSingleResult();
	}
	
	public static Integer getIntValue(String sql, EntityManager em){
		Query query = em.createNativeQuery(sql);
		return (Integer)query.getSingleResult();
	}
	
	public static Double getDoubleValue(String sql, EntityManager em){
		Query query = em.createNativeQuery(sql);
		return (Double)query.getSingleResult();
	}
	
	public static String getDateTime(){
		return getDate()+" "+getTime();
	}
	
	private static String convertToHex(byte[] data) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
 
    public static String SHA1(String text) 
    throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
	    MessageDigest md;
	    md = MessageDigest.getInstance("SHA-1");
	    byte[] sha1hash = new byte[40];
	    md.update(text.getBytes("iso-8859-1"), 0, text.length());
	    sha1hash = md.digest();
	    return convertToHex(sha1hash);
    }
    
    public static String convertCurrencyWS(BigDecimal ammount, String fromCurrency, String toCurrency)throws Exception{
    	StringBuffer buff = new StringBuffer();
    	buff.append("http://www.google.com/ig/calculator?hl=en&q=").append(ammount.toString()).append(fromCurrency).append("=?").append(toCurrency);
    	java.net.URL url = new java.net.URL(buff.toString());
    	HttpURLConnection m_con = (HttpURLConnection)url.openConnection();
    	InputStream  in = m_con.getInputStream();
    	int k = 0;
    	StringBuffer rta = new StringBuffer();
    	while( (k = in.read()) != -1 ){
    		rta.append((char)k);
    	}
    	String res = rta.toString();
    	String[] vals = res.split(",");
    	String[] v = vals[1].replace("rhs: \"", "").split(" ");
    	return v[0];
    }

    public static BigDecimal convertCurrencyBDWS(BigDecimal ammount, String fromCurrency, String toCurrency)throws Exception{
    	BigDecimal bd = new BigDecimal(convertCurrencyWS(ammount, fromCurrency, toCurrency));
    	return bd.setScale(2, 6);
    }
    
    public static Date getDateFromGMT(String place){
    	
    	try{
	    	
    		java.util.TimeZone zone = java.util.TimeZone.getTimeZone(place);
			java.util.Calendar calendar = java.util.Calendar.getInstance(zone);
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    formatter.setCalendar(calendar);	    
		    String fr = formatter.format(new java.util.Date());
			
			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
			
			return sdf.parse(fr);
			
    	}catch (Exception e) {}
    	
	    return new Date();
	    
    }
    
    public static String getDateStringFromGMT(String place){
    	java.util.TimeZone zone = java.util.TimeZone.getTimeZone(place);
		java.util.Calendar calendar = java.util.Calendar.getInstance(zone);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    formatter.setCalendar(calendar);	    
	    return formatter.format(new java.util.Date());
    }
    
    
    public  static String getNextId()
    {
    	
    	 long idLong=Math.abs(java.util.UUID.randomUUID().getMostSignificantBits());//System.currentTimeMillis();
 		return String.valueOf(idLong); //String.valueOf((idLong << 16) & idLong);
    }
    
    public static void main(String[] args) {
    	try{
    		
    		String prev="0";
    		Set<String> ids= new HashSet<String>();
			System.out.println("inicio");

    		for( int i=0;i<1000000000; i++)
    		{
    			String curr=getNextId();
    			
    			if(ids.contains(curr))
    				System.out.println("ya se encuentra creado");
    			ids.add(curr);
    			
    			//prev=curr;
    		}
    		
			System.out.println("fin");

    		/*BigDecimal bd = Util.convertCurrencyBDWS(new BigDecimal(1), "USD", "CRC");
    		System.out.println("$"+bd.toString());
    		
    		BigDecimal bd2 = Util.convertCurrencyBDWS(bd, "CRC", "USD");
    		System.out.println("$"+bd2.toString());
    		
    		System.out.println( Util.getDateFromGMT("America/Toronto").toString() );
    		System.out.println( Util.getTodayFromGMT("America/Costa_Rica") );
    		System.out.println( Util.getStringNowFromGMT("America/Costa_Rica") );
    		*/
    		//Util.getDateFromGMT("America/Toronto").getTime()
//    		ArrayList<Object> arr = new ArrayList<Object>();
//    		for( int i=0;i<4301712;i++ ){
//    			arr.add(new Object());
//    		}
    		
    	}catch (Exception e) {
			//e.printStackTrace();
		}
	}
    
}

