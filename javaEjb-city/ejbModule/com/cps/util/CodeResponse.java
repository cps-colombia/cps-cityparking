package com.cps.util;

public interface CodeResponse {

	public static final int REGISTER_VEHICLE_OK = -100;
	
	public static final int INVALID_PIN = -101;
	
	public static final int VEHICLE_ALREADY_REGISTRED = -102;
	
	public static final int EMPTY_RESIDUE = -103;
	
	public static final int PARKING_OK = -104;
	
	public static final int FREE_PARKING_OK = -105;
	
	public static final int INVALID_ZONE_CODE = -106;
	
	public static final int INVALID_CU = -107;
	
	public static final int NO_PARKING_ACTIVE = -108;
	
	public static final int DISABLED_PARKING = -109;
	
	public static final int NO_SPACE_AVALIABLE = -110;
	
	public static final int SPACES_AVALIABLE = -111;
	
	public static final int RELOAD_RESIDUE_OK = -112;
	
	public static final int VEHICLE_ALREADY_PARKING = -113;
	
	public static final int MOBILE_ALREADY_REGISTERED = -114;
	
	public static final int MOBILE_HAVE_MORE_THAN_ONE_VEHICLE = -115;
	
	public static final int DEBT_RELOAD_RESIDUE = -116;
	
	public static final int PASSWORD_SET_OK = -117;
	
	public static final int PASSWORD_SET_INVALID_PASS = -118;
	
	public static final int UPDATE_USER_OK = -119;
	
	public static final int ZONE_IS_CLOSED = -120;
	
	public static final int VEHICLE_DOES_NOT_EXISTS = -121;
	
	public static final int VEHICLE_ALREADY_SHARED = -122;
	
	public static final int ACCOUNT_NOT_SHARED = -123;
	
	public static final int VEHICLE_SHARED_SUCCESSFULLY = -124;
	
	public static final int MOBILE_NOT_REGISTERED = -125;
	
	public static final int ZONE_REGISTERED = -126;
	
	public static final int ZONE_NOT_REGISTERED = -127;
	
	public static final int VEHICLE_DELETED_OK = -126;
	
	public static final int VEHICLE_DELETED_FAIL = -127;
	
	public static final int PLACE_IS_OCCUPIED = -128;
	
	public static final int PLACE_DOES_NOT_EXISTS = -129;
	
	public static final int REQUEST_ERROR = -200;		
	
}
