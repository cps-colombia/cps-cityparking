
package com.cps.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Cvalorubicaws complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Cvalorubicaws">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="nombreparqueadero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccionparqueadero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="correoparqueadero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cupos" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="latitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cvalorubicaws", propOrder = {
    "token",
    "identificador",
    "nombreparqueadero",
    "direccionparqueadero",
    "correoparqueadero",
    "horario",
    "cupos",
    "latitud",
    "longitud"
})
public class Cvalorubicaws {

    protected String token;
    protected long identificador;
    protected String nombreparqueadero;
    protected String direccionparqueadero;
    protected String correoparqueadero;
    protected String horario;
    protected int cupos;
    protected String latitud;
    protected String longitud;

    /**
     * Obtiene el valor de la propiedad token.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     */
    public long getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     */
    public void setIdentificador(long value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreparqueadero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreparqueadero() {
        return nombreparqueadero;
    }

    /**
     * Define el valor de la propiedad nombreparqueadero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreparqueadero(String value) {
        this.nombreparqueadero = value;
    }

    /**
     * Obtiene el valor de la propiedad direccionparqueadero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionparqueadero() {
        return direccionparqueadero;
    }

    /**
     * Define el valor de la propiedad direccionparqueadero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionparqueadero(String value) {
        this.direccionparqueadero = value;
    }

    /**
     * Obtiene el valor de la propiedad correoparqueadero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoparqueadero() {
        return correoparqueadero;
    }

    /**
     * Define el valor de la propiedad correoparqueadero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoparqueadero(String value) {
        this.correoparqueadero = value;
    }

    /**
     * Obtiene el valor de la propiedad horario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHorario() {
        return horario;
    }

    /**
     * Define el valor de la propiedad horario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHorario(String value) {
        this.horario = value;
    }

    /**
     * Obtiene el valor de la propiedad cupos.
     * 
     */
    public int getCupos() {
        return cupos;
    }

    /**
     * Define el valor de la propiedad cupos.
     * 
     */
    public void setCupos(int value) {
        this.cupos = value;
    }

    /**
     * Obtiene el valor de la propiedad latitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatitud() {
        return latitud;
    }

    /**
     * Define el valor de la propiedad latitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatitud(String value) {
        this.latitud = value;
    }

    /**
     * Obtiene el valor de la propiedad longitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongitud() {
        return longitud;
    }

    /**
     * Define el valor de la propiedad longitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongitud(String value) {
        this.longitud = value;
    }

}
