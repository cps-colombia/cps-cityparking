
package com.cps.service;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.7.5
 * 2015-01-06T15:47:26.965-05:00
 * Generated source version: 2.7.5
 * 
 */
public final class WSpagoCPSWSHttpPost_WSpagoCPSWSHttpPost_Client {

    private static final QName SERVICE_NAME = new QName("http://tempuri.org/", "WSpagoCPSWS");

    private WSpagoCPSWSHttpPost_WSpagoCPSWSHttpPost_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = WSpagoCPSWS.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        WSpagoCPSWS ss = new WSpagoCPSWS(wsdlURL, SERVICE_NAME);
        WSpagoCPSWSHttpPost port = ss.getWSpagoCPSWSHttpPost();  
        
        {
        System.out.println("Invoking registrarAprobacion...");
        java.lang.String _registrarAprobacion_identificadors = "";
        java.lang.String _registrarAprobacion_numeroAprobacion1 = "";
        java.lang.String _registrarAprobacion_resultadoOperacion1 = "";
        java.lang.String _registrarAprobacion_token1 = "";
        java.lang.String _registrarAprobacion_cu1 = "";
        java.lang.String _registrarAprobacion_saldo1 = "";
        int _registrarAprobacion__return = port.registrarAprobacion(_registrarAprobacion_identificadors, _registrarAprobacion_numeroAprobacion1, _registrarAprobacion_resultadoOperacion1, _registrarAprobacion_token1, _registrarAprobacion_cu1, _registrarAprobacion_saldo1);
        System.out.println("registrarAprobacion.result=" + _registrarAprobacion__return);


        }

        System.exit(0);
    }

}
