
package com.cps.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cps.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Cvalorws_QNAME = new QName("http://tempuri.org/", "Cvalorws");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cps.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValorWS }
     * 
     */
    public ValorWS createValorWS() {
        return new ValorWS();
    }

    /**
     * Create an instance of {@link Cvalorws }
     * 
     */
    public Cvalorws createCvalorws() {
        return new Cvalorws();
    }

    /**
     * Create an instance of {@link ValorWSResponse }
     * 
     */
    public ValorWSResponse createValorWSResponse() {
        return new ValorWSResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cvalorws }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Cvalorws")
    public JAXBElement<Cvalorws> createCvalorws(Cvalorws value) {
        return new JAXBElement<Cvalorws>(_Cvalorws_QNAME, Cvalorws.class, null, value);
    }

}
