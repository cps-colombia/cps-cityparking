
package com.cps.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para Cvalorws complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Cvalorws">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="plazaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fechaInicioParqueo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="fechaFinParqueo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="importeParqueo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="tiempoParqueo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoRespuesta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoRespuestaPago" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Cu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cvalorws", propOrder = {
    "token",
    "identificador",
    "plazaId",
    "fechaInicioParqueo",
    "fechaFinParqueo",
    "importeParqueo",
    "tiempoParqueo",
    "codigoRespuesta",
    "codigoRespuestaPago",
    "saldo",
    "cu"
})
public class Cvalorws {

    protected String token;
    protected long identificador;
    protected int plazaId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaInicioParqueo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaFinParqueo;
    @XmlElement(required = true)
    protected BigDecimal importeParqueo;
    protected int tiempoParqueo;
    protected int codigoRespuesta;
    protected int codigoRespuestaPago;
    @XmlElement(name = "Saldo", required = true)
    protected BigDecimal saldo;
    @XmlElement(name = "Cu")
    protected String cu;

    /**
     * Obtiene el valor de la propiedad token.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     */
    public long getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     */
    public void setIdentificador(long value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad plazaId.
     * 
     */
    public int getPlazaId() {
        return plazaId;
    }

    /**
     * Define el valor de la propiedad plazaId.
     * 
     */
    public void setPlazaId(int value) {
        this.plazaId = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicioParqueo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicioParqueo() {
        return fechaInicioParqueo;
    }

    /**
     * Define el valor de la propiedad fechaInicioParqueo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicioParqueo(XMLGregorianCalendar value) {
        this.fechaInicioParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFinParqueo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaFinParqueo() {
        return fechaFinParqueo;
    }

    /**
     * Define el valor de la propiedad fechaFinParqueo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaFinParqueo(XMLGregorianCalendar value) {
        this.fechaFinParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad importeParqueo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getImporteParqueo() {
        return importeParqueo;
    }

    /**
     * Define el valor de la propiedad importeParqueo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setImporteParqueo(BigDecimal value) {
        this.importeParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad tiempoParqueo.
     * 
     */
    public int getTiempoParqueo() {
        return tiempoParqueo;
    }

    /**
     * Define el valor de la propiedad tiempoParqueo.
     * 
     */
    public void setTiempoParqueo(int value) {
        this.tiempoParqueo = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoRespuesta.
     * 
     */
    public int getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Define el valor de la propiedad codigoRespuesta.
     * 
     */
    public void setCodigoRespuesta(int value) {
        this.codigoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoRespuestaPago.
     * 
     */
    public int getCodigoRespuestaPago() {
        return codigoRespuestaPago;
    }

    /**
     * Define el valor de la propiedad codigoRespuestaPago.
     * 
     */
    public void setCodigoRespuestaPago(int value) {
        this.codigoRespuestaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad saldo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaldo() {
        return saldo;
    }

    /**
     * Define el valor de la propiedad saldo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaldo(BigDecimal value) {
        this.saldo = value;
    }

    /**
     * Obtiene el valor de la propiedad cu.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCu() {
        return cu;
    }

    /**
     * Define el valor de la propiedad cu.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCu(String value) {
        this.cu = value;
    }

}
