
package com.cps.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificadors" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroAprobacion1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResultadoOperacion1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Token1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cu1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Saldo1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "identificadors",
    "numeroAprobacion1",
    "resultadoOperacion1",
    "token1",
    "cu1",
    "saldo1"
})
@XmlRootElement(name = "registrarAprobacion")
public class RegistrarAprobacion {

    @XmlElement(name = "Identificadors")
    protected String identificadors;
    @XmlElement(name = "NumeroAprobacion1")
    protected String numeroAprobacion1;
    @XmlElement(name = "ResultadoOperacion1")
    protected String resultadoOperacion1;
    @XmlElement(name = "Token1")
    protected String token1;
    @XmlElement(name = "Cu1")
    protected String cu1;
    @XmlElement(name = "Saldo1", required = true)
    protected BigDecimal saldo1;

    /**
     * Obtiene el valor de la propiedad identificadors.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadors() {
        return identificadors;
    }

    /**
     * Define el valor de la propiedad identificadors.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadors(String value) {
        this.identificadors = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroAprobacion1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroAprobacion1() {
        return numeroAprobacion1;
    }

    /**
     * Define el valor de la propiedad numeroAprobacion1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroAprobacion1(String value) {
        this.numeroAprobacion1 = value;
    }

    /**
     * Obtiene el valor de la propiedad resultadoOperacion1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultadoOperacion1() {
        return resultadoOperacion1;
    }

    /**
     * Define el valor de la propiedad resultadoOperacion1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultadoOperacion1(String value) {
        this.resultadoOperacion1 = value;
    }

    /**
     * Obtiene el valor de la propiedad token1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken1() {
        return token1;
    }

    /**
     * Define el valor de la propiedad token1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken1(String value) {
        this.token1 = value;
    }

    /**
     * Obtiene el valor de la propiedad cu1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCu1() {
        return cu1;
    }

    /**
     * Define el valor de la propiedad cu1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCu1(String value) {
        this.cu1 = value;
    }

    /**
     * Obtiene el valor de la propiedad saldo1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaldo1() {
        return saldo1;
    }

    /**
     * Define el valor de la propiedad saldo1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaldo1(BigDecimal value) {
        this.saldo1 = value;
    }

}
