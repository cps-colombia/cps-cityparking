package com.cps.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cps.controller.CPSMobileController;

/**
 * Servlet implementation class MobileServer
 */
public class MobileServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static Logger LOGGER = Logger.getLogger(MobileServer.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MobileServer() {
        super();
    }

    @Override
    public void init() throws ServletException {
    	super.init();
    	try{
    		CPSMobileController.initCountrys();
    	}catch (Exception e) {
    		LOGGER.error("Error inicializando paises en cache", e);
    	}
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CPSMobileController.processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
