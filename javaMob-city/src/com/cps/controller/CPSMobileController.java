﻿package com.cps.controller;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cps.bean.ParkingResume;
import com.cps.bean.PriceResponse;
import com.cps.bean.ResponseBean;
import com.cps.bean.Vehicle;
import com.cps.bean.VehicleTypeID;
import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.ActiveParking;
import com.cps.entity.bean.City;
import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.Country;
import com.cps.entity.bean.PayItem;
import com.cps.entity.bean.PaymentResponse;
import com.cps.entity.bean.Platform;
import com.cps.entity.bean.PriceRestrict;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.SysUserType;
import com.cps.entity.bean.SysUserVehicle;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZoneRestriction;
import com.cps.entity.bean.CityDriver;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.remote.CityDriverBeanRemote;
import com.cps.remote.ClientBeanRemote;
import com.cps.remote.CountryBeanRemote;
import com.cps.remote.DeviceVersionRemote;
import com.cps.remote.NotificationBeanRemote;
import com.cps.remote.ParkingBeanRemote;
import com.cps.remote.PlaceBeanRemote;
import com.cps.remote.SocialNetworkRemote;
import com.cps.remote.SuggestionBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.remote.VehicleBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.security.RampENC;
import com.cps.util.CPSMobileConstants;
import com.cps.util.CodeResponse;
import com.cps.util.JSonUtil;
import com.cps.util.MailSender;
import com.cps.util.Util;
import com.cps.util.ZoneComparator;
import com.cps.wservice.Cvalorubicaws;
import com.cps.wservice.payment.verification.ArrayOfPagosV3;
import com.cps.wservice.payment.verification.PagosV3;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CPSMobileController {

	public static com.cps.bean.SysUser getUser(SysUser us) {

		com.cps.bean.SysUser nuser = new com.cps.bean.SysUser();
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat nf = new DecimalFormat("$#.###");

		nuser.setAddress(us.getAddress());
		nuser.setBalance(nf.format(us.getBalance()));
		nuser.setBirthDate(dt.format(us.getBirthDate()));
		nuser.setCity(us.getCity());
		nuser.setCountryIdcountry(us.getCountryIdcountry());
		nuser.setCu(us.getCu());
		nuser.setEmail(us.getEmail());
		nuser.setFavoriteMsisdn(us.getFavoriteMsisdn());
		nuser.setIdsysUser(us.getIdsysUser());
		nuser.setIdsysUserType(us.getIdsysUserType());
		nuser.setLastName(us.getLastName());
		nuser.setLogin(us.getLogin());
		nuser.setName(us.getName());
		nuser.setPass(us.getPass());
		nuser.setBalance(us.getBalance().toString());

		return nuser;

	}

	public static String countrys;

	public static int countrysNum;

	private static Logger LOGGER = Logger.getLogger(CPSMobileController.class);

	public static void initCountrys() throws Exception {
		if (countrys == null) {
			CountryBeanRemote cntb = (CountryBeanRemote) EJBServiceLocator
					.getInstance().getEjbService(CountryBeanRemote.class);
			ArrayList<Country> cnt = cntb.getCountrys();
			countrysNum = cnt.size();
			countrys = JSonUtil.getInstance().toJson(cnt);
			LOGGER.info("Paises cacheados");
		}
	}

	public static void processRequest(HttpServletRequest request,
			HttpServletResponse response) {

		try {

			int op = Integer.parseInt(request.getParameter("op"));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			String user = null;
			String pass = null;
			String locale = null;
			String subject;
			String sender;
			String body;

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			if (op != CPSMobileConstants.REGISTER
					&& op != CPSMobileConstants.GET_COUNTRY_LIST
					&& op != CPSMobileConstants.GET_CITIES_LIST
					&& op != CPSMobileConstants.RESET_PASSWORD
					&& op != CPSMobileConstants.CHECKVERSION) {

				user = request.getParameter("user");
				pass = request.getParameter("pass");

				if (pass != null) {
					pass = RampENC.decrypt(pass);
				}

				SysUserBeanRemote sysb = (SysUserBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(SysUserBeanRemote.class);

				if (user == null || pass == null
						|| !sysb.isValidCredentials(user, pass)) {
					response.setStatus(1001);

					JsonParser parser = new JsonParser();

					String jsString = "{\"ERROR\":\"invalid username or password\"}";
					response.getWriter().write(
							parser.parse(jsString).toString());

					// response.sendError(1001,"invalid username or password");
					return;
				}

			}

			switch (op) {

			case CPSMobileConstants.LOGIN:

				SysUserBeanRemote sysb = (SysUserBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(SysUserBeanRemote.class);
				boolean validCredentials = sysb.isValidCredentials(user, pass);
				SysUser sysUser = null;
				String resp = null;
				if (validCredentials) {
					HttpSession session = request.getSession();
					sysUser = sysb.getUser(user);
					session.setAttribute("user", sysUser);
					resp = JSonUtil.getInstance().toJson(getUser(sysUser));
				}
				// response.setContentType("application/json");
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.REGISTER:

				CountryBeanRemote cbr = (CountryBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(CountryBeanRemote.class);

				user = request.getParameter("user");
				pass = request.getParameter("pass");
				String name = request.getParameter("name");
				String last_name = request.getParameter("last_name");
				String address = request.getParameter("address");
				String email = request.getParameter("email");
				String birthdate = request.getParameter("bd");
				String country = request.getParameter("country");
				String msisdn = request.getParameter("msisdn");
				String city = request.getParameter("city");
				locale = request.getParameter("lang");
				if (locale == null) {
					locale = "es";
				}
				int emailAd = ("true".equals(request.getParameter("emailad"))) ? 1
						: 0;
				City cty = cbr.getCity(city);

				pass = RampENC.decrypt(pass);

				SysUser su = new SysUser();
				su.setLogin(user);
				su.setIdsysUserType(SysUserType.USER);
				su.setPass(pass);
				su.setName(name);
				su.setLastName(last_name);
				su.setAddress(address);
				su.setEmail(email);
				su.setCountryIdcountry(country);
				su.setFavoriteMsisdn(msisdn);
				su.setCity(cty);
				su.setEmailNotification(emailAd);
				su.setBalance(new BigDecimal(0));
				SimpleDateFormat formatoDeFecha = new SimpleDateFormat(
						"yyyy-MM-dd");
				su.setBirthDate(formatoDeFecha.parse(birthdate));

				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);

				sysb.registerUser(su);
				SysUser sysUserNew = sysb.getUser(su.getLogin());

				resp = JSonUtil.getInstance().toJson(
						new ResponseBean(0, "OK", getUser(su)));
				response.getWriter().print(resp);

				subject = Util.getMessage("registerMailSubject", locale);
				body = Util
						.getTemplateString("newsletter/emailWelcome.html",
								locale).replace("{cps_login}", su.getLogin())
						.replace("{cps_pass}", su.getPass())
						.replace("{cps_cu}", sysUserNew.getCu())
						.replace("{cps_name}", su.getName())
						.replace("{cps_lastName}", su.getLastName());
				sender = Util.getMessage("registerMailFrom", locale);
				MailSender.sendMail(subject, body, sender, email);

				break;

			case CPSMobileConstants.GET_USER_INFO:
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				HttpSession session = request.getSession();
				sysUser = (SysUser) session.getAttribute("user");
				resp = null;
				if (sysUser != null) {
					sysUser = sysb.getUser(sysUser.getLogin());
					resp = JSonUtil.getInstance().toJson(sysUser);
				} else {
					sysUser = sysb.getUser(user);
					resp = JSonUtil.getInstance().toJson(getUser(sysUser));
				}
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.GET_COUNTRY_LIST:
				String numP = request.getParameter("np");
				if (numP == null) {
					response.getWriter().print(countrys);
				} else {
					int np = Integer.parseInt(numP);
					if (np != countrysNum) {
						response.getWriter().print(countrys);
					} else {
						response.getWriter().print("OK");
					}
				}
				break;

			case CPSMobileConstants.GET_CITIES_LIST:
				country = request.getParameter("idc");
				CountryBeanRemote cntb = (CountryBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(CountryBeanRemote.class);
				ArrayList<City> cities = cntb.getCities(country);
				for (City c : cities) {
					c.setName(escapeHtml4(c.getName()));
				}
				resp = JSonUtil.getInstance().toJson(cities);
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.GET_ACTIVE_PARKING:
				ParkingBeanRemote pbr = (ParkingBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(ParkingBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				ArrayList<ActiveParking> act = pbr.getActiveParkingByUser(sysb
						.getUser(user).getIdsysUser());
				for (ActiveParking ac : act) {
					ac.setZoneIdzone(ac.getZoneIdzone().substring(3));
					ac.setPlaceIdplace(ac.getPlaceIdplace().substring(3));
				}
				resp = JSonUtil.getInstance().toJson(act);
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.END_PARKING:
				pbr = (ParkingBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(ParkingBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				String plate = request.getParameter("plate");
				su = sysb.getUser(user);
				ActiveParking ap = (ActiveParking) pbr.endParking(plate, su);
				resp = JSonUtil.getInstance().toJson(ap);
				su = sysb.getUser(user);

				ParkingResume pr = new ParkingResume(ap.getCurrentMins(),
						ap.getMaxMins(), String.valueOf(ap.getMinuteValue()),
						ap.getMsisdn(), sdf.format(ap.getStartTime()),
						sdf.format(new Date()), ap.getPlaceIdplace().substring(
								3), ap.getVehiclePlate(), String.valueOf(ap
								.getMinuteValue().multiply(
										new BigDecimal(ap.getCurrentMins()))),
						String.valueOf(su.getBalance()), String.valueOf(su
								.getBalance()), String.valueOf(ap
								.getMinuteValue()));

				resp = JSonUtil.getInstance().toJson(pr);
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.START_PARKING:

				pbr = (ParkingBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(ParkingBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);

				plate = request.getParameter("plate");
				msisdn = request.getParameter("num");
				String idCountry = request.getParameter("idc");
				String idPlace = request.getParameter("idp");

				int enableSMS = Integer.parseInt(request.getParameter("sms"));

				su = sysb.getUser(user);
				Object obj = pbr.startParking(Platform.MOBILE, plate, msisdn,
						idCountry, idPlace, sysb.getUser(user), enableSMS);

				if (obj instanceof Integer) {
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(((Integer) obj).intValue(),
									"Error", null));
				} else {
					ap = (ActiveParking) obj;
					pr = new ParkingResume(ap.getCurrentMins(),
							ap.getMaxMins(),
							String.valueOf(ap.getMinuteValue()),
							ap.getMsisdn(), sdf.format(ap.getStartTime()),
							sdf.format(new Date()), ap.getPlaceIdplace()
									.substring(3), ap.getVehiclePlate(),
							String.valueOf(ap.getMinuteValue().multiply(
									new BigDecimal(ap.getCurrentMins()))),
							String.valueOf(su.getBalance()), String.valueOf(su
									.getBalance()), String.valueOf(ap
									.getMinuteValue()));
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(0, "OK", pr));
				}

				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.GET_ZONES:

				idCountry = request.getParameter("idc");
				city = request.getParameter("idcity");

				ZoneBeanRemote zbr = EJBServiceLocator.getInstance()
						.getEjbService(ZoneBeanRemote.class);
				PlaceBeanRemote plbr = EJBServiceLocator.getInstance()
						.getEjbService(PlaceBeanRemote.class);
				pbr = (ParkingBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(ParkingBeanRemote.class);
				ArrayList<Zone> zones = null;

				if (city == null || city.equals("null") || city.equals("")
						|| city.equals("All") || city.equals("Todas")) {
					zones = zbr.getZones(idCountry);
				} else {
					sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
							.getEjbService(SysUserBeanRemote.class);
					zones = zbr.getZonesByCity(sysb.getUser(user), idCountry,
							city);
				}

				Collections.sort(zones, new ZoneComparator());
				ArrayList<com.cps.bean.Zone> zns = processZones(zones,
						idCountry);// new ArrayList<com.cps.bean.Zone>();
				/*
				 * CityParkingBeanRemote ctBeanRemote = (CityParkingBeanRemote)
				 * EJBServiceLocator .getInstance().getEjbService(
				 * CityParkingBeanRemote.class);
				 * 
				 * Map<String, Cvalorubicaws> pinfo = ctBeanRemote
				 * .getParkingsInfo(zones); for (Zone z : zones) {
				 * 
				 * if (z.getActive() == 0) continue; com.cps.bean.Zone zpl = new
				 * com.cps.bean.Zone(); // ArrayList<ActiveParking> actbz = pbr
				 * // .getActiveParkingByZone(z.getIdzone());
				 * 
				 * Cvalorubicaws zinfo = pinfo.get(z.getIdzone());//
				 * ctBeanRemote.getParkingInfo(z.getIdzone());
				 * zpl.setName(z.getName()); //
				 * zz.setPlaces(plbr.getPlacesZone(z
				 * .getIdzone()).size()-actbz.size()); if (zinfo != null) {
				 * zpl.setPlaces(String.valueOf(zinfo.getCupos())); //
				 * zpl.setName(zinfo.getNombreparqueadero()); } else { //
				 * zpl.setPlaces("(--)");
				 * 
				 * }
				 * 
				 * 
				 * // zpl.setPlaces(String.valueOf(plbr.getPlacesZone( //
				 * z.getIdzone()).size() - actbz.size()));
				 * 
				 * zpl.setIdzone(z.getIdzone().substring(3));
				 * zpl.setClientIdclient(z.getClientIdclient());
				 * zpl.setCloseTime(z.getCloseTime());
				 * zpl.setCountryIdCountry(idCountry);
				 * zpl.setIdzone(z.getIdzone());
				 * zpl.setLatitude(z.getLatitude());
				 * zpl.setLongitude(z.getLongitude());
				 * zpl.setMinuteValue(z.getMinuteValue().toString()); //
				 * zpl.setName(z.getName()); zpl.setOpenTime(z.getOpenTime());
				 * zpl.setAddress(z.getAddress());
				 * zpl.setSchedule(escapeHtml4(z.getSchedule()));
				 * zpl.setEmail(z.getEmail()); zns.add(zpl); }
				 */
				resp = JSonUtil.getInstance().toJson(zns);
				response.getWriter().print(resp);

				break;

			case CPSMobileConstants.GET_ZONES_TIMETABLE:

				idCountry = request.getParameter("idc");
				String idZone = request.getParameter("idz");

				zbr = EJBServiceLocator.getInstance().getEjbService(
						ZoneBeanRemote.class);
				ArrayList<ZoneRestriction> zr = zbr.getTimeRestrictOfWeek(
						idZone, idCountry);

				for (ZoneRestriction z : zr) {
					z.setZoneIdzone(z.getZoneIdzone().substring(3));
				}

				resp = JSonUtil.getInstance().toJson(zr);
				response.getWriter().print(resp);

				break;

			case CPSMobileConstants.UPDATE_PASSWORD:

				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);

				sysUser = sysb.getUser(user);

				String oldpass = request.getParameter("oldpass");
				String npass = request.getParameter("npass");

				oldpass = RampENC.decrypt(oldpass);
				npass = RampENC.decrypt(npass);

				locale = request.getParameter("lang");
				System.out.println(locale);
				if (locale == null) {
					locale = "es";
				}
				subject = Util.getMessage("changePassSubject", locale);
				body = Util
						.getTemplateString("newsletter/emailPassword.html",
								locale).replace("{cps_login}", user)
						.replace("{cps_newPass}", npass)
						.replace("{cps_name}", sysUser.getName())
						.replace("{cps_lastName}", sysUser.getLastName());
				sender = Util.getMessage("changeMailFrom", locale);

				if (sysb.isValidCredentials(user, oldpass)) {
					resp = JSonUtil.getInstance()
							.toJson(new ResponseBean(sysb.setPassword(user,
									pass, npass), "OK", null));
					MailSender.sendMail(subject, body, sender,
							sysUser.getEmail());
				} else {
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(1, "Failed", null));
				}
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.ADD_VEHICLE:
				VehicleBeanRemote vbr = (VehicleBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(VehicleBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				plate = request.getParameter("plate");

				String type = request.getParameter("type");
				String brand = request.getParameter("brand");
				String description = request.getParameter("description");

				if (sysb.isValidCredentials(user, pass)) {

					BigInteger vehicleType = null;
					BigInteger vehicleBrand = null;
					String vehicleDescription = null;
					if (type != null && !"".equals(type)) {
						vehicleType = new BigInteger(type);
					}
					if (brand != null && !"".equals(brand)) {
						vehicleBrand = new BigInteger(brand);
					}
					if (description != null && !"".equals(description)) {
						vehicleDescription = description;
					}
					// Send response
					if (vehicleType != null || vehicleBrand != null
							|| vehicleDescription != null) {
						resp = JSonUtil
								.getInstance()
								.toJson(

								new ResponseBean(
										vbr.registerVehicleFull(plate,
												vehicleType, vehicleBrand,
												vehicleDescription,
												sysb.getUser(user)), "OK", null));
					} else {
						resp = JSonUtil.getInstance().toJson(
								new ResponseBean(vbr.registerVehicle(plate,
										sysb.getUser(user)), "OK", null));
					}
				} else {
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(1, "Failed", null));
				}
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.PIN_RELOAD:
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				String pin = request.getParameter("pin");
				if (sysb.isValidCredentials(user, pass)) {
					int rta = sysb.reloadBalance(sysb.getUser(user), pin);
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(rta, "OK", sysb));
				} else {
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(1, "Failed", null));
				}
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.UPDATE_USER:
				cbr = (CountryBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(CountryBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);

				name = request.getParameter("name");
				last_name = request.getParameter("last_name");
				address = request.getParameter("address");
				email = request.getParameter("email");
				birthdate = request.getParameter("bd");
				country = request.getParameter("country");
				msisdn = request.getParameter("msisdn");
				city = request.getParameter("city");
				cty = cbr.getCity(city);

				su = sysb.getUser(user);
				// su.setIdsysUserType(SysUserType.USER);
				su.setName(name);
				su.setLastName(last_name);
				su.setAddress(address);
				su.setEmail(email);
				su.setCountryIdcountry(country);
				su.setFavoriteMsisdn(msisdn);
				su.setCity(cty);
				formatoDeFecha = new SimpleDateFormat("yyyy-MM-dd");
				su.setBirthDate(formatoDeFecha.parse(birthdate));

				sysb.updateUserNoBalance(su);
				SysUser usr = sysb.getUser(user);

				resp = JSonUtil.getInstance().toJson(
						new ResponseBean(0, "OK", getUser(usr)));
				response.getWriter().print(resp);

				break;

			case CPSMobileConstants.GIVE_BALANCE:

				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				String repass = request.getParameter("repass");
				repass = RampENC.decrypt(repass);
				String cu = request.getParameter("cu");
				BigDecimal ammout = new BigDecimal(request.getParameter("amm"));
				SysUser sudo = sysb.getUser(user);
				if (sysb.isValidCredentials(user, repass)
						&& (sudo.getBalance().compareTo(ammout) == 1
								|| sudo.getBalance().compareTo(ammout) == 0 || sudo
								.getLogin().equals("cityparking"))) {
					sysUser = sysb.getUserByCU(cu);
					if (sysUser != null) {
						int rt = sysb.reloadBalance(sysUser, ammout);
						if (rt == CodeResponse.RELOAD_RESIDUE_OK) {
							sudo.setBalance(sudo.getBalance().subtract(ammout));
							sysb.updateUser(sudo);
							sysb.registryUserTransaction(sudo.getIdsysUser(),
									sysUser.getIdsysUser(), ammout);
						}
						resp = JSonUtil.getInstance().toJson(
								new ResponseBean(rt, "OK", sudo));
					} else {
						resp = JSonUtil.getInstance().toJson(
								new ResponseBean(-1, "Fail", null));
					}
				} else {
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(-2, "Fail", null));
				}
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.GET_VEHICLES:
				vbr = (VehicleBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(VehicleBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				if (sysb.isValidCredentials(user, pass)) {
					SysUser suv = sysb.getUser(user);
					ArrayList<SysUserVehicle> sysvh = vbr
							.getSysUserVehicles(suv);
					ArrayList<Vehicle> vhsf = new ArrayList<Vehicle>();
					for (SysUserVehicle v : sysvh) {
						Vehicle vhc = new Vehicle();
						vhc.setPlate(v.getVehiclePlate());
						vhc.setDateReg(df.format(new Date()));
						vhc.setOwner(v.getOwner() == 1);
						vhsf.add(vhc);
					}
					resp = JSonUtil.getInstance().toJson(vhsf);
					response.getWriter().print(resp);
				}
				break;

			case CPSMobileConstants.GET_GPS_ZONES:

				pbr = (ParkingBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(ParkingBeanRemote.class);
				zbr = EJBServiceLocator.getInstance().getEjbService(
						ZoneBeanRemote.class);

				String lat = request.getParameter("lat");
				String lon = request.getParameter("lon");
				String idc = "472";
				ArrayList<Zone> znes = zbr.getZonasCercanas(lat, lon);
				zns = processZones(znes, idc);// new
												// ArrayList<com.cps.bean.Zone>();

				/*
				 * ctBeanRemote = (CityParkingBeanRemote) EJBServiceLocator
				 * .getInstance().getEjbService( CityParkingBeanRemote.class);
				 * for (Zone z : znes) {
				 * 
				 * com.cps.bean.Zone zz = new com.cps.bean.Zone();
				 * 
				 * ArrayList<ActiveParking> actbz = pbr
				 * .getActiveParkingByZone(z.getIdzone()); ClientBeanRemote cb =
				 * (ClientBeanRemote) EJBServiceLocator .getInstance()
				 * .getEjbService(ClientBeanRemote.class); plbr =
				 * EJBServiceLocator.getInstance().getEjbService(
				 * PlaceBeanRemote.class); Client cl =
				 * cb.getClient(z.getClientIdclient()); //
				 * cl.getCountry().getIdcountry(); Cvalorubicaws zinfo =
				 * ctBeanRemote.getParkingInfo(z .getIdzone());
				 * zz.setName(z.getName());
				 * 
				 * //
				 * zz.setPlaces(plbr.getPlacesZone(z.getIdzone()).size()-actbz
				 * .size()); if (zinfo != null) {
				 * zz.setPlaces(String.valueOf(zinfo.getCupos())); //
				 * zz.setName(zinfo.getNombreparqueadero()); } else {
				 * zz.setPlaces("(--)");
				 * 
				 * }
				 * 
				 * 
				 * // ArrayList<ActiveParking> actbz = pbr //
				 * .getActiveParkingByZone(z.getIdzone()); //
				 * zpl.setPlaces(String.valueOf(plbr.getPlacesZone( //
				 * z.getIdzone()).size() - actbz.size())); //
				 * zpl.setIdzone(z.getIdzone().substring(3)); //
				 * zpl.setClientIdclient(z.getClientIdclient()); //
				 * zpl.setCloseTime(z.getCloseTime()); //
				 * zpl.setCountryIdCountry(idCountry); //
				 * zpl.setIdzone(z.getIdzone()); //
				 * zpl.setLatitude(z.getLatitude()); //
				 * zpl.setLongitude(z.getLongitude()); //
				 * zpl.setMinuteValue(z.getMinuteValue().toString()); //
				 * zpl.setName(z.getName()); //
				 * zpl.setOpenTime(z.getOpenTime()); //
				 * zpl.setAddress(z.getAddress()); //
				 * zpl.setSchedule(escapeHtml4(z.getSchedule())); //
				 * zns.add(zpl);
				 * 
				 * zz.setIdzone(z.getIdzone().substring(3));
				 * zz.setClientIdclient(z.getClientIdclient());
				 * zz.setCloseTime(z.getCloseTime());
				 * zz.setCountryIdCountry(idc);
				 * zz.setMinuteValue(String.valueOf(z.getMinuteValue()));
				 * zz.setLatitude(z.getLatitude());
				 * zz.setLongitude(z.getLongitude()); //
				 * zz.setName("TestName"zinfo.getNombreparqueadero());
				 * zz.setAddress(z.getAddress());
				 * zz.setSchedule(escapeHtml4(z.getSchedule()));
				 * zz.setOpenTime(z.getOpenTime());
				 * 
				 * // zinfo.getDireccionparqueadero(); //
				 * zinfo.getNombreparqueadero();
				 * 
				 * zns.add(zz);
				 * 
				 * }
				 */

				resp = JSonUtil.getInstance().toJson(zns);
				response.getWriter().print(resp);

				break;

			case CPSMobileConstants.DELETE_VEHICLE:
				vbr = (VehicleBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(VehicleBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				plate = request.getParameter("plate");
				resp = JSonUtil.getInstance().toJson(
						new ResponseBean(vbr.deleteVehicle(plate,
								sysb.getUser(user)), "OK", null));
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.SOCIAL_NETWORK_URL:
				String sn = request.getParameter("sn");
				SocialNetworkRemote snr = (SocialNetworkRemote) EJBServiceLocator
						.getInstance().getEjbService(SocialNetworkRemote.class);
				response.getWriter().print(snr.getSNUrl(sn));
				break;

			case CPSMobileConstants.ADD_SUGGESTION:
				String sug = request.getParameter("sug");
				SuggestionBeanRemote sbr = (SuggestionBeanRemote) EJBServiceLocator
						.getInstance()
						.getEjbService(SuggestionBeanRemote.class);

				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				sbr.addSuggestion(sug, sysb.getUser(user));
				resp = JSonUtil.getInstance().toJson(
						new ResponseBean(0, "OK", null));
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.GET_NOTIFICATIONS:
				NotificationBeanRemote nbr = (NotificationBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(
								NotificationBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				resp = JSonUtil.getInstance().toJson(
						nbr.getUserNotifications(sysb.getUser(user)));
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.SET_READ:
				nbr = (NotificationBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(NotificationBeanRemote.class);
				String idn = request.getParameter("idn");
				nbr.setRead(idn);
				resp = JSonUtil.getInstance().toJson(
						new ResponseBean(0, "OK", null));
				response.getWriter().print(resp);
				break;

			case CPSMobileConstants.RESET_PASSWORD:
				SysUserBeanRemote subr = (SysUserBeanRemote) EJBServiceLocator
						.getInstance().getEjbService(SysUserBeanRemote.class);
				String mail = request.getParameter("mail");
				com.cps.entity.bean.SysUser us = subr.getUserByEmail(mail);
				locale = request.getParameter("lang");
				if (locale == null) {
					locale = "es";
				}
				if (us != null) {

					long expTime = Calendar.getInstance(
							TimeZone.getTimeZone("GMT")).getTimeInMillis()
							+ (1000 * 60 * 45);
					String recoverInfo = expTime + "," + us.getIdsysUser();
					String encodedInfo = RampENC.encrypt(recoverInfo);

					URL url = new URL(request.getRequestURL().toString());
					URL newUrl = new URL(url.getProtocol(), url.getHost(),
							url.getPort(), "/service");
					String urlstr = newUrl.toString()
							+ "/faces/security/passwordRecovery.xhtml"
							+ "?check=" + encodedInfo;
					// String newPass = Util.generatePassword();
					/*
					 * String body = (new
					 * StringBuilder("<strong>User: </strong>"))
					 * .append(us.getLogin())
					 * .append("<BR><strong>New Password: </strong>")
					 * .append(newPass) .append("<BR><strong>CU/UC: </strong>")
					 * .append(us.getCu()) .append(
					 * "<br><strong>ATTENTION: PASSWORD FIELD IS CASE SENSITIVE.</strong>"
					 * ) .toString();
					 */
					subject = Util.getMessage("forgotMailSubject", locale);
					body = Util
							.getTemplateString("newsletter/emailForgot.html",
									locale)
							.replace("{cps_login}", us.getLogin())
							.replace("{rlink}", urlstr)
							.replace("{cps_cu}", us.getCu())
							.replace("{cps_name}", us.getName())
							.replace("{cps_lastName}", us.getLastName());
					sender = Util.getMessage("forgotMailFrom", locale);
					// int respPass = subr.setPassword(us, newPass);
					// if (respPass == -117) {
					MailSender.sendMail(subject, body, sender, mail);
					// }
					response.getWriter().print(String.valueOf(-117));
				} else {
					response.getWriter().print("-118");
				}
				break;

			case CPSMobileConstants.GET_ZONE_INFO_BY_ACT_PARKING:
				zbr = (ZoneBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(ZoneBeanRemote.class);
				String actp = request.getParameter("act");
				resp = JSonUtil.getInstance().toJson(
						zbr.getZoneByActiveParking(actp));
				response.getWriter().print(resp);
				break;
			case CPSMobileConstants.VALIDATE_PAY_CITY_PARKING:

			{
				String parkingCode = request.getParameter("parkcode");
				String paycode = request.getParameter("paycode");
				String deviceid = request.getParameter("device");
				CityParkingBeanRemote cpkr = EJBServiceLocator.getInstance()
						.getEjbService(CityParkingBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				PaymentResponse payresp = cpkr.validateParking(parkingCode,
						paycode);

				if (payresp.getValue() == -301) {

					// SimpleDateFormat sdf = new
					// SimpleDateFormat("yyyy-MM-dd HH:ss");
					sysUser = sysb.getUser(user);
					sdf.setTimeZone(TimeZone.getTimeZone("GMT-5"));

					final String bodyMail = "cliente:" + sysUser.getLogin()
							+ "<BR>CU:" + sysUser.getCu()
							+ "<BR> codigo parqueo: " + parkingCode
							+ "<BR>codigo de pago:" + paycode
							+ "<BR> Hora de la transacciï¿½n:  "
							+ sdf.format(new Date());

					final String mailFrom = Util.getMessage("forgotMailFrom",
							locale);
					new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								MailSender
										.sendMail(
												"El cliente no pudo pagar parqueo",
												bodyMail,
												mailFrom,
												"jotero@cpsparking.ca,apereira@cpsparking.ca,soportedesarrollo@city-parking.com");
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					}).start();

					// int respPass = subr.setPassword(us, newPass);
					// if (respPass == -117) {
				}
				resp = JSonUtil.getInstance().toJson(payresp);
				response.getWriter().print(resp);
				// response.getWriter().print(String.valueOf(res));
			}
				break;
			case CPSMobileConstants.CONFIRM_PAY_CITY_PARKING:

			{

				String id = request.getParameter("id");
				String placeId = request.getParameter("placeid");
				String devId = request.getParameter("device");

				int deviceId = 2;
				if (devId != null && isInteger(devId))
					deviceId = Integer.parseInt(devId);
				double value = Double
						.parseDouble(request.getParameter("value"));

				String validationNumber = request
						.getParameter("validationNumber");
				String operationResult = request
						.getParameter("operationResult");
				String token = request.getParameter("token");
				String initparkingdate = request.getParameter("initparking");
				String endparkingdate = request.getParameter("endparking");

				CityParkingBeanRemote cpkr = EJBServiceLocator.getInstance()
						.getEjbService(CityParkingBeanRemote.class);
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);

				int res = cpkr.payParking(sysb.getUser(user), value, id,
						validationNumber, operationResult, token, placeId,
						initparkingdate, endparkingdate, deviceId);
				response.getWriter().print(String.valueOf(res));
			}
				break;
			case CPSMobileConstants.INIT_PAYMENT_CITY_PARKING_ZP: {

				double value = Double
						.parseDouble(request.getParameter("value"));
				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);

				resp = JSonUtil.getInstance().toJson(
						new ResponseBean(1, "Failed", null));
				if (sysb.isValidCredentials(user, pass)) {
					CityParkingBeanRemote cityParkingBeanRemote = EJBServiceLocator
							.getInstance().getEjbService(
									CityParkingBeanRemote.class);

					sysUser = sysb.getUser(user);
					String[] responseZP = cityParkingBeanRemote.initPayment(
							sysUser, value);
					String initId = responseZP[0];
					String payId = responseZP[1];
					if (!payId.contains("-1")) {
						cityParkingBeanRemote
								.registryTransaction(new CityParkingTransaction(
										payId, new Date(), sysUser
												.getIdsysUser(), sysUser
												.getEmail(), "Started", value,
										"CO"));
						// String outcome=
						// URLEncoder.encode("www.zonapagos.com/t_cityparkingmovil/pago.asp?estado_pago=iniciar_pago&identificador="+payId,"UTF-8");
						// resp = // Do your thing?
						// String
						// urlzp="https://www.zonapagosdemo.com/t_cityparkingmovil/pago.asp?estado_pago=iniciar_pago&identificador="+
						// initId;
						String urlzp = "https://www.zonapagos.com/t_cityparkingmovil/pago.asp?estado_pago=iniciar_pago&identificador="
								+ initId;
						resp = JSonUtil.getInstance().toJson(
								new ResponseBean(200, "OK", urlzp));
						// HttpServletResponse response =
						// (HttpServletResponse)facesContext.getExternalContext().getResponse();
						// context.addCallbackParam("payId", payId);
						// context.addCallbackParam("outcome", outcome);
						// context.addCallbackParam("outcome", outcome);

					}

					// resp = JSonUtil.getInstance().toJson(new
					// ResponseBean(vbr.registerVehicle(plate,
					// sysb.getUser(user)), "OK", null));
				}

				response.getWriter().print(resp);
			}
				break;
			case CPSMobileConstants.CHECK_PENDINGS_PAYMENT_CITY_PARKING_ZP: {

				sysb = (SysUserBeanRemote) EJBServiceLocator.getInstance()
						.getEjbService(SysUserBeanRemote.class);
				if (sysb.isValidCredentials(user, pass)) {
					sysUser = sysb.getUser(user);
					CityParkingBeanRemote cityParkingBeanRemote = EJBServiceLocator
							.getInstance().getEjbService(
									CityParkingBeanRemote.class);
					ArrayList<CityParkingTransaction> transactions = cityParkingBeanRemote
							.getTransactionsByUserId(Long.valueOf(sysUser
									.getIdsysUser()));

					ArrayList<PagosV3> pendingList = new ArrayList<PagosV3>();
					for (CityParkingTransaction trans : transactions) {
						if (trans.getStateTransaction().toLowerCase()
								.contains("pending")) {
							PayItem item = cityParkingBeanRemote
									.checkPayment(trans.getIdTransaction());
							if (item == null)
								continue;
							ArrayOfPagosV3 array = item.getResPagosV3();

							System.out.println("Check items- result="
									+ item.getVerificarPagoV3Result());
							if (item.getVerificarPagoV3Result() > 0
									&& array != null) {
								PagosV3 payment = array.getPagosV3().get(0);
								int payStatement = payment.getIntEstadoPago();
								String status = trans.getStateTransaction();
								if (payStatement == 999 || payStatement == 888) {
									payment.getStrIdPago();
									status = "Pending to end";
									pendingList.add(payment);
								}

								System.out.println("Check items- status="
										+ status);
								/*
								 * if (!tr.getStateTransaction().equals(status))
								 * { boolean updated
								 * =cityParkingBean.updateTransactionStatus
								 * (String.valueOf(tr.getIdTransaction()),
								 * status);
								 * System.out.println("Check items- updated="
								 * +updated); }
								 */

							}

						}
					}

					int pendings = 200;
					String msg = null;
					if (pendingList.size() > 1) {
						pendings = -101;
						String[] refNumbers = new String[pendingList.size()];
						String[] refTrans = new String[pendingList.size()];

						for (int i = 0; i < pendingList.size(); i++) {
							refNumbers[i] = pendingList.get(i).getStrIdPago();
							refTrans[i] = pendingList.get(i)
									.getStrCodigoTransaccion();

						}
						String transStr = Arrays.toString(refTrans)
								.replace("[", " ").replace("]", " ")
								+ ".";
						transStr = transStr.contains("-1") ? "" : ": "
								+ transStr;
						String s = "El lï¿½mite mï¿½nimo de transacciones pendientes ha sido alcanzado. En este momento sus Nï¿½meros de Referencia"
								+ Arrays.toString(refNumbers).replace("[", " ")
										.replace("]", " ")
								+ " presentan un proceso de pago cuya"
								+ " transacciï¿½n se encuentra PENDIENTE de recibir confirmaciï¿½n"
								+ " por parte de su entidad financiera, Por favor puede  esperar unos minutos y volver "
								+ "a consultar mas tarde para verificar si su pago fue confirmado "
								+ "de forma exitosa. Si desea mayor informaciï¿½n sobre el estado actual de "
								+ "sus operaciones puede comunicarse a nuestras lï¿½neas de atenciï¿½n al "
								+ "cliente los  telï¿½fonos (1) 6210372 (1) 6210373 o enviar sus inquietudes al correo soporte@zonavirtual.com "
								+ "y preguntar por el estado de las transacciones "
								+ transStr;
						msg = new String(s.getBytes("UTF-8"), "UTF-8");

					} else if (pendingList.size() > 0) {
						pendings = -100;

						String transStr = pendingList.get(0)
								.getStrCodigoTransaccion();
						transStr = transStr.contains("-1") ? "" : ": "
								+ transStr;
						String s = "En este momento su Numero de Referencia "
								+ pendingList.get(0).getStrIdPago()
								+ " presenta un proceso de pago cuya transacciï¿½n se encuentra PENDIENTE "
								+ "de recibir confirmaciÏ¿½n por parte de su entidad financiera, sin embargo le estï¿½ permitido realizar otra transacciï¿½n o "
								+ "puede  esperar unos minutos y volver a consultar mas tarde para verificar "
								+ "si su pago fue confirmado de forma exitosa. Si desea mayor informaciï¿½n sobre el "
								+ "estado actual de su operaciï¿½n puede comunicarse a nuestras lï¿½neas de atenciï¿½n al"
								+ " cliente a los  telï¿½fonos (1) 6210372 (1) 6210373 o enviar sus inquietudes al correo"
								+ " soporte@zonavirtual.com y preguntar por el estado de la transacciï¿½n"
								+ transStr + ".";
						msg = new String(s.getBytes("UTF-8"), "UTF-8");
					}

					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(pendings, "OK", msg));
					response.getWriter().print(resp);

				}
			}
				break;

			case CPSMobileConstants.CHECKVERSION: {

				DeviceVersionRemote deviceRmt = EJBServiceLocator.getInstance()
						.getEjbService(DeviceVersionRemote.class);

				Integer deviceId = Integer.parseInt(request
						.getParameter("deviceid"));
				String version = request.getParameter("version");
				boolean lastversion = deviceRmt.checklastDeviceVersion(
						deviceId, version);

				response.getWriter().print(lastversion);
			}
				break;

			case CPSMobileConstants.REGISTERDRIVER: {
				String cd_contact_name = request.getParameter("contact_name");
				String cd_contact_last = request
						.getParameter("contact_last_name");
				String cd_contact_phone = request.getParameter("contact_phone");
				String cd_origin = request.getParameter("orig");
				String cd_destination = request.getParameter("dest");
				String cd_plate = request.getParameter("plate");
				String cd_documents = request.getParameter("documents");
				String cd_date_service = request.getParameter("date_service");
				String cd_comments = request.getParameter("comments");
				locale = request.getParameter("lang");

				try {
					if (cd_contact_name == "" || cd_contact_last == ""
							|| cd_contact_phone == "" || cd_origin == ""
							|| cd_destination == "" || cd_plate == ""
							|| cd_documents == "" || cd_date_service == "") {

						resp = JSonUtil.getInstance().toJson(
								new ResponseBean(0,
										"Error, Please check fields", null));
						response.getWriter().print(resp);
					} else {

						CityDriverBeanRemote cdbr = (CityDriverBeanRemote) EJBServiceLocator
								.getInstance().getEjbService(
										CityDriverBeanRemote.class);
						sysb = (SysUserBeanRemote) EJBServiceLocator
								.getInstance().getEjbService(
										SysUserBeanRemote.class);

						sysUser = sysb.getUser(user);
						CityDriver cd = new CityDriver();

						cd.setSysUser(sysUser);
						cd.setContactName(cd_contact_name);
						cd.setContactLastName(cd_contact_last);
						cd.setContactPhone(cd_contact_phone);
						cd.setAddressOrigin(cd_origin);
						cd.setAddressDest(cd_destination);
						cd.setPlate(cd_plate);
						cd.setDocuments(new BigInteger(cd_documents));
						SimpleDateFormat cdf = new SimpleDateFormat(
								"yyyy-MM-dd hh:mm:ss");
						cd.setDateService(cdf.parse(cd_date_service));
						cd.setState(new BigInteger("1"));
						cd.setComments(cd_comments);

						CityDriver cdr = cdbr.registerCityDriver(cd);

						resp = JSonUtil.getInstance()
								.toJson(new ResponseBean(1, "OK", cdr
										.getIdCityDriver()));
						response.getWriter().print(resp);
					}
				} catch (Exception e) {
					resp = JSonUtil.getInstance().toJson(
							new ResponseBean(0, "Error in driver save", null));
					response.getWriter().print(resp);

					LOGGER.error("Error Registrando Usuario Driver:", e);
					e.printStackTrace();
				}
			}
			break;
			case CPSMobileConstants.GET_ZONE_PRICE: {
				 zbr = EJBServiceLocator.getInstance()
						.getEjbService(ZoneBeanRemote.class);
				 
				ArrayList<PriceRestrict> priceRstList = zbr.getPriceRestrictionList();
				HashMap<String,Object> objmap= new HashMap<String, Object>();
				HashMap<String,PriceResponse> objPricemap= new HashMap<String, PriceResponse>();
				
				
				for(PriceRestrict prItem :priceRstList)
				{
					String idzone= prItem.getIdZone();
					PriceResponse priceResp= objPricemap.get(idzone);
					if(priceResp==null )
					{
						priceResp= new PriceResponse();
						priceResp.setIdzone(idzone);
						objPricemap.put(idzone, priceResp);
					}
					
					priceResp.getPrice().getVtype_id().put(String.valueOf(prItem.getVehicleType().getVTypeId()), 
							new VehicleTypeID(String.valueOf(prItem.getFractionTime()),prItem.getDescription(),
									String.valueOf(prItem.getFractionValue())));
				}
				
				objmap.put("obj", objPricemap.values());
				
				resp = JSonUtil.getInstance().toJson(
						new ResponseBean(0, "ok", objmap));
				
				response.getWriter().print(JSonUtil.getInstance().toJson(objmap));
				

			}

				break;

			}

		} catch (Exception e) {

			Util.traceError(LOGGER, e, request);
			e.printStackTrace();
		}

	}

	private static ArrayList<com.cps.bean.Zone> processZones(
			ArrayList<Zone> zones, String idCountry) throws Exception {
		CityParkingBeanRemote ctBeanRemote = (CityParkingBeanRemote) EJBServiceLocator
				.getInstance().getEjbService(CityParkingBeanRemote.class);
		Map<String, Cvalorubicaws> pinfo = ctBeanRemote.getParkingsInfo(zones);
		ArrayList<com.cps.bean.Zone> zns = new ArrayList<com.cps.bean.Zone>();
		for (Zone z : zones) {

			if (z.getActive() == 1) {
				com.cps.bean.Zone zpl = new com.cps.bean.Zone();
				// ArrayList<ActiveParking> actbz = pbr
				// .getActiveParkingByZone(z.getIdzone());

				Cvalorubicaws zinfo = pinfo.get(z.getIdzone());// ctBeanRemote.getParkingInfo(z.getIdzone());
				// zz.setPlaces(plbr.getPlacesZone(z.getIdzone()).size()-actbz.size());
				if (zinfo != null) {
					zpl.setPlaces(String.valueOf(zinfo.getCupos()));
					// zpl.setName(zinfo.getNombreparqueadero());
				} else {
					//
					zpl.setPlaces("(--)");

				}

				/*
				 * zpl.setPlaces(String.valueOf(plbr.getPlacesZone(
				 * z.getIdzone()).size() - actbz.size()));
				 */
				zpl.setIdzone(z.getIdzone().substring(3));
				zpl.setClientIdclient(z.getClientIdclient());
				zpl.setCloseTime(z.getCloseTime());
				zpl.setCountryIdCountry(idCountry);
				zpl.setIdzone(z.getIdzone());
				zpl.setLatitude(z.getLatitude());
				zpl.setLongitude(z.getLongitude());
				zpl.setMinuteValue(z.getMinuteValue().toString());
				zpl.setName(z.getName());
				zpl.setOpenTime(z.getOpenTime());
				zpl.setAddress(z.getAddress());
				zpl.setSchedule(escapeHtml4(z.getSchedule()));
				zpl.setEmail(z.getEmail());
				zns.add(zpl);
			}
		}

		return zns;
	}

	private static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

}
