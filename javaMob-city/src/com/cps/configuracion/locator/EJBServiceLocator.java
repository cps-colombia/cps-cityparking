package com.cps.configuracion.locator;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Provee un mecanismo unico para el acceso a los ejb.
 * 
 * @author Jorge
 * 
 */
public class EJBServiceLocator {

	private static EJBServiceLocator ejbLocator = new EJBServiceLocator();
	
	//private static String filename= "jndi.properties";
	private static String filename= "jndi_wildfly.properties";
	private Properties jndiProperties;
	private static String DEV_JNDI_PROPERTY = "D:\\repositories\\cps\\web\\properties\\"+filename;
	private static String PROD_JNDI_PROPERTY = "/root/properties_city/"+filename;
	
	private HashMap<String, Object> cache;
	
	private Context ctx;
	
	private EJBServiceLocator() {
		cache = new HashMap<String, Object>();
	}

	public static EJBServiceLocator getInstance() {
		return ejbLocator;
	}

	/**
	 * 
	 * @param ejbName
	 * @return Object
	 * @throws Exception
	 */
	public Object getEjbService(String ejbName) throws Exception {
		
		Object obj = cache.get(ejbName);
		
		if( obj == null ){
			
			if( ctx == null ){			
				jndiProperties = new Properties();
				try {
					jndiProperties.load(new FileInputStream(new File(
							DEV_JNDI_PROPERTY)));
				} catch (Exception e) {
					jndiProperties.load(new FileInputStream(new File(
							PROD_JNDI_PROPERTY)));
				}
			}
			
			ctx = new InitialContext(jndiProperties);		
			obj = ctx.lookup(ejbName.concat("/remote"));
			cache.put(ejbName, obj);
		
		}
		
		return obj;
	}

	/**
	 * Retorna un ejb identificado con el mismo nombre de la clase pasado como
	 * argumento.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <X> X getEjbService(Class<X> clas) throws Exception {
		
		//X x = (X)cache.get(clas.getSimpleName());
		
		//if( x == null ){
		
			if( ctx == null ){		
				jndiProperties = new Properties();
				/*try {
					jndiProperties.load(new FileInputStream(new File(
							DEV_JNDI_PROPERTY)));
				} catch (Exception e) {
					jndiProperties.load(new FileInputStream(new File(
							PROD_JNDI_PROPERTY)));
				}*/
				
				   jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
			       // jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
			        jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:18080");
				ctx = new InitialContext(jndiProperties);
			}
			
		//	x = (X) ctx.lookup(clas.getSimpleName().concat("/remote"));
			X x =	(X) ctx.lookup("ejb:/COL-CPS-EJB/".concat(clas.getSimpleName()+"!"+clas.getName()));
			//cache.put(clas.getSimpleName(), x);
		
		//}
		
		return x;
		
	}

}
