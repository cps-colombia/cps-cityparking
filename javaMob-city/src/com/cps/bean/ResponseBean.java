package com.cps.bean;

public class ResponseBean {

	private int code;
	
	private String description;
	
	private Object obj;
	
	public ResponseBean(){
		
	}

	public ResponseBean(int code, String description, Object obj) {
		super();
		this.code = code;
		this.description = description;
		this.obj = obj;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
	
}
