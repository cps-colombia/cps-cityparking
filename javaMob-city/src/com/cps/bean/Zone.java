package com.cps.bean;

import java.sql.Time;

public class Zone {

	private String idzone;

	private String clientIdclient;
	
	private String countryIdCountry;

	private Time closeTime;

	private String minuteValue;

	private String latitude;

	private String longitude;

	private String name;

	private Time openTime;

	private String places;
	
	private String address; 
	
	private String schedule;
	private String email;

    public Zone() {
    }
    
	public Zone(String idzone, String clientIdclient, Time closeTime,
			String minuteValue, String latitude, String longitude, 
			String name, Time openTime,
			String places, String countryIdCountry) {
		super();
		this.idzone = idzone;
		this.clientIdclient = clientIdclient;
		this.closeTime = closeTime;
		this.minuteValue = minuteValue;
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
		this.openTime = openTime;
		this.places = places;
		this.countryIdCountry = countryIdCountry;
	}

	public String getIdzone() {
		return this.idzone;
	}

	public void setIdzone(String idzone) {
		this.idzone = idzone;
	}

	public String getClientIdclient() {
		return this.clientIdclient;
	}

	public void setClientIdclient(String clientIdclient) {
		this.clientIdclient = clientIdclient;
	}

	public Time getCloseTime() {
		return this.closeTime;
	}

	public void setCloseTime(Time closeTime) {
		this.closeTime = closeTime;
	}

	public String getMinuteValue() {
		return this.minuteValue;
	}

	public void setMinuteValue(String minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Time getOpenTime() {
		return this.openTime;
	}

	public void setOpenTime(Time openTime) {
		this.openTime = openTime;
	}

	public String getPlaces() {
		return this.places;
	}

	public void setPlaces(String places) {
		this.places = places;
	}

	public String getCountryIdCountry() {
		return countryIdCountry;
	}

	public void setCountryIdCountry(String countryIdCountry) {
		this.countryIdCountry = countryIdCountry;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
