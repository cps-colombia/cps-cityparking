package com.cps.bean;

public class ParkingResume {

	private int totalMins;

	private int maxMins;

	private String minuteValue;

	private String msisdn;

	private String startTime;
	
	private String endTime;

	private String idPlace;

	private String vehiclePlate;
	
	private String total;
	
	private String userBalance;
	
	private String balance;
	
	private String minValue;
	
	public ParkingResume(){
		
	}

	public ParkingResume(
			int totalMins, int maxMins, String minuteValue,
			String msisdn, String startTime, String endTime, String idPlace,
			String vehiclePlate, String total, String userBalance, String balance,
			String minValue
			) {
		super();
		this.totalMins = totalMins;
		this.maxMins = maxMins;
		this.minuteValue = minuteValue;
		this.msisdn = msisdn;
		this.startTime = startTime;
		this.endTime = endTime;
		this.idPlace = idPlace;
		this.vehiclePlate = vehiclePlate;
		this.total = total;
		this.userBalance = userBalance;
		this.balance = balance;
		this.minValue = minValue;
	}

	public int getTotalMins() {
		return totalMins;
	}

	public void setTotalMins(int totalMins) {
		this.totalMins = totalMins;
	}

	public int getMaxMins() {
		return maxMins;
	}

	public void setMaxMins(int maxMins) {
		this.maxMins = maxMins;
	}

	public String getMinuteValue() {
		return minuteValue;
	}

	public void setMinuteValue(String minuteValue) {
		this.minuteValue = minuteValue;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getIdPlace() {
		return idPlace;
	}

	public void setIdPlace(String idPlace) {
		this.idPlace = idPlace;
	}

	public String getVehiclePlate() {
		return vehiclePlate;
	}

	public void setVehiclePlate(String vehiclePlate) {
		this.vehiclePlate = vehiclePlate;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getUserBalance() {
		return userBalance;
	}

	public void setUserBalance(String userBalance) {
		this.userBalance = userBalance;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}
	
}
