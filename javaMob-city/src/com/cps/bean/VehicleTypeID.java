package com.cps.bean;

import java.io.Serializable;

public class VehicleTypeID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String fraction;
	private String rate;
	private String discount;
	
	
	
	public VehicleTypeID() {
		
	}
	
	public VehicleTypeID(String fraction, String rate, String discount) {
		
		this.fraction = fraction;
		this.rate = rate;
		this.discount = discount;
	}

	public String getFraction() {
		return fraction;
	}
	public void setFraction(String fraction) {
		this.fraction = fraction;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}

	
	
}
