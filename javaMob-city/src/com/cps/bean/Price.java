package com.cps.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class Price implements Serializable {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private  Map<String,VehicleTypeID> vtype_id = new HashMap<String, VehicleTypeID>();

	public Map<String,VehicleTypeID> getVtype_id() {
		return vtype_id;
	}

	public void setVtype_id(Map<String,VehicleTypeID> vtype_id) {
		this.vtype_id = vtype_id;
	}

}
