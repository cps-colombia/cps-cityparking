package com.cps.bean;

import java.io.Serializable;

public class PriceResponse  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idzone;
	private Price price = new Price();
	public String getIdzone() {
		return idzone;
	}
	public void setIdzone(String idzone) {
		this.idzone = idzone;
	}
	public Price getPrice() {
		return price;
	}
	public void setPrice(Price price) {
		this.price = price;
	}
	
 
}
