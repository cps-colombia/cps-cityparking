package com.cps.bean;

import java.io.Serializable;

import com.cps.entity.bean.City;


public class SysUser implements Serializable {
	private static final long serialVersionUID = 1L;

	private String idsysUser;

	private String address;

	private String balance;

	private String birthDate;

	private String countryIdcountry;

	private String cu;

	private String email;

	private int idsysUserType;

	private String lastName;

	private String login;

	private String name;

	private String pass;

	private String favoriteMsisdn;

	private City city_idcity;
	
	private double dBalance;
	
    public SysUser() {
    }

	public SysUser(String address, String balance, String birthDate,
			String countryIdcountry, String cu, String email,
			int idsysUserType, String lastName, String login, String name,
			String pass, String favoriteMsisdn, double dBalance) {
		super();
		this.address = address;
		this.balance = balance;
		this.birthDate = birthDate;
		this.countryIdcountry = countryIdcountry;
		this.cu = cu;
		this.email = email;
		this.idsysUserType = idsysUserType;
		this.lastName = lastName;
		this.login = login;
		this.name = name;
		this.pass = pass;
		this.favoriteMsisdn = favoriteMsisdn;
		this.dBalance = dBalance;
	}

	public SysUser(String idsysUser, String address, String balance,
			String birthDate, String countryIdcountry, String cu, String email,
			int idsysUserType, String lastName, String login, String name,
			String pass, String favoriteMsisdn, City city, double dBalance) {
		super();
		this.idsysUser = idsysUser;
		this.address = address;
		this.balance = balance;
		this.birthDate = birthDate;
		this.countryIdcountry = countryIdcountry;
		this.cu = cu;
		this.email = email;
		this.idsysUserType = idsysUserType;
		this.lastName = lastName;
		this.login = login;
		this.name = name;
		this.pass = pass;
		this.favoriteMsisdn = favoriteMsisdn;
		this.city_idcity = city;
		this.dBalance = dBalance;
	}

	public String getIdsysUser() {
		return this.idsysUser;
	}

	public void setIdsysUser(String idsysUser) {
		this.idsysUser = idsysUser;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBalance() {
		return this.balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getCountryIdcountry() {
		return this.countryIdcountry;
	}

	public void setCountryIdcountry(String countryIdcountry) {
		this.countryIdcountry = countryIdcountry;
	}

	public String getCu() {
		return this.cu;
	}

	public void setCu(String cu) {
		this.cu = cu;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdsysUserType() {
		return this.idsysUserType;
	}

	public void setIdsysUserType(int idsysUserType) {
		this.idsysUserType = idsysUserType;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getFavoriteMsisdn() {
		return favoriteMsisdn;
	}

	public void setFavoriteMsisdn(String favoriteMsisdn) {
		this.favoriteMsisdn = favoriteMsisdn;
	}

	public City getCity() {
		return city_idcity;
	}

	public void setCity(City city) {
		this.city_idcity = city;
	}

	public double getdBalance() {
		return dBalance;
	}

	public void setdBalance(double dBalance) {
		this.dBalance = dBalance;
	}
	
}