package com.cps.util;

import java.security.Security;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {

	private static final String mailhost = "smtp.gmail.com";
	
	public static final synchronized void sendMail(String subject, String body, String sender, String recipients) 
	throws Exception{	
		
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		 
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", mailhost);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
		"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.quitwait", "false");
 
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() 
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{ return new PasswordAuthentication("service@cpsparking.ca","$erviceCP$14*");	}
		});		
 
		MimeMessage message = new MimeMessage(session);
		message.setSender(new InternetAddress(sender));
		message.setSubject(subject);
		message.setContent(body, "text/html;charset=UTF-8");
		
		
		if (recipients.indexOf(',') > 0) 
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
		else
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
 
		
		Transport.send(message);
		
	}
	
	private static final String getStackTrace(Exception e){

		if(e!=null){

			String bug = e.toString()+"<br>";
	        StackTraceElement[] trace = e.getStackTrace();

	        for (int i=0; i < trace.length; i++){
	            bug += "at " + trace[i]+"<br>";
	        }

	        return bug;

		}else{
			return "";
		}
	}
	
	public static final void sendException(final Exception ex, final String note){
		new Thread(
				new Runnable(){
					public void run(){
						try{							
							String HTML = "<br><br><strong>Excepcion - StackTrace</strong><br>"+getStackTrace(ex)+"<br>" +
									"<strong>Nota</strong><br>"+note;								
							sendMail("Exception", HTML, "Bot_CPS_Bug", "robot.cps@gmail.com");							
						}catch (Exception e) {}
					}
				}
		).start();
	}
	
}
