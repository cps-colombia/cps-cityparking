package com.cps.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class PropertiesUtil {

	private static PropertiesUtil instance;
	
	private Properties props;
	
	private static String DEV_SECURITY_PROPERTY = "D:/properties/cps.security.properties";
	
	private static String PROD_SECURITY_PROPERTY = "/root/properties/cps.security.properties";
	
	private PropertiesUtil(){
		try{
			props = new Properties();
			props.load(new FileInputStream(new File(DEV_SECURITY_PROPERTY)));
		}catch (Exception e) {
			try{
				props.load(new FileInputStream(new File(PROD_SECURITY_PROPERTY)));
			}catch (Exception ex) {}
		}		
	}
	
	public static final PropertiesUtil getInstance(){
		if( instance == null ){
			instance = new PropertiesUtil();
		}
		return instance;
	}
	
	public String getProperty(String name){
		return props.getProperty("cps.security."+name);
	}
		
}
