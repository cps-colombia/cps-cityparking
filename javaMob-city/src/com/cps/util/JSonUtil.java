package com.cps.util;

import com.google.gson.Gson;

public class JSonUtil {

	private Gson gson;
	
	private static JSonUtil jutil;
	
	private JSonUtil(){
		gson = new Gson();
	}
	
	public static JSonUtil getInstance(){
		if( jutil == null ){
			jutil = new JSonUtil();
		}
		return jutil;
	}
	
	public String toJson(Object obj){
		return gson.toJson(obj);
	}
	
}
