package com.cps.util;

public interface CPSMobileConstants {

	public static final int LOGIN = 0;
	
	public static final int REGISTER = 1;
	
	public static final int GET_USER_INFO = 2;
	
	public static final int GET_COUNTRY_LIST = 3;
	
	public static final int GET_ACTIVE_PARKING = 4;
	
	public static final int END_PARKING = 5;
	
	public static final int START_PARKING = 6;
	
	public static final int GET_ZONES = 7;
	
	public static final int GET_CITIES_LIST = 8;
	
	public static final int GET_ZONES_TIMETABLE = 9;
	
	public static final int UPDATE_USER = 10;
	
	public static final int UPDATE_PASSWORD = 11;
	
	public static final int ADD_VEHICLE = 12;
	
	public static final int PIN_RELOAD = 13;
	
	public static final int GIVE_BALANCE = 14;
	
	public static final int GET_VEHICLES = 15;
	
	public static final int GET_GPS_ZONES = 16;
	
	public static final int DELETE_VEHICLE = 17;
	
	public static final int SOCIAL_NETWORK_URL = 18;
	
	public static final int ADD_SUGGESTION = 19;
	
	public static final int GET_NOTIFICATIONS = 20;
	
	public static final int SET_READ = 21;
	
    public static final int GET_ZONE_INFO_BY_ACT_PARKING = 22;
    
    public static final int RESET_PASSWORD = 23;
    
    public static final int VALIDATE_PAY_CITY_PARKING = 24;

	public static final int CONFIRM_PAY_CITY_PARKING = 25;
	public static final int INIT_PAYMENT_CITY_PARKING_ZP = 26;

	public static final int CHECK_PENDINGS_PAYMENT_CITY_PARKING_ZP = 27;
	
	public static final int CHECKVERSION = 28;
	
	public static final int REGISTERDRIVER = 29;
	
	public static final int GET_ZONE_PRICE = 30;

	
}
