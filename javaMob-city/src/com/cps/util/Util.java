package com.cps.util;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;


public class Util {

	 public static String generatePassword()
	    {
	        int k = 0;
	        String password = "";
	        for(; k < 6; k++)
	            password = (new StringBuilder(String.valueOf(password))).append(String.valueOf(chars[(int)(Math.random() * (double)chars.length)])).toString();

	        return password;
	    }

	    public static final String chars[] = {
	        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", 
	        "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", 
	        "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", 
	        "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", 
	        "o", "p", "q", "t", "s", "t", "u", "v", "w", "x", 
	        "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", 
	        "8", "9"
	    };

		public static void traceError(Logger log, Exception e, HttpServletRequest request, String text){
			StringBuffer trace = new StringBuffer();
			try{				
				Map<String, String[]> params = request.getParameterMap();		
				Iterator<String> i = params.keySet().iterator();
				trace.append("-----------------------------------------\n");
				trace.append("ERROR INFO: "+ text != null ? text : "NA\n");
				trace.append("-----------------------------------------\n");
				while ( i.hasNext() )		
				{		
					String key = (String) i.next();		
					String value = ((String[]) params.get( key ))[ 0 ];			
					trace.append(key).append("=").append(value).append("\n");
				}
				trace.append("-----------------------------------------\n");
				log.error(trace.toString());
			}catch (Exception ex) {}
			log.error(e);		
		}
		
		public static void traceError(Logger log, Exception e, HttpServletRequest request){
			traceError(log, e, request, null);
		}
		
		public static String getTemplateString(String template, String locale) throws IOException, URISyntaxException {
			String jarLocation = Util.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			
			String[] absolutePath = jarLocation.split("WEB-INF");
			//File getFile=new File(absolutePath[0]);
			String getTemplate = absolutePath[0]+template;
		   if(getTemplate.startsWith("/")&& System.getProperty( "os.name" ).contains( "indow" ))
		   {
			   getTemplate=  getTemplate.replaceFirst("/", "");
		   }
			byte[] encoded = Files.readAllBytes(Paths.get(getTemplate));
			String stringTemplate = new String(encoded, StandardCharsets.UTF_8);
			
			Pattern pattern = Pattern.compile("\\{msgs\\['(.*?)'\\]\\}");
			Matcher matcher = pattern.matcher(stringTemplate);

	        while (matcher.find()) {
			    stringTemplate = stringTemplate.replaceAll("(\\#\\{msgs\\['"+ matcher.group(1) +"'\\]\\})", getMessage(matcher.group(1), locale));
			}
			return stringTemplate;
		}
		
		public static String getMessage(String propertyName, String locale) {
		    ResourceBundle bundle;
			if("es".equals(locale)){
				Locale esLocale = new Locale("es");
				bundle = ResourceBundle.getBundle("messages", esLocale);
			} else {
				Locale enLocale = new Locale("en");
				bundle = ResourceBundle.getBundle("messages", enLocale);
			}
			String message = bundle.getString(propertyName);
			return message;
		}
		
	
	    
}
