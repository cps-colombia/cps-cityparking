package com.cps.security;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.DESEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.modes.PaddedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Base64;

import bouncycastle.HexDecoder;

@SuppressWarnings("deprecation")
public class DESBBEngine {
	
	private static final String KEY = "ueZ82h9h7N8=";
	
	private static String KEY_DECODED;
	
	private BlockCipher engine;
	
	private BufferedBlockCipher cipher;
	
	private static DESBBEngine instance;
	
	private DESBBEngine(){
		try{
			KEY_DECODED = new String(Base64.decode(KEY));		
			engine = new DESEngine();
			cipher = new PaddedBlockCipher(new CBCBlockCipher(engine));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static DESBBEngine getInstance(){
		if( instance == null ){
			instance = new DESBBEngine();
		}
		return instance;
	}
	
	public String encryptEncodeHex(String text)throws Exception{
		byte[] key = KEY_DECODED.getBytes();
		byte[] input = text.getBytes();
		cipher.init(true, new KeyParameter(key));
		byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
		int outputLen = cipher.processBytes(input, 0, input.length, cipherText, 0);
		cipher.doFinal(cipherText, outputLen);
		return HexDecoder.encode(cipherText).toLowerCase();
	}
	
	public String decryptEncodedHex(String text)throws Exception{
		byte[] key = KEY_DECODED.getBytes();
		byte[] input = HexDecoder.decode(text);
		cipher.init(false, new KeyParameter(key));
		byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
		int outputLen = cipher.processBytes(input, 0, input.length, cipherText, 0);
		cipher.doFinal(cipherText, outputLen);
		return new String(cipherText).trim();
	}

}
