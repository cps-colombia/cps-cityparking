package com.cps.security;
import java.util.HashMap;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import bouncycastle.HexDecoder;

import com.cps.util.PropertiesUtil;


public class CryptoHelper {

	private static CryptoHelper crypto;
	
	private DESEngine des;
	
	private static final String KEY = PropertiesUtil.getInstance().getProperty("key");
	
	private String paramName;
	
	private DESBBEngine desEncDec;
	
	private Logger LOGGER = Logger.getLogger(CryptoHelper.class);
	
	private CryptoHelper(){
		try{			
			paramName = PropertiesUtil.getInstance().getProperty("param");
			byte[] keyB = Base64.decodeBase64(KEY);
			SecretKey key = new SecretKeySpec(keyB, "DES");			
			des = new DESEngine(key);
			desEncDec = DESBBEngine.getInstance();
		}catch (Exception e) {
			LOGGER.error("Error en constructor CryptoHelper", e);
		}
	}
	
	public static CryptoHelper getInstance(){
		if( crypto == null ){
			crypto = new CryptoHelper();
		}
		return crypto;
	}
	
	public HashMap<String, String> decryptHttpRequest(HttpServletRequest request)throws Exception{
		String enc = request.getParameter(paramName);
		String device = request.getParameter("device");
		String hexDec = new String(Hex.decodeHex(enc.toCharArray()));
		String prms[] = null;
		String dc = null;
		if( "Blackberry".equals(device) || "J2ME".equals(device) ){
			dc = desEncDec.decryptEncodedHex(enc);
		}
		else{
			String base64 = Base64.encodeBase64String(hexDec.getBytes());
			LOGGER.info("B64: "+base64);
			dc = decryptBase64(base64);
			LOGGER.info("DECODED: "+dc);
		}
		prms = dc.split("&");
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("device", device);
		for( String item : prms ){
			String p[] = item.split("=");
			params.put(p[0], p[1]);
		}
		return params;

	}
	
	public String encryptEncodeHex(String text, String device)throws Exception{		
		if( "Blackberry".equals(device) || "J2ME".equals(device) ){
			return desEncDec.encryptEncodeHex(text).trim();
		}
		return encryptHex(text).trim();
	}
	
	public String encryptBase64(String text){
		return des.encrypt(text);
	}
	
	public String decryptBase64(String text){
		return des.decrypt(text);
	}
	
	public String encryptHex(String text){
		String base64 = encryptBase64(text);
		System.out.println("BS: "+base64);
		byte[] decode = Base64.decodeBase64(base64);
		return new String(Hex.encodeHex(decode));
	}

	public static void main(String[] args) {
		try{
			//&username=ramp&pass=12345678
			String hexa = getInstance().encryptHex("op=0");
			System.out.println(hexa);
			
			String hexDec = new String(HexDecoder.decode("34098a64f48ebfc12e427c268864f1dcbaee76cc2c9913a5b7f8e93c22a78e0839871f73810bf5f9"));			
			String base64 = Base64.encodeBase64String(HexDecoder.decode("34098a64f48ebfc12e427c268864f1dcbaee76cc2c9913a5b7f8e93c22a78e0839871f73810bf5f9"));
			String base642 = new String(bouncycastle.Base64.encode(HexDecoder.decode("34098a64f48ebfc12e427c268864f1dcbaee76cc2c9913a5b7f8e93c22a78e0839871f73810bf5f9")));

			System.out.println(base64);
			System.out.println(base642);
			System.out.println("NAmKZPSOv8EuQnwmiGTx3LrudswsmROlt/jpPCKnjgg5hx9zgQv1+Q==");

			String dc = getInstance().decryptBase64("NAmKZPSOv8EuQnwmiGTx3LrudswsmROlt/jpPCKnjgg5hx9zgQv1+Q==");			
			System.out.println(dc);
			
			System.out.println( getInstance().desEncDec.decryptEncodedHex("34098a64f48ebfc12e427c268864f1dcbaee76cc2c9913a5b7f8e93c22a78e08") );
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
