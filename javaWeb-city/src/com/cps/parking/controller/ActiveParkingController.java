/**
 * 
 */
package com.cps.parking.controller;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.ActiveParking;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.Notification;
import com.cps.entity.bean.Platform;
import com.cps.entity.bean.Zone;
import com.cps.remote.ClientBeanRemote;
import com.cps.remote.NotificationBeanRemote;
import com.cps.remote.ParkingBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 * 
 */
@ManagedBean
@ViewScoped
public class ActiveParkingController implements Serializable {

	private static final Logger LOGGER = Logger.getLogger(ActiveParkingController.class);
	private static final long serialVersionUID = 1L;

	private ZoneBeanRemote zoneBeanRemote;
	private ParkingBeanRemote parkingBeanRemote;
	private NotificationBeanRemote notificationBeanRemote;
	private ClientBeanRemote clientBeanRemote;

	private List<ActiveParking> listActiveParking;
	private List<ActiveParking> listAllActiveParking;
	private List<Zone> listZones;
	private List<Client> listClients;

	private ActiveParking activeParking;
	private String idZone;
	private String textSms;
	private String idClient;
	private boolean admin = false;

	public ActiveParkingController() {
		try {
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);

			ResourceBundle bundle = ResourceBundle.getBundle("cps");

			if (bundle.getString("cps.user.admin").equals(
					"" + JsfUtil.getUserSession().getIdsysUserType())) {
				clientBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(ClientBeanRemote.class);
				setListClients(clientBeanRemote.getListClients());
				setAdmin(true);
			} else {
				setListZones(zoneBeanRemote.getZonesClient(JsfUtil
						.getUserSession()));
			}
			this.setIdZone("-1");
		} catch (Exception e) {
			LOGGER.error("Error Active Parking", e);
		}

	}
	
	
	
	/**
	 * Muestra el parqueo activo para una zona.
	 * 
	 * @param event
	 */
	public void changeClient(ValueChangeEvent event) {
		try {
			String clientSelect = String.valueOf(event.getNewValue());
           
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			
			setListZones(zoneBeanRemote.getZonesByIdClient(clientSelect));
		

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	/**
	 * Muestra el parqueo activo para una zona.
	 * 
	 * @param event
	 */
	public void changeZone(ValueChangeEvent event) {
		try {
			String zoneSelect = String.valueOf(event.getNewValue());
			idZone = zoneSelect;
			parkingBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ParkingBeanRemote.class);
			listActiveParking = parkingBeanRemote
					.getActiveParkingByZone(zoneSelect);
			setListAllActiveParking(parkingBeanRemote.getAllActiveParkingByZone(zoneSelect));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Actualiza periodicamente la tabla de parqueo activo.
	 */
	public void updateActiveParking() {
		try {
			parkingBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ParkingBeanRemote.class);
			listActiveParking = parkingBeanRemote
					.getActiveParkingByZone(getIdZone());
			setListAllActiveParking(parkingBeanRemote.getAllActiveParkingByZone(getIdZone()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Envia un sms a un usuario parqueado.
	 * 
	 * @param event
	 */
	public void sendSms(ActionEvent event) {
		try {
			notificationBeanRemote = EJBServiceLocator.getInstace()
					.getEjbService(NotificationBeanRemote.class);
			Notification notificacion = new Notification();
			notificacion.setIdSysUser(activeParking.getIdSysUser().toString());
			notificacion.setIsread(0);
			notificacion.setMessage(getTextSms());
			notificacion.setMsisdn(activeParking.getMsisdn());
			notificacion.setPlatformIdplatform(Platform.SMS);
			notificacion.setVehiclePlate(activeParking.getVehiclePlate());
			notificationBeanRemote.addNotification(notificacion);

			setTextSms("");
			JsfUtil.addMessage("Mensaje Enviado", "",
					FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			JsfUtil.addMessage("No se pudo enviar el SMS", "",
					FacesMessage.SEVERITY_ERROR);
		}
	}

	public void setListZones(List<Zone> listZones) {
		this.listZones = listZones;
	}

	public List<Zone> getListZones() {
		return listZones;
	}

	public void setListActiveParking(List<ActiveParking> listActiveParking) {
		this.listActiveParking = listActiveParking;
	}

	public List<ActiveParking> getListActiveParking() {
		return listActiveParking;
	}

	public void setIdZone(String idZone) {
		this.idZone = idZone;
	}

	public String getIdZone() {
		return idZone;
	}

	public void setActiveParking(ActiveParking activeParking) {
		this.activeParking = activeParking;
	}

	public ActiveParking getActiveParking() {
		return activeParking;
	}

	public void setTextSms(String textSms) {
		this.textSms = textSms;
	}

	public String getTextSms() {
		return textSms;
	}

	public List<Client> getListClients() {
		return listClients;
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isAdmin() {
		return admin;
	}



	public void setListAllActiveParking(List<ActiveParking> listAllActiveParking) {
		this.listAllActiveParking = listAllActiveParking;
	}



	public List<ActiveParking> getListAllActiveParking() {
		return listAllActiveParking;
	}

}
