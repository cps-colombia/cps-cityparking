/**
 * 
 */
package com.cps.parking.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.ParkingHistoryC;
import com.cps.remote.ParkingBeanRemote;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 * 
 */
@ManagedBean
public class HistoryParkingController {

	private ParkingBeanRemote parkingBeanRemote;
	private List<ParkingHistoryC> lisHistory;

	private boolean history = false;

	public HistoryParkingController() {
		try {
			parkingBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ParkingBeanRemote.class);
			lisHistory = parkingBeanRemote.getHistoryParking(JsfUtil
					.getUserSession().getIdsysUser().toString());
			history = lisHistory != null && !lisHistory.isEmpty() ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<ParkingHistoryC> getLisHistory() {
		return lisHistory;
	}

	public void setLisHistory(List<ParkingHistoryC> lisHistory) {
		this.lisHistory = lisHistory;
	}

	public void generarInforme() throws JRException, IOException {
        try{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		String idHistory = (String) externalContext.getRequestParameterMap().get("idHistory");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "reportHistory_"+formatter.format(new java.util.Date());
		
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("idUsuario", JsfUtil.getUserSession().getIdsysUser());
		parameter.put("idHistory", idHistory);
		
		String reportFile = idHistory != null ? "reportBillingParking" : "reportParkingHistoryUser";		
		JasperReportHelper jrh = new JasperReportHelper(reportFile, parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  

		
		byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
        	JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	public boolean isHistory() {
		return history;
	}

	public void setHistory(boolean history) {
		this.history = history;
	}

}
