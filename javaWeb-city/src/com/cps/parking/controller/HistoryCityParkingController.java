/**
 * 
 */
package com.cps.parking.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.ParkingHistoryC;
import com.cps.entity.bean.ParkingHistoryCity;
import com.cps.remote.ParkingBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 * 
 */
@ManagedBean
@ViewScoped
public class HistoryCityParkingController {

	private ParkingBeanRemote parkingBeanRemote;
	private List<ParkingHistoryCity> lisHistory;
	private Date fechaIni;
	private Date fechaFin;
	private boolean history = false;

	public HistoryCityParkingController() {
		

	}
	
	public void reportParking(ActionEvent event) {
try {
			
			
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			parkingBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ParkingBeanRemote.class);
			lisHistory = parkingBeanRemote.getHistoryCityParking(JsfUtil
					.getUserSession().getIdsysUser().toString(),fechaI + " 00:01:01", fechaF + " 23:59:59");
			history = lisHistory != null && !lisHistory.isEmpty() ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<ParkingHistoryCity> getLisHistory() {
		return lisHistory;
	}

	public void setLisHistory(List<ParkingHistoryCity> lisHistory) {
		this.lisHistory = lisHistory;
	}

	public void generarInforme() throws JRException, IOException {
        try{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		String idHistory = (String) externalContext.getRequestParameterMap().get("idHistory");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "reportHistory_"+formatter.format(new java.util.Date());
		
		String fechaI = DateHelper.format(getFechaIni(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getFechaFin(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("idUsuario", JsfUtil.getUserSession().getIdsysUser());
		parameter.put("idHistory", idHistory);
		parameter.put("startDate", fechaI + " 00:01:01");
		parameter.put("endDate", fechaF + " 23:59:59");
		
		String reportFile = idHistory != null ? "reportBillingCityParking" : "reportCityParkingHistoryUser";		
		JasperReportHelper jrh = new JasperReportHelper(reportFile, parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  

		
		byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
        	JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	public boolean isHistory() {
		return history;
	}

	public void setHistory(boolean history) {
		this.history = history;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

}
