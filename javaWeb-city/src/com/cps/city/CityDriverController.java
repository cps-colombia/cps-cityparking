package com.cps.city;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.City;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.CityDriver;
import com.cps.entity.bean.CityDriverStatus;
import com.cps.entity.bean.CityDriverOperator;
import com.cps.entity.bean.Status;
import com.cps.entity.bean.SysUserVehicle;
import com.cps.entity.bean.Vehicle;
import com.cps.entity.bean.VehicleType;
import com.cps.entity.bean.VehicleBrand;
import com.cps.remote.CountryBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.remote.StatusBeanRemote;
import com.cps.remote.CityDriverBeanRemote;
import com.cps.remote.VehicleBeanRemote;
import com.cps.util.JsfUtil;
import com.cps.util.MailSender;
import com.cps.util.Util;

@ManagedBean
@ViewScoped
public class CityDriverController implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(CityDriverController.class);

	private SysUserBeanRemote beanRemote;
	private CityDriverBeanRemote cityDriverBeanRemote;
	private CountryBeanRemote countryBeanRemote;
	private VehicleBeanRemote vehicleBeanRemote;
	private StatusBeanRemote statusBeanRemote;
	private List<Status> listStatus;
	private List<SysUserVehicle> listVehicle;
	private List<VehicleType> listVehicleType;
	private List<VehicleBrand> listVehicleBrand;
	private List<CityDriver> listDriver;
	private List<CityDriver> listDriverById;
	private List<City> cities = new ArrayList<City>();
	private CityDriver cityDriver = new CityDriver();
	private CityDriverStatus cityDriverStatus = new CityDriverStatus();
	private CityDriverOperator cityDriverOperator = new CityDriverOperator();
	private SysUser sysUser = new SysUser();

	private int soat;
	private int rtm;
	private int userType;
	private String fullName;
	private String userPhone;
	private String email;
	private String CU;
	private String mobile;
	private String city;
	private String plate;
	private String comment;
	private String commentStatus;
	private String numberStops;
	private String stopsPrice;
	private String vehicleDescription;
	private BigInteger vehicleType;
	private BigInteger vehicleBrand;
	private BigInteger toStatus;

	private BigInteger assignTo;
	private List<SysUser> allOperators;
	private List<SysUser> listOperators;
	private List<SysUser> listOperatorsAdd;
	private List<SysUser> listOperatorsRemove;

	private Integer SysUserTypeOperator = 10;
	private boolean formNoEditable = true;
	private boolean formUserExits = false;
	private boolean userHasVehicle = false;
	private boolean emailNotification = false;

	/**
	 * @constructor
	 */
	public CityDriverController(){
		try {
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService(CityDriverBeanRemote.class);
			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );

			listVehicleType = vehicleBeanRemote.getVehicleType();
			listVehicleBrand = vehicleBeanRemote.getVehicleBrand();
			listDriver = cityDriverBeanRemote.getCityDrivers();
			allOperators = cityDriverBeanRemote.getCityDriversOperators(SysUserTypeOperator);

		} catch (Exception e) {
			LOGGER.error("Error listar driver", e);
			e.printStackTrace();
		}
	}
	/**
	 * Register new driver, if user no exist create and register
	 * @return
	 */
	public String registerDriver(){
		try{
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService(CityDriverBeanRemote.class);
			beanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );
			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService( CountryBeanRemote.class );
			SysUser SysUserSession = JsfUtil.getUserSession();

			switch (userType) {			
			// this userType is if new user or exist user
			case 1:
				if(soat == 1 && rtm == 1){
					String[] login = email.split("@");
					sysUser.setLogin(login[0]);
					sysUser.setEmail(email);
					plate = plate.toUpperCase();

					SysUser sysUserExist = beanRemote.getUser( sysUser.getLogin() );
					if(sysUserExist != null){
						sysUser.setLogin(login[0] + "2");
						sysUserExist = beanRemote.getUser( sysUser.getLogin() );
					}
					if ( sysUserExist == null )	{
						if ( beanRemote.getUserByEmail( sysUser.getEmail() ) == null ){
							if ( beanRemote.getUserByMobile( sysUser.getFavoriteMsisdn() ) == null ) {

									sysUser.setBalance( new BigDecimal( 0 ) );
									sysUser.setIdsysUserType( 2 );
									sysUser.setCity( countryBeanRemote.getCity( city ) );
									sysUser.setEmailNotification((emailNotification)?1:0);
									sysUser.setPass(com.cps.util.Util.generatePassword());

									beanRemote.registerUser( sysUser );

									SysUser sysUserNew = beanRemote.getUser( sysUser.getLogin() );
									try{
										vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
										vehicleBeanRemote.registerVehicleFull(plate, vehicleType, vehicleBrand, vehicleDescription, sysUserNew );
									} catch (Exception e) {
										LOGGER.error( "Error Registrando Vehiculo:", e );
									    e.printStackTrace();
									}

									cityDriver.setSysUser(sysUserNew);
									cityDriver.setPlate(plate);
									cityDriver.setDocuments(new BigInteger("1"));
									cityDriver.setSysUserCreator(new BigInteger(SysUserSession.getIdsysUser()));
									cityDriver.setState(new BigInteger("1"));

									cityDriverBeanRemote.registerCityDriver(cityDriver);

									JsfUtil.addMessage("registerDriverSuccess", FacesMessage.SEVERITY_INFO);
									listDriver = cityDriverBeanRemote.getCityDrivers();
									/*String subject = JsfUtil.getMessage( "registerMailSubject", FacesMessage.SEVERITY_INFO )
											.getSummary();
									String body = Util.getTemplateString("newsletter/emailWelcome.xhtml")
		                                .replace( "{cps_login}", sysUser.getLogin())
		                                .replace( "{cps_pass}", sysUser.getPass())
		                                .replace( "{cps_cu}", sysUserNew.getCu());
									String sender = JsfUtil.getMessage( "registerMailFrom", FacesMessage.SEVERITY_INFO )
											.getSummary();
									MailSender.sendMail( subject, body, sender, sysUser.getEmail() );*/

							} else{
								JsfUtil.addMessage( "registerMobileExist", FacesMessage.SEVERITY_ERROR );
							}

						} else {
							JsfUtil.addMessage( "registerEmailExist", FacesMessage.SEVERITY_ERROR );
						}

					} else {
						JsfUtil.addMessage( "registerLoginExist", FacesMessage.SEVERITY_ERROR );
					}
				} else {
					JsfUtil.addMessage("driverDocumentsError", FacesMessage.SEVERITY_ERROR);
				}
				break;

			case 2:
				if(soat == 1 && rtm == 1){
					if(sysUser != null){					
						SysUser sysUserDriver = beanRemote.getUserByCU(sysUser.getCu());
						cityDriver.setSysUser(sysUserDriver);
						cityDriver.setPlate(plate);
						cityDriver.setDocuments(new BigInteger("1"));
						cityDriver.setSysUserCreator(new BigInteger(SysUserSession.getIdsysUser()));
						cityDriver.setState(new BigInteger("1"));
	
						try{
							vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
							Vehicle dataVehicle = vehicleBeanRemote.getVehicle(plate, sysUserDriver);
							if(dataVehicle != null){
								Vehicle vehicle = new Vehicle();
								vehicle.setPlate(plate);
								vehicle.setType(vehicleType);
								if(vehicleBrand != null){
									vehicle.setBrand(vehicleBrand);	
								}
								vehicle.setDescription(vehicleDescription);
								vehicle.setState(1);
								vehicle.setDateReg(dataVehicle.getDateReg());
								vehicleBeanRemote.updateVehicle(vehicle);
							}
						} catch (Exception e) {
							LOGGER.error( "Error get vehicule:", e );
						    e.printStackTrace();
						}
	
						cityDriverBeanRemote.registerCityDriver(cityDriver);
	
						listDriver = cityDriverBeanRemote.getCityDrivers();
						JsfUtil.addMessage("registerDriverSuccess", FacesMessage.SEVERITY_INFO);
					} else {
						LOGGER.error( "Empty sysUser add exist user in registerDriver");
						JsfUtil.addMessage("registerDriverError", FacesMessage.SEVERITY_ERROR);
					}
				} else {
					JsfUtil.addMessage("driverDocumentsError", FacesMessage.SEVERITY_ERROR);
				}
				break;
			}

		} catch (Exception e) {
			LOGGER.error( "Error Registrando Usuario Driver:", e );
			e.printStackTrace();
			JsfUtil.addMessage("registerDriverError", FacesMessage.SEVERITY_ERROR);
		}
		return null;
	}
	
	/**
	 * Set id driver for sysUser or create new sysUser and set id
	 * @return
	 */
	public String addDriverOperator(){
		try {
			beanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService(CityDriverBeanRemote.class);
			
			switch (userType) {
			// this userType is if new user or exist user
			case 1:
				String[] login = email.split("@");
				sysUser.setLogin(login[0]);
				sysUser.setEmail(email);
				SysUser sysUserExist = beanRemote.getUser( sysUser.getLogin() );
				if ( sysUserExist == null )	{
					if ( beanRemote.getUserByEmail( sysUser.getEmail() ) == null ){
						if ( beanRemote.getUserByMobile( sysUser.getFavoriteMsisdn() ) == null ) {

								sysUser.setBalance( new BigDecimal( 0 ) );
								sysUser.setIdsysUserType(	SysUserTypeOperator);
								sysUser.setCity( countryBeanRemote.getCity( city ) );
								sysUser.setEmailNotification((emailNotification) ? 1 : 0);
								sysUser.setPass(com.cps.util.Util.generatePassword());

								beanRemote.registerUser( sysUser );
								allOperators = cityDriverBeanRemote.getCityDriversOperators(SysUserTypeOperator);
								
								JsfUtil.addMessage("addOperatorDriverSuccess", FacesMessage.SEVERITY_INFO);
						} else{
							JsfUtil.addMessage( "registerMobileExist", FacesMessage.SEVERITY_ERROR );
						}

					} else {
						JsfUtil.addMessage( "registerEmailExist", FacesMessage.SEVERITY_ERROR );
					}

				} else {
					JsfUtil.addMessage( "registerLoginExist", FacesMessage.SEVERITY_ERROR );
				}
				break;
			case 2:
				SysUser sysUserDriver = beanRemote.getUserByCU(sysUser.getCu());
				sysUserDriver.setIdsysUserType(SysUserTypeOperator);

				beanRemote.updateUser(sysUserDriver);
				allOperators = cityDriverBeanRemote.getCityDriversOperators(SysUserTypeOperator);

				JsfUtil.addMessage("addOperatorDriverSuccess", FacesMessage.SEVERITY_INFO);
				break;
			}
		} catch (Exception e) {
			LOGGER.error( "Error Registrando Operador Driver:", e );
			e.printStackTrace();
			JsfUtil.addMessage("addOperatorDriverError", FacesMessage.SEVERITY_ERROR);
		}
		return null;
	}

	/**
	 * Remove privileges but keep user
	 * @return
	 */
	public String removeOperator(){
		try {
			beanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );
			
			SysUser sysUserDriver = beanRemote.getUserByCU(sysUser.getCu());
			sysUserDriver.setIdsysUserType(2);
			
			beanRemote.updateUser(sysUserDriver);
			allOperators = cityDriverBeanRemote.getCityDriversOperators(SysUserTypeOperator);
			
			JsfUtil.addMessage("rmOperatorDriverSuccess", FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			LOGGER.error( "Error Remover operador Driver:", e );
			e.printStackTrace();
			JsfUtil.addMessage("rmOperatorDriverError", FacesMessage.SEVERITY_ERROR);
		}		
		return null;
	}
	
	/**
	 * update data cityDriver
	 * @return
	 */
	public String updateCityDriver(){
		try {
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService( CityDriverBeanRemote.class );	
			if(plate != null){
				cityDriver.setPlate(plate);
				
				try{
					vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class);
					Vehicle dataVehicle = vehicleBeanRemote.getVehicle(plate, cityDriver.getSysUser());
					if(dataVehicle != null){
						Vehicle vehicle = new Vehicle();
						vehicle.setPlate(plate);
						vehicle.setType(vehicleType);
						if(vehicleBrand != null){
							vehicle.setBrand(vehicleBrand);	
						}
						if(vehicleDescription != null){							
							vehicle.setDescription(vehicleDescription);
						}
						vehicle.setState(1);
						vehicle.setDateReg(dataVehicle.getDateReg());
						vehicleBeanRemote.updateVehicle(vehicle);	
					} else {
						JsfUtil.addMessage( "updateVehicleError", FacesMessage.SEVERITY_INFO );
					}
				} catch (Exception e) {
					LOGGER.error( "Error get vehicule:", e );
				    e.printStackTrace();
				}
				
				cityDriverBeanRemote.updateCityDriver(cityDriver);
				JsfUtil.addMessage( "updateDataSuccesDriver", FacesMessage.SEVERITY_INFO );				
			} else {
				JsfUtil.addMessage( "updateErrorDriverPlate", FacesMessage.SEVERITY_ERROR );
			}	
			// Clean and update list
			listDriver = cityDriverBeanRemote.getCityDrivers();
			formNoEditable = true;			
		} catch ( Exception e ){
			LOGGER.error( "No fue posible actualizar informacion de usuario", e );
			JsfUtil.addMessage( "updateErrorDriver", FacesMessage.SEVERITY_ERROR );
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * update status cityDriver
	 * @return
	 */
	public String updateStatusCityDriver(){
		try {
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService( CityDriverBeanRemote.class );
			SysUser SysUserSession = JsfUtil.getUserSession();

			if(cityDriver.getIdCityStatus() != null){
				cityDriverStatus.setIdCityStatus(cityDriver.getIdCityStatus().getIdCityStatus());
			}

			if(cityDriver.getState().intValue() == 5){
				cityDriverStatus.setComments(commentStatus);
			}
			if(cityDriver.getState().intValue() == 4){
				if(numberStops != null && stopsPrice != null){
					cityDriverStatus.setStops(new BigInteger(numberStops));
					cityDriverStatus.setStopsPrice(stopsPrice);
				}
			}
			
			if((cityDriver.getState().intValue() == 2 || cityDriver.getState().intValue() == 1) 
					&& listOperators != null  && !listOperators.isEmpty()){
				// Change status if have operator
				cityDriver.setState(new BigInteger("3"));
				cityDriverStatus.setStatusId(new BigInteger("3"));
			} else  {
				cityDriverStatus.setStatusId(cityDriver.getState());
			}
				
			// Update
			cityDriverStatus.setIdCityDriver(cityDriver);
			cityDriverStatus.setSysUserCreator(new BigInteger(SysUserSession.getIdsysUser()));
			cityDriverBeanRemote.updateCityDriverStatus(cityDriverStatus);
			
			CityDriverStatus oldStatus = cityDriverBeanRemote.getCityDriverStatus(cityDriver);

			if(oldStatus != null){			
				cityDriver.setIdCityStatus(oldStatus);
			} 
			
			cityDriverBeanRemote.updateCityDriver(cityDriver);
			listDriver = cityDriverBeanRemote.getCityDrivers();
			formNoEditable = true;
			JsfUtil.addMessage( "updateDataSuccesDriver", FacesMessage.SEVERITY_INFO );
			
			try {
				// Save oprators
				if(listOperators != null && !listOperators.isEmpty()){
					cityDriverOperator = new CityDriverOperator();
						if(listOperatorsRemove.size()>=1){
							for (int i = 0; i < listOperatorsRemove.size(); i++) {
								cityDriverBeanRemote.deleteCityDriverOperator(
										listOperatorsRemove.get(i),
										cityDriverBeanRemote.getCityDriverStatus(cityDriver)
										);
							}
						}
	
						if(listOperatorsAdd.size()>=1){
							for (int i = 0; i < listOperatorsAdd.size(); i++) {
								cityDriverOperator.setIdCityStatus(cityDriverBeanRemote.getCityDriverStatus(cityDriver));
								cityDriverOperator.setSysUser(listOperatorsAdd.get(i));
								cityDriverOperator.setSysUserCreator(new BigInteger(SysUserSession.getIdsysUser()));
								cityDriverBeanRemote.updateCityDriverOperator(cityDriverOperator);
							}
						}
				}
			} catch ( Exception e ){
				LOGGER.error( "No fue posible guardar operador Driver", e );
				JsfUtil.addMessage( "updateErrorDriverOperator", FacesMessage.SEVERITY_ERROR );
				e.printStackTrace();
			}	
		} catch ( Exception e ){
			LOGGER.error( "No fue posible actualizar informacion de usuario Driver", e );
			JsfUtil.addMessage( "updateErrorDriver", FacesMessage.SEVERITY_ERROR );
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Operator auto-complete
	 * @return
	 */
    public List<SysUser> completeOperator(String query) throws Exception {
        allOperators = cityDriverBeanRemote.getCityDriversOperators(SysUserTypeOperator);
        List<SysUser> filteredOperators = new ArrayList<SysUser>();

        for (int i = 0; i < allOperators.size(); i++) {
            SysUser operator = allOperators.get(i);
            if(operator.getName() != null){
                if(operator.getName().toLowerCase().contains(query) && !listOperators.contains(query)){
                    filteredOperators.add(operator);
                }	
            }
        }
        return filteredOperators;
    }
    public void handleSelectComplete(SelectEvent e){
    	SysUser su = (SysUser)e.getObject();
    	listOperatorsAdd.add(su);
    }
	public void handleUnSelectComplete(UnselectEvent e){
		SysUser su = (SysUser)e.getObject();
		listOperatorsRemove.add(su);
    }

	/**
	 * Get status Driver
	 * @return
	 */
	public void getStatus(){
		try {
			numberStops = null;
			stopsPrice = null;
			
			statusBeanRemote = EJBServiceLocator.getInstace().getEjbService(StatusBeanRemote.class);
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService(CityDriverBeanRemote.class);
			cityDriverStatus = new CityDriverStatus();
			listOperators = new ArrayList<SysUser>();
			listOperatorsAdd = new ArrayList<SysUser>();
			listOperatorsRemove = new ArrayList<SysUser>();

			listStatus = statusBeanRemote.getStatus();

			if(cityDriver.getIdCityStatus() != null){
				cityDriverStatus = cityDriverBeanRemote.getCityDriverStatus(cityDriver);
				setCommentStatus(cityDriverStatus.getComments());
				if(cityDriverStatus.getStops() != null){
					setNumberStops(cityDriverStatus.getStops().toString());	
				}
				setStopsPrice(cityDriverStatus.getStopsPrice());
			}

			List<CityDriverOperator> listCurrentOperators = cityDriverBeanRemote.currentCityDriversOperators(cityDriverBeanRemote.getCityDriverStatus(cityDriver));

	        for (int i = 0; i < listCurrentOperators.size(); i++) {
	        	CityDriverOperator operator = listCurrentOperators.get(i);
	        	listOperators.add(operator.getSysUser());
	        }
		} catch (Exception e) {
			LOGGER.error("Error listar driver", e);
			e.printStackTrace();
		}
	}

	/**
	 * Get more Info Driver
	 * @return
	 */
	public void getMoreDriver() {
		formNoEditable = true;
		getVehicleData();
	}
	
	/**
	 * Check is sys_user exister for new service
	 * @return
	 */
	public String checkDriver(){
		userHasVehicle = false;
		plate = null;
		vehicleType = null;
		vehicleDescription = null;
		vehicleBrand = null;
		try {
			beanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );
			cityDriver = new CityDriver();
			if(CU != null && CU.length() > 2 ){
				SysUser sysUserDriverCU = beanRemote.getUserByCU(CU);
				if ( sysUserDriverCU == null ){
					formUserExits = false;
					userType = 1;
					JsfUtil.addMessage("checkDriverNoExits", FacesMessage.SEVERITY_INFO);
				} else {
					setCU(CU);
					setEmail(sysUserDriverCU.getEmail());
					setSysUser(sysUserDriverCU);
					formUserExits = true;
					userType = 2;
				}
			} else if (email != null && email.length() > 2){
				SysUser sysUserDriverEmail = beanRemote.getUserByEmail(email);
				if ( sysUserDriverEmail == null ){
					formUserExits = false;
					userType = 1;
					JsfUtil.addMessage("checkDriverNoExits", FacesMessage.SEVERITY_INFO);
				} else {
					setEmail(email);
					setCU(sysUserDriverEmail.getCu());
					setSysUser(sysUserDriverEmail);
					formUserExits = true;
					userType = 2;
				}
			} else if (mobile != null && mobile.length() > 4){
				SysUser sysUserDriverMobile = beanRemote.getUserByMobile(mobile);
				if ( sysUserDriverMobile == null ){
					formUserExits = false;
					userType = 1;
					sysUser.setFavoriteMsisdn(mobile);
					JsfUtil.addMessage("checkDriverNoExits", FacesMessage.SEVERITY_INFO);
				} else {
					setMobile(sysUserDriverMobile.getFavoriteMsisdn());
					setSysUser(sysUserDriverMobile);
					changeCountry();
					try{
						vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
						listVehicle = vehicleBeanRemote.getSysUserVehicles(sysUserDriverMobile);
						if(listVehicle.size() > 0 ){
							userHasVehicle = true;
							getVehicleData("register");
						}
					} catch (Exception e) {
						LOGGER.error( "Error Obtener Vehiculo:", e );
					    e.printStackTrace();
					}
					formUserExits = true;
					userType = 2;
				}
			} else {
				JsfUtil.addMessage("checkDriverNoInput", FacesMessage.SEVERITY_ERROR);
			}


		} catch (Exception e)  {
			formUserExits = false;
			userType = 0;
			e.printStackTrace();
			JsfUtil.addMessage("registerDriverError", FacesMessage.SEVERITY_ERROR);
		}
		return null;
	}

	/**
	 * User type
	 * @return
	 */
	public void selectTypeUser(){
		formUserExits =  false;
		userType = 0;
		setEmail("");
		setCU("");
		setMobile("");
		sysUser = new SysUser();
		cityDriver = new CityDriver();
	}
	
	/**
	 * Toggle edit form
	 * @return
	 */
	public void toggleformEditable(){
		if(formNoEditable == true){
			formNoEditable = false;
		} else {
			formNoEditable = true;
		}
	}

	/**
	 * Get vehicle data by user
	 * @return
	 */
	public void getVehicleData(String action){
		try{
			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService(VehicleBeanRemote.class);
			vehicleType = null;
			vehicleBrand = null;
			vehicleDescription = null;	
			
			if(cityDriver != null && !"register".equals(action)){
				if(cityDriver.getPlate() != null && !"update".equals(action)){
					plate = cityDriver.getPlate();
				}
				sysUser = cityDriver.getSysUser();
			} else {
				sysUser = getSysUser();
			}
			if(sysUser != null){
				listVehicle = vehicleBeanRemote.getSysUserVehicles(sysUser);
				Vehicle dataVehicle = vehicleBeanRemote.getVehicle(plate, sysUser);
				if(dataVehicle != null){
					vehicleType = dataVehicle.getType();
					vehicleBrand = dataVehicle.getBrand();
					vehicleDescription = dataVehicle.getDescription();	
				}	
			}
		} catch (Exception e) {
			LOGGER.error("Error get data vehicle type and brand", e);
			e.printStackTrace();
		}
	}
	public void getVehicleData(){
		getVehicleData(null);
	}
	
	/**
	 * Get all cityDriver services
	 * @return
	 */
	public void getDriver(ValueChangeEvent event){
		try {
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService(CityDriverBeanRemote.class);
			listDriver = cityDriverBeanRemote.getCityDrivers();
		} catch (Exception e) {
			LOGGER.error("Error listar driver", e);
			e.printStackTrace();
		}

	}	

	/**
	 * Get city driver service by ID
	 * @return
	 */
	public void getCityDriversById(){
		try {
			cityDriverBeanRemote = EJBServiceLocator.getInstace().getEjbService(CityDriverBeanRemote.class);
			listDriverById = cityDriverBeanRemote.getCityDriversById("4");
		} catch (Exception e){
			LOGGER.error("Error get driver by id", e);
			e.printStackTrace();
		}
	}

	public void deleteDriver(ActionEvent event){
		//TODO
	}

	public void changeCountry() {
		try {
			String countrySelect = String.valueOf( sysUser.getCountryIdcountry() );
			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService( CountryBeanRemote.class );
			cities = countryBeanRemote.getCities( countrySelect );
		}
		catch ( Exception e ) {
			e.printStackTrace();
		}

	}
	// Getter and Setter
	public List<City> getCities() {
		return cities;
	}

	public void setCities( List<City> cities ) {
		this.cities = cities;
	}
	
	public void setListDriver(List<CityDriver> listDriver) {
		this.listDriver = listDriver;
	}

	public List<CityDriver> getListDriver() {
		return listDriver;
	}

	public void setListStatus(List<Status> listStatus) {
		this.listStatus = listStatus;
	}

	public List<Status> getListStatus() {
		return listStatus;
	}

	public void setListVehicle(List<SysUserVehicle> listVehicle){
		this.listVehicle = listVehicle;
	}
	public List<SysUserVehicle> getListVehicle(){
		return listVehicle;
	}

	public void setListVehicleType(List<VehicleType> listVehicleType){
		this.listVehicleType = listVehicleType;
	}
	public List<VehicleType> getListVehicleType(){
		return listVehicleType;
	}

	public void setListVehicleBrand(List<VehicleBrand> listVehicleBrand){
		this.listVehicleBrand = listVehicleBrand;
	}
	public List<VehicleBrand> getListVehicleBrand(){
		return listVehicleBrand;
	}

	public void setListCityDriverById(List<CityDriver> listDriverById) {
		this.listDriverById = listDriverById;
	}

	public List<CityDriver> getListCityDriverById() {
		return listDriverById;
	}

	public void setCityDriver(CityDriver cityDriver) {
		setSoat(1);
		setRtm(1);
		this.cityDriver = cityDriver;
	}
	public CityDriver getCityDriver() {
		return cityDriver;
	}

	public void setCityDriverStatus(CityDriverStatus cityDriverStatus) {
		this.cityDriverStatus = cityDriverStatus;
	}
	public CityDriverStatus getCityDriverStatus() {
		return cityDriverStatus;
	}

	public void setCityDriverOperator(CityDriverOperator cityDriverOperator) {
		this.cityDriverOperator = cityDriverOperator;
	}
	public CityDriverOperator getCityDriverOperator() {
		return cityDriverOperator;
	}

	public SysUser getSysUser()	{
		return sysUser;
	}
	public void setSysUser( SysUser sysUser ) {
		this.sysUser = sysUser;
	}

	public String getFullName(){
		return fullName;
	}
	public void setFullName(String fullName){
		this.fullName = fullName;
	}

	public String getCU(){
		return CU;
	}
	public void setCU(String CU){
		this.CU = CU;
	}

	public String getMobile(){
		return mobile;
	}
	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getEmail(){
		return email;
	}
	public void setEmail(String email){
		this.email = email;
	}

	public String getComment(){
		return comment;
	}
	public void setComment(String comment){
		this.comment = comment;
	}

	public String getCommentStatus(){
		return commentStatus;
	}
	public void setCommentStatus(String commentStatus){
		this.commentStatus = commentStatus;
	}

	public String getNumberStops(){
		return numberStops;
	}
	public void setNumberStops(String numberStops){
		this.numberStops = numberStops;
	}

	public String getStopsPrice(){
		return stopsPrice;
	}
	public void setStopsPrice(String stopsPrice){
		this.stopsPrice = stopsPrice;
	}


	public String getUserPhone(){
		return userPhone;
	}
	public void setUserPhone(String userPhone){
		this.userPhone = userPhone;
	}

	public int getUserType(){
		return userType;
	}
	public void setUserType(int userType){
		this.userType = userType;
	}

	public String getCity(){
		return city;
	}
	public void setCity(String city){
		this.city = city;
	}

	public String getPlate(){
		return plate;
	}
	public void setPlate(String plate){
		this.plate = plate;
	}

	public BigInteger getVehicleType(){
		return vehicleType;
	}
	public void setVehicleType(BigInteger vehicleType){
		this.vehicleType = vehicleType;
	}

	public BigInteger getVehicleBrand(){
		return vehicleBrand;
	}
	public void setVehicleBrand(BigInteger vehicleBrand){
		this.vehicleBrand = vehicleBrand;
	}

	public int getSoat(){
		return soat;
	}
	public void setSoat(int soat){
		this.soat = soat;
	}

	public int getRtm(){
		return rtm;
	}
	public void setRtm(int rtm){
		this.rtm = rtm;
	}

	public BigInteger getAssignTo(){
		return assignTo;
	}
	public void setAssignTo(BigInteger assignTo){
		this.assignTo = assignTo;
	}

	public BigInteger getToStatus(){
		return toStatus;
	}
	public void setToStatus(BigInteger toStatus){
		this.toStatus = toStatus;
	}

	public List<SysUser> getListOperators(){
		return listOperators;
	}
	public void setListOperators(List<SysUser> listOperators){
		this.listOperators = listOperators;
	}
	
	public List<SysUser> getAllOperators(){
		return allOperators;
	}
	public void setAllOperators(List<SysUser> allOperators){
		this.allOperators = allOperators;
	}

	public boolean getFormNoEditable(){
		return formNoEditable;
	}
	public void setFormNoEditable(boolean formNoEditable){
		this.formNoEditable = formNoEditable;
	}

	public boolean getFormUserExits(){
		return formUserExits;
	}
	public void setFormUserExits(boolean formUserExits){
		this.formUserExits = formUserExits;
	}

	public boolean getUserHasVehicle(){
		return userHasVehicle;
	}
	public void setUserHasVehicle(boolean userHasVehicle){
		this.userHasVehicle = userHasVehicle;
	}
	
	public String getVehicleDescription(){
		return vehicleDescription;	
	}
	public void setVehicleDescription(String vehicleDescription){
		this.vehicleDescription = vehicleDescription;
	}
}
