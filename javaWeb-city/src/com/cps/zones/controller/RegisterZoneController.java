/**
 * 
 */
package com.cps.zones.controller;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.City;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.Zone;
import com.cps.entity.bean.ZoneLocation;
import com.cps.remote.ClientBeanRemote;
import com.cps.remote.CountryBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 *
 */
@ManagedBean
@ViewScoped
public class RegisterZoneController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ClientBeanRemote clientBeanRemote;
	private ZoneBeanRemote zoneBeanRemote;
	private CountryBeanRemote countryBeanRemote;

	private List<Client> listClients;
	private Zone zone = new Zone();
	private List<City> listCities;
	private String openTime;
	private String closeTime;
	private String idCountry;
	private String idCity;
	private Client client;
	
	public RegisterZoneController(){
		
		try {
			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(ClientBeanRemote.class);
			setListClients(clientBeanRemote.getListClients());
			setClient(new Client());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Registra una zona
	 * @return
	 */
	public String registerZone(){
		try{
			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					CountryBeanRemote.class);
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(ZoneBeanRemote.class);
					
			//zone.setIdzone("id");
			zone.setOpenTime(Time.valueOf(openTime+":00"));
			zone.setCloseTime(Time.valueOf(closeTime+":00"));
			ZoneLocation  zl = new ZoneLocation();
			String pfix = countryBeanRemote.getCountryPrefix(idCountry);
			zone.setIdzone(pfix+zone.getIdzone());
			zl.setIdzone(zone.getIdzone());
			zl.setIdcountry(new BigInteger(idCountry));
			zl.setIdcity(new BigInteger(idCity));
			
			zoneBeanRemote.registerZone(zone, zl);
			
			JsfUtil.addMessage("registerZoneRegistered", FacesMessage.SEVERITY_INFO);
            
			zone = new Zone();
			
		}catch (Exception e) {
			e.printStackTrace();
			if(e.getMessage().toLowerCase(). contains("constraint"))
				JsfUtil.addMessage("registerZoneAlreadyRegistered", FacesMessage.SEVERITY_ERROR);
			else
				JsfUtil.addMessage("registerZoneNoRegistered", FacesMessage.SEVERITY_ERROR);

		}
		return null;
	}
	
	
	public void clientChangeMethod(){
		client = clientBeanRemote.getClient(zone.getClientIdclient());
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	public List<Client> getListClients() {
		return listClients;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public Zone getZone() {
		return zone;
	}

	
	public String getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	
	public void changeCountry( ) {
		try {

//			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService(
//					CountryBeanRemote.class);
//			listCities = idCountry == null || idCountry.equals("") ? new ArrayList<City>()
//				: countryBeanRemote.getCities(idCountry);

			listCities= new ArrayList<City>();
			List<City> cities = new ArrayList<City>(client.getClientCities());
			for(City city: cities )
			{
			   if(city.getIdcountry().equals(idCountry))
			   {
				   listCities.add(city);
			   }
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<City> getListCities() {
		return listCities;
	}

	public void setListCities(List<City> listCities) {
		this.listCities = listCities;
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public String getIdCity() {
		return idCity;
	}

	public void setIdCity(String idCity) {
		this.idCity = idCity;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}
