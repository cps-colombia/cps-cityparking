/**
 * 
 */
package com.cps.zones.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentSubZone;
import com.cps.entity.bean.AgentSubZonePK;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.PlaceSubzone;
import com.cps.entity.bean.Subzone;
import com.cps.entity.bean.Zone;
import com.cps.remote.AgentBeanRemote;
import com.cps.remote.ClientBeanRemote;
import com.cps.remote.SubZoneBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 *
 */
@ManagedBean
@ViewScoped
public class ManagerSubZoneController implements Serializable{
	
	/**
	 * 
	 */
	private Logger LOGGER = Logger.getLogger(ManagerSubZoneController.class);
	private static final long serialVersionUID = 1L;
	private ClientBeanRemote clientBeanRemote;
	private ZoneBeanRemote zoneBeanRemote;
	private SubZoneBeanRemote subZoneBeanRemote;
	private List<Client> listClients;
	private List<Zone> listZones;
	private List<Subzone> listSubZones;
	
	private List<Place> listPlaces;
	private List<PlaceSubzone> listPlacesSubzone;
	
	private Zone zoneSelected;
	private Subzone subZoneSelected;
	private Client clientSelected;
	private Agent selectedAgent;

	private Subzone subZone = new Subzone();
	
	private List<Agent> listAgents;
	
	private Place[] selectedAvailablePlaces;
	private PlaceSubzone[] selectedPlacesSubzone;

	
	public ManagerSubZoneController(){		
		try {
			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(ClientBeanRemote.class);
			listClients = clientBeanRemote.getListClients();
		} catch (Exception e) {
			LOGGER.error("Error listar clientes", e);
			e.printStackTrace();
		}
	}
	
	/**
	 * Evento del combo de cliente
	 */
	public void handleClientChange(){
		try {
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(ZoneBeanRemote.class);
			listZones = zoneBeanRemote.getZonesByIdClient(clientSelected.getIdclient());
		} catch (Exception e) {
			LOGGER.error("Error listar zonas", e);
			e.printStackTrace();
		}

	}
	
	/**
	 * Evento del combo de cliente
	 */
	public void handleZoneChange(){
		try {
			subZoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(SubZoneBeanRemote.class);
			listSubZones = subZoneBeanRemote.findAllByZone(zoneSelected.getIdzone());
		} catch (Exception e) {
			LOGGER.error("Error listar subzonas", e);
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 * @param event
	 */
	public void registerSubzone(ActionEvent event){
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		
		try {
			subZoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(SubZoneBeanRemote.class);
			subZone.setIdzone(zoneSelected);
			subZoneBeanRemote.create(subZone);
			listSubZones = subZoneBeanRemote.findAllByZone(zoneSelected.getIdzone());
			JsfUtil.addMessage("Registro Exitoso.", "", FacesMessage.SEVERITY_INFO);
            flag = true; 
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
            LOGGER.error("Error actualizar zonas", e);
            e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}
	
	/**
	 * Es invocado cuando se selecciona una subzona en la tabla
	 * @param subzone
	 */
	public void setSubZoneSelected(Subzone subZoneSelected){
		this.subZoneSelected = subZoneSelected;
		try{
		listAgents = subZoneBeanRemote.findAgentsBySubZone(""+subZoneSelected.getIdsubzone());
		listPlaces = subZoneBeanRemote.findPlacesAvailablesByZone(this.zoneSelected.getIdzone());
		listPlacesSubzone = subZoneBeanRemote.findPlacesBySubZone(""+this.subZoneSelected.getIdsubzone());
	    }catch (Exception e) {
			LOGGER.error("Error al seleccionar subzona", e);
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Es invocado cuando se elimina una subzona en la tabla
	 * @param subzone
	 */
	public void setRemoveSelectedSubZone(Subzone subZoneSelected){
		this.subZoneSelected = subZoneSelected;
		try{
			subZoneBeanRemote.destroy(subZoneSelected.getIdsubzone());
			listSubZones = subZoneBeanRemote.findAllByZone(zoneSelected.getIdzone());
			
	    }catch (Exception e) {
			LOGGER.error("Error al seleccionar subzona", e);
			e.printStackTrace();
		}
	}
	
	
    /**
     * Asocia un agente a una determinada subzona
     * @param event
     */
	public void addAgent(ActionEvent event){
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		
		try {
			AgentSubZonePK agentSubZonePK = new AgentSubZonePK();
			agentSubZonePK.setIdAgent(this.selectedAgent.getIdagent());
			agentSubZonePK.setIdSubZone(""+this.subZoneSelected.getIdsubzone());
			AgentSubZone agentSubZone = new AgentSubZone();
			agentSubZone.setId(agentSubZonePK);
			subZoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(SubZoneBeanRemote.class);
			subZoneBeanRemote.associateAgentSubzone(agentSubZone);
			listAgents = subZoneBeanRemote.findAgentsBySubZone(""+subZoneSelected.getIdsubzone());

			JsfUtil.addMessage("agentAsociatedZone", FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_INFO);
			LOGGER.error("No fue posible agregar agente", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}
	
	
	public void removeAgent( ActionEvent event )
	{
		
		RequestContext context = RequestContext.getCurrentInstance();
	
		try {
			
			String username  = (String) event.getComponent().getAttributes().get( "login" );
			subZoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(SubZoneBeanRemote.class);
        	AgentBeanRemote agentBeanRemote = EJBServiceLocator.getInstace().getEjbService(AgentBeanRemote.class);
        	Agent agent = agentBeanRemote.getAgent( username );
	
			AgentSubZonePK agentSubZonePK = new AgentSubZonePK();
			agentSubZonePK.setIdAgent(agent.getIdagent());
			agentSubZonePK.setIdSubZone(""+this.subZoneSelected.getIdsubzone());
			//AgentSubZone agentSubZone = new AgentSubZone();
			//agentSubZone.setId(agentSubZonePK);
			
			subZoneBeanRemote.disassociateAgentSubzone( agentSubZonePK);
			listAgents = subZoneBeanRemote.findAgentsBySubZone(""+subZoneSelected.getIdsubzone());

			JsfUtil.addMessage("agentDisassociateAsociatedZone", FacesMessage.SEVERITY_INFO);
		
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_INFO);
			LOGGER.error("No fue posible remover agente", e);
			e.printStackTrace();
		}
		
		

	}
	
	
	/**
	 * 
	 * @param event
	 */
	public void addPlace(ActionEvent event){
		
		try {
			if(selectedAvailablePlaces != null && selectedAvailablePlaces.length > 0){
			subZoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(SubZoneBeanRemote.class);
			PlaceSubzone placeSubzone;
			for (Place place : selectedAvailablePlaces) {
				placeSubzone = new PlaceSubzone();
				placeSubzone.setIdplace(place.getIdplace());
				placeSubzone.setSubzone(subZoneSelected);
				
				subZoneBeanRemote.createPlaceSubzone(placeSubzone);
			}
			listPlaces = subZoneBeanRemote.findPlacesAvailablesByZone(this.zoneSelected.getIdzone());
			listPlacesSubzone = subZoneBeanRemote.findPlacesBySubZone(""+this.subZoneSelected.getIdsubzone());		
			JsfUtil.addMessage("placesRegistered", FacesMessage.SEVERITY_INFO);
			}

		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
            LOGGER.error("Error actualizar zonas", e);
            e.printStackTrace();
		}
	}
	
	public void deletePlace(ActionEvent event){
		try {
			
			if(selectedPlacesSubzone != null && selectedPlacesSubzone.length > 0){
			subZoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(SubZoneBeanRemote.class);
			for (PlaceSubzone placeSubzone : selectedPlacesSubzone) {
 				 subZoneBeanRemote.destroyPlaceSubzone(placeSubzone);
			}
			listPlaces = subZoneBeanRemote.findPlacesAvailablesByZone(this.zoneSelected.getIdzone());
			listPlacesSubzone = subZoneBeanRemote.findPlacesBySubZone(""+this.subZoneSelected.getIdsubzone());		
			JsfUtil.addMessage("placesDeleted", FacesMessage.SEVERITY_INFO);
			}

		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
            LOGGER.error("No fue posible eliminar espacio", e);
            e.printStackTrace();
		}

	}
	
	/**
	 * Autocompletar de agente
	 * @param query
	 * @return
	 */
	public List<Agent> completeAgent(String query) {  
        List<Agent> suggestions = new ArrayList<Agent>();
        List<Agent> listAgentsComplete = null;
        try {
        	AgentBeanRemote agentBeanRemote = EJBServiceLocator.getInstace().getEjbService(AgentBeanRemote.class);
        	if(listAgentsComplete == null)
        		listAgentsComplete = agentBeanRemote.getAgents();
        	
        	for(Agent p : listAgentsComplete) {  
			    if(p.getName().toLowerCase().startsWith(query.toLowerCase()))  
			        suggestions.add(p);  
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  
          
        return suggestions;  
    }

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	public List<Client> getListClients() {
		return listClients;
	}

	public void setListZones(List<Zone> listZones) {
		this.listZones = listZones;
	}

	public List<Zone> getListZones() {
		return listZones;
	}

	public void setSelectedAgent(Agent selectedAgent) {
		this.selectedAgent = selectedAgent;
	}

	public Agent getSelectedAgent() {
		return selectedAgent;
	}

	
	public void setListAgents(List<Agent> listAgents) {
		this.listAgents = listAgents;
	}

	public List<Agent> getListAgents() {
		return listAgents;
	}

	public void setListPlaces(List<Place> listPlaces) {
		this.listPlaces = listPlaces;
	}

	public List<Place> getListPlaces() {
		return listPlaces;
	}

	public List<Subzone> getListSubZones() {
		return listSubZones;
	}

	public void setListSubZones(List<Subzone> listSubZones) {
		this.listSubZones = listSubZones;
	}

	public Zone getZoneSelected() {
		return zoneSelected;
	}

	public void setZoneSelected(Zone zoneSelected) {
		this.zoneSelected = zoneSelected;
	}

	public Subzone getSubZoneSelected() {
		return subZoneSelected;
	}

	public Client getClientSelected() {
		return clientSelected;
	}

	public void setClientSelected(Client clientSelected) {
		this.clientSelected = clientSelected;
	}

	public Subzone getSubZone() {
		return subZone;
	}

	public void setSubZone(Subzone subZone) {
		this.subZone = subZone;
	}

	public Place[] getSelectedAvailablePlaces() {
		return selectedAvailablePlaces;
	}

	public void setSelectedAvailablePlaces(Place[] selectedAvailablePlaces) {
		this.selectedAvailablePlaces = selectedAvailablePlaces;
	}

	public List<PlaceSubzone> getListPlacesSubzone() {
		return listPlacesSubzone;
	}

	public void setListPlacesSubzone(List<PlaceSubzone> listPlacesSubzone) {
		this.listPlacesSubzone = listPlacesSubzone;
	}

	public PlaceSubzone[] getSelectedPlacesSubzone() {
		return selectedPlacesSubzone;
	}

	public void setSelectedPlacesSubzone(PlaceSubzone[] selectedPlacesSubzone) {
		this.selectedPlacesSubzone = selectedPlacesSubzone;
	}
}
