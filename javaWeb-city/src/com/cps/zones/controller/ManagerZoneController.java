/**
 * 
 */
package com.cps.zones.controller;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Agent;
import com.cps.entity.bean.AgentZone;
import com.cps.entity.bean.AgentZonePK;
import com.cps.entity.bean.City;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.Place;
import com.cps.entity.bean.PriceRestrict;
import com.cps.entity.bean.RateType;
import com.cps.entity.bean.TimeRestrict;
import com.cps.entity.bean.VehicleType;
import com.cps.entity.bean.Zone;
import com.cps.remote.AgentBeanRemote;
import com.cps.remote.ClientBeanRemote;
import com.cps.remote.PlaceBeanRemote;
import com.cps.remote.VehicleBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.JsfUtil;
import com.mysema.query.alias.Alias;
import com.mysema.query.collections.CollQueryFactory;

/**
 * @author Jorge
 *
 */
@ManagedBean
@ViewScoped
public class ManagerZoneController implements Serializable {

	/**
	 * 
	 */
	private Logger LOGGER = Logger.getLogger(ManagerZoneController.class);
	private static final long serialVersionUID = 1L;
	private ClientBeanRemote clientBeanRemote;
	private VehicleBeanRemote vehicleBeanRemote;
	private AgentBeanRemote agentBeanRemote;
	private ZoneBeanRemote zoneBeanRemote;
	private PlaceBeanRemote placeBeanRemote;
	private List<Client> listClients;
	private String idCountry;
	private Client client;
	private List<Zone> listZones;
	private List<TimeRestrict> listTimeRestricts;
	private List<PriceRestrict> listPriceRestricts;
	private List<Place> listPlaces;
	private Zone zone = new Zone();
	private TimeRestrict timeRestrict = new TimeRestrict();
	private PriceRestrict priceRestrict = new PriceRestrict();
	private Place place = new Place();
	private String openTime;
	private String closeTime;
	private String startTime;
	private String endTime;
	private String clientSelect = "-1";
	private Agent selectedAgent;
	private int vehicleTypeId;
	private int rateTypeId;
	private List<Agent> listAgents;
	private List<RateType> rateTypes;
	private List<City> filteredCities = null;
	private List<VehicleType> vehicleTypes = new ArrayList<VehicleType>();
	private PriceRestrict priceals = Alias.alias(PriceRestrict.class, "prr");
	private boolean createPrice = false;

	private City cityals = Alias.alias(City.class, "ct");

	public ManagerZoneController() {
		try {
			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ClientBeanRemote.class);
			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					VehicleBeanRemote.class);
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			listClients = clientBeanRemote.getListClients();
			setVehicleTypes(vehicleBeanRemote.getVehicleType());
			rateTypes = zoneBeanRemote.getRateTypes();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listCountries(ValueChangeEvent event) {
		clientSelect = String.valueOf(event.getNewValue());

		try {

			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ClientBeanRemote.class);
			client = clientBeanRemote.getClient(clientSelect);
		} catch (Exception e) {
			LOGGER.error("Error listar zonas", e);
			e.printStackTrace();
		}
	}

	public void listCities(ValueChangeEvent event) {
		idCountry = String.valueOf(event.getNewValue());

		filteredCities = idCountry == null ? new ArrayList<City>()
				: CollQueryFactory
						.from(Alias.$(cityals), client.getClientCities())
						.where(Alias.$(cityals.getIdcountry()).eq(idCountry))
						.orderBy(Alias.$(cityals.getIdcountry()).asc())
						.list(Alias.$(cityals));
	}

	public void listZonesAction(ValueChangeEvent event) {
		String idCity = String.valueOf(event.getNewValue());

		try {

			// clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(
			// ClientBeanRemote.class);
			// client = clientBeanRemote.getClient(clientSelect);
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			if(idCountry!=null && idCity!=null )
			listZones = zoneBeanRemote.getZonesByCity(null, idCountry, idCity);// zoneBeanRemote.getZonesByIdClient(clientSelect);
		} catch (Exception e) {
			LOGGER.error("Error listar zonas", e);
			e.printStackTrace();
		}

	}

	public void toggleActivate(ActionEvent event) {
		try {
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);

			zoneBeanRemote.toggleActivate(zone.getIdzone(), zone.getActive());

			// String clientSelect=(client==null)?"-1":client.getIdclient();

			listZones = zoneBeanRemote.getZonesByIdClient(clientSelect);
			// setListCampaigns(campaignbr.listAllCampaigns());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param event
	 */
	public void updateZone(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			zone.setOpenTime(Time.valueOf(openTime + ":00"));
			zone.setCloseTime(Time.valueOf(closeTime + ":00"));
			zoneBeanRemote.updateZone(zone);
			JsfUtil.addMessage("Actualizar.", "", FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			LOGGER.error("Error actualizar zonas", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void registerTimeRestrict(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			timeRestrict.setZoneIdzone(this.zone.getIdzone());
			timeRestrict.setStartTime(Time.valueOf(startTime + ":00"));
			timeRestrict.setEndTime(Time.valueOf(endTime + ":00"));
			zoneBeanRemote.registerTimeRestrict(timeRestrict);
			setListTimeRestricts(zoneBeanRemote.getTimeRestrictByZone(this.zone
					.getIdzone()));
			timeRestrict = new TimeRestrict();
			setStartTime("");
			setEndTime("");
			JsfUtil.addMessage("timeRestrictCreated",
					FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			LOGGER.error("No fue posible registrar restriccion", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void deleteTimeRestrict(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			String idTimeRestrict = JsfUtil.getRequest().getParameter(
					"idTimeRestrict");
			zoneBeanRemote.deleteTimeRestrict(idTimeRestrict);
			setListTimeRestricts(zoneBeanRemote.getTimeRestrictByZone(this.zone
					.getIdzone()));
			JsfUtil.addMessage("timeRestrictDeleted",
					FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			LOGGER.error("No fue posible eliminar restriccion", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void registerPriceRestrict(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					VehicleBeanRemote.class);

			priceRestrict.setZone(this.zone);

			priceRestrict.setStartTime(Time.valueOf(startTime + ":00"));
			priceRestrict.setEndTime(Time.valueOf(endTime + ":00"));
			priceRestrict.setVehicleType(vehicleBeanRemote
					.getVehicleTypeById(vehicleTypeId));
			priceRestrict.setRateType(zoneBeanRemote
					.getRateTypeById(rateTypeId));

			List<PriceRestrict> pl = zoneBeanRemote
					.checkValidPriceRestrictItem(priceRestrict);

			// List<PriceRestrict> pl = CollQueryFactory
			// .from(Alias.$(priceals), listPriceRestricts)
			// .where(
			// Alias.$(priceals.getStartTime()
			// ).loe(priceRestrict.getEndTime())
			// .and(
			// Alias.$(priceals.getEndTime()).goe(priceRestrict.getStartTime())
			// )
			// .and( Alias.$(priceals.getDay()).eq(-1)
			// .or(Alias.$(priceals.getDay()).eq(-2)
			// .and(Integer.valueOf(7)).ne(priceRestrict.getDay())
			// .and(Alias.$(6).ne(priceRestrict.getDay()))))
			// .or( Alias.$(priceals.getDay()).eq(priceRestrict.getDay())))
			// .and(Alias.$(priceals.getVehicleType().getvTypeId()).eq(vehicleTypeId)))
			//
			// .list(Alias.$(priceals));
			//
			//
			if (pl.size() > 0&&rateTypeId!=2) {
				pl = CollQueryFactory
						.from(Alias.$(priceals), listPriceRestricts)
						.where(Alias.$(priceals.getRateType().getIdrateType())
								.ne(2).and(Alias.$(priceals.getRateType().getIdrateType()).eq(rateTypeId)))

						.list(Alias.$(priceals));

				if (pl.size() > 0)
				{
					JsfUtil.addMessage("zones.pricerestrict.add.error", FacesMessage.SEVERITY_ERROR);

					return;
				}
			}

			zoneBeanRemote.registerPriceRestrict(priceRestrict);

			setListPriceRestricts(zoneBeanRemote
					.getPriceRestrictByZone(this.zone.getIdzone()));
			priceRestrict = new PriceRestrict();
			setStartTime("");
			setEndTime("");
			JsfUtil.addMessage("priceRestrictCreated",
					FacesMessage.SEVERITY_INFO);
			flag = true;
			createPrice=true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			LOGGER.error("No fue posible eliminar restriccion", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void deletePriceRestrict(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
//			String idPriceRestrict = JsfUtil.getRequest().getParameter(
//					"idPriceRestrict");
			zoneBeanRemote.deletePriceRestrict(priceRestrict.getIdtimePriceRestriction());
			setListPriceRestricts(zoneBeanRemote
					.getPriceRestrictByZone(this.zone.getIdzone()));
			JsfUtil.addMessage("priceRestrictDeleted",
					FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			LOGGER.error("No fue posible eliminar restriccion", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void addAgent(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;

		try {
			AgentZonePK agentZonePK = new AgentZonePK();
			agentZonePK.setIdAgent(this.selectedAgent.getIdagent());
			agentZonePK.setIdZone(this.zone.getIdzone());
			AgentZone agentZone = new AgentZone();
			agentZone.setId(agentZonePK);
			agentBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					AgentBeanRemote.class);
			agentBeanRemote.registerAgentZone(agentZone);
			setListAgents(agentBeanRemote.getAgentsZone(this.zone.getIdzone()));
			selectedAgent = new Agent();
			JsfUtil.addMessage("agentAsociatedZone", FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_INFO);
			LOGGER.error("No fue posible agregar agente", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void addPlace(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;

		try {
			placeBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					PlaceBeanRemote.class);
			place.setZone(zone);
			placeBeanRemote.registerPlace(place);
			setListPlaces(placeBeanRemote.getPlacesZone(this.zone.getIdzone()));
			JsfUtil.addMessage("placesRegistered", FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			LOGGER.error("No fue posible agregar place", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void deletePlace(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			placeBeanRemote.deletePlace(place);
			setListPlaces(placeBeanRemote.getPlacesZone(this.zone.getIdzone()));
			JsfUtil.addMessage("placesDeleted", FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			LOGGER.error("No fue posible eliminar espacio", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	public List<Client> getListClients() {
		return listClients;
	}

	public void setListZones(List<Zone> listZones) {
		this.listZones = listZones;
	}

	public List<Zone> getListZones() {
		return listZones;
	}

	public void setZone(Zone zone) {
		openTime = zone.getOpenTime().toString();
		closeTime = zone.getCloseTime().toString();
		this.zone = zone;
	}

	public void setZoneRestrict(Zone zone) {
		try {
			this.zone = zone;
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			agentBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					AgentBeanRemote.class);
			placeBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					PlaceBeanRemote.class);
			setListPriceRestricts(zoneBeanRemote
					.getPriceRestrictByZone(this.zone.getIdzone()));
			setListTimeRestricts(zoneBeanRemote.getTimeRestrictByZone(this.zone
					.getIdzone()));
			setListAgents(agentBeanRemote.getAgentsZone(this.zone.getIdzone()));
			setListPlaces(placeBeanRemote.getPlacesZone(this.zone.getIdzone()));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Zone getZone() {
		return zone;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	public String getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}

	public void setListTimeRestricts(List<TimeRestrict> listTimeRestricts) {
		this.listTimeRestricts = listTimeRestricts;
	}

	public List<TimeRestrict> getListTimeRestricts() {
		return listTimeRestricts;
	}

	public void setListPriceRestricts(List<PriceRestrict> listPriceRestricts) {
		this.listPriceRestricts = listPriceRestricts;
	}

	public List<PriceRestrict> getListPriceRestricts() {
		return listPriceRestricts;
	}

	public void setTimeRestrict(TimeRestrict timeRestrict) {
		this.timeRestrict = timeRestrict;
	}

	public TimeRestrict getTimeRestrict() {
		return timeRestrict;
	}

	public void setPriceRestrict(PriceRestrict priceRestrict) {
		this.priceRestrict = priceRestrict;
	}

	public PriceRestrict getPriceRestrict() {
		return priceRestrict;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setSelectedAgent(Agent selectedAgent) {
		this.selectedAgent = selectedAgent;
	}

	public Agent getSelectedAgent() {
		return selectedAgent;
	}

	public List<Agent> completeAgent(String query) {
		List<Agent> suggestions = new ArrayList<Agent>();
		List<Agent> listAgents = null;
		try {
			AgentBeanRemote agentBeanRemote = EJBServiceLocator.getInstace()
					.getEjbService(AgentBeanRemote.class);
			if (listAgents == null)
				listAgents = agentBeanRemote.getAgents();

			for (Agent p : listAgents) {
				if (p.getName().toLowerCase().startsWith(query.toLowerCase()))
					suggestions.add(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return suggestions;
	}

	public void setListAgents(List<Agent> listAgents) {
		this.listAgents = listAgents;
	}

	public List<Agent> getListAgents() {
		return listAgents;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Place getPlace() {
		return place;
	}

	public void setListPlaces(List<Place> listPlaces) {
		this.listPlaces = listPlaces;
	}

	public List<Place> getListPlaces() {
		return listPlaces;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public List<City> getFilteredCities() {
		return filteredCities;
	}

	public void setFilteredCities(List<City> filteredCities) {
		this.filteredCities = filteredCities;
	}

	public List<VehicleType> getVehicleTypes() {
		return vehicleTypes;
	}

	public void setVehicleTypes(List<VehicleType> vehicleTypes) {
		this.vehicleTypes = vehicleTypes;
	}

	public int getVehicleTypeId() {
		return vehicleTypeId;
	}

	public void setVehicleTypeId(int vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}

	public int getRateTypeId() {
		return rateTypeId;
	}

	public void setRateTypeId(int rateTypeId) {
		this.rateTypeId = rateTypeId;
	}

	public List<RateType> getRateTypes() {
		return rateTypes;
	}

	public void setRateTypes(List<RateType> rateTypes) {
		this.rateTypes = rateTypes;
	}

	public boolean isCreatePrice() {
		return createPrice;
	}

	public void setCreatePrice(boolean createPrice) {
		this.createPrice = createPrice;
	}

}
