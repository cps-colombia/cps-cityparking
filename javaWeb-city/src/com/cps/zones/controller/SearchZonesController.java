package com.cps.zones.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Country;
import com.cps.entity.bean.Zone;
import com.cps.remote.CountryBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.JsfUtil;

@ManagedBean
public class SearchZonesController {

	private ZoneBeanRemote zoneBeanRemote;
	private CountryBeanRemote countryBeanRemote;
	private List<Zone> listZone;
	private MapModel simpleModel;
	private Country country;

	public void changeZone(ValueChangeEvent event) {
		try {
			String countrySelect = String.valueOf(event.getNewValue());
			
			if (countrySelect != null && !countrySelect.equals("-1")) {
				countryBeanRemote = EJBServiceLocator.getInstace().getEjbService(CountryBeanRemote.class);
				country = countryBeanRemote.getCountry(countrySelect);
				zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
						ZoneBeanRemote.class);
				listZone = zoneBeanRemote.getZones(countrySelect);
				simpleModel = new DefaultMapModel();
				for (Zone zone : listZone) {
					simpleModel.addOverlay(new Marker(new LatLng(Double
							.valueOf(zone.getLatitude()), Double.valueOf(zone
							.getLongitude())), zone.getName()));
				}
			}else{
				country = null;
				listZone = null;
			}
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}
