package com.cps.util;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cps.entity.bean.SysUser;

public class JsfUtil {

	public static void addMessage(String summary, String detail,FacesMessage.Severity severity) {
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(severity,
						summary, detail));
	}
	
	public static void addMessage(String propertyName ,FacesMessage.Severity severity) {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msgs");
	    String message = bundle.getString(propertyName);
	    String [] messages = message.split("\\|");
		context.addMessage(
				null,
				new FacesMessage(severity,
						messages[0], messages.length > 1 ? messages[1] : ""));
	}
	
	public static FacesMessage getMessage(String propertyName ,FacesMessage.Severity severity) {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msgs");
	    String message = bundle.getString(propertyName);
	    String [] messages = message.split("\\|");
	    FacesMessage facesMessage = new FacesMessage(severity,messages[0],messages.length > 1 ? messages[1] : "");
		return facesMessage;
	}
	
	public static HttpServletRequest getRequest(){
		FacesContext context = FacesContext.getCurrentInstance();
		return (HttpServletRequest) context.getExternalContext().getRequest();
	}
	
	
	public static void setRequestAtribute(String name, Object value){
		HttpServletRequest request = getRequest();
		request.setAttribute(name, value);
	}
	
	public synchronized static void setSessionAtribute(String name, Object value){
		HttpSession session = getRequest().getSession();
		session.setAttribute(name, value);
	}
	
	public static Object getSessionAtribute(String name){
		HttpSession session = getRequest().getSession();
		return session.getAttribute(name);
	}

	
	public synchronized static SysUser getUserSession(){
		HttpSession session = getRequest().getSession();
		return (SysUser) session.getAttribute("userSession");
	}
	
	public static String getStringMessageFor(String key){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msgs");
		    String message = bundle.getString(key);
		    return message;
		}catch (Exception e) {
			return "";
		}
	}
}
