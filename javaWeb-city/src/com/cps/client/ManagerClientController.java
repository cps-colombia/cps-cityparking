/**
 * 
 */
package com.cps.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.City;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.ClientCity;
import com.cps.entity.bean.ClientCountry;
import com.cps.entity.bean.Country;
import com.cps.remote.ClientBeanRemote;
import com.cps.remote.CountryBeanRemote;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 * 
 */
@ManagedBean
@ViewScoped
public class ManagerClientController implements Serializable {

	private TreeNode root = new DefaultTreeNode(new TreeItem(-1, "-", "-1"),
			null);

	public static class TreeItem implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private int type;
		private String name;
		private String id;

		public TreeItem() {
			super();
			// TODO Auto-generated constructor stub
		}

		public TreeItem(int type, String name, String id) {
			super();
			this.type = type;
			this.name = name;
			this.id = id;
		}

		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		@Override
		public int hashCode() {
			final int hash = 3245234;
			int result = 5;
			result = hash * result + ((name == null) ? 0 : name.hashCode());
			result += hash * result + type;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TreeItem other = (TreeItem) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (id != other.id)
				return false;

			return true;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return name;
		}

	}

	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(ManagerClientController.class);
	private TreeItem selectedItem;
	private ClientBeanRemote clientBeanRemote;
	private CountryBeanRemote countryBeanRemote;
	private List<Client> listClients;
	private Client client;
	private String idCountry;
	private String city;
	private List<City> listCities;

	public ManagerClientController() {
		try {
			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ClientBeanRemote.class);
			setListClients(clientBeanRemote.getListClients());
			client = new Client();
			// fillInfo();

		} catch (Exception e) {
			LOGGER.error("Error Manager Cliente controller", e);
		}
	}

	/**
	 * Registrar un nuevo client
	 * 
	 * @param event
	 */
	public void registerClient(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ClientBeanRemote.class);
			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					CountryBeanRemote.class);
			// Country country = countryBeanRemote.getCountry(idCountry);
			// City cityObj = countryBeanRemote.getCity(city);
			// client.setCity(cityObj);
			// client.setCountryIdcountry(country);
			client.setStatus("");
			clientBeanRemote.registerClient(client);
			setListClients(clientBeanRemote.getListClients());
			client = new Client();
			JsfUtil.addMessage("myClient.registerOk",
					FacesMessage.SEVERITY_INFO);

			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			flag = false;
			LOGGER.error("No se pudo registrar el cliente", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	/**
	 * Registrar un nuevo client
	 * 
	 * @param event
	 */
	public void deleteClient() {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ClientBeanRemote.class);
			client.setStatus("I");
			clientBeanRemote.updateClient(client);
			setListClients(clientBeanRemote.getListClients());
			JsfUtil.addMessage("myClient.deletedClient",
					FacesMessage.SEVERITY_INFO);
			client = new Client();
			flag = true;
		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			flag = false;
			LOGGER.error("No se pudo borrar el cliente", e);
			e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	public List<Client> getListClients() {
		return listClients;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getClient() {
		return client;
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public void setListCities(List<City> listCities) {
		this.listCities = listCities;
	}

	public List<City> getListCities() {
		return listCities;
	}

	public void changeCountry() {
		try {

			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					CountryBeanRemote.class);
			listCities = selectedItem == null
					|| selectedItem.getId().equals("") ? new ArrayList<City>()
					: countryBeanRemote.getCities(selectedItem.getId());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addCountry(ActionEvent event) {

		try {
			if (clientBeanRemote != null)
				clientBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(ClientBeanRemote.class);

			ClientCountry clientCountry = new ClientCountry(
					client.getIdclient(), idCountry);

			clientBeanRemote.registerClientCountry(clientCountry);
		} catch (Exception e) {
			e.printStackTrace();
		}
		updateCLient();

	}

	public void addCity(ActionEvent event) {

		try {
			if (clientBeanRemote != null)
				clientBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(ClientBeanRemote.class);

			clientBeanRemote.registerClientCity(new ClientCity(client
					.getIdclient(), selectedItem.getId(), city));
		} catch (Exception e) {
			e.printStackTrace();
		}

		updateCLient();

	}

	public void deleteCountry() {
		root.getChildren().contains(selectedItem);

		try {
			if (clientBeanRemote != null)
				clientBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(ClientBeanRemote.class);

			clientBeanRemote.deleteClientCountry(client.getIdclient(),
					selectedItem.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		updateCLient();

	}

	public void deleteCity() {

		try {
			if (clientBeanRemote != null)
				clientBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(ClientBeanRemote.class);

			List<TreeNode> chidrenCountries = root.getChildren();

			TreeNode selectedCityNode = null;
			TreeNode selectedCountryNode = null;
			for (TreeNode chCountry : chidrenCountries) {
				List<TreeNode> chidrenCities = chCountry.getChildren();

				for (TreeNode chCity : chidrenCities) {
					TreeItem itemt = (TreeItem) chCity.getData();

					if (itemt.equals(selectedItem)) {
						selectedCountryNode = chCountry;

						selectedCityNode = chCity;
						break;
					}

				}

			}

			TreeItem itemt = (TreeItem) selectedCountryNode.getData();

			clientBeanRemote.deleteClientCity(client.getIdclient(),
					itemt.getId(), selectedItem.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// selectedCountryNode.getChildren().remove(selectedCityNode);
		updateCLient();

	}

	private void updateCLient() {
		try {
			clientBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ClientBeanRemote.class);
			setListClients(clientBeanRemote.getListClients());
			for (Client cl : listClients) {
				if (cl.getIdclient().equals(client.getIdclient())) {
					client = cl;
					break;
				}
			}

			fillInfo();
		} catch (Exception e) {
			LOGGER.error("Error Manager Cliente controller", e);
		}
	}

	public void fillInfo() {

		root.getChildren().clear();

		List<Country> countries = new ArrayList<Country>(
				client.getClientCountries());
		List<City> cities = new ArrayList<City>(client.getClientCities());

		for (Country country : countries) {
			TreeNode itemCountry = new DefaultTreeNode(new TreeItem(1,
					country.getName(), country.getIdcountry()), root);
			for (City city : cities) {
				if (city.getIdcountry().equals(country.getIdcountry())) {
					new DefaultTreeNode(new TreeItem(2, city.getName(),
							city.getIdcity()), itemCountry);
				}
			}

		}
		// fillInfo();

		// TreeNode Colombia = new DefaultTreeNode(new TreeItem(1, "Colombia",
		// "472"), root);
		// TreeNode Bogota = new DefaultTreeNode(new TreeItem(2, "Bogota",
		// "103"), Colombia);

	}

	public TreeNode getRoot() {
		return root;
	}

	public TreeItem getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(TreeItem selectedItem) {
		this.selectedItem = selectedItem;
	}

}
