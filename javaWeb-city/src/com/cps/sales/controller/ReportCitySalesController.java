/**
 * 
 */
package com.cps.sales.controller;

import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.PayPalTransaction;
import com.cps.remote.PayPalBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;

/**
 * @author Juan Otero
 * 
 */
@ManagedBean
@ViewScoped
public class ReportCitySalesController implements Serializable {

	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(ReportCitySalesController.class);

	private PayPalBeanRemote paypalBeanRemote;
		
	private List<PayPalTransaction> listReportSales;

	private String paymentMethod;
	private Date fechaIni;
	private Date fechaFin;
	private double totalMoney;
	
	private boolean admin = false;
	private boolean sales = false;
	
	
	public ReportCitySalesController() {
		this.fechaIni = new Date();
		this.fechaFin = new Date();
	}
	
	
	/**
	 * Consulta las ventas realizadas en diferentes medios.
	 * 
	 * @param event
	 */
	public void reportSales(ActionEvent event) {
		try {
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			
			totalMoney = 0;
			
			paypalBeanRemote = EJBServiceLocator.getInstace().getEjbService(PayPalBeanRemote.class);
			listReportSales = paypalBeanRemote.getReportSales(fechaI + " 00:01:01", fechaF + " 23:59:59", paymentMethod);
			for (PayPalTransaction element : listReportSales) {
				totalMoney = totalMoney + element.getAmount();
			}
			
			setSales(listReportSales != null && !listReportSales.isEmpty() ? true : false);

		} catch (Exception e) {
			LOGGER.error("No se pudo realizar la consulta : ",e);
			e.printStackTrace();
		}

	}
	
	public void generarInforme(){
        try{
		
		String fechaI = DateHelper.format(getFechaIni(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getFechaFin(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "reportSales_"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("fechaIni", fechaI + " 00:01:01");
		parameter.put("fechaFin", fechaF + " 23:59:59");
		parameter.put("idMethod", paymentMethod);
		parameter.put("descriptionMethod", this.getDescripcionFormaPago(paymentMethod));
		
		JasperReportHelper jrh = new JasperReportHelper("reportSales", parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
        	LOGGER.error("No se pudo realizar el informe : ",e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	/**
	 * 
	 * @param idForma
	 * @return
	 */
	private String getDescripcionFormaPago(String idForma){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msgs");
		    String message = bundle.getString("parameter.sales.method."+idForma+".description");
		    return message;
		}catch (Exception e) {
			return "";
		}
	}
	
	private String getMessageFor(String key){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msgs");
		    String message = bundle.getString(key);
		    return message;
		}catch (Exception e) {
			return "";
		}
	}
   


	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}


	public void setTotalMoney(double totalMoney) {
		this.totalMoney = totalMoney;
	}

	public double getTotalMoney() {
		return totalMoney;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isAdmin() {
		return admin;
	}


	public boolean isSales() {
		return sales;
	}


	public void setSales(boolean sales) {
		this.sales = sales;
	}


	public String getPaymentMethod() {
		return paymentMethod;
	}


	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
    public List<PayPalTransaction> getListReportSales() {
		return listReportSales;
	}


	public void setListReportSales(List<PayPalTransaction> listReportSales) {
		this.listReportSales = listReportSales;
	}


}
