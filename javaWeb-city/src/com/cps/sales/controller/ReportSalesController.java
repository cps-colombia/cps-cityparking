/**
 * 
 */
package com.cps.sales.controller;

import java.io.OutputStream;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.data.FilterEvent;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.CityParkingTransactionUser;
import com.cps.entity.bean.PayPalTransaction;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.UserTransactionReport;
import com.cps.jsf.validator.EmailValidator;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.remote.PayPalBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;
import com.mysema.query.alias.Alias;
import com.mysema.query.collections.CollQueryFactory;

/**
 * @author Jorge
 * 
 */
@ManagedBean
@ViewScoped
public class ReportSalesController implements Serializable {

	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(ReportSalesController.class);
	private SysUserBeanRemote sysUserBeanRemote;
	private PayPalBeanRemote paypalBeanRemote;
	private CityParkingBeanRemote cityParkingBeanRemote;
	private List<PayPalTransaction> listReportSales;
	private List<CityParkingTransactionUser> listReportCitySales;
	private String paymentMethod;
	private String useremail;
	private String fullUsername;
	private CityParkingTransactionUser trusr = Alias.alias(
			CityParkingTransactionUser.class, "cityparkingtransactionUser");

	private Date fechaIni;
	private Date fechaFin;
	private double totalMoney;
	private double totalCompletedTransacMoney;
	private double totalRejectedTransacMoney;
	private int numberOfCompletedTransacMoney;
	private int numberOfRejecttedTransac;

	private boolean admin = false;
	private boolean sales = false;
	private SysUser sysUser;

	public ReportSalesController() {
		// this.fechaIni = new Date();
		this.fechaFin = new Date();

	}

	/**
	 * Consulta las ventas realizadas en diferentes medios.
	 * 
	 * @param event
	 */
	public void reportSales(ActionEvent event) {
		try {
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);

			totalMoney = 0;

			paypalBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					PayPalBeanRemote.class);
			listReportSales = paypalBeanRemote.getReportSales(fechaI
					+ " 00:01:01", fechaF + " 23:59:59", paymentMethod);

			for (PayPalTransaction element : listReportSales) {
				totalMoney = totalMoney + element.getAmount();
			}

			setSales(listReportSales != null && !listReportSales.isEmpty() ? true
					: false);

		} catch (Exception e) {
			LOGGER.error("No se pudo realizar la consulta : ", e);
			e.printStackTrace();
		}

	}

	public void onCompleteFilterSales(FilterEvent e) {

		DataTable dt = (DataTable) e.getSource();
		dt.getFilteredValue();
		Map<String, Object> filters = e.getFilters();

		String filterIdTransaction = (filters.containsKey("idTransaction")) ? filters
				.get("idTransaction").toString() : "";
		String filterStateTransaction = (filters
				.containsKey("stateTransaction")) ? filters.get(
				"stateTransaction").toString() : "";

		List<CityParkingTransactionUser> result;
		
		if (filterStateTransaction.equals("")) {
			result = CollQueryFactory
					.from(Alias.$(trusr), listReportCitySales)
					.where(Alias.$(trusr.getStateTransaction()).contains(
							"Completa")).list(Alias.$(trusr));
		} else {
			result = CollQueryFactory
					.from(Alias.$(trusr), listReportCitySales)
					.where(Alias
							.$(trusr.getIdTransaction())
							.contains(filterIdTransaction)
							.and(Alias.$(trusr.getStateTransaction())
									.containsIgnoreCase(filterStateTransaction)))
					.list(Alias.$(trusr));
		}
		double totalAmount = 0;
		for (CityParkingTransactionUser utr : result) {
			totalAmount += utr.getAmount();
		}

		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		String moneyTotalAmount = formatter.format(totalAmount);

		RequestContext.getCurrentInstance().addCallbackParam(
				"totalcompletedamount", moneyTotalAmount);

		// RequestContext.getCurrentInstance().update(":contact-form:tbl");

	}

	public void reportCitySales(ActionEvent event) {
		try {
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);

			totalCompletedTransacMoney = 0;
			totalRejectedTransacMoney = 0;
			numberOfCompletedTransacMoney = 0;
			numberOfRejecttedTransac = 0;
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);
			// sysUser = sysUserBeanRemote.getUserByEmail(useremail);

			if (useremail.equals("")) {
				useremail = "%";
			}
			// else
			// {
			// FacesContext facesContext = FacesContext.getCurrentInstance();
			// EmailValidator ev = new EmailValidator();
			// ev.validate(facesContext,
			// facesContext.getViewRoot().findComponent("registerEmail"),
			// useremail);

			// }

			cityParkingBeanRemote = EJBServiceLocator.getInstace()
					.getEjbService(CityParkingBeanRemote.class);
			// listReportCitySales =
			// cityParkingBeanRemote.getReportSalesByUser(sysUser,fechaI +
			// " 00:01:01", fechaF + " 23:59:59");
			listReportCitySales = cityParkingBeanRemote.getReportSales(fechaI
					+ " 00:01:01", fechaF + " 23:59:59", useremail,
					paymentMethod);

			if (listReportCitySales.size() > 0) {

				// setFullUsername((sysUser.getName()+" "+sysUser.getLastName()).toUpperCase());
			}

			for (CityParkingTransactionUser element : listReportCitySales) {
				if (element.getStateTransaction().toLowerCase()
						.contains("completed")) {
					totalCompletedTransacMoney = totalCompletedTransacMoney
							+ element.getAmount();
					numberOfCompletedTransacMoney++;
				} else {
					totalRejectedTransacMoney = totalRejectedTransacMoney
							+ element.getAmount();
					numberOfRejecttedTransac++;
				}

				// element.getSysUser().
				element.setStateTransaction(getMessageFor("parameter.sales.state."
						+ element.getStateTransaction().toLowerCase()));

			}

			setSales(listReportCitySales != null
					&& !listReportCitySales.isEmpty());

		} catch (Exception e) {
			LOGGER.error("No se pudo realizar la consulta : ", e);
			e.printStackTrace();
		}

	}

	public void reportUserCitySales(ActionEvent event) {
		try {
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);

			totalMoney = 0;
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);

			sysUser = sysUserBeanRemote.getUser(JsfUtil.getUserSession()
					.getIdsysUser().toString());

			cityParkingBeanRemote = EJBServiceLocator.getInstace()
					.getEjbService(CityParkingBeanRemote.class);
			listReportCitySales = cityParkingBeanRemote.getReportSalesByUser(
					sysUser, fechaI + " 00:01:01", fechaF + " 23:59:59");
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);
			useremail = sysUser.getEmail();
			setFullUsername((sysUser.getName() + " " + sysUser.getLastName())
					.toUpperCase());

			for (CityParkingTransactionUser element : listReportCitySales) {
				if (element.getStateTransaction().toLowerCase()
						.contains("completed"))
					totalMoney = totalMoney + element.getAmount();
				element.setStateTransaction(getMessageFor("parameter.sales.state."
						+ element.getStateTransaction().toLowerCase()));

			}

			setSales(listReportCitySales != null
					&& !listReportCitySales.isEmpty());

		} catch (Exception e) {
			LOGGER.error("No se pudo realizar la consulta : ", e);
			JsfUtil.addMessage(e.getMessage(), FacesMessage.SEVERITY_ERROR);

			e.printStackTrace();
		}

	}

	public void generarInforme() {
		try {

			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			HttpServletResponse response = (HttpServletResponse) externalContext
					.getResponse();

			String formato = (String) externalContext.getRequestParameterMap()
					.get("formato");

			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");

			String filename = "reportSales_"
					+ formatter.format(new java.util.Date());

			HashMap<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("fechaIni", fechaI + " 00:01:01");
			parameter.put("fechaFin", fechaF + " 23:59:59");
			parameter.put("idMethod", paymentMethod);
			parameter.put("descriptionMethod",
					this.getDescripcionFormaPago(paymentMethod));

			JasperReportHelper jrh = new JasperReportHelper("reportSales",
					parameter);
			JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS
					: JasperReportHelper.formatos.PDF;

			byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

			OutputStream os = response.getOutputStream();

			response.setContentType("application/" + formato); // fill in

			response.setContentLength(pdfStreamByteArray.length);

			response.setHeader("Content-disposition", "attachment; filename=\""
					+ filename + "." + formato + "\"");

			os.write(pdfStreamByteArray); // fill in bytes

			os.flush();
			os.close();
			facesContext.responseComplete();
		} catch (Exception e) {
			LOGGER.error("No se pudo realizar el informe : ", e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	private String emailFilter = "";
	private String cuFilter = "";
	private String transactFilter = "";
	private String placeFilter = "";

	public void onCompleteFilter(FilterEvent e) {
		System.out.println("filtering." + e.getFilters());

		List<CityParkingTransactionUser> listReportCitySa = (List<CityParkingTransactionUser>) e
				.getData();
		if (listReportCitySa == null)
			return;

		totalCompletedTransacMoney = 0;
		numberOfCompletedTransacMoney = 0;
		totalRejectedTransacMoney = 0;
		numberOfRejecttedTransac = 0;

		for (CityParkingTransactionUser element : listReportCitySa) {

			// System.out.println("filtering.... startwith"+
			// element.getStateTransaction());

			if (element.getStateTransaction().toLowerCase().contains("complet")) {
				totalCompletedTransacMoney = totalCompletedTransacMoney
						+ element.getAmount();
				numberOfCompletedTransacMoney++;

			} else {
				totalRejectedTransacMoney = totalRejectedTransacMoney
						+ element.getAmount();
				numberOfRejecttedTransac++;
			}
		}
		// element.getSysUser().
		// element.setStateTransaction(getMessageFor("parameter.sales.state."+
		// element.getStateTransaction().toLowerCase()));

		// System.out.println("complete transaction"+
		// totalCompletedTransacMoney);

		// setSales(listReportCitySales != null &&
		// !listReportCitySales.isEmpty() );
		// RequestContext.getCurrentInstance().update("contact-form:tbl:ftr");

		RequestContext.getCurrentInstance().addCallbackParam(
				"totalreloadcompletedamount",
				getMessageFor("myHistory.totalreloadcomplentedamount") + " : "
						+ totalCompletedTransacMoney);
		RequestContext.getCurrentInstance().addCallbackParam(
				"numberOfCompletedTransacMoney",
				getMessageFor("myHistory.numberofcompletedtransac") + " : "
						+ numberOfCompletedTransacMoney);

		RequestContext.getCurrentInstance().addCallbackParam(
				"totalRejectedTransacMoney",
				getMessageFor("myHistory.totalreloadrejectedamount") + " : "
						+ totalRejectedTransacMoney);
		RequestContext.getCurrentInstance().addCallbackParam(
				"numberOfRejecttedTransac",
				getMessageFor("myHistory.numberofrejectedtransac") + " : "
						+ numberOfRejecttedTransac);
		System.out.println("numberOfRejecttedTransac="
				+ getMessageFor("myHistory.numberofrejectedtransac") + " : "
				+ numberOfRejecttedTransac);

		// RequestContext.getCurrentInstance().update("tbl:ftr");
	}

	public boolean filterEmail(Object value, Object filter, Locale locale) {
		String filterText = (filter == null) ? null : filter.toString().trim();
		if (filterText == null) {
			return true;
		}

		if (value == null) {
			return false;
		}

		emailFilter = filterText.toLowerCase();
		String carName = value.toString().toLowerCase();

		// System.out.println("carName...."+ carName);

		totalCompletedTransacMoney = 0;
		numberOfCompletedTransacMoney = 0;
		totalRejectedTransacMoney = 0;
		numberOfRejecttedTransac = 0;
		if (carName.startsWith(emailFilter.toLowerCase())
				|| emailFilter.equals("")) {

			applyFilterOperation();
			return true;
		}

		return false;
	}

	public boolean filterPlace(Object value, Object filter, Locale locale) {
		placeFilter = "";
		System.out.println("filtering place....");
		String filterText = (filter == null) ? null : filter.toString().trim();
		if (filterText == null || filterText == "") {
			return true;
		}
		placeFilter = "col" + filterText.toLowerCase().replace(" ", "");
		if (value == null) {
			return false;
		}

		String carName = value.toString().toLowerCase().trim();

		// System.out.println("carName...."+ carName);
		// System.out.println("filter place...."+ placeFilter);

		if (carName.startsWith(placeFilter.toLowerCase())
				|| placeFilter.equals("")) {
			applyFilterOperation();
			return true;
		}

		return false;
	}

	public boolean filterCU(Object value, Object filter, Locale locale) {
		String filterText = (filter == null) ? null : filter.toString().trim();
		if (filterText == null) {
			return true;
		}

		if (value == null) {
			return false;
		}

		cuFilter = filterText.toLowerCase();
		String carName = value.toString().toLowerCase();

		// System.out.println("carName...."+ carName);

		totalCompletedTransacMoney = 0;
		numberOfCompletedTransacMoney = 0;
		totalRejectedTransacMoney = 0;
		numberOfRejecttedTransac = 0;
		if (carName.startsWith(cuFilter.toLowerCase()) || cuFilter.equals("")) {
			applyFilterOperation();
			return true;
		}

		return false;
	}

	public boolean filterTransac(Object value, Object filter, Locale locale) {
		String filterText = (filter == null) ? null : filter.toString().trim();
		if (filterText == null) {
			return true;
		}

		if (value == null) {
			return false;
		}

		transactFilter = filterText.toLowerCase();
		String carName = value.toString().toLowerCase();

		// System.out.println("carName...."+ carName);

		totalCompletedTransacMoney = 0;
		numberOfCompletedTransacMoney = 0;
		totalRejectedTransacMoney = 0;
		numberOfRejecttedTransac = 0;
		if (carName.startsWith(transactFilter.toLowerCase())
				|| transactFilter.equals("")) {
			applyFilterOperation();
			return true;

		}

		return false;
	}

	private void applyFilterOperation() {

		totalCompletedTransacMoney = 0;
		numberOfCompletedTransacMoney = 0;
		totalRejectedTransacMoney = 0;
		numberOfRejecttedTransac = 0;

		for (CityParkingTransactionUser element : listReportCitySales) {

			// System.out.println("zone...."+ element.getIdZone());

			if ((element.getEmailSysUser().startsWith(emailFilter) || emailFilter
					.equals(""))
					&& (element.getSysUser().getCu().startsWith(cuFilter) || cuFilter
							.equals(""))
					&& (element.getIdTransaction().startsWith(transactFilter) || transactFilter
							.equals(""))
					&& (placeFilter.equals("") || (element.getIdZone() != null && element
							.getIdZone().toLowerCase().startsWith(placeFilter)))) {

				// System.out.println("filtering.... startwith"+
				// element.getStateTransaction());

				if (element.getStateTransaction().toLowerCase()
						.contains("complet")) {
					totalCompletedTransacMoney = totalCompletedTransacMoney
							+ element.getAmount();
					numberOfCompletedTransacMoney++;
				} else {
					totalRejectedTransacMoney = totalRejectedTransacMoney
							+ element.getAmount();
					numberOfRejecttedTransac++;
				}
			}
			// element.getSysUser().
			// element.setStateTransaction(getMessageFor("parameter.sales.state."+
			// element.getStateTransaction().toLowerCase()));

		}

		// System.out.println("complete transaction"+
		// totalCompletedTransacMoney);

		// setSales(listReportCitySales != null &&
		// !listReportCitySales.isEmpty() );
		// RequestContext.getCurrentInstance().update("contact-form:tbl:ftr");

		RequestContext.getCurrentInstance().addCallbackParam(
				"totalreloadcompletedamount",
				getMessageFor("myHistory.totalreloadcomplentedamount") + " : "
						+ totalCompletedTransacMoney);
		RequestContext.getCurrentInstance().addCallbackParam(
				"numberOfCompletedTransacMoney",
				getMessageFor("myHistory.numberofcompletedtransac") + " : "
						+ numberOfCompletedTransacMoney);

		RequestContext.getCurrentInstance().addCallbackParam(
				"totalRejectedTransacMoney",
				getMessageFor("myHistory.totalreloadrejectedamount") + " : "
						+ totalRejectedTransacMoney);
		RequestContext.getCurrentInstance().addCallbackParam(
				"numberOfRejecttedTransac",
				getMessageFor("myHistory.numberofrejectedtransac") + " : "
						+ numberOfRejecttedTransac);
		// System.out.println("numberOfRejecttedTransac="+
		// getMessageFor("myHistory.numberofrejectedtransac")+" : "+numberOfRejecttedTransac);

	}

	public void generarInformeCity() {
		try {

			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			HttpServletResponse response = (HttpServletResponse) externalContext
					.getResponse();

			String formato = (String) externalContext.getRequestParameterMap()
					.get("formato");

			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");

			String filename = "reportSales_"
					+ formatter.format(new java.util.Date());
			if (useremail.equals("")) {
				useremail = "%";
			}

			HashMap<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("fechaIni", fechaI + " 00:01:01");
			parameter.put("fechaFin", fechaF + " 23:59:59");
			// parameter.put("userId", sysUser.getIdsysUser());
			parameter.put("userEmail", useremail);

			if ("ZP".equals(paymentMethod)) {

				paymentMethod = " and idzone is null";
			} else if ("PK".equals(paymentMethod)) {
				paymentMethod = " and idzone is not null";
			} else {
				paymentMethod = "";
			}
			parameter.put("descriptionMethod", paymentMethod);

			JasperReportHelper jrh = new JasperReportHelper("reportCitySales",
					parameter);
			JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS
					: JasperReportHelper.formatos.PDF;

			byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

			OutputStream os = response.getOutputStream();

			response.setContentType("application/" + formato); // fill in

			response.setContentLength(pdfStreamByteArray.length);

			response.setHeader("Content-disposition", "attachment; filename=\""
					+ filename + "." + formato + "\"");

			os.write(pdfStreamByteArray); // fill in bytes

			os.flush();
			os.close();
			facesContext.responseComplete();
		} catch (Exception e) {
			LOGGER.error("No se pudo realizar el informe : ", e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	private String getMessageFor(String key) {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = context.getApplication().getResourceBundle(
					context, "msgs");
			String message = bundle.getString(key);
			return message;
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 
	 * @param idForma
	 * @return
	 */
	private String getDescripcionFormaPago(String idForma) {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = context.getApplication().getResourceBundle(
					context, "msgs");
			String message = bundle.getString("parameter.sales.method."
					+ idForma + ".description");
			return message;
		} catch (Exception e) {
			return "";
		}
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public void setTotalMoney(double totalMoney) {
		this.totalMoney = totalMoney;
	}

	public double getTotalMoney() {
		return totalMoney;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isAdmin() {
		return admin;
	}

	public boolean isSales() {
		return sales;
	}

	public void setSales(boolean sales) {
		this.sales = sales;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public List<PayPalTransaction> getListReportSales() {
		return listReportSales;
	}

	public void setListReportSales(List<PayPalTransaction> listReportSales) {
		this.listReportSales = listReportSales;
	}

	public String getUseremail() {
		return useremail;
	}

	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	public List<CityParkingTransactionUser> getListReportCitySales() {
		return listReportCitySales;
	}

	public void setListReportCitySales(
			List<CityParkingTransactionUser> listReportCitySales) {
		this.listReportCitySales = listReportCitySales;
	}

	public String getFullUsername() {
		return fullUsername;
	}

	public void setFullUsername(String fullUsername) {
		this.fullUsername = fullUsername;
	}

	public double getTotalRejectedTransacMoney() {
		return totalRejectedTransacMoney;
	}

	public void setTotalRejectedTransacMoney(double totalRejectedTransacMoney) {
		this.totalRejectedTransacMoney = totalRejectedTransacMoney;
	}

	public int getNumberOfCompletedTransacMoney() {
		return numberOfCompletedTransacMoney;
	}

	public void setNumberOfCompletedTransacMoney(int numberOfTransacMoney) {
		this.numberOfCompletedTransacMoney = numberOfTransacMoney;
	}

	public int getNumberOfRejecttedTransac() {
		return numberOfRejecttedTransac;
	}

	public void setNumberOfRejecttedTransac(int numberOfRejecttedTransac) {
		this.numberOfRejecttedTransac = numberOfRejecttedTransac;
	}

	public double getTotalCompletedTransacMoney() {
		return totalCompletedTransacMoney;
	}

	public void setTotalCompletedTransacMoney(double totalCompletedTransacMoney) {
		this.totalCompletedTransacMoney = totalCompletedTransacMoney;
	}

}
