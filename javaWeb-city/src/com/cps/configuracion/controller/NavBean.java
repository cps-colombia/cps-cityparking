package com.cps.configuracion.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class NavBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String page = "reportsHome";

	public void setPage(String page) {
		this.page = page;
	}

	public String getPage() {
		return page;
	}
	
	public void pageAction(){
		
	}
	
	
}
