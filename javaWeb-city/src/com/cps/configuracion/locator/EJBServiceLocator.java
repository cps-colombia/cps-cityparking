package com.cps.configuracion.locator;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import com.cps.remote.SysUserBeanRemote;

/**
 * Provee un mecanismo unico para el acceso a los ejb.
 * 
 * @author Jorge
 * 
 */
public class EJBServiceLocator {

	private Logger LOGGER = Logger.getLogger(EJBServiceLocator.class);
	private static EJBServiceLocator ejbLocator = new EJBServiceLocator();
	private static Context ctx;
	private Properties jndiProperties;

	private EJBServiceLocator() {
	}

	public static EJBServiceLocator getInstace() {
		return ejbLocator;
	}

	/**
	 * 
	 * @param ejbName
	 * @return Object
	 * @throws Exception
	 */
	public Object getEjbService(String ejbName) throws Exception {
		if (ctx == null) {
			jndiProperties = new Properties();
			try {
				ResourceBundle bundleCps = ResourceBundle.getBundle("cps");
				jndiProperties.load(new FileInputStream(new File(
						bundleCps.getString("cps.jndiPath.properties"))));
			} catch (Exception e) {
				LOGGER.error("No se pudo cargar la configuracion jndi para EJB", e);
			}
			ctx = new InitialContext(jndiProperties);
		}
		return ctx.lookup(ejbName);
	}

	/**
	 * Retorna un ejb identificado con el mismo nombre de la clase pasado como
	 * argumento.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <X> X getEjbService(Class<X> clas) throws Exception {
		if (ctx == null) {
			jndiProperties = new Properties();
			/*try {
				ResourceBundle bundleCps = ResourceBundle.getBundle("cps");
				jndiProperties.load(new FileInputStream(new File(
						bundleCps.getString("cps.jndiPath.properties"))));
				
				jndiProperties.put("jboss.naming.client.ejb.context", true);
				jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
			} catch (Exception e) {
				LOGGER.error("No se pudo cargar la configuracion jndi para EJB", e);
			}*/
			
			   jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		      // jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		      jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:18080");
			ctx = new InitialContext(jndiProperties);
		}
		//return (X) ctx.lookup(clas.getSimpleName().concat("/remote"));
	
		//ctx.close();
		return (X) ctx.lookup("ejb:/COL-CPS-EJB/".concat(clas.getSimpleName()+"!"+clas.getName()));

	}

}
