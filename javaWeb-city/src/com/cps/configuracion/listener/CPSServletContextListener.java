package com.cps.configuracion.listener;

import java.util.ResourceBundle;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class CPSServletContextListener extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger LOGGER = Logger
			.getLogger(CPSServletContextListener.class);

	/**
	 * inicializa el LOGGER de la aplicacion.<br>
	 * Depende del init-parameter <code>log4j.propertiesFile</code> definido en
	 * web.xml, el cual debe indiar la ruta de un archivo .properties que debe
	 * estar localizado en WEB-INF/classes.<br>
	 * Este archivo debe contener el key <code>cps.logPath.properties</code> con
	 * el path absoluto del archivo de log4j de CPS.<br>
	 * Esta configuracion atiende el requerimiento de cps de tener los archivos
	 * de configuracion fuera de la estructura de directorios de la aplicacion.
	 * 
	 * @param servletContextEvent
	 */
	public void init(ServletConfig config) {
		/*
		 * Log4J initialization
		 */
		String log4jPropertiesFile = config
				.getInitParameter("log4j.propertiesFile");
		String log4jFile = null;

		try {
			ResourceBundle bundle = ResourceBundle
					.getBundle(log4jPropertiesFile);
			log4jFile = bundle.getString("cps.logPath.properties");

			PropertyConfigurator.configure(log4jFile);
			LOGGER.info("*** Log4j inicializado desde archivo " + log4jFile);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("*** Log4j no fue inicializado desde archivo "
					+ log4jFile, e);
		}
		try {
			ResourceBundle bundleCps = ResourceBundle.getBundle("cps");
			LOGGER.info("*** Ruta de configuracion de Paypal "
					+ bundleCps.getString("cps.payPalPath.properties"));
			LOGGER.info("*** Ruta de configuracion jndi para EJB "
					+ bundleCps.getString("cps.jndiPath.properties"));
		} catch (Exception e) {
			LOGGER.error(
					"*** No fue posible encontrar propiedad de configuracion ",
					e);
		}
	}

}