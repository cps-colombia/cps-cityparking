package com.cps.configuracion.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.PayPalTransaction;
import com.cps.remote.PayPalBeanRemote;
import com.cps.util.PayPalHelper;

/**
 * Servlet implementation class IPNServletListener
 */
public class IPNServletListener extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(IPNServletListener.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IPNServletListener() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// read post from PayPal system and add 'cmd'
		String urlPaypal = PayPalHelper.getValue("CONFIRM_NOTIFY");
		
		LOGGER.info("Notificacion de Pago Recibida");
		Enumeration<String> en = request.getParameterNames();
		String str = "cmd=_notify-validate";
		while (en.hasMoreElements()) {
			String paramName = (String) en.nextElement();
			String paramValue = request.getParameter(paramName);
			str = str + "&" + paramName + "=" + URLEncoder.encode(paramValue);
		}

		// post back to PayPal system to validate
		// NOTE: change http: to https: in the following URL to verify using SSL
		// (for increased security).
		// using HTTPS requires either Java 1.4 or greater, or Java Secure
		// Socket Extension (JSSE)
		// and configured for older versions.
		URL u = new URL(urlPaypal);
		URLConnection uc = u.openConnection();
		uc.setDoOutput(true);
		uc.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");
		PrintWriter pw = new PrintWriter(uc.getOutputStream());
		pw.println(str);
		pw.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(
				uc.getInputStream()));
		String res = in.readLine();
		in.close();

		// assign posted variables to local variables
		String itemName = request.getParameter("item_name");
		String itemNumber = request.getParameter("item_number");
		String paymentStatus = request.getParameter("payment_status");
		String paymentAmount = request.getParameter("mc_gross");
		String paymentCurrency = request.getParameter("mc_currency");
		String txnId = request.getParameter("txn_id");
		String receiverEmail = request.getParameter("receiver_email");
		String payerEmail = request.getParameter("payer_email");
		String custom = request.getParameter("custom");
		LOGGER.info("Custom :"+custom);
		LOGGER.info("paymentStatus :"+paymentStatus);
		LOGGER.info("paymentAmount :"+paymentAmount);
		LOGGER.info("res :"+res);
		// check notification validation
		if (res.equals("VERIFIED")) {
			if(paymentStatus.equals("Completed")){
				try {
					PayPalBeanRemote payPalBeanRemote = EJBServiceLocator.getInstace().getEjbService(PayPalBeanRemote.class);
					if(payPalBeanRemote.getTransaccionById(txnId) == null){
					LOGGER.info("Voy a registrar Transaction :"+txnId);
					PayPalTransaction payPalTransaction = new PayPalTransaction();
					payPalTransaction.setIdTransaction(txnId);
					payPalTransaction.setAmount(Double.parseDouble(paymentAmount));
					payPalTransaction.setCurrency(paymentCurrency);
					payPalTransaction.setDateTransaction(new java.util.Date());
					payPalTransaction.setEmailSysUser(payerEmail);
					payPalTransaction.setIdSysUser(custom);
					payPalTransaction.setStateTransaction(paymentStatus);
					payPalBeanRemote.registrarTransaccion(payPalTransaction);
					LOGGER.info("Transaction :"+txnId+" Registrada");
					}else{
					LOGGER.info("Transaction :"+txnId+" ya fue Registrada");	
					}
					
				} catch (Exception e) {
                    LOGGER.error("No se pudo obtener ejb PayPalBeanRemote", e);
				}
			// check that paymentStatus=Completed
			// check that txnId has not been previously processed
			// check that receiverEmail is your Primary PayPal email
			// check that paymentAmount/paymentCurrency are correct
			// process payment
			}
		} else if (res.equals("INVALID")) {
			LOGGER.info("Transaction :"+txnId+" no fue valida");
		} else {
			LOGGER.info("Respuesta de validacion de pago no valida :"+res);
		}

	}

}
