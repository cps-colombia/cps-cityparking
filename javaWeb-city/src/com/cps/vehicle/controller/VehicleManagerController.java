/**
 * 
 */
package com.cps.vehicle.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.ActiveParking;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.Vehicle;
import com.cps.remote.ParkingBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.remote.VehicleBeanRemote;
import com.cps.util.CodeResponse;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 * 
 */
@ManagedBean
@ViewScoped
public class VehicleManagerController implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private VehicleBeanRemote vehicleBeanRemote;
	private SysUserBeanRemote sysUserBeanRemote;
	private ParkingBeanRemote parkingbeBeanRemote;
	private List<Vehicle> listVehicle;
	private Vehicle vehicle;
	private String user;

	public VehicleManagerController() {
		try
		{
			vehicle = new Vehicle();
			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
			listVehicle = vehicleBeanRemote.getVehicles( JsfUtil.getUserSession() );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
	}

	public void registerVehicle( ActionEvent event )
	{
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try
		{

			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
			int resp = vehicleBeanRemote.registerVehicle( vehicle.getPlate(), JsfUtil.getUserSession() );
			if ( resp == CodeResponse.REGISTER_VEHICLE_OK )
			{
				JsfUtil.addMessage( "myVehicles.registered", FacesMessage.SEVERITY_INFO );
				listVehicle = vehicleBeanRemote.getVehicles( JsfUtil.getUserSession() );
				flag = true;
			}
			else
			{
				JsfUtil.addMessage( "myVehicles.alreadyRegistered", FacesMessage.SEVERITY_ERROR );
			}
		}
		catch ( Exception e )
		{
			JsfUtil.addMessage( "serviceDisabled", FacesMessage.SEVERITY_ERROR );
		}
		context.addCallbackParam( "flag", flag );

	}

	public void deleteVehicleUser( ActionEvent event )
	{
		String plate = (String) event.getComponent().getAttributes().get( "plate" );
		try
		{

			parkingbeBeanRemote = EJBServiceLocator.getInstace().getEjbService( ParkingBeanRemote.class );
			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
			if ( parkingbeBeanRemote.getActiveParkingVehicle( plate ) == null )
			{
				
				vehicleBeanRemote.deleteVehicle( plate, JsfUtil.getUserSession() );
				listVehicle = vehicleBeanRemote.getVehicles( JsfUtil.getUserSession() );
				
			}
			else
			{
				JsfUtil.addMessage( "myVehicles.RemoveError", FacesMessage.SEVERITY_ERROR );

			}
		/*	vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
			Vehicle vehicle = vehicleBeanRemote.getVehicle( plate, JsfUtil.getUserSession() );
			vehicle.setState( 0 );
			vehicleBeanRemote.updateVehicle( vehicle );
			listVehicle = vehicleBeanRemote.getVehicles( JsfUtil.getUserSession() );*/
		}
		catch ( Exception e )
		{
			JsfUtil.addMessage( "serviceDisabled", FacesMessage.SEVERITY_ERROR );
			e.printStackTrace();
		}

	}

	public void shareVehicle( ActionEvent event )
	{
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try
		{
			vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );
			SysUser sysUser = sysUserBeanRemote.getUser( user );
			int resp = vehicleBeanRemote.shareVehicle( sysUser, vehicle.getPlate() );
			if ( resp == CodeResponse.VEHICLE_SHARED_SUCCESSFULLY )
			{
				JsfUtil.addMessage( "myVehicles.shared", FacesMessage.SEVERITY_INFO );
				flag = true;
			}
			else
			{
				JsfUtil.addMessage( "myVehicles.alreadyShared", FacesMessage.SEVERITY_ERROR );
			}
		}
		catch ( Exception e )
		{
			JsfUtil.addMessage( "serviceDisabled", FacesMessage.SEVERITY_ERROR );
		}
		context.addCallbackParam( "flag", flag );
	}

	public List<Vehicle> getListVehicle()
	{
		return listVehicle;
	}

	public void setListVehicle( List<Vehicle> listVehicle )
	{
		this.listVehicle = listVehicle;
	}

	public Vehicle getVehicle()
	{
		return vehicle;
	}

	public void setVehicle( Vehicle vehicle )
	{
		this.vehicle = vehicle;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser( String user )
	{
		this.user = user;
	}

}
