
package com.cps.webservice.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pDireccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pCiudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pPreguntaSeguridad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pRespuestaSeguridad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pCU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pNombre",
    "pApellido",
    "pDireccion",
    "pEmail",
    "pTelefono",
    "pPais",
    "pEstado",
    "pCiudad",
    "pZip",
    "pUserName",
    "pPassword",
    "pPreguntaSeguridad",
    "pRespuestaSeguridad",
    "pcu"
})
@XmlRootElement(name = "agregarUsuario")
public class AgregarUsuario {

    @XmlElementRef(name = "pNombre", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pNombre;
    @XmlElementRef(name = "pApellido", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pApellido;
    @XmlElementRef(name = "pDireccion", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pDireccion;
    @XmlElementRef(name = "pEmail", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pEmail;
    @XmlElementRef(name = "pTelefono", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pTelefono;
    @XmlElementRef(name = "pPais", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pPais;
    @XmlElementRef(name = "pEstado", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pEstado;
    @XmlElementRef(name = "pCiudad", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pCiudad;
    @XmlElementRef(name = "pZip", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pZip;
    @XmlElementRef(name = "pUserName", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pUserName;
    @XmlElementRef(name = "pPassword", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pPassword;
    @XmlElementRef(name = "pPreguntaSeguridad", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pPreguntaSeguridad;
    @XmlElementRef(name = "pRespuestaSeguridad", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pRespuestaSeguridad;
    @XmlElementRef(name = "pCU", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pcu;

    /**
     * Obtiene el valor de la propiedad pNombre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPNombre() {
        return pNombre;
    }

    /**
     * Define el valor de la propiedad pNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPNombre(JAXBElement<String> value) {
        this.pNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad pApellido.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPApellido() {
        return pApellido;
    }

    /**
     * Define el valor de la propiedad pApellido.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPApellido(JAXBElement<String> value) {
        this.pApellido = value;
    }

    /**
     * Obtiene el valor de la propiedad pDireccion.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPDireccion() {
        return pDireccion;
    }

    /**
     * Define el valor de la propiedad pDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPDireccion(JAXBElement<String> value) {
        this.pDireccion = value;
    }

    /**
     * Obtiene el valor de la propiedad pEmail.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPEmail() {
        return pEmail;
    }

    /**
     * Define el valor de la propiedad pEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPEmail(JAXBElement<String> value) {
        this.pEmail = value;
    }

    /**
     * Obtiene el valor de la propiedad pTelefono.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPTelefono() {
        return pTelefono;
    }

    /**
     * Define el valor de la propiedad pTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPTelefono(JAXBElement<String> value) {
        this.pTelefono = value;
    }

    /**
     * Obtiene el valor de la propiedad pPais.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPPais() {
        return pPais;
    }

    /**
     * Define el valor de la propiedad pPais.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPPais(JAXBElement<String> value) {
        this.pPais = value;
    }

    /**
     * Obtiene el valor de la propiedad pEstado.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPEstado() {
        return pEstado;
    }

    /**
     * Define el valor de la propiedad pEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPEstado(JAXBElement<String> value) {
        this.pEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad pCiudad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPCiudad() {
        return pCiudad;
    }

    /**
     * Define el valor de la propiedad pCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPCiudad(JAXBElement<String> value) {
        this.pCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad pZip.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPZip() {
        return pZip;
    }

    /**
     * Define el valor de la propiedad pZip.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPZip(JAXBElement<String> value) {
        this.pZip = value;
    }

    /**
     * Obtiene el valor de la propiedad pUserName.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPUserName() {
        return pUserName;
    }

    /**
     * Define el valor de la propiedad pUserName.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPUserName(JAXBElement<String> value) {
        this.pUserName = value;
    }

    /**
     * Obtiene el valor de la propiedad pPassword.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPPassword() {
        return pPassword;
    }

    /**
     * Define el valor de la propiedad pPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPPassword(JAXBElement<String> value) {
        this.pPassword = value;
    }

    /**
     * Obtiene el valor de la propiedad pPreguntaSeguridad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPPreguntaSeguridad() {
        return pPreguntaSeguridad;
    }

    /**
     * Define el valor de la propiedad pPreguntaSeguridad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPPreguntaSeguridad(JAXBElement<String> value) {
        this.pPreguntaSeguridad = value;
    }

    /**
     * Obtiene el valor de la propiedad pRespuestaSeguridad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPRespuestaSeguridad() {
        return pRespuestaSeguridad;
    }

    /**
     * Define el valor de la propiedad pRespuestaSeguridad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPRespuestaSeguridad(JAXBElement<String> value) {
        this.pRespuestaSeguridad = value;
    }

    /**
     * Obtiene el valor de la propiedad pcu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPCU() {
        return pcu;
    }

    /**
     * Define el valor de la propiedad pcu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPCU(JAXBElement<String> value) {
        this.pcu = value;
    }

}
