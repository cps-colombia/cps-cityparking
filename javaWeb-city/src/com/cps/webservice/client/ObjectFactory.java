
package com.cps.webservice.client;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cps.webservice.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _AgregarUsuarioPApellido_QNAME = new QName("www.msj.go.cr:8090", "pApellido");
    private final static QName _AgregarUsuarioPRespuestaSeguridad_QNAME = new QName("www.msj.go.cr:8090", "pRespuestaSeguridad");
    private final static QName _AgregarUsuarioPEmail_QNAME = new QName("www.msj.go.cr:8090", "pEmail");
    private final static QName _AgregarUsuarioPCU_QNAME = new QName("www.msj.go.cr:8090", "pCU");
    private final static QName _AgregarUsuarioPUserName_QNAME = new QName("www.msj.go.cr:8090", "pUserName");
    private final static QName _AgregarUsuarioPDireccion_QNAME = new QName("www.msj.go.cr:8090", "pDireccion");
    private final static QName _AgregarUsuarioPCiudad_QNAME = new QName("www.msj.go.cr:8090", "pCiudad");
    private final static QName _AgregarUsuarioPPreguntaSeguridad_QNAME = new QName("www.msj.go.cr:8090", "pPreguntaSeguridad");
    private final static QName _AgregarUsuarioPPassword_QNAME = new QName("www.msj.go.cr:8090", "pPassword");
    private final static QName _AgregarUsuarioPTelefono_QNAME = new QName("www.msj.go.cr:8090", "pTelefono");
    private final static QName _AgregarUsuarioPPais_QNAME = new QName("www.msj.go.cr:8090", "pPais");
    private final static QName _AgregarUsuarioPZip_QNAME = new QName("www.msj.go.cr:8090", "pZip");
    private final static QName _AgregarUsuarioPNombre_QNAME = new QName("www.msj.go.cr:8090", "pNombre");
    private final static QName _AgregarUsuarioPEstado_QNAME = new QName("www.msj.go.cr:8090", "pEstado");
    private final static QName _CambiarContraseñaUsuarioPClaveNueva_QNAME = new QName("www.msj.go.cr:8090", "pClaveNueva");
    private final static QName _CambiarContraseñaUsuarioPClaveVieja_QNAME = new QName("www.msj.go.cr:8090", "pClaveVieja");
    private final static QName _ExisteTelefonoPNumTelefono_QNAME = new QName("www.msj.go.cr:8090", "pNumTelefono");
    private final static QName _ExisteCorreoPCorreo_QNAME = new QName("www.msj.go.cr:8090", "pCorreo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cps.webservice.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExisteUserName }
     * 
     */
    public ExisteUserName createExisteUserName() {
        return new ExisteUserName();
    }

    /**
     * Create an instance of {@link CambiarContrasenaUsuarioResponse }
     * 
     */
    public CambiarContrasenaUsuarioResponse createCambiarContrasenaUsuarioResponse() {
        return new CambiarContrasenaUsuarioResponse();
    }

    /**
     * Create an instance of {@link ExisteCorreoResponse }
     * 
     */
    public ExisteCorreoResponse createExisteCorreoResponse() {
        return new ExisteCorreoResponse();
    }

    /**
     * Create an instance of {@link Preparado }
     * 
     */
    public Preparado createPreparado() {
        return new Preparado();
    }

    /**
     * Create an instance of {@link ExisteCorreo }
     * 
     */
    public ExisteCorreo createExisteCorreo() {
        return new ExisteCorreo();
    }

    /**
     * Create an instance of {@link CambiarContrasenaUsuario }
     * 
     */
    public CambiarContrasenaUsuario createCambiarContrasenaUsuario() {
        return new CambiarContrasenaUsuario();
    }

    /**
     * Create an instance of {@link ExisteUserNameResponse }
     * 
     */
    public ExisteUserNameResponse createExisteUserNameResponse() {
        return new ExisteUserNameResponse();
    }

    /**
     * Create an instance of {@link AgregarUsuarioResponse }
     * 
     */
    public AgregarUsuarioResponse createAgregarUsuarioResponse() {
        return new AgregarUsuarioResponse();
    }

    /**
     * Create an instance of {@link ExisteTelefonoResponse }
     * 
     */
    public ExisteTelefonoResponse createExisteTelefonoResponse() {
        return new ExisteTelefonoResponse();
    }

    /**
     * Create an instance of {@link AgregarUsuario }
     * 
     */
    public AgregarUsuario createAgregarUsuario() {
        return new AgregarUsuario();
    }

    /**
     * Create an instance of {@link ExisteTelefono }
     * 
     */
    public ExisteTelefono createExisteTelefono() {
        return new ExisteTelefono();
    }

    /**
     * Create an instance of {@link PreparadoResponse }
     * 
     */
    public PreparadoResponse createPreparadoResponse() {
        return new PreparadoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pApellido", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPApellido(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPApellido_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pRespuestaSeguridad", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPRespuestaSeguridad(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPRespuestaSeguridad_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pEmail", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPEmail(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPEmail_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pCU", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPCU(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPCU_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pUserName", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPUserName(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPUserName_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pDireccion", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPDireccion(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPDireccion_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pCiudad", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPCiudad(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPCiudad_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pPreguntaSeguridad", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPPreguntaSeguridad(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPPreguntaSeguridad_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pPassword", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPPassword(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPPassword_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pTelefono", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPTelefono(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPTelefono_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pPais", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPPais(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPPais_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pZip", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPZip(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPZip_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pNombre", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPNombre(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPNombre_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pEstado", scope = AgregarUsuario.class)
    public JAXBElement<String> createAgregarUsuarioPEstado(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPEstado_QNAME, String.class, AgregarUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pClaveNueva", scope = CambiarContrasenaUsuario.class)
    public JAXBElement<String> createCambiarContraseñaUsuarioPClaveNueva(String value) {
        return new JAXBElement<String>(_CambiarContraseñaUsuarioPClaveNueva_QNAME, String.class, CambiarContrasenaUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pClaveVieja", scope = CambiarContrasenaUsuario.class)
    public JAXBElement<String> createCambiarContraseñaUsuarioPClaveVieja(String value) {
        return new JAXBElement<String>(_CambiarContraseñaUsuarioPClaveVieja_QNAME, String.class, CambiarContrasenaUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pUserName", scope = CambiarContrasenaUsuario.class)
    public JAXBElement<String> createCambiarContraseñaUsuarioPUserName(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPUserName_QNAME, String.class, CambiarContrasenaUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pUserName", scope = ExisteUserName.class)
    public JAXBElement<String> createExisteUserNamePUserName(String value) {
        return new JAXBElement<String>(_AgregarUsuarioPUserName_QNAME, String.class, ExisteUserName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pNumTelefono", scope = ExisteTelefono.class)
    public JAXBElement<String> createExisteTelefonoPNumTelefono(String value) {
        return new JAXBElement<String>(_ExisteTelefonoPNumTelefono_QNAME, String.class, ExisteTelefono.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "www.msj.go.cr:8090", name = "pCorreo", scope = ExisteCorreo.class)
    public JAXBElement<String> createExisteCorreoPCorreo(String value) {
        return new JAXBElement<String>(_ExisteCorreoPCorreo_QNAME, String.class, ExisteCorreo.class, value);
    }

}
