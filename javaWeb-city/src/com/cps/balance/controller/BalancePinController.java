/**
 * 
 */
package com.cps.balance.controller;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.SysUser;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.CodeResponse;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 *
 */
@ManagedBean
@ViewScoped
public class BalancePinController {

	private SysUserBeanRemote sysUserBeanRemote;
	private SysUser sysUser;
	private String pin;
	private Date fechaIni;
	private Date fechaFin;
	
	
	public String reloadBalancePin(){
		try{
		sysUserBeanRemote = EJBServiceLocator.getInstace()
		.getEjbService(SysUserBeanRemote.class);
		
		sysUser = (SysUser) JsfUtil.getSessionAtribute("userSession");
		int response = sysUserBeanRemote.reloadBalance(sysUser, pin);
		if(response == CodeResponse.RELOAD_RESIDUE_OK){
			JsfUtil.addMessage("reloadBalanceOk",
					FacesMessage.SEVERITY_INFO);
			
		}else{
			JsfUtil.addMessage("reloadBalanceBad",
					FacesMessage.SEVERITY_ERROR);
		}
		
		}catch(Exception e){
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}

		return "";
	}
	
	public void generarInformeVentasTarjetas(){
        try{
		
		String fechaI = DateHelper.format(getFechaIni(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getFechaFin(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "totalSalesCards_"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		
		parameter.put("dateIni", fechaI + " 00:01:01");
		parameter.put("dateFin", fechaF + " 23:59:59");
		
		JasperReportHelper jrh = new JasperReportHelper("reportSalesCards", parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	
	

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaFin() {
		return fechaFin;
	}
	
	
	
}
