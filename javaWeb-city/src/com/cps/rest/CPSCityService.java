package com.cps.rest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.PayItem;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.wservice.payment.verification.ArrayOfPagosV3;

@Path("/")
public class CPSCityService {

	private CityParkingBeanRemote cityParkingBeanRemote;
	private Logger LOGGER = Logger.getLogger( CPSCityService.class );

	
	@GET()
	@Path("CPSCity.jsp")
	@Produces("text/plain")
	public String zonapagosget(@QueryParam("id_pago") long paymentId,
			@QueryParam("estado_pago") long paymentStatus,
			@QueryParam("id_forma_pago") long paymentType,
			@QueryParam("valor_pagado") double paymentValue,
			@QueryParam("ticketID") long ticketID,
			@QueryParam("id_clave") String keyId,
			@QueryParam("id_cliente") String clientId,
			@QueryParam("franquicia") String franchise,
			@QueryParam("codigo_servicio") long serviceCode,
			@QueryParam("codigo_banco") long bankCode,
			@QueryParam("nombre_banco") String bankName,
			@QueryParam("codigo_transaccion") long transactionCode,
			@QueryParam("ciclo_transaccion") long transactionCycle,
			@QueryParam("campo1") String field1,
			@QueryParam("campo2") String field2,
			@QueryParam("campo3") String field3,
			@QueryParam("idcomercio") int comerceID) {
		
		return zonapagos(paymentId, paymentStatus, paymentType, paymentValue, ticketID, keyId, clientId, franchise, serviceCode, bankCode, bankName, transactionCode, transactionCycle, field1, field2, field3, comerceID);
	}
	
	@POST()
	@Path("CPSCity.jsp")
	@Produces("text/plain")
	public String zonapagos(@QueryParam("id_pago") long paymentId,
			@QueryParam("estado_pago") long paymentStatus,
			@QueryParam("id_forma_pago") long paymentType,
			@QueryParam("valor_pagado") double paymentValue,
			@QueryParam("ticketID") long ticketID,
			@QueryParam("id_clave") String keyId,
			@QueryParam("id_cliente") String clientId,
			@QueryParam("franquicia") String franchise,
			@QueryParam("codigo_servicio") long serviceCode,
			@QueryParam("codigo_banco") long bankCode,
			@QueryParam("nombre_banco") String bankName,
			@QueryParam("codigo_transaccion") long transactionCode,
			@QueryParam("ciclo_transaccion") long transactionCycle,
			@QueryParam("campo1") String field1,
			@QueryParam("campo2") String field2,
			@QueryParam("campo3") String field3,
			@QueryParam("idcomercio") int comerceID) {
		
		/*"&campo1="+field1+
		"&campo2="+field2+"&campo3"+field3+*/
		/*URL url;
		HttpURLConnection conn=null;
		
		try {
			franchise=((franchise!=null)?URLEncoder.encode(franchise,"UTF-8"):"");
			bankName=((bankName!=null)?URLEncoder.encode(bankName,"UTF-8"):"");
			String urlString= "http://181.131.128.141:8082/CAN-CPS-WEB/cps/CPSCity.jsp?id_pago="
		+paymentId+"&estado_pago="+paymentStatus+"&id_forma_pago="
					+paymentType+"&ticketID="+ticketID+"&id_clave="+keyId+
					"&id_cliente="+clientId+"&franquicia="+URLEncoder.encode(franchise,"UTF-8")+
					"&codigo_servicio="+serviceCode+"&codigo_banco="+bankCode+
					"&nombre_banco="+bankName+"&codigo_transaccion="+transactionCode+
					"&ciclo_transaccion="+transactionCycle+
					"&idcomercio="+comerceID;
			url = new URL(urlString);
			conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("GET");
			//conn.addRequestProperty("User-Agent", "Mozilla");
			conn.connect();
			return "code: " +conn.getResponseCode()+ " url: "+urlString;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally
		{
			
				
		}*/
		
		try {
			cityParkingBeanRemote = EJBServiceLocator.getInstace()
					.getEjbService(CityParkingBeanRemote.class);
			LOGGER.debug("request arrived payment_id=" +paymentId);
			
			
			
			PayItem item = cityParkingBeanRemote.checkPayment(String.valueOf(paymentId)	);
			ArrayOfPagosV3 array = item.getResPagosV3();
			String estado="Error, Transacción rechazada";
		if(item.getVerificarPagoV3Result()>0&&array!= null)
		{
			int payStatement = array.getPagosV3().get(0).getIntEstadoPago();
			String state= "Pending";
			if(payStatement==999)
			{
				state= "Pending to end";
				estado="Transacción pendiente por terminar";
			}else if(payStatement==888)
			{
				estado="Transacción pendiente por empezar";
				state= "Pending to begin";
			}else if(payStatement==1)
			{
				state= "Completed";
				estado="Transacción Completa";
			}else if(payStatement==0)
			{
				state= "Rejected";
				estado="Rechazada";
			}
			
			LOGGER.warn("Checking item with transaction number="+paymentId+" status=" + state);

			cityParkingBeanRemote.updateTransactionStatus(String.valueOf(paymentId), state);
		}else if(item.getVerificarPagoV3Result()==0&&item.getIntError()== 1)
		{
			cityParkingBeanRemote.updateTransactionStatus(String.valueOf(paymentId), "Rejected");
		}
			
		return estado;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Error en la transacción, consulte con el administrador.";//;paymentId + "|" + paymentStatus + "|" + paymentType;

	}
	
	
	
	@GET()
	@Path("CPSCity2.jsp")
	@Produces("text/plain")
	public String zonapagosgettest(@QueryParam("id_pago") long paymentId,
			@QueryParam("estado_pago") long paymentStatus,
			@QueryParam("id_forma_pago") long paymentType,
			@QueryParam("valor_pagado") double paymentValue,
			@QueryParam("ticketID") long ticketID,
			@QueryParam("id_clave") String keyId,
			@QueryParam("id_cliente") String clientId,
			@QueryParam("franquicia") String franchise,
			@QueryParam("codigo_servicio") long serviceCode,
			@QueryParam("codigo_banco") long bankCode,
			@QueryParam("nombre_banco") String bankName,
			@QueryParam("codigo_transaccion") long transactionCode,
			@QueryParam("ciclo_transaccion") long transactionCycle,
			@QueryParam("campo1") String field1,
			@QueryParam("campo2") String field2,
			@QueryParam("campo3") String field3,
			@QueryParam("idcomercio") int comerceID) {
		
		return "";
	}

	@GET()
	@Path("checkpayment")
	@Produces("text/plain")
	public String checkPaymentStatus(@QueryParam("payId") String id) {

		try {
			cityParkingBeanRemote = EJBServiceLocator.getInstace()
					.getEjbService(CityParkingBeanRemote.class);
			CityParkingTransaction transaction = cityParkingBeanRemote
					.checkTransaction(id);
			
			
			String currentState = transaction.getStateTransaction();
			int checks=0;
			while ("Started".contains(currentState)&&checks<119) {
				Thread.sleep(1000);
			
				transaction = cityParkingBeanRemote
						.checkTransaction(id);
				currentState = transaction.getStateTransaction();
				checks++;
				System.out.println("Id transaction="+ id+ " current status="+currentState);
			}

			return currentState;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

}
