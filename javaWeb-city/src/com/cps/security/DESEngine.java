package com.cps.security;
import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class DESEngine {
    Cipher ecipher;
    Cipher dcipher;

    DESEngine(SecretKey key) {
        try {
        	
            ecipher = Cipher.getInstance("DES");
            dcipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);

        } catch (javax.crypto.NoSuchPaddingException e) {
        } catch (java.security.NoSuchAlgorithmException e) {
        } catch (java.security.InvalidKeyException e) {
        }
    }

    public String encrypt(String str) {
        try {
            byte[] utf8 = str.getBytes("UTF8");
            byte[] enc = ecipher.doFinal(utf8);
            return Base64.encodeBase64String(enc);
        } catch (javax.crypto.BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {}
        return null;
    }

    public String decrypt(String str) {
        try {
            byte[] dec = Base64.decodeBase64(str);
            byte[] utf8 = dcipher.doFinal(dec);
            return new String(utf8, "UTF8");
        } catch (javax.crypto.BadPaddingException e) {
        	e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
        	e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
        	e.printStackTrace();
        }
        return null;
    }
    
    public static void main(String[] args) {
		try{
			byte[] keyB = Base64.decodeBase64("ueZ82h9h7N8=");
			SecretKey key = new SecretKeySpec(keyB, "DES");
			DESEngine dec = new DESEngine(key);
			//.decrypt("NAmKZPSOv8EuQnwmiGTx3LrudswsmROlt/jpPCKnjgg=")
			//NAmKZPSOv8EuQnwmiGTx3LrudswsmROlt/jpPCKnjgg5hx9zgQv1+Q==
			System.out.println( dec.encrypt("a") );
//			System.out.println(dec);
			System.out.println( 
					dec.decrypt("mr3uRtcXbPzgiGKmJQOXKA==")
					);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
    
}