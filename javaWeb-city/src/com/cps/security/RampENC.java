package com.cps.security;
import java.util.ArrayList;
import java.util.Date;


public class RampENC {

	private static long clave;
	
	private static String separators = "AEIOU";
	
	private static String alpha = "BCDFGHJKLMNPQRSTVWXYZ";
	
	private static String mask(String pr){
		String mask = "";
		for( int j=0;j<pr.length();j++ ){
			String next = String.valueOf(pr.charAt(j));
			if( j % 2 == 0 ){
				next = String.valueOf( alpha.charAt( Integer.parseInt( String.valueOf(pr.charAt(j)) ) ) );
				int rand = (int)(Math.random()*separators.length());
				if( rand % 2 == 0 ){
					next = next.toLowerCase();
				}
			}
			mask += next; 
		}
		return mask;
	}
	
	private static String unmask(String pr){
		String unmask = "";
		for( int j=0;j<pr.length();j++ ){
			String next = String.valueOf(pr.charAt(j));
			if( j % 2 == 0 ){
				next = String.valueOf(alpha.indexOf(String.valueOf(pr.charAt(j))));
			}
			unmask += ( !next.equals("-1") ? next :  String.valueOf(pr.charAt(j)) );
		}
		return unmask;
	}
	
	public static String encrypt(String data){				
		clave = new Date().getTime();
		String time = reverse(String.valueOf(clave))+
				String.valueOf(separators.charAt( (int)(Math.random()*separators.length())));				
		String encrypted = "";				
		encrypted = mask(time);		
		byte[] b = data.getBytes();		
		for( int i=0;i<b.length;i++ ){			
			long rk = b[i] * clave;			
			String pr = String.valueOf(rk);			
			encrypted += mask(pr);			 
			encrypted += String.valueOf(separators.charAt( (int)(Math.random()*separators.length())));
		}
		
		return encrypted;
		
	}
	
	public static String reverse(String data){
		String reverse = "";
		for( int i=data.length()-1;i>=0;i-- ){
			reverse += data.charAt(i); 
		}
		return reverse;
	}
	
	public static String decrypt(String data){
		return decrypt(data, 0);
	}	
	
	public static String decrypt(String data, int mins){
		long milis = mins*60*1000;
		data = data.toUpperCase();
		String time = reverse(unmask(data.substring(0, 13)));			
		long l = Long.parseLong(time);
		long now = new Date().getTime();
		if( now > l+milis && mins > 0 ){
			return null;
		}
		data = data.substring(13);		
		ArrayList<String> parts = new ArrayList<String>();
		int i=0;		
		String fl = "";		
		while( i < data.length() ){			
			int ind = separators.indexOf(String.valueOf(data.charAt(i)));					
			if( ind < 0 ){
				String unm = unmask(String.valueOf(data.charAt(i)) );									
				fl += unm;
			}
			else if( ind >= 0 && i > 0 ){
				parts.add(fl);
				fl = "";
			}
			i++;
		}		
		byte[] b = new byte[parts.size()];
		for( i=0;i<b.length;i++ ){			
			b[i] = Byte.parseByte( String.valueOf((Long.parseLong(parts.get(i))/l)) );			
		}						
		return new String(b);		
	}
	
}
