/**
 *
 */
package com.cps.cuenta.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.City;
import com.cps.entity.bean.SysUser;
import com.cps.remote.CountryBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.remote.VehicleBeanRemote;
import com.cps.util.JsfUtil;
import com.cps.util.MailSender;
import com.cps.util.Util;
import com.cps.webservice.client.IServicioPagoClient;

/**
 * @author Jorge
 *
 */

@ManagedBean
@ViewScoped
public class AdminUserController implements Serializable
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger( AdminUserController.class );
	private SysUserBeanRemote beanRemote;
	private CountryBeanRemote countryBeanRemote;
	private VehicleBeanRemote vehicleBeanRemote;
	private HtmlOutputText loginMessageBinding = new HtmlOutputText();
	private List<City> cityes = new ArrayList<City>();
	private String city = "";
	private String plate = "";
    private boolean emailNotification= true;
	private SysUser sysUser;

	/**
	 *
	 * @return
	 */
	public AdminUserController() {
		sysUser = JsfUtil.getUserSession() != null ? JsfUtil.getUserSession() : new SysUser();
		try
		{
			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService( CountryBeanRemote.class );
			cityes = countryBeanRemote.getCities( sysUser.getCountryIdcountry() );
		}
		catch ( Exception e )
		{
			LOGGER.error( "", e );
		}

	}

	/**
	 * Realiza el registro del Usuario
	 *
	 * @return
	 */
	public String registerUser()
	{
		String nextPage = "";
		try
		{

			beanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );
			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService( CountryBeanRemote.class );
			SysUser sysUserExist = beanRemote.getUser( sysUser.getLogin() );
			if ( sysUserExist == null )
			{
				if ( beanRemote.getUserByEmail( sysUser.getEmail() ) == null )
				{
					if ( beanRemote.getUserByMobile( sysUser.getFavoriteMsisdn() ) == null )
					{

						/*boolean existUser = IServicioPagoClient.getInstance().getPortBinding()
								.existeUserName( sysUser.getLogin() );
						boolean existPhone = IServicioPagoClient.getInstance().getPortBinding()
								.existeTelefono( sysUser.getFavoriteMsisdn() );
						boolean existEmail =IServicioPagoClient.getInstance().getPortBinding()	.existeCorreo( sysUser.getEmail() );
						LOGGER.info( "existencia de :  usuario=" +existUser +", phone="+ existPhone+", email="+ existEmail );
						if ( existUser || existPhone || existEmail )
						{
							JsfUtil.addMessage( "registerSJMExist", FacesMessage.SEVERITY_WARN );

						}
						else
						{*/

							// boolean registerMSJ =
							// IServicioPagoClient.getInstance().getPortBinding().agregarUsuario(sysUser.getName(),
							// sysUser.getLastName(), sysUser.getAddress(),
							// sysUser.getEmail(), sysUser.getFavoriteMsisdn()
							// , "CR", "1399",
							// "COS", "10101", sysUser.getLogin(),
							// sysUser.getPass(), "Cual es el mejor sistema de parqueo?",
							// "CPS", sysUser.getCu()).booleanValue();

							sysUser.setBalance( new BigDecimal( 0 ) );
							sysUser.setIdsysUserType( 2 );
							sysUser.setCity( countryBeanRemote.getCity( city ) );

								sysUser.setEmailNotification((emailNotification)?1:0);

							beanRemote.registerUser( sysUser );

							SysUser sysUserNew = beanRemote.getUser( sysUser.getLogin() );

							vehicleBeanRemote = EJBServiceLocator.getInstace().getEjbService( VehicleBeanRemote.class );
							vehicleBeanRemote.registerVehicle( plate, sysUserNew );

							HttpSession session = JsfUtil.getRequest().getSession( true );
							session.setAttribute( "userSession", sysUserNew );
							nextPage = "index";

							JsfUtil.addMessage( "registerSucces", FacesMessage.SEVERITY_INFO );

							String subject = JsfUtil.getMessage( "registerMailSubject", FacesMessage.SEVERITY_INFO )
									.getSummary();
							String body = Util.getTemplateString("newsletter/emailWelcome.xhtml")
                                .replace( "{cps_login}", sysUser.getLogin())
                                .replace( "{cps_pass}", sysUser.getPass())
                                .replace( "{cps_cu}", sysUserNew.getCu());
							String sender = JsfUtil.getMessage( "registerMailFrom", FacesMessage.SEVERITY_INFO )
									.getSummary();
							MailSender.sendMail( subject, body, sender, sysUser.getEmail() );

							/*java.lang.String _agregarUsuario_pNombre = normalize(sysUserNew.getName());
							java.lang.String _agregarUsuario_pApellido =normalize( sysUserNew.getLastName());
							java.lang.String _agregarUsuario_pDireccion = normalize(sysUserNew.getAddress());
							java.lang.String _agregarUsuario_pEmail = normalize(sysUserNew.getEmail());
							java.lang.String _agregarUsuario_pTelefono = sysUserNew.getFavoriteMsisdn();
							java.lang.String _agregarUsuario_pPais = "CR";
							java.lang.String _agregarUsuario_pEstado = "1399";
							java.lang.String _agregarUsuario_pCiudad = "COS";
							java.lang.String _agregarUsuario_pZip = "10101";
							java.lang.String _agregarUsuario_pUserName = sysUserNew.getLogin();
							java.lang.String _agregarUsuario_pPassword = sysUser.getPass();
							java.lang.String _agregarUsuario_pPreguntaSeguridad = "Cual es el mejor sistema de parqueo?";
							java.lang.String _agregarUsuario_pRespuestaSeguridad = "CPS";
							java.lang.String _agregarUsuario_pCU = sysUserNew.getCu();
							boolean registerMSJ = IServicioPagoClient
									.getInstance()
									.getPortBinding()
									.agregarUsuario( _agregarUsuario_pNombre, _agregarUsuario_pApellido,
											_agregarUsuario_pDireccion, _agregarUsuario_pEmail,
											_agregarUsuario_pTelefono, _agregarUsuario_pPais, _agregarUsuario_pEstado,
											_agregarUsuario_pCiudad, _agregarUsuario_pZip, _agregarUsuario_pUserName,
											_agregarUsuario_pPassword, _agregarUsuario_pPreguntaSeguridad,
											_agregarUsuario_pRespuestaSeguridad, _agregarUsuario_pCU );

							if ( !registerMSJ )
							{
								LOGGER.error( "Ocurri� un error tratando de registrar el usuario en MSJ\n: Info de registro:  nombre: "+ _agregarUsuario_pNombre+ " apellido: "
										+_agregarUsuario_pApellido+"  direccion: "+ _agregarUsuario_pDireccion+ " email: "+ _agregarUsuario_pEmail +" tel: "+ _agregarUsuario_pTelefono
										 + " pais: "+ _agregarUsuario_pPais +" estado: " + 	 _agregarUsuario_pEstado	+ " ciudad: "+	 _agregarUsuario_pCiudad
										 +" zipcode: " + _agregarUsuario_pZip+ "nombre usario: "+ _agregarUsuario_pUserName+" password"+ _agregarUsuario_pPassword+"  preg sec: "
										 + _agregarUsuario_pPreguntaSeguridad+ " respuesta sec"+ _agregarUsuario_pRespuestaSeguridad+ " CU:"+ _agregarUsuario_pCU);

								sysUserNew.setNotificationMsj( 2 );

								// JsfUtil.addMessage("registerError",
								// FacesMessage.SEVERITY_ERROR);
							}else
							{
								sysUserNew.setNotificationMsj( 1 );
							}*/

							beanRemote.updateUser( sysUserNew );
						//}

					}
					else
					{

						JsfUtil.addMessage( "registerMobileExist", FacesMessage.SEVERITY_ERROR );

					}

				}
				else
				{

					JsfUtil.addMessage( "registerEmailExist", FacesMessage.SEVERITY_ERROR );

				}

			}
			else
			{
				JsfUtil.addMessage( "registerLoginExist", FacesMessage.SEVERITY_ERROR );
			}

		}
		catch ( Exception e )
		{
			LOGGER.error( "Error Registrando Usuario:", e );
			JsfUtil.addMessage( "registerError", FacesMessage.SEVERITY_ERROR );
		}
		return nextPage;
	}

	public String updateUser()
	{
		try
		{
			beanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );

			SysUser sysUserSession = JsfUtil.getUserSession();
			sysUserSession.setName( sysUser.getName() );
			sysUserSession.setLastName( sysUser.getLastName() );
			sysUserSession.setAddress( sysUser.getAddress() );
			sysUserSession.setBirthDate( sysUser.getBirthDate() );
			sysUserSession.setCountryIdcountry( sysUserSession.getCountryIdcountry() );
			sysUserSession.setEmail( sysUser.getEmail() );
			sysUserSession.setLogin( sysUser.getLogin() );
			sysUserSession.setFavoriteMsisdn( sysUser.getFavoriteMsisdn() );

			beanRemote.updateUserNoBalance( sysUserSession );
			JsfUtil.setSessionAtribute( "userSession", sysUser );
			JsfUtil.addMessage( "updateDataSucces", FacesMessage.SEVERITY_INFO );

		}
		catch ( Exception e )
		{
			LOGGER.error( "No fue posible actualizar informacion de usuario", e );
			JsfUtil.addMessage( "updateError", FacesMessage.SEVERITY_ERROR );
			e.printStackTrace();
		}
		return "";
	}

	public void validLogin( ActionEvent event )
	{
		try
		{
			if ( sysUser.getLogin() != null && sysUser.getLogin().length() > 3 )
			{
				beanRemote = EJBServiceLocator.getInstace().getEjbService( SysUserBeanRemote.class );
				if ( beanRemote.getUser( sysUser.getLogin() ) != null )
				{
					loginMessageBinding.setValue( JsfUtil
							.getMessage( "registerLoginExist", FacesMessage.SEVERITY_ERROR ).getSummary() );
					loginMessageBinding.setStyleClass( "link" );
				}
				else
				{
					loginMessageBinding.setValue( "" );
					loginMessageBinding.setStyleClass( "" );
				}
			}
		}
		catch ( Exception e )
		{
			LOGGER.error( "Error al validar login", e );
		}

	}

	public SysUser getSysUser()
	{
		return sysUser;
	}

	public void setSysUser( SysUser sysUser )
	{
		this.sysUser = sysUser;
	}

	public HtmlOutputText getLoginMessageBinding()
	{
		return loginMessageBinding;
	}

	public void setLoginMessageBinding( HtmlOutputText loginMessageBinding )
	{
		this.loginMessageBinding = loginMessageBinding;
	}

	public void changeCountry()
	{
		try
		{
			String countrySelect = String.valueOf( sysUser.getCountryIdcountry() );

			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService( CountryBeanRemote.class );
			cityes = countryBeanRemote.getCities( countrySelect );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

	}

	public List<City> getCityes()
	{
		return cityes;
	}

	public void setCityes( List<City> cityes )
	{
		this.cityes = cityes;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity( String city )
	{
		this.city = city;
	}

	public String getPlate()
	{
		return plate;
	}

	public void setPlate( String plate )
	{
		this.plate = plate;
	}

	public static String normalize(String input) {
	    // Cadena de caracteres original a sustituir.
	    String original = "��������������u�������������������";
	    // Cadena de caracteres ASCII que reemplazar�n los originales.
	    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
	    String output = input;
	    for (int i=0; i<original.length(); i++) {
	        // Reemplazamos los caracteres especiales.
	        output = output.replace(original.charAt(i), ascii.charAt(i));
	    }//for i
	    return output;
	}//

	public boolean isEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(boolean emailNotification) {
		this.emailNotification = emailNotification;
	}

}
