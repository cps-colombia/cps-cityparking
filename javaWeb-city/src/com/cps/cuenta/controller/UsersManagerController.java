/**
 * 
 */
package com.cps.cuenta.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.SysUser;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 *
 */
@ManagedBean
public class UsersManagerController {
	
	private SysUserBeanRemote sysUserBeanRemote;
	private List<SysUser> listUsers;
	private List<SysUser> listRequestUsers;
	private String user;
	
	
	public UsersManagerController(){
		try {
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(SysUserBeanRemote.class);
			setListRequestUsers(sysUserBeanRemote.getRequestAccountUsersList(JsfUtil.getUserSession()));
			setListUsers(sysUserBeanRemote.getAccountUsersList(JsfUtil.getUserSession()));
		} catch (Exception e) {
     		e.printStackTrace();
		}
	}

	public void registerUser(ActionEvent event){
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try{
		  sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(SysUserBeanRemote.class);
		  SysUser userAdd = sysUserBeanRemote.getUser(getUser());
		  if(userAdd != null && !JsfUtil.getUserSession().getLogin().equals(userAdd.getLogin())){
			  sysUserBeanRemote.addAccount(JsfUtil.getUserSession(),userAdd);  
			  setListUsers(sysUserBeanRemote.getAccountUsersList(JsfUtil.getUserSession()));
			  JsfUtil.addMessage("myUser.registered", FacesMessage.SEVERITY_INFO);
			  flag = true;
		  }else{
			  JsfUtil.addMessage("myUser.userNoExist", FacesMessage.SEVERITY_ERROR);
			  flag = false;
		  }
		}catch (Exception e) {
			  JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			  flag = false;
		}
		context.addCallbackParam("flag", flag);
	}
	
	
	public void acceptUser(ActionEvent event){
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		String login = (String) event.getComponent().getAttributes().get("login");
		
		try{
		  sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(SysUserBeanRemote.class);
		  SysUser userAdd = sysUserBeanRemote.getUser(login);
		    sysUserBeanRemote.acceptAccount(userAdd,JsfUtil.getUserSession());  
			  setListUsers(sysUserBeanRemote.getAccountUsersList(JsfUtil.getUserSession()));
			  setListRequestUsers(sysUserBeanRemote.getRequestAccountUsersList(JsfUtil.getUserSession()));
			  JsfUtil.addMessage("myUser.registered", FacesMessage.SEVERITY_INFO);
			  flag = true;
		  
		}catch (Exception e) {
			  e.printStackTrace();
			  JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			  flag = false;
		}
		context.addCallbackParam("flag", flag);
	}
	
	

	public void setListUsers(List<SysUser> listUsers) {
		this.listUsers = listUsers;
	}

	public List<SysUser> getListUsers() {
		return listUsers;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public void setListRequestUsers(List<SysUser> listRequestUsers) {
		this.listRequestUsers = listRequestUsers;
	}

	public List<SysUser> getListRequestUsers() {
		return listRequestUsers;
	}


}
