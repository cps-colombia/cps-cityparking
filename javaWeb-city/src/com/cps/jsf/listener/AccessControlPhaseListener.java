package com.cps.jsf.listener;

import static com.cps.jsf.listener.AccessControlPhaseListener.AccessLevel.LOGGED_IN;
import static com.cps.jsf.listener.AccessControlPhaseListener.AccessLevel.NONE;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import org.apache.log4j.Logger;

import com.cps.entity.bean.SysUser;
import com.cps.util.JsfUtil;
import com.cps.util.UrlFilter;

/**
 * Phase Listener that checks the viewId (URL) against a set of filters to
 * determine the required access level. If the correct level is not there then
 * redirect.
 * 
 * See {@link UrlFilter} for details on the url matching.
 * 
 * @author Chris Watts 2009
 * 
 */
public class AccessControlPhaseListener implements PhaseListener {
	/** Logger for this class */
	private static final Logger LOGGER = Logger
			.getLogger(AccessControlPhaseListener.class);

	/** */
	private static final long serialVersionUID = 1L;
	
	private final HashMap<AccessLevel, List<UrlFilter>> levelFilters = new HashMap<AccessLevel, List<UrlFilter>>();

	public enum AccessLevel {
		NONE, LOGGED_IN, USER_ACTIVE, ADMIN;
	}

	/**
    * 
    */
	public AccessControlPhaseListener() {
		LOGGER.info("Configurando accesos.");
		initLevels();
		requires(LOGGED_IN)
		.include("*")
		.exclude("/index.xhtml")
		.exclude("/downloads.xhtml")
		.exclude("/tower.xhtml")
		.include("/account/*")
		.exclude("/account/registerUser.xhtml")
		.include("/zones/*")
		.exclude("/zones/SearchZones.xhtml")
		.include("/security/*")
		.exclude("/security/forgotPass.xhtml")
		.exclude("/security/passwordRecovery.xhtml")	
		.exclude("/aboutUs.xhtml")
		.exclude("/services.xhtml")
		.exclude("/contacts.xhtml")
		.exclude("/moreInf.xhtml")
		.exclude("/solutions.xhtml")
		.exclude("/support.xhtml")
		.exclude("/clients.xhtml")
		.exclude("/images/*")
		.exclude("/js/*");
		
		/*requires(USER_ACTIVE).include("/account/*").exclude(
				"/account/registerUser.xhtml");
				

		requires(ADMIN).include("/admin/*");
		*/
	}

	private void initLevels() {
		AccessLevel[] levels = AccessLevel.values();
		for (int i = 1; i < levels.length; i++) {
			levelFilters.put(levels[i], new ArrayList<UrlFilter>());
		}
	}

	private UrlFilter requires(AccessLevel level) {
		// ALL is default
		if (level == NONE)
			return null;

		UrlFilter filter = new UrlFilter();
		List<UrlFilter> list = levelFilters.get(level);
		list.add(filter);
		return filter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
	 */
	public void afterPhase(PhaseEvent event) {
		try {
			// check have correct access
			FacesContext context = event.getFacesContext();
			SysUser sessionBean = (SysUser) JsfUtil.getUserSession();

			// can't use this here. only valid at render response phase?
			String viewId = context.getViewRoot().getViewId();
			AccessLevel required = requiredLevel(viewId);
			//LOGGER.info("Required level={" + required + "} for viewId={" + viewId					+ "}");

			// check if page require access:
			switch (required) {
			case NONE:
				break;
			case LOGGED_IN:
				if (sessionBean == null)
					redirectLogin(event.getFacesContext(), null);
				break;
				/*case USER_ACTIVE:
				if (sessionBean != null)
					redirectActive(event.getFacesContext());
				break;
			
			 * case ADMIN: if (!sessionBean.)
			 * redirectAdmin(event.getFacesContext()); break;
			 */
			default:
				// error
				LOGGER.error("huh?");
				throw new IllegalArgumentException("Not a valid access level");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("beforePhase caught exception", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
	 */
	public void beforePhase(PhaseEvent event) {

	}

	private void redirectLogin(FacesContext context, SysUser sessionForm) {
		// trigger login popup to be shown on render.
		// sessionForm.logIn();
		JsfUtil.addMessage("access.loginrequired" , FacesMessage.SEVERITY_ERROR);;
		context.getApplication().getNavigationHandler()
				.handleNavigation(context, null, "index");
	}

	/*
	private void redirectActive(FacesContext context) {
		addError(context, "access.activerequired");
		context.getApplication().getNavigationHandler()
				.handleNavigation(context, null, "userActivate");
	}

	private void redirectAdmin(FacesContext context) {
		addError(context, "access.adminrequired");
		context.getApplication().getNavigationHandler()
				.handleNavigation(context, null, "home");
	}
	*/


	/**
	 * Checks defined filters for view id, checks starting at the highest level
	 * down to NONE.
	 * 
	 * @return the matching level or {@link AccessLevel#NONE} if none matching.
	 */
	private AccessLevel requiredLevel(String viewId) {
		AccessLevel[] levels = AccessLevel.values();
		for (int i = levels.length - 1; i > 0; i--) {
			if (checkLevel(levels[i], viewId))
				return levels[i];
		}

		return AccessLevel.NONE;
	}

	private boolean checkLevel(AccessLevel level, String viewId) {
		return matchUri(levelFilters.get(level), viewId);
	}

	private boolean matchUri(List<UrlFilter> list, String uri) {
		for (UrlFilter filter : list) {
			if (filter.matches(uri))
				return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.event.PhaseListener#getPhaseId()
	 */
	public PhaseId getPhaseId() {
		// ALL access go through RESTORE_VIEW and RENDER_VIEW (even direct url)
		return PhaseId.RESTORE_VIEW;
	}

}