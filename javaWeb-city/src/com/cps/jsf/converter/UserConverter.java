package com.cps.jsf.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.cps.balance.controller.BalanceCurrentController;
import com.cps.entity.bean.SysUser;
import com.mysema.query.alias.Alias;
import com.mysema.query.collections.CollQueryFactory;

@FacesConverter(value = "userConverter")
public class UserConverter implements Converter {

	private SysUser trusr = Alias.alias(SysUser.class, "sysuser");

	@Override
	public Object getAsObject(FacesContext fc, UIComponent arg1, String value) {
		if (value != null && value.trim().length() > 0) {

			try {
				BalanceCurrentController balanceCurrentController = (BalanceCurrentController) fc
						.getViewRoot().getViewMap()
						.get("balanceCurrentController");
				Object ob = CollQueryFactory
						.from(Alias.$(trusr),
								balanceCurrentController.getOriginUsers())
						.where(Alias.$(trusr.getIdsysUser()).equalsIgnoreCase(
								value)).singleResult(Alias.$(trusr));

				return ob;
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a user."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object object) {
		if (object != null) {
			return String.valueOf(((SysUser) object).getIdsysUser());
		} else {
			return null;
		}
	}

}
