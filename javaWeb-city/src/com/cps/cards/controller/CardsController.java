/**
 * 
 */
package com.cps.cards.controller;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Card;
import com.cps.remote.CardBeanRemote;
import com.cps.util.JsfUtil;

import javax.servlet.ServletContext;  
/**
 * @author Jorge
 * 
 */
@ManagedBean
public class CardsController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Logger LOGGER = Logger.getLogger(CardsController.class);
    private List<Card> listCards;
	private CardBeanRemote cardBeanRemote;
    private StreamedContent file;  
	
	public CardsController() {
		    InputStream stream = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream(File.separator + "WEB-INF"+ File.separator +"templates"+ File.separator +"cards.xls");    
	        setFile(new DefaultStreamedContent(stream, "application/xls", "cards.xls"));  
	}
	public void handleFileUpload(FileUploadEvent event) {  
		
		try {

			listCards = new ArrayList<Card>();
			LOGGER.info("Inicia Carga de Tarjetas : Archivo : "+event.getFile().getFileName());
			HSSFWorkbook wb  = new HSSFWorkbook(event.getFile().getInputstream());
			LOGGER.info("Se obtiene la primera hoja");
			HSSFSheet sheet = wb.getSheetAt(0);
			
			Iterator<?> iterator = sheet.rowIterator();
			//Se elimina fila titulo
			iterator.next();iterator.remove();
			Card card;
			while(iterator.hasNext()){
				
				HSSFRow hssfRow = (HSSFRow) iterator.next();
				HSSFCell pinCell = hssfRow.getCell(0);
				HSSFCell valueCell = hssfRow.getCell(1);
				HSSFCell currencyCell = hssfRow.getCell(2);
                String pinNumber = "";
                double amount = 0;
                String currency ="";
				//Se obtiene el pin
                switch(pinCell.getCellType())
                {
                    case HSSFCell.CELL_TYPE_NUMERIC:
                    	Double pinNumericValue = new Double(pinCell.getNumericCellValue());
                    	pinNumber = String.valueOf(pinNumericValue.longValue());
                        break;
                    case HSSFCell.CELL_TYPE_STRING:
                    	pinNumber = pinCell.toString();
                        break;
                }
				
                amount = valueCell.getNumericCellValue();
                currency = currencyCell.toString();
				
                card = new Card();
                card.setPin(pinNumber);
                card.setPrice(new BigDecimal(amount));
                card.setCurrency(currency);
                card.setUsed(0);
                card.setActivationDate(null);
                card.setUser(null);
                LOGGER.info("Datos de tarjeta: PIN="+pinNumber+" Price="+amount+" Currency:"+currency);
                listCards.add(card);
			}
			if(listCards != null && listCards.size() > 0){
			cardBeanRemote = EJBServiceLocator.getInstace().getEjbService(CardBeanRemote.class);
			LOGGER.info("Se inicia proceso para guardar en BD");
			cardBeanRemote.importCards(listCards);
            JsfUtil.addMessage("cards.importOk", FacesMessage.SEVERITY_INFO);
			}else{
			JsfUtil.addMessage("cards.emptyFile", FacesMessage.SEVERITY_ERROR);	
			}
			
		} catch (Exception e) {
			LOGGER.error("Error al procesar archivo de importacion de tarjetas", e);
			JsfUtil.addMessage("cards.failImport", FacesMessage.SEVERITY_ERROR);
		}
        
    }
	public void setFile(StreamedContent file) {
		this.file = file;
	}
	public StreamedContent getFile() {
		return file;
	}  
	
}
