package com.cps.seguridad.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.SysUser;
import com.cps.remote.SysUserBeanRemote;
import com.cps.security.RampENC;
import com.cps.util.JsfUtil;
import com.cps.util.MailSender;
import com.cps.util.Util;

/**
 * @author Jorge
 *
 */
@ManagedBean
@RequestScoped
public class LoginController {

	private Logger LOGGER = Logger.getLogger(LoginController.class);
	private SysUserBeanRemote sysUserBeanRemote;
	private SysUser sysUser;
	private String login;
	private String pass;

	/**
	 * Valida el usuario e inicia la sesion.
	 * 
	 * @return navigation to
	 */
	public String loggin() {
		String nextPage = null;
		try {

			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);

			if (login == null || login.trim().equals("")) {
				JsfUtil.addMessage("loginRequired", FacesMessage.SEVERITY_ERROR);
			} else if (pass == null || pass.trim().equals("")) {
				JsfUtil.addMessage("passRequired", FacesMessage.SEVERITY_ERROR);
			} else if (sysUserBeanRemote.isValidCredentials(login, pass)) {
				sysUser = sysUserBeanRemote.getUser(login);
				HttpSession session = JsfUtil.getRequest().getSession(true);
				session.setAttribute("userSession", sysUser);
				nextPage = "index";
			} else {
				JsfUtil.addMessage("loginPassIncorrect",
						FacesMessage.SEVERITY_ERROR);
			}
		} catch (Exception e) {
			LOGGER.error("Error en loggin de Usuario", e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
		return nextPage;
	}

	/**
	 * Invalida la Sesion del Usuario.
	 * 
	 * @return
	 */
	public String logout() {
		ExternalContext ctx = FacesContext.getCurrentInstance()
				.getExternalContext();
		String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();

		try {
			// Usar el contexto de JSF para invalidar la sesion,
			// NO EL DE SERVLETS (nada de HttpServletRequest)
			((HttpSession) ctx.getSession(false)).invalidate();
			setSysUser(null);
			// Redireccion de nuevo con el contexto de JSF,
			// si se usa una HttpServletResponse fallara.
			// Sin embargo, como ya esta fuera del ciclo de vida
			// de JSF se debe usar la ruta completa
			ctx.redirect(ctxPath + "/faces/index.xhtml");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String rememberPass() {
		try {
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);
			SysUser sysUser = sysUserBeanRemote.getUserByEmail(this.getLogin());
			if (sysUser != null) {
				// String newPass = com.cps.util.Util.generatePassword();
				// sysUserBeanRemote.setPassword(sysUser, newPass);

				long expTime = Calendar
						.getInstance(TimeZone.getTimeZone("GMT"))
						.getTimeInMillis()
						+ (1000 * 60 * 120);
				String recoverInfo = expTime + "," + sysUser.getIdsysUser();
				String encodedInfo = RampENC.encrypt(recoverInfo);

				URL url = new URL(JsfUtil.getRequest().getRequestURL()
						.toString());
				URL newUrl = new URL(url.getProtocol(), url.getHost(),
						url.getPort(), JsfUtil.getRequest().getContextPath());

				String urlstr = newUrl.toString()
						+ "/faces/security/passwordRecovery.xhtml";
				/*
				 * Este es el campo value de la etiqueta del hidden
				 */
				String checkValue = encodedInfo;
				String rlink = "<a class='btn' href=\"{urlstr}?check={checkvalue}\">"+JsfUtil.getMessage("email.forgot.1.description.doclickhere", FacesMessage.SEVERITY_INFO).getSummary()+"</a> ";

				rlink = rlink.replace("{checkvalue}", checkValue);
				rlink = rlink.replace("{urlstr}", urlstr);

				JsfUtil.addMessage("forgotOk", FacesMessage.SEVERITY_INFO);
				// TODO talk to MSJ in order to send SHA-1 Password instead of
				// raw, To avoid security gaps
				// IServicioPagoClient
				// .getInstance()
				// .getPortBinding().cambiarContraseñaUsuario(
				// sysUser.getLogin(), "", newPass );

				// #{msgs['email.forgot.1.description']}
				String subject = JsfUtil.getMessage("forgotMailSubject",
						FacesMessage.SEVERITY_INFO).getSummary();
				String body = Util
						.getTemplateString("newsletter/emailForgot.xhtml")
						.replace("{cps_login}", sysUser.getLogin())
						// .replace( "{cps_resetlink}", urlstr)
						.replace(
								"{cps_msg}",
								JsfUtil.getMessage(
										"email.forgot.1.description",
										FacesMessage.SEVERITY_INFO)
										.getSummary().replace("{rlink}", rlink))
						.replace("{cps_cu}", sysUser.getCu())
						.replace("{cps_name}", sysUser.getName())
						.replace("{cps_lastName}", sysUser.getLastName());
				// .replace( "{cps_phone}", sysUser.getFavoriteMsisdn() );
				// String body = JsfUtil.getMessage("forgotMailBody",
				// FacesMessage.SEVERITY_INFO).getSummary().replace("{0}",
				// sysUser.getName()).replace( "{1}", newPass ).replace( "{2}",
				// sysUser.getLogin() ).replace( "{3}", sysUser.getCu()
				// ).replace( "{4}", sysUser.getFavoriteMsisdn() )
				String sender = JsfUtil.getMessage("forgotMailFrom",
						FacesMessage.SEVERITY_INFO).getSummary();
				MailSender.sendMail(subject, body, sender, sysUser.getEmail());
			} else {
				this.setLogin(null);
				JsfUtil.addMessage("forgotLoginInvalid",
						FacesMessage.SEVERITY_ERROR);
			}

		} catch (Exception e) {
			e.printStackTrace();
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
		return null;

	}

	public SysUser getSysUser() {
		return sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}
