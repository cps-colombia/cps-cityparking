/*global module */

// app/help/helpRun.js
//
// Run config for cps help module
//
// 2015, CPS - Cellular Parking Systems

(function(){
    "use strict" ;

    helpRun.$inject = ['$window', '$App', '$Help'];
    function helpRun($window, $App, $Help) {

        if(!$window.localStorage.firstHelp) {
            $App.ready(function(){
                $Help.showModal();
            });
        }
    }

    // Exports
    module.exports = helpRun;
})();
