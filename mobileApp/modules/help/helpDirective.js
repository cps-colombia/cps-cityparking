(function(){
    "use strict";

    // Members
    var helpDirective = {
        button: helpButton
    };

    function helpButton(){
        var feedbackDirective = {
            restrict: "E",
            controller: "HelpController",
            controllerAs: "help",
            template: "<button class='button help-button button-icon " +
                "icon ion-ios-help-outline' hm-tap='help.showModal()'>" +
                "<span class='hide'>Help</span>" +
                "</button>"
        };
        return feedbackDirective;
    }

    // Exports
    module.exports = helpDirective;
})();
