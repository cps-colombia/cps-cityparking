/*global require */

// modules/help/helpController.js
//
// Help controller
//
// 2015, CPS - Cellular Parking Systems

(function(){
    "use strict";

    HelpController.$inject = ['$window', '$timeout', '$scope', '$Modal', '$Login'];
    function HelpController($window, $timeout, $scope, $Modal, $Login) {
        var help = this;

        // Members
        help.showModal = showModal;
        help.hideModal = hideModal;

        $scope.data = {
            sliderOpt: {
                speed: 500,
                autoplay: 5000,
                autoplayDisableOnInteraction: false
            }
        };
        // Watch
        $scope.$watch("data.slider", function(newSldier, oldSlider) {
            if(newSldier) {
                $timeout(function(){
                    newSldier.update();
                }, 100);
            }
        });

        // Functions
        function showModal() {
            $Modal.fromTemplateUrl("app/views/helpSlides.html", {
                id: "cityHelpModal",
                cssClass: "help-modal",
                closeBtn: false,
                removeOnHide: true,
                backdropClickToClose: false,
                hardwareBackButtonClose: false,
                scope: $scope
            }).then(function(modal) {
                $scope.loginButtons = !$Login.isAuthenticated();
                $scope.modal = modal;
                $scope.hideModal = hideModal;

                $timeout(function(){
                    $window.localStorage.firstHelp = "true";
                    modal.show();
                }, 100);
            });
        }

        function hideModal() {
            if($scope.modal) {
                $scope.modal.remove();
                $scope.modal = null;
            }
        }
    }
    // Exports
    module.exports = HelpController;
})();
