/*global require */

// modules/help/index.js
//
// index function for city help module
//
// 2015, CPS - Cellular Parking Systems

(function(){
    "use strict";

    var helpController = require("./helpController"),
        helpService = require("./helpService"),
        helpDirective = require("./helpDirective"),
        helpRun = require("./helpRun");

    window.angular.module("cityHelp", []);
    var cityHelp = window.angular.module("cityHelp");

    // Service
    cityHelp.service("$Help", helpService);

    // Controller
    cityHelp.controller("HelpController", helpController);

    // Directives
    cityHelp.directive("cityHelpButton", helpDirective.button);

    // OnRun
    cityHelp.run(helpRun);

})();
