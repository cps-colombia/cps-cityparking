(function(){
    "use strict";

    helpService.$inject = ['$controller', '$rootScope'];
    function helpService($controller, $rootScope) {
        // Members
        var Help = {
            showModal: showModal
        };
        return Help;

        // Functions
        function showModal() {
            var scope = $rootScope.$new(),
                helpCtrl = $controller('HelpController as payment', {
                    $scope: scope
                });

            helpCtrl.showModal();
        }
    }

    // Exports
    module.exports = helpService;
})();
