#!/usr/bin/env node

(function () {
    "use strict";

    var hookCopy = require("./copy"),
        hookReplaces = require("./replaces");

    function beforePlatformAdd (ctx) {
        var deferred = ctx.requireCordovaModule("q").defer();

        // Copy task
        hookCopy(ctx).then(function () {
            // Replaces in files
            hookReplaces(ctx).then(function (res) {
                deferred.resolve(res);
            }).catch(function (err) {
                deferred.reject(err);
            });
        }).catch(function (err) {
            deferred.reject(err);
        });

        // Promise
        return deferred.promise;
    }

    // Exports
    module.exports = beforePlatformAdd;
})();
