#!/usr/bin/env node

// This hook copy basic custom files by platform

(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path"),
        cpsUtil = require("../cli/lib/util");

    function hookCopy (ctx) {
        var deferred = ctx.requireCordovaModule("q").defer(),
            rootPath = ctx.opts.projectRoot,
            command = cpsUtil.cmdName(ctx.cmdLine),
            isAddUpdate = new RegExp("\\bplatform(?:s_add|_add|_update)?\\b"),
            srcFile,
            destFile;

        if(isAddUpdate.test(command) && ctx.opts.platforms.indexOf("android") > -1) {

            destFile = path.resolve(rootPath, "./platforms/android/build-extras.gradle");
            srcFile = path.resolve(rootPath, "templates/build-extras.gradle");

            if(!fs.existsSync(destFile)) {
                cpsUtil.copyFiles({
                    src: srcFile,
                    dest: destFile
                }, function (err, res) {
                    if(err) {
                        console.error(err);
                        console.error(("Error to copy build-extras.gradle").error);
                        deferred.reject(err);
                    } else {
                        console.info(("Success, Copy build-extras.gradle").debug);
                        deferred.resolve(res);
                    }
                });
            } else {
                deferred.resolve();
            }
        } else {
            deferred.resolve();
        }

        // Promise
        return deferred.promise;
    }

    // Exports
    module.exports = hookCopy;
})();
