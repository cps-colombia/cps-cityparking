#!/usr/bin/env node

// This hook call grunt for compilign css, js and other optimizations

(function () {
    "use strict";

    var path = require("path"),
        shell = require("shelljs"),
        cpsUtil = require("../cli/lib/util");


    function hookGrunt (ctx) {
        var deferred = ctx.requireCordovaModule("q").defer(),
            rootdir = ctx.opts.projectRoot,
            platforms = (ctx.opts.platforms || []).join(","),
            cpsBin = path.resolve(rootdir, "bin/cps"),
            command;

        if(ctx.cmdLine && ctx.cmdLine.indexOf("--no-grunt") > -1) {
            deferred.resolve(true);
            // Promise
            return deferred.promise;
        }
        // Run grunt from cps cli
        command = cpsUtil.buildCmd(cpsBin, [
            "--grunt", "build",
            "--platform=" + platforms
        ]);
        shell.exec(command, { async: true }, function (code, output) {
            if (code !== 0) {
                deferred.reject(code);
                shell.exit(1);
            } else {
                deferred.resolve(true);
            }
        });

        // Promise
        return deferred.promise;
    }

    // Exports
    module.exports = hookGrunt;
})();
