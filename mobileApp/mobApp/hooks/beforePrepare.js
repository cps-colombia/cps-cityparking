#!/usr/bin/env node

(function () {
    "use strict";

    var hookCopy = require("./copy"),
        hookGrunt = require("./grunt"),
        hookReplaces = require("./replaces");

    function beforePrepare (ctx) {
        var deferred = ctx.requireCordovaModule("q").defer();

        // Copy task
        hookCopy(ctx);

        // Replaces in files
        hookReplaces(ctx).then(function () {
            // Grunt task
            hookGrunt(ctx).then(function (res) {
                deferred.resolve(res);
            }).catch(function (err) {
                deferred.reject(err);
            });
        }).catch(function (err) {
            deferred.reject(err);
        });

        // Promise
        return deferred.promise;
    }

    // Exports
    module.exports = beforePrepare;
})();
