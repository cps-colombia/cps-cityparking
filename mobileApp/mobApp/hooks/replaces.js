#!/usr/bin/env node

// This hook replaces package parameters by platform
// get params from cpsConfig.json

(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path"),
        ld = require("lodash"),
        plist = require("plist"),
        cpsUtil = require("../cli/lib/util"),
        platform;

    function hookReplaces (ctx) {
        var deferred = ctx.requireCordovaModule("q").defer(),
            rootdir = ctx.opts.projectRoot,
            cmd = ctx.cmdLine.split(/\s+/),
            platforms = ctx.opts.platforms || [],
            configFile = cpsUtil.cpsConfig(),
            idPackage = "com.cps.cpsApp",
            finalDisplayName = configFile.displayName,
            platformData = ld.defaultsDeep({
                android: {
                    id: 1,
                    tag: "android-versionCode"
                },
                ios: {
                    id: 2,
                    tag: "ios-CFBundleVersion"
                },
                windows: {
                    id: 3
                }
            }, configFile.platform),
            changeDebug = false,
            debugState = false,
            isNotFisrt = false,
            filestoreplace = [],
            fullFilename,
            finalIdPackage,
            finalAppHost,
            displayName,
            appHost;

        platforms.forEach(function (value, key) {
            platform = value.toLowerCase();
            platformData = platformData[platform];

            // reads the configuration file and starts
            if (rootdir && configFile && platformData) {
                // TODO: better way to parse files

                if (platformData.id == 1) {
                    // Android
                    changeDebug = true;
                    idPackage = "(<widget/?.*?)(android-packageName=\")(:?.*?)(\")";
                    displayName = "(name=\"app_display_name\">)(:?.*?)(<\/?.*>)";
                    appHost = "(name=\"app_host\">)(:?.*?)(<\/?.*>)";

                    finalIdPackage = "$1$2" + platformData.packageId + "$4";
                    finalAppHost = "$1" + finalDisplayName.replace(/\s+/, "").toLowerCase() + "$3";
                    finalDisplayName = "$1" + finalDisplayName + "$3";

                    filestoreplace = [
                        "platforms/android/AndroidManifest.xml",
                        "platforms/android/res/values/strings.xml"
                    ];
                } else if (platformData.id == 2) {
                    // ios
                    idPackage = "(<widget/?.*?)(ios-CFBundleIdentifier=\")(:?.*?)(\")";

                    finalIdPackage = "$1$2" + platformData.packageId + "$4";

                    var plistFile = path.join(rootdir, "platforms/ios/cpsApp/cpsApp-Info.plist"),
                        hasPlistFile = fs.existsSync(plistFile),
                        infoPlist = hasPlistFile && plist.parse(fs.readFileSync(plistFile, "utf8")) || {},
                        infoContents;

                    if(infoPlist.CFBundleDisplayName) {
                        infoPlist.CFBundleDisplayName = finalDisplayName;
                        console.log("Update displayName for: " + platform);

                        infoContents = plist.build(infoPlist);
                        infoContents = infoContents.replace(/<string>[\s\r\n]*<\/string>/g,"<string></string>");
                        fs.writeFileSync(plistFile, infoContents, "utf-8");
                    }


                    filestoreplace = [
                        "platforms/ios/cpsApp/cpsApp-Info.plist"
                    ];
                } else if (platformData.id == 3) {
                    // windows
                    idPackage = null;
                    displayName = "\"cpsApp\"";
                    // filestoreplace = [
                    //     "platforms/windows/package.phone.appxmanifest",
                    //     "platforms/windows/package.windows.appxmanifest"
                    // ];
                }

                // All
                filestoreplace.push("config.xml");
                if(!finalIdPackage) {
                    finalIdPackage = platformData.packageId;
                }

                // Replace for file
                filestoreplace.forEach(function (val, index, array) {
                    fullFilename = path.join(rootdir, val);
                    if (fs.existsSync(fullFilename)) {

                        // Replace ID
                        if(idPackage && finalIdPackage) {
                            replaceInFile("appID",
                                          fullFilename,
                                          idPackage,
                                          finalIdPackage);
                            // Default ID
                            replaceInFile(null,
                                          fullFilename,
                                          "(<widget/?.*?)(id=\")(:?.*?)(\")",
                                          finalIdPackage);
                        }


                        // Update Display Name
                        if(displayName) {
                            replaceInFile("displayName", fullFilename,
                                          displayName,
                                          finalDisplayName);
                        }

                        // Update Host
                        if(appHost) {
                            replaceInFile("appHost",
                                          fullFilename,
                                          appHost,
                                          finalAppHost);
                        }

                        // Replace debuggable
                        if(changeDebug) {
                            debugState = cmd.indexOf("--release") <= -1;
                            replaceInFile(debugState + "Debug",
                                          fullFilename,
                                          "(debuggable=\")(:?.*?)(\")",
                                          "$1" + debugState + "$3");
                        }

                        // Update version
                        replaceInFile("appVersion",
                                      fullFilename,
                                      "(<widget/?.*?)(version=\")(:?.*?)(\")",
                                      "$1$2" + (configFile.versionPkg || configFile.version) + "$4");

                        if(platformData && (platformData.tag || platformData.regexp) && platformData.versionCode) {
                            replaceInFile("appVersionCode",
                                          fullFilename,
                                          platformData.regexp ||
                                          "(<widget/?.*?)(" + platformData.tag + "=\")(:?.*?)(\")",
                                          "$1$2" + platformData.versionCode + "$4");
                        }

                        isNotFisrt = true;
                        return isNotFisrt;
                    } else {
                        console.log("missing: " + fullFilename);
                    }
                });

            }

        });
        // TODO: make forEach async
        deferred.resolve(true);

        // Promise
        return deferred.promise;
    }
    // Private
    /**
     * @name replaceInFile
     *
     * @description replace custom values in multiple files
     *
     * @param filename relative path to file will be changed
     * @param to_replace string or regexp to replace
     * @param replace_with string that will be replaced
     *
     */
    function replaceInFile (action, filename, to_replace, replace_with) {
        var data = fs.readFileSync(filename, "utf8"),
            match = data.match(new RegExp(to_replace, "g")),
            result = data.replace(new RegExp(to_replace, "g"), replace_with);

        if(match && action) {
            console.log("Update " + action + " for: " + platform);
        }

        fs.writeFileSync(filename, result, "utf8");
    }

    // Exports
    module.exports = hookReplaces;
})();
