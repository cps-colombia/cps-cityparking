(function () {
    "use strict";

    var path = require("path"),
        ld = require("lodash"),
        loadGrunt = require("load-grunt-config"),
        timeGrunt = require("time-grunt"),
        cpsUtil = require("./cli/lib/util");

    function cpsGrunt (grunt) {
        var parentPath = cpsUtil.parentPath(true),
            rootPath = cpsUtil.rootPath(),
            configFile = cpsUtil.cpsConfig(),
            appPackage = cpsUtil.appPackage(),
            themeApp = configFile.theme ? parentPath + "/theme/" + configFile.theme : "src/theme/default";

        // Get time for optimizing build times
        timeGrunt(grunt);

        // run grunt config
        loadGrunt(grunt, {
            init: true,
            loadGruntTasks: {
                scope: cpsUtil.isNpm() && ["dependencies", "optionalDependencies"]
            },
            data: {
                jitGrunt: true,
                config: configFile,
                appApckage: appPackage,
                theme: themeApp,
                buildName: "<%= package.name %>-<%= ld.snakeCase(package.version) %>",
                themePath: path.resolve(__dirname, themeApp),
                parentPath: parentPath,
                rootPath: rootPath,
                libs: {
                    src: "src/js",
                    dist: parentPath + "/www/app"
                },
                www: {
                    src: "src/",
                    dist: parentPath + "/www"
                },
                static: {
                    src: "src/static",
                    dist: parentPath + "/www/static"
                },
                ld: ld,
                util: cpsUtil,
                platform: grunt.option("platform"),
                debug: (configFile.debug || !configFile.nologger || false)
            }
        });
    }

    // Exports
    module.exports = cpsGrunt;
})();
