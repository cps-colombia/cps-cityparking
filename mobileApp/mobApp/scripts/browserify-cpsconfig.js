(function () {
    "use strict"

    const stream = require("stream");
    const cpsUtil = require("../cli/lib/util");
    const isCpsConfigRegExp = /\b(?:cpsConfig.*)$/;

    function isCpsConfig (filename) {
        return isCpsConfigRegExp.test(filename);
    }

    module.exports = function (filename, options) {
        if (!isCpsConfig(filename)) {
            return new stream.PassThrough();
        }

        let data = "";
        return new stream.Transform({
            transform (chunk, encoding, callback) {
                data = cpsUtil.cpsConfig(options.cpsConfig);
                callback(null);
            },

            flush (callback) {
                this.push(JSON.stringify(data, null, 2));

                callback(null);
            }
        })
    }
})();
