#!/usr/bin/env node

// action after npm install
//
// 2015, CPS - Cellular Payment Systems

(function(){
    "use strict";

    var fs = require("fs"),
        path = require("path"),
        spawn = require("child_process").spawn,
        cpsUtil = require("../cli/lib/util");

    //Create asynchronous symlink of "cps" bin
    var modelePath = path.resolve(__dirname, "../"),
        parentPath = path.resolve(modelePath, "../../");

    fs.stat(path.resolve(parentPath, "./package.json"), function(err, stats){
        if(stats && stats.isFile()) {
            // Generate executable script file
            cpsUtil.generateExec(function(err, pathScript){
                if(pathScript) {
                    spawn(pathScript, ["install", "--base"], {
                        stdio:"inherit"
                    });
                }
            });
        }
    });
})();
