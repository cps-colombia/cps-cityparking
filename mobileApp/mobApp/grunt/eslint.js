(function () {
    "use strict";

    // Functions
    function eslintGrunt (grunt, data) {
        var eslint = {
            target: ["Gruntfile.js", "src/", "cli/", "hooks", "test/"]
        };
        return eslint;
    }

    // Exports
    module.exports = eslintGrunt;
})();
