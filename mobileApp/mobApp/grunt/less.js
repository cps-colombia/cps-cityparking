(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path");

    // Functions
    function lessGrunt (grunt, data) {
        var userLess = path.resolve(data.themePath, "static/less/main.less"),
            lessFiles = {
                "<%= static.dist %>/css/main.min.css": "<%= static.src %>/less/main.less"
            },
            less;


        if (fs.existsSync(userLess)) {
            lessFiles["<%= static.dist %>/css/" + data.config.theme + ".min.css"] = userLess;
        }
        less = {
            debug: {
                files: lessFiles,
                options: {
                    compress: false
                }
            },
            dist: {
                files: lessFiles,
                options: {
                    cleancss: true,
                    compress: true
                }
            }
        };
        return less;
    }

    // Exports
    module.exports = lessGrunt;
})();
