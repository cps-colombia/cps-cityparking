(function () {
    "use strict";

    // Functions
    function browserSyncGrunt (grunt, data) {
        var browserSync = {
            dev: {
                bsFiles: {
                    src : [
                        "<%= www.dist %>/**/*"
                    ]
                },
                options: {
                    watchTask: true,
                    server: "<%= www.dist %>"
                }
            }
        };
        return browserSync;
    }
    // Exports
    module.exports = browserSyncGrunt;
})();
