(function () {
    "use strict";

    // Functions
    function uglifyGrunt (grunt, data) {
        var isDebug = data.debug,
            licenses = "@preserve|@cc_on|\\bAGPL\\b|\\bMIT\\b|\\bMPL\\b|" +
                "\\bAGPL\\\\bMIT\\b|\\bGPL\\b|\\bBSD\\b|\\bISCL\\b|\\(c\\)|License|Copyright",
            licenseRegexp = new RegExp(licenses, "mi"),
            prevCommentLine = 0,
            prevFile = "",
            uglify = {
                options: {
                    banner: "/*! <%= package.name %> v<%= package.version %> - <%= package.author %> - " +
                        "<%= grunt.template.today('yyyy-mm-dd') %> */"
                },
                all: {
                    files: [{
                        expand: true,
                        cwd:  "<%= www.dist %>/build/",
                        src:  [ "**/*.js", "!**/*.min.js"],
                        dest: "<%= libs.dist %>",
                        ext:  ".min.js"
                    }]
                },
                debug: {
                    options: {
                        beautify: true,
                        sourceMap: true,
                        sourceMapIncludeSources: true,
                        preserveComments: "all"
                    },
                    files: "<%= uglify.all.files %>"
                },
                dist: {
                    options: {
                        compress: {
                            join_vars: !isDebug,
                            drop_console: !isDebug
                        },
                        sourceMap: isDebug,
                        sourceMapIncludeSources: isDebug,
                        preserveComments: preserveComments
                    },
                    files: "<%= uglify.all.files %>"
                }
            };
        return uglify;

        // Private
        function preserveComments (node, comment) {
            if (comment.file !== prevFile) {
                prevCommentLine = 0;
            }

            var isLicense = licenseRegexp.test(comment.value) ||
                    (comment.type === "comment2" && comment.value.charAt(0) === "!") ||
                    comment.line === 1 ||
                    comment.line === prevCommentLine + 1;

            if (isLicense) {
                prevCommentLine = comment.line;
            } else {
                prevCommentLine = 0;
            }

            prevFile = comment.file;

            return isLicense;
        }
    }


    module.exports = uglifyGrunt;
})();
