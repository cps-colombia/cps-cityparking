(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path"),
        ld = require("lodash");

    // Functions
    function browserifyGrunt (grunt, data) {
        var modules = data.config.modules || [],
            srcPath = data.libs.src,
            files = ["<%= libs.src %>/core/cps.js"],
            browserify = {
                options: { // Global options
                    keepAlive: false,
                    transform: [
                        ["babelify", { presets: ["es2015"], compact: false }],
                        ["<%= rootPath %>/scripts/browserify-cpsconfig.js", {
                            cpsConfig: "<%= parentPath %>/cpsConfig.json"
                        }]
                    ]
                },
                all: {
                    files: {}
                },
                debug: {
                    options: {
                        watch: true,
                        browserifyOptions: { debug: true }
                    },
                    files: {
                        "<%= libs.dist %>/cps.min.js": "<%= browserify.all.files %>"
                    }
                },
                dist: {
                    options: {
                        watch: false,
                        browserifyOptions: { debug: false }
                    },
                    files: {
                        "<%= www.dist %>/build/<%= buildName %>.js":
                        "<%= browserify.all.files %>"
                    }
                }
            },
            modulePath;

        ld.forEach(modules, function (module, key) {
            if(ld.isObject(module) && module.id) {
                modulePath = "/modules/" + module.id + "/index.js";
            } else {
                modulePath = "/modules/" + ld.last(ld.kebabCase(module).split("-")) + "/index.js";
            }

            if(fs.existsSync(path.resolve(data.util.parentPath(), "./" + modulePath))) {
                // Module in theme
                modulePath = path.resolve(data.util.parentPath(), "./" + modulePath);
                files.push(modulePath);
            } else if (fs.existsSync(path.resolve(srcPath, "./" + modulePath))) {
                // Module in framework
                modulePath = "<%= libs.src %>" + modulePath;
                files.push(modulePath);
            } else {
                console.warn("Module not found: " + modulePath); // eslint-disable-line
            }
        });

        browserify.all.files = files;

        return browserify;
    }

    // Exports
    module.exports = browserifyGrunt;
})();
