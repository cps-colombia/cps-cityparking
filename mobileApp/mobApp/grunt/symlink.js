(function () {
    "use strict";

    // Functions
    function symlinkGrunt (grunt, data) {
        var symlink = {
            install: {
                options: {
                    overwrite: true
                },
                files: [{
                    src : ["<%= parentPath %>/platforms"],
                    dest : "platforms/"
                },{
                    src : ["<%= parentPath %>/plugins"],
                    dest : "plugins/"
                },{
                    src : ["<%= parentPath %>/www"],
                    dest : "www/"
                },{
                    src : ["<%= parentPath %>/res"],
                    dest : "res/"
                },{
                    src: ["bin/cps"],
                    dest: "<%= parentPath %>/cps"
                },{
                    src: ["<%= parentPath %>/cpsConfig.json"],
                    dest: "cpsConfig.json"
                }]
            }
        };

        return symlink;
    }

    // Exports
    module.exports = symlinkGrunt;
})();
