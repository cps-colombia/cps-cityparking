(function () {
    "use strict";

    // Functions
    function watchGrunt (grunt, data) {
        var watch = {
            options: {
                atBegin: true,
                spawn: false
            },
            less: {
                files: ["<%= static.src %>/less/**/*.less", "<%= theme %>/**/*.less"],
                tasks: ["less:debug"]
            },
            // browserify: {
            //     files: ["<%= libs.src %>/**/*.js", "cpsConfig.json",
            //             "<%= theme %>/**/*.js", "<%= parentPath %>/modules/**/*.js"],
            //     tasks: ["browserify:debug"]
            // },
            static: {
                files: ["<%= theme %>/static/**/*", "<%= theme %>/locale/**/*"],
                tasks: ["newer:copy:static"]
            },
            views: {
                files: [
                    "<%= libs.src %>/**/*.html",
                    "<%= theme %>/**/*.html",
                    "<%= parentPath %>/modules/**/*.html"
                ],
                tasks: ["newer:copy:views"]
            },
            config: {
                files: ["<%= parentPath %>/*.json"],
                tasks: ["copy:config", "browserify:debug"],
                options: {
                    reload: true
                }
            }
        };

        return watch;
    }

    // Exports
    module.exports = watchGrunt;

})();
