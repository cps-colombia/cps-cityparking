(function () {
    "use strict";

    // Functions
    function concatGrunt (grunt, data) {
        var srcFiles = ["<%= www.dist %>/build/<%= buildName %>.js"],
            concat = {
                all: {
                    files: {
                        "<%= www.dist %>/build/cps.js": srcFiles
                    }
                },
                debug: {
                    files: "<%= concat.all.files %>"
                },
                dist: {
                    files: "<%= concat.all.files %>"
                }
            };

        if(data.platform === "windows") {
            srcFiles.unshift("<%= libs.src %>/libs/utils/winstore-jscompat.js");
        }

        return concat;
    }

    // Exports
    module.exports = concatGrunt;
})();
