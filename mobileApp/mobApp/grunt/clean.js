(function () {
    "use strict";

    // Functions
    function cleanGrunt (grunt, data) {
        var clean = {
            all: {
                src: ["<%= libs.dist %>/*.map"]
            },
            install: {
                src: ["./platforms", "./plugins", "./www", "./res"],
                options: {
                    force: true
                }
            },
            debug: {
                src: "<%= clean.all.src %>",
                options: {
                    force: true
                }
            },
            dist: {
                src: [
                    "<%= www.dist %>/build/",
                    "<%= libs.dist %>/build/",
                    "<%= libs.dist %>/*.map",
                    "<%= libs.dist %>/*.js",
                    "<%= libs.dist %>/views/*.html",
                    "<%= libs.dist %>/../template/**",
                    "<%= libs.dist %>/../locale/**",
                    "<%= libs.dist %>/../static/**"
                ],
                options: {
                    force: true
                }
            },
            after: {
                src: ["<%= www.dist %>/build/", "<%= libs.dist %>/build/",
                    "<%= libs.dist %>/*.js", "!<%= libs.dist %>/*.min.js",
                    "<%= static.dist %>/**/*.css", "!<%= static.dist %>/**/*.min.css"],
                options: {
                    force: true
                }
            }
        };

        return clean;
    }

    // Exports
    module.exports = cleanGrunt;
})();
