(function () {
    "use strict";

    // Functions
    function mkdirGrunt (grunt, data) {
        var mkdir = {
            install: {
                options: {
                    mode: "0755",
                    create: [
                        "<%= parentPath %>/res",
                        "<%= parentPath %>/www",
                        "<%= parentPath %>/theme",
                        "<%= parentPath %>/plugins",
                        "<%= parentPath %>/platforms"
                    ]
                }
            }
        };

        return mkdir;
    }

    // Exports
    module.exports = mkdirGrunt;
})();
