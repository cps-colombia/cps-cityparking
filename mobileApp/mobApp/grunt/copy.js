(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path");

    // Functions
    function copyGrunt (grunt, data) {
        var copy = {
            views: {
                files: [{
                    expand: true,
                    flatten: true,
                    filter: "isFile",
                    cwd: "<%= libs.src %>/",
                    src: ["**/*.html"],
                    dest: "<%= libs.dist %>/views/"
                }, {
                    expand: true,
                    flatten: true,
                    filter: "isFile",
                    cwd: "<%= parentPath %>/modules/",
                    src: ["**/*.html"],
                    dest: "<%= libs.dist %>/views/"
                }, {
                    expand: true,
                    cwd: "<%= theme %>/",
                    src: ["template/**/*.html", "index.html"],
                    dest: "<%= www.dist %>/"
                }]
            },
            static: {
                files: [{
                    expand: true,
                    cwd: "<%= static.src %>/",
                    src: ["fonts/*", "images/*"],
                    dest: "<%= static.dist %>/",
                    // Copy if file does not exist.
                    filter: function (filepath) {
                        var dest = path.join(
                            grunt.task.current.data.files[0].dest,
                            path.basename(filepath)
                        );
                        // Return false if the file exists.
                        return !(grunt.file.exists(dest));
                    }
                }, {
                    expand: true,
                    cwd: "<%= theme %>/",
                    src: ["locale/*", "static/images/*", "static/fonts/*"],
                    dest: "<%= www.dist %>/"
                }]
            },
            config: {
                files: [{
                    nonull: true,
                    src: "<%= parentPath %>/cpsConfig.json",
                    dest: "<%= rootPath %>/cpsConfig.json",
                    filter: "isFile"
                }]
            },
            test: {
                files: [{
                    nonull: true,
                    src: "<%= rootPath %>/templates/cpsConfig_example.json",
                    dest: "<%= rootPath %>/cpsConfig.json",
                    filter: function (src) {
                        try {
                            var stat = fs.statSync(path.join(data.rootPath, "./cpsConfig.json"));
                            return !stat.isFile();
                        }
                        catch (error) {
                            return true;
                        }
                    }
                }]
            }
        };

        return copy;
    }

    // Exports
    module.exports = copyGrunt;
})();
