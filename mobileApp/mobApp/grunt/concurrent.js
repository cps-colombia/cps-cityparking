(function () {
    "use strict";

    // Esports
    module.exports = {
        install: ["clean:install"],
        install2: ["mkdir:install"],
        debug1: ["newer:clean:debug"],
        debug2: ["newer:copy:config", "newer:copy:views",
            "newer:copy:static", "newer:less:debug", "newer:browserify:debug"],
        debug3: ["newer:cssmin:debug", "newer:concat:debug"],
        debug4: ["newer:uglify:debug"],
        dist1: ["clean:dist"],
        dist2: ["copy:config", "copy:views", "copy:static", "less:dist", "browserify:dist"],
        dist3: ["cssmin:dist", "concat:dist"],
        dist4: ["uglify:dist"],
        after: ["clean:after"],
        test: ["eslint", "copy:test"]
    };

})();
