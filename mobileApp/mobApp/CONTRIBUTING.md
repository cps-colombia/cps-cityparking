# Contributing to CPS Mobile Framework

As a contributor, here are the guidelines we would like you to follow:

 - [PRs and Code contributions](#prs-and-code-contributions)
 - [Branches](#branches)
 - [Coding Rules](#coding-rules)
 - [Commit Message Guidelines](#commit-message-guidelines)

## PRs and Code contributions

- Tests must pass.
- Follow existing coding style.
- If you fix a bug, add a test.

## Branches

- Use the *master* branch for bug fixes or minor work that is intended for the current release stream
- Use the correspondingly named branch, e.g. `feature-maps-bing`, for a future feature of major bug fixes
- Use the *develop* for next release

## Coding Rules

To ensure consistency throughout the source code, keep these rules in mind as you are working:

- All features or bug fixes must be tested by one or more specs (unit-tests).
- All public API methods must be documented. (Details TBC).
- We follow [Google's JavaScript Style Guide][js-style-guide], but wrap all code at 100 characters.

## Commit Message Guidelines

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
we use the git commit messages to **generate the CHANGELOG**.

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **component** and a **subject**:

```
<type>[<component>] <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **component** of the header is is optional but recommended for important commits.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier
to read on GitLab as well as in various git tools.

Footer should contain a [closing reference to an issue](https://help.github.com/articles/closing-issues-via-commit-messages/) if any.

Samples: (even more [samples](http://104.155.190.166/cps/mobApp/commits/master))

```
Docs[api]: update CHANGELOG 2.7
```
```
Fix[login]: the second password is not set

New unit is added to create correctly the second password hash
```

### Revert
If the commit reverts a previous commit, it should begin with `revert: `,
followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`,
where the hash is the SHA of the commit being reverted.

### Type
Must be one of the following, first letter capitalized:

* **Feat**: A new feature
* **Fix**: A bug fix
* **Changes**: Changes in functionality without new features
* **Docs**: Documentation only changes
* **Style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **Refactor**: A code change that neither fixes a bug nor adds a feature
* **Perf**: A code change that improves performance
* **Test**: Adding missing tests or correcting existing tests
* **Build**: Changes that affect the build system or external dependencies (example component: grunt, npm)
* **Ci**: Changes to our CI configuration files and scripts (example component: Travis, Gitlab)
* **Chore**: Other changes that don't modify `src` or `test` files

### Component
The *component* could be anything specifying place of the commit change, should be a module or important part of application. For example
* Modules: `login`, `payment`, etc.
* Parts: `cli`, `ui`, `core`, etc.

### Subject
The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* when only changing documentation, include [skip]
* don't capitalize first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to
reference **Jira** issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.



[js-style-guide]: https://google.github.io/styleguide/javascriptguide.xml