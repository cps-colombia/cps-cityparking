/* global module */

// app/modules/balance/balanceDirective.js
//
// directives function for balance module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    function walletPockets () {
        var balanceTransferDirective = {
            restrict: "E",
            templateUrl: "app/views/walletPockets.html"
        };
        return balanceTransferDirective;
    }

    // Exports
    module.exports = {
        walletPockets: walletPockets
    };

})();
