/* global require */

// modules/wallet/walletController.js
//
// Wallet modules for cps app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    WalletController.$inject = [
        "$q",
        "$scope",
        "$App",
        "$Logger",
        "$Mobile",
        "$Wallet",
        "$Dialog"
    ];
    function WalletController ($q, $scope, $App, $Logger, $Mobile, $Wallet, $Dialog) {
        var _ = $Mobile.locale,
            wallet = this,
            logger = $Logger.getInstance();

        // Members
        wallet.options = $Wallet.options;
        wallet.initPockets = initPockets;
        wallet.hasPockets = $Wallet.hasPockets;
        wallet.pocketDetail = $Wallet.pocketDetail;
        wallet.getPocketData = $Wallet.getPocketData;
        wallet.methodPocketDialog = methodPocketDialog;
        wallet.payDataDialog = payDataDialog;
        wallet.isPocketAllow = isPocketAllow;

        // Wallet pockets data
        wallet.pockets = {};
        wallet.pocketsAllow = [];
        wallet.payment = {
            data: null
        };
        // Select pocket to pay
        wallet.method = {
            pocket: {
                id: null
            }
        };

        // Control
        wallet.formSuccess = false;

        // Local event
        $scope.$on("cps:balanceUpdate", function (event, newBalance) {
            // TODO: suppport for multiples pockets
            if(ld.isObject(newBalance) && !ld.isEmpty(newBalance.detail)) {
                var pockets = newBalance.detail.pockets,
                    index = ld.findIndex(wallet.pockets, {id: parseInt(pockets.id)});

                if(index > -1) {
                    if(wallet.pockets[index] && wallet.pockets[index].info) {
                        wallet.pockets[index].info.balance = pockets.balance;
                        logger.info("wallet", "update balance for pocket {0} with new balance {1}", [
                            pockets.id,
                            pockets.balance
                        ]);
                    }
                }
            }
        });

        $scope.$on("$destroy", function () {
            // Clean events
            $scope.$$listeners["cps:balanceUpdate"] = [];
        });

        // Run
        setWalletPayData();

        // Functions
        function initPockets (inBackground) {
            // Load from storage to have to show
            wallet.pockets = $Wallet.getPocketsData();
            // Update pockets
            $Wallet.getPockets({
                background: inBackground
            }).then(function (pockets) {
                wallet.pockets = pockets;
            });
        }

        function setWalletPayData () {
            $Wallet.setWalletPayData(wallet.payment);
        }

        function methodPocketDialog (notifyObj, scope) {
            var deferred = $q.defer(),
                notifyData = notifyObj.data || {},
                payload = notifyData.payload || {},
                methodDialog,
                pocketData = {},
                pocketBalance = 0,
                pocketId;

            // Show pocket select
            scope.isPayment = true;
            scope.transaction = notifyData.payload;
            if(payload.branch && payload.branch.allow_pockets) {
                wallet.allowPockets = [];
                scope.allow_pockets = payload.branch.allow_pockets;
            }

            // Create dialog
            methodDialog = $Dialog.show({
                title: _("paymentMethod"),
                templateUrl: "app/views/walletPockets.html",
                scope: scope,
                buttons: [{
                    text: _("cancel"),
                    type: "button-default"
                }, {
                    text: _("submit"),
                    type: "button-primary",
                    onTap: function (e) {
                        if (!wallet.method.pocket.id) {
                            e.preventDefault();
                        } else {
                            pocketId = parseInt(wallet.method.pocket.id, 10);
                            wallet.method.pocket.id = pocketId;

                            pocketData = ld.extend(
                                wallet.method.pocket,
                                ld.clone(ld.find(wallet.pockets, { id: pocketId}))
                            );

                            // Get pocket balance
                            pocketBalance = pocketData.info && pocketData.info.balance;

                            // Prevent close if balance not enough
                            if(pocketBalance < payload.amount) {
                                $Mobile.alert(_("alertWalletBalanceNotEnough"));
                                e.preventDefault();
                            }

                            return {data: wallet.method, notify: notifyObj, dialog: methodDialog};
                        }
                    }
                }]
            });

            // When $Dialog close
            methodDialog.then(function (res) {
                if(res) {
                    deferred.resolve(res);
                } else {
                    deferred.reject(res);
                }
                // Clean
                wallet.method = {
                    pocket: {
                        id: null
                    }
                };
                scope.$destroy();
            });

            // Promise
            return deferred.promise;
        }


        function payDataDialog (pocketObj, scope, callback) {
            var deferred = $q.defer(),
                dataDialog;

            // Extend scope
            scope.pocket = pocketObj;

            // Fallback callback
            callback = callback || angular.noop;

            // Create dialog
            dataDialog = $Dialog.show({
                title: _("pocket" + pocketObj.type.id + "Pay"),
                templateUrl: "app/views/walletPayData.html",
                scope: scope,
                buttons: [{
                    text: _("btnOk"),
                    type: "button-positive",
                    onTap: function (e) {
                        // Prevent close
                        e.preventDefault();

                        // Check if have one select
                        if(!wallet.payment.data) {
                            $Mobile.alert(_("alertWalletSelectBond"));
                        } else {
                            // Traditional callback allow call multiple
                            callback({data: wallet.payment.data, dialog: dataDialog});
                        }
                    }
                },{
                    text: _("btnCancel"),
                    type: "button-default"
                }]
            });

            // When $Dialog close
            dataDialog.then(function (res) {
                if(res) {
                    callback(res);
                } else {
                    deferred.reject(res);
                }
                // Clean
                wallet.payment = {
                    data: null
                };
                scope.$destroy();
            });

            // Promise
            return deferred.promise;
        }

        function isPocketAllow (allowPockets, pocket) {
            var isAllow;

            if(!ld.isObject(pocket)) {
                return true;
            }

            isAllow = $Wallet.isPocketAllow(allowPockets, {pocket_id: String(pocket.id)});
            if(isAllow) {
                wallet.pocketsAllow.push(pocket);
            }

            return isAllow;
        }

    }

    // Exports
    module.exports = WalletController;

})();
