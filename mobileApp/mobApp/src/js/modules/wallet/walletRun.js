/* global require */

// module/wallet/walletRun.js
//
// when run module registe evens
// to wallet Module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    walletRun.$inject = [
        "$rootScope",
        "$App",
        "$Logger",
        "$Wallet",
        "$Mobile",
        "$Numbro",
        "$Balance",
        "AUTH_EVENTS"
    ];
    function walletRun ($rootScope, $App, $Logger, $Wallet,
                       $Mobile, $Numbro, $Balance, AUTH_EVENTS) {

        var _ = $Mobile.locale,
            logger = $Logger.getInstance();

        // Get user procket in background
        $App.ready(function () {
            $rootScope.$on(AUTH_EVENTS.socketAuth, function (event, data) {
                $Wallet.getPockets({background: true});
            });
        });

        $rootScope.$on("cps:paymentConfirmReceiver:click", operationConfirm);
        $rootScope.$on("cps:paymentConfirmReceiverSuccess", operationSuccess);
        $rootScope.$on("cps:paymentConfirmReceiverSuccess:click", operationOnClick);

        $rootScope.$on("cps:withdrawalConfirmReceiver:click", operationConfirm);
        $rootScope.$on("cps:withdrawalConfirmReceiverSuccess", operationSuccess);
        $rootScope.$on("cps:withdrawalConfirmReceiverSuccess:click", operationOnClick);

        $rootScope.$on("cps:paymentConfirmReceiverExpired:click", operationOnClick);
        $rootScope.$on("cps:withdrawalConfirmReceiverExpired:click", operationOnClick);

        // Funtions
        function operationConfirm (event, res) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // ensure that the object does not come as string
            if(angular.isString(res.notify.data)) {
                res.notify.data = JSON.parse(res.notify.data);
            }
            var notify = res.notify.data,
                notifyData = notify.notifyData,
                payload = notifyData.payload || {},
                opStatus;

            if(angular.isObject(notifyData) && notifyData.status !== "read") {
                $Mobile.confirm(_(notify.localeId + "Full", {
                    branch_name: payload.branch && payload.branch.name || "",
                    date: notify.dateFormat || notifyData.date || new Date(),
                    amount: $Numbro.formatCurrency(payload.amount) || 0
                }), function (buttonIndex) {

                    // Operation status
                    opStatus = buttonIndex !== 2 ? "approved" : "rejected";

                    if(event.name === "cps:paymentConfirmReceiver:click" ||
                       event.name === "cps:withdrawalConfirmReceiver:click") {
                        $Wallet.methods({
                            data: notifyData,
                            opStatus: opStatus
                        }).then(function (res) {
                            $Wallet.operationByPocket(res);
                        });
                    } else {
                        logger.warn("wallet", "operationConfirm: Invalid event");
                    }

                }, {btnCancel: _("btnReject")});
            }
        }

        function operationSuccess (event, notifyData) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // Confirm new balance
            if(notifyData.payload.balance) {
                $Balance.setBalance({
                    action: "new",
                    balances: notifyData.payload.balance
                });
            }
        }

        function operationOnClick (event, res) {
            // On click event
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // ensure that the object does not come as string
            if(angular.isString(res.notify.data)) {
                res.notify.data = JSON.parse(res.notify.data);
            }
            var notify = res.notify.data,
                notifyData = notify.notifyData,
                payload = notifyData.payload || {};

            // Check if notification exist
            if(angular.isObject(notifyData) && notifyData.status !== "read") {
                $Mobile.alert(_(notify.localeId + "Full", {
                    branch_name: payload.branch && payload.branch.name || "",
                    date: notify.dateFormat || notifyData.date || new Date(),
                    amount: $Numbro.formatCurrency(payload.amount) || 0
                }), function () {
                    // Something on click
                });
            }
        }

    }

    // Exports
    module.exports = walletRun;

})();
