/* global require */

// module/wallet/index.js
//
// index function for wallet
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        walletController = require("./walletController"),
        walletDirective = require("./walletDirective"),
        walletRoutes = require("./walletRoutes"),
        walletRun = require("./walletRun"),
        walletService = require("./walletService");

    angular.module("cpsWallet", ["cpsPayment"]);
    var cpsWallet = angular.module("cpsWallet");

    // Controller
    cpsWallet.controller("WalletController", walletController);

    // Services
    cpsWallet.factory("$Wallet", walletService);

    // Routes
    cpsWallet.config(walletRoutes);

    // Run
    cpsWallet.run(walletRun);

    // Directive
    cpsWallet.directive("cpsWalletPockets", walletDirective.walletPockets);
})();
