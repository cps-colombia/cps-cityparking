/* global module */

// modules/wallet/walletService.js
//
// factory service for wallet module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        async = require("async"),
        snakeize = require("snakeize"),
        ld = require("lodash");

    walletService.$inject = [
        "$q",
        "$window",
        "$controller",
        "$rootScope",
        "$Mobile",
        "$Logger",
        "$Login",
        "$App",
        "$Modal",
        "$Request",
        "$Dialog",
        "$Response",
        "$Socket",
        "$Balance",
        "$Encrypt"
    ];
    function walletService ($q, $window, $controller, $rootScope, $Mobile, $Logger,
                           $Login, $App, $Modal, $Request, $Dialog, $Response, $Socket, $Balance, $Encrypt) {
        var _ = $Mobile.locale,
            op = $App.ops,
            logger = $Logger.getInstance(),
            userHash = $Login.getUserHash,
            config = $App.config,
            walletOptions = ld.extend({
                unifiedBonus: false
            }, config.options && config.options.wallet || {}),
            userPockets = [],
            walletPayData = null;

        // Members
        var Wallet = {
            options: walletOptions,
            setWalletPayData: setPayData,
            setPayData: setPayData,
            isPocketAllow: isPocketAllow,
            hasPockets: hasPockets,
            getPocketsData: pocketsStorage,
            pockets: pocketsStorage,
            getPockets: getPockets,
            checkBond: checkBond,
            pocketDetail: pocketDetail,
            walletMethod: methods,
            methods: methods,
            walletOperationPocket: operationByPocket,
            operationByPocket: operationByPocket,
            operationGeneric: operationGeneric,
            operationCredit: operationCredit,
            operationBond: operationBond
        };
        return Wallet;

        // Functions
        function setPayData (data) {
            walletPayData = data;
        }

        function isPocketAllow (allowPockets, filterObj) {
            return allowPockets ? ld.find(allowPockets, filterObj) || false : true;
        }

        function hasPockets () {
            return userPockets.length >= 1;
        }

        function pocketsStorage (filterObj) {
            if(!userPockets.length) {
                if($window.localStorage.up) {
                    userPockets = $Encrypt.decrypt($window.localStorage.up);
                    if(ld.isObject(filterObj)) {
                        return ld.find(userPockets, filterObj);
                    }
                } else {
                    userPockets = [];
                }
            }
            return userPockets || [];
        }

        function getPockets (dataObject) {
            var deferred = $q.defer(),
                success = false,
                req = { // Wallet pockets Params
                    params: {
                        op: op.walletPockets,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            if(dataObject) {
                if(!dataObject.background) {
                    $Mobile.loading("show");
                }
                angular.extend(req.params, dataObject);
            } else {
                $Mobile.loading("show");
            }

            $Request.http(req).then(function (res) {
                var statusCode = res.data ? res.data.statusCode : null,
                    resData = res.data || {},
                    resPocket = {},
                    dataPocket;

                logger.debug("wallet", JSON.stringify(res.data));
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;
                    // TODO: parse all response to snake_case in $Request
                    dataPocket = snakeize(resData.data) || resData.data || {};
                    resPocket = dataPocket.pockets;

                    resPocket = ld.map(resPocket || [], function (pocket, key) {
                        if(pocket.pocket_type) {
                            if(!pocket.type) {
                                pocket.type = {};
                            }
                            pocket.id = pocket.pocket_id;
                            pocket.type.id = pocket.pocket_type.id_pocket_type;
                            pocket.created_date = new Date(pocket.created_date);
                        }
                        return pocket;
                    });

                    userPockets = angular.copy(resPocket);
                    $window.localStorage.up =  $Encrypt.aes(userPockets);
                    deferred.resolve(userPockets);
                }

                if(!success) {
                    deferred.reject(res.data);
                }

            }).catch(function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function checkBond (dataObject) {
            var deferred = $q.defer(),
                req = { // Wallet check bond
                    params: {
                        op: op.walletCheckBond,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            $Mobile.loading("show");

            if(dataObject) {
                angular.extend(req.params, dataObject);
            }

            $Request.http(req).then(function (res) {
                var statusCode = res.data ? res.data.statusCode : null;

                logger.debug("wallet", JSON.stringify(res.data));
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    deferred.resolve(res.data);
                } else if(statusCode === 320) {
                    // $Mobile.alert(_("alertWalletInvalidBond"));
                    deferred.reject(res.data);
                }
            }).catch(function (err) {
                deferred.reject(err);
            });

            // Promise
            return deferred.promise;
        }

        function pocketDetail (pocketObj) {
            var scope = $rootScope.$new(),
                paymentCtrl,
                pocketLocales;

            if(!ld.isObject(pocketObj)) {
                logger.warn("wallet", "pocketDetail need pocket object");
                return;
            }

            // Set values and pocket in scope
            pocketLocales = pocketObj.info.locales;

            try {
                paymentCtrl = $controller("PaymentController as payment", {
                    $scope: scope
                });
                paymentCtrl.getHistory({
                    filter: {pocket: {pocketId: pocketObj.id}}
                });
                pocketObj.history = paymentCtrl.userHistory;
            } catch(e) {
                logger.debug("wallet", "not payment module");
                pocketObj.history = null;
            }

            scope.pocket = pocketObj;

            // Create modal
            $Modal.fromTemplateUrl("app/views/walletPocketDetail.html", {
                scope: scope,
                id: "walletPocket" + pocketObj.pocket_id + "Modal",
                closeBtn: true,
                title: _({
                    key: "name",
                    keyFallback: "description",
                    locales: pocketLocales
                }, null, ld.toCapitalize((pocketObj.name || pocketObj.description)))
            }).then(function (modal) {
                scope.modal = modal;
                modal.show();
            });

            // Execute action on hide modal
            scope.$on("modal.hidden", function () {
                scope.$destroy();
            });
        }

        function methods (notifyObj) {
            var deferred = $q.defer(),
                defResponse = {data: {}, notify: notifyObj, dialog: null},
                scope,
                ctrl;

            if(notifyObj.opStatus === "approved") {
                $Login.passSecondDialog().then(function (res) {
                    if(res.authorized) {
                        // Call controller
                        scope = $rootScope.$new();
                        ctrl = $controller("WalletController as wallet", {
                            $scope: scope
                        });

                        if(!ctrl) {
                            $Mobile.alert(_("alertInternalError"));
                            notifyObj.opStatus = "rejected";
                            deferred.resolve(defResponse);

                            logger.warn("wallet", "methodPocket need WalletController");
                            return deferred.promise;
                        }

                        // Show dialog pocket method
                        ctrl.methodPocketDialog(notifyObj, scope).then(function (res) {
                            deferred.resolve(res);
                        }).catch(function () {
                            notifyObj.opStatus = "rejected";
                            deferred.resolve(defResponse);
                        });

                    } else {
                        notifyObj.opStatus = "rejected";
                        deferred.resolve(defResponse);
                    }
                });
            } else {
                deferred.resolve(defResponse);
            }

            // Promise
            return deferred.promise;
        }

        function operationByPocket (objData) {
            var notifyObj = objData.notify || {},
                notifyData = notifyObj && notifyObj.data || {},
                payload = notifyData.payload || {},
                pocketData = objData.data && objData.data.pocket || {},
                // pocketLocales = pocketData.info && pocketData.info.locales,
                pocketTypeId,
                pocketType,
                method;

            if(notifyObj.opStatus === "rejected" || ld.isEmpty(objData.data)) {
                pocketTypeId = 0;
            } else {
                pocketType = pocketData.type || {};
                pocketTypeId = pocketType.id >= 4 && pocketType.id <= 6 ? 4 : pocketType.id || 0;
            }

            method = {
                0: function () {
                    notifyObj.opStatus = "rejected"; // Sure is rejected
                    walletOperationEmit(notifyObj, {
                        id: pocketData.id,
                        data: [{amount: payload.amount}],
                        total: payload.amount
                    });
                },
                1: function () {
                    // Point
                    operationGeneric(notifyObj, pocketData);
                },
                2: function () {
                    // Bonds
                    if(walletOptions.unifiedBonus) {
                        operationGeneric(notifyObj, pocketData);
                    } else {
                        operationBond(notifyObj, pocketData);
                    }
                },
                3: function () {
                    // Credit
                    // walletOpreationGeneric(notifyObj, pocketData);
                    operationCredit(notifyObj, pocketData);
                },
                4: function () {
                    // Cash
                    operationGeneric(notifyObj, pocketData, {
                        fallbackLocale: "confirmPaymentPersonal"
                    });
                }
            };

            (method[pocketTypeId] || method[0])();
        }

        function operationGeneric (notifyObj, pocketObj, options) {
            var notifyData = notifyObj && notifyObj.data || {},
                payload = notifyData.payload || {},
                emitData = {
                    id: pocketObj.id,
                    data: [{amount: payload.amount}],
                    total: payload.amount
                },
                pocketBalance = pocketObj.info.balance,
                operation = ld.camelCase(notifyData.op),
                defaultTitle =  operation + "Dialog",
                titleArgs = {
                    name: pocketObj.info.locales[$Mobile.getLanguage()] || pocketObj.name || pocketObj.description,
                    operation: operation
                },
                success = false,
                newBalance;

            // Extend options
            options = ld.extend({
                title: null
            }, options);

            if(!options.fallbackLocale) {
                options.fallbackLocale = "operationConfirmDialog";
            }
            if(!options.title) {
                options.title = _(defaultTitle, titleArgs, _(options.fallbackLocale, titleArgs));
            }

            // Show confirm
            $Mobile.confirm(options.title, function (buttonIndex) {
                if (parseInt(buttonIndex) !== 2) {
                    if(pocketBalance < payload.amount) {
                        $Mobile.alert(_("alertWalletBalanceNotEnough"));
                    } else {
                        success = true;
                        // Emit socket event
                        walletOperationEmit(notifyObj, emitData).then(function (resPay) {
                            newBalance = resPay.data && resPay.data.balance;
                            if(resPay.statusCode == 352) {
                                $Mobile.alert(_(ld.camelCase("alertWallet-" + operation + "-expired"), null,
                                                _("alertWalletTransactionExpired")));
                            } else if(newBalance) {
                                $Balance.setBalance({
                                    action: "new",
                                    balances: newBalance
                                });
                            }
                        });
                    }
                }

                // Send rejected
                if(!success) {
                    notifyObj.opStatus = "rejected";
                    walletOperationEmit(notifyObj, emitData);
                }
            });
        }

        function operationCredit (notifyData) {
            var creditDialog = $Dialog.show({
                title: _("creditLine"),
                templateUrl: "app/views/walletPayData.html",
                buttons: [{
                    text: _("btnOk"),
                    type: "button-positive",
                    onTap: function (e) {
                        if(!walletPayData.data) {
                            e.preventDefault();
                        } else {
                            creditDialog.close();
                        }
                    }
                },{
                    text: _("btnCancel"),
                    type: "button-default"
                }]
            });
        }

        function operationBond (notifyObj, pocketObj) {
            var boundsTotal = 0,
                bondSendData = [],
                notifyData = notifyObj.data || {},
                payload = notifyData.payload || {},
                scope = $rootScope.$new(),
                ctrl = $controller("WalletController as wallet", {
                    $scope: scope
                }),
                newBalance;

            if(!ctrl) {
                $Mobile.alert(_("alertInternalError"));
                logger.warn("wallet", "methodPocket need WalletController");
                return;
            }

            // Call pay bond dialog
            ctrl.payDataDialog(pocketObj, scope, function (res) {
                // Clean
                boundsTotal = 0;
                bondSendData = [];

                if(angular.isObject(res) && angular.isObject(res.data)) {
                    // Check if bond is valid
                    async.forEachOf(res.data, function (amount, bond, callback) {
                        if(amount) {
                            boundsTotal = boundsTotal + parseInt(amount);
                            bondSendData.push({
                                pin: bond,
                                amount: amount
                            });
                            callback();
                            // checkBond({pin: bond}).then(function(res){

                            //     callback();
                            // }).catch(function(err){
                            //     callback(err);
                            // });
                        } else {
                            callback({error: "bond not amount"});
                        }
                    }, function (err) {
                        if(err) {
                            $Mobile.alert(_("alertWalletInvalidBond"));
                        } else if(bondSendData.length) {
                            // if pay is'n exact
                            if(notifyData.payload.amount < boundsTotal) {
                                $Mobile.alert(_("alertBondPayExact"));
                            } else {
                                // Emit success socket event
                                walletOperationEmit(notifyObj, {
                                    id: pocketObj.id,
                                    data: bondSendData,
                                    total: boundsTotal
                                }).then(function (resPay) {
                                    // TODO: Update bonds with socket
                                    getPockets({background: true});

                                    if(resPay.statusCode === 200) {
                                        if(res.dialog && res.dialog.close) {
                                            res.dialog.close();
                                        }

                                        newBalance = resPay.data && resPay.data.balance;
                                        if(newBalance) {
                                            $Balance.setBalance({
                                                action: "new",
                                                balances: newBalance
                                            });
                                        }
                                    } else if(resPay.statusCode === 320) {
                                        $Mobile.alert(_("alertWalletInvalidBond"));
                                    }
                                });
                            }
                        } else {
                            $Mobile.alert(_("alertWalletSelectBond"));
                        }
                    });

                } else {
                    logger.warn("wallet", "walletOperationBond Need object with data bonds");
                }
            }).catch(function (err) {
                notifyObj.opStatus = "rejected";
                walletOperationEmit(notifyObj, {
                    id: pocketObj.id,
                    data: [{amount: payload.amount}],
                    total: payload.amount
                });
            });
        }


        /**
         * @private
         */
        function walletOperationEmit (notifyObj, pocketData, callback) {
            var deferred = $q.defer(),
                notifyData = notifyObj && notifyObj.data || {},
                payload = notifyData.payload || {},
                eventName = ld.camelCase(notifyData.op);

            // Send event confirm
            logger.info("wallet", "Emit " + eventName);

            $Socket.emit("$cps:" + eventName + ":response",{
                platform_id: $App.getPlatform().platformId,
                transaction_id: payload.transaction_id,
                status: notifyObj.opStatus,
                pocket: pocketData
            }, function (resPay) {
                logger.info("wallet", JSON.stringify(resPay));
                // Update Personal pocket balance
                if(!resPay.data) {
                    $Mobile.alert(_("alertInternalError"));
                    deferred.reject(resPay);
                } else {
                    // Allow traditional callback
                    if(callback) {
                        callback(resPay);
                    }
                    deferred.resolve(resPay);
                }
            });

            // Promise
            return deferred.promise;
        }

    }

    // Exports
    module.exports = walletService;
})();
