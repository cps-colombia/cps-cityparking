/* global module */

// app/module/wallet/walletRoutes.js
//
// Routes for cps wallet module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    walletRoutes.$inject = [
        "$routeProvider",
        "USER_ROLES"
    ];
    function walletRoutes ($routeProvider, USER_ROLES) {
        $routeProvider
            .when("/wallet", {
                templateUrl: "template/wallet.html",
                controller: "WalletController",
                controllerAs: "wallet",
                data: {
                    title: "wallet",
                    roles: [USER_ROLES.user]
                }
            });
    }

    // Exports
    module.exports = walletRoutes;

})();
