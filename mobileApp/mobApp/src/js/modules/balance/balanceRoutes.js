/* global module */

// app/module/balance/balanceRoutes.js
//
// Routes for cps balance module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    balanceRoutes.$inject = ["$routeProvider", "USER_ROLES"];
    function balanceRoutes ($routeProvider, USER_ROLES) {
        $routeProvider
            .when("/balance", {
                templateUrl: "template/balance.html",
                controller: "BalanceController",
                controllerAs: "balance",
                data: {
                    title: "balance",
                    roles: [USER_ROLES.user]
                }
            })
            .when("/balance-transfer", {
                templateUrl: "template/balance.html",
                controller: "BalanceController",
                controllerAs: "balance",
                data: {
                    title: "transfers",
                    roles: [USER_ROLES.user]
                }
            }).when("/balance-recharge", {
                templateUrl: "template/balance.html",
                controller: "BalanceController",
                controllerAs: "balance",
                data: {
                    title: "recharging",
                    roles: [USER_ROLES.user]
                }
            });
    }

    // Exports
    module.exports = balanceRoutes;

})();
