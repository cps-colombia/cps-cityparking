/* global require */

// module/balance/balanceRun.js
//
// when run module registe evens
// to balance Module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    balanceRun.$inject = [
        "$rootScope",
        "$Logger",
        "$Mobile",
        "$Numbro",
        "$Socket",
        "$Balance",
        "$Notify",
        "AUTH_EVENTS"
    ];
    function balanceRun ($rootScope, $Logger, $Mobile, $Numbro, $Socket, $Balance, $Notify, AUTH_EVENTS) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance();

        // Events listen before controller is loaded
        $Socket.on("$cps:balanceTransfer:response", function (data, callback) {
            if(callback) {
                callback({success: true});
            }

            $Balance.transfer("on", data).then(function (res) {
                $rootScope.$broadcast("cps:balanceTransfer:success", res);
            }).catch(function (err) {
                logger.debug("debug", JSON.stringify(err));
            });
        });

        $rootScope.$on(AUTH_EVENTS.socketAuth, function (event, userData) {
            // Update user balance
            if(userData && userData.data) {
                $Balance.setBalance({
                    action: "new",
                    balances: userData.data.balance
                });
            }
        });

        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function (event, data) {
            // Destroy session
            $Balance.setBalance(null, {clean: true});
        });

        $rootScope.$on("cps:setBalance", function (event, data) {
            // Destroy session
            $Balance.setBalance(data);
        });

        // Transfer
        $rootScope.$on("cps:balanceTransferReceiver", function (event, notifyData) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            var payload = ld.get(notifyData, "payload") || {};
            // Update balance
            if(payload.to && payload.to.balance) {
                $Balance.setBalance({
                    action: "new",
                    balances: payload.to.balance
                });
            } else {
                $Balance.getBalance();
            }
        });
        $rootScope.$on("cps:balanceTransferReceiver:click", function (event, res) {
            // Transfer on click
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // ensure that the object does not come as string
            if(ld.isString(res.notify.data)) {
                res.notify.data = JSON.parse(res.notify.data);
            }
            var notify = res.notify.data,
                notifyData = notify.notifyData,
                payload = ld.get(notifyData, "payload") || {};

            if(notifyData && ld.isObject(notifyData)) {
                $Mobile.alert(_(notify.localeId + "Full", {
                    by_user: payload.by_user || payload.user && payload.user.name ||
                        payload.to && payload.to.name || "",
                    date: notify.dateFormat || notifyData.date || new Date(),
                    cu: payload.to.cu,
                    amount: $Numbro.formatCurrency(payload.amount) || 0
                }), function () {
                    // Something on click
                });
            }
        });

        // Recharge
        $rootScope.$on("cps:balanceRechargeReceiver", function (event, notifyData) {
            // Transfer
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // Update balance
            if(ld.has(notifyData, "payload.balance")) {
                $Balance.setBalance({
                    action: "new",
                    balances: notifyData.payload.balance
                });
            }
        });
        $rootScope.$on("cps:balanceRechargeReceiver:click", function (event, res) {
            // Recharge on click
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // ensure that the object does not come as string
            if(ld.isString(res.notify.data)) {
                res.notify.data = JSON.parse(res.notify.data);
            }
            var notify = res.notify.data,
                notifyData = notify.notifyData,
                payload = ld.get(notifyData, "payload") || {};

            if(ld.isObject(notifyData)) {
                $Mobile.alert(_(notify.localeId + "Full", {
                    date: notify.dateFormat || notifyData.date || new Date(),
                    amount: $Numbro.formatCurrency(payload.amount) || 0
                }), function () {
                    // Something on click
                });
            }
        });

    }

    // Exports
    module.exports = balanceRun;

})();
