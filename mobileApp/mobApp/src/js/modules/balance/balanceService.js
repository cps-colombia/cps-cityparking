/* global module */

// modules/balance/balanceService.js
//
// factory service for balance module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        cryptoJs = require("crypto-js"),
        ld = require("lodash");

    balanceService.$inject = [
        "$rootScope",
        "$timeout",
        "$window",
        "$q",
        "$Mobile",
        "$Logger",
        "$App",
        "$Login",
        "$Socket",
        "$Encrypt",
        "$Response",
        "$Request",
        "$Numbro"
    ];
    function balanceService ($rootScope, $timeout, $window, $q, $Mobile, $Logger,
                            $App, $Login, $Socket, $Encrypt, $Response, $Request, $Numbro) {
        var _ = $Mobile.locale,
            op = $App.ops,
            logger = $Logger.getInstance(),
            userData = $Login.getUserSession,
            userHash = $Login.getUserHash,
            currentBalance = null,
            timeoutSocket;

        // Members
        var Balance = {
            getBalance: getBalance,
            setBalance: setBalance,
            transfer: transfer,
            recharge: rechargeLink,
            rechargePin: rechargePin
        };

        return Balance;

        // Functions
        function getBalance (options) {
            var deferred = $q.defer(),
                balanceStorage = getBalanceStorage();
            options = ld.extend({
                onlyStorage: false,
                background: false
            }, options);

            if(options.format && balanceStorage) {
                balanceStorage.balance = $Numbro.formatCurrency(parseInt(balanceStorage.balance));
            }
            // Corrent storage in promise
            deferred.promise.balances = balanceStorage;

            // Get from socket
            if(!options.onlyStorage) {
                getBalanceReq(options).then(function (data) {
                    setBalance({
                        action: "new",
                        balances: data.balance
                    }).then(function (balances) {
                        deferred.resolve(balances);
                    }, function (errSet) {
                        deferred.reject(errSet);
                    });
                }, function (err) {
                    deferred.reject(err);
                });
            } else {
                deferred.resolve(balanceStorage);
            }

            // Promise
            return deferred.promise;
        }

        function setBalance (objBalance, options) {
            var deferred = $q.defer(),
                balanceStorate = getBalanceStorage() || 0,
                action = {
                    new: "new",
                    sum: "sum",
                    rest: "rest"
                }[objBalance && objBalance.action],
                newObj = {
                    balance: 0,
                    detail: {}
                },
                balance = 0,
                newBalance,
                amount;

            if(!ld.isObject(objBalance)) {
                objBalance = {};
            }

            if(ld.isObject(objBalance.balances)) {
                amount = objBalance.balances.total || objBalance.balances.balance;
                newObj.detail = objBalance.balances;
            } else {
                amount = objBalance.balances;
            }

            // Set default options
            options = angular.extend({
                clean: false
            }, options);

            if(action === "clean") {
                options.clean = true;
            }

            if (!ld.isNotdefined(amount) && action && !options.clean) {
                // Calculate
                amount = parseInt(amount);
                balance = ld.isObject(balanceStorate) ? parseInt(balanceStorate.total) : parseInt(balanceStorate);
                if(!balance) {
                    balance = 0;
                }
                newBalance = action === "new" ? amount : (action === "rest" ? balance - amount : balance + amount);

                // Save
                newObj.balance = parseInt(newBalance); // TODO: DEPRECATED
                newObj.total = parseInt(newBalance);
                $window.localStorage.ub = $Encrypt.aes(newObj);

                // Clean current
                currentBalance = null;
                // Send event
                logger.info("balance", "Set Balance {0}", [JSON.stringify(newObj)]);
                $rootScope.$broadcast("cps:balanceUpdate", newObj);
                deferred.resolve(newObj);
            } else if (options.clean) {
                // Remove
                $window.localStorage.removeItem("ub");
                newObj.balance = 0;
                // Clean current
                currentBalance = null;
                logger.debug("balance", "Clean Balance");
                $rootScope.$broadcast("cps:balanceUpdate", newObj);
                deferred.resolve(newObj);
            } else {
                logger.warn("balance", "setBalance need object with amount and action 'rest', 'sum' or 'new'");
                deferred.reject($Response.make(500));
            }

            // Promise
            return deferred.promise;
        }

        /**
         * transfer
         *
         * This function submit event the socket
         * and this is connected to the mob waiting for an answer,
         * the best way is to direct
         * @param {string} event event name, on or emit
         * @param {object} dataObject params to send
         *
         * @return {function} call function event type
         */
        function transfer (event, dataObject) {
            var eventType = {
                emit: function emitFun () {
                    return transferSend(dataObject);
                },
                on: function onFun () {
                    return transferRes(dataObject);
                }
            };

            return eventType[event]();
        }

        function rechargeLink (dataObject) {
            $Mobile.loading("show");
            var rechargeURL,
                deferred = $q.defer(),
                req = { // Request recharge link params
                    params: {
                        op: op.balanceRechargeLink,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            angular.extend(req.params, dataObject);

            $Request.http(req).then(function (res) {
                var statusCode = res.data ? res.data.statusCode : null,
                    resData = res.data ? res.data.data : {};
                logger.debug("recharge", JSON.stringify(res.data));

                $Mobile.loading("hide");

                if(statusCode === 200) {
                    rechargeURL = encodeURI(resData.url);
                    window.open(rechargeURL, "_system");
                    deferred.resolve(res);
                } else if(statusCode === 300) {
                    $Mobile.alert(_("alertBalanceRechargeError"));
                    deferred.reject(res);
                } else if(statusCode === 301) {
                    $Mobile.alert(_("alertBalanceRechargeMax"));
                    deferred.reject(res);
                } else if(statusCode === 310) {
                    $Mobile.alert(_("alertBalanceRechargePending"), function () {
                        if(resData.url) {
                            rechargeURL = encodeURI(resData.url);
                            window.open(rechargeURL, "_system");
                        }
                    });
                    deferred.reject(res);
                }
            });
            return deferred.promise;
        }

        function rechargePin (dataObj) {
            var deferred = $q.defer(),
                req = { // Payment history Params
                    params: {
                        op: op.balanceRechargePin,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            $Mobile.loading("show");

            // Extend
            angular.extend(req.params, dataObj);


            // get update data
            $Request.http(req).then(function (res) {
                var resData = res.data || {},
                    statusCode = resData.statusCode || null,
                    statusMsg = {};

                logger.debug("balance", "Recharge pin: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    $Mobile.alert(_("rechargePinSuccess"));
                    $rootScope.$emit("cps:rechargePin:success", resData.data);
                    // Success promise
                    setBalance({action: "new", balances: resData.data.balance});
                    deferred.resolve(resData);
                } else {
                    statusMsg = {
                        321: "rechargeInvalidPin"
                    };

                    $Mobile.alert(_(statusMsg[statusCode] || statusMsg[520]));
                    deferred.reject(resData);
                }
            });
            // Promise
            return deferred.promise;
        }

        // Private
        function getBalanceStorage () {
            if(!currentBalance) {
                if($window.localStorage.ub) {
                    currentBalance = $Encrypt.decrypt($window.localStorage.ub);
                } else {
                    currentBalance = null;
                }
            }
            return currentBalance;
        }

        function getBalanceReq (options) {
            var deferred = $q.defer();

            $Socket.emit("$cps:balance:get", null, function (resBalance) {
                if(resBalance && parseInt(resBalance.statusCode) === 200 && resBalance.data) {
                    logger.debug("balance", "Socket response: {0}", [JSON.stringify(resBalance)]);
                    deferred.resolve(resBalance.data);
                } else {
                    logger.warn("balance", "Socket response: {0}", [JSON.stringify(resBalance)]);
                    deferred.reject(resBalance);
                }
            }, options);

            // Promise
            return deferred.promise;
        }


        function transferSend (dataObj) {
            // Request tranfer params
            var segTimeout = 60,
                req = {
                    from_identifier: userData().phone
                };


            dataObj.repass = cryptoJs.SHA1(dataObj.repass).toString();
            dataObj.approved_token = cryptoJs.HmacSHA1(
                userData().cu + "&" + userData().login, dataObj.repass
            ).toString(cryptoJs.enc.Base64);

            angular.extend(req, ld.pick(dataObj, [
                "approved_token",
                "from_identifier",
                "to_identifier",
                "amount"
            ]));
            if(userData().phone == dataObj.to_identifier ||
               userData().cu == dataObj.to_identifier ||
               userData().login == dataObj.to_identifier) {
                $Mobile.alert(_("transferSameUser"));
                return;
            }

            $Mobile.loading("show");
            $Socket.emit("$cps:balanceTransfer:send", req);

            // Set TimeOut
            timeoutSocket = $timeout(function () {
                $Mobile.loading("hide");
                $Mobile.alert(_("alertTimeoutTransfer"));
                logger.debug("transfer", "transfer timeout after {0}", [1000 * segTimeout]);
            }, (1000 * segTimeout));
        }

        function transferRes (res) {
            var deferred = $q.defer(),
                statusCode = res ? parseInt(res.statusCode) : null,
                resData = res.data || {},
                success = false,
                newBalance;

            // Clear timeout
            $timeout.cancel(timeoutSocket);

            $Mobile.loading("hide");
            logger.info("tranfer", JSON.stringify(res));


            if(statusCode === 220 || statusCode === 200) {
                success = true;
                // Update Balance
                newBalance = resData.sys_user ? resData.sys_user.balance :
                    resData.from && resData.from.balance;

                if(newBalance) {
                    setBalance({
                        action: "new",
                        balances: newBalance
                    });
                } else {
                    getBalance();
                }
                $Mobile.alert(_("transferSuccess", _("alertGiveOk")));
                deferred.resolve(res);

            } else if(statusCode === 212 || statusCode === 101 || statusCode == 404) {
                $Mobile.alert(_("transferInvalidIdentifier", _("alertInvalidCu")));

            } else if(statusCode === 213) {
                $Mobile.alert(_("transferInvalidApprove", _("alertInvalidData")));

            } else if (statusCode === 211) {
                $Mobile.alert(_("transferNotBalance", _("alertNoBalance")));

            } else if(statusCode === 409) {
                $Mobile.alert(_("transferConflict"));
            }

            if(!success) {
                deferred.reject(res);
            }
            // Promise
            return deferred.promise;
        }

    }

    // Exports
    module.exports = balanceService;

})();
