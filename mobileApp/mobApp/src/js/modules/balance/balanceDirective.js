/* global module */

// app/modules/balance/balanceDirective.js
//
// directives function for balance module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    function balanceTransferForm () {
        var balanceTransferDirective = {
            restrict: "E",
            templateUrl: "app/views/balanceTransferForm.html"
        };
        return balanceTransferDirective;
    }

    function rechargeLinkForm () {
        var rechargeLinkDirective = {
            restrict: "E",
            transclude: true,
            templateUrl: "app/views/balanceRechargeForm.html"
        };
        return rechargeLinkDirective;
    }

    function rechargePinForm () {
        var rechargePinDirective = {
            restrict: "E",
            transclude: true,
            templateUrl: "app/views/rechargePinForm.html"
        };
        return rechargePinDirective;
    }


    balanceUser.$inject = ["$rootScope", "$Balance", "$Loading"];
    function balanceUser ($rootScope, $Balance, $Loading) {
        var balanceUserDirective = {
            restrict: "E",
            template: "<h2 class='title'>" +
                "<span locale-id='currentbalance'>Current Balance</span><span>:</span> " +
                "<span ng-if='balance.current !== null' class='calm user-balance' cps-format=':currency' " +
                "cps-format-value='{{balance.current}}'></span>" +
                "<a ng-if='showReload' hm-tap='getBalance()' class='icon ion-refresh secondary'></a>" +
                "</h2>",
            controller: "BalanceController",
            controllerAs: "balance",
            compile: compile
        };
        return balanceUserDirective;

        // Function
        function compile (tElement, tAttrs) {
            tElement.addClass("user-balance");

            return {
                pre: preLink,
                post:postLink
            };
        }

        function preLink (scope, element, attrs) {
            scope.spinner = $Loading.spinner({
                scope: scope,
                appendTo: element.children(),
                template: "<cps-spinner icon='dots'></cps-spinner>"
            });
        }

        function postLink (scope, element, attrs, ctrl) {
            var onBalance = ld.noop;
            ctrl.current = null;
            scope.showReload = false;
            scope.showBalance = false;
            scope.getBalance = getBalance;

            // Update balance
            scope.getBalance({background: true});
            onBalance = $rootScope.$on("cps:balanceUpdate", function (event, newBalance) {
                // Update balance
                ctrl.current = newBalance.total;
                scope.showReload = false;
                spinner("hide");

            });

            // Clean prevent memory leaks
            element.on("$destroy", function () {
                scope.$destroy();
            });
            scope.$on("$destroy", function () {
                onBalance();
            });

            function getBalance (options) {
                spinner("show");
                scope.showReload = false;

                $Balance.getBalance(options).then(function () {
                    scope.showReload = false;
                    spinner("hide");
                }).catch(function (err) {
                    scope.showReload = true;
                    spinner("hide");
                });
            }

            function spinner (action) {
                if(ctrl.current) {
                    action = "hide";
                }
                if(scope.spinner && scope.spinner[action]) {
                    scope.spinner[action]();
                }
            }
        }


    }

    // Exports
    module.exports = {
        balanceTransferForm: balanceTransferForm,
        rechargeLinkForm: rechargeLinkForm,
        rechargePinForm: rechargePinForm,
        balanceUser: balanceUser
    };

})();
