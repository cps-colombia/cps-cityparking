/* global require */

// module/balance/index.js
//
// index function for balance
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        balanceController = require("./balanceController"),
        balanceDirective = require("./balanceDirective"),
        balanceRoutes = require("./balanceRoutes"),
        balanceRun = require("./balanceRun"),
        balanceService = require("./balanceService");

    angular.module("cpsBalance", []);
    var cpsBalance = angular.module("cpsBalance");

    // Controller
    cpsBalance.controller("BalanceController", balanceController);

    // Services
    cpsBalance.factory("$Balance", balanceService);

    // Routes
    cpsBalance.config(balanceRoutes);

    // Run
    cpsBalance.run(balanceRun);

    // Directive
    cpsBalance.directive("cpsBalanceTransferForm", balanceDirective.balanceTransferForm);
    cpsBalance.directive("cpsBalanceRechargeForm", balanceDirective.rechargeLinkForm); // TODO: DEPRECATED
    cpsBalance.directive("cpsRechargeLinkForm", balanceDirective.rechargeLinkForm);
    cpsBalance.directive("cpsRechargePinForm", balanceDirective.rechargePinForm);
    cpsBalance.directive("cpsBalanceUser", balanceDirective.balanceUser);
})();
