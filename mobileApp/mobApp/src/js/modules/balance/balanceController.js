/* global require */

// modules/balance/balanceController.js
//
// Balance modules for cps app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    BalanceController.$inject = [
        "$q",
        "$rootScope",
        "$scope",
        "$App",
        "$Logger",
        "$Mobile",
        "$Encrypt",
        "$Balance",
        "$Dialog",
        "$Socket"
    ];
    function BalanceController ($q, $rootScope, $scope, $App, $Logger, $Mobile, $Encrypt, $Balance, $Dialog, $Socket) {
        var _ = $Mobile.locale,
            balance = this;

        // Members
        balance.transfer = transfer;
        balance.transferDialog = transferDialog;
        balance.recharge = rechargeLink;
        balance.rechargePin = rechargePin;
        balance.rechargeDialog = rechargeDialog;
        balance.getBalance = $Balance.getBalance;

        // Transfer Params
        balance.dataTransfer = {
            to_identifier: "",
            amount: "",
            repass: ""
        };

        // Recharge Params
        balance.dataRecharge = {
            value: ""
        };
        balance.dataPin = {
            pin: null
        };

        // Current Balance
        balance.current = null;

        // Control
        balance.formSuccess = false;

        // Event
        $scope.$on("cps:balanceTransfer:success", function (event, res) {
            // if(event.defaultPrevented) {
            //     return false;
            // }
            // event.preventDefault();
            // Clean form
            balance.formSuccess = true;
            // Close dialog
            if($scope.transferDialog) {
                $scope.transferDialog.close();
            }
        });

        $scope.$on("cps:balanceUpdate", function (event, newBalance) {
            // Update balance
            balance.current = newBalance.total;
        });

        $scope.$on("$destroy", function () {
            // Clean events
            $scope.$$listeners["cps:balanceTransfer:success"] = [];
            $scope.$$listeners["cps:balanceUpdate"] = [];
        });

        // Functions
        function transfer () {
            // Send transfer
            var dataTransfer = angular.copy(balance.dataTransfer);

            if(angular.isString(dataTransfer.amount)) {
                dataTransfer.amount = dataTransfer.amount.replace(/[^0-9]+/g, "");
            }

            $Balance.transfer("emit", dataTransfer);
        }

        function transferDialog (scope) {
            var deferred = $q.defer(),
                attrScope = scope,
                dialog;

            scope = scope || $scope;

            dialog = $Dialog.show({
                title: _("transfer"),
                templateUrl: "app/views/balanceTransferForm.html",
                scope: scope,
                buttons: [{
                    text: _("cancel"),
                    isClose: true,
                    hold: true,
                    onTap: function (e) {
                        return {data: {close: true}, dialog: dialog};
                    }
                }]
            });

            scope.transferDialog = dialog;

            // // When $Dialog close
            dialog.then(function (close) {
                scope.transferDialog = null;
                if(close) {
                    // Clean
                    if(attrScope) {
                        scope.$destroy();
                    }
                    deferred.reject(close);
                } else {
                    deferred.resolve(close);
                }
            });

            // Promise
            return deferred.promise;
        }

        function rechargeLink () {
            var dataRecharge = angular.copy(balance.dataRecharge);
            if(angular.isString(dataRecharge.value)) {
                dataRecharge.value = dataRecharge.value.replace(/[^0-9]+/g, "");
            }
            $Balance.recharge(dataRecharge).then(function () {
                balance.formSuccess = true;
            });
            if($scope.rechargeDialog) {
                $scope.rechargeDialog.close();
            }
        }

        function rechargePin () {
            var dataRecharge = angular.copy(balance.dataPin);
            if(angular.isString(dataRecharge.pin)) {
                dataRecharge.pin = dataRecharge.pin.replace(/[^0-9]+/g, "");
            }
            $Balance.rechargePin(dataRecharge).then(function () {
                balance.formSuccess = true;
                if($scope.rechargeDialog) {
                    $scope.rechargeDialog.close();
                }
            });
        }

        function rechargeDialog (type, scope) {
            var template = type == "pin" ? "<cps-recharge-pin-form></cps-recharge-pin-form>" :
                    "<cps-recharge-link-form></cps-recharge-link-form>",
                deferred = $q.defer(),
                attrScope = scope,
                dialog;

            scope = scope || $scope;

            dialog = $Dialog.show({
                title: _("recharge"),
                template: template,
                scope: scope,
                buttons: [{
                    text: _("cancel"),
                    isClose: true,
                    hold: true,
                    onTap: function (e) {
                        return {data: {close: true}, dialog: dialog};
                    }
                }]
            });

            scope.rechargeDialog = dialog;

            // // When $Dialog close
            dialog.then(function (close) {
                scope.rechargeDialog = null;
                balance.formSuccess = true;
                if(close) {
                    // Clean
                    if(attrScope) {
                        scope.$destroy();
                    }
                    deferred.reject(close);
                } else {
                    deferred.resolve(close);
                }
            });

            // Promise
            return deferred.promise;
        }
    }

    // Exports
    module.exports = BalanceController;

})();
