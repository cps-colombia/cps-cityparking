/* global require */

// modules/maps/mapsPlacesService.js
//
// Places service for maps module
// This service is responsible for get places information and autocomplete
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const mapsLoader = require("google-maps");

    // Global
    var logger = null;

    class MapsPlacesService {

        constructor ($q, $Logger) {
            logger = $Logger.getInstance();

            this.$q = $q;

            this.google = null;
            this.autoCompleteApi = null;
        }

        googleApi () {
            let deferred = this.$q.defer();

            mapsLoader.load((google) => {
                if(!this.google) {
                    this.google = google;
                }

                deferred.resolve(google);
            });
            return deferred.promise;
        }

        autoComplete () {
            let deferred = this.$q.defer();

            this.googleApi().then((google) => {
                if(!this.autoCompleteApi) {
                    this.autoCompleteApi = new google.maps.places.AutocompleteService();
                }

                deferred.resolve(this.autoCompleteApi);
            }, deferred.reject);

            return deferred.promise;
        }

        places (input) {
            let deferred = this.$q.defer();

            this.googleApi().then((google) => {
                let placesApi = new google.maps.places.PlacesService(input);

                deferred.resolve(placesApi);
            }, deferred.reject);

            return deferred.promise;
        }

        getPlaceDetails (opts, input) {
            let deferred = this.$q.defer();

            this.places(input).then((place) =>  {
                place.getDetails(opts, (placeDetails, placesServiceStatus) => {
                    if (placesServiceStatus == "OK") {
                        deferred.resolve(placeDetails);
                    } else {
                        deferred.reject(placesServiceStatus);
                    }
                });
            });

            return deferred.promise;
        }

        getPlacePredictions (opts) {
            let deferred = this.$q.defer();

            this.autoComplete().then((api) => {
                api.getPlacePredictions(opts, (predictions, status) => {
                    if (status == this.google.maps.places.PlacesServiceStatus.OK) {

                        deferred.resolve(predictions);
                    } else {
                        logger.warn("maps", status);
                        deferred.reject(status);
                    }
                });
            });

            return deferred.promise;
        }
    }

    // Exports
    MapsPlacesService.$inject = [
        "$q",
        "$Logger"
    ];
    module.exports = MapsPlacesService;

})();
