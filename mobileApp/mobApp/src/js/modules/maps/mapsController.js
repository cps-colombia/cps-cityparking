/* global require */

// modules/maps/mapsController.js
//
// Maps modules for cps app
// This module is responsible for creating
// maps for different application components
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    MapsController.$inject = ["$scope", "$rootScope", "$interval", "$Maps"];
    function MapsController ($scope, $rootScope, $interval, $Maps) {
        var maps = this;

        // Members
        maps.container = $scope.container;
        maps.markersOpen = $Maps.markersOpen;
        maps.buttonTapped = buttonTapped;
        maps.markerOpen = {};

        // Events
        $rootScope.$on("$routeChangeStart", function (event, next) {
            $Maps.setMarkerOpen(null, "clean");
        });
        $scope.$watch(function () {
            return $Maps.markerOpen() && $Maps.markerOpen().markId;
        }, function (newVal, oldVal) {
            if(newVal != (maps.markerOpen && maps.markerOpen.markId)) {
                maps.markerOpen = $Maps.markerOpen();
            }
        });

        $scope.$on("$destroy", function () {
            // Clean info window data
            if($scope.markId) {
                cleanInfoWindow($scope);
            }
        });

        // Function
        function buttonTapped (button, $event) {
            if(button.name) {
                $rootScope.$emit("cps:mapsMarkerClick:" + ld.camelCase(button.name), {
                    button: button,
                    $event: $event,
                    marker: $scope.marker
                });
            }
        }

        function cleanInfoWindow (infoWin) {
            $interval.cancel(infoWin.timer);
            $Maps.setMarkerOpen(infoWin, "remove");

            if (infoWin.listener) {
                $Maps.removeEventListener(infoWin.marker, "click", infoWin.listener);
                infoWin.listener = null;
            }

            infoWin.timer = null;
            infoWin.marker = null;
            infoWin.scope = null;
            infoWin.markId = null;
            infoWin.beforeRetry = 0;
            infoWin.beforePoint = [];
            infoWin.removeInfoWnd = ld.noop;

            if(infoWin.element && infoWin.element.remove) {
                infoWin.element.remove();
                infoWin.element = null;
            }
            if(infoWin.info && infoWin.info.css) {
                infoWin.info.removeClass("active");
                infoWin.info.css({
                    "z-index": "0",
                    "display": "none"
                });
                infoWin.info = null;
            }
            infoWin.infoModal = null;

            if(infoWin.map &&  infoWin.map.refreshLayout) {
                infoWin.map.refreshLayout();
            }
            infoWin.map = null;
        }
    }

    // Exports
    module.exports = {
        MapsController: MapsController
    };

})();
