/* global require */

// modules/maps/mapsService.js
//
// Maps service for maps module
// This service is responsible for creating
// maps for different application components
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        asyncJs = require("async"),
        mapsLoader = require("google-maps"),
        validator = require("validator"),
        ld = require("lodash"),
        $$ = angular.element;

    mapsService.$inject = [
        "$q",
        "$rootScope",
        "$timeout",
        "$interval",
        "$document",
        "$window",
        "$App",
        "$Mobile",
        "$Logger",
        "$Login",
        "$MapsRoutes",
        "$Geo",
        "$Dom",
        "$Body",
        "$Template",
        "$Modal"
    ];
    function mapsService ($q, $rootScope, $timeout, $interval, $document, $window,
                          $App, $Mobile, $Logger, $Login, $MapsRoutes, $Geo, $Dom, $Body, $Template, $Modal) {
        var _ = $Mobile.locale;
        var firstMarks = true;
        var isMapsLoaded = false;
        var isMapsReady = false;
        var showPosition = false;
        var hasUserMarker = false;
        var config = $App.config;
        var logger = $Logger.getInstance();
        var mapsOptions = ld.defaultsDeep(config.options && config.options.maps || {}, {
            jsMap: false,
            debug: false,
            routesEnabled: false,
            routesColor: "#AA00FF",
            routesLimit: 3,
            infoWindow: "infoWindow",
            infoButtons: [{
                name: "payment",
                text: "payment",
                localeId: "payment",
                type: "button-secondary"
            }],
            infoAnimated: false,
            infoType: "html",
            watchPosition: true,
            googleApi: {
                key: "AIzaSyACRvH5kxcYQ3IIUXnx5Nsx-AFQ3HcktYA",
                version: "3.8",
                client: null,
                language: null,
                region: null,
                libraries: []
            }
        });
        var flag = "static/images/cps-flag.png";
        var userFlag = "static/images/cps-flag-user.png";
        var flagType = {
            1: "static/images/cps-flag-type1.png",
            2: "static/images/cps-flag-type2.png",
            3: "static/images/cps-flag-type3.png",
            default: "static/images/cps-flag.png"
        };
        var userMarkData = {
            icon : userFlag,
            markId: 0,
            category: "user",
            title: _("youAreHere"),
            locale: "youAreHere",
            infoClick: function (marker) {
                marker.hideInfoWindow();
            },
            markerClick: function (marker) {
                var locale = marker.get("locale"),
                    title = marker.get("title");

                if(title && locale && marker.setTitle) {
                    marker.setTitle(_(locale, null, title));
                }
                marker.showInfoWindow();
            }
        };
        var readyCallbacks = [];
        var loadedCallbacks = [];
        var userMarkerPosition = {};
        var mapIntance = {};
        var markersOpen = [];
        var mapPolyline = [];
        var visibleCategory = ["zone", "user"];
        var targets = [];
        var markers = {};
        var userMarkerLatLng;
        var refreshTimer;
        var mapSpinner;
        var cpsMaps;
        var cpsBounds;

        // Events
        $rootScope.$on("cps:network:online", function (event, type) {
            if(event.defaultPrevented) {
                return false;
            }
            if(!isMapsLoaded && !isMapsSDK())  {
                logger.debug("maps", "Try to init maps after network online");
                initMaps();
            }
        });

        // Memebers
        var Maps = {
            options: mapsOptions,
            ready: mapsReady,
            onLoad: mapsLoaded,
            isSDK: isMapsSDK,
            initMaps: initMaps,
            loadMap: loadMap,
            removeMap: removeMap,
            marksMap: marksMap,
            marksRemove: marksRemove,
            markerOpen: markerOpen,
            markersOpen: markersOpen,
            boundsMap: boundsMap,
            routesMap: routesMap,
            panTo: panTo,
            geocoder: geocoder,
            watchPosition: watchPosition,
            setZoom: setZoom,
            getZoom: getZoom,
            setVisible: setVisible,
            setClickable: setClickable,
            refreshLayout: refreshLayout,
            setMarkerOpen: setMarkerOpen,
            addEventListener: addEventListener,
            removeEventListener: removeEventListener,
            hasInstance: hasInstance,
            setSpinner: setSpinner,
            spinner: spinner
        };

        $window.maps = getIntanceMap;
        return Maps;

        // Functions
        function mapsReady (cb) {
            // run taks when complete maps create
            $App.ready(function () {
                if (isMapsReady) {
                    cb();
                } else {
                    // or append callback
                    readyCallbacks.push(cb);
                }
            });
        }

        function mapsLoaded (cb) {
            // run through tasks to complete now that the app and maps is loaded
            if (isMapsLoaded) {
                cb();
            } else {
                // the maps isn't loaded yet, add it to this array
                // which will be called once the maps is loaded
                loadedCallbacks.push(cb);
            }
        }

        function isMapsSDK () {
            var platform = $App.getPlatform().platform;

            if($App.isIOS() && $App.getPlatform().version >= "9.2") {
                return false;
            }
            return !(mapsOptions.jsMap || (mapsOptions[platform] && mapsOptions[platform].jsMap)) && $App.isCordova();
        }

        function initMaps () {
            var deferred = $q.defer();

            $App.ready(function () {
                logger.info("maps", "Init Maps");
                // js api options
                mapsLoader.KEY = mapsOptions.googleApi.key;
                mapsLoader.VERSION = mapsOptions.googleApi.version;
                mapsLoader.CLIENT = mapsOptions.googleApi.client;
                mapsLoader.LIBRARIES = mapsOptions.googleApi.libraries;
                mapsLoader.LANGUAGE = mapsOptions.googleApi.language;
                mapsLoader.REGION = mapsOptions.googleApi.region;

                if(isMapsSDK()) {
                    logger.debug("maps", "Use google maps sdk");
                    cpsMaps = $window.plugin.google.maps;
                    cpsMaps.Map.isAvailable(function (isAvailable, message) {
                        if (isAvailable) {
                            onMapsLoaded();
                            deferred.resolve();
                        } else {
                            mapsOptions.jsMap = true;
                            $Body.addClass("maps-js");
                            $Body.removeClass("maps-sdk");
                            logger.debug("maps", "Error with SDK Map " + message);
                            mapsLoader.load(function (google) {
                                logger.debug("maps", "Fallback Load google maps lib js");
                                cpsMaps = google.maps;
                                onMapsLoaded();
                                deferred.resolve();
                            });
                        }
                    });
                } else {
                    mapsLoader.load(function (google) {
                        logger.debug("maps", "Load google maps lib js");
                        cpsMaps = google.maps;
                        onMapsLoaded();
                        deferred.resolve();
                    });
                }
            });
            return deferred.promise;
        }

        /**
         * @name loadMap
         * @description load map when sdk or js lib was loaded success
         *
         * @param {string} container id element to append map
         * @param {object} options for cerate map container like zoom, center
         * see #createMap
         *
         * @return {promise} with map instance
         */
        function loadMap (container, options, from) {
            var deferred = $q.defer();
            isMapsReady = false;

            if(!isMapsLoaded) {
                logger.debug("maps", "Load map without init, force init maps");
                initMaps();
            }
            mapsLoaded(function () {
                // $Dom.animationFrameThrottle(function(){
                createMap(container, options).then(function (map) {
                    // wait full load map
                    $timeout(function () {
                        onMapsReady(map);
                        // Watch user position
                        watchPosition(mapsOptions.watchPosition);

                        deferred.resolve(map);
                    }, 300);
                });
                // });
            });
            return deferred.promise;
        }

        /**
         * @name removeMap
         * @description remove map from the DOM
         */
        function removeMap () {
            var map = getIntanceMap();
            if(map && map.remove) {
                map.remove();
            }
            mapIntance = {};
        }

        /**
         * @name marksMap
         * @description add user, custom or clutter markers
         *
         * @param {object} options An options object with the following properties:
         - `{string=}` `markerType` which type of marker you need.
         *    Default: 'cluster'
         *
         *
         * @return {promise} with map instance
         */
        function marksMap (options) {
            var deferred = $q.defer();
            var response = [];

            if(!ld.isArray(options)) {
                options = [options];
            }
            mapsReady(function () {
                asyncJs.each(options, function (markOpts, next) {

                    if(ld.isObject(markOpts)) {
                        if(markOpts.markerType === "user" && ld.has(markOpts, "position")) {
                            setUserMarker({coords: {
                                latitude: markOpts.position.lat || markOpts.position.latitude,
                                longitude: markOpts.position.lng || markOpts.position.longitude
                            }}, markOpts).then(function (res) {
                                response.push(res);
                                next();
                            }).catch(function (err) {
                                next(err);
                            });
                        } else if(markOpts.markerType === "custom") {
                            addMarker(ld.extend(markOpts, {
                                position: new cpsMaps.LatLng(
                                    (markOpts.position.lat || markOpts.position.latitude),
                                    (markOpts.position.lng || markOpts.position.longitude)
                                )
                            })).then(function (res) {
                                response.push(res);
                                next();
                            }).catch(function (err) {
                                next(err);
                            });
                        } else {
                            clusterMarks(markOpts).then(function (res) {
                                response.push(res);
                                next();
                            }).catch(function (err) {
                                next(err);
                            });
                        }
                    } else {
                        next({statusText: "markMap need object with options", status: 500});
                    }
                }, function (err) {
                    if(err) {
                        deferred.reject(err);
                        logger.warn("maps", err);
                    } else {
                        response = response.length > 1 ? response[0] : response;
                        deferred.resolve(response);
                    }
                });
            });
            // Promise
            return deferred.promise;
        }

        function marksRemove (id, category) {
            var marker = getMarker(id, category);

            if(marker) {
                removeMarker(marker, id);
            }
        }

        function markerOpen () {
            return ld.last(markersOpen);
        }

        function boundsMap (options) {
            var deferred = $q.defer();
            var _targets = [];
            var hasBounds = false;
            var position;
            var bounds;
            var map;

            mapsReady(function () {
                bounds = options.bounds || new cpsMaps.LatLngBounds();

                options = ld.extend({
                    type: "target",
                    lastBounds: false
                }, options);
                map = options.map || getIntanceMap();

                // By type
                if(options.type === "target") {
                    if(options.limit && targets.length > options.limit && userMarkerLatLng) {
                        hasBounds = true;
                        _targets.push(userMarkerLatLng);
                        bounds.extend(userMarkerLatLng);
                    } else {
                        ld.forEach(targets, function (target, key) {
                            hasBounds = true;
                            _targets.push(target);
                            bounds.extend(target);
                        });
                    }
                } else if(options.type === "visible") {
                    ld.forEach(markers, function (value, key) {
                        ld.forEach(value, function (marker, key) {
                            if(marker && marker.get && isMarkerVisible(marker)) {
                                hasBounds = true;
                                position = marker.get("position");
                                _targets.push(position);
                                bounds.extend(position);
                            }
                        });
                    });
                } else if(options.type === "last") {
                    options.lastBounds = true;
                } else if(options.bounds) {
                    hasBounds = true;
                }

                // set bounds
                if(!hasBounds && cpsBounds) {
                    bounds = cpsBounds;
                    hasBounds = true;
                }

                if(hasBounds) {
                    fitBounds(bounds, map, {
                        lastBounds: options.lastBounds,
                        zoom: options.zoom,
                        withAnimate: !options.padding,
                        forceReturn: isMapsSDK() && options.padding
                    }, function (res) {
                        if(options.zoom && !options.padding) {
                            setZoom(options.zoom, map);
                        }
                        if(ld.isArray(options.panBy)) {
                            map.panBy(ld.first(options.panBy), ld.last(options.panBy));
                        }
                        if(options.padding) {
                            boundsPadding(map, bounds, options.padding, options);
                        }
                        deferred.resolve(res);
                    });
                } else {
                    deferred.reject();
                }
            });
            // Promise
            return deferred.promise;
        }

        function routesMap (dataRoute, options) {
            var deferred = $q.defer();
            var response = [];

            if(!ld.isArray(dataRoute)) {
                dataRoute = [dataRoute];
            }

            // Options
            options = ld.extend({
                clean: false
            }, options);

            if(options.clean) {
                cleanRoutes();
            }

            // task
            asyncJs.each(dataRoute, function (dataObj, callback) {
                if(ld.has(dataObj, "from") && ld.has(dataObj, "to")) {
                    $MapsRoutes.getCoordinates(ld.pick(dataObj, [
                        "from", "to"
                    ])).then(function (coordsData) {
                        addRoutes(coordsData, ld.extend(options, dataObj.options)).then(function (poly) {
                            response.push({polyline: poly, coord: coordsData});
                            callback();
                        }, callback);
                    }, callback);
                } else {
                    callback({statusText: "routesMap need object with coordinates", status: 500});
                }
            }, function (err) {
                if(err) {
                    logger.warn("maps", "routesMap error: {0}", [JSON.stringify(err)]);
                    deferred.reject(err);
                } else {
                    deferred.resolve(response);
                }
            });

            // Promise
            return deferred.promise;
        }

        function panTo (map, latLng, callback) {
            var listener;

            map = map || getIntanceMap();
            callback = callback || ld.noop;

            if(isMapsSDK()) {
                map.getCameraPosition(function (camera) {
                    map.animateCamera({
                        target: latLng,
                        zoom: camera.zoom,
                        tilt: camera.tilt,
                        duration: 1000,
                        bearing: camera.bearing
                    }, function () {
                        callback();
                    });
                });
            } else {
                map.panTo(latLng);
                map.panBy(0, -100);
                listener = addEventListener(map, "idle", function () {
                    removeEventListener(map, "idle", listener);
                    callback();
                });
            }
        }

        function geocoder (request, limit) {
            var deferred = $q.defer();
            var geocode = isMapsSDK() ? cpsMaps.Geocoder.geocode : new cpsMaps.Geocoder().geocode;

            if(ld.isObject(request)) {
                if(ld.has(request, "parse")) {
                    let isArray = true;
                    if(!ld.isArray(request.parse)) {
                        request.parse = [request.parse];
                        isArray = false;
                    }
                    let results = parseGeocoder(request.parse, limit);
                    deferred.resolve(isArray ? results : results[0]);
                    return deferred.promise;
                }
                if(ld.has(request, "position") || ld.has(request, "coords")) {
                    let position = request.position || request.coords;
                    let param = isMapsSDK() ? "position" : "location";

                    request[param] = {
                        lat: ld.isFunction(position.lat) ?
                            position.lat() : (position.lat || position.latitude),
                        lng: ld.isFunction(position.lng) ?
                            position.lng() : (position.lng || position.longitude)
                    };
                    request = ld.pick(request, [param]);
                }
                if(ld.has(request, "address") && ld.isString(request.address)) {
                    request.address = request.address.replace(/N°|#|No\b/g, ",");
                }
                mapsReady(function () {
                    geocode(request, function (results, status) {
                        if (results && results.length) {
                            results = parseGeocoder(results, limit);
                            deferred.resolve(results);
                        } else {
                            deferred.reject(results);
                        }
                    });
                });
            } else {
                deferred.reject({statusText: "geocoder need object with lat and lng", status: 500});
                logger.debug("maps", "geocoder need object with lat and lng");
            }
            // Promise
            return deferred.promise;
        }

        function watchPosition (isActive, options) {
            options = ld.extend({
                forceEnables: false
            }, options);

            if(isActive && (mapsOptions.watchPosition || options.forceEnables)) {
                $Geo.watchPosition(setUserMarker);
            } else {
                $Geo.clearWatch();
            }
        }

        function setZoom (level, instance) {
            var map = instance || getIntanceMap();
            map.setZoom(level);
        }
        function getZoom () {
            var args = arguments;
            var callback = ld.noop;
            var map = typeof args[0] === "object" ? args[0] : getIntanceMap();
            var zoom;

            callback = typeof args[0] === "function" ? args[0] :
                (typeof args[1] === "function" ? args[1] : ld.noop);

            if(isMapsSDK()) {
                map.getZoom(function (resZoom) {
                    zoom = resZoom;
                    callback(zoom);
                });
            } else {
                zoom = map.getZoom();
                callback(zoom);
            }
        }

        function setVisible (isVisible, category, options) {
            var deferred = $q.defer();
            var map = getIntanceMap();
            options = ld.extend({onlyMarker: false}, options);
            category = category || "zone";

            // clane and set
            setMarkerOpen(null, "clean");
            setMarkerVisible(isVisible, category).then(function () {
                if(!options.onlyMarker) {
                    setRoutesVisible(isVisible);
                }

                refreshLayout(map, options.delay);
                deferred.resolve();
            });
            // Promise
            return deferred.promise;
        }

        function setClickable (isClickable, options) {
            var map = getIntanceMap();
            options = ld.extend({}, options);
            if(map && map.setClickable) {
                map.setClickable(isClickable);
                // refreshLayout(map, options.delay);
            }
        }

        function refreshLayout (map, delay) {
            map = map || getIntanceMap();
            delay = delay || 10;

            $timeout.cancel(refreshTimer);

            if(map && map.refreshLayout) {
                $Dom.requestAnimationFrame(function () {
                    refreshTimer = $timeout(function () {
                        map.refreshLayout();
                    }, delay);
                });
            }
        }

        function setMarkerOpen (scope, action) {
            var markIndex;
            if(scope && scope.markId && action !== "clean") {
                markIndex = ld.findIndex(markersOpen, {markId: scope.markId});
                if(markIndex > -1 && action === "remove") {
                    ld.pullAt(markersOpen, markIndex);
                } else if(markIndex > -1) {
                    markersOpen[markIndex] = scope;
                } else {
                    markersOpen.push(scope);
                }
            } else if(action === "clean") {
                ld.forEach(markersOpen, function (mkScope, index) {
                    if(mkScope && ld.isFunction(mkScope.removeInfoWnd)) {
                        mkScope.removeInfoWnd(null, true);
                    }
                    ld.pullAt(markersOpen, index);
                });
            }
        }

        function addEventListener (instance, event, callback) {
            if(!instance || !callback) {
                return;
            }
            if(isMapsSDK()) {
                instance.addEventListener(event, callback);
                return callback;
            } else if(instance.addListener) {
                return instance.addListener(event, callback);
            } else {
                return cpsMaps.event.addListener(instance, event, callback);
            }
        }

        function removeEventListener (instance, event, listener, callback) {
            callback = callback || ld.noop;
            instance = instance || getIntanceMap();

            if(!instance) {
                return;
            }
            if(isMapsSDK()) {
                instance.removeEventListener(event, listener);
                callback();
            } else {
                if(event && listener) {
                    cpsMaps.event.removeListener(listener);
                } else if(event) {
                    cpsMaps.event.clearListeners(instance, event);
                } else {
                    cpsMaps.event.clearInstanceListeners(instance);
                }
                callback();
            }
        }

        function hasInstance () {
            return ld.isEmpty(getIntanceMap()) ? false : true;
        }

        function setSpinner (_spinner) {
            if(_spinner && _spinner.show) {
                mapSpinner = _spinner;
            }
        }
        function spinner (action, options) {
            var _spinner = mapSpinner || $Mobile.spinner();
            if(action) {
                _spinner[action](options);
            }
            return _spinner;
        }

        /**
         * @private
         */

        function onMapsLoaded () {
            // the maps is all set to go, init our own stuff then fire off our event
            isMapsLoaded = true;
            setMarkerOpen(null, "clean");

            for (var x = 0; x < loadedCallbacks.length; x++) {
                // fire off all the callbacks that were added before the maps was loaded
                loadedCallbacks[x]();
            }
            loadedCallbacks = [];
        }

        function onMapsReady (map) {
            // run callback after maps create
            isMapsReady = true;
            setMarkerOpen(null, "clean");
            for (var x = 0; x < readyCallbacks.length; x++) {
                // fire off all the callbacks
                readyCallbacks[x](map);
            }
            readyCallbacks = [];
        }

        function createMap (containerName, options) {
            var deferred = $q.defer();
            var staticPosition = $Login.staticPosition();
            var lngUser = staticPosition.coords.longitude;
            var latUser = staticPosition.coords.latitude;
            var initCord = new cpsMaps.LatLng(latUser, lngUser);
            var container;
            var mapDiv;
            var map;

            // User options
            options = ld.extend(mapsOptions, options);

            $Dom.animationFrameThrottle(function () {
                container = $document[0].getElementById(containerName);
                createInfoWindow(container);

                if(hasInstance()) {
                    // Not reposition maps if exist
                    initCord = null;
                }

                if(isMapsSDK()) {
                    mapDiv = container;
                    map = getMap(mapDiv, {
                        backgroundColor: "white",
                        mapType: cpsMaps.MapTypeId.ROADMAP,
                        controls: {
                            compass: true,
                            myLocationButton: false,
                            indoorPicker: true,
                            zoom: true // Only for Android
                        },
                        gestures: {
                            scroll: true,
                            tilt: true,
                            rotate: true,
                            zoom: true
                        },
                        camera: {
                            latLng: initCord,
                            tilt: 15,
                            zoom: 15,
                            bearing: 50
                        }
                    });
                    // You have to wait the MAP_READY event.
                    map.addEventListener(cpsMaps.event.MAP_READY, function () {
                        onCreate();

                        deferred.resolve(map);
                        logger.debug("Create maps sdk");
                    });
                } else {
                    $$(container).addClass("maps-fallback");
                    mapDiv = $document[0].getElementById(containerName + "-fallback");
                    map = getMap(mapDiv, {
                        zoom: 15,
                        center: initCord,
                        zoomControl: true,
                        mapTypeControl: false,
                        scaleControl: false,
                        streetViewControl: false,
                        overviewMapControl: false,
                        navigationControl: false,
                        navigationControlOptions: {
                            style: ld.get(cpsMaps, "MapTypeControlStyle.HORIZONTAL_BAR") || "",
                            position: ld.get(cpsMaps, "ControlPosition.TOP_RIGHT") || ""
                        },
                        mapTypeId: cpsMaps.MapTypeId.ROADMAP
                    });
                    onCreate();

                    deferred.resolve(map);
                    logger.debug("Create maps js");
                }
            });
            // Internal
            function onCreate () {
                setInstanceMap(map);
                if(initCord) {
                    map.setCenter(initCord);
                } else {
                    boundsMap({type: "last"});
                }
                ld.forEach(markers, function (value, key) {
                    ld.forEach(value, function (marker, key) {
                        if(marker && marker.setMap && isMarkerVisible(marker)) {
                            marker.setMap(map);
                        }
                    });
                });
                if(map.setDebuggable && mapsOptions.debug) {
                    map.setDebuggable(mapsOptions.debug);
                }
            }

            return deferred.promise;
        }

        // --------
        // Basic
        // --------
        function getMap (mapDiv, options) {
            var _map;

            if(isMapsSDK()) {
                _map = cpsMaps.Map.getMap(mapDiv, options);
            } else {
                _map = new cpsMaps.Map(mapDiv, options);
            }
            return _map;
        }

        function getIntanceMap () {
            return mapIntance;
        }

        function setInstanceMap (map) {
            mapIntance = map;
        }

        function cleanTargets () {
            targets = [];
        }

        function fromLatLngToPoint (LatLng, callback) {
            var map = getIntanceMap();
            var point = {};

            if(isMapsSDK()) {
                map.fromLatLngToPoint(LatLng, function (resPoint) {
                    point.x = resPoint[0];
                    point.y = resPoint[1];
                    callback(point);
                });
            } else {
                point = map.getProjection().fromLatLngToPoint(LatLng);
                callback(point);
            }
        }

        function fromPointToLatLng (point, callback) {
            var map = getIntanceMap();
            var LatLng = {};

            if(isMapsSDK()) {
                map.fromPointToLatLng([point.x, point.y], function (resLatLng) {
                    LatLng.lat = resLatLng[0];
                    LatLng.lng = resLatLng[1];
                    callback(LatLng);
                });
            } else {
                LatLng = map.getProjection().fromPointToLatLng(point);
                callback(LatLng);
            }
        }

        function fitBounds (bounds, map, options, callback) {
            var setBounds = bounds;
            var withTarget = false;
            var camAction = "animateCamera";
            var isSDK = isMapsSDK();
            var target;
            var listener;
            var isTarget;

            callback = callback || ld.noop;
            // Set options
            options = ld.extend({
                tilt: 60,
                zoom: 15,
                duration: 5000,
                withAnimate: true,
                forceBounds: false,
                lastBounds: false
            }, ld.pick(options, ld.identity));

            mapsReady(function () {
                map = map || getIntanceMap();
                if(bounds && (bounds.lat && bounds.lng)) {
                    isTarget = bounds;
                    bounds = new cpsMaps.LatLngBounds(bounds);
                }
                if(options.lastBounds || !bounds) {
                    setBounds = cpsBounds || bounds;
                }
                if(bounds) {
                    cpsBounds = bounds;
                }
                withTarget = (targets.length <= 1 || isTarget) && !options.forceBounds;
                target = withTarget ? (isTarget || targets[0]) : setBounds;

                if(options.forceReturn) {
                    boundsCallback();
                    return;
                }
                // By type
                if(isSDK) {
                    if(!options.withAnimate) {
                        camAction = "moveCamera";
                    }
                    map[camAction]({
                        target: target,
                        tilt: options.tilt,
                        zoom: options.zoom,
                        duration: options.duration
                    }, function () {
                        boundsCallback();
                    });
                } else {
                    if(withTarget) {
                        map.setZoom(15);
                        panTo(map, target, function () {
                            boundsCallback();
                        });
                    } else {
                        map.fitBounds(target);
                        listener = addEventListener(map, "bounds_changed", function () {
                            removeEventListener(map, "bounds_changed", listener);
                            boundsCallback();
                        });
                    }
                }
            });

            // Internal
            function boundsCallback () {
                var args =  {
                    map: map,
                    target: target,
                    asBouns: !withTarget,
                    isSDK: isSDK
                };
                $rootScope.$emit("cps:map:bounds", args);
                callback(args);
            }
        }

        function boundsPadding (map, bounds, xy, options, callback) {
            var isSDK = isMapsSDK();
            var southwest;
            var offsetx;
            var offsety;
            var point2;

            map = map || getIntanceMap();
            if(!map || !bounds || !xy) {
                return;
            }
            options = ld.extend({zoom: 12}, options);
            callback = callback || ld.noop;
            offsetx = xy[0];
            offsety = xy[1];

            southwest = bounds.getSouthWest && bounds.getSouthWest() || bounds.southwest;
            fromLatLngToPoint(southwest, function (point1) {
                getZoom(map, function (zoom) {
                    point2 = {
                        x: ((typeof(offsetx) === "number" ? offsetx : 0) / Math.pow(2, zoom)) || 0,
                        y: ((typeof(offsety) === "number" ? offsety : 0) / Math.pow(2, zoom)) || 0
                    };
                    fromPointToLatLng({
                        x: (point1.x - point2.x),
                        y: (point1.y + point2.y)
                    }, function (newPoint) {
                        bounds.extend(newPoint);
                        fitBounds((isSDK ? newPoint : bounds), map, {
                            zoom: (isSDK ? options.zoom : null)
                        }, function () {
                            if(isSDK) {
                                map.panBy((offsetx / 4), (offsety / 4));
                            }
                            callback();
                        });
                    });
                });
            });
        }

        function addPolyline (map, options, callback) {
            var polyline;
            if(!map) {
                logger.warn("maps", "addPolyline need valid map instance");
                return;
            }
            callback = callback || ld.noop;

            if(isMapsSDK()) {
                map.addPolyline(options, function (poly) {
                    polyline = poly;
                    callback(polyline);
                });
            } else {
                polyline = new cpsMaps.Polyline(ld.extend({
                    path : options.points,
                    geodesic : options.geodesic,
                    strokeColor : options.color,
                    strokeWeight : options.width
                }, options));
                polyline.setMap(map);
                callback(polyline);
            }
        }

        // --------
        // Markers
        // --------
        function clusterMarks (options) {
            var deferred = $q.defer();
            var map = getIntanceMap();
            var bounds = new cpsMaps.LatLngBounds();
            var resArgs = {};
            var pointsRoutes = [];
            var boundsCount = 0;
            var success = false;
            var userPosition;
            var points;
            var idzone;
            var email;
            var places;
            var price;
            var status;
            var schedule;
            var head;
            var info;
            var contentText;
            var contentHtml;
            var branchType;
            var position;
            var latitude;
            var longitude;
            var LatLng;
            var userLatLng;
            var i;

            // Extend
            options = ld.extend({
                points: [],
                category: "zone",
                showPosition: false,
                showRoutes: false,
                showInfo: false,
                cleanRoutes: true,
                boundsZones: true
            }, options);
            showPosition = options.showPosition;
            points = options.points;

            // Check if valid points
            if(!ld.isArray(points)) {
                deferred.reject({statusText: "points should be array", sttaus: 500});
                logger.warn("maps", "points should be array");
                return deferred.promise;
            }

            // Clean
            if(firstMarks) {
                cleanMarkers(map);
            } else {
                cleanTargets();
            }

            if(options.showInfo && points.length > 1) {
                // Not allow milti infowWindow yet
                options.showInfo = false;
            }

            // Show user position
            userPosition = $Geo.getUserPosition();
            if(showPosition && ld.has(userPosition, "coords")) {
                // Set user marker
                setUserMarker(userPosition);
                userLatLng = new cpsMaps.LatLng(userPosition.coords.latitude, userPosition.coords.longitude);

                // Set user target
                boundsCount++;
                targets.push(userLatLng);
                bounds.extend(userLatLng);
            }

            // Routes
            if(!mapsOptions.routesEnabled) {
                options.showRoutes = false;
            }
            if(mapsOptions.routesEnabled && options.cleanRoutes) {
                cleanRoutes();
            }
            // Limit bounds
            if(!options.boundsLimit) {
                options.boundsLimit = points.length;
            }

            // Create
            for (i = 0; i < points.length; i++) {
                position = points[i].position ? points[i].position.coordinates.coordinates[0] : null;
                latitude = position ? position.y : points[i].latitude || null;
                longitude = position ? position.x : points[i].longitude || null;

                idzone = points[i].idzone ? points[i].idzone : points[i].idbranch;
                idzone = validator.isNumeric(idzone) ? idzone : (idzone ? idzone.replace(/[^0-9]/g, "") : "");

                if (latitude === "0" || longitude === "0" || !latitude || !longitude) {
                    logger.debug("maps", "position null zone: {0}", [idzone + " - " + points[i].name]);
                } else {
                    success = true;
                    LatLng = new cpsMaps.LatLng(latitude, longitude);
                    targets.push(LatLng);

                    if(i <= options.boundsLimit && options.boundsZones) {
                        boundsCount++;
                        bounds.extend(LatLng);
                    }

                    // Check info data
                    price = points[i].minuteValue || points[i].price;
                    status = ld.has(points[i], "online") ?
                        (points[i].online === true || points[i].online === "true" ?
                         "online" : "offline") : null;
                    places = points[i].places ? (validator.isNumeric(points[i].places) ?
                                                 points[i].places : _(points[i].places)) : null;
                    email = points[i].email ?
                        points[i].email : (points[i].name && points[i].name.toLowerCase().replace(/\s/g, "") || null);
                    schedule = points[i].schedule && points[i].schedule.replace(/(?:\\[rn])+/g, "<br />");
                    if(email && !validator.isEmail(email)) {
                        email = email + "@" + config.clientDomain;
                    }

                    // Set flag type
                    if(points[i].commerceBranchType) {
                        branchType = points[i].commerceBranchType.idtype;
                        flag = flagType[branchType] || flagType["default"];
                    }

                    // Create Content Info Window
                    head = points[i].name.toUpperCase();

                    if(mapsOptions.infoType === "default") {
                        info = _("idzone") + ": " + idzone + "\n" +
                            (price ? _("price") + ": " + price + "\n" : "") +
                            (status ? _("status") + ": " + status + "\n" : "") +
                            (places ? _("places") + ": " + places + "\n" : "") +
                            _("address") + ": " + points[i].address + "\n" +
                            (email ? _("email") + ": " + email + "\n" : "") +
                            (schedule ? _("schedule") + ": " + schedule + "\n" : "");
                        contentText = head + "\n\n" + info;
                    } else {
                        price = price ? "<span class='infowin-note infowin-price'>" +
                            _("price") + ": <span>" + price + "</span></span>" : "";
                        status = status ? "<span class='infowin-note infowin-status'>" +
                            _("status") + ": <span locale-id='" + status + "' " +
                            "ng-class=\"{online: 'balanced', offline: 'assertive'}['" + status + "']\">" +
                            status + "</span></span>" : "";
                        info = "<span class='infowin-value row infowin-idzone'><span class='description col col-30'>" +
                        _("idzone") + ":</span> <span class='value col'>" + idzone + "</span></span>" +
                        (points[i].openTime && points[i].closeTime ?
                         "<span class='infowin-value row infowin-opentime'><span class='description col col-30'>" +
                         _("schedule") + ":</span> <span class='value col'>" +
                         points[i].openTime + " - " + points[i].closeTime + "</span></span>" : "") +
                        (places ?
                         "<span class='infowin-value row infowin-places'><span class='description col col-30'>" +
                         _("places") + ":</span> <span class='value col'>" + places + "</span></span>" : "") +
                        (points[i].address ?
                         "<span class='infowin-value row infowin-address'><span class='description col col-30'>" +
                         _("address") + ":</span> <span class='value col'>" +
                         points[i].address + "</span></span>" : "") +
                        (email ?
                         "<span class='infowin-value row infowin-email'><span class='description col col-30'>" +
                         _("email") + ":</span> <span class='value col'>" + email + "</span></span>" : "") +
                        (schedule ?
                         "<span class='infowin-value row infowin-schedule'><span class='description col col-30'>" +
                         _("schedule") + ":</span> <span class='value col'>" + schedule + "</span></span>" : "") +
                        (points[i].description ?
                         "<span class='infowin-value row infowin-description'>" +
                         "<span class='description col col-30'>" +
                         _("description") + ":</span> <span class='value col'>" + points[i].description +
                         "</span></span>" : "");

                        contentHtml = "<div class='infowin-content content padding'>" +
                            "<h3 class='title'>" + head + "</h3>" + price + status +
                            "<div class='infowin-text'>" + info + "</div>" +
                            "</div>";
                    }


                    // Set marker
                    addMarker({
                        position : LatLng,
                        markId: idzone,
                        icon : flag,
                        // in ios undefined or null convert to string and android empty sctring open native infowindow
                        title: contentText || ($App.isIOS() ? "" : contentText),
                        zone: points[i],
                        category: options.category,
                        contentHtml: contentHtml,
                        showInfo: options.showInfo,
                        styles: {
                            "maxWidth": "300"
                        },
                        infoClick: function (marker) {
                            marker.hideInfoWindow();
                        }
                    }, openInfoWindow);

                    // Add routes
                    if(!ld.isEmpty(userMarkerPosition) && options.showRoutes && i <= mapsOptions.routesLimit) {
                        pointsRoutes.push({from: {
                            lat: userMarkerPosition.latitude,
                            lng: userMarkerPosition.longitude
                        }, to: {
                            lat: latitude,
                            lng: longitude
                        }, options: {
                            markId: idzone,
                            color: lightenDarkenColor(mapsOptions.routesColor, -(i * 50))
                        }});
                    }
                }


            }
            // Get Route if have one
            if(pointsRoutes.length >= 1) {
                routesMap(pointsRoutes);
            }

            // Bounds
            if(boundsCount > 0) {
                // Prevent blue map if no have bounds
                boundsMap({
                    bounds: bounds,
                    map: map,
                    type: options.type,
                    lastBounds: options.lastBounds
                });
            }

            // Response
            if(success || userLatLng) {
                if(firstMarks) {
                    firstMarks = false;
                }
                resArgs = {
                    first: firstMarks,
                    bounds: bounds,
                    boundsCount: boundsCount,
                    targets: targets
                };
                $rootScope.$emit("cps:map:markerUpdate", resArgs);
                deferred.resolve(resArgs);
            } else {
                deferred.reject("Nothing to show, point wrong");
            }

            // Promise
            return deferred.promise;
        }

        function addMarker (options, callback) {
            var deferred = $q.defer();
            var map = getIntanceMap();
            var markPath = [(options.category || "zone"), (options.markId || 0)];
            var markById;
            var jsMarker;

            if (!map) {
                logger.warn("maps", "addMarker Invalid map instance");
                deferred.reject({statusText: "Invalid map instance", status: 500});
                return deferred.promise;
            }
            callback = callback || openInfoWindow;

            // create category
            if (!markers[options.category]) {
                markers[options.category] = {};
            }
            if (visibleCategory.indexOf(options.category) <= -1) {
                options.visible = false;
            }

            // remove if exist
            markById = getMarker(markPath);
            if (markById) {
                removeMarker(markById);
                markById = getMarker(markPath);
            }

            // set markers
            if (!markById) {
                if (isMapsSDK()) {
                    options.icon = "www/" + options.icon; // Fix url to skd map
                    options.disableAutoPan = false; // Enable auto pan
                    setMarker({status: "wait"}, markPath); // wait callback

                    map.addMarker(options, function (marker) {
                        addEventListener(marker, "click", function () {
                            marker.getPosition(function (latLng) {
                                panTo(map, latLng);
                            });
                            callback(marker);
                        });
                        if (options.showInfo) {
                            openInfoWindow(marker, true);
                        }
                        setMarker(marker, markPath);

                        deferred.resolve(marker);
                    });
                } else {
                    jsMarker = new cpsMaps.Marker(options);

                    jsMarker.setMap(map);
                    addEventListener(jsMarker, "click", function () {
                        panTo(map, jsMarker.getPosition());
                        callback(jsMarker);
                    });
                    if (options.showInfo) {
                        openInfoWindow(jsMarker, true);
                    }
                    setMarker(jsMarker, markPath);

                    deferred.resolve(jsMarker);
                }
            } else {
                deferred.resolve(markById);
            }

            // Promise
            return deferred.promise;
        }

        function getMarker (id, category) {
            var path = ld.isArray(id) ? id : [(category || "zone"), (id || 0)];
            return ld.get(markers, path);
        }

        function setMarker (marker, id, category) {
            var path = ld.isArray(id) ? id : [(category || "zone"), (id || 0)];
            ld.set(markers, path, marker);
        }

        function isMarkerVisible (marker) {
            var isVisible = true;
            if(marker) {
                if(marker.isVisible) {
                    isVisible = marker.isVisible();
                } else if(marker.getVisible) {
                    isVisible = marker.getVisible();
                }
            }
            return isVisible;
        }

        function markerLatLngToPoint (marker, callback) {
            var map = marker && marker.getMap && marker.getMap();
            var position = marker && marker.get && marker.get("position");
            var getProjection;
            var overlay;
            var point;

            callback = callback || ld.noop;
            if(!map || !position) {
                logger.warn("markerLatLngToPoint need valid marker instance");
                callback(null);
                return;
            }

            if(isMapsSDK()) {
                map.fromLatLngToPoint(position, callback);
            } else {
                overlay = new cpsMaps.OverlayView();
                overlay.draw = ld.noop;
                overlay.setMap(map);
                getProjection = overlay.getProjection();

                if(getProjection) {
                    point = getProjection.fromLatLngToContainerPixel(position);
                    if(point.x && point.y) {
                        point = [point.x, point.y];
                    }
                }
                callback(point);
            }
        }

        function cleanMarkers (map, category) {
            var marker = ld.get(markers, (category || "zone"));

            ld.forEach(marker, function (mark, key) {
                removeMarker(mark, key);
            });
            cleanTargets();
        }

        function setMarkerVisible (isVisible, category) {
            var deferred = $q.defer();

            // set visible category for future marks
            if (isVisible) {
                visibleCategory.push(category);
                ld.uniq(visibleCategory);
            } else {
                ld.remove(visibleCategory, function (value) {
                    return value == category;
                });
            }
            // set visibility for current marks
            asyncJs.each(markers[category], function (mark, next) {
                if(mark.setVisible) {
                    mark.setVisible(isVisible);
                }
                next();
            }, function () {
                deferred.resolve();
            });
            return deferred.promise;
        }

        function removeMarker (mark, id) {
            var category =  mark.get && mark.get("category");
            var markId = id || mark.get && mark.get("markId");

            if(!isMapsSDK()) {
                if(mark.setMap) {
                    mark.setMap(null);
                }
            } else {
                if(mark.remove) {
                    mark.remove();
                }
            }
            if(markers[category] && mark.status !== "wait") {
                delete markers[category][markId];
            }
        }

        function getUserMarker () {
            var userMarker = ld.get(markers, "user");
            return userMarker && ld(userMarker).values().first();
        }

        function setUserMarker (position, options) {
            var map = getIntanceMap();
            var deferred = $q.defer();
            var userMarker = getUserMarker();
            var newPosition = position.coords;
            var logMsg = "";

            if(!map) {
                logger.warn("maps", "setUserMarker Invalid maps instance");
                deferred.reject({statusText: "Invalid maps instance", status: 500});
                return deferred.promise;
            }

            options = ld.extend({
                boundsZones: false
            }, options);

            if(userMarkerPosition.latitude !== newPosition.latitude ||
               userMarkerPosition.longitude !== newPosition.longitude) {

                userMarkerPosition = ld.extend(userMarkerPosition, newPosition);
                userMarkerLatLng = new cpsMaps.LatLng(newPosition.latitude, newPosition.longitude);

                if(userMarker) {
                    // Update user position
                    if(userMarker.setPosition) {
                        userMarker.setPosition(userMarkerLatLng);
                        if(options.boundsZones) {
                            fitBounds(userMarkerLatLng, map, options);
                        }
                        deferred.resolve(userMarker);
                    } else {
                        logMsg = "userMarker no has method 'setPosition'";
                        logger.warn("maps", logMsg);
                        deferred.reject({statusText: logMsg, status: 500});
                    }
                    // FIXME: incorrect bouns animation
                    // if(showPosition && cpsBounds){
                    //     // Update cam position
                    //     cpsBounds.extend(LatLng);
                    //     fitBounds(map, cpsBounds);
                    // }
                } else if(!hasUserMarker) {
                    // Set user marker
                    hasUserMarker = true;
                    mapsReady(function () {
                        logger.debug("maps", "start set userMarker");

                        addMarker(ld.extend({
                            position : userMarkerLatLng
                        }, userMarkData)).then(function (marker) {
                            fitBounds(userMarkerLatLng, map, ld.extend(options, {
                                withAnimate:false
                            }));
                            logger.info("maps", "set userMarker success");

                            deferred.resolve(marker);
                        }).catch(function (err) {
                            hasUserMarker = false;
                            deferred.reject(err);
                        });
                    });
                } else {
                    logMsg = "userMarker has active 'addMarker'";
                    logger.warn("maps", logMsg);
                    deferred.reject({statusText: logMsg, status: 429});
                }
            } else {
                deferred.resolve(userMarker);
            }

            // Promise
            return deferred.promise;
        }

        // -------
        // Routes
        // -------
        function addRoutes (coordsData, options) {
            var deferred = $q.defer();
            var map = getIntanceMap();
            var coordinates = coordsData.coordinates;
            var arrayPoints = [];
            var marker, i;

            for (i = 0; i < coordinates.length; i++) {
                arrayPoints.push(new cpsMaps.LatLng(
                    coordinates[i][1], coordinates[i][0]));
            }

            if(mapPolyline.length >= mapsOptions.routesLimit) {
                cleanRoutes(ld.first(mapPolyline));
            }

            options = ld.extend({
                points: arrayPoints,
                color : mapsOptions.routesColor,
                width: 5,
                geodesic: true,
                markId: null
            }, options);

            addPolyline(map, options, function (polyline) {
                if(options.markId && polyline && polyline.setVisible) {
                    marker = getMarker(options.markId);
                    marker.isVisible = marker.isVisible || marker.getVisible;

                    if(marker && marker.isVisible) {
                        polyline.setVisible(marker.isVisible());
                    }
                }
                mapPolyline.push(polyline);

                deferred.resolve(polyline);
            });

            // Promise
            return deferred.promise;
        }

        function setRoutesVisible (isVisible, category) {
            ld.forEach(mapPolyline, function (polyline, key) {
                if(polyline.setVisible) {
                    polyline.setVisible(isVisible);
                }
            });
        }

        function cleanRoutes (polyObj) {
            if(mapPolyline.length) {

                if(polyObj) {
                    if(polyObj.remove) {
                        polyObj.remove();
                    } else if(polyObj.setMap) {
                        polyObj.setMap(null);
                    }
                    ld.pullAt(mapPolyline, 0);
                } else {
                    ld.forEach(mapPolyline, function (polyline, key) {
                        if(isMapsSDK()) {
                            polyline.remove();
                        } else {
                            polyline.setMap(null);
                        }
                    });
                    mapPolyline = [];
                }
            }
        }

        // -----------
        // InfoWindow
        // -----------
        function openInfoWindow (marker, force) {
            var infoWindow;
            var isOpen = ld.find(markersOpen, {markId: marker.get("markId")});

            if(!(marker && marker.getMap && marker.get)) {
                logger.warn("maps", "openInfoWindow need valid marker object");
                return;
            }

            if(force) {
                setMarkerOpen(null, "clean");
                isOpen = false;
            }
            if(isOpen || !marker.get("contentHtml")) {
                // if already open nothing happen
                return;
            } else if(mapsOptions.infoType === "default") {

                if(isMapsSDK()) {
                    infoWindow = marker;
                    infoWindow.showInfoWindow();
                } else {
                    infoWindow = new cpsMaps.InfoWindow({maxWidth: 300});
                    infoWindow.setContent(marker.get("title"));
                    infoWindow.open(marker.getMap(), marker);
                }
            } else {
                // Open custom infoWindow
                infoWindow = htmlInfoWindow(marker);
            }
        }

        function htmlInfoWindow (marker) {
            var INFOWIN_TPL;
            var scope;

            // Check if valid data
            if(!(marker && marker.getMap)) {
                logger.warn("maps", "htmlInfoWindow need valid marker object");
                return;
            }

            if(markersOpen.length >= 1) {
                // Prevent memory leak, gosth intervals
                setMarkerOpen(null, "clean");
            }

            // Create scope
            scope = $rootScope.$new();

            // Control data
            scope.beforeRetry = 0;
            scope.beforePoint = [];


            // Get parent element
            scope.info = $$("#" + mapsOptions.infoWindow);

            // Set current marker
            scope.markId = marker.get("markId");

            // Get map marker instance
            scope.marker = marker;
            ld.extend(scope.marker, {
                infoType: mapsOptions.infoType,
                close: closeInfoWnd
            });
            scope.map = scope.marker.getMap();

            scope.removeInfoWnd = removeInfoWnd;

            if (mapsOptions.infoType === "modal") {
                INFOWIN_TPL = "<div class='infowin-modal' ng-controller='MapsController as maps'>" +
                    "<div class='infowin-body infowin-primary'>" +
                    "<div hm-tap='removeInfoWnd($event, true)'>" + (marker.get("contentHtml") || "") + "</div>" +
                    "<div class='infowin-buttons padding' ng-show='buttons.length'>" +
                    "<button ng-repeat='button in buttons' hm-tap='maps.buttonTapped(button, $event)' " +
                    "class='button' ng-class='button.type || \"button-default\"' " +
                    "locale-id='{{button.localeId}}' ng-bind-html='button.text || button.localeId'></button>" +
                    "</div>" +
                    "</div></div>";

                $Modal.fromTemplate(INFOWIN_TPL, {
                    id: "mapsInfowinModal",
                    title: _("mapsInfowinTitle", marker.get("zone"), ld.startCase(marker.get("zone").name || "")),
                    scope: scope,
                    closeBtn: true,
                    removeOnHide: true,
                    onClose: () => {
                        closeInfoWnd();
                    }
                }).then((modal) => {
                    modal.show();
                    // Extend scope data
                    scope.infoModal = modal;

                    scope = ld.extend(scope, {
                        container: mapsOptions.infoWindow,
                        buttons: mapsOptions.infoButtons
                    });
                    setMarkerOpen(scope);
                });
            } else {
                // Remove previous instance
                scope.info.find(".infowin-frame").remove();

                // Insert content
                INFOWIN_TPL = "<div class='infowin infowin-frame'>" +
                    "<button hm-tap='removeInfoWnd($event, true)' " +
                    "class='infowin-close button button-small icon ion-close'></button>" +
                    "<div class='infowin-body infowin-primary'>" +
                    "<div hm-tap='removeInfoWnd($event, true)'>" + (marker.get("contentHtml") || "") + "</div>" +
                    "<div class='infowin-buttons padding' ng-show='buttons.length'>" +
                    "<button ng-repeat='button in buttons' hm-tap='maps.buttonTapped(button, $event)' " +
                    "class='button' ng-class='button.type || \"button-default\"' " +
                    "locale-id='{{button.localeId}}' ng-bind-html='button.text || button.localeId'></button>" +
                    "</div>" +
                    "</div></div>";

                // Load and compile template
                $Template.compile({
                    template: INFOWIN_TPL,
                    scope: scope,
                    controller: "MapsController as maps",
                    appendTo: "#" + mapsOptions.infoWindow
                }).then(function (template) {
                    // Extend scope data
                    scope.element = template.element;
                    scope = ld.extend(scope, {
                        container: mapsOptions.infoWindow,
                        buttons: mapsOptions.infoButtons
                    });

                    // Run
                    setInfoWindowPosition(scope, true);
                    // $$("#" + mapsOptions.infoWindow).append(frame);

                    // Timer position
                    scope.timer =  $interval(function () {
                        setInfoWindowPosition(scope);
                    }, 200);
                    setMarkerOpen(scope);

                    // // Events
                    // scope.info.on("click", function(event){
                    //     removeInfoWnd(event, true);
                    // });
                    // frame.one("remove", function(event){
                    //     removeInfoWnd(event, true);
                    // });
                    scope.listener = addEventListener(scope.marker, "click", scope.removeInfoWnd);
                });
            }

            // Return instance
            return scope;

            // Functions
            function removeInfoWnd (event, force) {
                var isOpen = ld.find(markersOpen, {markId: scope.marker.get("markId")});
                force = ld.isBoolean(event) ? event : force;

                if(scope.marker && isOpen && !force) {
                    return;
                }
                if(scope && scope.$destroy) {
                    scope.$destroy();
                }
            }
            function closeInfoWnd () {
                removeInfoWnd(true);
            }
        }

        function setInfoWindowPosition (scope, isInit) {
            if(isInit) {
                if(scope.info && scope.info.css && scope.info.addClass) {
                    $timeout(function () {
                        scope.info.addClass("active");
                        if(mapsOptions.infoAnimated) {
                            scope.info.addClass("info-animated");
                        }
                        scope.info.css({
                            "z-index": "1",
                            "display": "block"
                        });
                    }, 100);
                }
                if(scope.map && scope.map.refreshLayout) {
                    scope.map.refreshLayout();
                }
            }
            markerLatLngToPoint(scope.marker, function (point) {
                if(point) {
                    if (scope.beforePoint[0] !== point[0] || scope.beforePoint[1] !== point[1]) {
                        if(scope.info && scope.info.css) {
                            scope.info.css({
                                "left": Math.abs(point[0]) - 108,
                                "top": Math.abs(point[1]) - 180
                            });
                        }
                        // Update the children position.
                        if(mapsOptions.infoAnimated) {
                            $Dom.animationComplete(scope.info[0]).then(function () {
                                if(scope.map && scope.map.refreshLayout) {
                                    scope.map.refreshLayout();
                                }
                            });
                        } else {
                            if(scope.map && scope.map.refreshLayout) {
                                scope.map.refreshLayout();
                            }
                        }
                    }
                    scope.beforePoint = point;
                } else {
                    scope.beforeRetry++;
                    if(scope.beforeRetry >= 3) {
                        if(scope.removeInfoWnd) {
                            scope.removeInfoWnd(null, true);
                        }
                    }
                }
            });
        }

        function createInfoWindow (container) {
            var div;
            var child;
            if(container) {
                if(!$document[0].getElementById(mapsOptions.infoWindow)) {
                    div = document.createElement("div");
                    div.id = mapsOptions.infoWindow;
                    div.style.display = "none";
                    div.style.zIndex = "0";
                    div.className += "infowin";
                    child = container.getElementsByTagName("div");
                    if(child && child[0]) {
                        child[0].appendChild(div);
                    } else {
                        container.insertBefore(div, container.firstChild);
                    }
                } else {
                    div = $document[0].getElementById(mapsOptions.infoWindow);
                    div.style.display = "none";
                    div.style.zIndex = "0";
                    div.className = div.className.concat(" infowin");
                }
            }
        }

        // ---------
        // Geocoder
        // ---------
        function parseGeocoder (results, max) {
            var isAddress = /[^\w\d]*(([0-9]+.*[A-Za-z]+.*)|[A-Za-z]+.*([0-9]+.*))/;
            var hasDuplicate = /(\b\w+\b)\W+\1/;
            var match;

            return ld.map(ld.take(results, (max || 3)), function (point) { // take only some result, better performance
                if(point.extra && point.extra.lines) {
                    point.address_line = ld.first(ld.reduce(point.extra.lines, function (lines, value, index) {
                        if(value && isAddress.test(value)) {
                            lines.push(value);
                        }
                        return lines;
                    }, []));
                }

                if(!point.formatted_address) {
                    point.formatted_address = ld.reduce([
                        (point.address_line || point.subThoroughfare),
                        (point.address_line ? null : point.thoroughfare),
                        point.subLocality,
                        point.locality,
                        (ld.camelCase(point.adminArea) === ld.camelCase(point.locality) ? null : point.adminArea),
                        point.country
                    ], function (newVal, value, index) {
                        if(value && value !== "(null)") {
                            newVal.push(value);
                        }
                        return newVal;
                    }, []).join(", ");

                } else {
                    match = point.formatted_address.match(hasDuplicate);
                    if(match) {
                        match = match[0].split(",")[0];
                        match = new RegExp("\\s+" + match + ",");
                        point.formatted_address = point.formatted_address.replace(match, "");
                    }
                }
                if(point.geometry) {
                    point.position = {
                        lat: point.geometry.location.lat(),
                        lng: point.geometry.location.lng()
                    };
                }
                return point;
            });
        }
        // ------
        // Other
        // ------
        function lightenDarkenColor (col, amt) {
            var usePound = true;
            var num;
            var r;
            var g;
            var b;

            if (col[0] === "#") {
                col = col.slice(1);
                usePound = true;
            }
            num = parseInt(col,16);

            r = (num >> 16) + amt;
            if(r > 255) {
                r = 255;
            } else if(r < 0) {
                r = 0;
            }

            b = ((num >> 8) & 0x00FF) + amt;
            if (b > 255) {
                b = 255;
            } else if(b < 0) {
                b = 0;
            }

            g = (num & 0x0000FF) + amt;
            if (g > 255) {
                g = 255;
            } else if (g < 0) {
                g = 0;
            }

            return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
        }

    }

    // Exports
    module.exports = mapsService;

})();
