/* global require */

// modules/maps/index.js
//
// index function for maps module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        mapsDirective = require("./mapsDirective"),
        mapsController = require("./mapsController"),
        mapsRoutesService = require("./mapsRoutesService"),
        mapsPlacesService = require("./mapsPlacesService"),
        mapsService = require("./mapsService");

    angular.module("cpsMaps", []);
    var cpsMaps = angular.module("cpsMaps");

    // Service
    cpsMaps.service("$Maps", mapsService);
    cpsMaps.service("$MapsPlaces", mapsPlacesService);
    cpsMaps.factory("$MapsRoutes", mapsRoutesService);

    // Directive
    cpsMaps.directive("cpsMaps", mapsDirective.container);
    cpsMaps.directive("cpsMapsPlaces", mapsDirective.places);

    // Controller
    cpsMaps.controller("MapsController", mapsController.MapsController);

    // OnRun
    cpsMaps.run(cpsMapsRun);

    // Functions
    cpsMapsRun.$inject = ["$Maps"];
    function cpsMapsRun ($Maps) {
        $Maps.initMaps();
    }

})();
