/* global module */

// modules/maps/mapsDirective.js
//
// directives function for maps module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const angular = require("angular");
    const ld = require("lodash");
    const $$ = angular.element;


    function container ($rootScope, $window, $timeout, $Dom, $Mobile,
                       $Menu, $Body, $Backdrop, $Bind, $Maps, $Gestures) {
        var mapsDirective = {
            restrict: "E",
            scope: true,
            templateUrl: "app/views/mapsContainer.html",
            controller: "MapsController",
            controllerAs: "maps",
            replace: true,
            transclude: true,
            compile: compile
        };
        return mapsDirective;

        // Private
        function compile (tElement, tAttrs, transclude) {
            let isSDK = $Maps.isSDK;
            let height = tAttrs.height || "100%";
            let width = tAttrs.width || "100%";
            let bodyClass = "maps-sdk";

            if(!isSDK()) {
                bodyClass = "maps-js";
            }

            tElement[0].style.height = height;
            tElement[0].style.width = width;

            tElement.addClass("cps-maps"); // disabled
            $Body.addClass(bodyClass);

            return {
                pre: preLink,
                post: postLink
            };
        }

        function preLink (scope, element, attrs, ctrl) {
            // Geo spinner
            $Maps.setSpinner($Mobile.spinner({
                id: "mapsSpinner",
                icon: "ripple",
                scope: scope,
                appendTo: $Maps.options.spinnerAppendTo || element,
                startHidden: true
            }));

            // setDisabledMap(element, true);
        }

        function postLink (scope, element, attrs, ctrl) {
            let fixMenu = ld.noop;
            let fixBackdrop = ld.noop;
            let refreshMap = ld.noop;
            let hasInstance = $Maps.hasInstance();
            let timer = hasInstance ? 1 : 300;
            let isMapLoad = false;

            // Bind scope
            $Bind(scope, attrs, {
                infoWindow: "@",
                container: "@"
            });

            // Load maps
            scope.$on("cps:view:afterEnter", (event, data) => {
                // Add class by type
                $$("._gmaps_cdv_").css("background-color", "rgba(0, 0, 0, 0)");

                scope.$evalAsync(() => {
                    $timeout(() => {
                        $Mobile.loading("show");

                        $Maps.loadMap(scope.container, ld.cleanObject({
                            infoWindow: scope.infoWindow
                        })).then(() => {
                            isMapLoad = true;
                            setDisabledMap(element, false);
                            $timeout(function () {
                                $Mobile.loading("hide");
                            }, 200);
                        });
                    }, timer);
                });
            });

            // Watch
            fixBackdrop = scope.$watch(() => {
                return $Backdrop.isOpen({all: true}).length;
            }, (newVal, oldVal) => {
                if(newVal <= 0) {
                    $Maps.setClickable(true);
                } else {
                    $Maps.setClickable(false);
                }
            });
            // Fix transparent background SDK Maps
            fixMenu = scope.$watch(() => {
                return $Menu.isOpen();
            }, (newVal, oldVal) => {
                if(newVal !== oldVal && isMapLoad) {
                    setDisabledMap(element, newVal);
                }
            });

            // Fix resize map
            refreshMap = ld.setDebounce(() => {
                $Maps.refreshLayout();
            }, 10, true);
            $Gestures.on($window, "resize orientationchange", refreshMap);

            // Clean
            scope.$on("$destroy", () => {
                $Gestures.off($window, "resize orientationchange", refreshMap);
                // Remove sdk class
                $Body.removeClass("maps-sdk", "maps-js", "maps-disabled");
                // claen sttyle but keep class when user return to section
                $$("._gmaps_cdv_").css("background-color", "");
                // Clean events
                $Maps.removeEventListener();
                // Clean fix maps
                fixMenu();
                fixBackdrop();
            });
        }

        // Private
        function setDisabledMap (element, _isDisabled) {
            $Maps.setClickable(!_isDisabled, {delay: 200});
            $Body.enableClass(_isDisabled, "maps-disabled");
            element.toggleClass("disabled", _isDisabled);
        }
    }

    function places ($q, $timeout, $document, $Bind, $Geo, $App, $Mobile, $Logger, $Hardware,
                    $UiConfig, $Backdrop, $Template, $Maps, $MapsPlaces) {
        var placesDirective = {
            require: "?ngModel",
            restrict: "E",
            template: "<input type='text' readonly='readonly' class='maps-places' autocomplete='off'>",
            replace: true,
            scope: true,
            link: {
                pre: preLink,
                post: postLink
            }
        };
        var logger = $Logger.getInstance();
        var _ = $Mobile.locale;

        return placesDirective;

        // functions
        function preLink (scope, element, attrs) {
            $Bind(scope, attrs, {
                ngModel: "=?",
                filter: "=",
                $onClose: "&onClose",
                predictionsOptions: "=",
                currentLocation: "@"
            });
        }
        function postLink (scope, element, attrs, ngModel) {
            let unbindBackButtonAction;
            let searchEventTimeout;
            let _element;

            // Opts
            scope.locations = [];
            scope.getClearClass = getClearClass;
            scope.clearQueryInput = clearQueryInput;
            scope.fetchingLocation = fetchingLocation;
            scope.displayCurrentLocation = false;
            scope.currentLocation = scope.currentLocation === "true" ? true : false;
            scope.filter = scope.filter || attrs.pattern || false;

            if($Geo.isEnable() && scope.currentLocation) {
                scope.displayCurrentLocation = true;
            }

            let POPUP_TPL = `<div class='maps-places-container modal ng-hide'>
            <div class='bar bar-header item-input-inset'>
            <label class='item-input-wrapper'>
            <i class='icon ion-search placeholder-icon'></i>
            <input class='maps-places-search' type='search' ng-model='searchQuery'
               ${ attrs.localeId ? "locale-id='" + attrs.localeId + "'" +
                   (attrs.localeArgs ? " locale-args='" + attrs.localeArgs + "'" : "") +
                   (attrs.localeFallback ? " locale-fallback='" + attrs.localeFallback + "'" : "") : "" }
               placeholder='${ attrs.searchPlaceholder || attrs.placeholder || "Enter an address" }'>
               <button class='maps-places-clear button button-icon icon' hm-tap='clearQueryInput($event)'
                    ng-class='getClearClass()'></button>
            </label>
            <button class='button button-clear' ng-click='cancel($event)'>
               ${ attrs.cancelText || "{{'cancel' | cpsGetLocale:'Cancel'}}" }
            </button>
            </div>
            <cps-content class='has-header has-header'>
            <cps-list>
            <button class='button button-full button-secondary icon ion-location'
                                     ng-click='setCurrentLocation()' ng-if='displayCurrentLocation'>
               ${ attrs.locationText || "{{ 'mapsUseCurrentLocation' | cpsGetLocale:'Use current location'}}" }
               <cps-spinner processing="fetchingLocation()" icon="ripple"></cps-spinner>
            </button>
            <cps-item ng-repeat='location in locations' class='item-text-wrap' ng-click='selectLocation(location)'>
              {{location.description || location.formatted_address}}
            </cps-item>
            </cps-list>
            </cps-content>
            </div>`;

            $Template.compile({
                template: POPUP_TPL,
                scope: scope,
                appendTo: $document[0].body
            }).then((el) => {
                let searchInputElement = angular.element(el.element.find("input"));
                _element = el.element;

                scope.cancel = onCancel;
                scope.selectLocation = selectLocation;
                scope.setCurrentLocation = setCurrentLocation;

                // Events
                scope.$watch("searchQuery", (query) => {
                    if (searchEventTimeout) {
                        $timeout.cancel(searchEventTimeout);
                    }
                    searchEventTimeout = $timeout(() => {
                        if(!query) {
                            return;
                        }
                        if(query.length < 3) {
                            return;
                        }

                        if(scope.withGeocode) {
                            let req = ld.defaultsDeep(scope.geocodeOptions || {}, {});

                            req.address = query;

                            $Maps.geocoder(req, 50).then((results) => {
                                results = filterRegExp(results);
                                $timeout(() => {
                                    scope.$apply(() => {
                                        scope.locations = results;
                                    });
                                });
                            });
                        } else {
                            let req = ld.defaultsDeep(scope.predictionsOptions || {}, {
                                types: ["geocode", "establishment"],
                                componentRestrictions: {country: "co"}
                            });

                            req.input = query;

                            $MapsPlaces.getPlacePredictions(req).then((predictions) => {
                                predictions = filterRegExp(predictions);
                                $timeout(() => {
                                    scope.$apply(() => {
                                        scope.locations = predictions;
                                    });
                                });
                            });
                        }
                    }, 350); // we're throttling the input by 350ms to be nice to google's API
                });
                scope.$watch(() => {
                    return ld.get(ngModel, "$modelValue.formatted_address");
                }, function (newVal, oldVal) {
                    if(newVal != oldVal) {
                        ngModel.$render();
                    }
                });

                element.bind("click", onClick);
                element.bind("touchend", onClick);

                // Internal functions
                function selectLocation (location) {
                    if (scope.withGeocode) {
                        ngModel.$setViewValue(location);
                        scope.$emit("cps:mapsPlaces:location", location);

                        onClose(location);
                    } else {
                        $MapsPlaces.getPlaceDetails({
                            placeId: location.place_id
                        }, element[0]).then((details) => {
                            $Maps.geocoder({
                                parse: details
                            }).then((parseDetails) => {
                                details = parseDetails;

                                if (location.description !== details.formatted_address) {
                                    details.formatted_address_detail = details.formatted_address;
                                    details.formatted_address = `${details.name} - ${details.formatted_address}`;
                                }
                                ngModel.$setViewValue(details);
                                scope.$emit("cps:mapsPlaces:location", details);

                                onClose(details);
                            });
                        }, (error) => {
                            logger.error("maps", "cpsPlaces: selectLocation error: {0}", [JSON.stringify(error)]);
                        }, (update) => {
                            logger.debug("maps", "cpsPlaces: selectLocation notification: {0}", [
                                JSON.stringify(update)
                            ]);
                        });
                    }
                }

                function setCurrentLocation () {
                    let currentModel = ngModel.$modelValue && ld.cloneDeep(ngModel.$modelValue, true) || {};
                    ngModel.$setViewValue({
                        formatted_address: _("mapsGettingLocation", "Getting current location...")
                    });

                    fetchingLocation(true);

                    $Geo.getCurrentPosition()
                        .then($Maps.geocoder)
                        .then((results) => {
                            fetchingLocation(false);

                            ngModel.$setViewValue(results[0]);
                            element.attr("value", results[0].formatted_address);

                            scope.$emit("cps:mapsPlaces:location", results[0]);
                            onClose(results[0]);

                        }).catch((error) => {
                            logger.error("maps", "cpsPlaces: setCurrentLocation error: {0}", [JSON.stringify(error)]);

                            fetchingLocation(false);
                            onClose();

                            ngModel.$setViewValue({
                                formatted_address: _("mapsErrorGettingLocation", "Error in getting current location")
                            });

                            $timeout(() => {
                                ngModel.$setViewValue(currentModel);
                            }, 2000);
                        });
                }

                function filterRegExp (predictions) {
                    if (!scope.filter) {
                        return predictions;
                    }
                    // Filter by RegExp
                    return ld.reduce(predictions, (result, location) => {
                        if (location.description.match(new RegExp(scope.filter))) {
                            result.push(location);
                        }
                        return result;
                    }, []);
                }

                function onClick (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    $Backdrop.retain();
                    unbindBackButtonAction = $Hardware.registerBackButtonAction(
                        onCancel,
                        $Hardware.BACK_PRIORITY.modal
                    ); // TODO: history service

                    el.element.removeClass("ng-hide");

                    if(!scope.searchQuery) {
                        scope.searchQuery = ngModel.$modelValue && ngModel.$modelValue.formatted_address || "";
                    }
                    searchInputElement[0].focus();
                    $Maps.setClickable(false);
                    $timeout(() => {
                        searchInputElement[0].focus();
                    }, 500);
                }
            });

            if(attrs.placeholder) {
                element.attr("placeholder", attrs.placeholder);
            }

            ngModel.$formatters.unshift((modelValue) => {
                if (!modelValue) {
                    return "";
                }
                return modelValue;
            });

            ngModel.$parsers.unshift((viewValue) => {
                return viewValue;
            });

            scope.$on("$destroy", () => {
                onClose();
            });

            ngModel.$render = () => {
                if(!ngModel.$modelValue) {
                    element.val("");
                } else {
                    element.val(ngModel.$modelValue && ngModel.$modelValue.formatted_address || "");
                }
            };

            function clearQueryInput (e) {
                e.preventDefault();
                $timeout(() => {
                    scope.searchQuery = "";
                });
            }

            function getClearClass () {
                return scope.searchQuery && scope.searchQuery.length ? $UiConfig.buttons.clear() : "ng-hide";
            }

            function onCancel (e) {
                e.preventDefault();
                scope.searchQuery = "";
                scope.locations = null;

                onClose();
            }

            function onClose (location) {
                if (_element) {
                    _element.addClass("ng-hide");
                }
                $Maps.setClickable(true);
                $Backdrop.release();

                if (unbindBackButtonAction) {
                    unbindBackButtonAction();
                    unbindBackButtonAction = null;
                }
                if(location) {
                    $timeout(() => {
                        scope.$onClose({location: location});
                    }, 300);
                }
            }

            function fetchingLocation (val) {
                if(val !== undefined) {
                    if(ngModel.$modelValue) {
                        ngModel.$modelValue.fetchingLocation = val;
                    }
                    return val;
                }
                return ngModel.$modelValue && ngModel.$modelValue.fetchingLocation;
            }

        }
    }


    // Exports
    container.$inject = [
        "$rootScope",
        "$window",
        "$timeout",
        "$Dom",
        "$Mobile",
        "$Menu",
        "$Body",
        "$Backdrop",
        "$Bind",
        "$Maps",
        "$Gestures"
    ];
    places.$inject = [
        "$q",
        "$timeout",
        "$document",
        "$Bind",
        "$Geo",
        "$App",
        "$Mobile",
        "$Logger",
        "$Hardware",
        "$UiConfig",
        "$Backdrop",
        "$Template",
        "$Maps",
        "$MapsPlaces"
    ];

    module.exports = {
        container: container,
        places: places
    };
})();
