/* global require */

// modules/maps/mapsRoutesService.js
//
// Routes service for maps module
// This service is responsible for get routes cordinates
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    mapsRoutesService.$inject = ["$q", "$Logger", "$Request", "$Mobile"];
    function mapsRoutesService ($q, $Logger, $Request, $Mobile) {
        var logger = $Logger.getInstance();

        // Members
        var Routes = {
            getCoordinates: getCoordinates
        };
        return Routes;

        // Functions
        function getCoordinates (coords) {
            var deferred = $q.defer(),
                from = coords && coords.from || {},
                to = coords && coords.to || {},
                req = {
                    url: "http://www.yournavigation.org/api/1.0/gosmore.php",
                    method: "GET",
                    background: true,
                    params: {
                        format: "geojson",
                        v: "motorcar",
                        fast: 1,
                        layer: "mapnik",
                        instructions: 1,
                        lang: $Mobile.getLanguage(),
                        flat: from.lat,
                        flon: from.lng,
                        tlat: to.lat,
                        tlon: to.lng
                    }
                };

            $Request.http(req).then(function (res) {
                deferred.resolve(res.data);
            }).catch(function (err) {
                logger.debug("maps", "routesMaps error, {0} - {1} - {2}", [
                    err.statusText, err.status, err.data
                ]);
                // Send error route event register to Analytics
                $Mobile.ga("send", "exception",
                           "routeMap: " + err.statusText + " - " + err.status, true);

                // Call fallback
                coordinatesFallback(coords).then(function (resFallback) {
                    deferred.resolve(resFallback);
                }).catch(function (errFallback) {
                    deferred.reject(errFallback);
                });
            });

            return deferred.promise;
        }

        // Private

        function coordinatesFallback (coords) {
            var deferred = $q.defer(),
                from = coords && coords.from || {},
                to = coords && coords.to || {},
                req = {
                    url: "http://open.mapquestapi.com/directions/v2/route",
                    method: "GET",
                    background: true,
                    params: {
                        key: "LRvGmAsAiaLvdn92Jbakkagd11pJINhv",
                        outFormat: "json",
                        timeType: 1,
                        shapeFormat: "raw",
                        generalize: 0,
                        locale: $Mobile.getLanguage(),
                        unit: "k",
                        from: from.lat + "," + from.lng,
                        to: to.lat + "," + to.lng
                    }
                };

            $Request.http(req).then(function (res) {
                if(ld.has(res.data, "route.shape.shapePoints")) {
                    res.data = ld.extend({
                        coordinates: ld.map(ld.chunk(res.data.route.shape.shapePoints, 2), function (value, index) {
                            return value.reverse();
                        })
                    }, res.data);
                } else {
                    res.data = ld.extend({coordinates: []}, res.data);
                }
                deferred.resolve(res.data);
            }).catch(function (err) {
                logger.debug("maps", "routesMaps error, {0} - {1} - {2}", [
                    err.statusText, err.status, err.data
                ]);
                // Send error route event register to Analytics
                $Mobile.ga("send", "exception",
                           "routeMap Fallback: " + err.statusText + " - " + err.status, true);

                deferred.reject(err);
            });

            return deferred.promise;
        }

    }

    // Exports
    module.exports = mapsRoutesService;

})();
