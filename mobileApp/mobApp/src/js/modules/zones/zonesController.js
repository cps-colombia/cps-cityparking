/* global require */

// modules/zones/zonesController.js
//
// Zones controller for cps app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    ZonesController.$inject = [
        "$q",
        "$scope",
        "$timeout",
        "$Mobile",
        "$Login",
        "$Zones",
        "$Logger",
        "$App",
        "$Maps",
        "$Modal",
        "$Filter",
        "$Scroll",
        "$Geo"
    ];
    function ZonesController ($q, $scope, $timeout, $Mobile, $Login, $Zones,
                             $Logger, $App, $Maps, $Modal, $Filter, $Scroll, $Geo) {
        var _ = $Mobile.locale,
            zones = this,
            logger = $Logger.getInstance(),
            userData = $Login.getUserSession,
            filterInstance = ld.noop,
            geoSpinner;

        // Members
        zones.options = $Zones.options;
        zones.zonesInit = zonesInit;
        zones.zonesByCityMap = zonesByCityMap;
        zones.zonesByPosition = zonesByPosition;
        zones.zonesByCity = zonesByCity;
        zones.zonesByPoint = zonesByPoint;
        zones.zonesChangeCity = zonesChangeCity;
        zones.zonesRefresh = zonesRefresh;
        zones.zonesFilter = zonesFilter;

        zones.init = zonesInit;
        zones.byCityMap = zonesByCityMap;
        zones.byPosition = zonesByPosition;
        zones.byCity = zonesByCity;
        zones.byPoint = zonesByPoint;
        zones.changeCity = zonesChangeCity;
        zones.refresh = zonesRefresh;
        zones.filter = zonesFilter;

        // By City Params
        zones.dataZonesCity = {
            idc: userData().country,
            idcity: userData().city,
            commerceid: "all"
        };

        // By Position
        zones.dataZonesPosition = {
            lat: userData().city_lat,
            lon: userData().city_lng,
            range: 5000,
            commerceid: "all"
        };

        // Zones List
        zones.zonesList = [];
        zones.cities = $App.getCities(userData().country);

        // Events
        $scope.$on("cps:baseData:success", function (event, res) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // Update data
            zones.cities = $App.getCities(userData().country);
        });

        // Clean scope
        $scope.$on("$destroy", function () {
            // Cancel Listeners
            $scope.$$listeners["cps:zonesList:update"] = [];
            $scope.$$listeners["cps:view:completeEnter"] = [];
        });

        $scope.$on("cps:view:completeEnter", function (event, data) {
            zonesInit();
        });
        // Functions
        function zonesInit (options) {
            var timer = 300,
                hasActionPosition = false,
                withGeoPosition = zones.options.geoPosition;
            options = ld.extend({
                onlyList: false,
                fetch: true
            }, options);

            // get targets
            $Maps.ready(function () {
                geoSpinner = geoSpinner || $Maps.spinner();

                $timeout(function () {
                    zones.zonesList = $Zones.getZones(zones.dataZonesCity) || [];
                    if(!options.onlyList && options.fetch) {
                        zonesByCityMap({
                            background: true,
                            fromStorage: true,
                            showNetwork: !withGeoPosition,
                            cleanRoutes: false,
                            boundsZones: zones.options.startupBounds || false,
                            lastBounds: !$Zones.isFirstInit
                        }).then(null, function (err) {
                            initZonesByPosition();
                        }, function (update) {
                            initZonesByPosition();
                        });
                        $Zones.isFirstInit = false;
                    }
                }, timer);
            });
            function initZonesByPosition () {
                if(!hasActionPosition && withGeoPosition) {
                    hasActionPosition = true;
                    zonesByPosition({background: true});
                }
            }
        }

        function zonesByCityMap (options) {
            var deferred = $q.defer(),
                dataZonesCity = ld.clone(zones.dataZonesCity, true),
                marksOptions = {},
                zonesCity = [];

            options = ld.extend({
                fromStorage: false,
                lastBounds: false,
                showNetwork: true,
                showPosition: true,
                boundsZones: true
            }, options);

            marksOptions = ld.pick(options, [
                "showPosition",
                "lastBounds",
                "cleanRoutes",
                "boundsZones"
            ]) || {};

            if(options.background) {
                dataZonesCity.background = options.background;
            } else {
                // Hide modal and mark zones
                $Modal.hide({all: true});
            }

            if(options.fromStorage) {
                zonesCity = $Zones.getZones(dataZonesCity) || [];
            }
            if(!zonesCity || zonesCity.length <= 0) {
                options.showNetwork = true;
                dataZonesCity.background = false;
            } else {
                marksOptions.points = zonesCity;
                $Maps.marksMap(marksOptions).then(function () {
                    deferred.notify(zonesCity);
                });
            }
            // Get from network and return promise
            zonesByCity(dataZonesCity).then(function (res) {
                zonesCity = res.data || [];
                if(options.showNetwork) {
                    marksOptions.points = zonesCity;
                    $Maps.marksMap(marksOptions).then(function () {
                        deferred.notify(zonesCity);
                    });
                }
                deferred.resolve(zonesCity);
            }, function (err) {
                deferred.reject(err);
            });
            // Promsie
            return deferred.promise;
        }

        function zonesByPosition (options) {
            var reqZonesPosition = {},
                marksOptions = {},
                userPosition = $Geo.getUserPosition(),
                hasUserPosition = $Geo.hasUserPosition();

            options = ld.extend({
                loading: false
            }, options);

            geoSpinner = geoSpinner || $Maps.spinner();
            geoSpinner.show();

            marksOptions = {
                showPosition: true,
                showRoutes: true,
                cleanRoutes: false,
                boundsLimit: 5
            };

            // If already has userPosition show
            if(hasUserPosition) {
                // Update lat and lng
                zones.dataZonesPosition.lat = userPosition.coords.latitude;
                zones.dataZonesPosition.lon = userPosition.coords.longitude;
                // Extend req
                ld.extend(reqZonesPosition, ld.clone(zones.dataZonesPosition, true), {
                    background: options.background
                });
            }

            // Update user position and get zones around
            $Geo.getCurrentPosition().then(function (position) {
                // Set position
                zones.dataZonesPosition.lat = position.coords.latitude;
                zones.dataZonesPosition.lon = position.coords.longitude;

                // Extend data
                ld.extend(reqZonesPosition, ld.clone(zones.dataZonesPosition, true), options);

                // Get Zones
                $Zones.zonesByPosition(reqZonesPosition, options).then(function (res) {
                    geoSpinner.hide();

                    marksOptions.points = res.data;
                    $Maps.marksMap(marksOptions);
                }).catch(function (errZones) {
                    geoSpinner.hide();
                    $Maps.marksMap({
                        markerType: "user",
                        position: position.coords,
                        boundsZones: true
                    });
                });
            }).catch(function (err) {
                geoSpinner.hide();

                if(!err.backgroundJob && !err.canceled) {
                    if(hasUserPosition) {
                        // Get Zones around
                        $Zones.zonesByPosition(reqZonesPosition).then(function (res) {
                            geoSpinner.hide();

                            marksOptions.points = res.data;
                            $Maps.marksMap(marksOptions);
                        }).catch(function (errZones) {
                            geoSpinner.hide();
                        });
                    } else {
                        // From cache
                        logger.debug("zones", "zonesByPosition fallback cache zones");
                        if($Zones.isFirstInit) {
                            $Maps.marksMap({points: $Zones.getZones(zones.dataZonesCity)});
                        }
                    }
                }
            });
        }

        function zonesByCity (options) {
            var deferred = $q.defer(),
                dataZonesCity = ld.clone(zones.dataZonesCity, true);
            if(options) {
                ld.extend(dataZonesCity, options);
            }
            $Zones.zonesByCity(dataZonesCity).then(function (res) {
                zones.zonesList = res.data;
                deferred.resolve(res);
            }).catch(function (err) {
                zones.zonesList = $Zones.getZones(zones.dataZonesCity);
                deferred.reject(err);
            });
            // Promise
            return deferred.promise;
        }

        function zonesByPoint (index) {
            var deferred = $q.defer();

            if(zones.zonesList[index]) {
                $Maps.marksMap({
                    points: [zones.zonesList[index]],
                    showInfo: true
                }).then(function (res) {
                    $Modal.hide({all: true});
                    deferred.resolve(res);
                }).catch(function (err) {
                    $Mobile.alert(_("alertPointWrong"));
                    deferred.reject(err);
                });
            } else {
                deferred.reject();
            }
            // Promsie
            return deferred.promise;
        }

        function zonesChangeCity (cityId) {
            var deferred = $q.defer(),
                options = cityId ? {
                    idcity: cityId
                } : null;

            $Mobile.loading("show");
            zonesRefresh({timeout: 100}).then(function (res) {
                return zonesByCity(options);
            }).catch(function (err) {
                $Mobile.loading("hide");
                deferred.reject();
            });
            // Promsie
            return deferred.promise;
        }
        function zonesRefresh (options) {
            var deferred = $q.defer();
            options = ld.extend({
                fetch: false,
                timeout: 1000
            }, options);
            filterInstance();
            $timeout(function () {
                $Scroll.scrollTop();
                if(options.fetch) {
                    return zonesByCityMap();
                } else {
                    deferred.resolve();
                }
                $scope.$broadcast("cps:scroll:refreshComplete");
            }, options.timeout);
            // Promsie
            return deferred.promise;
        }

        function zonesFilter () {
            filterInstance = $Filter.show({
                items: zones.zonesList,
                container: ".modal.active",
                placeholderText: _("filterZone"),
                config: zones.options.filter,
                scrollDelegate: $Scroll.$getByHandle((zones.options.delegateHandle || "zonesList")),
                update: function (filteredItems, filterText) {
                    zones.zonesList = filteredItems;
                }
            });
        }
    }

    // Exports
    module.exports = {
        ZonesController: ZonesController
    };

})();
