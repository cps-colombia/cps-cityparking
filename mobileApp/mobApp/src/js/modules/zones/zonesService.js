/* global require */

// modules/zones/zonesController.js
//
// Zones factory service for zones module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    zonesService.$inject = ["$window", "$q", "$Geo", "$Login", "$Logger", "$App", "$Mobile", "$Request"];
    function zonesService ($window, $q, $Geo, $Login, $Logger, $App, $Mobile, $Request) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance(),
            userData = $Login.getUserSession,
            userHash = $Login.getUserHash,
            config = $App.config,
            zonesOptions = ld.extend({
                startupGeo: $Geo.isEnable(),
                startupBounds: false
            }, config.options.zones),
            op = $App.ops;

        // Members
        var Zones = {
            options: zonesOptions,
            isFirstInit: true,
            zonesByCity: zonesByCity,
            zonesByPosition: zonesByPosition,
            setZones: setZones,
            getZones: getZones
        };

        return Zones;

        // Functions
        function zonesByCity (dataObj, options) {
            var deferred = $q.defer(),
                req = { // Register Params
                    params: {
                        op: op.zonesByCity,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            options = ld.extend({}, options);
            ld.extend(req.params, dataObj);

            if(!dataObj.background && options.loading !== false) {
                $Mobile.loading("show");
            }

            // Get Zones
            getHttpZones(req).then(function (res) {
                $Mobile.loading("hide");
                deferred.resolve(res);
            }).catch(function (err) {
                $Mobile.loading("hide");
                deferred.reject(err);
            });


            return deferred.promise;
        }

        function zonesByPosition (dataObj, options) {
            var deferred = $q.defer(),
                req = { // zones Params
                    params: {
                        op: op.zonesByPosition,
                        user_token: userHash().ut,
                        user_secret: userHash().us,
                        lat: userData().city_lat,
                        lon: userData().city_lng
                    }
                };

            options = ld.extend({}, options);
            ld.extend(req.params, dataObj);

            if(!dataObj.background && options.loading !== false) {
                $Mobile.loading("show");
            }

            // Get Zones
            getHttpZones(req).then(function (res) {
                $Mobile.loading("hide");
                deferred.resolve(res);
            }).catch(function (err) {
                $Mobile.loading("hide");
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function setZones (zones) {
            var currentZones = getZones() || [],
                zoneIndex;
            if(ld.isArray(zones)) {
                ld.forEach(zones, function (zone, key) {
                    zoneIndex = ld.findIndex(currentZones, {idzone: zone.idzone});
                    if(ld.isObject(zone) && zoneIndex > -1) {
                        currentZones[zoneIndex] = zone;
                    } else {
                        currentZones.push(zone);
                    }
                });
            } else if (ld.isObject(zones)) {
                zoneIndex = ld.findIndex(currentZones, {idzone: zones.idzone});
                if(ld.isObject(zones) && zoneIndex > -1) {
                    currentZones[zoneIndex] = zones;
                } else {
                    currentZones.push(zones);
                }
            }
            ld.uniqBy(currentZones, "idzone");
            $window.localStorage.zones = JSON.stringify(currentZones);
        }

        function getZones (cityObj) {
            var zones = [],
                zonesCity = [],
                filterCity = {},
                filterCountry = {},
                isNewResponse = false,
                isString = false;

            if(!$window.localStorage.zones) {
                return zones;
            }

            zones = JSON.parse($window.localStorage.zones);
            if(ld.isObject(cityObj)) {
                isNewResponse = ld.has(zones[0], "city");
                isString = ld.isString(ld.get(zones[0], "city.idcity") ||
                                       ld.get(zones[0], "zoneLocation.idcity"));

                cityObj.idcity = isString ? String(cityObj.idcity) : parseInt(cityObj.idcity);
                cityObj.idc = isString ? String(cityObj.idc) : parseInt(cityObj.idc);

                if(isNewResponse) {
                    filterCity.city = {idcity: cityObj.idcity};
                    filterCountry.city = {idcountry: cityObj.idc};
                } else {
                    filterCity.zoneLocation = {idcity: cityObj.idcity};
                    filterCountry.zoneLocation = {idcountry: cityObj.idc};
                }
                zonesCity = ld.filter(zones, filterCity);
                if(!zonesCity.length) {
                    logger.info("zones", "getZones fallback by country");
                    zonesCity = ld.filter(zones, filterCountry);
                }
                return zonesCity;
            } else {
                return zones;
            }
        }

        // Private
        function getHttpZones (req) {
            var deferred = $q.defer(),
                success = false;

            $Request.http(req).then(function (res) {
                var resData = res.data || {},
                    statusCode = resData.statusCode || null;

                logger.debug("zones", JSON.stringify(resData));
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    resData.data = ld.map(resData.data, function (zone, key) {
                        zone.idzone = zone.idbranch || zone.idzone;
                        return zone;
                    });
                    setZones(resData.data);
                    success = true;
                } else if(statusCode === 218) {
                    success = false;
                    $Mobile.alert(_("alertNoZonesCountry"));
                } else if(statusCode === 219) {
                    success = false;
                    $Mobile.alert(_("alertNoZonesCity"));
                } else if(statusCode === 123 || statusCode === 124) {
                    success = false;
                    logger.warn("warn", "Invalid city or country? with code: {0}", [statusCode ]);
                    $Mobile.ga("send", "event", "zones", "error: " + statusCode);
                }

                if(success) {
                    deferred.resolve(resData);
                } else {
                    deferred.reject(resData);
                }

            }).catch(function (err) {
                logger.warn("zones", "{1}", [JSON.stringify(req), JSON.stringify(err)]);
                deferred.reject(err);
            });

            return deferred.promise;
        }
    }

    // Exports
    module.exports = {
        zonesService: zonesService
    };

})();
