/* global module */

// module/zones/zonesRoutes.js
//
// Routes for cps zones module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    zonesRoutes.$inject = ["$routeProvider"];
    function zonesRoutes ($routeProvider) {
        $routeProvider
            .when("/zones", {
                templateUrl: "template/zones.html",
                controller: "ZonesController",
                controllerAs: "zones",
                data: {
                    title: "zones"
                }
            });
    }

    // Exports
    module.exports = {
        zonesRoutes: zonesRoutes
    };

})();
