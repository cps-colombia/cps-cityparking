/* global require */
/* exported maps */

// modules/zones/index.js
//
// index function for zones module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        maps = require("../maps"), // eslint-disable-line
        zonesRoutes = require("./zonesRoutes"),
        zonesController = require("./zonesController"),
        zonesService = require("./zonesService");

    angular.module("cpsZones", ["cpsMaps"]);
    var cpsZones = angular.module("cpsZones");

    // Routes
    cpsZones.config(zonesRoutes.zonesRoutes);

    // Service
    cpsZones.service("$Zones", zonesService.zonesService);

    // Controller
    cpsZones.controller("ZonesController", zonesController.ZonesController);

})();
