/* global module */

// app/core/parking/parkingDirective.js
//
// directives function for parking module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const angular = require("angular");
    const ld = require("lodash");
    const $$ = angular.element;

    function vehicles () {
        let vehiclesDirective = {
            restrict: "E",
            templateUrl: "app/views/vehicles.html"
        };
        return vehiclesDirective;
    }

    function vehicleAddForm () {
        let vehiclesDirective = {
            restrict: "E",
            templateUrl: "app/views/vehiclesAddForm.html"
        };
        return vehiclesDirective;
    }

    function parkForm () {
        let parkDirective = {
            restrict: "E",
            templateUrl: "app/views/parkForm.html"
        };
        return parkDirective;
    }

    userVehicles.$inject = ["$timeout", "$Bind", "$Parking"];
    function userVehicles ($timeout, $Bind, $Parking) {
        let vehiclesDirective = ({
            restrict: "E",
            require: "ngModel",
            scope: true,
            replace: true,
            compile: compile,
            template: template
        });
        return vehiclesDirective;

        // Functions
        function template (tElement, tAttrs) {
            let element = tAttrs.element || "select";
            let isSelect = element === "select";
            let subElement = isSelect ? "option" : (element === "ul" ? "li" : "div");
            let htmlTemplate = "<" + element + "> " +
                "<" + subElement + " value='{{vehicleData.plate}}' label='{{vehicleData.plate | uppercase}}' " +
                "ng-repeat='vehicleData in vehiclesList track by $index'>" +
                "{{vehicleData.plate}}" +
                "</" + subElement + ">" +
                (isSelect ?
                 "<option value='' action='add' locale-id='addVehicleOption' locale-fallback='-- ADD --'></option>"
                 : "") +
                "</" + element + ">";

            return htmlTemplate;
        }

        // When we compile the directive, we have to remove the pre-interpolation
        function compile (tElement, tAttrs) {
            // Clear the value so the attribute-interpolation directive won't
            // bind a watcher.
            tAttrs.$set("element", null);

            // Return the linking function.
            return {
                pre: preLink,
                post: postLink
            };
        }

        function preLink (scope, element, attrs) {
            $Bind(scope, attrs, {
                fetchFn: "=",
                fetch: "="
            });
        }

        function postLink (scope, element, attrs, ngModel) {
            scope.fetchFn = fetchFn;
            scope.vehiclesList = $Parking.userVehicles();

            selectVehicle(ld.first(scope.vehiclesList));

            if(attrs.fetch === "true") {
                fetchFn();
            }

            scope.$watch("fetch", (newVal, oldval) => {
                if(newVal) {
                    fetchFn();
                }
            });

            element.on("change", function (e) {
                let optionAction = $$("option:selected", this).attr("action");
                if(optionAction === "add") {
                    $Parking.dialogVehicles(optionAction).then((vehicleData) => {
                        scope.vehiclesList.push(vehicleData);

                        $timeout(() => {
                            selectVehicle(ld.last(scope.vehiclesList));
                        });
                    }).catch(() => {
                        $timeout(() => {
                            selectVehicle(ld.first(scope.vehiclesList));
                        });
                    });
                }
            });

            function fetchFn () {
                $Parking.getVehicles().then((vehiclesList) => {
                    scope.vehiclesList = vehiclesList;
                    selectVehicle(ld.first(scope.vehiclesList));
                });
            }
            function selectVehicle (option) {
                if(ld.has(option, "plate")) {
                    $$("option[value='" + option.plate + "']", element).prop("selected", true);

                    ngModel.$setViewValue(option.plate);
                    ngModel.$render();
                } else {
                    $$("option:first", element).prop("selected", true);
                }
            }

        }
    }

    function bookingList () {
        let listDirective = {
            restrict: "E",
            templateUrl: "app/views/bookingList.html"
        };
        return listDirective;
    }

    function adminBalanceForm () {
        let adminBalanceDirective = {
            restrict: "E",
            templateUrl: "app/views/parkingAdminBalance.html"
        };
        return adminBalanceDirective;
    }

    // Exports
    module.exports = {
        vehicles: vehicles,
        vehicleAddForm: vehicleAddForm,
        parkForm: parkForm,
        userVehicles: userVehicles,
        adminBalanceForm: adminBalanceForm,
        bookingList: bookingList
    };

})();
