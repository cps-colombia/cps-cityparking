/* global require */

// modules/parking/parkingController.js
//
// Parking controller for cps app
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const ld = require("lodash");
    const moment = require("moment");
    const asyncLib = require("async");

    function ParkingController ($q, $scope, $interval, $timeout, $rootScope, $Main, $Mobile, $Encrypt,
                                $Scroll, $Login, $Parking, $Logger, $App, $Dialog, $Modal, $Utils,
                                $Hardware, $Barcode, $Numbro) {
        var _ = $Mobile.locale;
        var logger = $Logger.getInstance();
        var parking = this;
        var config = $App.config;
        var userData = $Login.getUserSession;
        var activeTimer = null;

        // Members
        parking.options = $Parking.options;
        parking.hasVehicles = hasVehicles;
        parking.getBrands = getBrands;
        parking.lastVehicle = lastVehicle;
        parking.getVehicle = getVehicle;
        parking.getVehicles = getVehicles;
        parking.addVehicles = addVehicles;
        parking.addVehiclesDialog = addVehiclesDialog;
        parking.updateVehicles = updateVehicles;
        parking.updateVehiclesDialog = updateVehiclesDialog;
        parking.deleteVehicles = deleteVehicles;
        parking.showRates = showRates;
        parking.startPark = startPark;
        parking.startParkDialog = startParkDialog;
        parking.endPark = endPark;
        parking.activePark = activePark;
        parking.priceBooking = priceBooking;
        parking.formBooking = formBooking;
        parking.addBooking = addBooking;
        parking.listBooking = listBooking;
        parking.detailBooking = detailBooking;
        parking.cancelBooking = cancelBooking;
        parking.adminGetBalance = adminGetBalance;

        // Add vehicles
        parking.vehicleData = {
            plate: null,
            type: null,
            brand: null,
            description: null
        };

        // Admin get userData
        parking.getUserData = {
            identifier: ""
        };
        parking.userData = {};

        // Vehicles
        parking.vehiclesList = $Parking.userVehicles();
        parking.rates = {};

        // Parking
        parking.userActivePark = [];
        parking.startData = {
            plate: null,
            idp: null,
            num: userData().phone,
            idc: userData().country,
            idu: userData().idu,
            sms: 0
        };

        // Booking
        parking.bookingData = {
            placeId: null,
            contact_phone: userData().phone,
            observation: "",
            date_service: $Utils.dateObject(),
            date_end_service: null,
            fee: null,
            plate: null,
            pocket_id: 1
        };
        parking.bookings = [];

        // Base
        parking.vehicleTypes = [];
        parking.vehicleBrands = [];

        $Main.baseData().then((data) => {
            parking.vehicleTypes = data.vehicleTypes;
        });

        // Control
        parking.formSuccess = false;
        parking.step = 1;

        // Run
        pushActivePark($Parking.getActivePark());

        // Events
        $scope.$on("$destroy", () =>{
            clearTimerActive();
        });

        // Functions
        function hasVehicles () {
            return $Parking.hasVehicles();
        }

        function getBrands (typeId) {
            typeId = typeId || parking.vehicleData.type;

            $Main.baseData().then((data) => {
                parking.vehicleBrands = ld.filter(data.vehicleBrands, {vehicleType: { vTypeId: parseInt(typeId)}});
            });
        }

        function lastVehicle () {
            return  ld.clone(ld.last(parking.vehiclesList) || {});
        }

        function getVehicle (plate) {
            let plateVehicle  = plate || parking.vehicleData.plate;
            let dataVehicle = $Parking.userVehicles({plate: plateVehicle});

            if(dataVehicle) {
                parking.vehicleData = dataVehicle;

                if(plateVehicle && (!dataVehicle.description || !dataVehicle.vehicleBrand)) {
                    updateVehiclesDialog({
                        plate: plateVehicle
                    }).then((data) => {
                        if(data && data.plate) {
                            parking.vehicleData = data;
                        }
                    });
                }
            }
            return parking.vehicleData;
        }

        function getVehicles (options) {
            parking.vehiclesList = $Parking.userVehicles();

            $Parking.getVehicles(options).then((resVehicles) => {
                parking.vehiclesList = resVehicles || [];
                if(!parking.startData.plate) {
                    parking.startData.plate = lastVehicle().plate;
                }
                if(!parking.vehicleData.plate) {
                    parking.vehicleData = lastVehicle();
                }
            });
        }

        function setVehicles (vehicleData) {
            if(ld.isObject(vehicleData)) {
                parking.vehicleData = {
                    plate: vehicleData.plate,
                    type: vehicleData.type,
                    brand: vehicleData.vehicleBrand && vehicleData.vehicleBrand.vBrandId,
                    description: vehicleData.destination
                };
            }
        }

        function addVehicles (vehicleData) {
            let deferred = $q.defer();
            $Parking.addVehicles((vehicleData || parking.vehicleData)).then((resAdd) => {
                if(resAdd.data && !ld.find(parking.vehiclesList, {plate: resAdd.data.plate})) {
                    parking.vehiclesList.push(ld.clone(resAdd.data));
                    parking.formSuccess = true;
                }
                if($scope.addDialog) {
                    $scope.addDialog.close(resAdd.data);
                }
                deferred.resolve(resAdd);
            }).catch((err) => {
                deferred.reject(err);
            });
            // Promise
            return deferred.promise;
        }

        function addVehiclesDialog () {
            let deferred = $q.defer();
            let emptyData = {
                plate: null,
                type: null,
                brand: null,
                description: null
            };
            parking.vehicleData = ld.clone(emptyData);

            $scope.addDialog = $Dialog.show({
                title: _("addVehicle"),
                templateUrl: "app/views/vehiclesAddForm.html",
                scope: $scope,
                buttons: [{
                    text: _("cancel"),
                    isClose: true
                }]
            });

            // When $Dialog close
            $scope.addDialog.then((res) =>{
                if(res) {
                    deferred.resolve(res);
                } else {
                    deferred.reject(res);
                }
                // Clean data
                parking.vehicleData = ld.clone(emptyData);
                delete $scope.addDialog;
            });
            // Promise
            return deferred.promise;
        }

        function updateVehicles () {
            let index;
            $Parking.updateVehicles(parking.vehicleData).then((resAdd) => {
                index = resAdd.data && ld.findIndex(parking.vehiclesList, {plate: resAdd.data.plate});
                if(index > -1) {
                    parking.vehiclesList[index] = ld.clone(resAdd.data);
                }
                if($scope.updateDialog) {
                    $scope.updateDialog.close(resAdd.data);
                }
            });
        }

        function updateVehiclesDialog (vehicle) {
            let deferred = $q.defer();

            setVehicles($Parking.userVehicles({plate: vehicle.plate}));

            $scope.updateDialog = $Dialog.show({
                title: _("updateVehicle"),
                templateUrl: "app/views/vehiclesUpdateForm.html",
                scope: $scope,
                buttons: [{
                    text: _("cancel"),
                    isClose: true
                }]
            });

            // When $Dialog close
            $scope.updateDialog.then((res) =>{
                if(res) {
                    deferred.resolve(res);
                } else {
                    deferred.reject(res);
                }

                // Clean data
                parking.vehicleData = {
                    plate: null,
                    type: null,
                    brand: null,
                    description: null
                };
                delete $scope.updateDialog;
            });
            // Promise
            return deferred.promise;
        }

        function deleteVehicles (vehicle) {
            $Mobile.confirm(_("vehicleRemoveConfirm", {
                plate: (vehicle && vehicle.plate || "").toUpperCase()
            })).then((buttonIndex) => {
                if(buttonIndex !== 2) {
                    $Parking.deleteVehicles({
                        plate: vehicle.plate
                    }).then((resDel) => {
                        if(resDel) {
                            ld.pullAt(parking.vehiclesList, ld.findIndex(parking.vehiclesList, {
                                plate: resDel.plate
                            }));
                        }
                    });
                }
            });
        }

        function showRates (zoneObj, scope) {
            var modalRates;
            scope = scope || $rootScope.$new();
            if(!ld.isEmpty(ld.get(zoneObj, "priceList.price.vtype_id"))) {
                parking.rates = zoneObj.priceList.price;

                $Modal.fromTemplateUrl("app/views/ratesList.html", {
                    id: "ratesListModal",
                    title: _("rates"),
                    closeBtn: true,
                    removeOnHide: true,
                    scope: scope
                }).then((modal) => {
                    modalRates = modal;
                    modalRates.show();
                });
            } else {
                scope.$destroy();
                $Mobile.alert(_("noRatesAvailable"));
            }
        }

        function startPark () {
            asyncLib.series([(next) => {
                if(!hasVehicles() && parking.startData.plate) {
                    addVehicles({plate: parking.startData.plate}).then((res) => {
                        next(null, res);
                    }).catch(next);
                } else {
                    next();
                }
            }, (next) => {
                $Parking.startPark(parking.startData).then((res) => {
                    next(null, res);
                }).catch(next);
            }], (err, res) => {
                if(!err) {
                    if($scope.startParkDialog) {
                        $scope.startParkDialog.close();
                    }
                    $Mobile.pageChange("/parking/active");
                }
            });
        }

        function startParkDialog (dataObj, scope) {
            let deferred = $q.defer();
            let attrScope = scope;
            let dialog;

            scope = scope || $scope;
            parking.startData.plate = dataObj.plate || lastVehicle().plate;

            dialog = $Dialog.show({
                title: _("park"),
                templateUrl: "app/views/parkForm.html",
                scope: scope,
                buttons: [{
                    text: _("cancel"),
                    isClose: true,
                    hold: true,
                    onTap: (e) => {
                        return {data: {close: true}, dialog: dialog};
                    }
                }]
            });

            scope.startParkDialog = dialog;

            // // When $Dialog close
            dialog.then((close) => {
                scope.startParkDialog = null;
                if(close) {
                    // Clean
                    if(attrScope) {
                        scope.$destroy();
                    }
                    deferred.reject(close);
                } else {
                    deferred.resolve(close);
                }
            });

            // Promise
            return deferred.promise;
        }

        function endPark (plate) {
            let parkIndex = ld.findIndex(parking.userActivePark, {vehiclePlate: plate});

            $Mobile.confirm(
                _("parkingEndConfirm",
                  parking.userActivePark[parkIndex])
            ).then((buttonIndex) => {
                if(buttonIndex == 1) {
                    $Parking.endPark({plate: plate}).then((res) => {
                        clearTimerActive(parkIndex);
                        ld.pullAt(parking.userActivePark, parkIndex);
                    });
                }
            });

        }

        function activePark (options) {
            if(!activeTimer) {
                activeTimer = $interval(() => {
                    activePark({background: true});
                }, 60000 * 10);
            }
            $Parking.activePark(options).then((actives) => {
                pushActivePark(actives);
            });
        }

        function priceBooking (prices) {
            parking.getVehicle();
            let dayName = moment().locale("en").format("ddd").toLowerCase();
            let priceData = prices.vtype_id[parking.vehicleData.type];
            let dateService = ($Utils.parseDate(parking.bookingData.date_service)).getTime();

            parking.bookingData.fee = ld.has(priceData, "weekRates") ? priceData.weekRates[dayName] : null;
            parking.bookingData.date_end_service = moment(dateService).add(
                ld.get(priceData, "fullRateTimeMinutes") || 0, "m").toDate();
        }

        function formBooking (dataObj, scope) {
            scope = scope || $rootScope.$new();
            scope.zoneData = dataObj;
            scope.zonePrice = dataObj.priceList.price;
            parking.bookingData.placeId = dataObj.idzone;

            scope.nextStep = () => {
                if (!parking.vehicleData.plate) {
                    $Mobile.alert(_("parkingBookingNeedVehicle"));
                    return;
                }
                if (moment($Utils.parseDate(parking.bookingData.date_service)).format("DD MMM YYYY h:mm a") ===
                    moment(parking.bookingData.date_end_service).format("DD MMM YYYY h:mm a")) {
                    $Mobile.alert(_("parkingBookingSameDate"));

                    $Mobile.ga("send", "event", "error", "booking:sameDates", dataObj.placeId, 400);
                    return;
                }
                if (!ld.has(scope.zonePrice, ["vtype_id", parking.vehicleData.type, "weekRates"]) ||
                    parking.bookingData.fee === null) {
                    $Mobile.alert(_("parkingBookingNotRates", {
                        vehicle_type: _("vehicleType" + parking.vehicleData.type)
                    }));

                    $Mobile.ga("send", "event", "error", "booking:notRates", dataObj.placeId, parking.vehicleData.type);
                    return;
                }
                parking.step++;
                $timeout(() => {
                    $Scroll.scrollTop();
                }, 100);
            };
            scope.prevStep = () => {
                if (parking.step === 1) {
                    if ($scope.modalBooking) {
                        $scope.modalBooking.hide();
                    }
                } else {
                    parking.step--;
                    $timeout(() => {
                        $Scroll.scrollTop();
                    }, 100);
                }
            };
            scope.bookingConfirm = (dataMethod) => {
                let dataPayment = dataMethod.data;
                let bookingData = ld.clone(parking.bookingData);
                parking.bookingData.pocket_id = dataMethod.id || 1;
                parking.bookingData.pocket_reference = dataPayment.card_reference;

                $Mobile.confirm(_("parkingBookingConfirm", {
                    date_start: moment($Utils.parseDate(bookingData.date_service)).format("DD MMM YYYY h:mm a"),
                    date_end: moment(bookingData.date_end_service).format("DD MMM YYYY h:mm a"),
                    fee: $Numbro.formatCurrency(bookingData.fee),
                    plate: ld.clone(parking.vehicleData.plate).toUpperCase(),
                    branch_name: scope.zoneData.name
                })).then((buttonIndex) => {

                    if (buttonIndex === 1) {
                        scope.dataMethod.exec({
                            dialog: false,
                            operation: "payment",
                            vat: dataPayment.vat,
                            product_amount: parking.bookingData.fee || dataPayment.amount,
                            card_reference: dataPayment.card_reference,
                            installments: parseInt(dataPayment.installments) || 1
                        }, (err, resMethod) => {
                            if (err) {
                                logger.warn("parking", "formBooking payment error {0}", [JSON.stringify(err)]);
                            } else {
                                addBooking();
                            }
                        });
                    }
                });
            };
            scope.paymentChage = (method, event) => {
                scope.isLastStep = method && method.isConfirm;
                if(ld.has(method, "id")) {
                    scope.dataMethod = method;
                }
                if(method && method.isNext) {
                    let dataMethod = ld.get(scope, "dataMethod.data");
                    if(ld.has(dataMethod, "balance") && dataMethod.balance < parking.bookingData.fee) {
                        $Mobile.alert(_("balanceNotEnough", {
                            amount: parking.bookingData.fee
                        }));
                    } else {
                        scope.isLastStep = true;
                        parking.paymentNext(null, event);
                    }
                }
            };
            scope._stepsDeregisterBackButton = $Hardware.registerBackButtonAction(
                scope.prevStep.bind(this),
                $Hardware.BACK_PRIORITY.modal + 10
            );

            $Modal.fromTemplateUrl("app/views/bookingAddForm.html", {
                id: "bookingFormModal",
                title: _("booking", "Booking"),
                closeBtn: true,
                removeOnHide: true,
                scope: scope,
                onClose: () => {
                    $scope.modalBooking = null;
                    if (scope._stepsDeregisterBackButton) {
                        scope._stepsDeregisterBackButton();
                        scope._stepsDeregisterBackButton = null;
                    }
                }
            }).then((modal) => {
                $scope.modalBooking = modal;
                $scope.modalBooking.show();
            });
        }

        function addBooking () {
            let bookingData = ld.cloneDeep(parking.bookingData);

            bookingData.date_service = ($Utils.parseDate(bookingData.date_service)).getTime();
            bookingData.date_end_service = new Date(bookingData.date_end_service).getTime();
            bookingData.plate = parking.vehicleData.plate;

            let promise = $Parking.addBooking(bookingData).then((res) => {
                parking.formSuccess = true;

                if($scope.modalBooking) {
                    // Clear modal from DOM
                    $scope.modalBooking.remove().then(() => {
                        $timeout(() => {
                            $Mobile.pageChange("/parking/bookings");
                        }, 300);
                    });
                }
                return res;
            });

            return promise;
        }

        function listBooking () {
            let promise = $Parking.listBooking().then((bookings) => {
                parking.bookings = bookings;
                return bookings;
            });
            return promise;
        }

        function cancelBooking (id) {
            let deferred = $q.defer();

            $Mobile.confirm(_("parkingBookingCancel", {
                id: id
            })).then((buttonIndex) => {
                if(buttonIndex == 1) {
                    $Parking.cancelBooking({idbooking: id}).then((data) => {
                        let index = ld.findIndex(parking.bookings, {idbooking: parseInt(id)});
                        if(index > -1) {
                            ld.pullAt(parking.bookings, [index]);
                        }
                        deferred.resolve(data);
                    }, deferred.reject);
                } else {
                    deferred.reject({reject: true});
                }
            });

            // Promise
            return deferred.promise;
        }

        function detailBooking (id) {
            let index = ld.findIndex(parking.bookings, {idbooking: parseInt(id)});

            if(index > -1) {
                let scope = $rootScope.$new();
                let booking = parking.bookings[index];
                scope.booking = booking;

                $Modal.fromTemplateUrl("app/views/bookingDetail.html", {
                    id: "bookingDetailModal",
                    title: _("bookingDetailTitle", {
                        id: booking.idbooking
                    }, "Booking"),
                    closeBtn: true,
                    removeOnHide: true,
                    scope: scope
                }).then((modal) => {
                    let element = modal.el.querySelector("#barcode");
                    let size = document.documentElement.offsetWidth / 2;
                    let startAt = new Date(booking.bookingDate).getTime();
                    let endAt = new Date(booking.bookingEndDate).getTime();
                    let value = `(${userData().cu},${booking.vehicle.plate},${booking.zone.idzone},` +
                        `${startAt},${endAt},${booking.idbooking})`; // ungly breakline "\" with es6 in emacs :(

                    value = $Encrypt.aes(value, $Parking.options.bookingSecret);

                    $Barcode.encode(value, {
                        element: element,
                        size: size < 300 ? 300 : size
                    }).then((data) => {
                        scope.barcode = data;
                        $scope.modalBookingDetail = modal;
                        $scope.modalBookingDetail.show();
                    });
                });
            }
        }

        function adminGetBalance () {
            $Parking.adminGetBalance(parking.getUserData).then((res) => {
                ld.extend(parking.userData, res.data);
                parking.formSuccess = true;
            }).catch((err) => {
                if(err.statusCode === 401) {
                    $Mobile.pageChange(config.homePage);
                }
            });
        }

        /**
         * @private
         */

        /**
         * @name pushActivePark
         * @description push in array active parking and
         * add timer for increment current minutes
         *
         * @param {array} actives array active parks
         */
        function pushActivePark (actives) {
            let minute = 60000;
            ld.forEach(actives, (active, i) => {
                let index = ld.findIndex(parking.userActivePark, {
                    idactiveParking: active.idactiveParking
                });
                active.currentMins = minute * active.currentMins;
                if(index <= -1) {
                    index = parking.userActivePark.push(active) - 1;
                } else {
                    parking.userActivePark[index].currentMins = active.currentMins;
                }

                if(parking.userActivePark[index] && !parking.userActivePark[index].timer) {
                    parking.userActivePark[index].timer = $interval(() => {
                        parking.userActivePark[index].currentMins = parking.userActivePark[index].currentMins + minute;
                    }, minute);
                }
            });
        }
        function clearTimerActive (index) {
            if(ld.isUndefined(index)) {
                if(activeTimer) {
                    $interval.cancel(activeTimer);
                }
                ld.forEach(parking.userActivePark, (active, i) => {
                    if(active.timer) {
                        $interval.cancel(active.timer);
                    }
                });
            } else if(parking.userActivePark[index].timer) {
                $interval.cancel(parking.userActivePark[index].timer);
            }
        }

    }

    // Exports
    ParkingController.$inject = [
        "$q",
        "$scope",
        "$interval",
        "$timeout",
        "$rootScope",
        "$Main",
        "$Mobile",
        "$Encrypt",
        "$Scroll",
        "$Login",
        "$Parking",
        "$Logger",
        "$App",
        "$Dialog",
        "$Modal",
        "$Utils",
        "$Hardware",
        "$Barcode",
        "$Numbro"
    ];
    module.exports = ParkingController;

})();
