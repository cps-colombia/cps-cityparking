/* global require */

// modules/parking/parkingController.js
//
// Parking factory service for parking module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const ld = require("lodash");
    const angular = require("angular");

    // Global
    var $q;
    var $window;
    var $rootScope;
    var $controller;
    var $Geo;
    var $Login ;
    var $Logger;
    var $App;
    var $Mobile;
    var $Maps;
    var $Request;
    var $Soap;
    var $Response;
    var $Numbro;
    var $Encrypt;

    var _userVehicles = [];
    var onResumeParking;
    var parkingPending;
    var userActivePark;
    var userHash;
    var userData;
    var config;
    var logger;
    var op;
    var _;

    class ParkingService {

        constructor (_$q, _$window, _$rootScope, _$controller, _$Geo, _$Login,
                     _$Logger, _$App, _$Mobile, _$Maps, _$Request, _$Soap, _$Response, _$Numbro, _$Encrypt) {
            // Dependencies
            $q = _$q;
            $window = _$window;
            $rootScope = _$rootScope;
            $controller = _$controller;
            $Geo = _$Geo;
            $Login  = _$Login ;
            $Logger = _$Logger;
            $App = _$App;
            $Mobile = _$Mobile;
            $Maps = _$Maps;
            $Request = _$Request;
            $Soap = _$Soap;
            $Response = _$Response;
            $Numbro = _$Numbro;
            $Encrypt = _$Encrypt;

            // Control vars
            _userVehicles = [];
            onResumeParking = null;
            userActivePark = this.getActivePark();
            parkingPending = null;
            userHash = $Login.getUserHash;
            userData = $Login.getUserSession;
            config = $App.config;
            logger = $Logger.getInstance();
            op = $App.ops;
            _ = $Mobile.locale;

            // Members
            this.options = ld.extend({
                vehicleComplete: true,
                alertAddVehicle: false,
                bookingSecret: $App.config.appSecret,
                role: "supervisor"
            }, config.options.parking);
        }

        // Functions
        getRoutes (marker) {
            let userPosition = $Geo.getUserPosition();
            let position;

            if(!ld.isObject(marker)) {
                $Mobile.alert(_("alertInternalError", {code: 0}));
                logger.warn("parking", "getRoutes need marker object");
                $Mobile.ga("send", "exception", "parking: getRoutes need marker object", true);
                return;
            }
            position = marker && marker.get("position");

            if(position && ld.has(userPosition, "coords")) {
                $Mobile.loading("show");

                $Maps.routesMap({from: {
                    lat: userPosition.coords.latitude,
                    lng: userPosition.coords.longitude
                }, to: {
                    lat: ld.isFunction(position.lat) ?
                        position.lat() : position.lat,
                    lng: ld.isFunction(position.lng) ?
                        position.lng() : position.lng
                }}, {
                    clean: true
                }).then((route) => {

                    $Mobile.loading("hide");
                    if (marker.infoType == "modal" && marker.close) {
                        marker.close();
                    }
                    logger.debug("parking", "getRoutes success: {0}", [JSON.stringify(route)]);
                    $Mobile.ga("send", "event", "route", "success", userData().cu);

                }).catch((err) => {

                    $Mobile.loading("hide");
                    $Mobile.alert(_("parkingRoutesProviderError"));

                    $Mobile.ga("send", "event", "route", "error", userData().cu);
                    logger.warn("parking", "getRoutes error: {0}", [JSON.stringify(err)]);
                });
            } else {
                $Mobile.loading("hide");
                $Mobile.alert(_("parkingRoutesGeneralError"));

                $Mobile.ga("send", "event", "route", "empty", userData().cu);
                logger.warn("parking", "getRoutes error: not have userPosition coords");
            }
        }

        showRates (zone) {
            let scope = $rootScope.$new();
            let ctrl = $controller("ParkingController as parking", {
                $scope: scope
            });

            if(!ctrl || !ld.isObject(zone)) {
                $Mobile.alert(_("alertInternalError", {code: 0}));
                logger.warn("wallet", "showRates need ParkingController and zone object");
                $Mobile.ga("send", "exception", "parking: showRates need ParkingController and zone object", true);
                scope.$destroy();
                return;
            }
            ctrl.showRates(zone, scope);
        }

        hasVehicles () {
            return _userVehicles.length > 0;
        }

        userVehicles (options) {
            options = ld.extend({
                fetch: false
            }, options);
            if(options.fetch) {
                this.getVehicles();
            }
            if(ld.isArray(_userVehicles) && !_userVehicles.length) {
                if($window.localStorage.uv) {
                    _userVehicles = $Encrypt.decrypt($window.localStorage.uv) || [];
                } else {
                    _userVehicles = [];
                }
            }

            if(options.plate) {
                return ld.find(ld.clone(_userVehicles, true), {plate: options.plate});
            }

            return ld.clone(_userVehicles, true) || [];
        }

        setUserVehicles (dataVehicles, action) {
            let currentVehicles = this.userVehicles() || [];
            let index;

            if(ld.isObject(dataVehicles) && action === "remove") {
                ld.pullAt(_userVehicles, ld.findIndex(_userVehicles, {
                    plate: dataVehicles.plate
                }));
                currentVehicles = _userVehicles;
            } else if(ld.isArray(dataVehicles)) {
                action = "get";
                currentVehicles = dataVehicles;
            } else if(ld.isObject(dataVehicles)) {
                index = ld.findIndex(currentVehicles, {plate: dataVehicles.plate});
                if(index > -1) {
                    action = "update";
                    currentVehicles[index] = dataVehicles;
                } else {
                    action = "new";
                    currentVehicles.push(dataVehicles);
                }
            }
            _userVehicles = currentVehicles;

            $rootScope.$broadcast(`cps:userVehicles:${action}`, {
                userVehicles: _userVehicles,
                data: dataVehicles
            });
            $window.localStorage.uv = $Encrypt.aes(currentVehicles);
        }

        getVehicles (dataObj) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Get Params
                params: {
                    op: op.parkingVehicles,
                    action: "list",
                    user_token: userHash().ut,
                    user_secret: userHash().us
                }
            };

            $Mobile.loading("show");
            angular.extend(req.params, dataObj);

            // Static data
            deferred.promise.userVehicles = this.userVehicles();

            // get vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                logger.debug("parking", "get vehicles: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;

                    this.setUserVehicles(resData.data);
                    deferred.resolve(resData.data);
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        addVehicles (dataObject) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Add Params
                params: {
                    op: op.parkingVehicles,
                    action: "add",
                    user_token: userHash().ut,
                    user_secret: userHash().us
                }
            };

            $Mobile.loading("show");
            angular.extend(req.params, dataObject);

            // add vehicles
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || res.status;

                logger.debug("parking", "add vehicles: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;

                    if(this.options.alertAddVehicle) {
                        $Mobile.alert(_("vehicleAdded"));
                    }
                    this.setUserVehicles(resData.data, "update");
                    deferred.resolve(resData);

                } else if (statusCode ===  160) {
                    $Mobile.alert(_("vehicleAlreadyRegister"));
                } else if(statusCode == 162) {
                    $Mobile.alert(_("vehicleLimitError"));
                } else {
                    $Mobile.alert(_("vehicleAddError"), {
                        code: statusCode
                    });
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        updateVehicles (dataObject) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Update Params
                params: {
                    op: op.parkingVehicles,
                    action: "update",
                    user_token: userHash().ut,
                    user_secret: userHash().us
                }
            };

            $Mobile.loading("show");
            angular.extend(req.params, dataObject);

            // update vehicles
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || res.status;

                logger.debug("parking", "update vehicles: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;

                    $Mobile.alert(_("vehicleUpdated"));
                    this.setUserVehicles(resData.data, "update");
                    deferred.resolve(resData);

                } else {
                    $Mobile.alert(_("vehicleUpdateError"));
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        deleteVehicles (dataObject) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Delete Params
                params: {
                    op: op.parkingVehicles,
                    action: "delete",
                    user_token: userHash().ut,
                    user_secret: userHash().us
                }
            };

            $Mobile.loading("show");
            angular.extend(req.params, dataObject);

            // delete vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                logger.debug("parking", "delete vehicles: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;
                    $Mobile.alert(_("vehicleRemoveSuccess"));

                    this.setUserVehicles(resData.data, "remove");
                    deferred.resolve(resData.data);

                } else if (statusCode === 161) {
                    $Mobile.alert(_("vehicleRemoveError"));
                } else {
                    $Mobile.alert(_("vehicleRemoveErrorGeneral"));
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        dialogVehicles (action, options) {
            let parkingCtrl;
            let scope;

            options = ld.extend({}, options);
            scope = (options.scope || $rootScope).$new();


            if(!options.plate && (action === "update" || action === "delete")) {
                logger.warn("parking", "dialogVehicles need plate for update or delete");
            }

            parkingCtrl = $controller("ParkingController as parking", {
                $scope: scope
            });

            if(action === "update") {
                return parkingCtrl.updateVehiclesDialog({plate: options.plate});
            } else if(action === "add") {
                return parkingCtrl.addVehiclesDialog();
            }
        }

        startPark (dataObj) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Delete Params
                params: {
                    op: op.parkingStart,
                    user_token: userHash().ut,
                    user_secret: userHash().us
                }
            };

            $Mobile.loading("show");
            angular.extend(req.params, dataObj);

            // delete vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                logger.debug("parking", "start park: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;
                    $Mobile.alert(_("parkingStartSuccess"));
                    deferred.resolve(resData.data);

                } else if (statusCode === 257) {
                    $Mobile.alert(_(("parkingStartError" + Math.abs(resData.data)), {
                        code: resData.data
                    }, "parkingStartErrorGeneral")).then(() => {
                        if(resData.data == -103) {
                            parkingPending = dataObj;
                            $Mobile.pageChange("/balance");
                        }
                    });
                } else {
                    $Mobile.alert(_("parkingStartUnexpectedError"));
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        startParkDialog (dataObj) {
            let scope = $rootScope.$new();
            let ctrl = $controller("ParkingController as parking", {
                $scope: scope
            });

            if(!ctrl || !ld.isObject(dataObj)) {
                $Mobile.alert(_("alertInternalError", {code: 0}));
                logger.warn("parking", "paymentCodeDialog need ParkingController and dataObj");
                $Mobile.ga("send", "exception",
                                "parking: paymentCodeDialog need ParkingController and dataObj", true);
                scope.$destroy();
                return;
            }

            ctrl.startParkDialog(dataObj);
        }

        endPark (dataObj) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Delete Params
                params: {
                    op: op.parkingEnd,
                    user_token: userHash().ut,
                    user_secret: userHash().us
                }
            };
            let parkIndex;

            $Mobile.loading("show");
            angular.extend(req.params, dataObj);

            // delete vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                logger.debug("parking", "end park: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;
                    $Mobile.alert(_("parkingEndSuccess"));

                    parkIndex = ld.findIndex(userActivePark, {vehiclePlate: dataObj.plate});
                    ld.pullAt(userActivePark, parkIndex);
                    $window.localStorage.pa = $Encrypt.aes(userActivePark);

                    deferred.resolve(resData.data);
                } else {
                    $Mobile.alert(_(("parkingEndError" + Math.abs(resData.data)), {
                        code: resData.data
                    }, "parkingEndErrorGeneral"));
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        activePark (dataObj) {
            let deferred = $q.defer();
            let req = { // Delete Params
                params: {
                    op: op.parkingActive,
                    user_token: userHash().ut,
                    user_secret: userHash().us
                }
            };

            angular.extend(req.params, dataObj);

            if(!req.params.background) {
                $Mobile.loading("show");
            }

            // delete vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                logger.debug("parking", "active park: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    userActivePark = resData.data || [];

                    if(onResumeParking) {
                        onResumeParking();
                    }

                    if(userActivePark.length >= 1) {
                        // Register event onResume
                        onResumeParking = $App.on("resume", (event) => {
                            $Mobile.pageChange("/parking/active");
                        });
                    }

                    $window.localStorage.pa = $Encrypt.aes(userActivePark);
                    deferred.resolve(userActivePark);
                } else {
                    deferred.reject(resData);
                }
            });
            // Promise
            return deferred.promise;
        }

        pendingPark () {
            if(ld.isObject(parkingPending)) {
                this.startPark(parkingPending).then(() => {
                    parkingPending = null;
                    $Mobile.pageChange("/parking/active");
                });
            }
        }

        getActivePark () {
            if($window.localStorage.pa) {
                userActivePark = $Encrypt.decrypt($window.localStorage.pa);
                return userActivePark;
            } else {
                return userActivePark || [];
            }
        }

        hasActivePark () {
            return this.getActivePark().length > 0;
        }

        formBooking (dataObj) {
            let scope = $rootScope.$new();
            let ctrl = $controller("ParkingController as parking", {
                $scope: scope
            });

            if(!ctrl || !ld.isObject(dataObj)) {
                $Mobile.alert(_("alertInternalError", {code: 0}));
                logger.warn("parking", "formBooking need ParkingController and dataObj");
                $Mobile.ga("send", "exception",
                                "parking: formBooking need ParkingController and dataObj", true);
                scope.$destroy();
                return;
            }

            ctrl.formBooking(dataObj, scope);
        }

        addBooking (dataObj) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Get Params
                params: {
                    op: op.parkingBooking,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    action: "register"
                }
            };

            $Mobile.loading("show");
            ld.extend(req.params, dataObj);


            if (!dataObj.fee) {
                $Mobile.loading("hide");
                $Mobile.alert(_("parkingBookingInvalidFee"));

                $Mobile.ga("send", "event", "error", "booking:invalidFee", dataObj.placeId, 400);
                return deferred.promise;
            }

            // get vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                $Mobile.loading("hide");
                logger.debug("parking", "add booking: {0}", [JSON.stringify(resData)]);

                if(statusCode === 200) {
                    success = true;
                    $Mobile.alert(_("parkingBookingSuccessRegister")).then(() => {
                        deferred.resolve(resData);
                    });
                } else {
                    $Mobile.alert(_("parkingBookingErrorGeneral"));
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        listBooking (dataObj) {
            let deferred = $q.defer();
            let req = { // Get Params
                params: {
                    op: op.parkingBooking,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    action: "list"
                }
            };

            $Mobile.loading("show");
            ld.extend(req.params, dataObj);

            // get vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                $Mobile.loading("hide");
                logger.debug("parking", "list booking: {0}", [JSON.stringify(resData)]);

                if (statusCode === 200) {
                    deferred.resolve(ld.orderBy(ld.get(resData, "data.bookings") || [], [
                        "idbooking"
                    ], ["desc"]));
                } else {
                    $Mobile.alert(_("alertUnknownError"));
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        cancelBooking (dataObj) {
            let deferred = $q.defer();
            let req = { // Get Params
                params: {
                    op: op.parkingBooking,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    idcountry: userData().country,
                    action: "cancel"
                }
            };

            $Mobile.loading("show");
            ld.extend(req.params, dataObj);

            // get vehicles data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                $Mobile.loading("hide");
                logger.debug("parking", "cancel booking: {0}", [JSON.stringify(resData)]);

                if(statusCode === 200) {
                    deferred.resolve(resData.data);
                } else if (statusCode === 126) {
                    logger.debug("parking", "booking apply fine");
                    $Mobile.ga("send", "event", "booking:fine", "apply", resData.description, statusCode);
                    deferred.resolve(resData.data);
                } else {
                    $Mobile.alert(_("alertUnknownError"));
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        adminGetBalance (dataObj) {
            let deferred = $q.defer();
            let soapData = config.webservices.soap || {};
            let userType = parseInt(userData().userType);
            let success = false;
            let req = { // Get soap Params
                params: {
                    token: soapData.token,
                    codigoCPS: dataObj.identifier
                }
            };

            $Mobile.loading("show");
            angular.extend(req.params, dataObj);

            if(userType !== $Login.USER_ROLES.admin && userType !== $Login.parseRole(this.options.role)) {
                $Mobile.loading("hide");
                $Mobile.alert(_("alertUnauthorized"));
                deferred.reject($Response.make(401));

                return deferred.promise;
            }

            // Send soap
            $Soap.post(soapData.url, op.soapGetBalance, req .params).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || res.status;
                let opData = resData.data || {};

                logger.debug("parking", "admin getBalance: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200 && opData.codigoRespuesta) {
                    opData.code = parseInt(opData.codigoRespuesta);
                    if(opData.code === 12 || opData.code === 13) {
                        success = true;
                        if(resData.data) {
                            resData.data.balance = $Numbro.formatCurrency(resData.data.saldo) || "0";
                            resData.data.lastName = resData.data.apellido;
                            resData.data.fullName = resData.data.nombre + " " + resData.data.lastName;
                            resData.data.phone = resData.data.telefono;
                        }

                        deferred.resolve(resData);

                    } else if(opData.code === 11) {
                        $Mobile.alert(_("parkingIdentifierNotFound"));
                    } else {
                        $Mobile.alert(_("alertGeneralError"));
                    }
                } else {
                    $Mobile.alert(_("alertGeneralError"));
                    deferred.reject(resData);
                }

                if(!success) {
                    deferred.reject(resData);
                }
            });

            // Promise
            return deferred.promise;
        }

        clearData () {
            $window.localStorage.removeItem("uv");
            _userVehicles = [];
        }


    }

    // Exports
    ParkingService.$inject = [
        "$q",
        "$window",
        "$rootScope",
        "$controller",
        "$Geo",
        "$Login",
        "$Logger",
        "$App",
        "$Mobile",
        "$Maps",
        "$Request",
        "$Soap",
        "$Response",
        "$Numbro",
        "$Encrypt"
    ];
    module.exports = ParkingService;

})();
