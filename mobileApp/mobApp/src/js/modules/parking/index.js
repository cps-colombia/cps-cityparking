/* global require */

// modules/parking/index.js
//
// index function for parking module
//
// 2015, CPS - Cellular Parking Systems

(() =>{
    "use strict";

    const angular = require("angular");
    const parkingRoutes = require("./parkingRoutes");
    const parkingController = require("./parkingController");
    const parkingDirective = require("./parkingDirective");
    const parkingService = require("./parkingService");
    const parkingRun = require("./parkingRun");

    angular.module("cpsParking", ["cpsMaps"]);
    var cpsParking = angular.module("cpsParking");

    // Routes
    cpsParking.config(parkingRoutes);

    // Service
    cpsParking.service("$Parking", parkingService);

    // Controller
    cpsParking.controller("ParkingController", parkingController);

    // Directive
    cpsParking.directive("cpsVehicles", parkingDirective.vehicles);
    cpsParking.directive("cpsVehicleAddForm", parkingDirective.vehicleAddForm);
    cpsParking.directive("cpsParkForm", parkingDirective.parkForm);
    cpsParking.directive("cpsUserVehicles", parkingDirective.userVehicles);
    cpsParking.directive("cpsBookingList", parkingDirective.bookingList);
    cpsParking.directive("cpsAdminBalanceForm", parkingDirective.adminBalanceForm);

    // Run
    cpsParking.run(parkingRun);

})();
