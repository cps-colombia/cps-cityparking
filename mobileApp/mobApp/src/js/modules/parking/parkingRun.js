/* global module */

// module/parking/parkingRun.js
//
// Run for cps parking module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    parkingRun.$inject = [
        "$rootScope",
        "$App",
        "$Mobile",
        "$Parking",
        "AUTH_EVENTS"
    ];
    function parkingRun ($rootScope, $App, $Mobile, $Parking, AUTH_EVENTS) {

        // Events
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function (event, data) {
            $Parking.clearData();
        });

        $rootScope.$on("cps:routeStartHome", function (event, next) {
            if(next.$home == "/parking/park" && $Parking.hasActivePark()) {
                $Mobile.pageChange("/parking/active");
            }
        });

        $rootScope.$on("cps:rechargePin:success", function (event, data) {
            $Parking.pendingPark();
        });

        $rootScope.$on("cps:mapsMarkerClick:parkingStartDialog", function (event, data) {
            $Parking.startParkDialog(ld.get(data, "marker"));
        });
        $rootScope.$on("cps:mapsMarkerClick:parkingRoutes", function (event, data) {
            $Parking.getRoutes(ld.get(data, "marker"));
        });
        $rootScope.$on("cps:mapsMarkerClick:parkingRates", function (event, data) {
            var marker = ld.get(data, "marker") || {};
            $Parking.showRates((marker.get ? marker.get("zone") : marker.zone));
        });
        $rootScope.$on("cps:mapsMarkerClick:parkingBooking", function (event, data) {
            var marker = ld.get(data, "marker") || {};
            $Parking.formBooking((marker.get ? marker.get("zone") : marker.zone));
        });
    }

    // Exports
    module.exports = parkingRun;

})();
