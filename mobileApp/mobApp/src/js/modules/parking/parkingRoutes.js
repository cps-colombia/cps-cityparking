/* global module */

// module/parking/parkingRoutes.js
//
// Routes for cps parking module
//
// 2015, CPS - Cellular Parking Systems

(() =>{
    "use strict";

    function parkingRoutes ($routeProvider, USER_ROLES) {
        $routeProvider
            .when("/vehicles", {
                templateUrl: "template/vehicles.html",
                controller: "ParkingController",
                controllerAs: "parking",
                data: {
                    title: "vehicles"
                }
            })
            .when("/parking/park", {
                templateUrl: "template/parking.html",
                controller: "ParkingController",
                controllerAs: "parking",
                data: {
                    title: "park"
                }
            })
            .when("/parking/active", {
                templateUrl: "template/parking.html",
                controller: "ParkingController",
                controllerAs: "parking",
                data: {
                    title: "activePark"
                }
            })
            .when("/parking/code", {
                templateUrl: "template/parking-code.html",
                controller: "ParkingController",
                controllerAs: "parking",
                data: {
                    title: "payment"
                }
            }).
            when("/parking/bookings", {
                templateUrl: "template/booking.html",
                controller: "ParkingController",
                controllerAs: "parking",
                data: {
                    title: "booking"
                }
            })
        // TODO: DEPRECATED URL
            .when("/parking-code", {
                templateUrl: "template/parking-code.html",
                controller: "ParkingController",
                controllerAs: "parking",
                data: {
                    title: "payment"
                }
            })
        //
            .when("/admin/balance", {
                templateUrl: "template/admin-balance.html",
                controller: "ParkingController",
                controllerAs: "parking",
                data: {
                    title: "admin",
                    roles: [USER_ROLES.admin, 11] // TODO: loginProvider to set roles by section

                }
            });
    }

    // Exports
    parkingRoutes.$inject = ["$routeProvider", "USER_ROLES"];
    module.exports = parkingRoutes;

})();
