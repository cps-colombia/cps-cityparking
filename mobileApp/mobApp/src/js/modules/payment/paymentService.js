/* global module */

// modules/payment/paymentService.js
//
// factory service for payment module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    const angular = require("angular");
    const enco = require("crypto-js/enc-utf8");
    const aes = require("crypto-js/aes");
    const ld = require("lodash");

    function paymentService ($q, $rootScope, $controller, $window, $filter, $Mobile, $Logger, $App,
                            $Login, $Encrypt, $Request, $Response, $Dialog, $Socket, $Balance, $Template) {
        var _ = $Mobile.locale;
        var op = $App.ops;
        var logger = $Logger.getInstance();
        var config = $App.config;
        var userData = $Login.getUserSession;
        var userHash = $Login.getUserHash;
        var methods = [];
        var historyStorage = null;

        // Members
        var Payment = {
            methods: getMethods,
            setMethods: setMethods,
            onInitMethod: onInitMethod,
            getHistory: getHistory,
            setHistory: setHistory,
            getHistoryStorage: getHistoryStorage,
            paymentCodeDialog: paymentCodeDialog,
            paymentCode: paymentCode,
            methodsDialog: methodsDialog
        };
        return Payment;

        // Functions
        function getMethods (id) {
            if(id) {
                return ld.find(methods, {
                    id: parseInt(id)
                });
            }
            return methods;
        }

        function setMethods (methodData) {
            if(ld.hasIn(methodData, ["id", "name"])) {
                let _onMethod = ld.isFunction(methodData.onMethod) && methodData.onMethod;
                let _onInit = ld.isFunction(methodData.onInit) && methodData.onInit;
                let _content = methodData.content;
                let _exec = ld.isFunction(methodData.exec) && methodData.exec;

                methodData.id = parseInt(methodData.id);
                if(!methodData.locale) {
                    methodData.locale = methodData.name;
                }

                methodData.onInit = (callback) => {
                    callback = callback || ld.noop;
                    if(_onInit) {
                        _onInit(callback);
                    } else {
                        callback(null, {});
                    }
                };

                methodData.onMethod = (opts, callback) => {
                    callback = callback || ld.noop;
                    if(_onMethod) {
                        _onMethod(opts, callback);
                    } else {
                        callback(null, {});
                    }
                };

                methodData.content = (opts) => {
                    if(ld.isFunction(_content)) {
                        _content = _content(opts);
                    }
                    if(ld.isObject(_content)) {
                        return ld.extend({
                            template: "",
                            templateUrl: "",
                            scope: null,
                            controller: null,
                            controllerAs: null,
                            locals: {}
                        }, ld.omit(_content, ["appendTo"]));
                    } else {
                        return;
                    }
                };

                methodData.exec = (data, callback) => {
                    callback = callback || ld.noop;
                    if(_exec) {
                        _exec(data, callback);
                    } else {
                        callback(null, data);
                    }
                };

                methodData.hasBalance = (method) => {
                    return ld.has(method, "data.balance") || ld.has(method, "balance");
                };

                if(!ld.isObject(methodData.data)) {
                    methodData.data = {};
                }

                if(!ld.find(methods, {id: methodData.id})) {
                    methods.push(methodData);
                } else {
                    logger.warn("payment", "setMethods, id method already register");
                }
            } else {
                logger.warn("payment", "setMethods, invalid data need id and name");
            }
        }

        function onInitMethod (method) {
            let index;
            if(ld.has(method, "onInit") && ld.isFunction(method.onInit)) {

                method.onInit((onInit) => {
                    index = ld.findIndex(methods, {id: parseInt(method.id)});
                    if(ld.has(onInit, "data") && index > -1) {
                        methods[index].data = onInit.data;
                    }
                });
            }

        }

        function getHistory (dataObject) {
            let deferred = $q.defer();
            let success = false;
            let req = { // Payment history Params
                params: {
                    op: op.paymentHistory,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    version: 2
                }
            };

            $Mobile.loading("show");
            angular.extend(req.params, dataObject);

            // get update data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;
                let parseData = {
                    transactions: []
                };

                logger.debug("history", JSON.stringify(resData));
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;

                    // Parse data
                    parseData = parseHistory(ld.clone(resData.data, true));
                    // Save
                    setHistory(parseData);
                    // Success promise
                    deferred.resolve(parseData);
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }


        function setHistory (historyData, options) {
            historyStorage = historyData;
            options = angular.extend({
                clean: false
            }, options);

            if(!options.clean) {
                $window.localStorage.hd = aes.encrypt(JSON.stringify(historyData), config.appsecret);
            } else {
                $window.localStorage.removeItem("hd");
            }

        }

        function getHistoryStorage () {
            // Return storage
            if(historyStorage) {
                return historyStorage;
            } else {
                if($window.localStorage.hd) {
                    historyStorage = JSON.parse(aes.decrypt($window.localStorage.hd, config.appsecret).toString(enco));

                    if(ld.has(historyStorage, "transactions") || ld.has(historyStorage, "data.transactions")) {
                        $window.localStorage.removeItem("hd");
                        historyStorage = null;
                    }
                    return historyStorage;
                } else {
                    return null;
                }
            }
        }

        function paymentCodeDialog (event, data) {
            let scope = $rootScope.$new();
            let ctrl = $controller("PaymentController as payment", {
                $scope: scope
            });
            let marker = data && data.marker;

            if(!ctrl) {
                $Mobile.alert(_("alertInternalError"));
                logger.warn("wallet", "paymentCodeDialog need PaymentController");
                return;
            }

            if(marker && marker.get) {
                ctrl.dataCode.opcode = Number(marker.get("markId"));
            }
            return ctrl.paymentCodeDialog();
        }

        function paymentCode (dataObj) {
            let deferred = $q.defer();
            let action = {
                validate: "validate",
                confirm: "confirm"
            };
            let req = { // Payment history Params
                params: {
                    action: dataObj.action,
                    op: op.paymentCode,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    device: $App.getPlatform().platformId,
                    lang: $Mobile.getLanguage()
                }
            };

            $Mobile.loading("show");

            if(!req.params.action) {
                $Mobile.loading("hide");
                logger.warn("paymentCode need valid action, validate or confirm");
                deferred.reject({error: "Invalid Action"});
                return deferred.promise;
            }
            // Extend
            dataObj.parkcode = dataObj.opcode;
            angular.extend(req.params, dataObj);


            // get update data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;
                let statusMsg = {};

                resData.code = ld.get(resData, "data.code") || ld.get(resData, "data.value") || statusCode;

                logger.debug("payment", "Payment Code: {0}", [JSON.stringify(resData)]);
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    if(req.params.action === action.confirm) {
                        $Mobile.alert(_("paymentCodeSuccessConfirm"));
                    }
                    if(ld.has(resData, "data.info.paymentValue")) {
                        resData.data.info.amount = resData.data.info.paymentValue;
                    }
                    // Success promise
                    deferred.resolve(resData);
                } else {
                    statusMsg = {
                        250: "paymentCodeErrorNullValidate",
                        251: "paymentCodeErrorTokenValidate",
                        252: "paymentCodeErrorFailValidate",
                        253: "paymentCodeErrorEmptyConfirm",
                        254: "paymentCodeErrorOfflineConfirm",
                        255: "paymentCodeErrorTokenConfirm",
                        256: "paymentCodeErrorFailConfirm",
                        257: "paymentCodeErrorRefund",
                        520: "alertUnknownError"
                    };

                    if(statusCode != 256) {
                        $Mobile.ga("send", "event", "error", "payment: paymentCode - code: " +
                                   resData.code + " statusCode: " + statusCode, resData.description, userData().idu);
                    }

                    $Mobile.alert(_(statusMsg[statusCode] || statusMsg[520], {
                        code: resData.code
                    }));

                    deferred.reject(resData);
                }
            });
            // Promise
            return deferred.promise;
        }

        function methodsDialog (dataObj) {
            let deferred = $q.defer();
            let scope = $rootScope.$new();
            let ctrl = $controller("PaymentController as payment", {
                $scope: scope
            });

            if(!ctrl) {
                $Mobile.alert(_("alertInternalError"));
                deferred.reject({code: 500});

                logger.warn("payment", "paymentMethod need PaymentController");
                return deferred.promise;
            }

            // Show dialog method payment
            return ctrl.methodsDialog(dataObj, scope);
        }

        /**
         * @private
         */
        function parseHistory (dataArray) {
            let trackId = 0;
            let parseData = [];
            let hasTransactions = ld.has(ld.first(dataArray), "transactions");

            dataArray = ld.clone(dataArray, true);

            if(hasTransactions) {
                ld.map(dataArray, (history) => {
                    history.dateFormat = $filter("date")(new Date(history.date), "M/d/yyyy");
                    history.datereg = new Date(history.date).getTime();

                    ld.map(history.transactions, (transaction) => {
                        transaction.trackId = trackId++;
                        transaction.locale = transaction.operation !== transaction.type ?
                            ld.camelCase(transaction.operation + "-" + transaction.type) :
                            ld.camelCase(transaction.operation);

                        // Convert date
                        transaction.datereg = new Date(transaction.date).getTime();
                        transaction.dateFormat = $filter("date")(new Date(transaction.date), "d/M/yyyy h:mm a");
                        transaction.hourFormat = $filter("date")(new Date(transaction.date), "h:mm:ss a");

                        // Amount convert
                        transaction.amountFormat = $filter("cpsFormat")(transaction.amount);
                        return transaction;
                    });
                    history.transactions = ld(history.transactions).uniqBy("datereg").orderBy(
                        ["datereg"], ["asc"]
                    ).value();

                    return history;
                });
                parseData = dataArray;
            } else {
                parseData = ld.reduce(dataArray, (newHistory, history, key) => {
                    if(ld.size(history) > 2) {

                        newHistory.push(ld.reduce(history, (result, operation, opkey) => {

                            if(ld.isArray(operation) && operation.length) {

                                operation = ld.reduce(operation, (transaction, tval, tkey) => {
                                    // Set new structure data
                                    tval.date = tval.dateTransaction || tval.paymentDate ||
                                        tval.endTime || tval.transactionDate || tval.activationDate ||
                                        operation.date || history.date;
                                    if(tval.date) {
                                        tval.trackId = trackId++;
                                        tval.operation = opkey;
                                        tval.locale = ld.camelCase(tval.operation);
                                        // Convert date
                                        tval.datereg = new Date(tval.date).getTime();
                                        tval.dateFormat = $filter("date")(new Date(tval.date), "M/d/yyyy h:mm a");
                                        tval.hourFormat = $filter("date")(new Date(tval.date), "h:mm:ss a");

                                        if(tval.commerceBranch || tval.zone) {
                                            tval.branch = tval.commerceBranch || tval.zone;

                                        }
                                        // set amount
                                        if(tval.price && !tval.amount && !tval.fee) {
                                            tval.amount = tval.price;
                                        }
                                        if(angular.isDefined(tval.fee)) {
                                            tval.amount = -Math.abs(tval.fee);
                                        }

                                        // operation type
                                        tval.egress = tval.amount < 0;
                                        tval.amountFormat = $filter("cpsFormat")(tval.amount);

                                        // Push type transaction
                                        transaction.push(tval);
                                    }

                                    return transaction;
                                }, []);
                                if(!result.transactions) {
                                    result.transactions = [];
                                }
                                // ld.forEach(operation, function(value){
                                // });
                                result.transactions.push(operation);

                            } else {
                                result[opkey] = operation;
                                if(opkey == "date") {
                                    result.dateFormat = $filter("date")(new Date(operation), "M/d/yyyy");
                                    result.datereg = new Date(operation).getTime();
                                }
                            }
                            result.transactions = ld(result.transactions).flatten().orderBy(
                                ["datereg"], ["asc"]
                            ).uniqBy("datereg").value();
                            return result;
                        }, {}));
                    }
                    return newHistory;
                }, []);
            }
            return ld.clone(parseData, true); // Remove $$hashKey
        }
    }

 // Exports
    paymentService.$inject = [
        "$q",
        "$rootScope",
        "$controller",
        "$window",
        "$filter",
        "$Mobile",
        "$Logger",
        "$App",
        "$Login",
        "$Encrypt",
        "$Request",
        "$Response",
        "$Dialog",
        "$Socket",
        "$Balance",
        "$Template"
    ];
    module.exports = paymentService;

})();
