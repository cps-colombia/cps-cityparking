/* global require */

// modules/payment/paymentController.js
//
// Payment modules for cps app
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const angular = require("angular");
    const ld = require("lodash");

    function PaymentController ($q, $scope, $timeout, $App, $Logger, $Mobile, $Payment, $Dialog) {
        var _ = $Mobile.locale;
        var payment = this;

        // Members
        payment.methods = $Payment.methods;
        payment.onInitMethod = $Payment.onInitMethod;
        payment.initHistory = initHistory;
        payment.getHistory = getHistory;
        payment.paymentCode = paymentCode;
        payment.paymentCodeDialog = paymentCodeDialog;
        payment.methodsDialog = methodsDialog;

        // Hisotry Params
        payment.dataHistory = {
            date: ""
        };
        // User Hisotry Data
        payment.userHistory = [];

        // Payment confirm params
        payment.dataMethod = {
            id: null
        };

        // Payment with code
        payment.dataCode = {
            opcode: null, // Operation code
            paycode: null // Payment Code
        };
        payment.dataCodeConfirm = {};

        // Functions
        function initHistory () {
            $Mobile.loading("show");
            // Prevent slow page transition fot ng-repeat
            // with big history
            $timeout(() => {
                getHistory();
            }, 100);
        }

        function getHistory (options) {
            let localHistory = $Payment.getHistoryStorage();

            options = ld.extend({}, options);

            // Show local first
            if(localHistory) {
                addHistory(localHistory, options.filter);
            }

            // get from network
            $Payment.getHistory(payment.dataHistory).then((res) => {
                addHistory(res, options.filter);
            });
        }

        function paymentCode (dialogCode) {
            let deferred = $q.defer();
            let codeValidate = ld.extend({
                action: "validate"
            }, ld.clone(payment.dataCode));


            $Payment.paymentCode(codeValidate).then((resPay) => {
                let payCode = resPay.data && resPay.data.info || {};
                let dialog;

                if(dialogCode) {
                    dialogCode.close(payCode);
                }

                ld.extend(payment.dataCodeConfirm, {
                    id: payCode.id,
                    place: payCode.placeId,
                    initDate: payCode.parkingInitDate,
                    endDate: payCode.parkingEndDate,
                    minAmount: payCode.paymentValue,
                    type: (payCode.parkingInitDate == payCode.parkingEndDate ? "services" : "parking")
                });

                dialog = $Dialog.show({
                    title: _("paymentCode"),
                    subTitle: _("paymentCodeConfirm"),
                    templateUrl: "app/views/paymentCodeConfirm.html",
                    scope: $scope,
                    buttons: [{
                        text: _("confirm"),
                        type: "button-primary",
                        hold: true,
                        onTap: function (e) {
                            return {data: payCode, dialog: dialog};
                        }
                    }, {
                        text: _("cancel"),
                        isClose: true
                    }]
                });
                // When $Dialog close
                dialog.then((res) => {
                    if(res) {
                        methodsDialog().then((payload) => {
                            payload.method.exec(res.data, (err, resMethod) => {
                                if(ld.has(resMethod, "transaction_id") || ld.has(resMethod, "reference_id")) {
                                    res.data.reference_id = resMethod.transaction_id || resMethod.reference_id;
                                }
                                $Payment.paymentCode(ld.extend({
                                    action: "confirm",
                                    value: res.data.paymentValue,
                                    placeid: res.data.placeId,
                                    initparking: res.data.parkingInitDate,
                                    endparking: res.data.parkingEndDate,
                                    pocket_id: payload.method.id
                                }, res.data)).then((resConfirm) => {
                                    payment.formSuccess = true;
                                    deferred.resolve(resConfirm);

                                }, deferred.reject);
                            });

                        }, deferred.reject);
                    } else {
                        deferred.reject({close: true});
                    }
                }, deferred.reject);
            });

            // Promise
            return deferred.promise;
        }

        function paymentCodeDialog (scope) {
            let deferred = $q.defer();
            let attrScope = scope;
            let dialog;

            scope = scope || $scope;

            dialog = $Dialog.show({
                title: _("paymentCode"),
                templateUrl: "app/views/paymentCodeForm.html",
                scope: scope || $scope,
                buttons: [{
                    text: _("cancel"),
                    isClose: true,
                    hold: true,
                    onTap: (e) => {
                        return {close: true, dialog: dialog};
                    }
                }]
            });

            scope.paymentCodeDialog = dialog;

            // When $Dialog close
            dialog.then((res) => {
                scope.paymentCodeDialog = null;

                if(!res || ld.has(res, "close")) {
                    // Clean
                    if(attrScope) {
                        scope.$destroy();
                    }
                    deferred.reject(res);
                } else {
                    deferred.resolve(res);
                }
            }, deferred.reject);

            // Promise
            return deferred.promise;
        }


        function methodsDialog (dataObj, scope) {
            let deferred = $q.defer();
            let attrScope = scope;
            let dialog;

            scope = scope || $scope;

            dialog = $Dialog.show({
                title: _("paymentMethod"),
                templateUrl: "app/views/paymentMethods.html",
                scope: scope,
                buttons: [{
                    text: _("submit"),
                    type: "button-primary",
                    hold: true,
                    onTap: (e) => {
                        if (!payment.dataMethod.id) {
                            $Mobile.alert(_("alertPaymentEmptyMethod"));
                            e.preventDefault();
                        } else {

                            return {
                                method: ld.clone(payment.dataMethod, true),
                                notify: dataObj,
                                dialog: dialog
                            };
                        }
                    }
                }, {
                    text: _("cancel"),
                    isClose: true
                }]
            });

            // When $Dialog close
            dialog.then((res) => {
                if(ld.has(res, "method")) {
                    deferred.resolve(res);
                } else if(!res || ld.has(res, "close")) {
                    deferred.reject(ld.extend({close: true}, res));
                } else if(ld.has(res, "error")) {
                    deferred.reject(res);
                } else {
                    deferred.reject(res);
                }
                // Clean
                payment.dataMethod = {id: null};
                if(attrScope) {
                    scope.$destroy();
                }
            });

            // Promise
            return deferred.promise;
        }

        // Private
        function addHistory (history, filterObj) {
            if(ld.isObject(filterObj)) {
                let newHisotry = [];
                payment.userHistory = [];
                ld.forEach(history, (opDay) => {
                    newHisotry.push(ld.filter(opDay.transactions, filterObj));
                });
                payment.userHistory = ld(newHisotry).flatten().orderBy(
                    ["datereg"],
                    [false]
                ).uniq().value();

            } else {
                ld.forEach(history, (opDay) => {
                    let indexDay = ld.findIndex(payment.userHistory, {dateFormat: opDay && opDay.dateFormat});

                    if(indexDay <= -1) {
                        payment.userHistory.push(opDay);
                    } else {
                        payment.userHistory[indexDay].balance = opDay.balance;
                        angular.forEach(opDay.transactions, (transaction) => {
                            let userTransactions = payment.userHistory[indexDay].transactions;
                            let index = ld.findIndex(userTransactions, {datereg: transaction.datereg});

                            if(index <= -1) {
                                index = ld.findIndex(userTransactions, {dateTime: transaction.dateTime});
                            }
                            if(index <= -1) {
                                $timeout(function () {
                                    payment.userHistory[indexDay].transactions.push(transaction);
                                }, 300);
                            } else {
                                payment.userHistory[indexDay].transactions[index] = transaction;
                            }
                        });
                    }
                });
            }
        }
    }

    // Exports
    PaymentController.$inject = [
        "$q",
        "$scope",
        "$timeout",
        "$App",
        "$Logger",
        "$Mobile",
        "$Payment",
        "$Dialog"
    ];
    module.exports = PaymentController;

})();
