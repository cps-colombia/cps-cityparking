/* global module */

// app/module/payment/paymentRoutes.js
//
// Routes for cps payment module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    function paymentRoutes ($routeProvider, USER_ROLES) {
        $routeProvider
            .when("/payment-history", { // TODO: DEPRECATED
                templateUrl: "template/payment-history.html",
                controller: "PaymentController",
                controllerAs: "payment",
                data: {
                    title: "history",
                    roles: [USER_ROLES.user]
                }
            })
            .when("/payment/history", {
                templateUrl: "template/payment-history.html",
                controller: "PaymentController",
                controllerAs: "payment",
                data: {
                    title: "history",
                    roles: [USER_ROLES.user]
                }
            })
            .when("/payment-code", { // TODO: DEPRECATED
                templateUrl: "template/payment-code.html",
                controller: "PaymentController",
                controllerAs: "payment",
                data: {
                    title: "paymentCode",
                    roles: [USER_ROLES.user]
                }
            })
            .when("/payment/code", {
                templateUrl: "template/payment-code.html",
                controller: "PaymentController",
                controllerAs: "payment",
                data: {
                    title: "paymentCode",
                    roles: [USER_ROLES.user]
                }
            });
    }

    // Exports
    paymentRoutes.$inject = [
        "$routeProvider",
        "USER_ROLES"
    ];
    module.exports = paymentRoutes;

})();
