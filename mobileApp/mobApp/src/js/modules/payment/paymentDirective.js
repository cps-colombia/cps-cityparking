/* global module */

// app/modules/payment/paymentDirective.js
//
// directives function for payment module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const angular = require("angular");
    const ld = require("lodash");
    const $$ = angular.element;

    function paymentHistory () {
        let paymentHistoryDirective = {
            restrict: "E",
            templateUrl: "app/views/paymentHistory.html"
        };
        return paymentHistoryDirective;
    }

    function paymentCodeForm () {
        let paymentCodeFormDirective = {
            restrict: "E",
            templateUrl: "app/views/paymentCodeForm.html"
        };
        return paymentCodeFormDirective;
    }

    paymentMethods.$inject = ["$compile", "$timeout", "$templateCache", "$Mobile", "$Template"];
    function paymentMethods ($compile, $timeout, $templateCache, $Mobile, $Template) {
        let _ = $Mobile.locale;
        let paymentMethodsDirective = {
            restrict: "E",
            template: "<div class='methods-content'>" +
                "<div class='methods-body'>" +
                "<div ng-include='templateMethods'></div>" +
                "<div class='methods-confirm' ng-if='current == steps[1]'></div>" +
                "</div>" +
                "<div class='methods-buttons'></div>" +
                "<button hm-tap='buttonNext(button, $event)' ng-hide='nextFn' " +
                "class='button' ng-class='setClass(button)' " +
                "ng-bind-html='button.text' cps-hold='button.hold'></button>" +
                "</div>",
            controller: "PaymentController",
            controllerAs: "payment",
            scope: {
                nextFn: "=",
                ngModel: "=",
                onMethod: "&",
                onChange: "&"
            },
            link: {
                pre: preLink,
                post: postLink
            }
        };
        return paymentMethodsDirective;

        // Functions
        function preLink (scope, element, attrs) {
            scope.current = null;
            scope.steps = ["methods-select", "methods-confirm"];
            scope.button = {
                isLast: false,
                text: _("next", "Next"),
                hold: true
            };

            // load methods form
            scope.templateMethods = "app/views/paymentMethods.html";
            scope.current = scope.steps[0];
            // $Template.load(
            //     "app/views/paymentMethods.html"
            // ).then(function(template) {
            //     $templateCache.put('templateMethodsCache', template);

            //     $timeout(() => {
            //         scope.selectEl = $$(element[0].querySelector("." + scope.steps[0]));
            //         if (template) {
            //             let _element = $$('<div>').html(template).contents();
            //             //scope.selectEl.html(_element);

            //             $compile(_element)(scope);
            //         }
            //     });
            // });
        }

        function postLink (scope, element, attrs, ctrl) {
            var hasSubMethod = false;
            scope.buttonNext = buttonNext;
            scope.setClass = setClass;

            if(attrs.nextFn) {
                scope.nextFn = function nextFn (button, event) {
                    buttonNext(button || {}, event);
                };
            }

            // Watch
            scope.$watch(function () {
                return ld.get(ctrl, "dataMethod.id");
            }, function (newVal, oldVal) {
                if(newVal && newVal != oldVal) {
                    let onChange = scope.onChange || ld.noop;

                    hasSubMethod = ld.has(ctrl.dataMethod, "data.info");
                    ctrl.dataMethod.hasSubMethod = hasSubMethod;
                    ctrl.dataMethod.isConfirm = !hasSubMethod ? true : scope.current == scope.steps[1];

                    onChange({
                        $method: ld.clone(ctrl.dataMethod, true)
                    });
                }
            });

            // Internal
            function buttonNext (button, event) {
                let method = ctrl.dataMethod;
                let result = {id: method.id};
                let content;

                event = event.originalEvent || event.srcEvent || event; // jquery events

                if(!method.id) {
                    $Mobile.alert(_(ld.camelCase("paymentMethodUnselect")));
                    event.preventDefault();
                }

                if(!event.defaultPrevented) {
                    content = method.content() || {};
                    result.data = ld.isFunction(content.onMethod) && content.onMethod();

                    if(hasSubMethod && !scope.confirmEl) {

                        scope.current = scope.steps[1];
                        $timeout(() => {
                            scope.confirmEl = $$(element[0].querySelector("." + scope.steps[1]));
                            content.replaceTo = scope.confirmEl;

                            $Template.compile(content).then((_confirm) => {
                                scope.confirm = _confirm;
                                ctrl.dataMethod.isConfirm = true;
                                scope.templateMethods = null;
                            });
                        });
                    } else if(hasSubMethod && !result.data) {
                        $Mobile.alert(_(ld.camelCase(`${method.locale}-unselect`)));
                        event.preventDefault();
                    } else {
                        result.data = result.data || method.data;

                        if(button.onMethod) {
                            button.onMethod(result, event);
                        } else if(scope.onMethod) {
                            scope.onMethod({$method: result, $event: event});
                        }
                        if(attrs.ngModel) {
                            scope.ngModel = result;
                        }
                    }
                }
            }

            function setClass (button) {
                let cssClass = (button.type || "button-default");
                return cssClass  + " " + (button.isLast ? "button-last-step" : "button-step");
            }
        }
    }

    // Exports
    module.exports = {
        paymentHistory: paymentHistory,
        paymentCodeForm: paymentCodeForm,
        paymentMethods: paymentMethods
    };

})();
