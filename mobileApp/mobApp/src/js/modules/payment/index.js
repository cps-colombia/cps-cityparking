/* global require */

// modules/payment/index.js
//
// index function for payment module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const angular = require("angular");
    const paymentController = require("./paymentController");
    const paymentDirective = require("./paymentDirective");
    const paymentRoutes = require("./paymentRoutes");
    const paymentRun = require("./paymentRun");
    const paymentService = require("./paymentService");

    angular.module("cpsPayment", ["cpsBalance"]);
    const cpsPayment = angular.module("cpsPayment");

    // Controller
    cpsPayment.controller("PaymentController", paymentController);

    // Services
    cpsPayment.factory("$Payment", paymentService);

    // Routes
    cpsPayment.config(paymentRoutes);

    // Run
    cpsPayment.run(paymentRun);

    // Directive
    cpsPayment.directive("cpsPaymentHistory", paymentDirective.paymentHistory);
    cpsPayment.directive("cpsPaymentCodeForm", paymentDirective.paymentCodeForm);
    cpsPayment.directive("cpsPaymentMethods", paymentDirective.paymentMethods);

})();
