// module/payment/paymentRun.js
//
// when run module registe evens
// to payment Module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    function paymentRun ($rootScope, $Balance, $Payment, AUTH_EVENTS) {

        // Events listen before controller is loaded
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, (event, data) => {
            // Destroy session
            $Payment.setHistory(null, {clean: true});
        });
        $rootScope.$on("cps:mapsMarkerClick:paymentCode", $Payment.paymentCodeDialog);

        // Add basic method
        $Payment.setMethods({
            id: 1,
            name: "personal",
            locale: "personalMethod",
            data: {
                balance: null
            },
            onInit: (callback) => {
                $Balance.getBalance();
                $rootScope.$on("cps:balanceUpdate", (event, newBalance) => {
                    // Update balance
                    callback({data: {
                        balance: newBalance.total
                    }});
                });
            }
        });

    }

    // Exports
    paymentRun.$inject = [
        "$rootScope",
        "$Balance",
        "$Payment",
        "AUTH_EVENTS"
    ];
    module.exports = paymentRun;

})();
