/* global module */

// app/module/profile/profileRoutes.js
//
// Routes for cps profile module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    profileRoutes.$inject = ["$routeProvider", "USER_ROLES"];
    function profileRoutes ($routeProvider, USER_ROLES) {
        $routeProvider
            .when("/profile", {
                url: "/profile",
                templateUrl: "template/profile.html",
                controller: "ProfileController",
                controllerAs: "profile",
                // resolve: {
                //     profileInfo: profileInfo
                // },
                data: {
                    title: "profile",
                    roles: [USER_ROLES.user]
                }
            });
    }

    profileInfo.$inject = ["$Profile"];
    function profileInfo ($Profile) {
        return $Profile.getProfile();
    }

    // Exports
    module.exports = {
        profileRoutes: profileRoutes
    };

})();
