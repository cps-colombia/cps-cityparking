/* global module */

// app/module/login/loginController.js
//
// Login controller for cps login module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    ProfileController.$inject = [
        "$scope",
        "$rootScope",
        "$route",
        "$window",
        "$App",
        "$Profile",
        "$Login",
        "$Logger",
        "$Mobile",
        "$Encrypt",
        "AUTH_EVENTS"
    ];
    function ProfileController ($scope, $rootScope, $route, $window, $App,
                               $Profile, $Login, $Logger, $Mobile, $Encrypt, AUTH_EVENTS) {
        var _ = $Mobile.locale,
            profile = this;

        // Members
        profile.initProfile = initProfile;
        profile.getCities = getCities;
        profile.getProfile = getProfile;
        profile.sendProfile = sendProfile;
        profile.editProfile = editProfile;
        profile.cancelEditProfile = cancelEditProfile;

        // Profile Data
        profile.dataProfile =  {
            country: "",
            city: "",
            city_name: "",
            msisdn: "",
            idu: "",
            address: "",
            balance: "",
            cu: "",
            login: "",
            email: "",
            userType: "",
            name: "",
            lastName: "",
            bdyear: "",
            bdmonth: "",
            bdday: "",
            bd: ""
        };

        profile.countries = {};
        profile.cities = {};

        // Control
        profile.options = $Profile.options;
        profile.isDisabled = !profile.options.alwaysEditable;
        profile.formSuccess = false;
        profile.emailExist = false;
        profile.phoneExist = false;

        // Events
        $scope.$on("cps:baseData:success", function (event, res) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // Update data
            profile.countries = $App.getCountries();
            getCities();
        });

        $scope.$on(AUTH_EVENTS.loginSuccess, function (event, data) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // Update Data Profile
            angular.extend(profile.dataProfile, $Profile.profileInfo());
        });

        $scope.$on("$destroy", function () {
            // Clean events
            $scope.$$listeners["cps:baseData:success"] = [];
            $scope.$$listeners[AUTH_EVENTS.loginSuccess] = [];
        });

        // Funtions
        function initProfile () {
            // Extend Data Profile
            angular.extend(profile.dataProfile, $Profile.profileInfo());
            profile.countries = $App.getCountries();
            profile.cities = getCities();
            getProfile({background: true});
        }

        function getCities () {
            profile.cities = $App.getCities(profile.dataProfile.country);
            return profile.cities;
        }

        function getProfile (options) {
            $Profile.getProfile(options).then(function (dataProfile) {
                profile.countries = $App.getCountries();
                profile.cities = getCities();

                angular.extend(profile.dataProfile, dataProfile);
            });
            return profile.dataProfile;
        }

        function sendProfile () {
            var currentData = profile.dataProfile,
                bd = currentData,
                dataUpdateProfile = {
                    name: currentData.name,
                    lastName: currentData.lastName,
                    address: currentData.address,
                    email: currentData.email,
                    bd: bd.bdyear + "-" + bd.bdmonth + "-" + bd.bdday,
                    country: currentData.country,
                    city: currentData.city,
                    msisdn: currentData.msisdn
                };

            $Mobile.confirm(_("alertconfirmmodifyinfo"), function (btnIndex) {
                updateProfile(btnIndex, dataUpdateProfile);
            });
        }

        function updateProfile (btnIndex, dataUpdateProfile) {
            if(parseInt(btnIndex) == 1) {
                $Profile.updateProfile(dataUpdateProfile).then(function (dataProfile) {
                    profile.formSuccess = true;
                    if(!profile.options.alwaysEditable) {
                        profile.isDisabled = true;
                    }
                    angular.extend(profile.dataProfile, dataProfile);
                }).catch(function (err) {
                    switch(err.statusCode) {
                    case 120:
                        profile.emailExist = true;
                        break;
                    case 122:
                        profile.phoneExist = true;
                        break;
                    }
                });

            }
        }

        function editProfile () {
            if(!profile.options.alwaysEditable) {
                profile.isDisabled = false;
            }
            return profile.isDisabled;
        }

        function cancelEditProfile () {
            if(!profile.options.alwaysEditable) {
                profile.isDisabled = true;
            }
            profile.formSuccess = true;
            angular.extend(profile.dataProfile, $Profile.profileInfo());
        }

    }

    // Exports
    module.exports = {
        ProfileController: ProfileController
    };

})();
