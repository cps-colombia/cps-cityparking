/* global module */

// app/modules/profile/profileService.js
//
// factory for profile module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    profileService.$inject = [
        "$q",
        "$App",
        "$Logger",
        "$Mobile",
        "$Login",
        "$Request"
    ];
    function profileService ($q, $App, $Logger, $Mobile, $Login, $Request) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance(),
            userData = $Login.getUserSession,
            userHash = $Login.getUserHash,
            userProfile = userData(),
            config = $App.config,
            profileOptions = ld.extend({
                phonePattern: "[0-9]{10}",
                phoneMin: 10,
                phoneMax: 10,
                alwaysEditable: false
            }, config.options.register, config.options.profile),
            op = $App.ops;

        // Members
        var Profile = {
            options: profileOptions,
            getProfile: getProfile,
            updateProfile: updateProfile,
            profileInfo: profileInfo
        };

        return Profile;

        // Functions
        function profileInfo () {
            return userProfile;
        }

        function getProfile (options) {
            $Mobile.loading("show", options);
            var deferred = $q.defer(),
                req = { // Request login params
                    params: {
                        op: op.profileGet,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            $Request.http(req).then(function (res) {
                var statusCode = res.data && res.data.statusCode || null;
                logger.info("profile", JSON.stringify(res.data));

                $Mobile.loading("hide", options);

                if(statusCode === 200) {
                    // Update user data
                    $Login.setUserSession(res.data.data).then(function (data) {
                        // set data
                        userProfile = data;
                        deferred.resolve(userProfile);
                    }).catch(function (err) {
                        deferred.reject(err);
                    });
                } else {
                    deferred.reject(res);
                }

            }).catch(function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function updateProfile (dataObject) {
            $Mobile.loading("show");
            var deferred = $q.defer(),
                req = { // Request update profile params
                    method: "POST",
                    params: {
                        op: op.profileUpdate,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            angular.extend(req.params, dataObject);

            $Request.http(req).then(function (res) {
                var resData = res.data || {},
                    statusCode = resData.statusCode || res.status,
                    success = false;

                $Mobile.loading("hide");

                if (statusCode === 200) {
                    success = true;
                    // Set User Data
                    $Login.setUserSession(resData.data).then(function (data) {
                        // set data
                        userProfile = data;
                        $Mobile.alert(_("alertUpdateProfileOk"));
                        deferred.resolve(userProfile);
                    }).catch(function (err) {
                        deferred.reject(err);
                    });

                } else if(statusCode === 120) {
                    success = false;
                    $Mobile.alert(_("alertEmailAlreadyRegister"));

                } else if(statusCode === 122) {
                    success = false;
                    $Mobile.alert(_("alertPhoneAlreadyRegister"));

                }

                if(!success) {
                    deferred.reject(resData);
                }
            }).catch(function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

    }

    // Export
    module.exports = {
        profileService: profileService
    };
})();
