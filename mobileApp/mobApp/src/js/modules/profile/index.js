/* global require */

// app/utils/index.js
//
// index function for utils
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        profileController = require("./profileController"),
        profileDirective = require("./profileDirective"),
        profileService = require("./profileService"),
        profileRoutes = require("./profileRoutes");

    angular.module("cpsProfile", []);
    var cpsProfile = angular.module("cpsProfile");

    // Services
    cpsProfile.factory("$Profile", profileService.profileService);

    // Directive
    cpsProfile.directive("cpsProfileForm", profileDirective.profileForm);

    // Controller
    cpsProfile.controller("ProfileController", profileController.ProfileController);

    // Routes
    cpsProfile.config(profileRoutes.profileRoutes);

})();
