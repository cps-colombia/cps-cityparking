/* global module */

// modules/profile/profileDirective.js
//
// directives function for profile module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    function profileForm () {
        var profileDirective = {
            restrict: "E",
            templateUrl: "app/views/profileForm.html"
        };
        return profileDirective;
    }

    // Exports
    module.exports = {
        profileForm: profileForm
    };


})();
