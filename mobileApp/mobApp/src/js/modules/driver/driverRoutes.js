/* global module */

// module/driver/driverRoutes.js
//
// Routes for cps driver module
//
// 2015, CPS - Cellular Driver Systems

(() => {
    "use strict";

    function driverRoutes ($routeProvider) {
        $routeProvider
            .when("/driver-list", {
                templateUrl: "template/driver.html",
                controller: "DriverController",
                controllerAs: "driver",
                data: {
                    title: "driver"
                }
            });
    }

    // Exports
    driverRoutes.$inject = ["$routeProvider"];
    module.exports = driverRoutes;

})();
