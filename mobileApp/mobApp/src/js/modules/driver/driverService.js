/* global require */

// modules/driver/driverController.js
//
// Driver factory service for driver module
//
// 2015, CPS - Cellular Driver Systems

(() => {
    "use strict";

    // Dependencies
    const ld = require("lodash");

    // Global
    var _showControls = false;
    var userHash;
    var logger;
    var config;
    var op;
    var _;

    class DriverService {

        constructor ($q, $timeout, $document, $rootScope, $compile, $Hardware,
                     $Dom, $Login, $Logger, $App, $Mobile, $Request, $Maps) {
            userHash = $Login.getUserHash;
            logger = $Logger.getInstance();
            config = $App.config;
            op = $App.ops;
            _ = $Mobile.locale;

            // Members
            this.options = ld.defaultsDeep(config.options.driver || {}, {
                zoom: 13,
                addresses: {
                    transition: "vertical"
                }
            });
            this.$q = $q;
            this.$timeout = $timeout;
            this.$document = $document;
            this.$rootScope = $rootScope;
            this.$compile = $compile;
            this.$Hardware = $Hardware;
            this.$Dom = $Dom;
            this.$Login = $Login;
            this.$Logger = $Logger;
            this.$App = $App;
            this.$Mobile = $Mobile;
            this.$Request = $Request;
            this.$Maps = $Maps;
        }

        // Functions
        showControls () {
            return _showControls;
        }

        controls (status) {
            let opts = this.options;
            let controlsStatus = {
                show: this.showAddresses,
                hide: this.hideAddresses
            }[status];

            if(controlsStatus) {
                controlsStatus.call(this);
            }

            // Action in maps
            this.$Maps.watchPosition(!_showControls);

            this.$Maps.setVisible(!_showControls).then(() => {
                this.$timeout(() => {

                    this.$Maps.setVisible(_showControls, "driver", {
                        onlyMarker: true
                    }).then(() => {
                        this.$Maps.boundsMap({
                            type: _showControls ? "visible" : "target",
                            padding: _showControls ? [0, -500] : null,
                            zoom: _showControls ? opts.zoom : null,
                            limit: 5
                        });
                    });

                }, 500);
            });
        }

        showAddresses (opts, done) {
            let scope = ld.get(opts, "scope") || this.$rootScope.$new(true);
            if (scope.removed) {
                return;
            }
            ld.extend(scope, {
                container: this.$document[0].body
            }, opts);

            let element = this.$compile("<cps-driver-addresses> </cps-driver-addresses>")(scope);
            scope.container.appendChild(element[0]);

            this.addressesScope = scope;
            this.addressesEl = element;

            _showControls = true;

            this._controlsDeregisterBackButton = this.$Hardware.registerBackButtonAction(() => {
                this.controls("hide");
            }, this.$Hardware.BACK_PRIORITY.view + 10);
            this.$Maps.refreshLayout();
        }

        hideAddresses (done) {
            let scope = this.addressesScope || {};
            let element = this.addressesEl;

            if (scope.removed) {
                return;
            }

            if (this._controlsDeregisterBackButton) {
                this._controlsDeregisterBackButton();
                this._controlsDeregisterBackButton = null;
            }
            if(element && scope.$destroy) {
                this.addressesScope.removed = true;

                scope.$destroy();
                element.remove();
            }
            _showControls = false;
            this.$Maps.refreshLayout();
        }

        register (dataObj) {
            let deferred = this.$q.defer();
            let success = false;
            let req = { // Get Params
                params: {
                    op: op.driverService,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    action: "register"
                }
            };

            this.$Mobile.loading("show");
            ld.extend(req.params, dataObj);

            // get vehicles data
            this.$Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                this.$Mobile.loading("hide");
                logger.debug("driver", "register service: {0}", [JSON.stringify(resData)]);

                if(statusCode === 200) {
                    success = true;
                    this.$Mobile.alert(_("driverSuccessRegister")).then(() => {
                        deferred.resolve(resData);
                    });
                } else if(statusCode === 123) {
                    this.$Mobile.alert(_("driverErrorRegister"));
                } else {
                    this.$Mobile.alert(_("driverErrorGeneral"));
                }

                if(!success) {
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        list (dataObj) {
            let deferred = this.$q.defer();
            let req = { // Get Params
                params: {
                    op: op.driverService,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    action: "list"
                }
            };

            this.$Mobile.loading("show");
            ld.extend(req.params, dataObj);

            // get vehicles data
            this.$Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;
                let driversList = resData.data.drivers;

                this.$Mobile.loading("hide");
                logger.debug("driver", "list services: {0}", [JSON.stringify(resData)]);

                if (statusCode === 200) {
                    ld.map(driversList, function (obj) {
                        obj.id = obj.idCityDriver;
                        obj.status = obj.idCityStatus;
                        obj.operators = obj.status.citydriverOperators || [];
                        return obj;
                    });
                    deferred.resolve(ld.orderBy(driversList, [
                        "id"
                    ], ["desc"]));
                } else {
                    this.$Mobile.alert(_("alertUnknownError"));
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }

        info (dataObj, opts) {
            let deferred = this.$q.defer();
            let req = { // Get Params
                params: {
                    op: op.driverService,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    action: "info"
                }
            };
            opts = ld.extend({
                background: false
            }, opts);

            req.background = opts.background;
            if(!req.background) {
                this.$Mobile.loading("show");
            }
            ld.extend(req.params, dataObj);

            // get vehicles data
            this.$Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                this.$Mobile.loading("hide");
                logger.debug("driver", "info prices: {0}", [JSON.stringify(resData)]);

                if(statusCode === 200) {
                    deferred.resolve(resData.data);
                } else {
                    this.$Mobile.alert(_("alertUnknownError"));
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }


        cancel (dataObj) {
            let deferred = this.$q.defer();
            let req = { // Get Params
                params: {
                    op: op.driverService,
                    user_token: userHash().ut,
                    user_secret: userHash().us,
                    action: "cancel"
                }
            };

            this.$Mobile.loading("show");
            ld.extend(req.params, dataObj);

            // get vehicles data
            this.$Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.statusCode || null;

                this.$Mobile.loading("hide");
                logger.debug("driver", "cancel service: {0}", [JSON.stringify(resData)]);

                if(statusCode === 200) {
                    deferred.resolve(resData.data);
                } else if (statusCode === 258) {
                    logger.debug("driver", "apply fine");
                    this.$Mobile.ga("send", "event", "driver:fine", "apply", resData.description, statusCode);
                    deferred.resolve(resData.data);
                } else {
                    this.$Mobile.alert(_("alertUnknownError"));
                    deferred.reject(resData);
                }

            });
            // Promise
            return deferred.promise;
        }
    }


    // Exports

    DriverService.$inject = [
        "$q",
        "$timeout",
        "$document",
        "$rootScope",
        "$compile",
        "$Hardware",
        "$Dom",
        "$Login",
        "$Logger",
        "$App",
        "$Mobile",
        "$Request",
        "$Maps"
    ];
    module.exports = DriverService;

})();
