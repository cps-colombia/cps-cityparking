/* global require */

// modules/driver/driverController.js
//
// Driver controller for cps app
//
// 2015, CPS - Cellular Driver Systems

(() => {
    "use strict";

    const ld = require("lodash");
    const asyncLib = require("async");

    // Constants
    const FILTER_ADDRESS = /^(?=.*[0-9])(?=.*\s+)(?=.*[a-zA-Z])([a-zA-Z0-9\u00C0-\u017F-.,#\s+]+)$/;
    // GLobal Vars
    var geoSpinner;
    var userData;
    var logger;
    var _;


    class DriverController {

        constructor ($q, $scope, $timeout, $Logger, $Modal, $Dialog, $Maps, $UiConfig,
                     $Geo, $Utils, $Hardware, $Numbro, $Main, $Scroll, $App, $Mobile, $Login,
                     $Driver, $Parking, $Payment) {

            // set global
            userData = $Login.getUserSession;
            logger = $Logger.getInstance();
            _ = $Mobile.locale;

            // Expose dependencies
            this.$q = $q;
            this.$scope = $scope;
            this.$timeout = $timeout;
            this.$Logger = $Logger;
            this.$Modal = $Modal;
            this.$Dialog = $Dialog;
            this.$Maps = $Maps;
            this.$Geo = $Geo;
            this.$Utils = $Utils;
            this.$Hardware = $Hardware;
            this.$Numbro = $Numbro;
            this.$Main = $Main;
            this.$Scroll = $Scroll;
            this.$App = $App;
            this.$Mobile = $Mobile;
            this.$Login = $Login;
            this.$Driver = $Driver;
            this.$Parking = $Parking;
            this.$Paymen = $Payment;

            this.controls = $Driver.controls.bind($Driver);
            this.showAddresses = $Driver.showAddresses.bind($Driver);
            // control
            this.step = 1;
            this.formSuccess = false;
            this.fetchVehicles = false;

            // Drivar data
            this.position = {
                country: userData().country_name,
                city: userData().city_name,
                origin: {
                    formatted_address: userData().city_name + ", " + userData().country_name
                },
                destination: {
                    formatted_address: null
                }
            };

            this.dataRequest = {
                plate: null,
                phone: userData().phone,
                name: userData().name,
                last_name: userData().lastName,
                display_name: userData().displayName,
                contact_name: null,
                contact_last_name: null,
                contact_phone: null,
                documents: 0,
                stops: 0,
                legals: false,
                date_service: null,
                comments: "",
                orig: ld.get(this.position, "origin.formatted_address"),
                dest: ld.get(this.position, "destination.formatted_address"),
                date: $Utils.dateObject(),
                soat: false,
                rtm: false
            };

            this.dataVehicle = {
                plate: null
            };

            this.dataPayment = {};
            this.dataServices = [];

            this.agreements = [];
            this.rates = {
                stop: null,
                stopFormat: null,
                rural: null,
                ruralFormat: null,
                city: null,
                cityFormat: null
            };
            this.fine = {
                rercent: null
            };
            this.predictionsOpts = {
                location: {
                    lat: function () {
                        return userData().city_lat;
                    },
                    lng: function () {
                        return userData().city_lng;
                    }
                },
                radius: 24210000
            };
            this.filterAddress = FILTER_ADDRESS;
            this.backButton = $UiConfig.backButton;
        }

        // Functions
        init () {
            if(!geoSpinner) {
                geoSpinner = this.$Maps.spinner();
            }

            this.$Main.baseData().then((data) => {
                this.setBasicData(data);
            });

            this.$Maps.geocoder({address: this.position.origin.formatted_address}).then((results) => {
                this.$timeout(() => {
                    this.position.origin = results[0] || this.position.origin;
                    this.$scope.$apply();
                });
            });
            this.setOrigin({withGeo: true});
        }

        hide () {
            this.$Driver.controls("hide");
            this.$Geo.clearGetPosition();
            geoSpinner.hide();
        }

        steps () {
            let onRemove = ld.noop;
            let userCountry = userData().country_name;
            let orig = ld.get(this.position, "origin.formatted_address") || "";
            let dest = ld.get(this.position, "destination.formatted_address") || "";
            let checkOrig = addressValidator(orig, 1) || {};
            let checkDest = addressValidator(dest, 1) || {};

            this._stepsDeregisterBackButton = this.$Hardware.registerBackButtonAction(
                this.prevStep.bind(this),
                this.$Hardware.BACK_PRIORITY.modal + 10
            );
            asyncLib.waterfall([
                (next) => {
                    let err;
                    ld.set(this.position, "destination.invalid", false);
                    ld.set(this.position, "origin.invalid", false);

                    if (!orig.length) {
                        this.$Mobile.alert(_("alertDriverAddressOriginNeed"));
                        err = {error: "need_address", type: "origin"};

                    } else if (!dest.length) {
                        this.$Mobile.alert(_("alertDriverAddressDestNeed"));
                        err = {error: "need_address", type: "destination"};

                    } else if (!checkOrig.valid) {
                        this.$Mobile.alert(_("alertDriverAddressOriginFormat", {
                            city: userData().city_name,
                            country: userCountry
                        }));
                        err = {error: "invalid_address", type: "origin"};

                    } else if (!checkDest.valid) {
                        this.$Mobile.alert(_("alertDriverAddressDestFormat", {
                            city: userData().city_name,
                            country: userCountry
                        }));
                        err = {error: "invalid_address", type: "destination"};

                    } else if (orig === dest) {
                        this.$Mobile.alert(_("alertDriverAddressDuplicate"));
                        err = {error: "duplicate_address", type: "destination"};

                    } else {
                        this.$Main.baseData().then((data) => {
                            this.setBasicData(data);
                        });
                    }
                    if (err) {
                        ld.set(this.position, [err.type, "invalid"], true);
                        return next(err);
                    }
                    next();
                }
            ], (err) => {
                if(err) {
                    logger.debug("driver", "reject: {0}", [JSON.stringify(err)]);
                } else {
                    this.$Geo.clearGetPosition();
                    this.$Mobile.loading("show");

                    // Set parse address
                    if(checkOrig.parse.address) {
                        this.position.origin.address = checkOrig.parse.address;
                    }
                    if(checkDest.parse.address) {
                        this.position.destination.address = checkDest.parse.address;
                    }
                    // Set country
                    if(checkOrig.cityCountry.indexOf(ld.camelCase(userCountry)) <= -1) {
                        this.position.origin.formatted_address = this.position.origin.formatted_address +
                            ", " + userCountry;
                    }

                    if(checkDest.cityCountry.indexOf(ld.camelCase(userCountry)) <= -1) {
                        this.position.destination.formatted_address = this.position.destination.formatted_address +
                            ", " + userCountry;
                    }

                    // Set address after parse
                    this.setAddress();

                    this.$timeout(() => {
                        this.$Mobile.loading("hide");

                        this.$Modal.fromTemplateUrl("app/views/driverSteps.html", {
                            id: "driverStepsModal",
                            title: _("driver"),
                            scope: this.$scope,
                            actionClass: "ng.hide",
                            actionBtn: () => {
                                this.prevStep();
                            },
                            closeBtn: () => {
                                this._onCloseSteps();
                            },
                            onClose: () => {
                                this.formSuccess = true;
                                this.fetchVehicles = false;
                                this.$timeout(() => {
                                    this.nextStep(1);
                                }, 300);
                                if (this._stepsDeregisterBackButton) {
                                    this._stepsDeregisterBackButton();
                                    this._stepsDeregisterBackButton = null;
                                }
                            }
                        }).then((modal) => {
                            modal.show();
                            this.$scope.modalSteps = modal;
                            this.fetchVehicles = true;
                        });

                        onRemove = this.$scope.$on("cps:modal:removed", (event, modal) => {
                            if(modal.id == "driverStepsModal") {
                                delete this.$scope.modalSteps;
                            }
                            // Clear event listener
                            onRemove();
                        });
                    }, 2000);
                }
            });
        }

        prevStep () {
            if (this.step === 1) {
                this._onCloseSteps();
            } else {
                this.nextStep(this.step - 1);
            }
        }
        nextStep (_step) {
            this.step = _step ? _step : this.step + 1;
            if (this.$scope.modalSteps) {
                this.$scope.modalSteps.actionClass = this.step === 1 ? "mg-hide" : this.backButton.icon();
            }
            this.$timeout(() => {
                this.$Scroll.scrollTop();
            }, 100);
        }

        sendForm () {
            let dataRequest = this.dataRequest || {};
            let dataVehicle = this.dataVehicle || {};

            this.dataRequest.documents = dataRequest.soat && dataRequest.rtm ? 1 : 0;

            if(!dataRequest.documents) {
                this.$Mobile.alert(_("alertNeedActiveDocuments"));
            } else if(!dataVehicle.description || !dataVehicle.vehicleBrand) {
                this.$Mobile.alert(_("alertUpdateVehicle")).then(() => {
                    this.getVehicle(dataVehicle.plate);
                });
            } else {
                this.info({background: true});
                this.nextStep();
            }
        }

        ratesStep () {
            if(!this.dataRequest.legals) {
                this.$Mobile.alert(_("driverLegalsAgree"));
            } else {
                this.nextStep();
            }
        }

        paymentStep (data) {
            this.dataPayment = data;
            return this.register();
        }

        paymentChage (method, event) {
            this.$scope.isLastStep = method && method.isConfirm;
            if(ld.has(method, "id")) {
                this.$scope.dataMethod = method;
            }
            if(method && method.isNext) {
                let dataMethod = ld.get(this.$scope, "dataMethod.data");
                if(ld.has(dataMethod, "balance") && dataMethod.balance < parseInt(this.rates.rural)) {
                    this.$Mobile.alert(_("driverBalanceNotEnough", {
                        amount: this.rates.ruralFormat
                    }));
                } else {
                    this.$scope.isLastStep = true;
                    this.paymentNext(null, event);
                }
            }
        }

        register () {
            let dataRequest = ld.clone(this.dataRequest) || {};
            let dataVehicle = this.dataVehicle || {};

            let promise = this.$Driver.register(ld.extend(dataRequest, {
                plate: dataVehicle.plate,
                date_service: this.$Utils.parseDate(this.dataRequest.date, {asTime: true}),
                orig: ld.get(this.position, "origin.formatted_address"),
                dest: ld.get(this.position, "destination.formatted_address"),
                pocket_id: this.dataPayment.id,
                pocket_reference: ld.get(this.dataPayment, "data.card_reference")
            })).then((res) => {
                this.formSuccess = true;

                if(this.$scope.modalSteps) {
                    // Clear modal from DOM
                    this.$scope.modalSteps.remove().then(() => {
                        this.$timeout(() => {
                            this.$Mobile.pageChange("/driver-list");
                        }, 300);
                    });
                }
                return res;
            });

            return promise;
        }

        info (id, opts) {
            let dataObj = {};
            if(ld.isObject(id)) {
                opts = id;
                id = null;
            }
            if(id) {
                dataObj.id_driver = id;
            }
            let promise = this.$Driver.info(dataObj, opts).then((data) => {
                this.setBasicData(data);
                return data;
            });
            return promise;
        }

        list () {
            let promise = this.$Driver.list().then((drivers) => {
                this.dataServices = drivers;
                return drivers;
            });
            return promise;
        }

        cancel (id) {
            let deferred = this.$q.defer();

            this.info(id).then((infoData) => {
                let confirmKey = infoData.fineFee ? "driverCancelConfirmFee" : "driverCancelConfirm";

                this.$Mobile.confirm(_(confirmKey, {
                    finePercent: infoData.finePercent,
                    fineFee: this.$Numbro.formatCurrency(infoData.fineFee),
                    serviceFee: this.$Numbro.formatCurrency(infoData.serviceFee)
                })).then((buttonIndex) => {
                    if(buttonIndex == 1) {
                        this.$Driver.cancel({id_driver: id}).then((data) => {
                            let index = ld.findIndex(this.dataServices, {idCityDriver: parseInt(id)});
                            if(index > -1) {
                                ld.pullAt(this.dataServices, [index]);
                            }
                            deferred.resolve(data);
                        }, deferred.reject);
                    } else {
                        deferred.reject({reject: true});
                    }
                });
            }, deferred.reject);

            // Promise
            return deferred.promise;
        }

        setOrigin (options) {
            let deferred = this.$q.defer();
            let userPosition = this.$Geo.getUserPosition();

            options = ld.extend({withGeo: false}, options);

            if(options.withGeo) {
                geoSpinner.show();
                this.position.origin.fetchingLocation = true;

                if(userPosition) {
                    // Provisional user mark
                    this.$Maps.geocoder(userPosition).then((results) => {
                        this.position.origin = results[0] || {};
                        this.setPosition("origin", results[0].position);

                        deferred.notify(this.position.origin);
                    });
                } else {
                    this.setPosition("origin", {
                        lat: userData().city_lat,
                        lng: userData().city_lng
                    });
                }
                this.$Geo.getCurrentPosition()
                    .then(this.$Maps.geocoder)
                    .then((results) => {
                        geoSpinner.hide();
                        this.position.origin.fetchingLocation = false;
                        this.position.origin = results[0] || {};
                        this.setPosition("origin", results[0].position);

                        deferred.resolve(this.position.origin);

                    }).catch((err) => {
                        let currentAddress = ld.clone(this.position.origin.formatted_address, true);
                        geoSpinner.hide();
                        this.position.origin.fetchingLocation = false;

                        this.position.origin.formatted_address = _("mapsErrorGettingLocation",
                                                                   "Error in getting current location");

                        this.$timeout(() => {
                            this.position.origin.formatted_address = currentAddress;
                        }, 2000);
                        deferred.reject(this.position.origin);
                    });
            } else {
                this.setPosition("origin", this.position.origin.position);
                deferred.resolve(this.position.origin);
            }
            // Promise
            return deferred.promise;
        }

        setDestination () {
            let deferred = this.$q.defer();

            this.setPosition("destination", this.position.destination.position);
            deferred.resolve(this.position.destination);
            // this.$Maps.geocoder({address: this.position.destination.formatted_address}).then((results) => {
            //     if(results && results.length) {
            //         deferred.resolve(results[0].position);
            //     } else {
            //         deferred.reject();
            //     }
            // });
            // Promise
            return deferred.promise;
        }


        setAddress (options) {
            this.setOrigin(options).then((oriPosition) => {
                this.setDestination().catch((err) => {
                    logger.warn("driver", "Error to get Destination geocode");
                });
            }, (err) => {
                logger.warn("driver", "Error to get Origin geocode");
            }, (notifyPosition) => {
                this.setPosition("origin", notifyPosition);
            });

        }

        setPosition (type, position) {
            let validType = {
                origin: "origin",
                destination: "destination"
            }[type];

            if(!validType) {
                return;
            }
            if(!position) {
                return;
            }
            this.$timeout(() => {
                this.$scope.$apply(() => {
                    this.position[validType].lat = position.latitude || position.lat;
                    this.position[validType].lng = position.longitude || position.lng;
                    this.setMarkers();
                });
            });
        }

        setMarkers () {
            let marksOpts = [];
            let origin = {};
            let destination = {};

            origin = this.position.origin;
            destination = this.position.destination;

            if(origin.lat && origin.lng) {
                marksOpts.push({
                    markerType: "user",
                    position: {
                        lat: origin.lat,
                        lng: origin.lng
                    }
                });
            }
            if(destination.lat && destination.lng) {
                marksOpts.push({
                    markId: 0,
                    icon: "static/images/cps-flag-driver.png",
                    markerType: "custom",
                    category: "driver",
                    position: {
                        lat: destination.lat,
                        lng: destination.lng
                    }
                });
            }

            this.$Maps.marksMap(marksOpts).then(() => {
                this.$Maps.boundsMap({
                    type: "visible",
                    // padding: [0, -500]
                    zoom: this.$Driver.options.zoom
                });
            });
        }

        legalsDialog () {
            this.$Modal.fromTemplateUrl("app/views/driverLegals.html", {
                id: "driverLegalModal",
                title: _("driver"),
                closeBtn: true,
                scope: this.$scope
            }).then((modal) => {
                modal.show();
                this.$scope.legalsAgree = () => {
                    this.$timeout(() => {
                        this.dataRequest.legals = true;
                    }, 100);
                    if(modal && modal.hide) {
                        modal.hide();
                    }
                };
            });
        }

        getVehicle (plate) {
            let plateVhicle  = plate || this.dataVehicle.plate;
            let dataVehicle = this.$Parking.userVehicles({plate: plateVhicle});

            if(dataVehicle) {
                this.dataVehicle = dataVehicle;

                if(plateVhicle && (!dataVehicle.description || !dataVehicle.vehicleBrand)) {
                    this.$Parking.dialogVehicles("update", {
                        plate: plateVhicle,
                        scope: this.$scope
                    }).then((data) => {
                        if(data && data.plate) {
                            this.dataVehicle = data;
                        }
                    });
                }
            }
            return this.dataVehicle;
        }

        setBasicData (data) {
            let driverData = data.driver || data;

            this.rates.stop = driverData.stopPrice;
            this.rates.rural = driverData.ruralPrice;
            this.rates.city = driverData.cityPrice;
            this.rates.stopFormat = this.$Numbro.formatCurrency(driverData.stopPrice);
            this.rates.ruralFormat = this.$Numbro.formatCurrency(driverData.ruralPrice);
            this.rates.cityFormat = this.$Numbro.formatCurrency(driverData.cityPrice);

            this.fine.percent = driverData.finePercent;

            if(driverData.agreements) {
                this.agreements = driverData.agreements;
            }
        }

        setSpinner (spinner) {
            geoSpinner = spinner;
        }

        _onCloseSteps () {
            this.$Mobile.confirm(_("driverCloseSteps")).then((buttonIndex) => {
                if (buttonIndex == 1 && this.$scope.modalSteps) {
                    this.$scope.modalSteps.hide();
                }
            });
        }
    }

    /**
     * @private
     */
    function addressValidator (address, length) {
        address = address || "";
        let regex = FILTER_ADDRESS;
        let regexCity = /.*\d\s+\w{3,}(.*?)$/;
        let cityCountry = [];
        let parse = {};

        // TODO: Basic function need a more complex and better
        if(regexCity.test(address)) {
            parse.address = address = address.replace(/\s+(?=\S*$)/, ", ");
        }
        cityCountry = getCountryCity(address) || [];
        return {
            valid: (regex.test(address) && cityCountry.length >= (length || 2)),
            cityCountry: cityCountry,
            parse: parse
        };
    }
    function getCountryCity (address) {
        return ld.reduce(ld.takeRight((address && address.split(",") || []), 2), function (result, value) {
            value = value && ld.camelCase(value.replace(/\s+/, ""));
            if(value && value.match(/^[a-zA-Z\u00C0-\u017F]*$/)) {
                result.push(value);
            }
            return result;
        }, []);
    }

    // Exports
    DriverController.$inject = [
        "$q",
        "$scope",
        "$timeout",
        "$Logger",
        "$Modal",
        "$Dialog",
        "$Maps",
        "$UiConfig",
        "$Geo",
        "$Utils",
        "$Hardware",
        "$Numbro",
        "$Main",
        "$Scroll",
        "$App",
        "$Mobile",
        "$Login",
        "$Driver",
        "$Parking",
        "$Payment"
    ];
    module.exports = DriverController;

})();
