// module/driver/driverRun.js
//
// when run module registe evens
// to driver Module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    function driverRun ($rootScope, $timeout, $Logger, $App, $Mobile, $Notify, $Menu, $Driver, $Maps, AUTH_EVENTS) {
        var _ = $Mobile.locale;

        // Events listen before controller is loaded
        $rootScope.$on("cps:initDriver", (event, data) => {
            $Menu.close();
            $Mobile.pageChange("zones");
            $Maps.ready(() => {
                $timeout(() => {
                    $Driver.controls("show");
                }, 300);
            });
        });

        $rootScope.$on("$routeChangeStart", (event, next) => {
            if($Driver.showControls()) {
                $Driver.controls("hide");
            }
        });

        $rootScope.$on(AUTH_EVENTS.loginSuccess, function (event, data) {
            // Driver static notification
            $Notify.new([{
                id: 1480019570907,
                type: "static",
                target: "receiver",
                status: "new",
                code: 200,
                op: "campaign",
                payload: {
                    page: "driver-list"
                },
                idsys_user: null,
                user_id: null,
                title: _("campaignReceiverDriver200Text"),
                body: _("driverDescription"),
                description: "new driver service",
                event: "cps:pageChange",
                action: "driver",
                datereg: Date.now()
            }]);
        });

    }
    // Exports
    driverRun.$inject = [
        "$rootScope",
        "$timeout",
        "$Logger",
        "$App",
        "$Mobile",
        "$Notify",
        "$Menu",
        "$Driver",
        "$Maps",
        "AUTH_EVENTS"
    ];
    module.exports = driverRun;

})();
