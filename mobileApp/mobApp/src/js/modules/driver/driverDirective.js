/* global module */

// app/core/driver/driverDirective.js
//
// directives function for driver module
//
// 2015, CPS - Cellular Driver Systems

(() => {
    "use strict";

    const ld = require("lodash");
    const angular = require("angular");
    const $$ = angular.element;

    function driverForm () {
        let formDirective = {
            restrict: "E",
            templateUrl: "app/views/driverForm.html",
            link: postLink
        };
        return formDirective;

        // functions
        function postLink (scope, element, attrs) {
            scope.buttonDisabled = attrs.buttonDisabled === false ||
                attrs.buttonDisabled === "false";
        }
    }

    function driverRates () {
        let ratesDirective = {
            restrict: "E",
            templateUrl: "app/views/driverRates.html"
        };
        return ratesDirective;
    }

    function driverList () {
        let listDirective = {
            restrict: "E",
            templateUrl: "app/views/driverList.html"
        };
        return listDirective;
    }

    driverAddresses.$inject = ["$timeout", "$compile", "$Dom", "$Mobile", "$Driver", "$UiConfig"];
    function driverAddresses ($timeout, $compile, $Dom, $Mobile, $Driver, $UiConfig) {
        let addressesDirective = {
            restrict: "E",
            templateUrl: "app/views/driverAddresses.html",
            controller: "DriverController",
            controllerAs: "driver",
            scope: true,
            compile: compile
        };
        return addressesDirective;

        // Function
        function compile (tElement, tAttrs) {
            tElement.addClass("driver-addresses ng-hide");
            return {
                pre: preLink,
                post: postLink
            };
        }
        function preLink (scope, element, attrs) {
            // Insert control button
            scope.buttonEl = $$("<div class='controls-driver'>" +
                                "<button hm-tap='driver.steps()' locale-id='request' id='btnRequestDriver'" +
                                "class='button button-primary' cps-is-hold>" +
                                "Request" +
                                "</button>" +
                                "</div>");
            scope.mapsEl = $$($Dom.querySelector(".maps-inside"));
            scope.mapsEl.append(scope.buttonEl);
            $$(".controls-driver").css("top", $$(".cps-maps").height() - 100);

            $compile(scope.mapsEl.contents())(scope);
        }

        function postLink (scope, element, attrs, ctrl) {
            let wrapperEl = element.children().eq(0);
            ctrl = scope.driver || ctrl;
            // let input = wrapperEl.find('input')[0];

            ctrl.setSpinner($Mobile.spinner({
                id: "driverSpinner",
                icon: "ripple",
                scope: scope,
                appendTo: $Dom.querySelector(".driver-addresses .bar-header"),
                startHidden: true
            }));
            scope.container.classList.add("driver-addresses-open");

            // Expose options
            scope.addressesOpts = $Driver.options.addresses;
            scope.addressesOpts.buttons = $UiConfig.buttons;

            // start driverBar animation, show backrop and focus the input
            $Dom.requestAnimationFrame(function () {
                if (scope.removed) {
                    return;
                }

                $timeout(function () {
                    wrapperEl.addClass("driver-addresses-in");
                    $Dom.animationComplete(wrapperEl[0]).then(() => {
                        (scope.done || ld.noop)();
                    });
                }, 20, false);
            });

            scope.$watch(() => {
                return $Driver.showControls();
            }, (newVal, oldVal) => {
                if(newVal) {
                    element.removeClass("ng-hide");
                    if(ctrl.init) {
                        ctrl.init();
                    }
                } else {
                    scope.container.classList.remove("driver-addresses-open");
                    element.addClass("ng-hide");
                }
            });

            element.on("remove", () => {
                scope.buttonEl.remove();
            });
            scope.$on("$destroy", () => {
                scope.container.classList.remove("driver-addresses-open");
                scope.buttonEl.remove();
            });
        }
    }


    // Exports
    module.exports = {
        driverForm: driverForm,
        driverRates: driverRates,
        driverList: driverList,
        driverAddresses: driverAddresses
    };

})();
