/* global require */

// modules/driver/index.js
//
// index function for driver module
//
// 2015, CPS - Cellular Driver Systems

(() => {
    "use strict";

    const angular = require("angular");
    const driverRun = require("./driverRun");
    const driverRoutes = require("./driverRoutes");
    const driverDirective = require("./driverDirective");
    const DriverController = require("./driverController");
    const DriverService = require("./driverService");

    angular.module("cpsDriver", ["cpsParking", "cpsZones"]);
    const cpsDriver = angular.module("cpsDriver");

    // Routes
    cpsDriver.config(driverRoutes);

    // Service
    cpsDriver.service("$Driver", DriverService);

    // Controller
    cpsDriver.controller("DriverController", DriverController);

    // Directive
    cpsDriver.directive("cpsDriverForm", driverDirective.driverForm);
    cpsDriver.directive("cpsDriverRates", driverDirective.driverRates);
    cpsDriver.directive("cpsDriverList", driverDirective.driverList);
    cpsDriver.directive("cpsDriverAddresses", driverDirective.driverAddresses);

    // Run
    cpsDriver.run(driverRun);

})();
