/* global require */

// modules/paymentez/paymentezController.js
//
// Paymentez modules for API integration
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const ld = require("lodash");

    function PaymentezController ($q, $scope, $App, $Logger, $Login, $Mobile, $Modal, $Dialog, $Paymentez) {
        var _ = $Mobile.locale;
        var paymentez = this;
        var logger = $Logger.getInstance();

        // Members
        paymentez.init = init;
        paymentez.listCards = listCards;
        paymentez.editCards = editCards;
        paymentez.deleteCard = deleteCard;
        paymentez.recharge = recharge;
        paymentez.payment = payment;
        paymentez.debigDialog = debigDialog;
        paymentez.userCards = $Paymentez.userCards;
        paymentez.addCard = $Paymentez.addCard;
        paymentez.hasCards = $Paymentez.hasCards;

        // User data
        paymentez.cardsData = [];

        paymentez.debitData = {
            vat: 0,
            amount: "",
            installments: 1,
            card_reference: null
        };

        // Functions
        function init () {
            listCards({background: true});
        }

        function listCards (options) {
            // From cache
            paymentez.cardsData = $Paymentez.userCards();

            // From network
            $Paymentez.listCards({}, options).then((cards) => {
                paymentez.cardsData = cards;
            });
        }

        function editCards () {
            $Modal.fromTemplateUrl("app/views/paymentezCards.html", {
                id: "paymentezEditCards",
                closeBtn: true,
                title: _("editCards")
            }).then((modal) => {
                modal.show();
            });
        }

        function deleteCard (card) {
            $Mobile.confirm(_("paymentezDeleteCardConfirm", {
                termination: card.termination
            })).then((buttonIndex) => {
                if(buttonIndex !== 2) {
                    $Paymentez.deleteCard({
                        card_reference: card.card_reference
                    }).then((res) => {
                        if(res.remove) {
                            logger.debug("paymentez", "Delete Card termination {0}", [res.remove.termination]);
                        }
                    });
                }
            });
        }

        function recharge () {
            let deferred = $q.defer();
            let options = {
                title: _("recharge"),
                operation: "recharge"
            };

            debigDialog(options, (res) => {
                let resData = res.data || {};

                $Paymentez.debitCard({
                    vat: resData.vat,
                    product_amount: resData.amount,
                    card_reference: resData.card_reference,
                    installments: parseInt(resData.installments) || 1
                }, options).then((resDebit) => {
                    if(res.dialog && res.dialog.close) {
                        res.dialog.close();
                    }
                    paymentez.formSuccess = true;
                }).catch((errDebit) => {
                    deferred.reject(errDebit);
                });
            }).catch((err) => {
                deferred.reject(err);
            });
            // Promise
            return deferred.promise;
        }

        function payment (datObj) {
            let deferred = $q.defer();
            let options = {
                title: _("payment"),
                operation: "payment"
            };

            debigDialog(options, (res) => {
                let resData = res.data || {};

                $Paymentez.debitCard({
                    vat: resData.vat,
                    product_amount: datObj.amount || resData.amount,
                    card_reference: resData.card_reference,
                    installments: parseInt(resData.installments) || 1
                }, options).then((resDebit) => {
                    if(res.dialog && res.dialog.close) {
                        res.dialog.close();
                    }
                    paymentez.formSuccess = true;
                    deferred.resolve(resDebit);
                }).catch((errDebit) => {
                    deferred.reject(errDebit);
                });
            }).catch((err) => {
                deferred.reject(err);
            });
            // Promise
            return deferred.promise;
        }

        function debigDialog (options, callback) {
            let deferred = $q.defer();
            let scope = $scope.$new();
            let dialog;

            scope.actionType = "select";
            options = ld.extend({
                title: _("recharge"),
                operation: "recharge"
            }, options);

            // Create dialog
            dialog = $Dialog.show({
                title: options.title,
                templateUrl: "app/views/paymentezCards.html",
                scope: scope,
                buttons: [{
                    text: _("btnOk"),
                    type: "button-primary",
                    hold: true,
                    onTap: (e) => {
                        // Prevent close
                        e.preventDefault();

                        // Check if have one select
                        if(!paymentez.debitData.card_reference) {
                            $Mobile.alert(_(ld.camelCase("alert-" + options.operation + "-select-card")));
                        } else {
                            paymentez.debitData.operation = options.operation;
                            // Traditional callback allow call multiple times
                            callback({data: paymentez.debitData, dialog: dialog});
                        }
                    }
                },{
                    text: _("cancel"),
                    type: "button-default",
                    isClose: true
                }]
            });

            // When $Dialog close
            dialog.then((res) => {
                if(res) {
                    deferred.resolve(res);
                } else {
                    deferred.reject(null);
                }
                // Clean
                paymentez.debitData.card_reference = null;
                scope.$destroy();
            });

            // Promise
            return deferred.promise;
        }
    }

    // Exports
    PaymentezController.$inject = [
        "$q",
        "$scope",
        "$App",
        "$Logger",
        "$Login",
        "$Mobile",
        "$Modal",
        "$Dialog",
        "$Paymentez"
    ];
    module.exports = PaymentezController;

})();
