/* global module */

// modules/paymentez/paymentezService.js
//
// factory service for paymentez module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const angular = require("angular");
    const crypto = require("crypto-js");
    const enco = require("crypto-js/enc-utf8");
    const aes = require("crypto-js/aes");
    const ld = require("lodash-contrib");

    function paymentezService ($q, $sce, $timeout, $rootScope, $window, $document, $controller, $Mobile, $Logger,
                              $App, $Login, $Oauth, $Request, $InBrowser, $Network, $ocLazyLoad, $Numbro, $Response) {
        var _ = $Mobile.locale;
        var logger = $Logger.getInstance();
        var config = $App.config;
        var userData = $Login.getUserSession;
        var webservice = config.webservices ? (config.webservices.paymentez || {}) : {};
        var paymentezOptions = ld.extend({}, config.options.paymentez);
        var readyCallbacks = [];
        var _hasDebitInProgress = null;
        var _isReady = false;
        var _hasCards = null;
        var sessionID = null;
        var userCards = [];

        // Members
        var Paymentez = {
            ready: ready,
            hasSession: hasSession,
            hasCards: hasCards,
            setHasCards:setHasCards,
            userCards: getUserCards,
            setUserCards: setUserCards,
            loadLibrary: loadLibrary,
            setSessionId: setSessionId,
            getSessionId: getSessionId,
            addCard: addCard,
            listCards: listCards,
            deleteCard: deleteCard,
            debitCard: debitCard,
            payment: payment,
            clearData: clearData
        };
        return Paymentez;

        // Functions
        function ready (cb) {
            // run through tasks to complete now that library paymentez and session is ready
            if (_isReady) {
                cb();
            } else {
                readyCallbacks.push(cb);
            }
        }
        function hasSession () {
            let deferred = $q.defer();

            if(sessionID) {
                deferred.resolve(sessionID);
            } else {
                loadLibrary().then((session) => {
                    deferred.resolve(session);
                }).catch((err) => {
                    deferred.reject(err);
                });
            }
            // Promise
            return deferred.promise;
        }

        function hasCards () {
            let hasCardStorage;
            if(_hasCards === null) {
                if($window.localStorage.uhc) {
                    hasCardStorage = aes.decrypt($window.localStorage.uhc, config.appsecret).toString(enco);
                    _hasCards = parseInt(hasCardStorage) === 1 ? true : false;
                } else {
                    _hasCards = false;
                }
            }
            return _hasCards;
        }

        function setHasCards (hasCard) {
            _hasCards = hasCard;
            $window.localStorage.uhc = aes.encrypt((hasCard ? "1" : "0"), config.appsecret);
        }

        function getUserCards () {
            return userCards;

        }
        function setUserCards (cards, action) {

            if(action === "remove" && ld.isObject(cards)) {
                ld.pullAt(userCards, ld.findIndex(userCards, {
                    card_reference: cards.card_reference
                }));
            } else if (ld.isArray(cards)) {
                userCards = cards;
            } else {
                userCards = userCards || [];
            }
            setHasCards((userCards.length ? true : false));
        }

        function loadLibrary (callbackString) {
            let deferred = $q.defer();
            let paymentezClass = "paymentez-session-id-js";
            let callback = callbackString || "paymentezSession"; // String name to global function
            let element = $document[0].getElementsByClassName(paymentezClass)[0];

            if(!element) {
                element = $document[0].createElement("div");
                element.className = paymentezClass;
                $document[0].body.appendChild(element);
            }

            $ocLazyLoad.load(
                webservice.url + `/js/paymentez-session-id.js?onload=${callback}`
            ).then((res) => {
                if(sessionID) {
                    logger.debug("paymentez", "Load paymentez library success with Session ID: {0}", [sessionID]);

                    onReady();
                    deferred.resolve(sessionID);
                } else {
                    logger.debug("paymentez", "Error get paymentez session ID: {0}", [sessionID]);
                    $Mobile.ga("send", "exception", "paymentez: get session ID error", true);

                    deferred.reject({status: 500, statusText: "Not have session ID"});
                }
            }).catch((err) => {
                logger.warn("paymentez", "Error load js library {0}", [JSON.stringify(err.error)]);
                deferred.reject(err);
            });

            // Promise
            return deferred.promise;
        }

        function setSessionId (_id) {
            sessionID = _id;
        }

        function getSessionId () {
            return sessionID;
        }

        function addCard (dataObject, options) {
            let deferred = $q.defer();
            let req = { // Paymentez add
                url: webservice.url + "/api/cc/add/",
                segTimeout: 120,
                params: {
                    session_id: sessionID,
                    application_code: webservice.code,
                    uid: userData().cu,
                    email: userData().email,
                    auth_timestamp: Date.now()
                }
            };

            $Mobile.loading("show");

            hasSession().then((session) => {
                let addCardUrl;
                req.params.session_id = session;

                // Extra params and options
                ld.extend(req.params, dataObject);
                ld.extend(req, options);

                // Get token paymentez
                req.params.auth_token = genToken(req.params);

                // Return Url for Iframe
                // deferred.resolve({url: req.url + "?" + ld.toQuery(req.params)});

                // New scope with url data
                addCardUrl = $sce.trustAsResourceUrl(req.url + "?" + ld.toQuery(req.params));

                // Open in Browser
                $InBrowser.open(addCardUrl, "_blank", {
                    hidden: "yes"
                }).then((data) => {
                    $Mobile.loading("hide");
                    $InBrowser.show();
                    deferred.resolve(data);
                }).catch((err) => {
                    $Mobile.loading("hide");
                    deferred.reject(err);
                });

            }).catch((err) => {
                deferred.reject(err);
                $Mobile.loading("hide");
            });

            // Promise
            return deferred.promise;
        }

        function listCards (dataObj, options) {
            let deferred = $q.defer();
            let req = { // list cards params
                url: webservice.url + "/api/cc/list/",
                method: "GET",
                segTimeout: 120,
                params: {
                    application_code: webservice.code,
                    uid: userData().cu,
                    auth_timestamp: Date.now()
                }
            };

            // Options
            options = angular.extend({}, options);
            if(!options.background) {
                $Mobile.loading("show");
            }

            // Extend extra params and options
            angular.extend(req.params, dataObj);
            angular.extend(req, options);

            // Get token paymentez
            req.params.auth_token = genToken(req.params);

            // get update data
            $Request.http(req).then((res) => {
                let resData = res.data || [];
                let statusCode = resData.errors ? resData.errors.code : res.status;

                logger.debug("paymentez", "Get cards: {0} - {1}", [statusCode, JSON.stringify(resData)]);

                setUserCards(resData);

                deferred.resolve(resData);
                $Mobile.loading("hide");
            });
            // Promise
            return deferred.promise;
        }

        function deleteCard (dataObject, options) {
            let deferred = $q.defer();
            let card = ld.find(getUserCards(), {
                card_reference: dataObject.card_reference
            }) || {};
            let req = { // delete card params
                url: webservice.url + "/api/cc/delete/",
                method: "POST",
                segTimeout: 120,
                transformResponse: (data, headersGetter, status) => {
                    if(!ld.isObject(data)) {
                        data = {response: data}; // Custom response transform,
                        // paymentez return "Content-Type:application/json" but return text
                    }
                    return data;
                },
                params: {
                    application_code: webservice.code,
                    uid: userData().cu,
                    auth_timestamp: Date.now()
                }
            };

            // Options
            options = ld.extend({}, options);
            if(!options.background) {
                $Mobile.loading("show");
            }

            // Extend extra params and options
            angular.extend(req.params, dataObject);
            angular.extend(req, options);

            // Get token paymentez
            req.params.auth_token = genToken(req.params);

            // get update data
            $Request.http(req).then((res) => {
                let resData = res.data || {};
                let statusCode = resData.errors ? resData.errors.code : res.status;

                logger.debug("paymentez", "Delete card: {0}", [JSON.stringify(resData)]);

                $Mobile.loading("hide");

                if(resData.response === "OK") {
                    $Mobile.alert(_("paymentezDeleteCardSuccess", {
                        termination: card.termination || "--"
                    }));
                    // Remove from var cache
                    setUserCards(card, "remove");

                    deferred.resolve({
                        cards: getUserCards,
                        remove: card
                    });
                } else {
                    $Mobile.alert(_("paymentezDeleteCardError"),{
                        termination: card.termination || "--",
                        code: statusCode
                    });
                    $Mobile.ga("send", "event", "error", "paymentez: error delete card - " +
                               JSON.stringify(resData.response));
                    deferred.reject(resData);
                }
                // setUserCards(resData);
            });
            // Promise
            return deferred.promise;
        }

        function debitCard (dataObj, options) {
            let deferred = $q.defer();
            let segTimeout = 120;
            let req = { // Debit carrd Params
                url: webservice.url + "/api/cc/debit/",
                method: "POST",
                segTimeout: segTimeout,
                params: {
                    session_id: sessionID,
                    application_code: webservice.code,
                    uid: userData().cu,
                    email: userData().email,
                    auth_timestamp: Date.now(),
                    ip_address: $Network.getIP(),
                    buyer_fiscal_number: paymentezOptions.buyerNumber,
                    installments: 1
                }
            };

            $Mobile.loading("show");

            // Options
            options = ld.extend({
                operation: "recharge"
            },options);

            // Aditionals params
            req.params.product_description = _(ld.camelCase("paymentez-" + options.operation + "-description"));
            req.params.dev_reference = (options.operation === "recharge" ? "MR-" : "MP-") + userData().cu;
            options.localeOperation = _(ld.camelCase("op-" + options.operation));

            if(!paymentezOptions.buyerNumber) {
                $Mobile.alert(_("alertInternalError"));
                $Mobile.ga("send", "exception", "paymentez: 'buyerNumber' is required", true);
                logger.error("paymentez", "paymentez option 'buyerNumber' is required");
            }

            // Check if have operation in progress
            if(_hasDebitInProgress) {
                $Mobile.loading("hide");
                $Mobile.alert(_("paymentezDebitInProgress", {
                    operation: options.localeOperation
                }));
                deferred.reject($Response.make(429));
                // Promise
                return deferred.promise;
            }
            // Set timeout to prevent send multi operations by user
            _hasDebitInProgress = $timeout(() => {
                _hasDebitInProgress = null;
            }, (1000 * segTimeout));

            // Check paymentez session and send request
            hasSession().then((session) => {
                // Set paymentez session
                req.params.session_id = session;
                // Convert to paymentez format
                dataObj.vat = parseInt(dataObj.vat).toFixed(2);
                dataObj.product_amount = parseInt(dataObj.product_amount).toFixed(2);

                // Extend extra params and options
                ld.extend(req.params, dataObj);
                ld.extend(req, options);

                // Get token paymentez
                req.params.auth_token = genToken(req.params);

                // get update data
                $Request.http(req).then((res) => {
                    let statusCode = res.data && res.data.errors ? res.data.errors.code : res.status;
                    let resData = res.data || {};

                    $Mobile.loading("hide");
                    logger.debug("paymentez", "Debit card: {0} success: {1}", [
                        options.operation, JSON.stringify(resData)
                    ]);
                    cancelDebitTimer();

                    if(resData.status === "success") {
                        if(options.operation === "recharge") {
                            $rootScope.$broadcast("cps:setBalance", {
                                action: "sum",
                                balances: resData.amount
                            });
                            $Mobile.alert(_("paymentezDebitCardSuccess", {
                                amount: $Numbro.formatCurrency(resData.amount) || 0,
                                card: resData.card_data && resData.card_data.number
                            }));
                        }
                        deferred.resolve(resData);
                    } else {
                        $Mobile.alert(_("paymentezDebitCardError" + resData.status_detail, {
                            operation: options.localeOperation,
                            code: statusCode + "" + resData.status_detail
                        }, _("paymentezDebitCardErrorGeneral")));
                        $Mobile.ga("send", "event", "error",
                                   "paymentez: " + resData.status_detail + " - " + options.operation,
                                   resData.status_detail, userData().idu);
                        deferred.reject(resData);
                    }
                });

            }).catch((err) => {
                deferred.reject(err);
            });

            // Promise
            return deferred.promise;
        }

        function payment (dataObj) {
            let deferred = $q.defer();
            let scope = $rootScope.$new();
            let ctrl =  $controller("PaymentezController as paymentez", {
                $scope: scope
            });
            let stackCtrl = ctrl;

            if(_hasDebitInProgress && stackCtrl) {
                ctrl = stackCtrl;
            }
            if(!ctrl || !ld.isObject(dataObj)) {
                $Mobile.alert(_("alertInternalError", {code: 0}));

                logger.warn("paymentez", "payment need PaymentezController and dataObj");
                $Mobile.ga("send", "exception", "paymentez: payment need PaymentezController and dataObj",
                           true, userData().idu);
                scope.$destroy();
                return deferred.promise;
            }

            ctrl.payment(dataObj).then((res) => {
                stackCtrl = null;
                deferred.resolve(res);
            }, (err) => {
                stackCtrl = null;
                deferred.reject(err);
            });

            // Promise
            return deferred.promise;
        }

        function clearData () {
            $window.localStorage.removeItem("uhc");
            _hasCards = null;
            userCards = [];
        }

        // Private
        function genToken (params) {
            let paramsList = new $Oauth.parametersElement(ld.omit(params, [
                "auth_timestamp",
                "auth_token",
                "display_name",
                "installments",
                "buyer_fiscal_number"
            ])).get();
            return crypto.SHA256(paramsList + "&" + params.auth_timestamp + "&" + webservice.key).toString();
        }

        function cancelDebitTimer () {
            $timeout.cancel(_hasDebitInProgress);
            _hasDebitInProgress = null;
        }

        /**
         * onReady
         *
         * When playmentez library is ready fire all callback on hold
         *
         */
        function onReady () {
            _isReady = true;
            for (let x = 0; x < readyCallbacks.length; x++) {
                readyCallbacks[x]();
            }
            readyCallbacks = [];
        }

    }

    // Exports
    paymentezService.$inject = [
        "$q",
        "$sce",
        "$timeout",
        "$rootScope",
        "$window",
        "$document",
        "$controller",
        "$Mobile",
        "$Logger",
        "$App",
        "$Login",
        "$Oauth",
        "$Request",
        "$InBrowser",
        "$Network",
        "$ocLazyLoad",
        "$Numbro",
        "$Response"
    ];
    module.exports = paymentezService;

})();
