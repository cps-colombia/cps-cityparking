/* global require */

// module/paymentez/paymentezRun.js
//
// when run module registe evens
// to paymentez Module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const cookie = require("cookie");
    const ld = require("lodash");

    function paymentezRun ($window, $rootScope, $Logger, $App, $Mobile, $Login,
                          $Paymentez, $InBrowser, $ocLazyLoad, $Payment, AUTH_EVENTS) {
        var _ = $Mobile.locale;
        var logger = $Logger.getInstance();
        var userData = $Login.getUserSession;

        // Global function
        $window.paymentezSession = $Paymentez.setSessionId;

        // Load Library
        $Paymentez.loadLibrary().then(() => {
            $rootScope.$on(AUTH_EVENTS.loginSuccess, (event, data) => {
                // Get user cards
                $Paymentez.listCards({}, {background: true});
            });
        });

        // Init InBrowser
        $App.ready(() => {
            $InBrowser.init();
        });

        // Register method
        $Payment.setMethods({
            id: 2,
            name: "paymentez",
            locale: "paymentezMethod",
            data: {
                info: $Paymentez.userCards()
            },
            onInit: (callback) => {
                callback = callback || ld.noop;
                // Get cache data
                callback({data: {
                    info: $Paymentez.userCards()
                }});
                // Get from network
                $Paymentez.listCards().then(() => {
                    callback({data: {
                        info: $Paymentez.userCards()
                    }});
                });
            },
            content: (opts) => {
                opts = ld.extend({
                    scope: null
                }, opts);
                if(!opts.scope) {
                    opts.scope = $rootScope.$new();
                }
                opts.scope.actionType = "select";
                return {
                    controller: "PaymentezController",
                    controllerAs: "paymentez",
                    scope: opts.scope,
                    onMethod: () => {
                        let debitData = ld.get(opts.scope, "paymentez.debitData") || {};

                        return debitData.card_reference && debitData;
                    },
                    templateUrl: "app/views/paymentezCards.html"
                };
            },
            exec: (data, callback) => {
                let withDialog = ld.get(data, "dialog") === false ? false : true;
                let operation = ld.get(data, "operation");
                let operationFun = {
                    "payment": $Paymentez.payment,
                    "recharge": $Paymentez.recharge
                }[operation];
                data = ld.omit(data, ["dialog", "operation"]);

                if (!withDialog && operation) {
                    $Paymentez.debitCard(data, {
                        operation: operation
                    }).then(function (res) {
                        callback(null, res);
                    }, callback);
                } else if (operationFun) {
                    operationFun(data).then(function (res) {
                        callback(null, res);
                    }, callback);
                } else {
                    logger.debug("paymentez", "exec need valid method");
                }
            }
        });

        // Events
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, (event, data) => {
            $Paymentez.clearData();
        });

        $rootScope.$on("cps:inBrowser:loadstop", (event, data) => {
            let isSave = (data.url || "").indexOf("api/cc/save");
            let resObj;
            let errMsg;

            if(isSave >= 0) {
                $InBrowser.executeScript({code: "document.cookie"}).then((resCookie) => {
                    $InBrowser.close();
                    if(resCookie && resCookie.length) {
                        logger.debug("paymentez", JSON.stringify(resCookie));
                        // Parse cookie to object
                        resObj = cookie.parse(resCookie[0]);
                        resObj.pmntz_error_message = resObj.pmntz_error_message || "";

                        errMsg = resObj.pmntz_error_message.split(":");

                        if(resObj.pmntz_add_success === "true" || resObj.pmntz_add_success === true) {
                            $Paymentez.setHasCards(true);
                            $Mobile.alert(_("paymentezAddCardSuccess"));
                            $Paymentez.listCards({}, {background: true});
                        } else {
                            $Mobile.alert(_("paymentezAddCardError", {
                                "error": (errMsg.length ?
                                          _(ld.camelCase("paymentez-" + errMsg[0]), null, errMsg[0]) :
                                          _("paymentezGeneralError"))
                            }));
                            $Mobile.ga("send", "exception", "paymentez: Error add card - " +
                                       resObj.pmntz_error_message, true, userData().idu);
                        }
                    } else {
                        logger.debug("paymentez", "Response Add Card Empty");
                        $Mobile.ga("send", "exception", "paymentez: Response Add Card Empty", true, userData().idu);
                        $Mobile.alert(_("paymentezAddCardResponseError"));
                    }
                }).catch((err) => {
                    $InBrowser.close();
                    logger.warn("paymentez", "Error to get response Add Card {0}", [JSON.stringify(err)]);
                    $Mobile.ga("send", "exception", "paymentez: Error to get response Add Card", true, userData().idu);
                    $Mobile.alert("paymentezErrorAddCard");
                });
            }
        });
    }

    // Exports
    paymentezRun.$inject = [
        "$window",
        "$rootScope",
        "$Logger",
        "$App",
        "$Mobile",
        "$Login",
        "$Paymentez",
        "$InBrowser",
        "$ocLazyLoad",
        "$Payment",
        "AUTH_EVENTS"
    ];
    module.exports = paymentezRun;

})();
