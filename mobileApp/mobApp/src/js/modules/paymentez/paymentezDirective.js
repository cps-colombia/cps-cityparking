/* global module */

// app/modules/paymentez/paymentezDirective.js
//
// directives function for paymentez module
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    function paymentezCards () {
        let paymentezCardsDirective = {
            restrict: "E",
            controller: "PaymentezController",
            controllerAs: "paymentez",
            templateUrl: "app/views/paymentezCards.html",
            link: postLink
        };
        return paymentezCardsDirective;

        // Functions
        function postLink (scope, element, attrs) {
            let actions = {
                modal: "modal",
                select: "select",
                default: "default"
            };
            scope.actionType = actions[attrs.actionType] || "default";
        }
    }

    function paymentezAddCard () {
        let paymentezAddCardDirective = {
            restrict: "E",
            controller: "PaymentezController",
            controllerAs: "paymentez",
            templateUrl: "app/views/paymentezAddCard.html"
        };
        return paymentezAddCardDirective;
    }

    function paymentezRechargeForm () {
        let paymentezRechargeDirective = {
            restrict: "E",
            controller: "PaymentezController",
            controllerAs: "paymentez",
            templateUrl: "app/views/paymentezRechargeForm.html"
        };
        return paymentezRechargeDirective;
    }

    // Exports
    module.exports = {
        paymentezCards: paymentezCards,
        paymentezAddCard: paymentezAddCard,
        paymentezRechargeForm: paymentezRechargeForm
    };

})();
