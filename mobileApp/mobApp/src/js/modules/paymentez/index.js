/* global require */

// modules/paymentez/index.js
//
// index function for paymentez API Integration
//
// 2015, CPS - Cellular Parking Systems

(() => {
    "use strict";

    const angular = require("angular");
    const paymentezRun = require("./paymentezRun");
    const paymentezDirective = require("./paymentezDirective");
    const paymentezController = require("./paymentezController");
    const paymentezService = require("./paymentezService");

    angular.module("cpsPaymentez", ["cpsBalance"]);
    const cpsPaymentez = angular.module("cpsPaymentez");

    // Controller
    cpsPaymentez.controller("PaymentezController", paymentezController);

    // Services
    cpsPaymentez.factory("$Paymentez", paymentezService);

    // Routes

    // Run
    cpsPaymentez.run(paymentezRun);

    // Directive
    cpsPaymentez.directive("cpsPaymentezCards", paymentezDirective.paymentezCards);
    cpsPaymentez.directive("cpsPaymentezAddCard", paymentezDirective.paymentezAddCard);
    cpsPaymentez.directive("cpsPaymentezRechargeForm", paymentezDirective.paymentezRechargeForm);

})();
