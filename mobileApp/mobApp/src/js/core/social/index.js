/* global require */

// modules/social/index.js
//
// index function for social module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        socialDirective = require("./socialDirective"),
        socialService = require("./socialService");

    angular.module("cpsSocial", []);
    var cpsSocial = angular.module("cpsSocial");

    // Service
    cpsSocial.factory("$Social", socialService);

    // Directive
    cpsSocial.directive("cpsSocial", socialDirective.social);


})();
