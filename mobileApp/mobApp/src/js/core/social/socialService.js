/* global module */

// modules/social/socialService.js
//
// social factory module for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    socialService.$inject = [
        "$Logger",
        "$App",
        "$Mobile"
    ];
    function socialService ($Logger, $App, $Mobile) {
        var logger = $Logger.getInstance();

        // Members
        var Social = {
            open: socialOpen
        };

        return Social;

        // Functions
        function socialOpen (link, options) {
            var platform = $App.getPlatform(),
                surl = link.split("/"),
                schema = surl[2].indexOf("facebook") > -1 ? "fb" : "twitter",
                finalLink;

            options = ld.extend({}, options);

            if (parseInt(platform.platformId) !== 3) {
                schema = schema + (schema === "fb" ? "://page/" : "://user?screen_name=") + surl[3];

                $Mobile.canOpen(schema, function (isInstalled) {
                    finalLink = isInstalled ? schema : link;
                    $Mobile.open(finalLink, "location=no");

                    logger.debug("social", "general: {0}", [link]);
                });
            } else {
                finalLink = schema === "fb" ? schema + ":pages?id=" + (options.id || surl[3]) : link;
                $Mobile.open(finalLink, "location=no");

                logger.debug("social", "wp: {0}", [link]);
            }
            // Send event to Analytics
            $Mobile.ga("send", "event", "social", surl[2]);
        }

    }

    // Export
    module.exports = socialService;

})();
