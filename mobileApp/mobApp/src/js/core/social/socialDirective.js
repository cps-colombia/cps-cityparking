/* global module */

// modules/social/socialDirective.js
//
// social directives function
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";


    // Members
    var socialDirective = {
        social: social
    };

    // Functions
    social.$inject = ["$Social", "$Bind"];
    function social ($Social, $Bind) {
        var socialOpenDirective = {
            restrict: "A",
            scope: true,
            link: postLink
        };
        return socialOpenDirective;

        function postLink (scope, element, attrs) {
            var options = {},
                actionFun = {
                    open: $Social.open
                };

            $Bind(scope, attrs, {
                cpsSocial: "@",
                socialId: "@",
                action: "@"
            });

            actionFun = actionFun[scope.action] || actionFun.open;
            options = {
                id: scope.socialId
            };

            element.on("click", function (e) {
                actionFun(scope.cpsSocial, options);
            });

            scope.$on("$destroy", function () {
                element.off("click");
            });
        }
    }


    // Export
    module.exports = socialDirective;

})();
