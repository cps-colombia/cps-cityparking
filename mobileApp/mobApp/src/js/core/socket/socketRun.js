/* global module */

// app/core/login/loginRoutes.js
//
// Routes for cps login module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";


    socketRun.$inject = [
        "$window",
        "$rootScope",
        "$timeout",
        "$Socket",
        "$Login",
        "AUTH_EVENTS"
    ];
    function socketRun ($window, $rootScope, $timeout, $Socket, $Login, AUTH_EVENTS) {
        // Events
        $rootScope.$on(AUTH_EVENTS.loginSuccess, function (event, data) {
            // Connect Socket
            if($Socket.config.enabled) {
                if(!$Socket.isInit()) {
                    $Socket.init();
                } else {
                    $Socket.connect();
                }
            }
        });

        $rootScope.$on(AUTH_EVENTS.notAuthenticated, function (event, data) {
            // Disconnect socket
            $Socket.disconnect(data);
        });

        $rootScope.$on("cps:network:online", function (event, type) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // Ensure Reconnect socket when change to online
            $timeout(function () {
                if($Socket.config.enabled && $Login.isAuthenticated() && !$Socket.isConnect()) {
                    $Socket.connect();
                }
            }, 1000);
        });

        $rootScope.$on("$stateChangeStart", function (event, next) {
            // if(!$Socket.isConnect() && $Socket.isInit()){
            //     console.log("not run");
            //     $Socket.connect();
            // }
        });
        // Disconnect when close app,
        // fix bug IOS keep socket connected but events not are received
        $window.onunload = $Socket.disconnect;
    }

    // Exports
    module.exports = socketRun;

})();
