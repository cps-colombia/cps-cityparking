/* global require */

// core/socket/index.js
//
// index function for socket module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        socketService = require("./socketService"),
        socketRun = require("./socketRun");

    angular.module("cpsSocket", []);
    var cpsSocket = angular.module("cpsSocket");

    // Service
    cpsSocket.provider("$Socket", socketService);

    // When run module
    cpsSocket.run(socketRun);

})();
