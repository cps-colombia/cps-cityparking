/* global module */

// core/socket/socketService.js
//
// provide service for socket module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var io = require("socket.io-client"),
        ld = require("lodash");

    function socketService () {
        var provideConfig = {},
            socketInit = false,
            readySocketCallbacks = [];

        // $get injector
        $get.$inject = [
            "$timeout",
            "$rootScope",
            "$Logger",
            "$App",
            "$Login",
            "$Utils",
            "$Mobile",
            "$Request",
            "$Network",
            "AUTH_EVENTS"
        ];

        // setter and getter
        var SocketServices = {
            ioConfig: ioConfig,
            $get: $get
        };
        return SocketServices;

        // set cordova value
        function ioConfig (_config) {
            if(ld.isObject(_config)) {
                provideConfig.url = _config.url;
                provideConfig.enabled = _config.enabled;
            }
        }

        // Getter
        function $get ($timeout, $rootScope, $Logger, $App, $Login, $Utils, $Mobile, $Request, $Network, AUTH_EVENTS) {
            var _ = $Mobile.locale,
                config = $App.config,
                userHash = $Login.getUserHash,
                userData = $Login.getUserSession,
                logger = $Logger.getInstance(),
                socketConfig = {
                    enabled: typeof provideConfig.enabled === "undefined" ?
                        config.socketEnabled : provideConfig.enabled,
                    url: typeof provideConfig.enabled === "undefined" ?
                        config.socketUrl : provideConfig.url
                },
                socketParams = {},
                currentRequest = null,
                timeoutReconnect,
                socketio;

            $App.ready(function () {
                socketParams = {
                    validate: $App.getValidation()
                };
            });

            // Members
            var Socket = {
                config: socketConfig,
                isConnect: isConnect,
                isInit: isInit,
                init: init,
                connect: connect,
                disconnect: disconnect,
                on: on,
                off: off,
                emit: emit
            };
            return Socket;

            // Functions
            /**
             * @name readySocket
             *
             * @description Promise function if socker is ready
             *
             * @return {function} callback
             */
            function readySocket (cb, force) {
                // when socket is connected return callback
                if (isConnect()) {
                    cb();
                } else {
                    // but if no connecte push in array
                    if(readySocketCallbacks.length > 10 && !force) {
                        cb({statusText: "Socket limit events", statusCode: -1, status: -1});
                    } else {
                        return readySocketCallbacks.push(cb);
                    }
                }
            }

            /**
             * @name isConnect
             *
             * @description Check if socket already connected
             *
             * @return {boolean} status connected
             */
            function isConnect () {
                return socketio ? socketio.connected : false;
            }

            /**
             * @name isInit
             *
             * @description Check if socket already initialized
             *
             * @return {boolean} status socket
             */
            function isInit () {
                return socketInit;
            }

            /**
             * @name init
             *
             * @description Initialize socket.io and listen connect events
             */
            function init () {
                if(!socketConfig.enabled) {
                    socketio = null;
                    return;
                }

                try{
                    socketio = io.connect(socketConfig.url, {
                        forceNew: true
                    });
                    socketInit = true;
                    window.socketio = socketio;
                    window.io = io;

                    // Socket Events
                    socketio.on("connect", function (res) {
                        logger.info("socket", "Socket connect to: {0}", [socketConfig.url]);
                        readySocket(function () {
                            authenticate();
                        });

                        onSocketReady();
                        // Send event
                        $rootScope.$emit(AUTH_EVENTS.socketConnect, res);
                    });

                    socketio.on("disconnect", function (res) {
                        if(res !== "transport close") {
                            logger.info("socket", "Disconnect {0}", [res]);
                            // when disconnect for unaotirize but
                            // is authenticated reconnect user
                            if($Login.isAuthenticated()) {
                                connect();
                            } else {
                                // emit root event
                                $rootScope.$emit(AUTH_EVENTS.socketDisconnect, res);
                            }
                        }
                    });

                    socketio.on("error", function (err) {
                        // when is error socket.io has a bug and stop reconection attempt
                        // then force to reconnect
                        $timeout(function () {
                            logger.warn("socket", "Error {0}", [ld.truncate(JSON.stringify(err), 200)]);
                            if(!isConnect()) {
                                socketio.io.reconnecting = false;
                                socketio.io.reconnect();
                            }
                        }, 1000);
                    });

                    // Manager Events
                    socketio.io.on("reconnect", function (res) {
                        // ensure clean timeout when connect by natural way
                        reconnectClean();
                    });

                    socketio.io.on("reconnect_attempt", function (res) {
                        // clean timeout when try connect by natural way
                        $timeout.cancel(timeoutReconnect);
                    });

                    socketio.io.on("connect_error", function (err) {
                        // desperate way of trying to connect the socket
                        reconnectForce();
                    });

                    socketio.io.on("connect_timeout", function (err) {
                        logger.debug("socket", "Connect timeout {0}", [JSON.stringify(err)]);
                    });

                } catch(err) {
                    logger.warn("socket", "Client error: {0}", [err.message]);
                }
            }

            /**
             * @name disconnect
             *
             * @description Send logout event and disconnect instance
             */
            function disconnect (data) {
                if(socketio) {
                    socketio.emit("$cps:logout", data);
                    // ensure user disconnect
                    if(isConnect()) {
                        $timeout(function () {
                            socketio.disconnect();
                        }, 2000);
                    }
                }
            }

            /**
             * @name connect
             *
             * @description connect/reconnect if socket.io instance exist
             */
            function connect () {
                if(socketio) {
                    socketio.connect();
                    // Clean
                    reconnectClean();

                }
            }

            /**
             * @name on
             *
             * @description On receive event from socket
             *
             * @param {string} event name of the event from the socket
             * @param {function} callback data of socket response
             * @param {function} options aditional options like {background: true}
             */
            function on (event, callback, options) {
                readySocket(function () {
                    if(!socketio) {
                        logger.warn("socket", "on, error to get socket.io instance event: {0}", [event]);
                        return;
                    }
                    socketio.on(event, function () {
                        var rawArgs = arguments,
                            args;

                        if(!ld.has(rawArgs, "status_code") && ld.has(rawArgs, "statusCode")) {
                            rawArgs.status_code = rawArgs.statusCode;
                        } else if(!ld.has(ld.first(rawArgs), "status_code") &&
                                  ld.has(ld.first(rawArgs), "statusCode")) {
                            rawArgs[0].status_code = rawArgs.statusCode;
                        }
                        args = parseArguments(rawArgs, options);
                        // Check if response has https errors
                        hasErrorRequest(args);

                        // fire callback
                        if(callback) {
                            $rootScope.$apply(function () {
                                callback.apply(socketio, rawArgs);
                            });
                        }
                    });
                }, true);
            }

            /**
             * @name off
             *
             * @description Unbind the specified event handler (opposite of .on()).
             *
             * @param {string} event name of the event from the socket
             * @param {function} handlerFn event handler function to unbind from the specified event.
             */
            function off (event, handlerFn) {
                readySocket(function () {
                    if(!socketio) {
                        logger.warn("socket", "on, error to get socket.io instance event: {0}", [event]);
                        return;
                    }
                    socketio.removeListener(event, handlerFn);
                }, true);
            }

            /**
             * @name emit
             *
             * @description Emite event to socket
             *
             * @param {string} event name of the event send to the socket
             * @param {string/object} req send to socket
             * @param {function} callback before emite event
             * @param {function} options aditional options like {background: true}
             */
            function emit (eventName, req, callback, options) {
                var cancelTimeout = ld.noop,
                    cbIndex;

                // Set values if come empty
                if(!req) {
                    req = {};
                }
                if(!callback) {
                    callback = ld.noop;
                }
                if(ld.isFunction(req)) {
                    if(ld.isObject(callback)) {
                        options = callback;
                    }
                    callback = req;
                    req = {};
                }
                // Extend options
                options = ld.extend({
                    background: true,
                    segTimeout: 60
                }, options);

                // Prevent similar Request
                if(currentRequest && ld.isEqual(currentRequest, {event: eventName, req: req})) {
                    return callback({status: 409, statusText: "Active similar request"});
                } else {
                    currentRequest = {event: eventName, req: req};
                }

                // Push emit if not ready
                cbIndex = readySocket(function (err) {
                    // Clean timeout on ready socket
                    cancelTimeout();
                    // Check network connection or limit cb
                    if(!$Network.hasInternet() || !socketio || err) {
                        // Show alert when is a user request
                        if(!options.background) {
                            $Mobile.alert(_("alertNetworkConnection"));
                        }
                        logger.warn("socket", "Socket connect error - event: {0} req: {1}",
                                    [eventName, JSON.stringify(req)]);
                        if(err) {
                            logger.warn("socket", err);
                        }
                        return callback({statusText: "No Network", statusCode: -1, status: -1});
                    }
                    // Set background options
                    if(ld.has(req, "background")) {
                        options.background = req.background;
                        delete req.background;
                    }
                    // Emit soket.io event
                    socketio.emit(eventName, req, socketTimeout(options.segTimeout, function () {
                        var rawArgs = arguments,
                            args = parseArguments(rawArgs, ld.extend(req, options, {eventName: eventName})),
                            res = args.res;

                        currentRequest = null;

                        if(res.status === 408) {
                            if(!options.background) {
                                $Mobile.alert(_("alertTimeout"));
                            }
                            logger.warn("socket", "Socket timeout - event: {0} req: {1}",
                                        [eventName, JSON.stringify(req)]);
                            $Mobile.ga("send", "event", "error", "Socket emit: " + res.status +
                                       "-" + res.statusText, eventName);
                        } else if(hasErrorRequest(args)) {
                            logger.warn("socket", "Socket error request - event: {0} req: {1}",
                                        [eventName, JSON.stringify(req)]);
                        }

                        // fire callback
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socketio, rawArgs);
                            }
                        });
                    }));

                });

                // timeout if no fire after socketReady
                cancelTimeout = socketTimeout(options.segTimeout, function (res) {
                    currentRequest = null;
                    if(res && res.status == 408 && !ld.isNotdefined(cbIndex)) {
                        if(!options.background) {
                            $Mobile.alert(_("alertTimeout"));
                        }
                        logger.warn("socket", "Socket pre-connect timeout - event: {0} req: {1}",
                                    [eventName, JSON.stringify(req)]);

                        ld.pullAt(readySocketCallbacks, cbIndex);
                        return callback(res);
                    }
                });
            }

            /**
             * @Private
             */

            /**
             * @name onSocketReady
             *
             * @description When socket is ready fire all callback on hold
             * @param {object} socket instance
             *
             */
            function onSocketReady () {
                // on socket is ready fire callback in array
                for (var x = 0; x < readySocketCallbacks.length; x++) {
                    // fire off all the callbacks
                    readySocketCallbacks[x]();
                }
                readySocketCallbacks = [];
            }

            /**
             * @name authenticate
             *
             * @description Emite event authenticate
             */
            function authenticate () {
                var req = {
                    app_token: config.apptoken,
                    app_secret: config.appsecret,
                    platform_id: $App.getPlatform().id,
                    user_token: userHash().ut,
                    user_secret: userHash().us
                };
                ld.extend(req, socketParams);

                if(!req.user_token || !req.user_secret) {
                    $Mobile.ga("send", "event", "socket", "empty_usertokens", userData().cu);
                    return;
                }

                emit("$cps:authenticate", req, function (res) {
                    logger.info("socket", JSON.stringify(res));
                    if(res.statusCode === 200) {
                        $rootScope.$emit(AUTH_EVENTS.socketAuth, res);
                        // update userData
                        $Login.setUserSession(res.data || {});
                    }
                });
            }

            /**
             * @name reconnectForce
             *
             * @description custom and desperate way of trying to connect the socket
             *
             * @param {object} self CpsSocket class instance
             */
            function reconnectForce () {
                $timeout.cancel(timeoutReconnect);
                timeoutReconnect = $timeout(function () {
                    connect();
                    logger.debug("socket", "Force reconnect");
                }, 6000);
            }

            /**
             * @name reconnectClean
             *
             * @description Clean timeout reconnect
             *
             * @param {object} self CpsSocket class instance
             */
            function reconnectClean () {
                // Ensure clean timeout
                if(timeoutReconnect) {
                    $timeout.cancel(timeoutReconnect);
                }
                // Enable reconnect
                socketio.io.reconnecting = false;
            }

            /**
             * @name socketTimeout
             *
             * @description check if request exceeds the time
             *
             * @param {integer} timeout seconds of max wait time
             * @param {function} callback to call after response
             */
            function socketTimeout (timeout, callback) {
                var called = false,
                    interval;
                if(typeof timeout === "function") {
                    callback = timeout;
                    timeout = 60;
                }

                interval = $timeout(function () {
                    if(called) {
                        return;
                    }
                    called = true;
                    callback({
                        status: 408,
                        statusText: "Timeout after " + timeout + " seconds."
                    });
                }, (1000 * timeout));

                return function () {
                    if(called) {
                        return;
                    }
                    called = true;
                    $timeout.cancel(interval);
                    callback.apply(this, arguments);
                };
            }

            /**
             * @name parseArguments
             * @description check if emit/on response has http errors
             *
             * @param {object} args socket events arguments
             * @param {object} req data for request to socket (optional)
             */
            function parseArguments (args, req) {
                var res,
                    statusCode,
                    data = {};

                if(ld.isObject(args) && !ld.isEmpty(args)) {
                    res = args.status_code ? args : args[0] || {};
                    res.status = res.status && parseInt(res.status) || res.status_code || 500;
                    res.statusText = res.statusText || res.description || "Empty response";
                    statusCode = res.status_code = res.status_code && parseInt(res.status_code) || res.status;
                    data = {
                        req: req || {},
                        res: res,
                        statusCode: statusCode,
                        status_code: statusCode
                    };
                }
                return data;
            }

            /**
             * @name hasErrorRequest
             *
             * @description check if emit/on response has http errors
             *
             * @param {object} data with res, req and statusCode
             */
            function hasErrorRequest (data) {
                var status = data.res && data.res.status || 500,
                    statusCode = data.statusCode,
                    eventName = data.req.eventName,
                    statusText = data.res && data.res.statusText,
                    resData = data.res && data.res.data || {};

                if($Request.hasErrorRequest(data)) {
                    // Check if has internal or http error
                    $Mobile.ga("send", "exception", "Socket error: " + status +
                               " - " + statusText + " - event: " + eventName +
                               " - network: " + $Network.getNetwork() +
                               " - onLine: " + $Network.onLine(), true);
                    logger.warn("socket", "Error $Socket with status: {0}, statusText: {1}, " +
                                "statusCode: {2}, data: {3}: req: {4}", [
                                    status, statusText, statusCode,
                                    ld.truncate(JSON.stringify(resData), 200), JSON.stringify(data.req || {})]);
                    return true;
                }
                return false;
            }

        }
    }

 // Exports
    module.exports = socketService;

})();
