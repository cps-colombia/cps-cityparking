/* global require */

// app/core/notify/notifyController.js
//
// function for cps notify module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    NotifyController.$inject = [
        "$scope",
        "$rootScope",
        "$location",
        "$Logger",
        "$Mobile",
        "$Notify"
    ];
    function NotifyController ($scope, $rootScope, $location, $Logger, $Mobile, $Notify) {
        var _ = $Mobile.locale,
            notify = this;

        // members
        notify.get = getNotify;
        notify.userNotify = $Notify.userNotify();
        notify.getNotify = getNotify;
        notify.onClickNotify = onClickNotify;
        notify.onClick = onClickNotify;
        notify.update = updateNotify;
        notify.updateNotify = updateNotify;

        // Functions
        function getNotify () {
            notify.userNotify = $Notify.userNotify();
        }

        function onClickNotify (notifyTray) {
            var notifyData = notifyTray.data.notifyData,
                redirect = ld.get(notifyData, "payload.page") || "/payment-history",
                notification = {
                    notify: notifyTray,
                    state: "fire"
                };

            if(notifyData.status !== "read") {
                // Update
                updateNotify(notifyTray);
                // Fire event if diferent to read
                $rootScope.$emit(notifyTray.data.event + ":click", notification);
            } else {
                // Redirect to section
                if(($location.path() || "").match(new RegExp(redirect))) {
                    if (notifyData.op !== "campaign") {
                        $Mobile.alert(_("notifyReadSection"));
                    } else {
                        $Mobile.alert(_(notifyTray.data.localeId + "Full", notifyData.body));
                    }
                } else {
                    if($scope.popmenu) {
                        $scope.popmenu.hide();
                    }
                    $Mobile.pageChange(redirect);
                }
            }
        }

        function updateNotify (notifyInfo) {
            var notifyTray = ld.cloneDeep(notifyInfo); // Remove $$hashKey

            $Notify.update(notifyTray, "read").then(function () {
                notifyTray.data.notifyData.status = "read";
                $Notify.set(notifyTray);
            });
        }
    }

    // Exports
    module.exports = NotifyController;

})();
