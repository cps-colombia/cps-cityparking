/* global require */

// core/notify/index.js
//
// index function for notify module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        notifyRun = require("./notifyRun"),
        notifyDirective = require("./notifyDirective"),
        notifyController = require("./notifyController"),
        notifyService = require("./notifyService");

    angular.module("cpsNotify", []);
    var cpsNotify = angular.module("cpsNotify");

    // Service
    cpsNotify.factory("$Notify", notifyService);

    // Directive
    cpsNotify.directive("cpsNotify", notifyDirective.notify);

    // Controller
    cpsNotify.controller("NotifyController", notifyController);

    // OnRun Module
    cpsNotify.run(notifyRun);

})();
