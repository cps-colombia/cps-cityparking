/* global require */

// app/core/notify/notifyService.js
//
// factory service for notify module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        asyncLib = require("async"),
        ld = require("lodash");

    notifyService.$inject = [
        "$q",
        "$rootScope",
        "$timeout",
        "$filter",
        "$window",
        "$Logger",
        "$App",
        "$Mobile",
        "$Socket",
        "$Login",
        "$Encrypt",
        "$Storage"
    ];
    function notifyService ($q, $rootScope, $timeout, $filter, $window,
                           $Logger, $App, $Mobile, $Socket, $Login, $Encrypt, $Storage) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance(),
            config = $App.config,
            userHash = $Login.getUserHash,
            localNotify = ld.get($window.cordova, "plugins.notification.local"),
            Notification = $window.Notification,
            badgeNotify = ld.get($window.cordova, "plugins.notification.badge"),
            pushNotify = $window.PushNotification,
            notifyOptions = ld.extend({gcm: {senderId: "558519297062"}}, config.options.notify),
            promptPermissionBadge = true,
            promptPermissionLocal = true,
            userNotify = {
                unread: 0,
                enabled: false,
                fromSave: false,
                notify: []
            },
            registrationId,
            push;

        // Memebers
        var Notify = {
            trayEnabled: trayEnabled,
            isTrayEnabled: isTrayEnabled,
            userNotify: getUserNotify,
            getUserNotify: getUserNotify,
            set: setNotify,
            get: getNotify,
            new: newNotify,
            update: updateNotify,
            setUserNotify: setNotify,
            getNotify: getNotify,
            newNotify: newNotify,
            updateNotify: updateNotify,
            clearAll: clearAllNotify,
            local: {
                schedule: schedule,
                update: update,
                cancel: cancel,
                cancelAll: cancelAll,
                clear: clear,
                clearAll: clearAll,
                isScheduled: isScheduled,
                hasPermission: hasPermission,
                promptForPermission: promptForPermission,
                registerPermission: registerPermission,
                getScheduledIds: getScheduledIds,
                isTriggered: isTriggered,
                getTriggeredIds: getTriggeredIds,
                getAllIds: getAllIds,
                getAll: getAll,
                get: get,
                getDefaults: getDefaults,
                setDefaults: setDefaults
            },
            push: {
                init: initPush,
                register: registerPush,
                unregister: unregisterPush,
                reset: resetPush
            },
            badge: {
                hasPermission: hasPermissionBadge,
                promptForPermission: promptForPermissionBadge,
                set: setBadge,
                get: getBadge,
                clear: clearBadge,
                increase: increaseBadge,
                decrease: decreaseBadge,
                configure: configureBadge
            }
        };
        return Notify;

        // Functions
        function trayEnabled (status) {
            userNotify.enabled = status;
            storeNotify(userNotify);
        }

        function isTrayEnabled () {
            return userNotify.enabled;
        }

        function getUserNotify (id) {
            var deferred = $q.defer(),
                _userNotify = storeNotify().userNotify;

            if(id) {
                _userNotify = ld.find(ld.get(_userNotify, "notify"), {id: id});
                if(_userNotify) {
                    deferred.resolve(_userNotify);
                } else {
                    deferred.reject();
                }
            } else {
                // TODO: make async
                return _userNotify;
            }

            return deferred.promise;
        }

        function setNotify (notifyData) {
            var deferred = $q.defer(),
                _userNotify = angular.copy(userNotify), // Prevent unnecesary watches
                unread = 0,
                indexNew;

            // Default array
            if(!notifyData) {
                notifyData = [];
            }

            // Set data
            if(!angular.isArray(notifyData)) {
                notifyData = [notifyData];
            }

            angular.forEach(notifyData, function (newData, nkey) {
                indexNew = ld.findIndex(_userNotify.notify, {id: newData.id});
                if(indexNew > -1) {
                    _userNotify.notify[indexNew] = newData;
                } else {
                    _userNotify.notify.push(newData);
                }
            });

            // Prevent duplicate notify
            _userNotify.notify = ld.uniqBy(_userNotify.notify, "id");
            // Organized by date register and limit
            _userNotify.notify = ld.orderBy(
                _userNotify.notify,
                ["data.notifyData.datereg"],
                [false]
            );
            _userNotify.notify = ld.take(_userNotify.notify, 10);

            // count unread
            asyncLib.each(_userNotify.notify, function (notifyInfo, callback) {
                if(ld.get(notifyInfo, "data.notifyData.status") !== "read") {
                    unread++;
                }
                callback();
            }, function (err) {
                setBadge(unread);
                _userNotify.unread = unread;
                // Update data notify
                userNotify = _userNotify;
                storeNotify(userNotify).then(function () {
                    deferred.resolve(userNotify);
                }, deferred.reject);
            });

            return deferred.promise;
        }

        function getNotify () {
            var deferred = $q.defer(),
                resData = [],
                req = {
                    background: true,
                    user_token: userHash().ut,
                    user_secret: userHash().us
                };

            $Socket.emit("$cps:notify:get", req, function (res) {
                $rootScope.$emit("cps:notify:get", res);

                if(res.status_code === 200 || res.status_code === 131) {
                    logger.debug("notify", "Get success, count: {0}", [resData.length]);
                    resData = res.data || [];

                    onGetNotify(resData, "rewrite");
                    deferred.resolve(res);
                } else {
                    logger.debug("notify", "Error get {0}", [JSON.stringify(res)]);
                    deferred.reject(res);
                }
            });
            return deferred.promise;
        }

        function newNotify (res) {
            var resData = ld.get(res, "data") || (ld.isArray(res) ? res : []);
            logger.debug("notify", "New notifications: {0}", [resData.length]);

            if(resData.length > 0) {
                $timeout(function () {
                    onGetNotify(resData, "new");
                });
            }
        }

        function updateNotify (notifyData, status) {
            var deferred = $q.defer(),
                notify = angular.copy(notifyData), // Remove $$hashKey
                oldData = notify && notify.data && angular.copy(notify.data.notifyData) || {},
                errMsg = "updateNotify error need data and data.notifyData";

            if(ld.has(notify, "data.notifyData")) {
                var res = notify.data.notifyData;
                if (notify.data.notifyData.type === "static") {
                    logger.debug("notify", "update static {0}", [
                        JSON.stringify(res)
                    ]);
                    deferred.resolve(res);
                    return deferred.promise;
                }
                // Update status tray
                $Socket.emit("$cps:notify:update", {
                    status: status,
                    notify: [{
                        id: oldData.id,
                        idsys_user: oldData.idsys_user
                    }]
                }, function (res) {
                    // Log
                    logger.debug("notify", "update {0}", [JSON.stringify(res)]);
                    if(status == "read") {
                        cancel(notify.id);
                    }
                    deferred.resolve(res);
                });
            } else {
                logger.warn("notify", errMsg);
                deferred.reject({error: errMsg});
            }
            // Promise
            return deferred.promise;
        }

        function clearAllNotify () {
            var deferred = $q.defer();
            userNotify.unread = 0;
            userNotify.notify = [];

            storeNotify(userNotify).then(function () {
                clearBadge().then(deferred.resolve, deferred.reject);
            }, deferred.reject);
            return deferred.promise;
        }

        /**
         * Local notification
         */
        function schedule (options, scope) {
            var deferred = $q.defer();
            var results = [];
            scope = scope || null;

            if(localNotify) {
                hasPermission(scope).then(function () {
                    localNotify.schedule(options, function (result) {
                        deferred.resolve(result);
                    }, scope);
                }, deferred.reject);
            } else {
                if(angular.isArray(options)) {
                    angular.forEach(options, function (value, key) {
                        value.body = value.text;
                        results.push(new Notification(value));
                    });
                } else {
                    options.body = options.text;
                    results.push(new Notification(options));
                }
                deferred.resolve("alert");
            }
            return deferred.promise;
        }

        function update (options, scope) {
            var deferred = $q.defer();
            scope = scope || null;
            if(localNotify) {
                localNotify.update(options, function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function clear (id, scope) {
            var deferred = $q.defer();
            scope = scope || null;
            if(localNotify) {
                localNotify.clear(id, function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function clearAll (scope) {
            var deferred = $q.defer();
            scope = scope || null;
            if(localNotify) {
                localNotify.clearAll(function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function cancel (id, scope) {
            var deferred = $q.defer();
            scope = scope || null;
            if(localNotify) {
                localNotify.cancel(id, function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function cancelAll (scope) {
            var deferred = $q.defer();
            scope = scope || null;
            if(localNotify) {
                localNotify.cancelAll(function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function isScheduled (id, scope) {
            var deferred = $q.defer();
            scope = scope || null;
            if(localNotify) {
                localNotify.isScheduled(id, function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function hasPermission (scope) {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.hasPermission(function (permission) {
                    if (permission) {
                        deferred.resolve(true);
                    } else {
                        if(promptPermissionLocal) {
                            promptForPermission();
                        }
                        logger.warn("notify", "local notification you do not have permission");
                        deferred.reject("You do not have permission");
                    }
                }, scope);
            } else if (Notification && Notification.permission === "granted") {
                deferred.resolve(true);
            } else if (Notification && Notification.permission !== "denied") {
                if(promptPermissionLocal) {
                    promptForPermission();
                }
                logger.warn("notify", "local notification you do not have permission");
                deferred.reject("You do not have permission");
            } else {
                deferred.reject(false);
            }
            return deferred.promise;
        }

        function promptForPermission () {
            var deferred = $q.defer();
            promptPermissionLocal = false;

            if(localNotify) {
                localNotify.promptForPermission(function (result) {
                    if(result) {
                        deferred.resolve(result);
                    } else {
                        deferred.reject(result);
                    }
                });
            } else if(Notification) {
                Notification.requestPermission().then(function (result) {
                    if(result === "granted") {
                        deferred.resolve(true);
                    } else {
                        deferred.reject(false);
                    }
                });
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function registerPermission () {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.registerPermission(function (result) {
                    if(result) {
                        deferred.resolve(result);
                    } else {
                        deferred.reject(result);
                    }
                });
            } else {
                deferred.reject();
            }
            return deferred.promise;
        }

        function getScheduledIds (scope) {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.getScheduledIds(function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function isTriggered (id, scope) {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.isTriggered(id, function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function getTriggeredIds (scope) {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.getTriggeredIds(function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function getAllIds (scope) {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.getAllIds(function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function getAll (scope) {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.getAll(function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function get (scope) {
            var deferred = $q.defer();
            if(localNotify) {
                localNotify.get(function (result) {
                    deferred.resolve(result);
                }, scope);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function getDefaults () {
            if(localNotify) {
                return localNotify.getDefaults();
            }
        }

        function setDefaults (Object) {
            if(localNotify) {
                localNotify.setDefaults(Object);
            }
        }

        /**
         * Push notification
         */
        function initPush () {
            var deferred = $q.defer();

            if(!pushNotify) {
                logger.warn("notify", "need PushNotification plugin");
                push = {
                    on: ld.noop,
                    off: ld.noop,
                    init: ld.noop,
                    unregister: ld.noop
                };
                deferred.resolve();
                return deferred.promise;
            }

            push = pushNotify.init({
                android: {
                    senderID: notifyOptions.gcm.senderId,
                    icon: "icon",
                    sound: true,
                    vibrate: true,
                    forceShow: true
                },
                ios: {
                    alert: true,
                    badge: true,
                    sound: true
                }
            });
            logger.debug("notify", "Init push notify plugin");

            deferred.resolve();
            return deferred.promise;
        }

        function registerPush () {
            var deferred = $q.defer(),
                timerPromise;

            asyncLib.series([function (next) {
                if(!push) {
                    initPush().then(function () {
                        next();
                    }, next);
                } else {
                    next();
                }
            }], function (err, results) {
                if(err) {
                    deferred.reject(err);
                } else if(registrationId) {
                    deferred.resolve({data: {registrationId: registrationId}, push: push});
                } else {
                    $timeout(function () {
                        push.on("notification", onPushNotify);
                        push.on("error", onPushError);
                    });
                    push.on("registration", function (data) {
                        if(timerPromise) {
                            $timeout.cancel(timerPromise);
                        }

                        onRegistration(data).then(function (res) {
                            res.push = push;
                            deferred.resolve(res);
                        }).catch(function (err) {
                            deferred.reject(err);
                        });
                    });

                    deferred.promise.push = push;
                    timerPromise = $timeout(function () {
                        logger.warn("notify", "Registration push not fire after 10s");
                        deferred.reject({push: push});
                    }, 10000);
                }
            });

            return deferred.promise;
        }

        function unregisterPush () {
            var deferred = $q.defer(),
                pushToken = $window.localStorage.nt;

            if(push) {
                push.off("registration", onRegistration);
                push.off("notification", onPushNotify);
                push.off("error", onPushError);

                registrationId = null;
                $Storage.removeItem("nt");

                push.unregister(function (success) {
                    if(pushToken) {
                        $Socket.emit("$cps:notify:unregister", {
                            token: pushToken
                        }, function (res) {
                            deferred.resolve(res);
                            logger.debug("notify", "Sucess unregister push: {0}", JSON.stringify(res));
                        });
                    } else {
                        deferred.resolve(success);
                        logger.debug("notify", "Sucess unregister push");
                    }

                }, function (err) {
                    deferred.reject(err);
                    logger.debug("notify", "Error unregister push");
                });
            } else {
                deferred.reject();
                logger.debug("notify", "push.init must be called before any other operation");
            }

            return deferred.promise;
        }

        function resetPush () {
            return unregisterPush().then(function () {
                return registerPush();
            });
        }
        /**
         * notification.badge
         */

        function hasPermissionBadge () {
            var deferred = $q.defer();

            if(!badgeNotify) {
                deferred.resolve();
                return deferred.promise;
            }

            badgeNotify.hasPermission(function (permission) {
                if (permission) {
                    deferred.resolve(true);
                } else {
                    if(promptPermissionBadge) {
                        promptForPermissionBadge();
                    }
                    logger.warn("notify", "Badge you do not have permission");
                    deferred.reject("You do not have permission");
                }
            });

            return deferred.promise;
        }

        function promptForPermissionBadge () {
            if(!badgeNotify) {
                return false;
            }
            promptPermissionBadge = false;
            return badgeNotify.promptForPermission();
        }

        function setBadge (badge, callback, scope) {
            var deferred = $q.defer();

            if(!badgeNotify) {
                deferred.resolve();
                return deferred.promise;
            }
            hasPermissionBadge().then(function () {
                deferred.resolve(
                    badgeNotify.set(badge, callback, scope)
                );
            }, function () {
                deferred.reject("You do not have permission to set Badge");
            });
            return deferred.promise;
        }

        function getBadge () {
            var deferred = $q.defer();

            if(!badgeNotify) {
                deferred.resolve();
                return deferred.promise;
            }
            hasPermissionBadge().then(function () {
                badgeNotify.get(function (badge) {
                    deferred.resolve(badge);
                });
            }, function () {
                deferred.reject("You do not have permission to get Badge");
            });
            return deferred.promise;
        }

        function clearBadge (callback, scope) {
            var deferred = $q.defer();

            if(!badgeNotify) {
                deferred.resolve();
                return deferred.promise;
            }
            hasPermissionBadge().then(function () {
                deferred.resolve(
                    badgeNotify.clear(callback, scope)
                );
            }, function () {
                deferred.reject("You do not have permission to clear Badge");
            });
            return deferred.promise;
        }

        function decreaseBadge (count, callback, scope) {
            var deferred = $q.defer();

            if(!badgeNotify) {
                deferred.resolve();
                return deferred.promise;
            }
            hasPermissionBadge().then(function () {
                deferred.resolve(
                    badgeNotify.decrease(count, callback, scope)
                );
            }, function () {
                deferred.reject("You do not have permission to decrease Badge");
            }) ;
            return deferred.promise;
        }

        function increaseBadge (count, callback, scope) {
            var deferred = $q.defer();

            if(!badgeNotify) {
                deferred.resolve();
                return deferred.promise;
            }
            hasPermissionBadge().then(function () {
                deferred.resolve(
                    badgeNotify.increase(count, callback, scope)
                );
            }, function () {
                deferred.reject("You do not have permission to increase Badge");
            }) ;
            return deferred.promise;
        }


        function configureBadge (config) {
            if(!badgeNotify) {
                return false;
            }
            return badgeNotify.configure(config);
        }

        /**
         * @private
         */

        function storeNotify (notifySave) {
            var deferred = $q.defer(),
                _userNotify = $window.localStorage.nu,
                _syncData;

            if(notifySave) {
                $window.localStorage.nu = $Encrypt.aes(notifySave);
                deferred.resolve(notifySave);
            } else if(userNotify.fromSave) {
                deferred.resolve(userNotify);
            } else if(_userNotify) {
                _syncData = $Encrypt.decrypt(_userNotify, function (err, notifyData) {
                    if(err) {
                        $window.localStorage.removeItem("nu");
                        deferred.reject(err);
                    } else {
                        userNotify = notifyData;
                        userNotify.fromSave = true; // Prevent watchCollection loop
                        deferred.resolve(userNotify);
                    }
                });
            } else {
                deferred.resolve(userNotify);
            }
            // for sync response
            deferred.promise.userNotify = _syncData || userNotify;
            // Promise
            return deferred.promise;
        }

        function onGetNotify (data, action) {
            var isTrigger = action == "trigger",
                newStatus = isTrigger ? "read" : "received",
                notifyNewArray = [],
                notifyCancelArray = [],
                notifyUpdateArray = [],
                notifyTrayArray = [],
                notifyNewData = {},
                notifySaveData = {},
                i;

            if(data.length) {
                for(i = 0; i < data.length; i++) {
                    notifySaveData = getUserNotify().notify;

                    if(data[i].target && data[i].target.indexOf("operator") <= -1 &&
                       !ld.find(notifySaveData, {id: data[i].id})) {

                        // TODO: Remove target migration
                        data[i].target = ld.isString(data[i].target) ?
                            data[i].target.split("_")[0] : data[i].target;
                        // New values migration
                        data[i].code = data[i].code || data[i].status_code;
                        data[i].payload = data[i].payload || data[i].full_data;

                        // Set data title and event
                        var actionString = ld.isString(data[i].action) ? ld.camelCase(data[i].action) : "",
                            opCode = data[i].code >= 409 ? "Error" : data[i].code,
                            eventName = data[i].event || "cps:" +
                                (ld.camelCase(data[i].op + "_" + data[i].target + "_"  + actionString)),
                            localeId = ld.camelCase(data[i].op + "_" + data[i].target + "_"  + actionString + opCode),
                            dateFormat = $filter("date")(new Date(data[i].datereg), "M/d/yyyy h:mm a");

                        data[i].locale_id = localeId;

                        notifyNewData = {
                            id: data[i].id,
                            title: _(localeId, (ld.isObject(data[i].title) ? data[i].title.text : data[i].title)),
                            icon: ld.get(data[i], "options.image") ||
                                ld.get(data[i], "options.icon") || "res://icon",
                            smallIcon: "res://icon",
                            text: _(localeId + "Text", ld.extend({
                                date: data[i].date_format
                            }, $filter("cpsFlatObject")(angular.copy(data[i].payload), {
                                currency: ["amount"]
                            })), (ld.isObject(data[i].body) ? data[i].body.text : data[i].body || data[i].description)),
                            data: {
                                notifyData: angular.copy(data[i]),
                                event: eventName,
                                dateFormat: dateFormat,
                                localeId: localeId
                            }
                        };

                        // Check for new notifications
                        if(data[i].status === "new") {
                            notifyUpdateArray.push({
                                id: data[i].id,
                                idsys_user: data[i].idsys_user
                            });
                            // Log
                            logger.debug("notify", "new notification: {0}", [JSON.stringify(data[i])]);
                            // Update status for new notifications
                            notifyNewData.data.notifyData.status = newStatus;
                            // Set data notify
                            if(data[i].type == "push" || data[i].type == "static") {
                                notifyCancelArray.push(data[i].id);
                                notifyNewArray.push(notifyNewData);
                            }
                            // Fire event
                            $rootScope.$emit(eventName, data[i]);
                            if(isTrigger) {
                                $rootScope.$emit(eventName + ":click", {
                                    notify: notifyNewData
                                });
                            }
                        }
                        // Data for tray
                        if(data[i].type == "push" || data[i].type == "static") {
                            // Prevent dynamic modify by angular and circular structure
                            notifyTrayArray.push(angular.copy(notifyNewData));
                        }
                    }
                }

                // Update status notifications
                if(notifyUpdateArray.length) {
                    $Socket.emit("$cps:notify:update", {
                        status: newStatus,
                        notify: notifyUpdateArray
                    }, function (res) {
                        // Log
                        logger.debug("notify update", JSON.stringify(res));
                    });
                }

                // schendule
                cancel(notifyCancelArray).then(function () {
                    if(!isTrigger && notifyNewArray.length) {
                        schedule(notifyNewArray);
                    }
                });

                // Set user notifty
                if(notifyTrayArray.length) {
                    setNotify(notifyTrayArray);
                }
            }
        }

        function onRegistration (data) {
            var deferred = $q.defer(),
                today = new Date().setHours(0, 0, 0, 0),
                oldDate,
                diffdays;
            // Register in db if not exist same toeken
            registrationId = data.registrationId;
            logger.info("notify", "Register push registrationId: {0}", [registrationId]);

            $Storage.getItem("nt").then(function (nt) {
                if(!nt) {
                    nt = {};
                }
                if(nt.datereg) {
                    today = new Date().setHours(0, 0, 0, 0);
                    oldDate = new Date(nt.datereg).setHours(0, 0, 0, 0);
                    diffdays = Math.round((today - oldDate) / (864e5)); // 1000*60*60*24, negate dst
                }

                // Update token only if id is diferent or id date is old (2 days)
                if(nt.token != registrationId || diffdays > 2) {
                    $Socket.emit("$cps:notify:register", {
                        token: registrationId,
                        platform_id: $App.getPlatform().id
                    }, function (res) {
                        if(ld.has(res, "data.token")) {
                            return $Storage.setItem("nt", {
                                datereg: new Date().getTime(),
                                token: res.data.token,
                                user_id: res.data.user_id
                            }, false).then(function (saveData) {
                                logger.info("notify", "Success save register push: {0}", [
                                    JSON.stringify(saveData)
                                ]);
                                return res.data;
                            });
                        } else {
                            deferred.reject(res.data || {});
                            logger.warn("notify", "Error save regiter push: {0}", [
                                JSON.stringify(res)
                            ]);
                        }
                    });
                } else {
                    logger.info("notify", "Success get register push: {0}", [
                        JSON.stringify(nt)
                    ]);
                    deferred.resolve(nt);
                }

            }).catch(function (err) {
                logger.warn("notify", "Error get item from storage: {0}", [
                    ld.isObject(err) ? JSON.stringify(err) : err
                ]);
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function onPushNotify (pushData) {
            var res = ld.has(pushData, "additionalData.status_code") ? pushData.additionalData :
                    ld.get(pushData, "additionalData.data") || {}; // In iOS come with aditional data

            logger.debug("notify", "New notification from push services: {0}", [JSON.stringify(res)]);

            if(parseInt(res.status_code) === 200) {
                var resData = res.data || [];

                $timeout(function () {
                    $rootScope.$emit("cps:pushNotify:new", pushData);
                    onGetNotify(resData, "trigger");
                });
            }
        }

        function onPushError (e) {
            $rootScope.$emit("cps:pushNotify:error", e);
            logger.error("notify", "Error push notification: {0}", [JSON.stringify(e)]);
        }

    }
    // Exports
    module.exports = notifyService;

})();
