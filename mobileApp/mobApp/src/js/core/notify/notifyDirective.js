/* global module */

// core/notify/notifyDirective.js
//
// directives function for ui notify module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";
    var angular = require("angular"),
        $$ = angular.element;

    cpsNotify.$inject = [
        "$rootScope",
        "$Dom",
        "$Logger",
        "$Locale",
        "$Popmenu",
        "$Notify",
        "$Scroll"
    ];
    function cpsNotify ($rootScope, $Dom, $Logger, $Locale, $Popmenu, $Notify, $Scroll) {
        var logger = $Logger.getInstance(),
            notifyDirective = {
                restrict: "E",
                // transclude: true,
                // replace: true,
                controller: "NotifyController",
                controllerAs: "notify",
                scope: {
                    templateUrl: "@"
                },
                template: "<span class='badge badge-assertive'>{{unread}}</span>" +
                    "<button class='button button-icon notify-button notify-tray-icon ion-notify' " +
                    "ng-click='$popmenu.show($event)'>" +
                    "<span class='hide'>Notifications</span>" +
                    "</button>",
                compile: compile
            };

        return notifyDirective;

        // Functions
        function compile (tElement, tAttr) {
            tElement.addClass("button button-clear notify-tray");

            return { post: postLink };
        }

        function postLink (scope, element, attrs, ctrl) {
            var templateUrl = scope.templateUrl || "app/views/notifyTray.html",
                elId = attrs.popmenuId || "cpsNotifyTray",
                scrollView;

            // Scopes
            scope.unread = 0;
            scope.userNotify = [];

            // Load template
            if(templateUrl && templateUrl.length) {
                $Locale.ready(function () {
                    // Get template tray
                    $Popmenu.fromTemplateUrl(templateUrl, {
                        id: elId,
                        scope: scope,
                        class: "popover-notify-tray"
                    }).then(function (popmenu) {
                        scope.$popmenu = popmenu;
                        scope.scrollDelegate = $Scroll.$getByHandle("notifyTray");
                        scrollView = scope.scrollDelegate.getScrollView();

                        // Scope size watch
                        if(scrollView) {
                            scope.$watch(function () {
                                return scrollView.__contentHeight;
                            }, function (newVal, oldVal) {
                                if(newVal && newVal !== oldVal && popmenu.modalEl) {
                                    popmenu.modalEl.style.height = newVal + "px";
                                }
                            });
                            scope.$on("cps:popmenu:shown", function () {
                                if(!popmenu.modalEl) {
                                    return;
                                }
                                popmenu.scrollEl = popmenu.modalEl.getElementsByClassName("scroll");

                                if(scrollView.__contentHeight > 0) {
                                    popmenu.modalEl.style.height = scrollView.__contentHeight + "px";
                                } else if(popmenu.scrollEl) {
                                    scope.scrollDelegate.resize();
                                    popmenu.modalEl.style.height = $$(popmenu.scrollEl[0]).outerHeight() + "px";
                                }
                            });
                        }

                        // Close when change page
                        scope.$on("$routeChangeStart", function (event, next) {
                            if(scope.$popmenu._isShown) {
                                scope.$popmenu.hide();
                            }
                        });
                        // DEPRECATED
                        scope.$watch(function () {
                            return scope.$popmenu.isShown();
                        }, function (newVal, oldVal) {
                            if (newVal && !oldVal) {
                                $rootScope.onTopOpen = true;
                            } else {
                                $rootScope.onTopOpen = false;
                            }
                        });
                    });
                });
            } else {
                logger.warn("notify", "Notify tray need valid url template");
            }

            // Watch new unread notifications
            scope.$watchCollection(function () {
                return $Notify.userNotify();
            }, function (newData, oldData) {
                scope.unread = newData.unread;
                scope.userNotify = newData.notify;

                if(parseInt(scope.unread) <= 0) {
                    element.removeClass("notify-unread notify-has").addClass("notify-none");
                } else {
                    element.removeClass("notify-none").addClass("notify-unread notify-has");
                }
            });

            // Watch tray status
            scope.$watch(function () {
                return $Notify.isTrayEnabled();
            }, function (newVal, oldVal) {
                if(newVal) {
                    element.removeClass("hide");
                } else {
                    element.addClass("hide");
                }
            });

            // Clean prevent memory leaks
            element.on("$destroy", function () {
                if(scope.$popmenu) {
                    scope.$popmenu.remove();
                }
                scope.$destroy();
            });
            scope.$on("$destroy", function () {
                scrollView = null;
            });

        }
    }

    // Exports
    module.exports = {
        notify: cpsNotify
    };

})();
