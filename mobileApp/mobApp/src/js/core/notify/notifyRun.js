/* global require */

// module/notify/notifyRun.js
//
// run function for notify module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    notifyRun.$inject = [
        "$window",
        "$rootScope",
        "$timeout",
        "$App",
        "$Socket",
        "$Mobile",
        "$Notify",
        "AUTH_EVENTS"
    ];
    function notifyRun ($window, $rootScope, $timeout, $App, $Socket, $Mobile, $Notify, AUTH_EVENTS) {
        var localNotify = ld.get($window.cordova,  "plugins.notification.local"),
            timmerGet = 3000;

        // Events
        $rootScope.$on(AUTH_EVENTS.socketAuth, function (event, data) {
            // Enabled tray
            $Notify.trayEnabled(true);
            // Register push
            $Notify.push.register().finally(function (res) {
                // Get from Socket but wait pushRegister
                $timeout(function () {
                    $Notify.get();
                }, timmerGet);
            });
        });
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function (event, data) {
            // Destroy session
            $Notify.clearAll();
            $Notify.trayEnabled(false);
            $Notify.push.unregister();
            $Notify.local.clearAll();
        });
        $rootScope.$on(AUTH_EVENTS.loginSuccess, function (event, data) {
            // Enabled tray
            $Notify.trayEnabled(true);
        });

        $rootScope.$on("cps:pageChange:click", function (event, res) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            // ensure that the object does not come as string
            if(angular.isString(res.notify.data)) {
                res.notify.data = JSON.parse(res.notify.data);
            }
            var notify = res.notify.data,
                notifyData = notify.notifyData || {},
                payload = notifyData.payload || {};

            if(payload.page && notifyData.status !== "read") {
                // Change page
                $Mobile.pageChange(payload.page);
            }
        });

        $rootScope.$on("cps:openLink:click", function (event, notifyData) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            var payload = ld.get(notifyData, "payload") || {};
            // Open link
            if(payload.url) {
                $Mobile.open(payload.url);
            }
        });

        $Socket.on("$cps:notify:new", function (data, callback) {
            $Notify.new(data);
        });

        $Socket.on("$cps:notify:reset", function (data, callback) {
            $Notify.push.reset(data);
        });

        $App.ready(function () {
            // Local notification events
            if (localNotify) {
                localNotify.on("cancel", function (notify, state) {
                    var notification = {
                        notify: notify,
                        state: state
                    };
                    $timeout(function () {
                        $rootScope.$emit("cps:localNotify:cancel", notification);
                    });
                });

                localNotify.on("clear", function (notify, state) {
                    var notification = {
                        notify: notify,
                        state: state
                    };
                    $timeout(function () {
                        $rootScope.$emit("cps:localNotify:clear", notification);
                    });
                });

                localNotify.on("click", function (notify, state) {
                    if(!ld.isFinite(notify.id)) {
                        notify.id = parseInt(notify.id);
                    }
                    // Fix object as string
                    if(angular.isString(notify.data)) {
                        notify.data = JSON.parse(notify.data);
                    }
                    var notification = {
                        notify: notify,
                        state: state
                    };
                    $timeout(function () {
                        $Notify.userNotify(notify.id).then(function (currrent) {
                            if(ld.get(currrent, "data.notifyData.status") !== "read") {
                                // Send event
                                $rootScope.$emit("cps:localNotify:click", notification);
                                $rootScope.$emit(notify.data.event + ":click", notification);
                                // Update in server
                                $Notify.update(notify, "read").then(function (res) {
                                    notify.data.notifyData.status = "read";
                                    $Notify.set(notify);
                                });
                            }
                        });
                    });
                });

                localNotify.on("trigger", function (notify, state) {
                    var notification = {
                        notify: notify,
                        state: state
                    };
                    $timeout(function () {
                        $rootScope.$emit("cps:localNotify:trigger", notification);
                    });
                });

                localNotify.on("schedule", function (notify, state) {
                    var notification = {
                        notify: notify,
                        state: state
                    };
                    $timeout(function () {
                        $rootScope.$emit("cps:localNotify:schedule", notification);
                    });
                });
            }

        });
    }

    // Export
    module.exports = notifyRun;

})();
