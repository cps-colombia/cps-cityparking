/* global module */

// app/core/login/loginService.js
//
// Login factory for cps login module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    loginService.$inject = [
        "$q",
        "$rootScope",
        "$location",
        "$window",
        "$Locale",
        "$Mobile",
        "$Logger",
        "$App",
        "$Request",
        "$Dialog",
        "$Encrypt",
        "$Storage",
        "USER_ROLES",
        "AUTH_EVENTS"
    ];
    function loginService ($q, $rootScope, $location, $window, $Locale, $Mobile,
                          $Logger, $App, $Request, $Dialog, $Encrypt, $Storage, USER_ROLES, AUTH_EVENTS) {
        var _ = $Mobile.locale,
            op = $App.ops,
            logger = $Logger.getInstance(),
            config = $App.config,
            currentSession = null,
            loginOptions = ld.extend({}, config.options.login),
            minRole = USER_ROLES[config.minRole] || 0,
            defaultUser = {
                country: "",
                country_name: "",
                country_lat: "",
                country_lng: "",
                city: "",
                city_name: "",
                city_lat: "",
                city_lng: "",
                phone: "",
                msisdn: "",
                idu: "",
                address: "",
                cu: "",
                email: "",
                userType: 0,
                name: "",
                lastName: "",
                displayName: "",
                hasSecondPassword: false,
                login: "",
                bd: "",
                bdyear: "",
                bdmonth: "",
                bdday: ""
            },
            userRole = null;

        // Members
        var Login = {
            options: loginOptions,
            initLogin: initLogin,
            logOut: logOut,
            passRecovery: passRecovery,
            passChange: passChange,
            passSecond: passSecond,
            passSecondDialog: passSecondDialog,
            hasSecondPass: hasSecondPass,
            checkAuthenticated: checkAuthenticated,
            isAuthenticated: isAuthenticated,
            isAuthorized: isAuthorized,
            isRole: isRole,
            isMinRole: isMinRole,
            parseRole: parseRole,
            minRole: minRole,
            setUserSession: setUserSession,
            getUserSession: getUserSession,
            staticPosition: staticPosition,
            destroyUserSession: destroyUserSession,
            getUserHash: getUserHash,
            USER_ROLES: USER_ROLES
        };

        return Login;

        // Functions
        /**
         * @name initLogin
         *
         * @description Request for login on success go to homePage and keep a session
         * on error destroy session
         *
         * @param user username/NID or phone user
         * @param pass already encrypted password
         *
         * @return {promise} object with error or succes
         * Succes return statusCode, user_token, user_secret an basic info, line contry etc
         * Error return statusCode and desciption
         */
        function initLogin (user, pass) {
            $Mobile.loading("show");
            // Request login params
            var deferred = $q.defer(),
                req = {
                    params: {
                        op: op.loginSend,
                        user: user,
                        pass: pass,
                        app_token: config.apptoken,
                        app_secret: config.appsecret,
                        device: $App.getPlatform().platform,
                        device_uuid: $App.getPlatform().deviceId
                    }
                };

            $Request.http(req).then(function (res) {
                var success = false,
                    resData = res.data || {},
                    statusCode = resData.statusCode || null;

                logger.debug("login", JSON.stringify(resData));
                $Mobile.loading("hide");

                if(statusCode === 200) {
                    success = true;
                    logger.info("login", "Success, Hi: " + resData.data && resData.data.login);

                    setUserSession(resData.data).then(function (resSession) {
                        // Send public event
                        $rootScope.$emit(AUTH_EVENTS.loginSuccess, getUserSession());
                        // Send Analytics
                        $Mobile.ga("set", "uid", resSession.login);
                        deferred.resolve(res);
                    }).catch(function (err) {
                        deferred.resolve(err);
                    });

                } else if(statusCode === 102) {
                    success = false;
                    $Mobile.alert(_("alertUsername"));
                } else if(statusCode === 103) {
                    success = false;
                    $Mobile.alert(_("alertUserBlock"));
                } else if(statusCode === 112) {
                    success = false;
                    $Mobile.alert(_("alertPassBlock", resData.description));
                } else {
                    success = false;
                    $Mobile.alert(_("alertInternalError", {
                        code: statusCode || 0
                    }));
                }
                // else if(statusCode === 106)
                // duplicated uuid, necesary?

                if(!success) {
                    $Mobile.pageChange("/login");
                    destroyUserSession(res);
                    deferred.reject(res);
                    // Debug error
                    $Mobile.ga("send", "event", "error", "login: " + statusCode);
                    logger.warn("login", "Error Login with status  {0} and statusCode {1}", [ res.status, statusCode ]);
                    // Send Event
                    $rootScope.$emit(AUTH_EVENTS.loginError, res);
                }
            }).catch(function (err) {
                $Mobile.loading("hide");
                $Mobile.ga("send", "event", "error", "login: " + err.status);
                logger.warn("login", "Error Login with status {0} and statusText {1}", [ err.status, err.statusText ]);
                deferred.reject(err);

            });
            return deferred.promise;
        }

        /**
         * @name logOut
         *
         * @description Close session after user confirm
         * when user confirm if realy wants close session,
         * app destroy session and redirect to login
         *
         * @return {promise} with success or error
         */
        function logOut () {
            var deferred = $q.defer(),
                userHash = getUserHash();

            // Send logout request

            $Mobile.confirm(_("alertLogout"), function (buttonIndex) {
                if (buttonIndex !== 2) {
                    // Send request
                    logOutRequest(userHash.ut, userHash.us).then(function (res) {
                        deferred.resolve(res);
                    }).catch(function (err) {
                        deferred.resolve(err);
                    });
                    // does not expect the request, send it in the background so as not to delay logOut
                    destroyUserSession({statusCode: 200, desciption: "Logout", data: {}});
                }
            });
            return deferred.promise;
        }

        /**
         * @name passRecovery
         *
         * @description Send request password recovery to server
         *
         * @param {string} email email/NID or phone user
         *
         * @return {promise} object with error or succes
         * Succes return statusCode and if send emial recovery or email(NID) not valid
         * Error return statusCode and desciption
         */
        function passRecovery (email) {
            $Mobile.loading("show");
            // Request recovery params
            var deferred = $q.defer(),
                req = {
                    params: {
                        op: op.loginRecovery,
                        mail: email,
                        lang: $Locale.getLanguage(),
                        app_token: config.apptoken,
                        app_secret: config.appsecret
                    }
                };

            $Request.http(req).then(function (res) {
                var statusCode = res.data ? res.data.statusCode : null;
                $Mobile.loading("hide");

                if (statusCode === 200) {
                    $Mobile.alert(_("alertresetsend"));
                    $Mobile.pageChange("/login");
                    deferred.resolve(res);
                } else if (statusCode === 104) {
                    $Mobile.alert(_("alertcheckemail"));
                    deferred.reject(res);
                }

            });
            return deferred.promise;
        }

        /**
         * @name passChange
         *
         * @description Send request to change password
         *
         * @param {object} objectData with pass, newPass, and newRePass
         *
         * @return {promise} object with error or succes
         * Succes return statusCode
         * Error return statusCode and desciption
         */
        function passChange (dataObject) {
            $Mobile.loading("show");
            // Request recovery params
            var deferred = $q.defer(),
                req = {
                    params: {
                        op: op.loginChange,
                        lang: $Locale.getLanguage(),
                        user_token: getUserHash().ut,
                        user_secret: getUserHash().us
                    }
                };

            angular.extend(req.params, dataObject);

            $Request.http(req).then(function (res) {
                var statusCode = res.data ? res.data.statusCode : null;
                $Mobile.loading("hide");

                if (statusCode === 200) {
                    $Mobile.alert(_("alertPassChange"));
                    deferred.resolve(res);
                } else if (statusCode === 106) {
                    $Mobile.alert(_("alertPassInvalid"));
                    deferred.reject(res);
                }

            });
            return deferred.promise;
        }

        /**
         * @name passSecond
         *
         * @description Send create, change or validate second password
         *
         * @param {object} objectData with pass and/or newPass, newRePass
         * and action with create, change or validate string
         *
         * @return {promise} object with error or succes
         * Succes return statusCode
         * Error return statusCode and desciption
         */
        function passSecond (dataObject) {
            $Mobile.loading("show");
            // Request second pass
            var deferred = $q.defer(),
                req = {
                    params: {
                        op: op.loginPass2,
                        lang: $Locale.getLanguage(),
                        user_token: getUserHash().ut,
                        user_secret: getUserHash().us
                    }
                };

            angular.extend(req.params, dataObject);

            $Request.http(req).then(function (res) {
                var statusCode = res.data ? res.data.statusCode : null;
                $Mobile.loading("hide");

                if (statusCode === 200) {
                    if(req.params.action === "create") {
                        setUserSession({hasSecondPassword: true});
                        $Mobile.alert(_("alertPassSecondCreate"));
                    } else if (req.params.action === "change") {
                        $Mobile.alert(_("alertPassSecondChange"));
                    }

                    if($location.path() === "/pass-second") {
                        $Mobile.pageChange("/configuration");
                    }

                    deferred.resolve(res);
                } else if (statusCode === 106 || statusCode === 107) {
                    $Mobile.alert(_("alertPassSecondInvalid"));
                    deferred.reject(res);
                }  else if (statusCode === 110) {
                    $Mobile.alert(_("alertPassSecondAlreadyCreated"));
                    deferred.reject(res);
                } else if(statusCode === 111) {
                    $Mobile.alert(_("alertPassSecondBlock"));
                    deferred.reject(res);
                }

            });
            return deferred.promise;
        }

        function passSecondDialog () {
            var deferred = $q.defer(),
                dialogPass = $Dialog.show({
                    title: _("passSecond"),
                    templateUrl: "app/views/passSecondForm.html",
                    buttons: [{
                        text: _("btnCancel"),
                        type: "button-default",
                        onTap: function (e) {
                            return {authorized: false, response: {}};
                            // $rootScope.$emit('cps:secondPassword', {authorized: false, response: {}});
                        }
                    }]
                }),
                onSecondPassword;

            // Primise callback
            dialogPass.then(function (res) {
                deferred.resolve(res);
            });
            dialogPass.catch(function (err) {
                deferred.reject(err);
            });

            // Listen event
            onSecondPassword = $rootScope.$on("cps:secondPassword", function (event, data) {
                // Unregister event
                onSecondPassword();

                // Close dialog
                if(dialogPass && dialogPass.close) {
                    dialogPass.close();
                }
                // callback
                deferred.resolve(data);
            });

            // Promise
            return deferred.promise;
        }

        function hasSecondPass () {
            return getUserSession().hasSecondPassword;
        }

        // Utils
        /**
         * @name checkAuthenticated
         *
         * @description Check if used already autenticated
         * and emit event
         *
         * @return {event} emit event login or notAuthenticated
         */
        function checkAuthenticated () {
            if(isAuthenticated()) {
                $rootScope.$emit(AUTH_EVENTS.loginSuccess, getUserSession());
            } else {
                $rootScope.$emit(AUTH_EVENTS.notAuthenticated, {statusCode: 200, description: "No login"});
            }
        }
        /**
         * @name isAuthenticated
         *
         * @description Check if used already autenticated
         *
         * @return {boolean} true or false
         */
        function isAuthenticated () {
            var logueado = $window.localStorage.logueado === "true" ? true : false;
            return logueado;
        }

        /**
         * @name isAuthorized
         *
         * @description Check if used is Autorized for view seccion or content
         *
         * @param {array} authorizedRoles roles autorized by seccion
         *
         * @return {boolean} true or false
         */
        function isAuthorized (authorizedRoles) {
            var allowRoles = false;

            authorizedRoles = authorizedRoles || minRole;

            if (!ld.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }

            if((isRole() >= minRole && authorizedRoles.indexOf(isRole()) > -1) || isRole() === USER_ROLES.admin) {
                allowRoles = true;
            } else if(authorizedRoles.indexOf(minRole) > -1 && isRole() > minRole) {
                allowRoles = true;
            } else if(authorizedRoles.indexOf(USER_ROLES.guest) > -1) {
                allowRoles = true;
            }

            return allowRoles;
        }

        /**
         * @name isRole
         *
         * @description Role current user
         *
         * @return {string} role id
         */
        function isRole () {
            var role = 0;

            if(userRole !== null) {
                return (parseInt(userRole) || 0);
            }

            role = $window.localStorage.ur || 0;

            if(role && role.length > 3) {
                userRole = $Encrypt.decrypt(role);
            } else {
                userRole = role;
                $Storage.getItem("ur").then(function (value) {
                    if(!value) {
                        $Storage.setItem("ur", value);
                    } else {
                        userRole = parseInt($Encrypt.decrypt(value));
                    }
                });
            }

            return (parseInt(userRole) || 0);
        }

        function setRole (role) {
            $Storage.setItem("ur", $Encrypt.aes(String((role || "0"))));
            userRole = null;
        }

        function isMinRole (_minRole) {
            return isRole() === 1 ? true : isRole() >= (parseInt(_minRole) || minRole);
        }

        function parseRole (_role) {
            if(!isFinite(_role)) {
                _role = USER_ROLES[_role];
            }
            if(ld.isUndefined(_role)) {
                _role = 0;
            }
            return parseInt(_role);
        }

        /**
         * @name setUserSession
         *
         * @description Set new or update user session,
         * encrypPass is required for new session
         *
         * @param {object} userInfo all user info, like country
         * login, email, name, userType
         *
         * @return {promise} with user Data
         */
        function setUserSession (userInfo) {
            var deferred = $q.defer();

            try {
                if(ld.isObject(userInfo)) {
                    var sessionUser = angular.copy(defaultUser),
                        oldData = angular.copy(currentSession || {}),
                        bdDate,
                        cityInfo,
                        cityCord,
                        countryCord;
                    // Clean var currentData
                    currentSession = null;

                    // Set values
                    bdDate = userInfo.birthDate || userInfo.birthdate || oldData.bd;
                    cityInfo = angular.isObject(userInfo.city_idcity) ? userInfo.city_idcity : (userInfo.city || {});
                    cityCord = cityInfo.position ? cityInfo.position.coordinates : null;
                    countryCord = angular.isObject(cityInfo.country) && cityInfo.country.position ?
                        cityInfo.country.position.coordinates : null;

                    // set session
                    sessionUser.idu = userInfo.idsysUser || userInfo.id || oldData.idu;
                    sessionUser.login = userInfo.login || oldData.login;
                    sessionUser.name = userInfo.name || oldData.name;
                    sessionUser.lastName = userInfo.lastName || userInfo.last_name || oldData.lastName;
                    sessionUser.displayName = userInfo.display_name ||
                        getDisplayName(sessionUser.name, sessionUser.lastName);
                    sessionUser.address = userInfo.address || oldData.address;
                    sessionUser.email = userInfo.email || oldData.email;
                    sessionUser.bd = bdDate;
                    sessionUser.userType = parseInt((userInfo.idsysUserType ||
                                                     userInfo.user_type ||
                                                     oldData.userType || 0));
                    sessionUser.hasSecondPassword = typeof userInfo.hasSecondPassword !== "undefined" ?
                        userInfo.hasSecondPassword : typeof userInfo.second_password !== "undefined" ?
                        userInfo.second_password : oldData.hasSecondPassword || false;

                    sessionUser.cu = userInfo.cu || oldData.cu;
                    sessionUser.phone = userInfo.favoriteMsisdn || userInfo.phone || oldData.phone;
                    sessionUser.msisdn = sessionUser.phone; // TODO: Deprecated

                    // TODO: Organized in object
                    sessionUser._city = cityInfo;

                    sessionUser.country = angular.isObject(cityInfo.country) ?
                        (cityInfo.country.idcountry || cityInfo.country.id) : oldData.country;
                    sessionUser.country = parseInt(sessionUser.country);
                    sessionUser.country_name = angular.isObject(cityInfo.country) ?
                        cityInfo.country.name : oldData.country_name;
                    sessionUser.country_lat = countryCord ? countryCord.coordinates[0].y : (oldData.country_lat || "");
                    sessionUser.country_lng = countryCord ? countryCord.coordinates[0].x : (oldData.country_lng || "");

                    sessionUser.city = cityInfo.idcity || cityInfo.id || oldData.city;
                    sessionUser.city = parseInt(sessionUser.city);
                    sessionUser.city_name = cityInfo.name || oldData.city_name;
                    sessionUser.city_lat = cityCord ? cityCord.coordinates[0].y : (oldData.city_lat || "");
                    sessionUser.city_lng = cityCord ? cityCord.coordinates[0].x : (oldData.city_lng || "");

                    // Date
                    if(bdDate) {
                        bdDate = new Date((bdDate.length <= 10 ? bdDate.replace(/-/g, "/") : bdDate));
                        sessionUser.bdyear = bdDate.getFullYear().toString();
                        sessionUser.bdmonth = (bdDate.getMonth() + 1).toString();
                        sessionUser.bdmonth = (sessionUser.bdmonth > 9 ?
                                               sessionUser.bdmonth : "0" + sessionUser.bdmonth);
                        sessionUser.bdday = (bdDate.getDate()).toString();
                        sessionUser.bdday = (sessionUser.bdday > 9 ? sessionUser.bdday : "0" + sessionUser.bdday);
                    }

                    // Save login in localStorage
                    setRole(sessionUser.userType);
                    $Storage.setItem("ub", $Encrypt.aes(userInfo.balance));

                    if ($window.localStorage.logueado !== "true" ||
                        !$window.localStorage.ut || !$window.localStorage.us) {
                        $window.localStorage.logueado = "true";
                        $window.localStorage.ut = userInfo.userToken;
                        $window.localStorage.us = userInfo.secretToken;
                    }
                    // Try to make async
                    $Storage.getItem("logueado", function (err, value) {
                        if(err || !value || value !== "true") {
                            $Storage.setItem("logueado", true);
                            userInfo.userToken = userInfo.userToken || $window.localStorage.ut;
                            userInfo.secretToken = userInfo.secretToken || $window.localStorage.us;
                            if(userInfo.secretToken && userInfo.userToken) {
                                $Storage.setItem("us", {
                                    st: userInfo.secretToken,
                                    ut: userInfo.userToken
                                });
                            }
                        }
                    });

                    $Storage.setItem("ud", $Encrypt.aes(sessionUser)).then(function (res) {
                        logger.debug("login", "Set session user {0}", [JSON.stringify(sessionUser)]);
                        $rootScope.$apply(function () {
                            deferred.resolve(sessionUser);
                        });
                    }).catch(function (err) {
                        $rootScope.$apply(function () {
                            deferred.reject(err);
                        });
                        logger.warn("login", "Error storage setUserSession: {0}", [err]);
                    });
                } else {
                    $rootScope.$apply(function () {
                        deferred.reject({error: "need object data"});
                    });
                    logger.warn("login", "User info need object data");
                }

            } catch(err) {
                $rootScope.$apply(function () {
                    deferred.reject(err);
                });
                logger.warn("login", err);
            }
            // Promise
            return deferred.promise;
        }

        /**
         * @name getUserSession
         *
         * @description Get user session
         *
         * @return {object} all info current user
         */
        function getUserSession () {
            if(!currentSession) {
                if($window.localStorage.ud) {
                    currentSession = $Encrypt.decrypt($window.localStorage.ud, function (err, data) {
                        if(err) {
                            destroyUserSession(err);
                        }
                    });
                } else {
                    currentSession = null;
                }
            }
            return currentSession || defaultUser;
        }

        /**
         * @name staticPosition
         *
         * @description return city or contry position by user
         *
         * @param {object} geo coords data
         *
         */
        function staticPosition () {
            var ud = getUserSession(),
                latUser = ud.city_lat.length > 0 ? ud.city_lat : ud.country_lat,
                lngUser = ud.city_lat.length > 0 ? ud.city_lng : ud.country_lng;

            return {
                lat: latUser,
                lng: lngUser,
                coords: {
                    latitude: latUser,
                    longitude: lngUser
                }};
        }
        /**
         * @name destroyUserSession
         *
         * @description Destroy session current user
         *
         * @param {object} data reason logout
         *
         */
        function destroyUserSession (data) {
            var deferred = $q.defer();

            //  Destroy data
            currentSession = null;

            ld.forEach([
                "hd", "nu", "ub",
                "ud", "up", "us", "ut"
            ], function (key, index) {
                $window.localStorage.removeItem(key);
                $Storage.removeItem(key);
            });

            setRole(0);
            $Storage.setItem("logueado", false);
            $rootScope.$emit(AUTH_EVENTS.logoutSuccess, data);
            $rootScope.$emit(AUTH_EVENTS.notAuthenticated, data);

            // Redirect
            $Mobile.pageChange("/login");

            // Promise
            deferred.resolve("destroy");
            return deferred.promise;
        }

        function getUserHash () {
            var userData = {},
                ut = $window.localStorage.getItem("ut"),
                us = $window.localStorage.getItem("us");

            if(ut && us) {
                userData.ut = ut;
                userData.us = us;
            }
            return userData;
        }

        // Private
        function logOutRequest (ut, us) {
            var deferred = $q.defer(),
                req = { // request logout
                    background: true,
                    params: {
                        op: op.logoutSend,
                        user_token: ut,
                        user_secret: us
                    }
                };

            $Request.http(req).then(function (res) {
                var statusCode = res.data ? res.data.statusCode : null;
                if(statusCode === 200) {
                    deferred.resolve(res);
                } else if(statusCode === 105) {
                    // Try Again
                    deferred.reject(res);
                    $Request.http(req);
                }
            }).catch(function (err) {
                // Try Again
                $Request.http(req);
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function getDisplayName (name, lastName) {
            var arrName = name && name.split(/\s*[\s,]\s*/) || [] ,
                arrLastName = lastName && lastName.split(/\s*[\s,]\s*/) || [];

            if(arrLastName.length > 0) {
                return arrName[0] + " " + arrLastName[0];
            } else {
                return name;
            }
        }

    }

    // Exports
    module.exports = loginService;

})();
