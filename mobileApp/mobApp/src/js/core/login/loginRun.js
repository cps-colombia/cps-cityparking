/* global module */

// app/core/login/loginRun.js
//
// Run config for cps login module
//
// 2015, CPS - Cellular Parking Systems
(function () {
    "use strict" ;


    // Restricting route access
    loginRun.$inject = [
        "$rootScope",
        "$window",
        "$Mobile",
        "$App",
        "$Logger",
        "$Login",
        "AUTH_EVENTS",
        "USER_ROLES"
    ];
    function loginRun ($rootScope, $window, $Mobile, $App, $Logger, $Login, AUTH_EVENTS, USER_ROLES) {
        var config = $App.config;

        // Listen Events
        $rootScope.$on(AUTH_EVENTS.registerSuccess, function (event, data) {
            // Listen to register module and send login
            $Login.initLogin(data.user, data.pass).then(function (res) {
                // Redirect
                $Mobile.pageChange(config.homePage);
            });
        });
        $rootScope.$on(AUTH_EVENTS.userBlocked, function (event, data) {
            // Destroy Session
            $Login.destroyUserSession(data);
        });
        $rootScope.$on(AUTH_EVENTS.invalidTokens, function (event, data) {
            // Destroy Session
            $Login.destroyUserSession(data);
        });

        $rootScope.$on("$routeChangeStart", function (event, next) {
            var authorizedRoles = next.data && next.data.roles || $Login.minRole,
                inLogin = next.data && next.data.inLogin;

            // Chech user permissions
            if(inLogin) {
                if($Login.isAuthenticated() && $Login.isMinRole()) {
                    event.preventDefault();
                    $Mobile.pageChange(config.homePage);
                }
            } else {
                if (!$Login.isAuthorized(authorizedRoles)) {
                    event.preventDefault();
                    if ($Login.isAuthenticated() && $Login.isMinRole()) {
                        // user is not allowed
                        $Mobile.pageChange(config.homePage);
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                    } else {
                        // user is not logged in
                        $Mobile.pageChange("/login");
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                    }
                }
            }
        });
    }

    // Exports
    module.exports = loginRun;
})();
