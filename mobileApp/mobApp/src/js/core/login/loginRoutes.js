/* global module */

// app/core/login/loginRoutes.js
//
// Routes for cps login module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    loginRoutes.$inject = ["$routeProvider", "USER_ROLES"];
    function loginRoutes ($routeProvider, USER_ROLES) {
        $routeProvider
            .when("/login", {
                templateUrl: "template/login.html",
                controller: "LoginController",
                controllerAs: "login",
                data: {
                    title: "login",
                    roles: [USER_ROLES.guest],
                    inLogin: true
                }
            })
            .when("/pass-recovery",{
                templateUrl: "template/pass-recovery.html",
                controller: "LoginController",
                controllerAs: "login",
                data: {
                    title: "passRecovery",
                    roles: [USER_ROLES.guest],
                    inLogin: true
                }
            })
            .when("/pass-change",{
                templateUrl: "template/security.html",
                controller: "LoginController",
                controllerAs: "login",
                data: {
                    title: "passChange",
                    roles: [USER_ROLES.user],
                    inLogin: false
                }
            })
            .when("/pass-second",{
                templateUrl: "template/security.html",
                controller: "LoginController",
                controllerAs: "login",
                data: {
                    title: "passSecond",
                    roles: [USER_ROLES.user],
                    inLogin: false
                }
            });
    }


    module.exports = loginRoutes;

})();
