/* global module */

// app/core/login/loginDirective.js
//
// directives function for login module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    loginForm.$inject = ["$Bind"];
    function loginForm ($Bind) {
        var loginDirective =  {
            restrict: "E",
            templateUrl: "app/views/loginForm.html",
            compile: compile
        };
        return loginDirective;

        // Functions
        function compile (tElement, tAttrs) {
            return { pre: prelink };
        }
        function prelink (scope, element, attrs) {
            $Bind(scope, attrs, {
                recovery: "@"
            });
        }
    }

    function passRecoveryForm () {
        var recoveryDirective = {
            restrict: "E",
            templateUrl: "app/views/passRecoveryForm.html"
        };

        return recoveryDirective;
    }

    function passChangeForm () {
        var changeDirective = {
            restrict: "E",
            templateUrl: "app/views/passChangeForm.html"
        };

        return changeDirective;
    }

    function passSecondValidateForm () {
        var secondCreateDirective = {
            restrict: "E",
            templateUrl: "app/views/passSecondValidateForm.html"
        };

        return secondCreateDirective;
    }
    function passSecondCreateForm () {
        var secondCreateDirective = {
            restrict: "E",
            templateUrl: "app/views/passSecondCreateForm.html"
        };

        return secondCreateDirective;
    }

    function passSecondChangeForm () {
        var secondChangeDirective = {
            restrict: "E",
            templateUrl: "app/views/passSecondChangeForm.html"
        };

        return secondChangeDirective;
    }

    userInfo.$inject = ["$Login"];
    function userInfo ($Login) {
        var userInfoDirective = {
            restrict: "E",
            template: "<div class='user'><span class='display-name'>{{displayName}}</span>" +
                "<span class='unique-code'><span locale-id='cuFull'></span>: " +
                "<span>{{uniqueCode}}</span></span></div>",
            link: function postLink (scope, element, attrs) {
                var userData = $Login.getUserSession;
                scope.currentUser = "";

                // Scope watch
                scope.$watch(function () {
                    return userData().displayName;
                }, function (newVal, oldVal) {
                    if(newVal || newVal === "" || newVal === undefined) {
                        scope.displayName = ld.startCase(newVal) || "";
                        scope.uniqueCode = userData().cu || "";
                    }
                });
            }
        };
        return userInfoDirective;
    }

    loginRoles.$inject = ["$animate", "$Dom", "$Login"];
    function loginRoles ($animate, $Dom, $Login) {
        var roleDirective = {
            restrict: "EA",
            require: "cpsLoginRoles",
            // asks for $scope to fool the BC controller module
            controller: ["$scope", function loginRolesController () {
                this.cases = {};
            }],
            link: postLink
        };
        return roleDirective;

        // Functions
        function postLink (scope, element, attr, loginRolesController) {
            var selectedTranscludes = [],
                selectedElements = [],
                previousLeaveAnimations = [],
                selectedScopes = [];

            var spliceFactory = function (array, index) {
                return function () {
                    array.splice(index, 1);
                };
            };

            scope.$watch(function () {
                return $Login.isRole();
            }, function cpsLoginRoleAction (role) {
                var block = {},
                    allowRole = $Login.minRole,
                    selected, promise, anchor, i, ii;

                for (i = 0, ii = previousLeaveAnimations.length; i < ii; ++i) {
                    $animate.cancel(previousLeaveAnimations[i]);
                }
                previousLeaveAnimations.length = 0;

                for (i = 0, ii = selectedScopes.length; i < ii; ++i) {
                    selected = $Dom.getBlockNodes(selectedElements[i].clone);
                    selectedScopes[i].$destroy();

                    promise = previousLeaveAnimations[i] = $animate.leave(selected);
                    promise.then(spliceFactory(previousLeaveAnimations, i));
                }

                selectedElements.length = 0;
                selectedScopes.length = 0;
                // Add some defaults roles
                allowRole = role >= $Login.minRole || role === $Login.USER_ROLES.admin ? $Login.minRole : 0;

                // Join roles allow
                selectedTranscludes = ld.union(loginRolesController.cases[allowRole],
                                               loginRolesController.cases[role]) || [];

                // Enabled content for by role
                if (selectedTranscludes) {
                    ld.forEach(selectedTranscludes, function (selectedTransclude) {
                        selectedTransclude.transclude(function (caseElement, selectedScope) {
                            selectedScopes.push(selectedScope);
                            anchor = selectedTransclude.element;

                            caseElement[caseElement.length++] = document.createComment(" end cpsRole: ");
                            block = { clone: caseElement };

                            selectedElements.push(block);
                            $animate.enter(caseElement, anchor.parent(), anchor);
                        });
                    });
                }
            });
        }

    }

    userRole.$inject = ["$Login"];
    function userRole ($Login) {
        var roleDirective = {
            transclude: "element",
            priority: 1300,
            require: "^cpsLoginRoles",
            multiElement: true,
            link: function (scope, element, attrs, ctrl, $transclude) {
                var userRole = attrs.cpsRole || $Login.minRole;
                userRole = $Login.parseRole(userRole);

                ctrl.cases[userRole] = (ctrl.cases[userRole] || []);
                ctrl.cases[userRole].push({ transclude: $transclude, element: element });
            }
        };
        return roleDirective;
    }

    // Exports
    module.exports = {
        loginForm: loginForm,
        passRecoveryForm: passRecoveryForm,
        passChangeForm: passChangeForm,
        passSecondValidateForm: passSecondValidateForm,
        passSecondCreateForm: passSecondCreateForm,
        passSecondChangeForm: passSecondChangeForm,
        userInfo: userInfo,
        loginRoles: loginRoles,
        userRole: userRole
    };

})();
