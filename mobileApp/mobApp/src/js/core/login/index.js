/* global require */

// core/login/index.js
//
// index function for Login
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        loginController = require("./loginController"),
        loginDirective = require("./loginDirective"),
        loginService = require("./loginService"),
        loginRoutes = require("./loginRoutes"),
        loginRun = require("./loginRun");

    angular.module("cpsLogin", []);
    var cpsLogin = angular.module("cpsLogin");

    // Controller
    cpsLogin.controller("LoginController", loginController);

    // Service
    cpsLogin.factory("$Login", loginService);

    // Directive
    cpsLogin.directive("cpsLoginForm", loginDirective.loginForm);
    cpsLogin.directive("cpsPassrecoveryForm", loginDirective.passRecoveryForm);
    cpsLogin.directive("cpsPassChangeForm", loginDirective.passChangeForm);
    cpsLogin.directive("cpsPassSecondValidateForm", loginDirective.passSecondValidateForm);
    cpsLogin.directive("cpsPassSecondCreateForm", loginDirective.passSecondCreateForm);
    cpsLogin.directive("cpsPassSecondChangeForm", loginDirective.passSecondChangeForm);
    cpsLogin.directive("cpsUserInfo", loginDirective.userInfo);
    cpsLogin.directive("cpsLoginRoles", loginDirective.loginRoles);
    cpsLogin.directive("cpsRole", loginDirective.userRole);

    // Routes
    cpsLogin.config(loginRoutes);
    cpsLogin.run(loginRun);

    // Constant
    cpsLogin.constant("AUTH_EVENTS", {
        loginSuccess: "cps:login:success",
        loginError: "cps:login:error",
        logoutSuccess: "cps:logout:success",
        registerSuccess: "cps:register:success",
        registerError: "cps:register:error",
        sessionTimeout: "cps:sessionTimeout",
        notAuthenticated: "cps:notAuthenticated",
        notAuthorized: "cps:notAuthorized",
        invalidTokens: "cps:invalidTokens",
        userBlocked: "cps:userBlocked",
        socketAuth: "cps:socket:auth",
        socketConnect: "cps:socket:connect",
        socketDisconnect: "cps:socket:disconnect"
    });

    cpsLogin.constant("USER_ROLES", {
        guest: 0,
        admin: 1,
        user: 2,
        editor: 3,
        supervisor: 11
    });

})();
