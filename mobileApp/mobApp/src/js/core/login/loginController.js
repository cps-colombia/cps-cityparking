/* global module */

// app/core/login/loginController.js
//
// Login controller for cps login module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash"),
        angular = require("angular");

    LoginController.$inject = [
        "$q",
        "$scope",
        "$Dialog",
        "$Payment",
        "$rootScope",
        "$window",
        "$Login",
        "$App",
        "$Logger",
        "$Mobile",
        "$Encrypt",
        "AUTH_EVENTS"
    ];
    function LoginController ($q, $scope, $Dialog, $Payment, $rootScope,
                             $window, $Login, $App, $Logger, $Mobile, $Encrypt, AUTH_EVENTS) {

        var _ = $Mobile.locale,
            login = this,
            logger = $Logger.getInstance(),
            config = $App.config;

        // Members
        login.initLogin = initLogin;
        login.sendLogin = sendLogin;
        login.passRecovery = passRecovery;
        login.passRecoveryDialog = passRecoveryDialog;
        login.passChange = passChange;
        login.passSecond = passSecond;
        login.hasSecondPass = $Login.hasSecondPass;

        // Login params
        login.auth = {
            user: "",
            pass: ""
        };
        // Recovery params
        login.recovery = {
            email: ""
        };

        // Login change
        login.changePass = {
            pass: "",
            newPass: "",
            newRePass: ""
        };

        // Second pass
        login.secondPass = {
            pass: ""
        };

        // Control
        login.formSuccess = false;


        // Functions
        /**
         * @name initLogin
         *
         * @description Send request login to $Login service
         *
         * @param user username/NID or phone user
         * @param pass already encrypted password
         *
         * @return {object} promise with error or succes
         * Succes return statusCode, user_token, user_secret an basic info, line contry etc
         * Error return statusCode and desciption
         */
        function initLogin (user, pass) {
            if (user.length <= 0 || pass.length === 14) {
                $Mobile.loading("hide");
                $Mobile.alert(_("alertEmptyUsername"));
            } else {
                $Login.initLogin(user, pass).then(function (res) {
                    // Redirect
                    login.formSuccess = true;
                    $Mobile.pageChange(config.homePage);
                });
            }
        }

        /**
         * @name sendLogin
         *
         * @description Send login from Form, encrypt the password and senr to request
         *
         */
        function sendLogin () {
            // when send by form
            initLogin(login.auth.user, $Encrypt.genEncrypt(ld.clone(login.auth.pass)));
        }


        /**
         * @name passRecovery
         *
         * @description Send request password recovery to $Login service
         *
         */
        function passRecovery (dialogRecovery) {
            var email = login.recovery.email;

            if(!$Mobile.emailValidator(email)) {
                $Mobile.alert(_("alertTypeEmail"));
                $Mobile.loading("hide");
                return false;
            }

            $Login.passRecovery(email).then(function () {
                login.formSuccess = true;
                if(dialogRecovery) {
                    dialogRecovery.close();
                }
            });
        }

        function passRecoveryDialog (scope) {
            var deferred = $q.defer(),
                attrScope = scope,
                dialog;

            scope = attrScope || $scope;

            dialog = $Dialog.show({
                title: _("passRecovery"),
                templateUrl: "app/views/passRecoveryForm.html",
                scope: scope,
                buttons: [{
                    text: _("cancel"),
                    isClose: true,
                    onTap: function (e) {
                        return {data: {close: true}, dialog: dialog};
                    }
                }]
            });

            scope.passRecoveryDialog = dialog;

            // // When $Dialog close
            dialog.then(function (close) {
                scope.passRecoveryDialog = null;
                if(close) {
                    // Clean
                    if(attrScope) {
                        scope.$destroy();
                    }
                    deferred.reject(close);
                } else {
                    deferred.resolve(close);
                }
            });

            // Promise
            return deferred.promise;
        }

        /**
         * @name passChange
         *
         * @description Change password to $Login service
         *
         */
        function passChange () {
            var rawChange = angular.copy(login.changePass),
                changePass = {
                    npass: $Encrypt.genEncrypt(rawChange.newPass),
                    oldpass: $Encrypt.genEncrypt(rawChange.pass)
                };

            if(rawChange.pass === rawChange.newPass) {
                $Mobile.alert(_("alertPassNeedDiferent"));
                $Mobile.loading("hide");
            } else {
                $Mobile.confirm(_("alertPassChangeConfirm"), function (buttonIndex) {
                    if (buttonIndex == 1) {
                        // Send request pass change
                        $Login.passChange(changePass).then(function () {
                            login.formSuccess = true;
                            $Mobile.pageChange("/configuration");
                        });
                    } else {
                        login.formSuccess = true;
                    }
                });
            }
        }

        /**
         * @name passSecond
         *
         * @description Change password to $Login service
         *
         * @return {object} promise with error or succes
         */
        function passSecond (action) {
            var rawData = angular.copy(login.secondPass),
                secondData = {
                    action: action,
                    pass: $Encrypt.genEncrypt(rawData.pass)
                };

            if(secondData.action === "change") {
                secondData.npass =  $Encrypt.genEncrypt(rawData.newPass);
                secondData.oldpass =  secondData.pass;

                passSecondChange(rawData, secondData);

            } else if(secondData.action === "validate") {

                $Login.passSecond(secondData).then(function (res) {
                    login.formSuccess = true;
                    $rootScope.$emit("cps:secondPassword", {authorized: true, response: res});
                });

            } else if(secondData.action === "create") {

                passSecondCreate(rawData, secondData);

            } else {
                $Mobile.alert(_("alertInternalError"));
                logger.warn("passSecond", "Invalid Action");
            }

        }

        // Private
        function passSecondCreate (rawData, secondData) {
            if(rawData.pass !== rawData.rePass) {
                $Mobile.alert(_("alertPassSecondNeedEqual"));
                $Mobile.loading("hide");
            } else {
                $Login.passSecond(secondData).then(function () {
                    login.formSuccess = true;
                });
            }
        }

        function passSecondChange (rawData, secondData) {
            if(rawData.pass === rawData.newPass) {
                $Mobile.alert(_("alertPassSecondNeedDiferent"));
                $Mobile.loading("hide");
            } else {
                $Mobile.confirm(_("alertPassSecondChangeConfirm"), function (buttonIndex) {
                    if (buttonIndex !== 2) {
                        // Send request pass change
                        $Login.passSecond(secondData).then(function () {
                            login.formSuccess = true;
                        });
                    } else {
                        login.formSuccess = true;
                    }
                });
            }
        }
    }

    // Exports
    module.exports = LoginController;


})();
