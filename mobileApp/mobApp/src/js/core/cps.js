/* global require */

// app/cps.js
//
// main function for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

/**
 * Run app and load modules, config
 * @constructor
 */
(function () {
    "use strict";

    // Dependencies
    const $ = require("jquery");
    const ld = require("lodash");
    const l10n = require("l10n");
    const angular = require("angular");
    const ngRoute = require("angular-route");
    const ngTouch = require("angular-touch");
    const ngAnimate = require("angular-animate");
    const ngMessages = require("angular-messages");
    const ngSanitize = require("angular-sanitize");
    const oclazyload = require("oclazyload");
    const utils = require("../utils");
    const cpsUi = require("./ui");
    const cpsMain = require("./main");
    const cpsLocale = require("./locale");
    const cpsSocial = require("./social");
    const cpsNotify = require("./notify");
    const cpsSocket = require("./socket");
    const cpsLogin = require("./login");
    const cpsRegister = require("./register");
    const modules = require("./modules");
    const confFile = require("../../../templates/cpsConfig_example.json");
    const $$ = angular.element;

    // Load default modules
    var cpsApp = {},
        appModules = [],
        coreModules = [
            "ngRoute", "ngIOS9UIWebViewPatch", "ngMessages", "ngAnimate", "ngTouch",
            "ngLocale", "ngSanitize", "oc.lazyLoad", "cpsUi", "cpsUtils", "cpsLocale",
            "cpsSocial", "cpsNotify", "cpsSocket", "cpsLogin", "cpsRegister"
        ];

    // Set global cps Object
    window.cps = window.cps || {};
    window.cps.isCordova = isCordova();

    // Load extra modules
    appModules = modules.loadModules(coreModules, confFile);

    // Init cpsApp
    cpsApp = cpsMain.initApp({
        modules: appModules,
        config: confFile
    });

    // Bootstrap App!!
    document.webL10n.ready(function () {
        if(document.webL10n.getReadyState() === "interactive") {
            bootstrap();
        }
    });

    // Functions
    //
    // - bootstrap
    // - onBootReady
    // - isCordova

    // run app if cordova or not with callback
    function bootstrap () {
        var initInjector = angular.injector(["ng"]),
            $q = initInjector.get("$q"),
            deferred = $q.defer();

        if (isCordova()) {
            $$(document).ready(function () {
                document.addEventListener("deviceready", function () {
                    onBootReady();
                    deferred.resolve(true);
                }, false);
            });
        } else {
            $$(document).ready(function () {
                onBootReady();
                deferred.resolve(false);
            });
        }
        return deferred.promise;
    }


    // Private
    function onBootReady () {
        angular.bootstrap(document.body, ["cpsMain"],{
            strictDi: !confFile.debug // Strict DI Mode
        });
    }

    function isCordova () {
        return !(!window.cordova && !window.PhoneGap && !window.phonegap);
    }

    // Exports
    module.exports = {
        _app: cpsApp,
        angular: angular,
        ngRoute: ngRoute,
        ngTouch: ngTouch,
        ngAnimate: ngAnimate,
        ngMessages: ngMessages,
        ngSanitize: ngSanitize,
        oclazyload: oclazyload,
        utils: utils,
        cpsUi: cpsUi,
        cpsMain: cpsMain,
        cpsLocale: cpsLocale,
        cpsSocial: cpsSocial,
        cpsNotify: cpsNotify,
        cpsSocket: cpsSocket,
        cpsLogin: cpsLogin,
        cpsRegister: cpsRegister,
        modules: modules,
        confFile: confFile,
        l10n: l10n,
        ld: ld,
        $$: $$,
        $: $
    };

})();
