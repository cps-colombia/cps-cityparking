/* global require */

// app/modules.js
//
// object to return and load modules
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    // Dependencies
    var angular = require("angular"),
        ld = require("lodash");

    // Functions
    /**
     * @name loadModules
     *
     * @description Load extra modules for cps mobileApp from
     * array in property modules in json config file
     * @param {Array} coreModules default modules array
     * @param {Object} confFile json file with configurations
     * @return {Array} extra and core modules
     */
    function loadModules (appModules, confFile) {
        angular.forEach(confFile.modules, function (module, key) {
            if(ld.isObject(module) && (module.name || module.id)) {
                this.push((module.name || module.id));
            } else {
                this.push(module);
            }
        }, appModules);

        return appModules;
    }

    // Exports
    module.exports = {
        loadModules: loadModules
    };

})();
