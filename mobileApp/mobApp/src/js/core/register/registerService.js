/* global module */

// app/core/register/registerService.js
//
// factory for register module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    registerService.$inject = [
        "$window",
        "$q",
        "$App",
        "$Mobile",
        "$Logger",
        "$Request",
        "$Locale"
    ];
    function registerService ($window, $q, $App, $Mobile, $Logger, $Request, $Locale) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance(),
            config = $App.config,
            registerOptions = angular.extend({
                requireAd: true,
                countryCode: true,
                phonePattern: "[0-9]{10}",
                phoneMin: 10,
                phoneMax: 10
            }, (config && config.options &&
                config.options.register || {})),
            op = $App.ops;

        // Members
        var Register = {
            options: registerOptions,
            sendRegister: sendRegister
        };
        return Register;

        // Funtions
        /**
         * register a new user in cps backend,
         * @param dataObject contains different parameter information
         * like: user, pass, name, last_name, address, email, bd, country, city
         */
        function sendRegister (dataObject, pass) {
            $Mobile.loading("show");

            var deferred = $q.defer(),
                success = false,
                req = { // Register Params
                    params: {
                        op: op.registerSend,
                        pass: pass,
                        lang: $Locale.getLanguage(),
                        app_token: config.apptoken,
                        app_secret: config.appsecret
                    }
                };
            angular.extend(req.params, dataObject);

            $Request.http(req).then(function (res) {
                var resData = res.data || {},
                    statusCode = resData.statusCode || res.status;

                logger.debug("register", JSON.stringify(res.data));
                $Mobile.loading("hide");

                if(statusCode === 200) {

                    success = true;
                    $Mobile.alert(_("alertRegisterOk"));
                    $Locale.setLanguage($Locale.getLanguage(), "save");

                    // Send success event register to Analytics
                    $Mobile.ga("send", "event", "register", "success");

                } else if(statusCode === 120) {

                    $Mobile.alert(_("alertEmailAlreadyRegister"));
                    $Mobile.ga("send", "event", "register", "error: email register");

                } else if(statusCode === 121) {

                    $Mobile.alert(_("alertUserAlreadyRegister"));
                    $Mobile.ga("send", "event", "register", "error: user register");

                } else if(statusCode === 122) {

                    $Mobile.alert(_("alertPhoneAlreadyRegister"));
                    $Mobile.ga("send", "event", "register", "error: phone register");
                }

                if(success) {
                    deferred.resolve(resData);
                } else {
                    $Mobile.ga("send", "event", "register", "error: " + resData.statusCode);
                    deferred.reject(resData);
                }

            });

            return deferred.promise;
        }
    }

    // exports
    module.exports = registerService;

})();
