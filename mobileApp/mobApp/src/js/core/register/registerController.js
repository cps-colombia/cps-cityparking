/* global module */

// app/core/register/registerController.js
//
// Register controller for cps register module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    RegisterController.$inject = [
        "$scope",
        "$rootScope",
        "$window",
        "$Utils",
        "$App",
        "$Logger",
        "$Mobile",
        "$Encrypt",
        "$Register",
        "AUTH_EVENTS"
    ];
    function RegisterController ($scope, $rootScope, $window, $Utils,
                                $App, $Logger, $Mobile, $Encrypt, $Register, AUTH_EVENTS) {
        var _ = $Mobile.locale,
            register = this,
            config = $App.config;

        // Members
        register.options = $Register.options;
        register.sendRegister = sendRegister;
        register.initRegister = initRegister;

        // Register params
        register.availableLocale = config.locales;

        register.dataSecret = {
            pass: "",
            repass: ""
        };
        register.dataRegister = {
            user: "",          // user,nid or cedula
            name: "",
            last_name: "",
            address: "",
            email: "",
            dob: {},
            bd: "2000-01-01",  // (yyyy-MM-dd)
            country: register.options.defaultCountry || 472,      // Default Colombia
            city: register.options.defaultCity || 103,         // Default Bogota
            msisdn: "",
            emailad: true
        };

        // Control
        register.formSuccess = false;
        register.emailExist = false;
        register.userExist = false;
        register.phoneExist = false;

        // Functions
        function initRegister () {
            var userData = $App.deviceData || {},
                extendData = {
                    name: userData.name,
                    last_name: userData.lastName,
                    email: userData.email,
                    msisdn: userData.phone
                };
            angular.extend(register.dataRegister, extendData);
        }

        function sendRegister (userInfo) {
            // send register
            if(register.dataSecret.pass !== register.dataSecret.repass) {
                $Mobile.loading("hide");
                $Mobile.alert(_("alertpasswords"));
            } else {
                if(!ld.isEmpty(register.dataRegister.dob)) {
                    register.dataRegister.bd = $Utils.parseDate(register.dataRegister.dob, {
                        withHour: false
                    });
                }
                $Register.sendRegister(register.dataRegister, $Encrypt.genEncrypt(ld.clone(register.dataSecret.pass)))
                    .then(function (res) {
                        // Clean form
                        register.formSuccess = true;

                        if(res.data) {
                            $rootScope.$emit(AUTH_EVENTS.registerSuccess, {
                                user: res.data.login,
                                pass: res.data.pass
                            });
                        }
                    }).catch(function (err) {
                        switch(err.statusCode) {
                        case 120:
                            register.emailExist = true;
                            break;
                        case 121:
                            register.userExist = true;
                            break;
                        case 122:
                            register.phoneExist = true;
                            break;
                        default:
                            $rootScope.$emit(AUTH_EVENTS.dataRegisterError);
                        }
                    });
            }
        }
    }

    // Exports
    module.exports = RegisterController;

})();
