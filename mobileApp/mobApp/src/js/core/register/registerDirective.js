/* global module */

// app/core/register/registerDirective.js
//
// directives function for register module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    function registerForm () {
        var registerDirective = {
            restrict: "E",
            templateUrl: "app/views/registerForm.html"
        };
        return registerDirective;
    }

    // Exports
    module.exports = {
        registerForm: registerForm
    };

})();
