/* global module */

// app/core/register/registerRoutes.js
//
// Routes for cps register module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    registerRoutes.$inject = ["$routeProvider", "USER_ROLES"];
    function registerRoutes ($routeProvider, USER_ROLES) {
        $routeProvider
            .when("/register", {
                url: "/register",
                templateUrl: "template/register.html",
                controller: "RegisterController",
                controllerAs: "register",
                data: {
                    title: "register",
                    roles: [USER_ROLES.guest],
                    inLogin: true
                }
            });
    }

    // Exports
    module.exports = registerRoutes;

})();
