/* global require */

// app/utils/index.js
//
// index function for utils
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        registerController = require("./registerController"),
        registerDirective = require("./registerDirective"),
        registerService = require("./registerService"),
        registerRoutes = require("./registerRoutes");

    angular.module("cpsRegister", []);
    var cpsRegister = angular.module("cpsRegister");

    cpsRegister.controller("RegisterController", registerController);

    // Directive
    cpsRegister.directive("cpsRegisterForm", registerDirective.registerForm);

    // Service
    cpsRegister.factory("$Register", registerService);

    // Routes
    cpsRegister.config(registerRoutes);

})();
