/* global require */

// core/ui/utils/backdropService.js
//
// faccotyr service create backdrop in the DOM
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    backdropService.$inject = [
        "$document",
        "$timeout",
        "$Dom",
        "$Logger"
    ];
    function backdropService ($document, $timeout, $Dom, $Logger) {
        var el = $$("<div class='backdrop'>"),
            logger = $Logger.getInstance(),
            backdropHolds = 0,
            _isOpen = [];

        $document[0].body.appendChild(el[0]);

        // Members
        var Backdrop = {
            retain: retain,
            release: release,
            getElement: getElement,
            isOpen: isOpen,
            setOpen: setOpen,
            // exposed for testing
            _element: el
        };
        return Backdrop;

        // Functions
        /**
         * retain
         *
         * Retains the backdrop.
         */
        function retain () {
            backdropHolds++;
            if (backdropHolds === 1) {
                el.addClass("visible");
                $Dom.requestAnimationFrame(function () {
                    // If we're still at >0 backdropHolds after async...
                    if (backdropHolds >= 1) {
                        el.addClass("active");
                    }
                });
            }
        }

        /**
         * release
         *
         * Releases the backdrop.
         */
        function release () {
            if (backdropHolds === 1) {
                el.removeClass("active");
                $timeout(function () {
                    // If we're still at 0 backdropHolds after async...
                    if (backdropHolds === 0) {
                        el.removeClass("visible");
                    }
                }, 400, false);
            }
            backdropHolds = Math.max(0, backdropHolds - 1);
        }

        function getElement () {
            return el;
        }

        function isOpen (options) {
            var id = ld.get(options, "id") || ld.get(options, "scope.$id");
            options = ld.extend({all: false}, options);

            if(options.all) {
                return _isOpen;
            } else if(ld.has(options, "viewType") && id) {
                logger.warn("backdrop", "isOpen need objec with viewType and id/$id");
                return false;
            } else {
                return ld.find(_isOpen, {id: id, viewType: options.viewType});
            }
        }

        function setOpen (dataObj) {
            var id = ld.get(dataObj, "id") || ld.get(dataObj, "scope.$id"),
                isShown = ld.isUndefined(ld.get(dataObj, "_isShown")) ?
                    ld.get(dataObj, "isShown") : ld.get(dataObj, "_isShown"),
                index;

            if(ld.has(dataObj, "viewType") && id) {
                if(!isShown) {
                    index = ld.findIndex(_isOpen, {id: id, viewType: dataObj.viewType});
                    if(index > -1) {
                        ld.pullAt(_isOpen, index);
                    }
                } else {
                    _isOpen.push({
                        id: id,
                        viewType: dataObj.viewType
                    });
                }
            } else {
                logger.warn("backdrop", "isOpen need objec with viewType and id/$id");
            }
        }
    }

    // Exports
    module.exports = backdropService;

})();
