/* global module */

// core/ui/utils/uiConfigService.js
//
// Management ui configuration
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    uiConfigService.$inject = ["$Dom", "$App", "$View"];
    function uiConfigService ($Dom, $App, $View) {
        var uiConfig = {},
            appConfig = $App.config,
            customUiConfig = appConfig.options && appConfig.options.ui || {},
            PLATFORM = "platform",
            configProperties = {
                views: {
                    maxCache: PLATFORM,
                    forwardCache: PLATFORM,
                    transition: PLATFORM,
                    swipeBackEnabled: PLATFORM,
                    swipeBackHitWidth: PLATFORM
                },
                dialog: {
                    closeBtn: PLATFORM,
                    stackPushDelay: PLATFORM
                },
                navBar: {
                    alignTitle: PLATFORM,
                    positionPrimaryButtons: PLATFORM,
                    positionSecondaryButtons: PLATFORM,
                    transition: PLATFORM
                },
                buttons: {
                    clear: PLATFORM,
                    add: PLATFORM,
                    close: PLATFORM,
                    cancel: PLATFORM,
                    done: PLATFORM,
                    remove: PLATFORM,
                    reorder: PLATFORM,
                    favorite: PLATFORM,
                    search: PLATFORM
                },
                backButton: {
                    icon: PLATFORM,
                    text: PLATFORM,
                    previousTitleText: PLATFORM
                },
                form: {
                    checkbox: PLATFORM,
                    toggle: PLATFORM
                },
                scrolling: {
                    jsScrolling: PLATFORM
                },
                spinner: {
                    icon: PLATFORM
                },
                loading: {
                    noBackdrop: PLATFORM
                },
                preload: {
                    duration: PLATFORM
                },
                tabs: {
                    style: PLATFORM,
                    position: PLATFORM
                },
                filter: {
                    theme: PLATFORM,
                    clear: PLATFORM,
                    add: PLATFORM,
                    close: PLATFORM,
                    done: PLATFORM,
                    remove: PLATFORM,
                    reorder: PLATFORM,
                    favorite: PLATFORM,
                    search: PLATFORM,
                    backdrop: PLATFORM,
                    transition: PLATFORM,
                    platform: {},
                    placeholder: PLATFORM
                },
                templates: {
                    maxPrefetch: PLATFORM
                },
                platform: {}
            };

        // Member
        uiConfig.platform = {};
        uiConfig.setPlatformConfig = setPlatformConfig;

        createConfig(configProperties, uiConfig, "");

        // Default
        // -------------------------
        setPlatformConfig("default", {
            views: {
                maxCache: 10,
                forwardCache: false,
                transition: "ios",
                swipeBackEnabled: true,
                swipeBackHitWidth: 45
            },

            navBar: {
                alignTitle: "center",
                positionPrimaryButtons: "left",
                positionSecondaryButtons: "right",
                transition: "view"
            },

            dialog: {
                closeBtn: false,
                stackPushDelay: 75
            },

            buttons: {
                clear: "ion-ios-close",
                add: "ion-ios-plus-outline",
                close: "ion-ios-close-empty",
                cancel: "ion-ios-close-empty",
                done: "ion-ios-checkmark-empty",
                remove: "ion-ios-trash-outline",
                reorder: "ion-drag",
                favorite: "ion-ios-star",
                search: "ion-ios-search-strong"
            },

            backButton: {
                icon: "ion-ios-arrow-back",
                text: "Back",
                previousTitleText: true
            },

            form: {
                checkbox: "circle",
                toggle: "large"
            },

            scrolling: {
                jsScrolling: true
            },

            spinner: {
                icon: "ios"
            },

            loading: {
                noBackdrop: true
            },

            preload: {
                duration: 300
            },

            tabs: {
                style: "standard",
                position: "bottom"
            },

            filter: {
                clear: "ion-ios-close",
                add: "ion-ios-plus-outline",
                close: "ion-ios-close-empty",
                done: "ion-ios-checkmark-empty",
                remove: "ion-ios-trash-outline",
                reorder: "ion-drag",
                favorite: "ion-ios-star",
                search: "ion-ios-search-strong",
                backdrop: true,
                transition: "vertical",
                placeholder: "search"
            },

            templates: {
                maxPrefetch: 30
            }

        });

        // iOS (it is the default already)
        // -------------------------
        setPlatformConfig("ios", {});

        // Android
        // -------------------------
        setPlatformConfig("android", {
            views: {
                transition: "android",
                swipeBackEnabled: false
            },

            navBar: {
                alignTitle: "left",
                positionPrimaryButtons: "right",
                positionSecondaryButtons: "right"
            },

            buttons: {
                clear: "ion-android-close",
                close: "ion-android-close",
                done: "ion-android-done",
                remove: "ion-android-delete",
                favorite: "ion-android-star"
            },

            backButton: {
                icon: "ion-android-arrow-back",
                text: false,
                previousTitleText: false
            },

            form: {
                checkbox: "square",
                toggle: "small"
            },

            spinner: {
                icon: "android"
            },

            tabs: {
                style: "striped",
                position: "top"
            },

            filter: {
                clear: "ion-android-close",
                close: "ion-android-close",
                done: "ion-android-done",
                cancel: "ion-android-close",
                remove: "ion-android-delete",
                favorite: "ion-android-star",
                search: false,
                backdrop: false,
                transition: "horizontal"
            }
        });

        // Windows Phone
        // -------------------------
        setPlatformConfig("windowsphone", {
            // scrolling: {
            //  jsScrolling: false
            // }
            spinner: {
                icon: "android"
            }
        });


        uiConfig.transitions = {
            views: {},
            navBar: {}
        };


        // iOS Transitions
        // -----------------------
        uiConfig.transitions.views.ios = function (enteringEle, leavingEle, direction, shouldAnimate) {
            var d;
            function setStyles (ele, opacity, x, boxShadowOpacity) {
                var css = {};
                css[$View.getCss().TRANSITION_DURATION] = d.shouldAnimate ? "" : 0;
                css.opacity = opacity;
                if (boxShadowOpacity > -1) {
                    css.boxShadow = "0 0 10px rgba(0,0,0," + (d.shouldAnimate ? boxShadowOpacity * 0.45 : 0.3) + ")";
                }
                css[$View.getCss().TRANSFORM] = "translate3d(" + x + "%,0,0)";
                $Dom.cachedStyles(ele, css);
            }

            d = {
                run: function (step) {
                    if (direction === "forward") {
                        setStyles(enteringEle, 1, (1 - step) * 99, 1 - step); // starting at 98% prevents a flicker
                        setStyles(leavingEle, (1 - 0.1 * step), step * -33, -1);

                    } else if (direction === "back") {
                        setStyles(enteringEle, (1 - 0.1 * (1 - step)), (1 - step) * -33, -1);
                        setStyles(leavingEle, 1, step * 100, 1 - step);

                    } else {
                        // swap, enter, exit
                        setStyles(enteringEle, 1, 0, -1);
                        setStyles(leavingEle, 0, 0, -1);
                    }
                },
                shouldAnimate: shouldAnimate && (direction === "forward" || direction === "back")
            };

            return d;
        };

        uiConfig.transitions.navBar.ios = function (enteringHeaderBar, leavingHeaderBar, direction, shouldAnimate) {
            var d;

            function setStyles (ctrl, opacity, titleX, backTextX) {
                var css = {};
                css[$View.getCss().TRANSITION_DURATION] = d.shouldAnimate ? "" : "0ms";
                css.opacity = opacity === 1 ? "" : opacity;

                ctrl.setCss("buttons-left", css);
                ctrl.setCss("buttons-right", css);
                ctrl.setCss("back-button", css);

                css[$View.getCss().TRANSFORM] = "translate3d(" + backTextX + "px,0,0)";
                ctrl.setCss("back-text", css);

                css[$View.getCss().TRANSFORM] = "translate3d(" + titleX + "px,0,0)";
                ctrl.setCss("title", css);
            }

            function enter (ctrlA, ctrlB, step) {
                if (!ctrlA || !ctrlB) {
                    return;
                }
                var titleX = (ctrlA.titleTextX() + ctrlA.titleWidth()) * (1 - step);
                var backTextX = (ctrlB && (ctrlB.titleTextX() - ctrlA.backButtonTextLeft()) * (1 - step)) || 0;
                setStyles(ctrlA, step, titleX, backTextX);
            }

            function leave (ctrlA, ctrlB, step) {
                if (!ctrlA || !ctrlB) {
                    return;
                }
                var titleX = (-(ctrlA.titleTextX() - ctrlB.backButtonTextLeft()) - (ctrlA.titleLeftRight())) * step;
                setStyles(ctrlA, 1 - step, titleX, 0);
            }

            d = {
                run: function (step) {
                    var enteringHeaderCtrl = enteringHeaderBar.controller();
                    var leavingHeaderCtrl = leavingHeaderBar && leavingHeaderBar.controller();
                    if (d.direction === "back") {
                        leave(enteringHeaderCtrl, leavingHeaderCtrl, 1 - step);
                        enter(leavingHeaderCtrl, enteringHeaderCtrl, 1 - step);
                    } else {
                        enter(enteringHeaderCtrl, leavingHeaderCtrl, step);
                        leave(leavingHeaderCtrl, enteringHeaderCtrl, step);
                    }
                },
                direction: direction,
                shouldAnimate: shouldAnimate && (direction === "forward" || direction === "back")
            };

            return d;
        };


        // Android Transitions
        // -----------------------

        uiConfig.transitions.views.android = function (enteringEle, leavingEle, direction, shouldAnimate) {
            var d;
            shouldAnimate = shouldAnimate && (direction === "forward" || direction === "back");

            function setStyles (ele, x) {
                var css = {};
                css[$View.getCss().TRANSITION_DURATION] = d.shouldAnimate ? "" : 0;
                css[$View.getCss().TRANSFORM] = "translate3d(" + x + "%,0,0)";
                $Dom.cachedStyles(ele, css);
            }

            d = {
                run: function (step) {
                    if (direction === "forward") {
                        setStyles(enteringEle, (1 - step) * 99); // starting at 98% prevents a flicker
                        setStyles(leavingEle, step * -100);

                    } else if (direction === "back") {
                        setStyles(enteringEle, (1 - step) * -100);
                        setStyles(leavingEle, step * 100);

                    } else {
                        // swap, enter, exit
                        setStyles(enteringEle, 0);
                        setStyles(leavingEle, 0);
                    }
                },
                shouldAnimate: shouldAnimate
            };

            return d;
        };

        uiConfig.transitions.navBar.android = function (enteringHeaderBar, leavingHeaderBar, direction, shouldAnimate) {

            function setStyles (ctrl, opacity) {
                if (!ctrl) {
                    return;
                }
                var css = {};
                css.opacity = opacity === 1 ? "" : opacity;

                ctrl.setCss("buttons-left", css);
                ctrl.setCss("buttons-right", css);
                ctrl.setCss("back-button", css);
                ctrl.setCss("back-text", css);
                ctrl.setCss("title", css);
            }

            return {
                run: function (step) {
                    setStyles(enteringHeaderBar.controller(), step);
                    setStyles(leavingHeaderBar && leavingHeaderBar.controller(), 1 - step);
                },
                shouldAnimate: shouldAnimate && (direction === "forward" || direction === "back")
            };
        };


        // No Transition
        // -----------------------

        uiConfig.transitions.views.none = function (enteringEle, leavingEle) {
            return {
                run: function (step) {
                    uiConfig.transitions.views.android(enteringEle, leavingEle, false, false).run(step);
                },
                shouldAnimate: false
            };
        };

        uiConfig.transitions.navBar.none = function (enteringHeaderBar, leavingHeaderBar) {
            return {
                run: function (step) {
                    uiConfig.transitions.navBar.ios(enteringHeaderBar, leavingHeaderBar, false, false).run(step);
                    uiConfig.transitions.navBar.android(enteringHeaderBar, leavingHeaderBar, false, false).run(step);
                },
                shouldAnimate: false
            };
        };


        // private: used to set platform configs
        function setPlatformConfig (platformName, platformConfigs) {
            configProperties.platform[platformName] = angular.extend(
                platformConfigs,
                customUiConfig[platformName] || customUiConfig
            );
            uiConfig.platform[platformName] = {};

            addConfig(configProperties, configProperties.platform[platformName]);

            createConfig(configProperties.platform[platformName], uiConfig.platform[platformName], "");
        }


        // private: used to recursively add new platform configs
        function addConfig (configObj, platformObj) {
            for (var n in configObj) {
                if (n !== PLATFORM && configObj.hasOwnProperty(n)) {
                    if (angular.isObject(configObj[n])) {
                        if (!angular.isDefined(platformObj[n])) {
                            platformObj[n] = {};
                        }
                        addConfig(configObj[n], platformObj[n]);

                    } else if (!angular.isDefined(platformObj[n])) {
                        platformObj[n] = null;
                    }
                }
            }
        }


        // private: create methods for each config to get/set
        function createConfig (configObj, uiConfigObj, platformPath) {
            angular.forEach(configObj, function (value, namespace) {

                if (angular.isObject(configObj[namespace])) {
                    // recursively drill down the config object so we can create a method for each one
                    uiConfigObj[namespace] = {};
                    createConfig(configObj[namespace], uiConfigObj[namespace], platformPath + "." + namespace);
                } else {
                    // create a method for the uiConfig/config methods that will be exposed
                    uiConfigObj[namespace] = function (newValue) {
                        if (arguments.length) {
                            configObj[namespace] = newValue;
                            return uiConfigObj;
                        }
                        if (configObj[namespace] === PLATFORM) {
                            // if the config is set to 'platform', then get this config's platform value
                            var platformConfig = stringObj(configProperties.platform,
                                                           $App.getPlatform().platform +
                                                           platformPath + "." + namespace);
                            if (platformConfig || platformConfig === false) {
                                return platformConfig;
                            }
                            // didnt find a specific platform config, now try the default
                            return stringObj(configProperties.platform, "default" + platformPath + "." + namespace);
                        }
                        return configObj[namespace];
                    };
                }

            });
        }

        function stringObj (obj, str) {
            str = str.split(".");
            for (var i = 0; i < str.length; i++) {
                if (obj && angular.isDefined(obj[str[i]])) {
                    obj = obj[str[i]];
                } else {
                    return null;
                }
            }
            return obj;
        }

        // private: Service definition for internal Cps use
        /**
         * @name uiConfig
         * @private
         */
        uiConfig.get = function () {
            return uiConfig;
        };

        //
        return uiConfig;
    }

    // Export
    module.exports = uiConfigService;

})();
