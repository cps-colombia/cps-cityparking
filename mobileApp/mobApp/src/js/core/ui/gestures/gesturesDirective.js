/* global module */

// core/ui/gestures/gesturesDirective.js
//
// directive for gestures/hammer
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        Hammer = require("hammerjs");

    function loadGestures (directiveName, eventName) {
        gesturesDirective.$inject = ["$timeout", "$window", "$parse","$Logger", "$Gestures"];
        function gesturesDirective ($timeout, $window, $parse, $Logger, $Gestures) {
            var logger = $Logger.getInstance(),
                hammerDirective = {
                    restrict: "A",
                    link: postLink
                };
            return hammerDirective;

            // Functions
            function postLink (scope, element, attrs) {
                // var handlerName = attrs[directiveName],
                attrs.$observe(directiveName, function (value) {
                    var gesturesOpts = $Gestures.getOptions(eventName),
                        callback = $parse(value),
                        opts = $parse(attrs[directiveName + "Opts"])(scope, {}),
                        defaultOpts = angular.copy(gesturesOpts.hammer);

                    angular.extend(defaultOpts, opts);

                    if (!Hammer || !$window.addEventListener) {
                        if (directiveName === "hmTap") {
                            element.bind("click", handler);
                        }
                        if (directiveName === "hmDoubletap") {
                            element.bind("dblclick", handler);
                        }
                        return;
                    }

                    if (angular.isUndefined(element.hammertime) && !element.hammer && eventName) {
                        element.hammer = $Gestures.hammer(element[0], eventName, handler, defaultOpts);
                        scope.$on("$destroy", function () {
                            $Gestures.off(element[0], eventName);
                            element.hammer.off(eventName, handler);
                            element.hammer.destroy();
                        });
                    }

                    // Handler event
                    function handler (event) {
                        if (gesturesOpts.verbose) {
                            logger.debug("gestures", eventName + " - " + JSON.stringify(event.center));
                        }

                        var phase = scope.$root && scope.$root.$$phase,
                            callbackHandler = function callbackHandler () {
                                var cb = callback(scope, { $event : event});
                                if (typeof cb === "function") {
                                    cb.call(scope, event);
                                }
                            };

                        if (phase === "$apply" || phase === "$digest") {
                            scope.$evalAsync(callbackHandler());
                        } else {
                            scope.$apply(callbackHandler);
                        }
                    }
                });
            }
        }

        return gesturesDirective;
    }

    // Export
    module.exports = {
        loadGestures: loadGestures
    };

})();
