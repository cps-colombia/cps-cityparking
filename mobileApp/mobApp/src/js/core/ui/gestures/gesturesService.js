/* global module */

// core/ui/gestures/gesturesService.js
//
// factory service for hammer gestures
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        Hammer = require("hammerjs"),
        $$ = angular.element;

    function gesturesService () {
        var opts = {
                verbose: false,
                preventGhosts: false,
                preventDefault: false,
                hammer: {
                    recognizers: {
                        Pan: {enable: true},
                        Pinch: {enable: false},
                        Press: {enable: false},
                        Rotate: {enable: false},
                        Swipe: {enable: true},
                        Tap: {
                            enable: true,
                            threshold: 150
                        }
                    }
                }
            }, GesturesServices = {
                set: set,
                $get: $get
            };
        return GesturesServices;

        // Functions
        function set (value) {
            var recognizers = angular.copy(opts.hammer.recognizers),
                valRecognizers,
                valueOptions = {};

            if(angular.isObject(value)) {
                if(value.hammer) {
                    valRecognizers = value.hammer.recognizers;
                    if(valRecognizers) {
                        angular.forEach(valRecognizers, function (hvalue, hkey) {
                            hkey = hkey.toLowerCase().charAt(0).toUpperCase() + hkey.slice(1);
                            valueOptions[hkey] = hvalue;
                        });
                        value.hammer.recognizers = angular.extend(recognizers, valueOptions);
                    }
                }
                opts = angular.extend(opts, value);
            }
        }
        function $get () {
            // Memebers
            var Gestures = {
                getOptions: getOptions,
                hammer: hammer,
                on: on,
                off: off,
                onGesture: onGesture,
                offGesture: offGesture,
                destroy: destroy
            };
            return Gestures;

            // Functions
            function getOptions (eventName) {
                var recognizers = angular.copy(opts.hammer.recognizers),
                    options = angular.copy(opts),
                    optsRecognizers = [],
                    singleRecognizer,
                    eventType;
                if(eventName) {
                    // only send options by eventName
                    if(eventName.indexOf("pan") >= 0 || eventName.indexOf("drag") >= 0) {
                        eventType = "Pan";
                    } else if(eventName.indexOf("tap") >= 0) {
                        eventType = "Tap";
                    } else if(eventName.indexOf("pinch") >= 0) {
                        eventType = "Pinch";
                    } else if(eventName.indexOf("press") >= 0) {
                        eventType = "Press";
                    } else if(eventName.indexOf("rotate") >= 0) {
                        eventType = "Rotate";
                    } else if(eventName.indexOf("swipe") >= 0) {
                        eventType = "Swipe";
                    } else {
                        eventType = "Tap";
                    }
                    singleRecognizer = recognizers[eventType];
                    recognizers = {};
                    recognizers[eventType] = singleRecognizer;
                }

                // Push Hammer event instance
                angular.forEach(recognizers, function (hvalue, hkey) {
                    optsRecognizers.push([Hammer[hkey], hvalue]);
                });
                options.hammer.recognizers = optsRecognizers;
                return options;
            }

            function hammer (element, eventName, callbackHandler, hmOptions) {
                var gesture,
                    recognizer;

                hmOptions = hmOptions || getOptions(eventName).hammer;
                callbackHandler = callbackHandler || angular.noop;

                gesture = new Hammer.Manager(element, hmOptions);

                // register actual event
                gesture.on(eventName, function (e) {
                    recognizer = gesture.get(e.type);
                    if (recognizer) {
                        if (recognizer.options.preventDefault || hmOptions.preventDefault) {
                            e.srcEvent.preventDefault();
                        }
                        if (recognizer.options.stopPropagation) {
                            e.srcEvent.stopPropagation();
                        }
                    }
                    // Preventing Ghost Clicks
                    if(hmOptions.preventGhosts) {
                        preventGhosts(element);
                    }
                    if(!e.srcEvent.defaultPrevented && $$(e.target).closest("[data-tap-disabled=true]")[0]) {
                        e.srcEvent.preventDefault();
                    }
                    callbackHandler(e);
                });
                return gesture;
            }

            function on (element, eventName, callbackHandler) {
                Hammer.on(element, eventName, callbackHandler);
            }

            function off (element, eventName, callbackHandler) {
                Hammer.off(element, eventName, callbackHandler);
            }

            function onGesture (gesture, eventName, callbackHandler) {
                if(gesture) {
                    gesture.on(eventName, callbackHandler);
                }
            }

            function offGesture (gesture, eventName, callbackHandler) {
                if(gesture) {
                    gesture.off(eventName, callbackHandler);
                }
            }

            function destroy (gesture) {
                if(gesture) {
                    gesture.destroy();
                }
            }

            // Private

            /**
             * Modified from: https://gist.github.com/jtangelder/361052976f044200ea17
             *
             * Prevent click events after a touchend.
             *
             * Inspired/copy-paste from this article of Google by Ryan Fioravanti
             * https://developers.google.com/mobile/articles/fast_buttons#ghost
             */

            function preventGhosts (element) {
                if (!element) {
                    return;
                }

                var coordinates = [],
                    threshold = 25,
                    timeout = 2500;

                if ("ontouchstart" in window) {
                    element[0].addEventListener("touchstart", resetCoordinates, true);
                    element[0].addEventListener("touchend", registerCoordinates, true);
                    element[0].addEventListener("click", preventGhostClick, true);
                    element[0].addEventListener("mouseup", preventGhostClick, true);
                }


                /**
                 * prevent clicks if they're in a registered XY region
                 * @param {MouseEvent} ev
                 */
                function preventGhostClick (e) {
                    for (var i = 0; i < coordinates.length; i++) {
                        var x = coordinates[i][0];
                        var y = coordinates[i][1];

                        // within the range, so prevent the click
                        if (Math.abs(e.clientX - x) < threshold &&
                            Math.abs(e.clientY - y) < threshold) {
                            e.stopPropagation();
                            e.preventDefault();
                            break;
                        }
                    }
                }

                /**
                 * reset the coordinates array
                 */
                function resetCoordinates () {
                    coordinates = [];
                }

                /**
                 * remove the first coordinates set from the array
                 */
                function popCoordinates () {
                    coordinates.splice(0, 1);
                }

                /**
                 * if it is an final touchend, we want to register it's place
                 * @param {TouchEvent} ev
                 */
                function registerCoordinates (e) {
                    // touchend is triggered on every releasing finger
                    // changed touches always contain the removed touches on a touchend
                    // the touches object might contain these also at some browsers (firefox os)
                    // so touches - changedTouches will be 0 or lower, like -1, on the final touchend
                    if(e.touches.length - e.changedTouches.length <= 0) {
                        var touch = e.changedTouches[0];
                        coordinates.push([touch.clientX, touch.clientY]);

                        setTimeout(popCoordinates, timeout);
                    }
                }
            }

        }
    }

    // Exports
    module.exports = gesturesService;

})();
