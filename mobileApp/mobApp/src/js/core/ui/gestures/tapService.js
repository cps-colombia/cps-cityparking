/* global module */

// core/ui/gestures/tapService.js
//
// service to control tap
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    tapService.$inject = ["$rootScope", "$Dom", "$Logger", "$App", "$Activator", "$Keyboard", "$Element"];
    function tapService ($rootScope, $Dom, $Logger, $App, $Activator, $Keyboard, $Element) {
        var logger = $Logger.getInstance(),
            tapDoc, // the element which the listeners are on (document.body)
            tapActiveEle, // the element which is active (probably has focus)
            tapEnabledTouchEvents,
            tapMouseResetTimer,
            tapPointerMoved,
            tapPointerStart,
            tapTouchFocusedInput,
            tapLastTouchTarget,
            tapTouchMoveListener = "touchmove",
            // how much the coordinates can be off between start/end, but still a click
            TAP_RELEASE_TOLERANCE = 12, // default tolerance
            TAP_RELEASE_BUTTON_TOLERANCE = 50, // button elements should have a larger tolerance

            tapEventListeners = {
                "click": tapClickGateKeeper,

                "mousedown": tapMouseDown,
                "mouseup": tapMouseUp,
                "mousemove": tapMouseMove,

                "touchstart": tapTouchStart,
                "touchend": tapTouchEnd,
                "touchcancel": tapTouchCancel,
                "touchmove": tapTouchMove,

                "pointerdown": tapTouchStart,
                "pointerup": tapTouchEnd,
                "pointercancel": tapTouchCancel,
                "pointermove": tapTouchMove,

                "MSPointerDown": tapTouchStart,
                "MSPointerUp": tapTouchEnd,
                "MSPointerCancel": tapTouchCancel,
                "MSPointerMove": tapTouchMove,

                "focusin": tapFocusIn,
                "focusout": tapFocusOut
            };

        // Memebres
        var Tap = {
            hasCheckedClone: false,
            register: register,
            ignoreScrollStart: ignoreScrollStart,
            isTextInput: isTextInput,
            isDateInput: isDateInput,
            isKeyboardElement: isKeyboardElement,
            isLabelWithTextInput: isLabelWithTextInput,
            containsOrIsTextInput: containsOrIsTextInput,
            cloneFocusedInput: cloneFocusedInput,
            removeClonedInputs: removeClonedInputs,
            requiresNativeClick: requiresNativeClick,
            isLabelContainingFileInput: isLabelContainingFileInput,
            isElementTapDisabled: isElementTapDisabled,
            setTolerance: setTolerance,
            cancelClick: cancelClick,
            pointerCoord: pointerCoord
        };
        return Tap;

        // Functions
        function register (ele) {
            tapDoc = ele;

            tapEventListener("click", true, true);
            tapEventListener("mouseup");
            tapEventListener("mousedown");

            if (window.navigator.pointerEnabled) {
                tapEventListener("pointerdown");
                tapEventListener("pointerup");
                tapEventListener("pointcancel");
                tapTouchMoveListener = "pointermove";

            } else if (window.navigator.msPointerEnabled) {
                tapEventListener("MSPointerDown");
                tapEventListener("MSPointerUp");
                tapEventListener("MSPointerCancel");
                tapTouchMoveListener = "MSPointerMove";

            } else {
                tapEventListener("touchstart");
                tapEventListener("touchend");
                tapEventListener("touchcancel");
            }

            tapEventListener("focusin");
            tapEventListener("focusout");

            return function () {
                for (var type in tapEventListeners) {
                    if(tapEventListeners.hasOwnPropertie(type)) {
                        tapEventListener(type, false);
                    }
                }
                tapDoc = null;
                tapActiveEle = null;
                tapEnabledTouchEvents = false;
                tapPointerMoved = false;
                tapPointerStart = null;
            };
        }

        function ignoreScrollStart (e) {
            return (e.defaultPrevented) || // defaultPrevented has been assigned by another component handling the event
                (/^(file|range)$/i).test(e.target.type) ||
                // manually set within an elements attrs
                (e.target.dataset ? e.target.dataset.preventScroll :
                 e.target.getAttribute("data-prevent-scroll")) === "true" ||
                // flash/movie/object touches should not try to scroll
                (!!(/^(object|embed)$/i).test(e.target.tagName)) ||
                // check if this element, or an ancestor, has `data-tap-disabled` attribute
                isElementTapDisabled(e.target);
        }

        function isTextInput (ele) {
            return $Element.isTextInput(ele);
        }

        function isDateInput (ele) {
            return $Element.isDateInput(ele);
        }

        function isKeyboardElement (ele) {
            return $Element.isKeyboardElement(ele);
        }

        function isLabelWithTextInput (ele) {
            var container = tapContainingElement(ele, false);

            return !!container &&
                isTextInput(tapTargetElement(container));
        }

        function containsOrIsTextInput (ele) {
            return isTextInput(ele) || isLabelWithTextInput(ele);
        }

        function cloneFocusedInput (container) {
            if (Tap.hasCheckedClone) {
                return;
            }
            Tap.hasCheckedClone = true;

            $Dom.requestAnimationFrame(function () {
                var focusInput = container.querySelector(":focus");
                if (isTextInput(focusInput)) {
                    var clonedInput = focusInput.cloneNode(true);

                    clonedInput.value = focusInput.value;
                    clonedInput.classList.add("cloned-text-input");
                    clonedInput.readOnly = true;
                    if (focusInput.isContentEditable) {
                        clonedInput.contentEditable = focusInput.contentEditable;
                        clonedInput.innerHTML = focusInput.innerHTML;
                    }
                    focusInput.parentElement.insertBefore(clonedInput, focusInput);
                    focusInput.classList.add("previous-input-focus");

                    clonedInput.scrollTop = focusInput.scrollTop;
                }
            });
        }

        function removeClonedInputs (container) {
            Tap.hasCheckedClone = false;

            $Dom.requestAnimationFrame(function () {
                var clonedInputs = container.querySelectorAll(".cloned-text-input");
                var previousInputFocus = container.querySelectorAll(".previous-input-focus");
                var x;

                for (x = 0; x < clonedInputs.length; x++) {
                    clonedInputs[x].parentElement.removeChild(clonedInputs[x]);
                }

                for (x = 0; x < previousInputFocus.length; x++) {
                    previousInputFocus[x].classList.remove("previous-input-focus");
                    previousInputFocus[x].style.top = "";
                    if ($Keyboard.isOpen() && !$Keyboard.isClosing()) {
                        previousInputFocus[x].focus();
                    }
                }
            });
        }

        function requiresNativeClick (ele) {
            if (!ele || ele.disabled || (/^(file|range)$/i).test(ele.type) ||
                (/^(object|video)$/i).test(ele.tagName) ||
                isLabelContainingFileInput(ele)) {
                return true;
            }
            return isElementTapDisabled(ele);
        }

        function isLabelContainingFileInput (ele) {
            var lbl = tapContainingElement(ele),
                fileInput = lbl.querySelector("input[type=file]");
            if (lbl.tagName !== "LABEL") {
                return false;
            }
            if (fileInput && fileInput.disabled === false) {
                return true;
            }
            return false;
        }

        function isElementTapDisabled (ele) {
            if (ele && ele.nodeType === 1) {
                var element = ele;
                while (element) {
                    if ((element.dataset ?
                         element.dataset.tapDisabled :
                         element.getAttribute("data-tap-disabled")) === "true") {
                        return true;
                    }
                    element = element.parentElement;
                }
            }
            return false;
        }

        function setTolerance (releaseTolerance, releaseButtonTolerance) {
            TAP_RELEASE_TOLERANCE = releaseTolerance;
            TAP_RELEASE_BUTTON_TOLERANCE = releaseButtonTolerance;
        }

        function cancelClick () {
            // used to cancel any simulated clicks which may happen on a touchend/mouseup
            // gestures uses this method within its tap and hold events
            tapPointerMoved = true;
        }

        function pointerCoord (event) {
            // This method can get coordinates for both a mouse click
            // or a touch depending on the given event
            var c = { x: 0, y: 0 };
            if (event) {
                var touches = event.touches && event.touches.length ? event.touches : [event],
                    e = (event.changedTouches && event.changedTouches[0]) || touches[0];
                if (e) {
                    c.x = e.clientX || e.pageX || 0;
                    c.y = e.clientY || e.pageY || 0;
                }
            }
            return c;
        }

        // Private
        function afterActivator (e) {
            return (($rootScope.scroll && $rootScope.scroll.isScrolling) || requiresNativeClick(e.target));
        }

        function tapEventListener (type, enable, useCapture) {
            if (enable !== false) {
                tapDoc.addEventListener(type, tapEventListeners[type], useCapture);
            } else {
                tapDoc.removeEventListener(type, tapEventListeners[type]);
            }
        }

        function tapClick (e) {
            // simulate a normal click by running the element's click method then focus on it
            var container = tapContainingElement(e.target);
            var ele = tapTargetElement(container);

            if (requiresNativeClick(ele) || tapPointerMoved) {
                return false;
            }

            var c = pointerCoord(e);

            // logger.debug('tapClick', e.type, ele.tagName, '('+c.x+','+c.y+')');
            triggerMouseEvent("click", ele, c.x, c.y);

            // if it's an input, focus in on the target, otherwise blur
            tapHandleFocus(ele);
        }

        function triggerMouseEvent (type, ele, x, y) {
            // using initMouseEvent instead of MouseEvent for our Android friends
            var clickEvent = document.createEvent("MouseEvents");
            clickEvent.initMouseEvent(type, true, true, window, 1, 0, 0, x, y, false, false, false, false, 0, null);
            clickEvent.isCpsTap = true;
            ele.dispatchEvent(clickEvent);
        }

        function tapClickGateKeeper (e) {
            // logger.debug('click ' + Date.now() + ' isCpsTap: ' + (e.isCpsTap ? true : false));
            if (e.target.type === "submit" && e.detail === 0) {
                // do not prevent click if it came from an "Enter" or "Go" keypress submit
                return null;
            }

            // do not allow through any click events that were not created by $Tap
            if (($rootScope.scroll.isScrolling && containsOrIsTextInput(e.target)) ||
                (!e.isCpsTap && !requiresNativeClick(e.target))) {
                // logger.debug('clickPrevent', e.target.tagName);
                e.stopPropagation();

                if (!isLabelWithTextInput(e.target)) {
                    // labels clicks from native should not preventDefault
                    // othersize keyboard will not show on input focus
                    e.preventDefault();
                }
                return false;
            }
        }

        // MOUSE
        function tapMouseDown (e) {
            var hitX;
            // logger.debug('mousedown ' + Date.now());
            if (e.isCpsTap || tapIgnoreEvent(e)) {
                return null;
            }

            if (tapEnabledTouchEvents) {
                e.stopPropagation();

                if ((!isTextInput(e.target) || tapLastTouchTarget !== e.target) &&
                    !(/^(select|option)$/i).test(e.target.tagName)) {
                    // If you preventDefault on a text input then you cannot move its text caret/cursor.
                    // Allow through only the text input default. However, without preventDefault on an
                    // input the 300ms delay can change focus on inputs after the keyboard shows up.
                    // The focusin event handles the chance of focus changing after the keyboard shows.
                    e.preventDefault();
                }

                return false;
            }

            tapPointerMoved = false;
            tapPointerStart = pointerCoord(e);

            tapEventListener("mousemove");

            hitX = pointerCoord(e).x;
            if (hitX > 0 && hitX < 30) {
                return;
            }
            if(requiresNativeClick(e.target)) {
                return;
            }
            // Acivate
            if(!afterActivator(e)) {
                $Activator.start(e);
            }
        }

        function tapMouseUp (e) {
            // logger.debug("mouseup " + Date.now());
            if (tapEnabledTouchEvents) {
                e.stopPropagation();
                e.preventDefault();
                return false;
            }

            if (tapIgnoreEvent(e) || (/^(select|option)$/i).test(e.target.tagName)) {
                return false;
            }

            if (!tapHasPointerMoved(e)) {
                tapClick(e);
            }
            tapEventListener("mousemove", false);
            $Activator.end();
            tapPointerMoved = false;
        }

        function tapMouseMove (e) {
            if (tapHasPointerMoved(e)) {
                tapEventListener("mousemove", false);
                $Activator.end();
                tapPointerMoved = true;
                return false;
            }
        }


        // TOUCH
        function tapTouchStart (e) {
            var hitX;
            // logger.debug("touchstart " + Date.now());
            if (tapIgnoreEvent(e)) {
                return;
            }

            tapPointerMoved = false;

            tapEnableTouchEvents();
            tapPointerStart = pointerCoord(e);

            tapEventListener(tapTouchMoveListener);

            hitX = pointerCoord(e).x;
            if (hitX > 0 && hitX < 30) {
                return;
            }
            if(requiresNativeClick(e.target)) {
                return;
            }
            if(!afterActivator(e)) {
                $Activator.start(e);
            }

            if ($App.isIOS() && isLabelWithTextInput(e.target)) {
                // if the tapped element is a label, which has a child input
                // then preventDefault so iOS doesn't ugly auto scroll to the input
                // but do not prevent default on Android or else you cannot move the text caret
                // and do not prevent default on Android or else no virtual keyboard shows up

                var textInput = tapTargetElement(tapContainingElement(e.target));
                if (textInput !== tapActiveEle) {
                    // don't preventDefault on an already focused input or else iOS's text caret isn't usable
                    e.preventDefault();
                }
            }
        }

        function tapTouchEnd (e) {
            // logger.debug('touchend ' + Date.now());
            if (tapIgnoreEvent(e)) {
                return;
            }

            tapEnableTouchEvents();
            if (!tapHasPointerMoved(e)) {
                tapClick(e);

                if ((/^(select|option)$/i).test(e.target.tagName)) {
                    e.preventDefault();
                }
            }

            tapLastTouchTarget = e.target;
            tapTouchCancel();
        }

        function tapTouchMove (e) {
            if (tapHasPointerMoved(e)) {
                tapPointerMoved = true;
                tapEventListener(tapTouchMoveListener, false);
                $Activator.end();
                return false;
            }
        }

        function tapTouchCancel () {
            tapEventListener(tapTouchMoveListener, false);
            $Activator.end();
            tapPointerMoved = false;
        }

        function tapEnableTouchEvents () {
            tapEnabledTouchEvents = true;
            clearTimeout(tapMouseResetTimer);
            tapMouseResetTimer = setTimeout(function () {
                tapEnabledTouchEvents = false;
            }, 600);
        }

        function tapIgnoreEvent (e) {
            if (e.isTapHandled) {
                return true;
            }
            e.isTapHandled = true;

            if(isElementTapDisabled(e.target)) {
                return true;
            }

            if ($rootScope.scroll.isScrolling && containsOrIsTextInput(e.target)) {
                e.preventDefault();
                return true;
            }
        }

        function tapHandleFocus (ele) {
            tapTouchFocusedInput = null;
            var triggerFocusIn = false;

            if (ele.tagName === "SELECT") {
                // trick to force Android options to show up
                triggerMouseEvent("mousedown", ele, 0, 0);
                if(ele.focus) {
                    ele.focus();
                }
                triggerFocusIn = true;

            } else if (tapActiveElement() === ele) {
                // already is the active element and has focus
                triggerFocusIn = true;

            } else if ((/^(input|textarea)$/i).test(ele.tagName) || ele.isContentEditable) {
                triggerFocusIn = true;
                if(ele.focus) {
                    ele.focus();
                }
                ele.value = ele.value;
                if (tapEnabledTouchEvents) {
                    tapTouchFocusedInput = ele;
                }

            } else {
                tapFocusOutActive();
            }

            if (triggerFocusIn) {
                tapActiveElement(ele);
                $Dom.trigger("cps:focusin", {
                    target: ele
                }, true);
            }
        }

        function tapFocusOutActive () {
            var ele = tapActiveElement();
            if (ele && ((/^(input|textarea|select)$/i).test(ele.tagName) || ele.isContentEditable)) {
                logger.debug("tapFocusOutActive", ele.tagName);
                ele.blur();
            }
            tapActiveElement(null);
        }

        function tapFocusIn (e) {
            // logger.debug('focusin ' + Date.now());
            // Because a text input doesn't preventDefault (so the caret still works) there's a chance
            // that its mousedown event 300ms later will change the focus to another element after
            // the keyboard shows up.

            if (tapEnabledTouchEvents &&
                isTextInput(tapActiveElement()) &&
                isTextInput(tapTouchFocusedInput) &&
                tapTouchFocusedInput !== e.target) {

                // 1) The pointer is from touch events
                // 2) There is an active element which is a text input
                // 3) A text input was just set to be focused on by a touch event
                // 4) A new focus has been set, however the target isn't the one the touch event wanted
                // logger.debug('focusin', 'tapTouchFocusedInput');
                tapTouchFocusedInput.focus();
                tapTouchFocusedInput = null;
            }
            $rootScope.scroll.isScrolling = false;
        }

        function tapFocusOut () {
            // logger.debug("focusout");
            tapActiveElement(null);
        }

        function tapActiveElement (ele) {
            if (arguments.length) {
                tapActiveEle = ele;
            }
            return tapActiveEle || document.activeElement;
        }

        function tapHasPointerMoved (endEvent) {
            if (!endEvent || endEvent.target.nodeType !== 1 || !tapPointerStart ||
                (tapPointerStart.x === 0 && tapPointerStart.y === 0)) {
                return false;
            }
            var endCoordinates = pointerCoord(endEvent),
                hasClassList = !!(endEvent.target.classList && endEvent.target.classList.contains &&
                                  typeof endEvent.target.classList.contains === "function"),
                releaseTolerance = hasClassList && endEvent.target.classList.contains("button") ?
                    TAP_RELEASE_BUTTON_TOLERANCE :
                    TAP_RELEASE_TOLERANCE;

            return Math.abs(tapPointerStart.x - endCoordinates.x) > releaseTolerance ||
                Math.abs(tapPointerStart.y - endCoordinates.y) > releaseTolerance;
        }

        function tapContainingElement (ele, allowSelf) {
            var climbEle = ele;
            for (var x = 0; x < 6; x++) {
                if (!climbEle) {
                    break;
                }
                if (climbEle.tagName === "LABEL") {
                    return climbEle;
                }
                climbEle = climbEle.parentElement;
            }
            if (allowSelf !== false) {
                return ele;
            }
        }

        function tapTargetElement (ele) {
            if (ele && ele.tagName === "LABEL") {
                if (ele.control) {
                    return ele.control;
                }

                // older devices do not support the "control" property
                if (ele.querySelector) {
                    var control = ele.querySelector("input,textarea,select");
                    if (control) {
                        return control;
                    }
                }
            }
            return ele;
        }

    }

    // Exports
    module.exports = tapService;

})();
