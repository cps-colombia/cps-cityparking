(function () {
    "use strict";

    var angular = require("angular");

    gesturesRun.$inject = ["$document", "$Dom", "$Tap"];
    function gesturesRun ($document, $Dom, $Tap) {
        $Dom.ready(function () {
            var ng = typeof angular !== "undefined" ? angular : null;
            if (!ng || (ng && !ng.scenario)) {
                $Tap.register($document[0]);
            }
        });
    }

    // Export
    module.exports = gesturesRun;

})();
