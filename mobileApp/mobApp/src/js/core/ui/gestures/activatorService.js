/* global module */

// core/ui/gestures/activatorServices.js
//
// factory service for add class to active elements,
// is required because the :active css some times is active
// although no execute action
//
// 2015, CPS - Cellular Parking Systems


(function () {
    "use strict";

    activatorService.$inject = [
        "$timeout",
        "$Dom"
    ];
    function activatorService ($timeout, $Dom) {
        var queueElements = {},  // elements that should get an active state in XX milliseconds
            activeElements = {},  // elements that are currently active
            keyId = 0,
            ACTIVATED_CLASS = "activated";

        // Memebers
        var Activator = {
            start: start,
            end: end
        };
        return Activator;

        // Functions
        function start (e) {
            // when an element is touched/clicked, it climbs up a few
            // parents to see if it is an .item or .button element
            $Dom.requestAnimationFrame(function () {
                var ele = e.target,
                    eleToActivate,
                    x;

                for (x = 0; x < 6; x++) {
                    if (!ele || ele.nodeType !== 1) {
                        break;
                    }
                    if (eleToActivate && ele.classList && ele.classList.contains("item")) {
                        eleToActivate = ele;
                        break;
                    }
                    if (ele.tagName === "A" || ele.tagName === "BUTTON" || ele.hasAttribute("ng-click")) {
                        eleToActivate = ele;
                        break;
                    }
                    if (ele.classList && ele.classList.contains("button")) {
                        eleToActivate = ele;
                        break;
                    }
                    // no sense climbing past these
                    if (ele.tagName === "CPS-CONTENT" ||
                        (ele.classList && ele.classList.contains("pane")) ||
                        ele.tagName === "BODY") {
                        break;
                    }
                    ele = ele.parentElement;
                }

                if (eleToActivate) {
                    // queue that this element should be set to active
                    queueElements[keyId] = eleToActivate;

                    // on the next frame, set the queued elements to active
                    $Dom.requestAnimationFrame(activateElements);

                    keyId = (keyId > 29 ? 0 : keyId + 1);
                }

            });
        }

        function end () {
            // clear out any active/queued elements after XX milliseconds
            $timeout(clear, 200);
        }

        // Private
        function clear () {
            // clear out any elements that are queued to be set to active
            queueElements = {};

            // in the next frame, remove the active class from all active elements
            $Dom.requestAnimationFrame(deactivateElements);
        }

        function activateElements () {
            var key;
            // activate all elements in the queue
            for (key in queueElements) {
                if (queueElements[key]) {
                    queueElements[key].classList.add(ACTIVATED_CLASS);
                    activeElements[key] = queueElements[key];
                }
            }
            queueElements = {};
        }

        function deactivateElements () {
            var key;
            // if (transition && transition.isActive) {
            //     $timeout(deactivateElements, 400);
            //     return;
            // }

            for (key in activeElements) {
                if (activeElements[key]) {
                    activeElements[key].classList.remove(ACTIVATED_CLASS);
                    delete activeElements[key];
                }
            }
        }
    }

    // Export
    module.exports = activatorService;
})();
