/* global require */
/* exported Hammer */

// core/ui/gestures/index.js
//
// Gestures module with hammer.js
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        Hammer = require("hammerjs"),
        gesturesDirective = require("./gesturesDirective"),
        gesturesService = require("./gesturesService"),
        activatorService = require("./activatorService"),
        tapService = require("./tapService"),
        gesturesRun = require("./gesturesRun");

    angular.module("cpsGestures", []);
    var cpsGestures = angular.module("cpsGestures");

    // Service
    cpsGestures.provider("$Gestures", gesturesService);
    cpsGestures.factory("$Activator", activatorService);
    cpsGestures.factory("$Tap", tapService);

    // Run
    cpsGestures.run(gesturesRun);

    // Directive
    /**
     * Inspired by AngularJS' implementation of "click dblclick mousedown..."
     * This ties in the Hammer 2 events to attributes like:
     * hm-tap="add_something()" hm-swipe="remove_something()"
     * and also has support for Hammer options with:
     * hm-tap-opts="{hold: false}"
     * or any other of the "hm-event" listed underneath.
     */
    var HGESTURES = {
        hmDoubleTap: "doubletap",
        hmDragStart: "panstart", // will bedeprecated soon, us Pan*
        hmDrag: "pan", // will bedeprecated soon, us Pan*
        hmDragUp: "panup", // will bedeprecated soon, us Pan*
        hmDragDown: "pandown", // will bedeprecated soon, us Pan*
        hmDragLeft: "panleft", // will bedeprecated soon, us Pan*
        hmDragRight: "panright", // will bedeprecated soon, us Pan*
        hmDragEnd: "panend", // will bedeprecated soon, us Pan*
        hmPanStart: "panstart",
        hmPan: "pan",
        hmPanUp: "panup",
        hmPanDown: "pandown",
        hmPanLeft: "panleft",
        hmPanRight: "panright",
        hmPanEnd: "panend",
        hmPanCancel: "pancancel",
        hmHold: "press",
        hmPinch: "pinch",
        hmPinchIn: "pinchin",
        hmPinchOut: "pinchout",
        hmPress: "press",
        hmRelease: "release",
        hmRotate: "rotate",
        hmSwipe: "swipe",
        hmSwipeUp: "swipeup",
        hmSwipeDown: "swipedown",
        hmSwipeLeft: "swipeleft",
        hmSwipeRight: "swiperight",
        hmTap: "tap",
        hmTouch: "touch",
        hmTransformStart: "transformstart",
        hmTransform: "transform",
        hmTransformEnd: "transformend"
    };

    angular.forEach(HGESTURES, function (eventName, directiveName) {
        cpsGestures.directive(directiveName, gesturesDirective.loadGestures(directiveName, eventName));
    });

    // Exports
    module.exports = {
        Hammer: Hammer
    };

})();
