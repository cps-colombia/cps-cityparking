/* global module */

// core/ui/slide/slideDirective.js
//
// directives function for ui slide module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    cpsSlide.$inject = [];
    function cpsSlide () {
        var slideDirective =  {
            restrict: "E",
            transclude: true,
            link: postLink,
            scope: {
                options: "=",
                slider: "=?"
            },
            controller: "SlideController",
            template: "<div class='swiper-container'>" +
                "<div class='swiper-wrapper' ng-transclude>" +
                "</div>" +
                "<div ng-hide='!showPager' class='swiper-pagination'></div>" +
                "</div>"
        };
        return slideDirective;

        // Functions
        function postLink (scope, element, attrs, ctrl) {
            scope.showPager = true;
            // Disable ngAnimate for slidebox and its children
            // $animate.enabled(false, $element);
        }
    }

    cpsSlidePage.$inject = [];
    function cpsSlidePage () {
        var slidePageDirective =  {
            restrict: "E",
            transclude: true,
            replace: true,
            require: "?^cpsSlide",
            template: "<div class='swiper-slide' ng-transclude></div>",
            link: postLink
        };
        return slidePageDirective;

        // Functions
        function postLink (scope, element, attrs, ctrl) {
            ctrl.rapidUpdate();

            scope.$on("$destroy", function () {
                ctrl.rapidUpdate();
            });
        }
    }

    // Exports
    module.exports = {
        slide: cpsSlide,
        slidePage: cpsSlidePage
    };

})();
