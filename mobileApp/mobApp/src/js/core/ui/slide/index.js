/* global require */

// core/ui/slide/index.js
//
// index function slide ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        slideDirective = require("./slideDirective"),
        slideController = require("./slideController"),
        slideService = require("./slideService");

    angular.module("cpsSlide", []);
    var cpsSlide = angular.module("cpsSlide");

    // Service
    cpsSlide.factory("$Slide", slideService);

    // Controller
    cpsSlide.controller("SlideController", slideController);

    // Directive
    cpsSlide.directive("cpsSlide", slideDirective.slide);
    cpsSlide.directive("cpsSlidePage", slideDirective.slidePage);

})();
