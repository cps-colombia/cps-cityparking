/* global module */

// core/ui/slide/slideControler.js
//
// Controller for ui slide module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    SlideController.$inject = [
        "$scope",
        "$element",
        "$timeout",
        "$compile",
        "$Slide"
    ];
    function SlideController ($scope, $element, $timeout, $compile, $Slide) {
        var slide = this,
            rapidUpdate = ld.setDebounce(function () {
                update();
            }, 50),
            options = $scope.options || {},
            newOptions = ld.extend({
                pagination: ".swiper-pagination",
                paginationClickable: true,
                lazyLoading: true,
                preloadImages: false
            }, options);

        // Members
        slide._options = newOptions;
        slide.update = update;
        slide.rapidUpdate = rapidUpdate;
        slide.getSlider = getSlider;

        // Run
        $timeout(function () {
            var slider = new $Slide.Swiper($element.children()[0], newOptions, $scope, $compile);

            slide.__slider = slider;
            $scope.slider = slide.__slider;

            $scope.$on("$destroy", function () {
                slider.destroy();
                slide.__slider = null;
            });
        });

        // Functions
        function update () {
            var slidesLength;

            $timeout(function () {
                if (!slide.__slider) {
                    return;
                }

                slide.__slider.update();
                if (slide._options.loop) {
                    slide.__slider.createLoop();
                }

                slidesLength = slide.__slider.slides.length;

                // Don't allow pager to show with > 10 slides
                if (slidesLength > 10) {
                    $scope.showPager = false;
                }

                // When slide index is greater than total then slide to last index
                if (slide.__slider.activeIndex > slidesLength - 1) {
                    slide.__slider.slideTo(slidesLength - 1);
                }
            });
        }

        function getSlider () {
            return slide.__slider;
        }
    }
    // Exports
    module.exports = SlideController;
})();
