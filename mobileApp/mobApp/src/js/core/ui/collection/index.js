/* global require */
// core/ui/collection/index.js
//
// index function for utils
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        collectionService = require("./collectionService"),
        collectionDirective = require("./collectionDirective");

    angular.module("cpsCollection", []);
    var cpsUtil = angular.module("cpsCollection");

    // Service
    cpsUtil.factory("$Collection", collectionService);

    // Directive
    cpsUtil.directive("cpsRepeat", collectionDirective);

})();
