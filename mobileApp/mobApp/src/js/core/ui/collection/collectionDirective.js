/* global module */

// utils/collectionDirective.js
//
// filters functions for cps App
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        $$ = angular.element;

    cpsRepeat.$inject = [
        "$parse",
        "$window",
        "$$rAF",
        "$rootScope",
        "$timeout",
        "$Dom",
        "$Collection"
    ];
    function cpsRepeat ($parse, $window, $$rAF, $rootScope, $timeout, $Dom, $Collection) {
        var cpsRepeatDirective = {
            restrict: "A",
            priority: 1000,
            transclude: "element",
            $$tlb: true,
            require: "^?ScrollController",
            link: postLink
        };

        return cpsRepeatDirective;

        // Functions
        function postLink (scope, element, attr, scrollCtrl, transclude) {
            scrollCtrl = element.scope().scrollCtrl;
            var scrollView = scrollCtrl.scrollView,
                node = element[0],
                containerNode = $$("<div class='cps-repeat-container'>")[0],
                DEFAULT_RENDER_BUFFER = 3;

            node.parentNode.replaceChild(containerNode, node);

            if (scrollView.options.scrollingX && scrollView.options.scrollingY) {
                throw new Error("cps-repeat expected a parent x or y scrollView, not " +
                                "an xy scrollView.");
            }

            var repeatExpr = attr.cpsRepeat,
                match = repeatExpr.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);
            if (!match) {
                throw new Error("cps-repeat expected expression in form of '_item_ in " +
                                "_collection_[ track by _id_]' but got '" + attr.cpsRepeat + "'.");
            }
            var keyExpr = match[1],
                listExpr = match[2],
                listGetter = $parse(listExpr),
                heightData = {},
                widthData = {},
                computedStyleDimensions = {},
                data = [],
                repeatManager,
                computedStyleNode,
                computedStyleScope,

                // attr.collectionBufferSize is deprecated
                renderBufferExpr = attr.itemRenderBuffer || attr.collectionBufferSize,
                renderBuffer = angular.isDefined(renderBufferExpr) ?
                    parseInt(renderBufferExpr) :
                    DEFAULT_RENDER_BUFFER,

                // attr.collectionItemHeight is deprecated
                heightExpr = attr.itemHeight || attr.collectionItemHeight,
                // attr.collectionItemWidth is deprecated
                widthExpr = attr.itemWidth || attr.collectionItemWidth,

                afterItemsContainer = initAfterItemsContainer(),

                changeValidator = makeChangeValidator();

            initDimensions();

            // Dimensions are refreshed on resize or data change.
            scrollCtrl.$element.on("scroll-resize", refreshDimensions);

            angular.element($window).on("resize", onResize);
            var unlistenToExposeAside = $rootScope.$on("$ionicExposeAside", $Dom.animationFrameThrottle(function () {
                scrollCtrl.scrollView.resize();
                onResize();
            }));
            $timeout(refreshDimensions, 0, false);

            function onResize () {
                if (changeValidator.resizeRequiresRefresh(scrollView.__clientWidth, scrollView.__clientHeight)) {
                    refreshDimensions();
                }
            }

            scope.$watchCollection(listGetter, function (newValue) {
                data = newValue || (newValue = []);
                if (!angular.isArray(newValue)) {
                    throw new Error("cps-repeat expected an array for '" + listExpr + "', " +
                                    "but got a " + typeof newValue);
                }
                // Wait for this digest to end before refreshing everything.
                scope.$$postDigest(function () {
                    getRepeatManager().setData(data);
                    if (changeValidator.dataChangeRequiresRefresh(data)) {
                        refreshDimensions();
                    }
                });
            });

            scope.$on("$destroy", function () {
                angular.element($window).off("resize", onResize);
                unlistenToExposeAside();
                if(scrollCtrl.$element) {
                    scrollCtrl.$element.off("scroll-resize", refreshDimensions);
                }

                if(computedStyleNode && computedStyleNode.parentNode) {
                    computedStyleNode.parentNode.removeChild(computedStyleNode);
                }
                if(computedStyleScope) {
                    computedStyleScope.$destroy();
                }
                computedStyleScope = computedStyleNode = null;

                if(repeatManager) {
                    repeatManager.destroy();
                }
                repeatManager = null;
            });

            function makeChangeValidator () {
                var self;
                return (self = {
                    dataLength: 0,
                    width: 0,
                    height: 0,
                    // A resize triggers a refresh only if we have data, the scrollView has size,
                    // and the size has changed.
                    resizeRequiresRefresh: function (newWidth, newHeight) {
                        var requiresRefresh = self.dataLength && newWidth && newHeight &&
                                (newWidth !== self.width || newHeight !== self.height);

                        self.width = newWidth;
                        self.height = newHeight;

                        return !!requiresRefresh;
                    },
                    // A change in data only triggers a refresh if the data has length, or if the data's
                    // length is less than before.
                    dataChangeRequiresRefresh: function (newData) {
                        var requiresRefresh = newData.length > 0 || newData.length < self.dataLength;

                        self.dataLength = newData.length;

                        return !!requiresRefresh;
                    }
                });
            }

            function getRepeatManager () {
                var Collection = $Collection;
                return repeatManager || (repeatManager = new Collection({
                    afterItemsNode: afterItemsContainer[0],
                    containerNode: containerNode,
                    heightData: heightData,
                    widthData: widthData,
                    forceRefreshImages: !!(angular.isDefined(attr.forceRefreshImages) &&
                                           attr.forceRefreshImages !== "false"),
                    keyExpression: keyExpr,
                    renderBuffer: renderBuffer,
                    scope: scope,
                    scrollView: scrollCtrl.scrollView,
                    transclude: transclude
                }));
            }

            function initAfterItemsContainer () {
                var container = angular.element(
                    scrollView.__content.querySelector(".cps-repeat-after-container")
                );
                // Put everything in the view after the repeater into a container.
                if (!container.length) {
                    var elementIsAfterRepeater = false;
                    var afterNodes = [].filter.call(scrollView.__content.childNodes, function (node) {
                        if ($Dom.contains(node, containerNode)) {
                            elementIsAfterRepeater = true;
                            return false;
                        }
                        return elementIsAfterRepeater;
                    });
                    container = angular.element("<span class=\"cps-repeat-after-container\">");
                    if (scrollView.options.scrollingX) {
                        container.addClass("horizontal");
                    }
                    container.append(afterNodes);
                    scrollView.__content.appendChild(container[0]);
                }
                return container;
            }

            function initDimensions () {
                // Height and width have four 'modes':
                // 1) Computed Mode
                //  - Nothing is supplied, so we getComputedStyle() on one element in the list and use
                //    that width and height value for the width and height of every item. This is re-computed
                //    every resize.
                // 2) Constant Mode, Static Integer
                //  - The user provides a constant number for width or height, in pixels. We parse it,
                //    store it on the `value` field, and it never changes
                // 3) Constant Mode, Percent
                //  - The user provides a percent string for width or height. The getter for percent is
                //    stored on the `getValue()` field, and is re-evaluated once every resize. The result
                //    is stored on the `value` field.
                // 4) Dynamic Mode
                //  - The user provides a dynamic expression for the width or height.  This is re-evaluated
                //    for every item, stored on the `.getValue()` field.
                if (heightExpr) {
                    parseDimensionAttr(heightExpr, heightData);
                } else {
                    heightData.computed = true;
                }
                if (widthExpr) {
                    parseDimensionAttr(widthExpr, widthData);
                } else {
                    widthData.computed = true;
                }
            }

            function refreshDimensions () {
                var hasData = data.length > 0;

                if (hasData && (heightData.computed || widthData.computed)) {
                    computeStyleDimensions();
                }

                if (hasData && heightData.computed) {
                    heightData.value = computedStyleDimensions.height;
                    if (!heightData.value) {
                        throw new Error("cps-repeat tried to compute the height of repeated elements \"" + repeatExpr +
                                        "\", but was unable to. Please provide the \"item-height\" attribute. ");
                    }
                } else if (!heightData.dynamic && heightData.getValue) {
                    // If it's a constant with a getter (eg percent), we just refresh .value after resize
                    heightData.value = heightData.getValue();
                }

                if (hasData && widthData.computed) {
                    widthData.value = computedStyleDimensions.width;
                    if (!widthData.value) {
                        throw new Error("cps-repeat tried to compute the width of repeated elements \"" + repeatExpr +
                                        "\", but was unable to. Please provide the \"item-width\" attribute. ");
                    }
                } else if (!widthData.dynamic && widthData.getValue) {
                    // If it's a constant with a getter (eg percent), we just refresh .value after resize
                    widthData.value = widthData.getValue();
                }
                // Dynamic dimensions aren't updated on resize. Since they're already dynamic anyway,
                // .getValue() will be used.

                getRepeatManager().refreshLayout();
            }

            function parseDimensionAttr (attrValue, dimensionData) {
                if (!attrValue) {
                    return;
                }

                var parsedValue;
                // Try to just parse the plain attr value
                try {
                    parsedValue = $parse(attrValue);
                } catch (e) {
                    // If the parse fails and the value has `px` or `%` in it, surround the attr in
                    // quotes, to attempt to let the user provide a simple `attr="100%"` or `attr="100px"`
                    if (attrValue.trim().match(/\d+(px|%)$/)) {
                        attrValue = "\"" + attrValue + "\"";
                    }
                    parsedValue = $parse(attrValue);
                }

                var constantAttrValue = attrValue.replace(/(\'|\"|px|%)/g, "").trim();
                var isConstant = constantAttrValue.length && !/([a-zA-Z]|\$|:|\?)/.test(constantAttrValue);
                dimensionData.attrValue = attrValue;

                // If it's a constant, it's either a percent or just a constant pixel number.
                if (isConstant) {
                    // For percents, store the percent getter on .getValue()
                    if (attrValue.indexOf("%") > -1) {
                        var decimalValue = parseFloat(parsedValue()) / 100;
                        dimensionData.getValue = dimensionData === heightData ?
                            function () { return Math.floor(decimalValue * scrollView.__clientHeight); } :
                        function () { return Math.floor(decimalValue * scrollView.__clientWidth); };
                    } else {
                        // For static constants, just store the static constant.
                        dimensionData.value = parseInt(parsedValue());
                    }

                } else {
                    dimensionData.dynamic = true;
                    dimensionData.getValue = dimensionData === heightData ?
                        function heightGetter (scope, locals) {
                            var result = parsedValue(scope, locals);
                            if (result.charAt && result.charAt(result.length - 1) === "%") {
                                return Math.floor(parseFloat(result) / 100 * scrollView.__clientHeight);
                            }
                            return parseInt(result);
                        } :
                    function widthGetter (scope, locals) {
                        var result = parsedValue(scope, locals);
                        if (result.charAt && result.charAt(result.length - 1) === "%") {
                            return Math.floor(parseFloat(result) / 100 * scrollView.__clientWidth);
                        }
                        return parseInt(result);
                    };
                }
            }

            function computeStyleDimensions () {
                if (!computedStyleNode) {
                    transclude(computedStyleScope = scope.$new(), function (clone) {
                        clone[0].removeAttribute("cps-repeat"); // remove absolute position styling
                        computedStyleNode = clone[0];
                    });
                }

                computedStyleScope[keyExpr] = (listGetter(scope) || [])[0];
                if (!$rootScope.$$phase) {
                    computedStyleScope.$digest();
                }
                containerNode.appendChild(computedStyleNode);

                var style = $window.getComputedStyle(computedStyleNode);
                computedStyleDimensions.width = parseInt(style.width);
                computedStyleDimensions.height = parseInt(style.height);

                containerNode.removeChild(computedStyleNode);
            }

        }

    }
    // Exports
    module.exports = cpsRepeat;

})();
