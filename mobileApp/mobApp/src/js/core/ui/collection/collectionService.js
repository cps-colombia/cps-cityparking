/* global module */

// utils/collectionService.js
//
// Collection Manager factory for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    collectionService.$inject = ["$rootScope", "$window", "$$rAF", "$Dom", "$View", "$Utils"];
    function collectionService ($rootScope, $window, $$rAF, $Dom, $View, $Utils) {
        var EMPTY_DIMENSION = { primaryPos: 0, secondaryPos: 0, primarySize: 0, secondarySize: 0, rowPrimarySize: 0 },
            ONE_PX_TRANSPARENT_IMG_SRC = "data:image/gif;" +
            "base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
            WIDTH_HEIGHT_REGEX = /height:.*?px;\s*width:.*?px/;

        // Members
        return RepeatController;

        // Functions
        function RepeatController (options) {
            var afterItemsNode = options.afterItemsNode,
                containerNode = options.containerNode,
                forceRefreshImages = options.forceRefreshImages,
                heightData = options.heightData,
                widthData = options.widthData,
                keyExpression = options.keyExpression,
                renderBuffer = options.renderBuffer,
                scope = options.scope,
                scrollView = options.scrollView,
                transclude = options.transclude,
                data = [],
                getterLocals = {},
                heightFn = heightData.getValue || getHeightData,
                widthFn = widthData.getValue || getWidthData,

                isVertical = !!scrollView.options.scrollingY,
                isGridView = isVertical ? // We say it's a grid view if we're either dynamic or not 100% width
                    (widthData.dynamic || widthData.value !== scrollView.__clientWidth) :
                    (heightData.dynamic || heightData.value !== scrollView.__clientHeight),
                isStaticView = !heightData.dynamic && !widthData.dynamic,

                contentSizeStr = isVertical ? "getContentHeight" : "getContentWidth",
                originalGetContentSize = scrollView.options[contentSizeStr],

                repeaterBeforeSize = 0,
                repeaterAfterSize = 0,

                renderStartIndex = -1,
                renderEndIndex = -1,
                renderAfterBoundary = -1,
                renderBeforeBoundary = -1,

                itemsPool = [],
                itemsLeaving = [],
                itemsEntering = [],
                itemsShownMap = {},
                nextItemId = 0,

                isLayoutReady = false,
                isDataReady = false,

                PRIMARY = "PRIMARY",
                SECONDARY = "SECONDARY",
                TRANSLATE_TEMPLATE_STR = isVertical ?
                    "translate3d(SECONDARYpx,PRIMARYpx,0)" :
                    "translate3d(PRIMARYpx,SECONDARYpx,0)",
                WIDTH_HEIGHT_TEMPLATE_STR = isVertical ?
                    "height: PRIMARYpx; width: SECONDARYpx;" :
                    "height: SECONDARYpx; width: PRIMARYpx;",

                estimatedHeight,
                estimatedWidth,
                view;

            // view is a mix of list/grid methods + static/dynamic methods.
            // See bottom for implementations. Available methods:
            //
            // getEstimatedPrimaryPos(i), getEstimatedSecondaryPos(i), getEstimatedIndex(scrollTop),
            // calculateDimensions(toIndex), getDimensions(index),
            // updateRenderRange(scrollTop, scrollValueEnd), onRefreshLayout(), onRefreshData()
            view = isVertical ? new VerticalViewType() : new HorizontalViewType();
            (isGridView ? GridViewType : ListViewType).call(view);
            (isStaticView ? StaticViewType : DynamicViewType).call(view);

            scrollView.options[contentSizeStr] = angular.bind(view, view.getContentSize);

            scrollView.__$callback = scrollView.__callback;
            scrollView.__callback = scrollViewCallback;

            this.refreshLayout = function () {
                if (data.length) {
                    estimatedHeight = heightGetter(0, data[0]);
                    estimatedWidth = widthGetter(0, data[0]);
                } else {
                    // If we don't have any data in our array, just guess.
                    estimatedHeight = 100;
                    estimatedWidth = 100;
                }

                // Get the size of every element AFTER the repeater. We have to get the margin before and
                // after the first/last element to fix a browser bug with $window.getComputedStyle() not counting
                // the first/last child's margins into height.
                var style = $window.getComputedStyle(afterItemsNode) || {};
                var firstStyle = afterItemsNode.firstElementChild &&
                        $window.getComputedStyle(afterItemsNode.firstElementChild) || {};
                var lastStyle = afterItemsNode.lastElementChild &&
                        $window.getComputedStyle(afterItemsNode.lastElementChild) || {};
                repeaterAfterSize = (parseInt(style[isVertical ? "height" : "width"]) || 0) +
                    (firstStyle && parseInt(firstStyle[isVertical ? "marginTop" : "marginLeft"]) || 0) +
                    (lastStyle && parseInt(lastStyle[isVertical ? "marginBottom" : "marginRight"]) || 0);

                // Get the offsetTop of the repeater.
                repeaterBeforeSize = 0;
                var current = containerNode;
                do {
                    repeaterBeforeSize += current[isVertical ? "offsetTop" : "offsetLeft"];
                } while ($Dom.contains(scrollView.__content, current = current.offsetParent));

                var containerPrevNode = containerNode.previousElementSibling;
                var beforeStyle = containerPrevNode ? $window.getComputedStyle(containerPrevNode) : {};
                var beforeMargin = parseInt(beforeStyle[isVertical ? "marginBottom" : "marginRight"] || 0);

                // Because we position the collection container with position: relative, it doesn't take
                // into account where to position itself relative to the previous element's marginBottom.
                // To compensate, we translate the container up by the previous element's margin.
                containerNode.style[$View.getCss().TRANSFORM] = TRANSLATE_TEMPLATE_STR
                    .replace(PRIMARY, -beforeMargin)
                    .replace(SECONDARY, 0);
                repeaterBeforeSize -= beforeMargin;

                if (!scrollView.__clientHeight || !scrollView.__clientWidth) {
                    scrollView.__clientWidth = scrollView.__container.clientWidth;
                    scrollView.__clientHeight = scrollView.__container.clientHeight;
                }

                (view.onRefreshLayout || ld.noop)();
                view.refreshDirection();
                scrollViewSetDimensions();

                // Create the pool of items for reuse, setting the size to (estimatedItemsOnScreen) * 2,
                // plus the size of the renderBuffer.
                if (!isLayoutReady) {
                    var poolSize = Math.max(20, renderBuffer * 3);
                    for (var i = 0; i < poolSize; i++) {
                        itemsPool.push(new RepeatItem());
                    }
                }

                isLayoutReady = true;
                if (isLayoutReady && isDataReady) {
                    // If the resize or latest data change caused the scrollValue to
                    // now be out of bounds, resize the scrollView.
                    if (scrollView.__scrollLeft > scrollView.__maxScrollLeft ||
                        scrollView.__scrollTop > scrollView.__maxScrollTop) {
                        scrollView.resize();
                    }
                    forceRerender(true);
                }
            };

            this.setData = function (newData) {
                data = newData;
                (view.onRefreshData || ld.noop)();
                isDataReady = true;
            };

            this.destroy = function () {
                render.destroyed = true;

                itemsPool.forEach(function (item) {
                    item.scope.$destroy();
                    item.scope = item.element = item.node = item.images = null;
                });
                itemsPool.length = itemsEntering.length = itemsLeaving.length = 0;
                itemsShownMap = {};

                // Restore the scrollView's normal behavior and resize it to normal size.
                scrollView.options[contentSizeStr] = originalGetContentSize;
                scrollView.__callback = scrollView.__$callback;
                scrollView.resize();

                (view.onDestroy || ld.noop)();
            };

            // Private
            function heightGetter (index, value) {
                getterLocals[keyExpression] = value;
                getterLocals.$index = index;
                return heightFn(scope, getterLocals);
            }

            function widthGetter (index, value) {
                getterLocals[keyExpression] = value;
                getterLocals.$index = index;
                return widthFn(scope, getterLocals);
            }

            function getHeightData () {
                return heightData.value;
            }

            function getWidthData () {
                return widthData.value;
            }

            function scrollViewSetDimensions () {
                if(isVertical) {
                    scrollView.setDimensions(null, null, null, view.getContentSize(), true);
                } else {
                    scrollView.setDimensions(null, null, view.getContentSize(), null, true);
                }
            }

            function scrollViewCallback (transformLeft, transformTop, zoom, wasResize) {
                var scrollValue = view.getScrollValue();
                if (renderStartIndex === -1 ||
                    scrollValue + view.scrollPrimarySize > renderAfterBoundary ||
                    scrollValue < renderBeforeBoundary) {
                    render();
                }
                scrollView.__$callback(transformLeft, transformTop, zoom, wasResize);
            }

            function forceRerender () {
                return render(true);
            }

            function render (forceRerender) {
                if (render.destroyed) {
                    return;
                }
                var scrollValue = view.getScrollValue(),
                    scrollValueEnd = scrollValue + view.scrollPrimarySize,
                    scope,
                    item,
                    dim,
                    ii,
                    i;

                view.updateRenderRange(scrollValue, scrollValueEnd);

                renderStartIndex = Math.max(0, renderStartIndex - renderBuffer);
                renderEndIndex = Math.min(data.length - 1, renderEndIndex + renderBuffer);

                for (i in itemsShownMap) {
                    if (i < renderStartIndex || i > renderEndIndex) {
                        item = itemsShownMap[i];
                        delete itemsShownMap[i];
                        itemsLeaving.push(item);
                        item.isShown = false;
                    }
                }

                // Render indicies that aren't shown yet
                //
                // NOTE(ajoslin): this may sound crazy, but calling any other functions during this render
                // loop will often push the render time over the edge from less than one frame to over
                // one frame, causing visible jank.
                // DON'T call any other functions inside this loop unless it's vital.
                for (i = renderStartIndex; i <= renderEndIndex; i++) {
                    // We only go forward with render if the index is in data, the item isn't already shown,
                    // or forceRerender is on.
                    if (i >= data.length || (itemsShownMap[i] && !forceRerender)) {
                        continue;
                    }

                    item = itemsShownMap[i] || (itemsShownMap[i] = itemsLeaving.length ? itemsLeaving.pop() :
                                                itemsPool.length ? itemsPool.shift() :
                                                new RepeatItem());
                    itemsEntering.push(item);
                    item.isShown = true;

                    scope = item.scope;
                    scope.$index = i;
                    scope[keyExpression] = data[i];
                    scope.$first = (i === 0);
                    scope.$last = (i === (data.length - 1));
                    scope.$middle = !(scope.$first || scope.$last);
                    scope.$odd = !(scope.$even = (i & 1) === 0); // jshint ignore:line

                    if (scope.$$disconnected) {
                        $Utils.reconnectScope(item.scope);
                    }

                    dim = view.getDimensions(i);
                    if (item.secondaryPos !== dim.secondaryPos || item.primaryPos !== dim.primaryPos) {
                        item.node.style[$View.getCss().TRANSFORM] = TRANSLATE_TEMPLATE_STR
                            .replace(PRIMARY, (item.primaryPos = dim.primaryPos))
                            .replace(SECONDARY, (item.secondaryPos = dim.secondaryPos));
                    }
                    if (item.secondarySize !== dim.secondarySize || item.primarySize !== dim.primarySize) {
                        item.node.style.cssText = item.node.style.cssText
                            .replace(WIDTH_HEIGHT_REGEX, WIDTH_HEIGHT_TEMPLATE_STR
                                     // TODO fix item.primarySize + 1 hack
                                     .replace(PRIMARY, (item.primarySize = dim.primarySize) + 1)
                                     .replace(SECONDARY, (item.secondarySize = dim.secondarySize))
                                    );
                    }

                }

                // If we reach the end of the list, render the afterItemsNode - this contains all the
                // elements the developer placed after the collection-repeat
                if (renderEndIndex === data.length - 1) {
                    dim = view.getDimensions(data.length - 1) || EMPTY_DIMENSION;
                    afterItemsNode.style[$View.getCss().TRANSFORM] = TRANSLATE_TEMPLATE_STR
                        .replace(PRIMARY, dim.primaryPos + dim.primarySize)
                        .replace(SECONDARY, 0);
                }

                while (itemsLeaving.length) {
                    item = itemsLeaving.pop();
                    item.scope.$broadcast("$collectionRepeatLeave");
                    $Utils.disconnectScope(item.scope);
                    itemsPool.push(item);
                    item.node.style[$View.getCss().TRANSFORM] = "translate3d(-9999px,-9999px,0)";
                    item.primaryPos = item.secondaryPos = null;
                }

                if (forceRefreshImages) {
                    for (i = 0, ii = itemsEntering.length; i < ii && (item = itemsEntering[i]); i++) {
                        if (!item.images) {
                            continue;
                        }
                        for (var j = 0, jj = item.images.length, img; j < jj && (img = item.images[j]); j++) {
                            var src = img.src;
                            img.src = ONE_PX_TRANSPARENT_IMG_SRC;
                            img.src = src;
                        }
                    }
                }
                if (forceRerender) {
                    var rootScopePhase = $rootScope.$$phase;
                    while (itemsEntering.length) {
                        item = itemsEntering.pop();
                        if (!rootScopePhase) {
                            item.scope.$digest();
                        }
                    }
                } else {
                    digestEnteringItems();
                }
            }

            function digestEnteringItems () {
                var item;
                if (digestEnteringItems.running) {
                    return;
                }
                digestEnteringItems.running = true;

                $$rAF(function process () {
                    var rootScopePhase = $rootScope.$$phase;
                    while (itemsEntering.length) {
                        item = itemsEntering.pop();
                        if (item.isShown) {
                            if (!rootScopePhase) {
                                item.scope.$digest();
                            }
                        }
                    }
                    digestEnteringItems.running = false;
                });
            }

            function RepeatItem () {
                var self = this;
                this.scope = scope.$new();
                this.id = "item" + (nextItemId++);
                transclude(this.scope, function (clone) {
                    self.element = clone;
                    self.element.data("$$collectionRepeatItem", self);
                    // TODO destroy
                    self.node = clone[0];
                    // Batch style setting to lower repaints
                    self.node.style[$View.getCss().TRANSFORM] = "translate3d(-9999px,-9999px,0)";
                    self.node.style.cssText += " height: 0px; width: 0px;";
                    $Utils.disconnectScope(self.scope);
                    containerNode.appendChild(self.node);
                    self.images = clone[0].getElementsByTagName("img");
                });
            }

            function VerticalViewType () {
                this.getItemPrimarySize = heightGetter;
                this.getItemSecondarySize = widthGetter;

                this.getScrollValue = function () {
                    return Math.max(0, Math.min(scrollView.__scrollTop - repeaterBeforeSize,
                                                scrollView.__maxScrollTop - repeaterBeforeSize - repeaterAfterSize));
                };

                this.refreshDirection = function () {
                    this.scrollPrimarySize = scrollView.__clientHeight;
                    this.scrollSecondarySize = scrollView.__clientWidth;

                    this.estimatedPrimarySize = estimatedHeight;
                    this.estimatedSecondarySize = estimatedWidth;
                    this.estimatedItemsAcross = isGridView &&
                        Math.floor(scrollView.__clientWidth / estimatedWidth) ||
                        1;
                };
            }
            function HorizontalViewType () {
                this.getItemPrimarySize = widthGetter;
                this.getItemSecondarySize = heightGetter;

                this.getScrollValue = function () {
                    return Math.max(0, Math.min(scrollView.__scrollLeft - repeaterBeforeSize,
                                                scrollView.__maxScrollLeft - repeaterBeforeSize - repeaterAfterSize));
                };

                this.refreshDirection = function () {
                    this.scrollPrimarySize = scrollView.__clientWidth;
                    this.scrollSecondarySize = scrollView.__clientHeight;

                    this.estimatedPrimarySize = estimatedWidth;
                    this.estimatedSecondarySize = estimatedHeight;
                    this.estimatedItemsAcross = isGridView &&
                        Math.floor(scrollView.__clientHeight / estimatedHeight) ||
                        1;
                };
            }

            function GridViewType () {
                this.getEstimatedSecondaryPos = function (index) {
                    return (index % this.estimatedItemsAcross) * this.estimatedSecondarySize;
                };
                this.getEstimatedPrimaryPos = function (index) {
                    return Math.floor(index / this.estimatedItemsAcross) * this.estimatedPrimarySize;
                };
                this.getEstimatedIndex = function (scrollValue) {
                    return Math.floor(scrollValue / this.estimatedPrimarySize) *
                        this.estimatedItemsAcross;
                };
            }

            function ListViewType () {
                this.getEstimatedSecondaryPos = function () {
                    return 0;
                };
                this.getEstimatedPrimaryPos = function (index) {
                    return index * this.estimatedPrimarySize;
                };
                this.getEstimatedIndex = function (scrollValue) {
                    return Math.floor((scrollValue) / this.estimatedPrimarySize);
                };
            }

            function StaticViewType () {
                this.getContentSize = function () {
                    return this.getEstimatedPrimaryPos(data.length - 1) + this.estimatedPrimarySize +
                        repeaterBeforeSize + repeaterAfterSize;
                };
                // static view always returns the same object for getDimensions, to avoid memory allocation
                // while scrolling. This could be dangerous if this was a public function, but it's not.
                // Only we use it.
                var dim = {};
                this.getDimensions = function (index) {
                    dim.primaryPos = this.getEstimatedPrimaryPos(index);
                    dim.secondaryPos = this.getEstimatedSecondaryPos(index);
                    dim.primarySize = this.estimatedPrimarySize;
                    dim.secondarySize = this.estimatedSecondarySize;
                    return dim;
                };
                this.updateRenderRange = function (scrollValue, scrollValueEnd) {
                    renderStartIndex = Math.max(0, this.getEstimatedIndex(scrollValue));

                    // Make sure the renderEndIndex takes into account all the items on the row
                    renderEndIndex = Math.min(data.length - 1,
                                              this.getEstimatedIndex(scrollValueEnd) + this.estimatedItemsAcross - 1);

                    renderBeforeBoundary = Math.max(0,
                                                    this.getEstimatedPrimaryPos(renderStartIndex));
                    renderAfterBoundary = this.getEstimatedPrimaryPos(renderEndIndex) +
                        this.estimatedPrimarySize;
                };
            }

            function DynamicViewType () {
                var self = this,
                    debouncedScrollViewSetDimensions = ld.setDebounce(scrollViewSetDimensions, 25, true),
                    calculateDimensions = isGridView ? calculateDimensionsGrid : calculateDimensionsList,
                    dimensionsIndex,
                    dimensions = [];


                // Get the dimensions at index. {width, height, left, top}.
                // We start with no dimensions calculated, then any time dimensions are asked for at an
                // index we calculate dimensions up to there.
                function calculateDimensionsList (toIndex) {
                    var i, prevDimension, dim;
                    for (i = Math.max(0, dimensionsIndex); i <= toIndex && (dim = dimensions[i]); i++) {
                        prevDimension = dimensions[i - 1] || EMPTY_DIMENSION;
                        dim.primarySize = self.getItemPrimarySize(i, data[i]);
                        dim.secondarySize = self.scrollSecondarySize;
                        dim.primaryPos = prevDimension.primaryPos + prevDimension.primarySize;
                        dim.secondaryPos = 0;
                    }
                }
                function calculateDimensionsGrid (toIndex) {
                    var i, prevDimension, dim;
                    for (i = Math.max(dimensionsIndex, 0); i <= toIndex && (dim = dimensions[i]); i++) {
                        prevDimension = dimensions[i - 1] || EMPTY_DIMENSION;
                        dim.secondarySize = Math.min(
                            self.getItemSecondarySize(i, data[i]),
                            self.scrollSecondarySize
                        );
                        dim.secondaryPos = prevDimension.secondaryPos + prevDimension.secondarySize;

                        if (i === 0 || dim.secondaryPos + dim.secondarySize > self.scrollSecondarySize) {
                            dim.secondaryPos = 0;
                            dim.primarySize = self.getItemPrimarySize(i, data[i]);
                            dim.primaryPos = prevDimension.primaryPos + prevDimension.rowPrimarySize;

                            dim.rowStartIndex = i;
                            dim.rowPrimarySize = dim.primarySize;
                        } else {
                            dim.primarySize = self.getItemPrimarySize(i, data[i]);
                            dim.primaryPos = prevDimension.primaryPos;
                            dim.rowStartIndex = prevDimension.rowStartIndex;

                            dimensions[dim.rowStartIndex].rowPrimarySize = dim.rowPrimarySize = Math.max(
                                dimensions[dim.rowStartIndex].rowPrimarySize,
                                dim.primarySize
                            );
                            dim.rowPrimarySize = Math.max(dim.primarySize, dim.rowPrimarySize);
                        }
                    }
                }

                this.getContentSize = function () {
                    var dim = dimensions[dimensionsIndex] || EMPTY_DIMENSION;
                    return ((dim.primaryPos + dim.primarySize) || 0) +
                        this.getEstimatedPrimaryPos(data.length - dimensionsIndex - 1) +
                        repeaterBeforeSize + repeaterAfterSize;
                };
                this.onDestroy = function () {
                    dimensions.length = 0;
                };

                this.onRefreshData = function () {
                    var i;
                    var ii;
                    // Make sure dimensions has as many items as data.length.
                    // This is to be sure we don't have to allocate objects while scrolling.
                    for (i = dimensions.length, ii = data.length; i < ii; i++) {
                        dimensions.push({});
                    }
                    dimensionsIndex = -1;
                };
                this.onRefreshLayout = function () {
                    dimensionsIndex = -1;
                };
                this.getDimensions = function (index) {
                    index = Math.min(index, data.length - 1);

                    if (dimensionsIndex < index) {
                        // Once we start asking for dimensions near the end of the list, go ahead and calculate
                        // everything. This is to make sure when the user gets to the end of the list, the
                        // scroll height of the list is 100% accurate (not estimated anymore).
                        if (index > data.length * 0.9) {
                            calculateDimensions(data.length - 1);
                            dimensionsIndex = data.length - 1;
                            scrollViewSetDimensions();
                        } else {
                            calculateDimensions(index);
                            dimensionsIndex = index;
                            debouncedScrollViewSetDimensions();
                        }

                    }
                    return dimensions[index];
                };

                var oldRenderStartIndex = -1;
                var oldScrollValue = -1;
                this.updateRenderRange = function (scrollValue, scrollValueEnd) {
                    var i;
                    var len;
                    var dim;

                    // Calculate more dimensions than we estimate we'll need, to be sure.
                    this.getDimensions(this.getEstimatedIndex(scrollValueEnd) * 2);

                    // -- Calculate renderStartIndex
                    // base case: start at 0
                    if (oldRenderStartIndex === -1 || scrollValue === 0) {
                        i = 0;
                        // scrolling down
                    } else if (scrollValue >= oldScrollValue) {
                        for (i = oldRenderStartIndex, len = data.length; i < len; i++) {
                            if ((dim = this.getDimensions(i)) && dim.primaryPos + dim.rowPrimarySize >= scrollValue) {
                                break;
                            }
                        }
                        // scrolling up
                    } else {
                        for (i = oldRenderStartIndex; i >= 0; i--) {
                            if ((dim = this.getDimensions(i)) && dim.primaryPos <= scrollValue) {
                                // when grid view, make sure the render starts at the beginning of a row.
                                i = isGridView ? dim.rowStartIndex : i;
                                break;
                            }
                        }
                    }

                    renderStartIndex = Math.min(Math.max(0, i), data.length - 1);
                    renderBeforeBoundary = renderStartIndex !== -1 ?
                        this.getDimensions(renderStartIndex).primaryPos : -1;

                    // -- Calculate renderEndIndex
                    var lastRowDim;
                    for (i = renderStartIndex + 1, len = data.length; i < len; i++) {
                        if ((dim = this.getDimensions(i)) && dim.primaryPos + dim.rowPrimarySize > scrollValueEnd) {

                            // Go all the way to the end of the row if we're in a grid
                            if (isGridView) {
                                lastRowDim = dim;
                                while (i < len - 1 &&
                                       (dim = this.getDimensions(i + 1)).primaryPos === lastRowDim.primaryPos) {
                                    i++;
                                }
                            }
                            break;
                        }
                    }

                    renderEndIndex = Math.min(i, data.length - 1);
                    renderAfterBoundary = renderEndIndex !== -1 ?
                        ((dim = this.getDimensions(renderEndIndex)).primaryPos +
                         (dim.rowPrimarySize || dim.primarySize)) : -1;

                    oldScrollValue = scrollValue;
                    oldRenderStartIndex = renderStartIndex;
                };
            }
        }

    }
    // Exports
    module.exports = collectionService;

})();
