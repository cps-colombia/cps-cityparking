/* global module */

// core/ui/bar/barDirective.js
//
// directives function for ui bar module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    function headerFooter (isHeader) {
        headerFooterBar.$inject = [
            "$document",
            "$timeout",
            "$Dom"
        ];
        function headerFooterBar ($document, $timeout, $Dom) {
            var barDirective = {
                restrict: "E",
                controller: "BarController",
                compile: compile
            };

            return barDirective;

            // Functions
            function compile (tElement) {
                tElement.addClass(isHeader ? "bar bar-header" : "bar bar-footer");
                // top style tabs? if so, remove bottom border for seamless display
                $timeout(function () {
                    if (isHeader && $document[0].getElementsByClassName("tabs-top").length) {
                        tElement.addClass("has-tabs-top");
                    }
                });

                return { pre: prelink };
            }

            function prelink (scope, element, attr, ctrl) {
                var isShown,
                    isSubfooter,
                    isSubheader;

                if (isHeader) {
                    scope.$watch(function () {
                        return element[0].className;
                    }, function (value) {
                        isShown = value.indexOf("ng-hide") === -1;
                        isSubheader = value.indexOf("bar-subheader") !== -1;

                        scope.$hasHeader = isShown && !isSubheader;
                        scope.$hasSubheader = isShown && isSubheader;
                        scope.$emit("cps:subheader", scope.$hasSubheader);
                    });
                    scope.$on("$destroy", function () {
                        delete scope.$hasHeader;
                        delete scope.$hasSubheader;
                    });
                    ctrl.align();
                    scope.$on("cps:header:align", function () {
                        $Dom.requestAnimationFrame(function () {
                            ctrl.align();
                        });
                    });

                } else {
                    scope.$watch(function () {
                        return element[0].className;
                    }, function (value) {
                        isShown = value.indexOf("ng-hide") === -1;
                        isSubfooter = value.indexOf("bar-subfooter") !== -1;

                        scope.$hasFooter = isShown && !isSubfooter;
                        scope.$hasSubfooter = isShown && isSubfooter;
                    });
                    scope.$on("$destroy", function () {
                        delete scope.$hasFooter;
                        delete scope.$hasSubfooter;
                    });
                    scope.$watch("$hasTabs", function (val) {
                        element.toggleClass("has-tabs", !!val);
                    });
                    ctrl.align();
                    scope.$on("cps:footer:align", function () {
                        $Dom.requestAnimationFrame(function () {
                            ctrl.align();
                        });
                    });
                }
            }
        }

        return headerFooterBar;
    }

    // Exports
    module.exports = {
        headerFooter: headerFooter
    };

})();
