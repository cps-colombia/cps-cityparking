/* global require */

// core/ui/bar/index.js
//
// index function bar ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        barDirective = require("./barDirective"),
        barController = require("./barController");

    angular.module("cpsBar", []);
    var cpsBar = angular.module("cpsBar");


    // Directive
    cpsBar.directive("cpsHeader", barDirective.headerFooter(true));
    cpsBar.directive("cpsFooter", barDirective.headerFooter(false));

    // Controller
    cpsBar.controller("BarController", barController);

})();
