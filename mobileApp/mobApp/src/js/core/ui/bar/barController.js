/* global module */

// core/ui/bar/barController.js
//
// Controller for footer and header bars
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    BarController.$inject = [
        "$q",
        "$scope",
        "$element",
        "$attrs",
        "$Dom",
        "$UiConfig"
    ];
    function BarController ($q, $scope, $element, $attrs, $Dom, $UiConfig) {
        var bar = this,
            titleText = "",
            previousTitleText = "",
            titleLeft = 0,
            titleRight = 0,
            titleCss = "",
            isBackEnabled = false,
            isBackShown = true,
            isNavBackShown = true,
            isBackElementShown = false,
            _titleTextWidth = 0,
            eleCache = {},

            TITLE = "title",
            BACK_TEXT = "back-text",
            BACK_BUTTON = "back-button",
            NOTIFY_TRAY = "notify-tray",
            NOTIFY_BUTTON = "notify-button",
            DEFAULT_TITLE = "default-title",
            PREVIOUS_TITLE = "previous-title",
            HIDE = "hide";


        // Members
        bar.beforeEnter = beforeEnter;
        bar.title = title;
        bar.enableBack = enableBack;
        bar.showBack = showBack;
        bar.showNavBack = showNavBack;
        bar.updateBackButton = updateBackButton;
        bar.titleTextWidth = titleTextWidth;
        bar.titleWidth = titleWidth;
        bar.titleTextX = titleTextX;
        bar.titleLeftRight = titleLeftRight;
        bar.backButtonTextLeft = backButtonTextLeft;
        bar.resetBackButton = resetBackButton;
        bar.align = align;
        bar.calcWidths = calcWidths;
        bar.updatePositions = updatePositions;
        bar.setCss = setCss;

        // Events
        $scope.$on("$destroy", function () {
            for (var n in eleCache) {
                if(eleCache.hasOwnProperty(n)) {
                    eleCache[n] = null;
                }
            }
        });

        // Functions
        function beforeEnter (viewData) {
            $scope.$broadcast("cpsView:beforeEnter", viewData);
        }

        function title (newTitleText) {
            if (arguments.length && newTitleText !== titleText) {
                getEle(TITLE).innerHTML = newTitleText;
                titleText = newTitleText;
                _titleTextWidth = 0;
            }
            return titleText;
        }


        function enableBack (shouldEnable, disableReset) {
            // whether or not the back button show be visible, according
            // to the navigation and history
            if (arguments.length) {
                isBackEnabled = shouldEnable;
                if (!disableReset) {
                    updateBackButton();
                }
            }
            return isBackEnabled;
        }


        function showBack (shouldShow, disableReset) {
            // different from enableBack() because this will always have the back
            // visually hidden if false, even if the history says it should show
            if (arguments.length) {
                isBackShown = shouldShow;
                if (!disableReset) {
                    updateBackButton();
                }
            }
            return isBackShown;
        }


        function showNavBack (shouldShow) {
            // different from showBack() because this is for the entire nav bar's
            // setting for all of it's child headers. For internal use.
            isNavBackShown = shouldShow;
            updateBackButton();
        }


        function updateBackButton () {
            var ele;
            if ((isBackShown && isNavBackShown && isBackEnabled) !== isBackElementShown) {
                isBackElementShown = isBackShown && isNavBackShown && isBackEnabled;
                ele = getEle(BACK_BUTTON);
                if(ele) {
                    ele.classList[ isBackElementShown ? "remove" : "add" ](HIDE);
                }
            }

            if (isBackEnabled) {
                ele = ele || getEle(BACK_BUTTON);
                if (ele) {
                    if (bar.backButtonIcon !== $UiConfig.backButton.icon()) {
                        ele = getEle(BACK_BUTTON + " .icon");
                        if (ele) {
                            bar.backButtonIcon = $UiConfig.backButton.icon();
                            ele.className = "icon " + bar.backButtonIcon;
                        }
                    }

                    if (bar.backButtonText !== $UiConfig.backButton.text()) {
                        ele = getEle(BACK_BUTTON + " .back-text");
                        if (ele) {
                            ele.textContent = bar.backButtonText = $UiConfig.backButton.text();
                        }
                    }
                }
            }
        }


        function titleTextWidth () {
            if (!_titleTextWidth) {
                var bounds = $Dom.getTextBounds(getEle(TITLE));
                _titleTextWidth = Math.min(bounds && bounds.width || 30);
            }
            return _titleTextWidth;
        }


        function titleWidth () {
            var _titleWidth = titleTextWidth(),
                _offsetWidth = getEle(TITLE).offsetWidth;
            if (_offsetWidth < _titleWidth) {
                _titleWidth = _offsetWidth + (titleLeft - titleRight - 5);
            }
            return _titleWidth;
        }


        function titleTextX () {
            return ($element[0].offsetWidth / 2) - (titleWidth() / 2);
        }


        function titleLeftRight () {
            return titleLeft - titleRight;
        }


        function backButtonTextLeft () {
            var offsetLeft = 0,
                ele = getEle(BACK_TEXT);
            while (ele) {
                offsetLeft += ele.offsetLeft;
                ele = ele.parentElement;
            }
            return offsetLeft;
        }


        function resetBackButton (viewData) {
            if ($UiConfig.backButton.previousTitleText()) {
                var previousTitleEle = getEle(PREVIOUS_TITLE),
                    newPreviousTitleText;

                if (previousTitleEle) {
                    previousTitleEle.classList.remove(HIDE);

                    // TODO: implements $history service
                    // var view = (viewData && $History.getViewById(viewData.viewId)),
                    //     newPreviousTitleText = $History.backTitle(view);

                    if (newPreviousTitleText && newPreviousTitleText !== previousTitleText) {
                        previousTitleText = previousTitleEle.innerHTML = newPreviousTitleText;
                    }
                }
                var defaultTitleEle = getEle(DEFAULT_TITLE);
                if (defaultTitleEle) {
                    defaultTitleEle.classList.remove(HIDE);
                }
            }
        }


        function align (textAlign) {
            var titleEle = getEle(TITLE);

            textAlign = textAlign || $attrs.alignTitle || $UiConfig.navBar.alignTitle();

            var widths = calcWidths(textAlign, false);

            if (isBackShown && previousTitleText && $UiConfig.backButton.previousTitleText()) {
                var previousTitleWidths = calcWidths(textAlign, true),
                    availableTitleWidth = $element[0].offsetWidth - previousTitleWidths.titleLeft -
                        previousTitleWidths.titleRight;

                if (titleTextWidth() <= availableTitleWidth) {
                    widths = previousTitleWidths;
                }
            }

            return updatePositions(
                titleEle,
                widths.titleLeft,
                widths.titleRight,
                widths.buttonsLeft,
                widths.buttonsRight,
                widths.css,
                widths.showPrevTitle
            );
        }


        function calcWidths (textAlign, isPreviousTitle) {
            var titleEle = getEle(TITLE),
                backBtnEle = getEle(BACK_BUTTON),
                notifyTrayEle = getEle(NOTIFY_TRAY),
                childNodes = $element[0].childNodes,
                buttonsLeft = 0,
                buttonsRight = 0,
                isCountRightOfTitle,
                updateTitleLeft = 0,
                updateTitleRight = 0,
                backButtonWidth = 0,
                notifyTrayWidth = 0,
                updateCss = "",
                x, y, z, b, c, d, childSize, bounds;

            // Compute how wide the left children are
            // Skip all titles (there may still be two titles, one leaving the dom)
            // Once we encounter a titleEle, realize we are now counting the right-buttons, not left
            for (x = 0; x < childNodes.length; x++) {
                c = childNodes[x];

                childSize = 0;
                if (c.nodeType === 1) {
                    // element node
                    if (c === titleEle) {
                        isCountRightOfTitle = true;
                        continue;
                    }

                    if (c.classList.contains(HIDE)) {
                        continue;
                    }

                    if (isBackShown && c === backBtnEle) {

                        for (y = 0; y < c.childNodes.length; y++) {
                            b = c.childNodes[y];

                            if (b.nodeType === 1) {

                                if (b.classList.contains(BACK_TEXT)) {
                                    for (z = 0; z < b.children.length; z++) {
                                        d = b.children[z];

                                        if (isPreviousTitle) {
                                            if (d.classList.contains(DEFAULT_TITLE)) {
                                                continue;
                                            }
                                            backButtonWidth += d.offsetWidth;
                                        } else {
                                            if (d.classList.contains(PREVIOUS_TITLE)) {
                                                continue;
                                            }
                                            backButtonWidth += d.offsetWidth;
                                        }
                                    }

                                } else {
                                    backButtonWidth += b.offsetWidth;
                                }

                            } else if (b.nodeType === 3 && b.nodeValue.trim()) {
                                bounds = $Dom.getTextBounds(b);
                                backButtonWidth += bounds && bounds.width || 0;
                            }

                        }
                        childSize = backButtonWidth || c.offsetWidth;

                    } else if (c === notifyTrayEle) {
                        for (y = 0; y < c.childNodes.length; y++) {
                            b = c.childNodes[y];

                            if (b.nodeType === 1) {

                                if (b.classList.contains(NOTIFY_BUTTON)) {
                                    notifyTrayWidth += b.offsetWidth;
                                }

                            } else if (b.nodeType === 3 && b.nodeValue.trim()) {
                                bounds = $Dom.getTextBounds(b);
                                notifyTrayWidth += bounds && bounds.width || 0;
                            }

                        }
                        childSize = notifyTrayWidth || c.offsetWidth;
                    } else {
                        // not the title, not the back button, not a hidden element
                        childSize = c.offsetWidth;
                    }

                } else if (c.nodeType === 3 && c.nodeValue.trim()) {
                    // text node
                    bounds = $Dom.getTextBounds(c);
                    childSize = bounds && bounds.width || 0;
                }

                if (isCountRightOfTitle) {
                    buttonsRight += childSize;
                } else {
                    buttonsLeft += childSize;
                }
            }

            // Size and align the header titleEle based on the sizes of the left and
            // right children, and the desired alignment mode
            if (textAlign === "left") {
                updateCss = "title-left";
                if (buttonsLeft) {
                    updateTitleLeft = buttonsLeft + 15;
                }
                if (buttonsRight) {
                    updateTitleRight = buttonsRight + 15;
                }

            } else if (textAlign === "right") {
                updateCss = "title-right";
                if (buttonsLeft) {
                    updateTitleLeft = buttonsLeft + 15;
                }
                if (buttonsRight) {
                    updateTitleRight = buttonsRight + 15;
                }

            } else {
                // center the default
                var margin = Math.max(buttonsLeft, buttonsRight) + 10;
                if (margin > 10) {
                    updateTitleLeft = updateTitleRight = margin;
                }
            }

            return {
                backButtonWidth: backButtonWidth,
                notifyTrayWidth: notifyTrayWidth,
                buttonsLeft: buttonsLeft,
                buttonsRight: buttonsRight,
                titleLeft: updateTitleLeft,
                titleRight: updateTitleRight,
                showPrevTitle: isPreviousTitle,
                css: updateCss
            };
        }


        function updatePositions (titleEle, updateTitleLeft, updateTitleRight,
                                 buttonsLeft, buttonsRight, updateCss, showPreviousTitle) {
            var deferred = $q.defer();

            // only make DOM updates when there are actual changes
            if (titleEle) {
                if (updateTitleLeft !== titleLeft) {
                    titleEle.style.left = updateTitleLeft ? updateTitleLeft + "px" : "";
                    titleLeft = updateTitleLeft;
                }
                if (updateTitleRight !== titleRight) {
                    titleEle.style.right = updateTitleRight ? updateTitleRight + "px" : "";
                    titleRight = updateTitleRight;
                }

                if (updateCss !== titleCss) {
                    if(updateCss) {
                        titleEle.classList.add(updateCss);
                    }
                    if(titleCss) {
                        titleEle.classList.remove(titleCss);
                    }
                    titleCss = updateCss;
                }
            }

            if ($UiConfig.backButton.previousTitleText()) {
                var prevTitle = getEle(PREVIOUS_TITLE),
                    defaultTitle = getEle(DEFAULT_TITLE);

                if(prevTitle) {
                    prevTitle.classList[ showPreviousTitle ? "remove" : "add"](HIDE);
                }
                if(defaultTitle) {
                    defaultTitle.classList[ showPreviousTitle ? "add" : "remove"](HIDE);
                }
            }

            $Dom.requestAnimationFrame(function () {
                if (titleEle && titleEle.offsetWidth + 10 < titleEle.scrollWidth) {
                    var minRight = buttonsRight + 5,
                        testRight = $element[0].offsetWidth - titleLeft - titleTextWidth() - 20;
                    updateTitleRight = testRight < minRight ? minRight : testRight;
                    if (updateTitleRight !== titleRight) {
                        titleEle.style.right = updateTitleRight + "px";
                        titleRight = updateTitleRight;
                    }
                }
                deferred.resolve();
            });

            return deferred.promise;
        }


        function setCss (elementClassname, css) {
            $Dom.cachedStyles(getEle(elementClassname), css);
        }


        // Private
        function getEle (className) {
            if (!eleCache[className]) {
                eleCache[className] = $element[0].querySelector("." + className);
            }
            return eleCache[className];
        }
    }

    // Export
    module.exports = BarController;

})();
