/* global module */

// core/ui/modal/modalService.js
//
// provide service for ui modal module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        $$ = angular.element;

    function modalService () {
        var _isOpen = false,
            currentModal = {
                name: null },
            globalOptions = {
                closeButtonHtml: "<span class='modal-close-x'>&times;</span>"
            };

        // inject
        $get.$inject = ["$rootScope", "$q", "$compile", "$Logger", "$Template", "$Body"];

        // setter and getter
        var ModalService = {
            set: $set,
            $get: $get
        };
        return ModalService;

        // setter
        /**
         * $set
         *
         * Set options in provider config
         * Example: $ModalProvider.set({closeButtonHtml: "X"});
         * @param {string} keyOrHash name options to change
         * @param {string} value value options
         *
         * @return {object} new options
         */
        function $set (keyOrHash, value) {
            var k, v, _results;
            if (typeof keyOrHash === "object") {
                _results = [];
                for (k in keyOrHash) {
                    if(keyOrHash.hasOwnProperty(k)) {
                        v = keyOrHash[k];
                        _results.push(globalOptions[k] = v);
                    }
                }
                return _results;
            } else {
                globalOptions[keyOrHash] = value;
                return globalOptions[keyOrHash];
            }
        }

        // Getter
        function $get ($rootScope, $q, $compile, $Logger, $Template, $Body) {
            var logger = $Logger.getInstance();

            // Members
            var Modal = {
                options: globalOptions,
                toggle: toggle,
                open: open,
                close: close,
                checkIsOpen: checkIsOpen,
                isOpen: isOpen,
                fromTemplate: fromTemplate,
                fromTemplateUrl: fromTemplateUrl
            };

            return Modal;

            /**
             * @name toggle
             * @description Alternating modal state between open and closed
             * @param {string} modalName unique id for modal instance
             * @param {object} options aditional options like, animation, title, closeBtn
             *
             * @return {promise}
             */
            function toggle (modalName, options) {
                var deferred = $q.defer(),
                    modal,
                    err;
                if(modalName) {
                    Modal.checkIsOpen(currentModal.name).then(function () {
                        currentModal.name = modalName;
                        modal = $rootScope.modals[currentModal.name];

                        modal.show = !modal.show;
                        _isOpen = !_isOpen;
                        if(!modal.show) {
                            currentModal.name = null;
                        }
                        deferred.resolve(modal);
                    });
                } else {
                    err = "Id modal is required";
                    logger.warn("modal", err);
                    deferred.reject(err);
                }
                return deferred.promise;
            }

            /**
             * @name open
             * @description Open modal instance
             * @param {string} modalName unique id for modal instance
             * @param {object} options aditional options like, animation, title, closeBtn
             *
             * @return {promise}
             */
            function open (modalName, options) {
                var deferred = $q.defer(),
                    modal,
                    err;
                if(modalName) {
                    Modal.checkIsOpen(currentModal.name).then(function () {
                        currentModal.name = modalName;
                        modal = $rootScope.modals[currentModal.name];

                        modal.show = true;
                        _isOpen = true;
                        deferred.resolve(modal);
                    });
                } else {
                    err = "Id modal is required";
                    logger.warn("modal", err);
                    deferred.reject(err);
                }
                return deferred.promise;
            }

            /**
             * @name close
             * @description Close current modal instance
             *
             * @return {promise}
             */
            function close () {
                var deferred = $q.defer(),
                    modal,
                    err;
                _isOpen = false;
                if(currentModal.name) {
                    modal = $rootScope.modals[currentModal.name];
                    modal.show = false;
                    currentModal.name = null;
                    deferred.resolve(modal);
                } else {
                    err = "modal intance no exist";
                    logger.warn("modal", err);
                    deferred.reject(err);
                }
                return deferred.promise;
            }

            /**
             * @name checkIsOpen
             * @description verifies whether any modal instance, if there is closed
             * @param {string} modalName unique id for modal instance
             *
             * @return {promise}
             */
            function checkIsOpen (modalName) {
                var deferred = $q.defer();
                if(modalName) {
                    Modal.close();
                    deferred.resolve(modalName);
                } else {
                    deferred.resolve(modalName);
                }

                return deferred.promise;
            }

            /**
             * @name isOpen
             * @description Check if any modal is open
             *
             * @return {boolean} true or false
             */
            function isOpen () {
                return _isOpen;
            }
            /**
             * @name fromTemplate
             * @param {string} templateString The template string to use as the modal's
             * content.
             * @param {object} options Options to be passed method.
             * @returns {object} An instance of an
             * controller.
             */
            function fromTemplate (templateString, options) {
                var deferred = $q.defer(),
                    modal = createModal(templateString, options || {});
                if(modal) {
                    deferred.resolve(modal);
                } else {
                    deferred.reject(modal);
                }
                return deferred.promise;
            }

            /**
             * @name fromTemplateUrl
             * @param {string} templateUrl The url to load the template from.
             * @param {object} options Options to be passed method.
             * options object.
             * @returns {promise} A promise that will be resolved with an instance of
             * an controller.
             */
            function fromTemplateUrl (url, options) {
                return $Template.load(url).then(function (templateString) {
                    var modal = createModal(templateString, options || {});
                    return modal;
                });
            }

            // Private
            function createModal (templateString, options) {
                // Create a new scope for the modal
                var self = {},
                    scope = options.scope && options.scope.$new() || $rootScope.$new(true),
                    btnClose = options.btnClose ? "modal-close-btn='true' " : "",
                    modalTitle = options.title ? "modal-title='" + options.title + "' " : "",
                    onClose = options.onClose ? "modal-on-close='" + options.onClose + "' " : "",
                    attrOptions = btnClose + modalTitle + onClose,
                    element;

                options.viewType = options.viewType || "modal";

                if(!options.id) {
                    logger.warn("modal", "Need option with id modal");
                }

                // Compile the template
                self.scope = scope;
                self.element = $$("<cps-" + options.viewType + " id='" + options.id + "' " + attrOptions + " >" +
                                  templateString + "</cps-" + options.viewType + ">");
                $Body.get().appendChild(self.element[0]);
                element = $compile(self.element)(self.scope);

                // Options
                options.$el = element;
                options.el = element[0];
                options.modalEl = options.el.querySelector("." + options.viewType);

                // Instance
                self.toggle = function () {
                    toggle(options.id, options);
                };
                self.open = function () {
                    open(options.id, options);
                };
                self.close = function () {
                    close();
                };
                self.modal = options;

                // If this wasn't a defined scope, we can assign the viewType to the isolated scope
                // we created
                if (!options.scope) {
                    self.scope[ options.viewType ] = self;
                }

                return self;
            }

        }
    }

    // Exports
    module.exports = {
        modalService: modalService
    };

})();
