/* global module */

// core/ui/modal/modalDirective.js
//
// directives function for ui modal module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        $$ = angular.element;

    /**
     * This directive contain all elements for modal windows
     * @usage
     * ```html
     * <cps-modal id="modalZonesList" modal-close-btn="true" modal-dialog-title='CPS'>
     *  <!-- Other html content -->
     * </cps-modal>
     * ```
     * @Options Attributes:
     * id: {string} id moda (required)
     * modal-title: {string} title window
     * modal-close-btn: {boolean} if show btn close
     * modal-on-close: {function} callback if close
     * modal-show: {string} scope controller
     */
    cpsModal.$inject = ["$Logger", "$Body", "$Modal", "$Locale", "$sce", "$rootScope", "$timeout"];
    function cpsModal ($Logger, $Body, $Modal, $Locale, $sce, $rootScope, $timeout) {
        var logger = $Logger.getInstance(),
            modalDirective = {
                restrict: "E",
                controller: "ModalController",
                controllerAs: "modal",
                scope: {
                    id: "@",
                    modalShow: "=",
                    modalCloseBtn: "@",
                    modalTitle: "@",
                    modalOnClose: "&?"
                },
                replace: true,
                transclude: true,
                link: function postLink (scope, element, attrs) {
                    var el = element,
                        viewType = "modal",
                        modalEl = $$("cps-modal-view"),
                        modalId = scope.id,
                        newModal,
                        modalScope,
                        modalOptions,
                        modalAnimation;

                    if(!scope.id) {
                        logger.warn("modal", "Need option with id modal");
                    }

                    newModal = makeObj(modalId, {show: false});

                    if(!$rootScope.modals) {
                        $rootScope.modals = {};
                    }
                    angular.extend($rootScope.modals, newModal);
                    modalScope = $rootScope.modals[modalId];

                    // Get options
                    if(angular.isDefined(modalScope.options)) {
                        modalOptions = modalScope.options;
                        modalAnimation = modalOptions.animation;
                    }
                    // Modal title
                    if(scope.modalTitle) {
                        scope.modalTitleLocale = scope.modalTitle;
                        $$("#modalTitle").attr("locale-id", scope.modalTitleLocale);
                        scope.modalTitle = $Locale.get(scope.modalTitle, null, scope.modalTitle);
                    }

                    // Scope watch
                    scope.$watch("modalShow", function (newVal, oldVal) {
                        if (newVal && !oldVal) {
                            el.removeClass("hide");
                            $rootScope.onTopOpen = true;
                            $timeout(function () {
                                $Body.addClass(viewType + "-open");
                            }, 400);

                            modalEl.addClass(modalAnimation || "slide-in-up");
                            modalEl.addClass("ng-enter active").removeClass("ng-leave ng-leave-active");
                            if(scope.modalTitle) {
                                scope.modalTitle = $Locale.get(scope.modalTitle, null, scope.modalTitle);
                            }

                            $timeout(function () {
                                modalEl.addClass("ng-enter-active");
                                // TODO: send events
                                // self.scope.$parent && self.scope.$parent.$broadcast(self.viewType + ".shown", self);
                                el.addClass("active");
                                // self.scope.$broadcast("$Header.align");
                            }, 20);

                        } else {
                            el.removeClass("active");
                            $rootScope.onTopOpen = false;
                            modalEl.addClass("ng-leave");

                            $timeout(function () {
                                modalEl.addClass("ng-leave-active").removeClass("ng-enter ng-enter-active active");
                            }, 20);

                            return $timeout(function () {
                                $Body.removeClass(viewType + "-open");
                                el.addClass("hide");
                            }, 320);
                        }
                        if ((!newVal && oldVal) && (scope.modalOnClose !== null)) {
                            return scope.modalOnClose();
                        }
                    });

                    // Clean prevent memory leaks
                    element.on("$destroy", function () {
                        scope.$destroy();
                        delete $rootScope.modals[modalId];
                    });

                    // Functions
                    function setupCloseButton () {
                        scope.closeButtonHtml = $sce.trustAsHtml($Modal.options.closeButtonHtml);
                        return scope.closeButtonHtml;
                    }

                    function setupStyle () {
                        scope.dialogStyle = {};
                        if (angular.isDefined(attrs.modalWidth)) {
                            scope.dialogStyle.modalWidth = attrs.modalWidth;
                        }
                        if (angular.isDefined(attrs.modalHeight)) {
                            scope.dialogStyle.modalHeight = attrs.modalHeight;
                            return scope.dialogStyle.modalHeight;
                        }
                    }

                    // Run
                    setupCloseButton();
                    return setupStyle();
                },
                template: function (tElement, tAttrs) {
                    var contentHtml = "<div class='modal-backdrop hide' modal-show='modals." + tAttrs.id + ".show'>\n" +
                            "<div class='modal-backdrop-bg' ng-click='modal.close()'></div>\n" +
                            "<div class='modal-wrapper' ng-style='dialogStyle'>\n" +
                            "<cps-modal-view class='modal slide-in-up ng-enter active ng-enter-active'>" +
                            "<div class='bar bar-header bar-positive disable-user-behavior'" +
                            " ng-show='modalTitle && modalTitle.length'>" +
                            "<h1 class='title' id='modalTitle' ng-bind='modalTitle'></h1>" +
                            "<button ng-show='modalCloseBtn' class='button button-clear button-primary' " +
                            "locale-id='close' hm-tap='modal.close()'>Close</button>" +
                            "</div>\n" +
                            "<div class='modal-close' hm-tap='modal.close()'>\n" +
                            "<div ng-bind-html='closeButtonHtml'></div>\n" +
                            "</div>\n" +
                            "<div class='padding scroll-content' ng-class='{\"has-header\": modalTitle && " +
                            "modalTitle.length}' ng-transclude></div>\n" +
                            "</cps-modal-view>\n" +
                            "</div>\n" +
                            "</div>";
                    return contentHtml;
                }
            };
        return modalDirective;

        function makeObj (key, value) {
            var obj = {};
            obj[key] = value;
            return obj;
        }
    }

    /**
     * This directive control action for modal window, like close or open
     * @usage
     * ```html
     * <button modal-id='modalZonesList' cps-modal-control='toggle'>Modal</\button>
     * ```
     * @Options Attributes:
     * modal-id: {string} id modal to control (required)
     * modal-options: {Objcet} options modal
     * cps-modal-control: {string} action to controll, toggle, open, close (required)
     */
    modalControl.$inject = ["$parse", "$Logger", "$Modal"];
    function modalControl ($parse, $Logger, $Modal) {
        var logger = $Logger.getInstance(),
            controlDirective = {
                restrict: "A",
                controller: "ModalController",
                controllerAs: "modal",
                link: function postLink (scope, element, attrs, ctl) {
                    var modalAction = attrs.cpsModalControl,
                        modalId = attrs.modalId,
                        modalOptions = attrs.modalOptions,
                        fn = $parse($Modal[modalAction], null, true);

                    element.on("tap touchstart click", function (e) {
                        e.preventDefault();
                        if(angular.isDefined($Modal[modalAction])) {
                            if(modalId) {
                                var callback = function () {
                                    fn(modalId, modalOptions);
                                };
                                scope.$apply(callback);
                            } else {
                                logger.warn("modal", "modal-id is required with id modal");
                            }
                        } else {
                            logger.warn("modal", "cps-modal-control need valid action, toggle, open or close");
                        }
                    });
                }
            };

        return controlDirective;
    }

    /**
     * This directive add view container
     *
     */
    function modalView () {
        var view = {
            restrict: "E",
            compile: function (element, attr) {
                element.addClass("modal");
            }
        };
        return view;
    }

    // Exports
    module.exports = {
        modal: cpsModal,
        control: modalControl,
        view: modalView
    };

})();
