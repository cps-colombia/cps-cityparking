/* global module */

// core/ui/modal/modalController.js
//
// This controller acts as an intermediary
// between directive and services
// for ui modal module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    ModalController.$inject = ["$scope", "$rootScope", "$Modal"];
    function ModalController ($scope, $rootScope, $Modal) {
        var modal = this;

        // Membres
        /**
         * Delegate function to $Modal service
         */
        modal.toggle = $Modal.toggle;
        modal.open = $Modal.open;
        modal.close = $Modal.close;

    }

    // Exports
    module.exports = {
        ModalController: ModalController
    };

})();
