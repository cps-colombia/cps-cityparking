/* global module */

// core/ui/menu/menuService.js
//
// provide service for ui menu module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    menuService.$inject = [
        "$rootScope",
        "$Logger",
        "$Body",
        "$Dom",
        "$View"
    ];
    function menuService ($rootScope, $Logger, $Body, $Dom, $View) {
        var logger = $Logger.getInstance(),
            _isOpen = false,
            _isAsideExposed,
            _enableMenuWithBackViews = true,
            edgeThreshold = 25,
            dragThresholdX = 10,
            edgeThresholdEnabled = false,
            defaultLeft = {
                width: 275
            },
            defaultRight = {
                width: 275
            },
            content,
            rightShowing,
            leftShowing,
            isDragging,
            currentSection,
            startX,
            lastX,
            offsetX;


        // Scope
        $rootScope.sideMenuContentTranslateX = 0;

        // Members
        var Menu = {
            left: defaultLeft,
            right: defaultRight,
            setContent: setContent,
            getContent: getContent,
            setSection: setSection,
            getSection: getSection,
            setIsOpen: setIsOpen,
            isOpen: isOpen,
            isOpenFull: isOpenFull,
            isOpenLeft: isOpenLeft,
            isOpenRight: isOpenRight,
            toggleLeft: toggleLeft,
            toggleRight: toggleRight,
            toggle: toggle,
            close: close,
            endDrag: endDrag,
            handleDrag: handleDrag,
            enableMenuWithBackViews: enableMenuWithBackViews,
            isAsideExposed: isAsideExposed,
            exposeAside: exposeAside,
            activeAsideResizing: activeAsideResizing,
            canDragContent: canDragContent,
            edgeDragThreshold: edgeDragThreshold,
            isDraggableTarget: isDraggableTarget,
            getOpenAmount: getOpenAmount,
            getOpenPercentage: getOpenPercentage,
            getOpenRatio: getOpenRatio
        };
        return Menu;

        // Functions
        /**
         * @description Set the content view controller if not
         * passed in the constructor options.
         * @param {object} content
         */
        function setContent (_content) {
            if (_content) {
                content = _content;
                content.onDrag = function (e) {
                    handleDrag(e);
                };
                content.endDrag = function (e) {
                    endDrag(e);
                };
            }
        }
        function getContent () {
            return content;
        }

        function hasContent () {
            var _hasContent = content ? true : false;
            if(!_hasContent) {
                logger.warn("menu", "need 'menu-content-control' directive main div");
            }
            return _hasContent;
        }

        function setSection (path) {
            currentSection = path;
        }

        function getSection () {
            return currentSection;
        }

        function setIsOpen (dataIsOpen) {
            _isOpen = dataIsOpen;
        }

        function isOpen () {
            return _isOpen;
        }

        function isOpenFull () {
            return getOpenAmount() !== 0;
        }

        function isOpenLeft () {
            return getOpenAmount() > 0;
        }
        function isOpenRight () {
            return getOpenAmount() < 0;
        }
        /**
         * Toggle the left menu to open 100%
         */
        function toggleLeft (shouldOpen) {
            if (isAsideExposed() || !Menu.left.isEnabled || !hasContent()) {
                return;
            }
            var openAmount = getOpenAmount();
            if (arguments.length === 0) {
                shouldOpen = openAmount <= 0;
            }
            content.enableAnimation();
            if (!shouldOpen) {
                openPercentage(0);
            } else {
                _isOpen = true; // Prevent white background
                openPercentage(100);
            }
        }
        /**
         * Toggle the right menu to open 100%
         */
        function toggleRight (shouldOpen) {
            if (isAsideExposed()  || !Menu.right.isEnabled || !hasContent()) {
                return;
            }
            var openAmount = getOpenAmount();
            if (arguments.length === 0) {
                shouldOpen = openAmount >= 0;
            }
            content.enableAnimation();
            if (!shouldOpen) {
                openPercentage(0);
            } else {
                _isOpen = true; // Prevent white background
                openPercentage(-100);
            }
        }
        function toggle (side) {
            if (side === "right") {
                toggleRight();
            } else {
                toggleLeft();
            }
        }
        /**
         * Close all menus.
         */
        function close () {
            openPercentage(0);
        }

        function endDrag (e) {
            // End a drag with the given event
            if (isAsideExposed()) {
                return;
            }
            if (isDragging) {
                snapToRest(e);
            }
            startX = null;
            lastX = null;
            offsetX = null;
        }
        // Handle a drag event
        function handleDrag (e) {
            if (isAsideExposed() || !$rootScope.dragContent || !hasContent()) {
                return;
            }
            // If we don't have start coords, grab and store them
            if (!startX) {
                startX = e.pointers[0].pageX;
                lastX = startX;
            } else {
                // Grab the current tap coords
                lastX = e.pointers[0].pageX;
            }
            // Calculate difference from the tap points
            if (!isDragging && Math.abs(lastX - startX) > dragThresholdX) {
                // if the difference is greater than threshold, start dragging using the current
                // point as the starting point
                startX = lastX;
                isDragging = true;
                // Initialize dragging
                content.disableAnimation();
                offsetX = getOpenAmount();
            }
            if (isDragging) {
                openAmount(offsetX + (lastX - startX));
            }
        }

        function enableMenuWithBackViews (val) {
            if (arguments.length) {
                _enableMenuWithBackViews = !!val;
            }
            return _enableMenuWithBackViews;
        }

        function isAsideExposed () {
            return !!_isAsideExposed;
        }
        function exposeAside (shouldExposeAside) {
            if (!(Menu.left && Menu.left.isEnabled) && !(Menu.right && Menu.right.isEnabled)) {
                return;
            }
            if(!hasContent()) {
                return;
            }
            close();
            _isAsideExposed = shouldExposeAside;
            if (Menu.left && Menu.left.isEnabled) {
                // set the left marget width if it should be exposed
                // otherwise set false so there's no left margin
                content.setMarginLeft(_isAsideExposed ? Menu.left.width : 0);
            } else if (Menu.right && Menu.right.isEnabled) {
                content.setMarginRight(_isAsideExposed ? Menu.right.width : 0);
            }
            $rootScope.$emit("cpsExposeAside", isAsideExposed());
        }

        function activeAsideResizing (isResizing) {
            $Body.enableClass(isResizing, "aside-resizing");
        }

        function canDragContent (canDrag) {
            if (arguments.length) {
                $rootScope.dragContent = !!canDrag;
            }
            return $rootScope.dragContent;
        }

        function edgeDragThreshold (value) {
            if (arguments.length) {
                if (angular.isNumber(value) && value > 0) {
                    edgeThreshold = value;
                    edgeThresholdEnabled = true;
                } else {
                    edgeThresholdEnabled = !!value;
                }
            }
            return edgeThresholdEnabled;
        }

        function isDraggableTarget (e) {
            // Only restrict edge when sidemenu is closed and restriction is enabled
            var startX = e.center && e.center.x,
                shouldOnlyAllowEdgeDrag = edgeThresholdEnabled && !isOpen(),
                dragIsWithinBounds = !shouldOnlyAllowEdgeDrag ||
                    startX <= edgeThreshold ||
                    startX >= content.element.offsetWidth - edgeThreshold,
                menuEnabled = _enableMenuWithBackViews ? true : false;

            // TODO: backView
            return ($rootScope.dragContent || isOpen()) &&
                dragIsWithinBounds &&
                !e.srcEvent.defaultPrevented &&
                menuEnabled &&
                !e.target.tagName.match(/input|textarea|select|object|embed/i) &&
                !e.target.isContentEditable &&
                !(e.target.dataset ?
                  e.target.dataset.preventScroll : e.target.getAttribute("data-prevent-scroll") === "true");
        }

        /**
         * @return {float} The amount the side menu is open, either
         * positive or negative for left (positive), or right (negative)
         */
        function getOpenAmount () {
            return content && content.getTranslateX() || 0;
        }

        /**
         * @return {float} The percentage of open amount over menu width. For example, a
         * menu of width 100 open 50 pixels would be open 50%. Value is negative
         * for right menu.
         */
        function getOpenPercentage () {
            return getOpenRatio() * 100;
        }

        /**
         * @return {float} The ratio of open amount over menu width. For example, a
         * menu of width 100 open 50 pixels would be open 50% or a ratio of 0.5. Value is negative
         * for right menu.
         */
        function getOpenRatio () {
            var amount = getOpenAmount();
            if (amount >= 0) {
                return amount / Menu.left.width;
            }
            return amount / Menu.right.width;
        }

        // Private
        function onActionComplete (cb) {
            $Dom.animationComplete(content.element || content).then(function () {
                _isOpen = getOpenAmount() !== 0;
                if(cb) {
                    cb();
                }
            });
        }

        /**
         * Open the menu with a given percentage amount.
         * @param {float} percentage The percentage (positive or negative for left/right) to open the
         */
        function openPercentage (percentage) {
            var p = percentage / 100;
            if (Menu.left && percentage >= 0) {
                openAmount(Menu.left.width * p);
            } else if (Menu.right && percentage < 0) {
                openAmount(Menu.right.width * p);
            }
            // add the CSS class "menu-open" if the percentage does not
            // equal 0, otherwise remove the class from the body element
            $Body.enableClass((percentage !== 0), "menu-open");
            content.setCanScroll(percentage === 0);
        }

        /**
         * Open the menu the given pixel amount.
         * @param {float} amount the pixel amount to open the menu. Positive value for left menu,
         * negative value for right menu (only one menu will be visible at a time).
         */
        function openAmount (amount) {
            var maxLeft = Menu.left && Menu.left.width || 0,
                maxRight = Menu.right && Menu.right.width || 0;

            // Check if we can move to that side, depending if the left/right panel is enabled
            if (!(Menu.left && Menu.left.isEnabled) && amount > 0) {
                content.setTranslateX(0);
                return;
            }
            if (!(Menu.right && Menu.right.isEnabled) && amount < 0) {
                content.setTranslateX(0);
                return;
            }

            if (leftShowing && amount > maxLeft) {
                content.setTranslateX(maxLeft);
                return;
            }
            if (rightShowing && amount < -maxRight) {
                content.setTranslateX(-maxRight);
                return;
            }

            content.setTranslateX(amount);
            onActionComplete();
            if (amount >= 0) {
                leftShowing = true;
                rightShowing = false;
                if (amount > 0) {
                    // Push the z-index of the right menu down
                    if(Menu.right && Menu.right.pushDown) {
                        Menu.right.bringDown();
                    }
                    // Bring the z-index of the left menu up
                    if(Menu.left && Menu.left.bringUp) {
                        Menu.left.bringUp();
                    }
                }
            } else {
                rightShowing = true;
                leftShowing = false;
                if(Menu.right) {
                    // Bring the z-index of the right menu up
                    if(Menu.right.bringUp) {
                        Menu.right.bringUp();
                    }
                }
                if(Menu.left) {
                    // Push the z-index of the left menu down
                    if(Menu.left.pushDown) {
                        Menu.left.pushDown();
                    }
                }
            }
        }

        /**
         * Given an event object, find the final resting position of this side
         * menu. For example, if the user "throws" the content to the right and
         * releases the touch, the left menu should snap open (animated, of course).
         *
         * @param {Event} e the gesture event to use for snapping
         */
        function snapToRest (e) {
            var ratio = getOpenRatio(),
                velocityThreshold = 0.3,
                velocityX = e.velocityX,
                direction = e.direction;

            // We want to animate at the end of this
            content.enableAnimation();
            isDragging = false;

            // Check how much the panel is open after the drag, and
            // what the drag velocity is
            if (ratio === 0) {
                // Just to be safe
                openPercentage(0);
                return;
            }
            // Going right, less than half, too slow (snap back)
            if (ratio > 0 && ratio < 0.5 && direction === 4 && velocityX < velocityThreshold) {
                openPercentage(0);
            }
            // Going left, more than half, too slow (snap back)
            else if (ratio > 0.5 && direction === 2 && velocityX < velocityThreshold) {
                openPercentage(100);
            }
            // Going left, less than half, too slow (snap back)
            else if (ratio < 0 && ratio > -0.5 && direction === 2 && velocityX < velocityThreshold) {
                openPercentage(0);
            }
            // Going right, more than half, too slow (snap back)
            else if (ratio < 0.5 && direction === 4 && velocityX < velocityThreshold) {
                openPercentage(-100);
            }
            // Going right, more than half, or quickly (snap open)
            else if (direction === 4 && ratio >= 0 && (ratio >= 0.5 || velocityX > velocityThreshold)) {
                openPercentage(100);
            }
            // Going left, more than half, or quickly (span open)
            else if (direction === 2 && ratio <= 0 && (ratio <= -0.5 || velocityX > velocityThreshold)) {
                openPercentage(-100);
            }
            // Snap back for safety
            else {
                openPercentage(0);
            }
        }
    }
    // Exports
    module.exports = {
        menuService: menuService
    };

})();
