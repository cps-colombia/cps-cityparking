/* global module */

// core/ui/menu/menuDirective.js
//
// directives function for ui menu module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash"),
        angular = require("angular"),
        $$ = angular.element;

    cpsMenu.$inject = [
        "$location",
        "$rootScope",
        "$Menu",
        "$Body",
        "$Gestures"
    ];
    function cpsMenu ($location, $rootScope, $Menu, $Body, $Gestures) {
        var menuDirective = {
            restrict: "E",
            scope: true,
            controller: "MenuController",
            controllerAs: "menu",
            compile: function (tElement, tAttr) {
                if(angular.isUndefined(tAttr.isEnabled)) {
                    tAttr.$set("isEnabled", "true");
                }
                if(angular.isUndefined(tAttr.width)) {
                    tAttr.$set("width", "275");
                }
                tElement.addClass("menu menu menu-" + tAttr.side);

                return { pre: prelink, post: postLink };
            }
        };

        // Constructor
        function SideMenu (opts) {
            this.el = opts.el;
            this.isEnabled = opts.isEnabled || true;
            this.setWidth(opts.width);

            return this;
        }
        // Prototype
        SideMenu.prototype.getFullWidth = function () {
            return this.width;
        };
        SideMenu.prototype.setWidth = function (width) {
            this.width = width;
            this.el.style.width = width + "px";
        };
        SideMenu.prototype.setIsEnabled = function (isEnabled) {
            this.isEnabled = isEnabled;
        };
        SideMenu.prototype.bringUp = function () {
            if(this.el.style.zIndex !== "0") { // FIXME: index 0 not work in some devices
                this.el.style.zIndex = "0";
            }
        };
        SideMenu.prototype.pushDown = function () {
            if(this.el.style.zIndex !== "-1") {
                this.el.style.zIndex = "-1";
            }
        };

        // Return instance
        return menuDirective;

        // Private Function
        function prelink (scope, element, attrs, ctrl) {
            if (angular.isDefined(attrs.dragContent)) {
                scope.$watch(attrs.dragContent, function (value) {
                    $Menu.canDragContent(value);
                });
            } else {
                $Menu.canDragContent(true);
            }

            scope.$on("cpsExposeAside", function (evt, isAsideExposed) {
                if (!scope.$exposeAside) {
                    scope.$exposeAside = {};
                }
                scope.$exposeAside.active = isAsideExposed;
                $Body.enableClass(isAsideExposed, "aside-open");
            });
            scope.$on("$destroy", function () {
                $Body.removeClass("menu-open", "aside-open");
            });
        }

        function postLink (scope, element, attr, sideMenuCtrl) {
            scope.side = attr.side || "left";
            var sideMenu = $Menu[scope.side] = new SideMenu({
                    width: attr.width,
                    el: element[0],
                    isEnabled: true
                }),
                dragLeftGesture,
                numberVal,
                targetSection;

            // Event
            dragLeftGesture = $Gestures.hammer(element[0], "pan" + scope.side , function () {
                $Menu.close();
            });

            // Check if curren section is the same of event target
            $$(element).on("touchstart", "a", function (e) {
                targetSection = $$(e.currentTarget).attr("href");
                if(targetSection) {
                    if(targetSection.replace("#", "/") === $location.path()) {
                        e.preventDefault();
                        $Menu.close();
                    }
                }
            });

            // Watch menu width
            scope.$watch(attr.width, function (val) {
                numberVal = +val;
                if (numberVal && numberVal === val) {
                    sideMenu.setWidth(+val);
                }
            });

            // watch status menu
            scope.$watch(function () {
                return $Menu.isOpen();
            }, function (newVal, oldVal) {
                if(newVal !== oldVal) {
                    element.toggleClass("menu-visible", newVal);
                }
            });

            // watch current section
            scope.$watch(function () {
                return $Menu.getSection();
            }, function (newValue, oldValue) {
                if(newValue) {
                    // Clean before
                    $$(element).find(".active-section").removeClass("active-section");
                    // Section tag
                    $$(element).find("[cps-section='" + newValue + "']").addClass("active-section");
                    // href without html5 style enabled
                    newValue = newValue.replace("/", "#");
                    $$(element).find("a[href='" + newValue + "']").addClass("active-section");
                }
            });

            scope.$watch(attr.isEnabled, function (val) {
                sideMenu.setIsEnabled(!!val);
            });
            // Clean
            scope.$on("$destroy", function () {
                $Gestures.offGesture(dragLeftGesture, "panleft", function () {
                    $Menu.close();
                });
            });
        }
    }

    cpsMenuControl.$inject = ["$timeout", "$window", "$Tap", "$Dom", "$View", "$Gestures", "$Menu"];
    function cpsMenuControl ($timeout, $window, $Tap, $Dom, $View, $Gestures, $Menu) {
        var menuControlDirective = {
            restrict: "A",
            scope: true,
            compile: function (tElement, tAttr) {
                tElement.addClass("menu-content pane");
                return { post: postLink };
            }
        };
        // Links
        function postLink (scope, element, attrs) {
            if (angular.isDefined(attrs.dragContent)) {
                scope.$watch(attrs.dragContent, function (value) {
                    $Menu.canDragContent(value);
                });
            } else {
                $Menu.canDragContent(true);
            }

            if (angular.isDefined(attrs.edgeDragThreshold)) {
                scope.$watch(attrs.edgeDragThreshold, function (value) {
                    $Menu.edgeDragThreshold(value);
                });
            }

            var startCoord = null,
                primaryScrollAxis = null,
                content = {
                    element: element[0],
                    onDrag: ld.noop,
                    endDrag: ld.noop,
                    setCanScroll: function (canScroll) {
                        var c = element[0].querySelector(".scroll"),
                            contentEl,
                            scrollScope;

                        if (!c) {
                            return;
                        }

                        contentEl = $$(c.parentElement);
                        if (!contentEl) {
                            return;
                        }

                        // freeze our scroll container if we have one
                        scrollScope = contentEl.scope();
                        if(scrollScope && scrollScope.scrollCtrl) {
                            scrollScope.scrollCtrl.freezeScrollShut(!canScroll);
                        }
                    },
                    getTranslateX: function () {
                        return scope.sideMenuContentTranslateX || 0;
                    },
                    setTranslateX: function (amount) {
                        $Dom.animationFrameThrottle(function () {
                            var xTransform = content.offsetX + amount;
                            element[0].style[$View.getCss().TRANSFORM] = "translate3d(" + xTransform + "px,0,0)";

                            $timeout(function () {
                                scope.sideMenuContentTranslateX = amount;
                            });
                        });
                    },
                    setMarginLeft: function (amount) {
                        $Dom.animationFrameThrottle(function () {
                            if (amount) {
                                amount = parseInt(amount, 10);
                                element[0].style[$View.getCss().TRANSFORM] = "translate3d(" + amount + "px,0,0)";
                                element[0].style.width = ($window.innerWidth - amount) + "px";
                                content.offsetX = amount;
                            } else {
                                element[0].style[$View.getCss().TRANSFORM] = "translate3d(0,0,0)";
                                element[0].style.width = "";
                                content.offsetX = 0;
                            }
                        });
                    },
                    setMarginRight: function (amount) {
                        $Dom.animationFrameThrottle(function () {
                            if (amount) {
                                amount = parseInt(amount, 10);
                                element[0].style.width = ($window.innerWidth - amount) + "px";
                                content.offsetX = amount;
                            } else {
                                element[0].style.width = "";
                                content.offsetX = 0;
                            }
                            // reset incase left gets grabby
                            element[0].style[$View.getCss().TRANSFORM] = "translate3d(0,0,0)";
                        });
                    },
                    enableAnimation: function () {
                        scope.animationEnabled = true;
                        element[0].classList.add("menu-animated");
                    },
                    disableAnimation: function () {
                        scope.animationEnabled = false;
                        element[0].classList.remove("menu-animated");
                    },
                    offsetX: 0
                },
                gestureOpts = {};

            // Set content
            $Menu.setContent(content);

            // Watch menu status
            scope.$watch(function () {
                return content.getTranslateX();
            }, function (newVal, oldVal) {
                if(newVal !== 0) {
                    $Menu.setIsOpen(true);
                } else {
                    $Menu.setIsOpen(false);
                }
            });
            // Reewrite options
            if ($Dom.getParentOrSelfWithClass(element[0], "overflow-scroll")) {
                gestureOpts.prevent_default_directions = ["left", "right"];
            }

            // add gesture handlers
            var contentTapGesture = $Gestures.hammer(element[0], "tap", onContentTap),
                dragRightGesture = $Gestures.hammer(element[0], "panright", onDragX),
                dragLeftGesture = $Gestures.hammer(element[0], "panleft", onDragX),
                dragUpGesture = $Gestures.hammer(element[0], "panup", onDragY),
                dragDownGesture = $Gestures.hammer(element[0], "pandown", onDragY),
                releaseGesture = $Gestures.hammer(element[0], "panend", onDragRelease);

            // Cleanup
            scope.$on("$destroy", function () {
                if (content) {
                    content.setCanScroll(true);
                    content.element = null;
                    content = null;
                    $Menu.setContent(content);
                }
                // TODO:
                // deregisterInstance();
                // deregisterBackButtonAction();

                $Gestures.offGesture(dragLeftGesture, "panleft", onDragX);
                $Gestures.offGesture(dragRightGesture, "panright", onDragX);
                $Gestures.offGesture(dragUpGesture, "panup", onDragY);
                $Gestures.offGesture(dragDownGesture, "pandown", onDragY);
                $Gestures.offGesture(releaseGesture, "panend", onDragRelease);
                $Gestures.offGesture(contentTapGesture, "tap", onContentTap);
            });

            // Internal Functions
            function onContentTap (gestureEvt) {
                // Listen for taps on the content to close the menu
                if ($Menu.getOpenAmount() !== 0) {
                    $Menu.close();
                    gestureEvt.srcEvent.preventDefault();
                    startCoord = null;
                    primaryScrollAxis = null;
                } else if (!startCoord) {
                    startCoord = $Tap.pointerCoord(gestureEvt.srcEvent);
                }
            }

            function onDragX (e) {
                if (!$Menu.isDraggableTarget(e)) {
                    return;
                }

                if (getPrimaryScrollAxis(e) === "x") {
                    $Menu.handleDrag(e);
                    e.srcEvent.preventDefault();
                }
            }

            function onDragY (e) {
                if (getPrimaryScrollAxis(e) === "x") {
                    e.srcEvent.preventDefault();
                }
            }

            function onDragRelease (e) {
                $Menu.endDrag(e);
                startCoord = null;
                primaryScrollAxis = null;
            }

            function getPrimaryScrollAxis (gestureEvt) {
                // gets whether the user is primarily scrolling on the X or Y
                // If a majority of the drag has been on the Y since the start of
                // the drag, but the X has moved a little bit, it's still a Y drag

                if (primaryScrollAxis) {
                    // we already figured out which way they're scrolling
                    return primaryScrollAxis;
                }

                if (gestureEvt) {
                    if (!startCoord) {
                        // get the starting point
                        startCoord = $Tap.pointerCoord(gestureEvt.srcEvent);
                    } else {
                        // we already have a starting point, figure out which direction they're going
                        var endCoord = $Tap.pointerCoord(gestureEvt.srcEvent),
                            xDistance = Math.abs(endCoord.x - startCoord.x),
                            yDistance = Math.abs(endCoord.y - startCoord.y),
                            scrollAxis = (xDistance < yDistance ? "y" : "x");

                        if (Math.max(xDistance, yDistance) > 30) {
                            // ok, we pretty much know which way they're going
                            // let's lock it in
                            primaryScrollAxis = scrollAxis;
                        }

                        return scrollAxis;
                    }
                }
                return "y";
            }
        }

        return menuControlDirective;
    }

    // Exports
    module.exports = {
        menu: cpsMenu,
        control: cpsMenuControl
    };

})();
