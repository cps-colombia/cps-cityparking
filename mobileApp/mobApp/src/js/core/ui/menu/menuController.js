/* global module */

// core/ui/menu/menuController.js
//
// This controller acts as an intermediary
// between directive and services
// for ui menu module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    MenuController.$inject = ["$scope", "$Menu"];
    function MenuController ($scope, $Menu) {
        var menu = this;

        // Members
        // TODO: migrate service to controller and use new delegate service
        /**
         * Delegate function to $Menu service
         */
        menu.close = $Menu.close;
        menu.toggle = $Menu.toggle;
        menu.toggleLeft = $Menu.toggleLeft;
        menu.toggleRight = $Menu.toggleRight;
        menu.enableMenuWithBackViews = $Menu.enableMenuWithBackViews;
    }
    // Exports
    module.exports = {
        MenuController: MenuController
    };
})();
