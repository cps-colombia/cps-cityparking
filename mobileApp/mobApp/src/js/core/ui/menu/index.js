/* global require */

// core/ui/menu/index.js
//
// index function menu ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        menuDirective = require("./menuDirective"),
        menuController = require("./menuController"),
        menuService = require("./menuService"),
        menuRun = require("./menuRun");

    angular.module("cpsMenu", []);
    var cpsMenu = angular.module("cpsMenu");

    // Service
    cpsMenu.factory("$Menu", menuService.menuService);

    // Directive
    cpsMenu.directive("cpsMenu", menuDirective.menu);
    cpsMenu.directive("cpsMenuControl", menuDirective.control);

    // Controller
    cpsMenu.controller("MenuController", menuController.MenuController);

    // Run
    cpsMenu.run(menuRun);


})();
