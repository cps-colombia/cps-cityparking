/* global module */

// core/ui/menu/menuService.js
//
// Run angular config for ui menu module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    menuRun.$inject = ["$location", "$rootScope", "$Menu"];
    function menuRun ($location, $rootScope, $Menu) {
        $rootScope.$on("$routeChangeSuccess", function (event, next) {
            $Menu.setSection($location.path());

            if($Menu.isOpen()) {
                $Menu.close();
            }
        });
    }

    // Export
    module.exports = menuRun;

})();
