/* global module */

// core/ui/popmenu/popmenuService.js
//
// factory service for ui popmenu module
// this use Modal controller and service for
// Instantiated but with custom options
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    popmenuService.$inject =  ["$window", "$Position", "$Modal"];
    function popmenuService ($window, $Position, $Modal) {
        var POPMENU_BODY_PADDING = 6,
            POPMENU_OPTIONS = {
                viewType: "popmenu",
                hideDelay: 1,
                animation: "none",
                positionView: positionView
            };

        // Members
        var Popmenu = {
            fromTemplate: fromTemplate,
            fromTemplateUrl: fromTemplateUrl
        };

        return Popmenu;

        // Functions
        /**
         * @name fromTemplate
         * @param {string} templateString The template string to use as the popmenus's
         * content.
         * @param {object} options Options to be passed to the initialize method.
         * @returns {object} An instance of an {@link popmenu}
         * controller (popmenu is built on top of $Popmenu).
         */
        function fromTemplate (templateString, options) {
            return $Modal.fromTemplate(templateString, ld.extend(options, POPMENU_OPTIONS));
        }
        /**
         * @name fromTemplateUrl
         * @param {string} templateUrl The url to load the template from.
         * @param {object} options Options to be passed to the initialize method.
         * @returns {promise} A promise that will be resolved with an instance of
         * an {@link ionic.controller:popmenu} controller (popmenu is built on top of $Popmenu).
         */
        function fromTemplateUrl (url, options) {
            return $Modal.fromTemplateUrl(url, ld.extend(options, POPMENU_OPTIONS));
        }

        /**
         * @name popmenu#show
         * @description Show this popmenu instance.
         * @param {$event} $event The $event or target element which the popmenu should align
         * itself next to.
         * @returns {promise} A promise which is resolved when the popmenu is finished animating in.
         */

        /**
         * @name popmenu#hide
         * @description Hide this popmenu instance.
         * @returns {promise} A promise which is resolved when the popmenu is finished animating out.
         */

        /**
         * @name popmenu#remove
         * @description Remove this popmenu instance from the DOM and clean up.
         * @returns {promise} A promise which is resolved when the popmenu is finished animating out.
         */

        /**
         * @name popmenu#isShown
         * @returns boolean Whether this popmenu is currently shown.
         */


        // Private
        function positionView (target, popmenuEle) {
            var targetEle = $$(target.target || target),
                buttonOffset = $Position.offset(targetEle),
                popmenuWidth = popmenuEle.prop("offsetWidth"),
                popmenuHeight = popmenuEle.prop("offsetHeight"),
                // Use innerWidth and innerHeight, because clientWidth and clientHeight
                // doesn't work consistently for body on all platforms
                bodyWidth = $window.innerWidth,
                bodyHeight = $window.innerHeight,
                popmenuCSS = {
                    left: buttonOffset.left + buttonOffset.width / 2 - popmenuWidth / 2
                },
                arrowEle = $$(popmenuEle[0].querySelector(".popover-arrow"));

            if (popmenuCSS.left < POPMENU_BODY_PADDING) {
                popmenuCSS.left = POPMENU_BODY_PADDING;
            } else if (popmenuCSS.left + popmenuWidth + POPMENU_BODY_PADDING > bodyWidth) {
                popmenuCSS.left = bodyWidth - popmenuWidth - POPMENU_BODY_PADDING;
            }

            // If the popmenu when popped down stretches past bottom of screen,
            // make it pop up if there's room above
            if (buttonOffset.top + buttonOffset.height + popmenuHeight > bodyHeight &&
                buttonOffset.top - popmenuHeight > 0) {
                popmenuCSS.top = buttonOffset.top - popmenuHeight;
                popmenuEle.addClass("popover-bottom");
            } else {
                popmenuCSS.top = buttonOffset.top + buttonOffset.height;
                popmenuEle.removeClass("popover-bottom");
            }

            arrowEle.css({
                left: buttonOffset.left + buttonOffset.width / 2 -
                    arrowEle.prop("offsetWidth") / 2 - popmenuCSS.left + "px"
            });

            popmenuEle.css({
                top: popmenuCSS.top + "px",
                left: popmenuCSS.left + "px",
                marginLeft: "0",
                opacity: "1"
            });

        }

    }

    // Exports
    module.exports = popmenuService;

})();
