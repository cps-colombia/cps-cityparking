/* global module */

// core/ui/popmenu/popmenuController.js
//
// This controller acts as an intermediary
// between directive and services
// for ui popmenu module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    PopmenuController.$inject = ["$scope", "$rootScope", "$Popmenu"];
    function PopmenuController ($scope, $rootScope, $Popmenu) {
        var popmenu = this;

        // Membres
        /**
         * Delegate function to $Popmenu service
         */
        popmenu.show = $Popmenu.show;
        popmenu.hide = $Popmenu.hide;

    }

    // Exports
    module.exports = PopmenuController;

})();
