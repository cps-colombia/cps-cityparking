/* global module */

// core/ui/popmenu/popmenuDirective.js
//
// directives function for ui popmenu module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    cpsPopmenu.$inject = [
        "$Bind",
        "$Backdrop",
        "$Logger",
        "$Popmenu"
    ];
    function cpsPopmenu ($Bind, $Backdrop, $Logger, $Popmenu) {
        var logger = $Logger.getInstance(),
            popmenuDirective = {
                restrict: "E",
                transclude: true,
                replace: true,
                controller: [ld.noop],
                scope: true,
                template: "<div class='popover-backdrop'>" +
                    "<div class='popover-wrapper'>" +
                    "<cps-popmenu-view>" +
                    "<div class='popover-content' ng-transclude></div>" +
                    "</cps-popmenu-view>" +
                    "</div>" +
                    "</div>",
                compile: compile
            };

        return popmenuDirective;

        // Functions
        function compile (tElement, tAttrs) {
            tElement.addClass("hide");

            return {
                pre: preLink,
                post: postLink
            };
        }
        function preLink (scope, element, attrs) {
            $Bind(scope, attrs,  {
                id: "@",
                popmenuCompile: "@",
                onClose: "&?"
            });
        }
        function postLink (scope, element, attrs, ctrl) {
            if(!scope.popmenuCompile) {
                if(!scope.id) {
                    scope.id = "modal" + scope.$id;
                    logger.warn("popmenu", "Need option with id Popmenu");
                }

                $Popmenu.fromTemplate(null, {
                    scope: scope,
                    element: element
                }).then(function (newPopmenu) {
                    scope.$popmenu = newPopmenu;
                });
            }

            // close popmenu if onTopOpen change to false
            scope.$watch(function () {
                return $Backdrop.isOpen({all: true}).length;
            }, function (newVal, oldVal) {
                if(newVal > 1 && scope.$popmenu && scope.$popmenu.isShown()) {
                    scope.$popmenu.hide();
                }
            });

            scope.$on("$routeChangeSuccess", function (event, next) {
                scope.$popmenu.hide();
            });

            // Clean prevent memory leaks
            element.on("$destroy", function () {
                scope.$destroy();
                if(scope.$popmenu && scope.$popmenu.remove) {
                    scope.$popmenu.remove();
                }
            });

        }
    }
    function cpsPopmenuView () {
        var popmenuViewDirective = {
            restrict: "E",
            compile: function (element) {
                element.append($$("<div class='popover-arrow'>"));
                element.addClass("popover popmenu");
            }
        };

        return popmenuViewDirective;
    }

    cpsPopmenuControl.$inject = ["$rootScope", "$Logger", "$Modal"];
    function cpsPopmenuControl ($rootScope, $Logger, $Modal) {
        var logger = $Logger.getInstance(),
            popmenuControlDirective = {
                restrict: "A",
                scope: {
                    cpsPopmenuControl: "@"
                },
                controller: "PopmenuController",
                controllerAs: "popmenu",
                link: postLink
            };

        return popmenuControlDirective;

        // Functions
        function postLink (scope, element, attrs, ctl) {
            var popmenuControl = scope.cpsPopmenuControl ? scope.cpsPopmenuControl.split("|") : [],
                popmenuAction = {
                    show: "show",
                    hide: "hide"
                }[popmenuControl[1] || "show"],
                popmenuId = popmenuControl[0],
                popmenu;

            element.on("tap touchstart click", function (e) {
                e.preventDefault();
                popmenu = ld.find($Modal.getModals(), {id: popmenuId});
                if(angular.isDefined(popmenuAction)) {
                    if(popmenu && popmenu[popmenuAction]) {
                        var callback = function () {
                            popmenu[popmenuAction](e);
                        };
                        scope.$apply(callback);
                    } else {
                        logger.warn("popmenu", "popmenu-id is required with id popmenu");
                    }
                } else {
                    logger.warn("popmenu", "cps-popmenu-control need valid action, 'show' or 'hide' ");
                }
            });
        }
    }

    // Exports
    module.exports = {
        popmenu: cpsPopmenu,
        popmenuView: cpsPopmenuView,
        popmenuControl: cpsPopmenuControl
    };

})();
