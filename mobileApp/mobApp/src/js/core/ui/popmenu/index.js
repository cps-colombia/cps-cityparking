/* global require */

// core/ui/popmenu/index.js
//
// index function popmenu ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        popmenuDirective = require("./popmenuDirective"),
        popmenuController = require("./popmenuController"),
        popmenuService = require("./popmenuService");

    angular.module("cpsPopmenu", []);
    var cpsPopmenu = angular.module("cpsPopmenu");

    // Service
    cpsPopmenu.factory("$Popmenu", popmenuService);

    // Directive
    cpsPopmenu.directive("cpsPopmenu", popmenuDirective.popmenu);
    cpsPopmenu.directive("cpsPopmenuView", popmenuDirective.popmenuView);
    cpsPopmenu.directive("cpsPopmenuControl", popmenuDirective.popmenuControl);


    // Controller
    cpsPopmenu.controller("PopmenuController", popmenuController);

})();
