/* global module */

// core/ui/modal/modalService.js
//
// provide service for ui modal module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    modalService.$inject = [
        "$window",
        "$rootScope",
        "$timeout",
        "$q",
        "$compile",
        "$Dom",
        "$Logger",
        "$Hardware",
        "$Template",
        "$Body",
        "$Backdrop"
    ];
    function modalService ($window, $rootScope, $timeout, $q, $compile, $Dom,
                          $Logger, $Hardware, $Template, $Body, $Backdrop) {
        var logger = $Logger.getInstance(),
            modalStack = [];

        // Members
        var Modal = {
            fromTemplate: fromTemplate,
            fromTemplateUrl: fromTemplateUrl,
            show: show,
            hide: hide,
            isShown: isShown,
            getModals: getModals
        };


        // Prototype
        /**
         * @name ModalView
         * @constructor
         * @description Creates a new modal controller instance.
         * @param {object} options An options object with the following properties:
         *  - `{object=}` `scope` The scope to be a child of.
         *    Default: creates a child of $rootScope.
         *  - `{string=}` `animation` The animation to show & hide with.
         *    Default: "slide-in-up"
         *  - `{boolean=}` `focusFirstInput` Whether to autofocus the first input of
         *    the modal when shown. Will only show the keyboard on iOS, to force the keyboard to show
         *    on Android, please use the #keyboardshow.
         *    Default: false.
         *  - `{boolean=}` `backdropClickToClose` Whether to close the modal on clicking the backdrop.
         *    Default: true.
         *  - `{boolean=}` `hardwareBackButtonClose` Whether the modal can be closed using the hardware
         *    back button on Android and similar devices.  Default: true.
         */
        var ModalView = function (opts) {
            this.focusFirstInput = false;
            this.unfocusOnHide = true;
            this.focusFirstDelay = 600;
            this.backdropClickToClose = true;
            this.hardwareBackButtonClose = true;
            this.animation = opts.animation || "slide-in-up";

            angular.extend(this, opts);

            return this;
        };

        /**
         * @name show
         * @description Show this modal instance.
         *
         * @return {promise} A promise which is resolved when the modal is finished animating in.
         */
        ModalView.prototype.show = function (target, opts) {
            var self = this,
                modalEl = $$(self.modalEl),
                scrollCtrl = modalEl.data("$ScrollController"),
                input = self.el.querySelector("input, textarea");

            if (self._isShown || self._removed) {
                return $q.when();
            }

            if (self.scope.$$destroyed) {
                logger.error("Cannot call " + self.viewType + ".show() after remove(). Please create a new " +
                             self.viewType + " instance.");
                return $q.when();
            }
            // on iOS, clicks will sometimes bleed through/ghost click on underlying
            // elements
            // TODO: $clickBlock.show(600);
            stackShow(self);

            self.el.classList.remove("hide");
            $timeout(function () {
                if (!self._isShown) {
                    return;
                }
                $Body.addClass(self.viewType + "-open");
            }, 400, false);

            if (!self.el.parentElement || self.element) {
                modalEl.addClass(self.animation);
                $Body.append(self.el);
            }

            // if modal was closed while the keyboard was up, reset scroll view on
            // next show since we can only resize it once it's visible
            if(scrollCtrl) {
                scrollCtrl.resize();
            }

            if (target && self.positionView) {
                self.positionView(target, modalEl);
                // set up a listener for in case the window size changes
                self._onWindowResize = function () {
                    if (self._isShown) {
                        self.positionView(target, modalEl);
                    }
                };
                $$($window).on("resize", self._onWindowResize);
            }

            modalEl.addClass("ng-enter active").removeClass("ng-leave ng-leave-active");

            self._isShown = true;
            $Backdrop.setOpen(self);

            self._deregisterBackButton = $Hardware.registerBackButtonAction(
                self.hardwareBackButtonClose ? self.hide.bind(this) : angular.noop,
                $Hardware.BACK_PRIORITY.modal
            );

            if(self.focusFirstInput) {
                // Let any animations run first
                $timeout(function () {
                    input = self.el.querySelector("input, textarea");
                    if(input) {
                        if(input.focus) {
                            input.focus();
                        }
                    }
                }, self.focusFirstDelay);
            }

            $timeout(function () {
                if (!self._isShown) {
                    return;
                }
                modalEl.addClass("ng-enter-active");
                $$($window).triggerHandler("resize");
                if(self.scope.$parent) {
                    self.scope.$parent.$broadcast(self.viewType + ".shown", self); // DEPRECATED
                    self.scope.$parent.$broadcast("cps:" + self.viewType + ":shown", self);
                }

                self.el.classList.add("active");

                if(self.scope.$parent) {
                    self.scope.$parent.$broadcast("cps:header:align");
                    self.scope.$parent.$broadcast("cps:footer:align");
                } else {
                    self.scope.$broadcast("cps:header:align");
                    self.scope.$broadcast("cps:footer:align");
                }
            }, 20);


            return $timeout(function () {
                if (!self._isShown) {
                    return;
                }
                self.$el.on("touchmove", function (e) {
                    // Don't allow scrolling while open by dragging on backdrop
                    var isInScroll = $Dom.getParentOrSelfWithClass(e.target, "scroll");
                    if (!isInScroll) {
                        e.preventDefault();
                    }
                });
                // After animating in, allow hide on backdrop click
                $$(self.$el).on("click", function (e) {
                    if (self.backdropClickToClose && e.target === self.el && stackIsHighest(self)) {
                        if(self.removeOnHide) {
                            self.remove();
                        } else {
                            self.hide();
                        }
                    }
                });
                // After open fire onShow function
                if(angular.isFunction(self.scope.$onShow)) {
                    $timeout(function () {
                        if (!self._isShown) {
                            return;
                        }
                        self.scope.$onShow();
                    }, 20);
                }
            }, 400);
        };

        /**
         * @name hide
         * @description Hide this modal instance.
         *
         * @return {promise} A promise which is resolved when the modal is finished animating out.
         */
        ModalView.prototype.hide = function () {
            var self = this,
                modalEl = $$(self.modalEl),
                inputs = self.el.querySelectorAll("input, textarea");

            // on iOS, clicks will sometimes bleed through/ghost click on underlying
            // elements
            // TODO: $clickBlock.show(600);
            stackHide(self);

            self.el.classList.remove("active");
            modalEl.addClass("ng-leave");

            $timeout(function () {
                if (self._isShown) {
                    return;
                }
                modalEl.addClass("ng-leave-active")
                    .removeClass("ng-enter ng-enter-active active");
            }, 20, false);

            self.$el.off("click");
            self._isShown = false;
            $Backdrop.setOpen(self);

            if(angular.isFunction(self.scope.$onClose)) {
                self.scope.$onClose();
            }
            if(self.scope.$parent) {
                self.scope.$parent.$broadcast(self.viewType + ".hidden", self); // DEPRECATED
                self.scope.$parent.$broadcast("cps:" + self.viewType + ":hidden", self);
            }

            if(self._deregisterBackButton) {
                self._deregisterBackButton();
            }

            if(self.unfocusOnHide) {
                inputs = this.el.querySelectorAll("input, textarea");
                // Let any animations run first
                $timeout(function () {
                    for(var i = 0; i < inputs.length; i++) {
                        if(inputs[i].blur) {
                            inputs[i].blur();
                        }
                    }
                });
            }

            // clean up event listeners
            if (self.positionView) {
                $$($window).off("resize", self._onWindowResize);
            }

            return $timeout(function () {
                $Body.removeClass(self.viewType + "-open");
                self.el.classList.add("hide");
            }, self.hideDelay || 320);
        };

        /**
         * @name remove
         * @description Remove this modal instance from the DOM and clean up.
         *
         * @return {promise} A promise which is resolved when the modal is finished animating out.
         */
        ModalView.prototype.remove = function () {
            var self = this;

            if(self._removed) {
                return $q.when(); // If removed return promise to avoid errors
            }
            self._removed = true;
            if(self.scope.$parent) {
                self.scope.$parent.$broadcast(self.viewType + ".removed", self); // DEPRECATED
                self.scope.$parent.$broadcast("cps:" + self.viewType + ":removed", self);
            }

            return self.hide(true).then(function () {
                self.scope.$destroy();
                self.$el.remove();
                stackRemove(self);
            });
        };

        /**
         * @name isShown
         *
         * @return {boolean} Whether this modal is currently shown.
         */
        ModalView.prototype.isShown = function () {
            return !!this._isShown;
        };

        return Modal;

        // Function
        /**
         * @name fromTemplate
         * @param {string} templateString The template string to use as the modal's
         * content.
         * @param {object} options Options to be passed method.
         * @return {promise}  with instance of an modal
         * controller.
         */
        function fromTemplate (templateString, options) {
            var deferred = $q.defer(),
                modal = createModal(templateString, options || {});
            if(modal) {
                deferred.resolve(modal);
            } else {
                deferred.reject(modal);
            }
            return deferred.promise;
        }
        /**
         * @name fromTemplateUrl
         * @param {string} templateUrl The url to load the template from.
         * @param {object} options Options to be passed method.
         * options object.
         * @return {promise} A promise that will be resolved with an instance of
         * an modal controller.
         */
        function fromTemplateUrl (url, options) {
            return $Template.load(url).then(function (templateString) {
                var modal = createModal(templateString, options || {});
                return modal;
            });
        }


        /**
         * @name show
         * @description Delegator to show modal instance
         * @param {string} modalId unique id for modal instance
         * @param {object} target when position modal
         * @param {object} options aditional options like, animation, title, closeBtn
         *
         * @return {promise}
         */
        function show (modalId, target, options) {
            var deferred = $q.defer(),
                modal,
                err;
            if(modalId) {
                modal = ld.find(modalStack, {id: modalId});
                if(modal && modal.show) {
                    modal.show(target, options);
                }
                deferred.resolve(modal);
            } else {
                err = "Id modal is required";
                logger.warn("modal", err);
                deferred.reject(err);
            }
            return deferred.promise;
        }

        /**
         * @name hide
         * @description Delegator to hide current modal instance
         * @param {string} attrs unique id for modal instance
         *
         * @return {promise}
         */
        function hide (attrs) {
            var deferred = $q.defer(),
                modalId = ld.isObject(attrs) ? attrs.id : attrs,
                options = ld.extend({all: false}, ld.isObject(attrs) ? attrs : {}),
                modal;

            if(options.all) {
                ld.forEach(modalStack, function (value, index) {
                    modal = value;
                    if(modal && modal.isShown && modal.hide) {
                        modal.hide();
                    }
                });
                deferred.resolve(modalStack);
            } else {
                modal = ld.find(modalStack, {id: modalId});
                if(modal && modal.hide) {
                    modal.hide();
                }
                deferred.resolve(modal);
            }
            return deferred.promise;
        }

        /**
         * @name isShown
         * @param {number/string} modalId
         *
         * @return {boolean} Whether any instance modal is currently shown or modal by id.
         */
        function isShown (modalId) {
            var modal = ld.find(modalStack, {id: modalId});
            return modal &&  modal.isShown && modal.isShown();
        }

        /**
         * @name getModals
         * @param {number/string} modalId
         *
         * @returns {array} Get all modals in stack or single modal by id.
         */
        function getModals (modalId) {
            if(modalId) {
                return ld.find(modalStack, {id: modalId});
            }
            return modalStack || [];
        }

        // Private
        function stackAdd (modal) {
            modalStack.push(modal);
        }
        function stackRemove (modal) {
            var index = modalStack.indexOf(modal);
            if (index > -1 && index < modalStack.length) {
                modalStack.splice(index, 1);
            }
        }
        function stackIsHighest (modal) {
            var index = modalStack.indexOf(modal);
            return (index > -1 && index === modalStack.length - 1);
        }
        function stackShow (modal) {
            var index = modalStack.indexOf(modal);
            if(index > -1) {
                ld.move(modalStack, index, (modalStack.length - 1));
            }
        }
        function stackHide (modal) {
            var index = modalStack.indexOf(modal);
            if(index > -1) {
                ld.move(modalStack, index, (modalStack.length - 2));
            }
        }
        // function remove(modalId) {
        //     var index = ld.findIndex(modalStack, {id: modalId});
        //     if(index > -1) {
        //         ld.pullAt(modalStack, index);
        //     }
        // }

        function createModal (templateString, options) {
            // Create a new scope for the modal
            var scope = options.scope && options.scope.$new() || $rootScope.$new(true),
                modalId = options.id || scope.id || "modal" + scope.$id,
                elementId = modalId ? "id='" + modalId + "'" : "",
                closeBtn = options.closeBtn === true ? "close-btn='true' " : "",
                modalTitle = options.title ? "modal-title='" + options.title + "' " : "",
                oldModal = getModals(modalId),
                onClose = "",
                extraOptions,
                element,
                modal;

            if(oldModal && !oldModal.scope.$$destroyed) {
                if(options.replace) {
                    if(oldModal.remove) {
                        oldModal.remove().then(function () {
                            // TODO: call create after remove
                        });
                    }
                } else {
                    return oldModal;
                }
            }

            scope.viewType = options.viewType = options.viewType || "modal";
            angular.extend(scope, {
                $hasHeader: false,
                $hasSubheader: false,
                $hasFooter: false,
                $hasSubfooter: false,
                $hasTabs: false,
                $hasTabsTop: false
            });

            if (options.onClose && !scope.$onClose) {
                // $onClose = "on-close='_onClose' ";
                scope.$onClose = function onClose () {
                    options.onClose();
                };
            }

            // Add custom class
            scope.cssClass = [];
            scope.cssClass.push(ld.kebabCase(modalId));

            if(ld.isString(options.cssClass)) {
                scope.cssClass.push(options.cssClass);
            }

            options.scope = scope;

            // Create modal
            modal = new ModalView(options);
            scope["$" + scope.viewType] = modal;

            // Compile the template
            extraOptions = closeBtn + modalTitle + onClose;
            if(options.element) {
                element = options.element;
            } else {
                element = $compile("<cps-" + scope.viewType + " " + extraOptions + "" +
                                   scope.viewType + "-compile='true' " +
                                   "ng-class='cssClass' " + elementId + ">" +
                                   templateString + "</cps-" + scope.viewType + ">")(scope);
            }

            modal.$el = element;
            modal.el = element[0];
            modal.modalEl = modal.el.querySelector("." + scope.viewType);
            modal.scope = scope;
            // Custom class
            element.addClass(options.class);

            // Set id
            if(modalId) {
                modal.id = modalId;
            } else {
                logger.warn(scope.viewType, scope.viewType + " id is required");
            }
            stackAdd(modal);

            return modal;
        }
    }
    // Exports
    module.exports = modalService;

})();
