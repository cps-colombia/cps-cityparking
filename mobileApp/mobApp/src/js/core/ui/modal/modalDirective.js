/* global module */

// core/ui/modal/modalDirective.js
//
// directives function for ui modal module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    /**
     * This directive contain all elements for modal windows
     * @usage
     * ```html
     * <cps-modal id="modalZonesList" modal-close-btn="true" modal-dialog-title='CPS'>
     *  <!-- Other html content -->
     * </cps-modal>
     * ```
     * @Options Attributes:
     * id: {string} id moda (required)
     * modal-title: {string} title window
     * modal-close-btn: {boolean} if show btn close
     * modal-on-close: {function} callback if close
     */
    cpsModal.$inject = [
        "$sce",
        "$rootScope",
        "$timeout",
        "$Logger",
        "$Body",
        "$Bind",
        "$Modal",
        "$Locale"
    ];
    function cpsModal ($sce, $rootScope, $timeout, $Logger, $Body, $Bind, $Modal, $Locale) {
        var logger = $Logger.getInstance(),
            modalDirective = {
                restrict: "E",
                transclude: true,
                replace: true,
                controller: "ModalController",
                controllerAs: "modal",
                scope: true,
                template: "<div class='modal-backdrop'>" +
                    "<div class='modal-backdrop-bg'></div>" +
                    "<div class='modal-wrapper'>" +
                    "<cps-modal-view>" +
                    "<cps-header class='bar-header-modal disable-user-behavior' " +
                    "ng-show='modalTitle && modalTitle.length || $closeBtn' " +
                    "ng-class='modalTitle ? \"bar-primary\" : \"bar-clear\"'>" +
                    "<button ng-if='$actionBtn' class='button button-icon icon' " +
                    "ng-class='actionClass || \"ion-search\"' hm-tap='$actionBtn()'></button>" +
                    "<h1 class='title' id='modalTitle' ng-bind='modalTitle' " +
                    "ng-show='modalTitle && modalTitle.length'></h1>" +
                    "<button ng-show='$closeBtn' class='button button-clear button-close' " +
                    "locale-id='close' hm-tap='$closeBtn()'>Close</button>" +
                    "</cps-header>" +
                    "<div class='general-content' ng-transclude></div>" +
                    "</cps-modal-view>" +
                    "</div>" +
                    "</div>",
                compile: compile
            };
        return modalDirective;

        // Functions
        function compile (tElement, tAttrs) {
            tElement.addClass("hide");

            return {
                pre: preLink,
                post: postLink
            };
        }
        function preLink (scope, element, attrs) {
            $Bind(scope, attrs, {
                id: "@",
                modalCompile: "@",
                modalTitle: "@",
                actionClass: "@actionClass",
                $closeBtn: "&?closeBtn",
                $actionBtn: "&?actionBtn",
                $onClose: "&?onClose",
                $onShow: "&?onShow"
            });
        }

        function postLink (scope, element, attrs, ctrl, transclude) {
            var transcludeEl = element.find("[ng-transclude]"),
                childrenEl = transcludeEl && transcludeEl.children();

            if(!scope.modalCompile) {
                if(!scope.id) {
                    scope.id = "modal" + scope.$id;
                    logger.warn("modal", "Need option with id modal");
                }

                $Modal.fromTemplate(null, {
                    scope: scope,
                    element: element
                }).then(function (newModal) {
                    scope.$modal = newModal;
                    // Modal title
                    if(scope.modalTitle) {
                        scope.modalTitleLocale = scope.modalTitle;
                        $$("#modalTitle").attr("locale-id", scope.modalTitleLocale);
                        scope.modalTitle = $Locale.get(scope.modalTitle, null, scope.modalTitle);
                    }

                });
            }
            // Scope watch
            scope.$watch(function () {
                return attrs.actionBtn;
            }, function (newVal) {
                if (!newVal) {
                    scope.$actionBtn = false;
                }
            });

            scope.$watch(function () {
                return attrs.closeBtn;
            }, function (newVal) {
                if (!newVal || newVal === "false") {
                    scope.$closeBtn = false;
                } else if (newVal === "true") {
                    scope.$closeBtn = (scope.$modal.removeOnHide ?
                                       scope.$modal.remove : scope.$modal.hide).bind(scope.$modal);
                }
            });

            scope.$watch(function () {
                return ld.get(scope, "$modal.actionBtn");
            }, function (newVal) {
                if (ld.isFunction(newVal)) {
                    scope.$actionBtn = newVal;
                }
            });

            scope.$watch(function () {
                return ld.get(scope, "$modal.actionClass");
            }, function (newVal) {
                scope.actionClass = newVal;
            });

            scope.$watch(function () {
                return ld.get(scope, "$modal.closeBtn");
            }, function (newVal) {
                if (ld.isFunction(newVal)) {
                    scope.$closeBtn = newVal;
                }
            });

            scope.$watch(function () {
                return scope.$hasHeader;
            }, function (newVal, oldVal) {
                if (newVal && scope.modalTitle && childrenEl && childrenEl.length) {
                    scope.$evalAsync(function () {
                        if(!childrenEl.first().hasClass("has-header")) {
                            transcludeEl.first().addClass("has-header");
                        }
                    });
                }
            });

            // Clean prevent memory leaks
            scope.$on("$destroy", function () {
                if(scope.$modal && scope.$modal.remove) {
                    scope.$modal.remove();
                }
            });

        }
    }

    /**
     * This directive control action for modal window, like close or open
     * @usage
     * ```html
     * <button cps-modal-control='modalZonesList|show'>Modal</\button>
     * ```
     * @Options Attributes:
     * cps-modal-control: {string} id|action, id is required, action to control, open, close (options)
     */
    modalControl.$inject = ["$parse", "$timeout", "$Logger", "$Modal", "$Gestures"];
    function modalControl ($parse, $timeout, $Logger, $Modal, $Gestures) {
        var logger = $Logger.getInstance(),
            controlDirective = {
                restrict: "A",
                scope: true,
                controller: "ModalController",
                controllerAs: "modal",
                link: postLink
            };

        return controlDirective;

        // Functions
        function postLink (scope, element, attrs, ctl) {
            var modalControl = attrs.cpsModalControl ? attrs.cpsModalControl.split("|") : [],
                allowAction = {
                    show: "show",
                    hide: "hide"
                }[modalControl[1] || "show"],
                modalId = modalControl[0],
                modal;

            if(attrs.openOnInit == "true") {
                $timeout(function () {
                    show();
                }, 600);
            }
            // Expose
            scope.show = show;
            scope.hide = hide;

            // Events
            $Gestures.hammer(element[0], "tap", function onTap (e) {
                e.preventDefault();
                modalAction();
            });
            // Functions
            function show () {
                modalAction("show");
            }
            function hide () {
                modalAction("hide");
            }
            // Private
            function modalAction (action) {
                action = action || allowAction;
                modal = ld.find($Modal.getModals(), {id: modalId});
                if(angular.isDefined(action)) {
                    if(modal && modal[action]) {
                        var callback = function () {
                            modal[action]();
                        };
                        scope.$apply(callback);
                    } else {
                        logger.warn("modal", "modal-id is required with id modal");
                    }
                } else {
                    logger.warn("modal", "cps-modal-control need valid action 'show' or 'hide'");
                }
            }
        }

    }

    /**
     * This directive add view container
     *
     */
    function modalView () {
        var view = {
            restrict: "E",
            compile: function (element, attr) {
                element.addClass("modal");
            }
        };
        return view;
    }

    // Exports
    module.exports = {
        modal: cpsModal,
        control: modalControl,
        view: modalView
    };

})();
