/* global require */

// core/ui/modal/index.js
//
// index function modal ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        modalDirective = require("./modalDirective"),
        modalController = require("./modalController"),
        modalService = require("./modalService");

    angular.module("cpsModal", []);
    var cpsModal = angular.module("cpsModal");

    // Service
    cpsModal.factory("$Modal", modalService);

    // Directive
    cpsModal.directive("cpsModal", modalDirective.modal);
    cpsModal.directive("cpsModalControl", modalDirective.control);
    cpsModal.directive("cpsModalView", modalDirective.view);

    // Controller
    cpsModal.controller("ModalController", modalController);


})();
