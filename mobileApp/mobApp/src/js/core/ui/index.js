/* global require */

// core/ui/index.js
//
// index function all modules of cps ui
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        constant = require("./constant"),
        backdropService = require("./utils/backdropService"),
        uiConfigService = require("./utils/uiConfigService"),
        bar = require("./bar"),
        view = require("./view"),
        loading = require("./loading"),
        modal = require("./modal"),
        filter = require("./filter"),
        popmenu = require("./popmenu"),
        dialog = require("./dialog"),
        menu = require("./menu"),
        list = require("./list"),
        tab = require("./tab"),
        scroll = require("./scroll"),
        slide = require("./slide"),
        collection = require("./collection"),
        gestures = require("./gestures");

    // "cpsGestures"
    angular.module("cpsUi", [
        "cpsBar", "cpsView", "cpsModal", "cpsFilter", "cpsList",
        "cpsTab", "cpsMenu", "cpsLoading", "cpsGestures", "cpsDialog",
        "cpsPopmenu", "cpsScroll", "cpsSlide"
    ]);

    var cpsUi = angular.module("cpsUi");

    // Service
    cpsUi.factory("$Backdrop", backdropService);
    cpsUi.service("$UiConfig", uiConfigService);

    // Constant webServices
    cpsUi.constant("BACK_PRIORITY", constant.priorityConstant());

    // Exports
    module.exports = {
        bar: bar,
        view: view,
        loading: loading,
        modal: modal,
        filter: filter,
        popmenu: popmenu,
        dialog: dialog,
        menu: menu,
        list: list,
        tab: tab,
        scroll: scroll,
        slide: slide,
        collection: collection,
        gestures: gestures
    };

})();
