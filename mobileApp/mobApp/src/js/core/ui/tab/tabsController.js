/* global module */

// core/ui/tab/tabsController.js
//
// controller for manage group of tab
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    TabsController.$inject = [
        "$scope",
        "$element",
        "$Mobile",
        "$View"
    ];
    function TabsController ($scope, $element, $Mobile, $View) {
        var self = this,
            selected = null,
            previousSelected = null,
            isVisible = true,
            selectedTabIndex;

        // Members
        self.tabs = [];
        self.selectedIndex = selectedIndex;
        self.selectedTab = selectedTab;
        self.previousSelectedTab = previousSelectedTab;
        self.add = add;
        self.remove = remove;
        self.deselect = deselect;
        self.select = select;
        self.hasActiveScope = hasActiveScope;
        self.showBar = showBar;

        // Functions
        function selectedIndex () {
            return self.tabs.indexOf(selected);
        }

        function selectedTab () {
            return selected;
        }

        function previousSelectedTab () {
            return previousSelected;
        }

        function add (tab) {
            // $History.registerHistory(tab);
            self.tabs.push(tab);
        }

        function remove (tab) {
            var tabIndex = self.tabs.indexOf(tab),
                newTabIndex;
            if (tabIndex === -1) {
                return;
            }
            // Use a field like '$tabSelected' so developers won't accidentally set it in controllers etc
            if (tab.$tabSelected) {
                self.deselect(tab);
                // Try to select a new tab if we're removing a tab
                if (self.tabs.length !== 1) {
                    // Select previous tab if it's the last tab, else select next tab
                    newTabIndex = tabIndex === self.tabs.length - 1 ? tabIndex - 1 : tabIndex + 1;
                    self.select(self.tabs[newTabIndex]);
                }
            }
            self.tabs.splice(tabIndex, 1);
        }

        function deselect (tab) {
            if (tab.$tabSelected) {
                previousSelected = selected;
                selected = selectedTabIndex = null;
                tab.$tabSelected = false;
                (tab.onDeselect || angular.noop)();
                if(tab.$broadcast) {
                    tab.$broadcast("cps:history:deselect");
                }
            }
        }

        function select (tab, shouldEmitEvent) {
            var tabIndex;
            if (angular.isNumber(tab)) {
                tabIndex = tab;
                if (tabIndex >= self.tabs.length) {
                    return;
                }
                tab = self.tabs[tabIndex];
            } else {
                tabIndex = self.tabs.indexOf(tab);
            }

            if(!tab.href) {
                return;
            }

            if (arguments.length === 1) {
                shouldEmitEvent = !!(tab.href);
            }

            if (selected && selected.href == tab.href) {
                if (shouldEmitEvent) {
                    $Mobile.pageChange(tab.href);
                }

            } else if (selectedTabIndex !== tabIndex) {
                angular.forEach(self.tabs, function (tab) {
                    self.deselect(tab);
                });

                selected = tab;
                selectedTabIndex = tabIndex;

                // Use a funny name like $tabSelected so the developer doesn't overwrite the var in a child scope
                tab.$tabSelected = true;
                (tab.onSelect || angular.noop)();

                if (shouldEmitEvent) {
                    $scope.$emit("cps:history:change", {
                        type: "tab",
                        tabIndex: tabIndex,
                        title: tab.title,
                        url: tab.href,
                        href: tab.href
                    });
                    $Mobile.pageChange(tab.href);
                }

                $scope.$broadcast("tabSelected", { selected: tab, selectedTabIndex: tabIndex});
            }
        }

        function hasActiveScope () {
            for (var x = 0; x < self.tabs.length; x++) {
                if ($View.isActiveScope(self.tabs[x])) {
                    return true;
                }
            }
            return false;
        }

        function showBar (show) {
            if (arguments.length) {
                if (show) {
                    $element.removeClass("tabs-item-hide");
                } else {
                    $element.addClass("tabs-item-hide");
                }
                isVisible = !!show;
            }
            return isVisible;
        }
    }

    // Exports
    module.exports = TabsController;
})();
