/* global require */

// core/ui/tab/index.js
//
// index function tab ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        tabDirective = require("./tabDirective"),
        tabController = require("./tabController"),
        tabsController = require("./tabsController"),
        tabService = require("./tabService");

    angular.module("cpsTab", []);
    var cpsTab = angular.module("cpsTab");

    // Service
    cpsTab.factory("$Tab", tabService);

    // Directive
    cpsTab.directive("cpsTabGroup", tabDirective.tabGroup)
        .directive("cpsTab", tabDirective.tab)
        .directive("cpsTabNav", tabDirective.tabNav);


    // Controller
    cpsTab.controller("TabController", tabController).
        controller("TabsController", tabsController);


})();
