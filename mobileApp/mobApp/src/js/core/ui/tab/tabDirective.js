/* global module */

// core/ui/tab/tabsDirective.js
//
// diferent directives for tab module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";
    var angular = require("angular"),
        $$ = angular.element;

    tabGroup.$inject = ["$Tab", "$UiConfig", "$Bind", "$location"];
    function tabGroup ($Tab, $UiConfig, $Bind, $location) {
        var tabGroupDirective = {
            restrict: "E",
            controller: "TabsController",
            compile: compile
        };
        return tabGroupDirective;

        // Functions
        function compile (tElement) {
            // We cannot use regular transclude here because it breaks element.data()
            // inheritance on compile
            var innerElement = $$("<div class='tab-nav tabs'>");
            innerElement.append(tElement.contents());

            tElement.append(innerElement)
                .addClass("tabs-" + $UiConfig.tabs.position() + " tabs-" + $UiConfig.tabs.style());

            return { pre: prelink, post: postLink };
        }
        function prelink (scope, element, attrs, tabsCtrl) {
            var parentScope = scope.$parent || scope,
                deregisterInstance = $Tab._registerInstance(
                    tabsCtrl, attrs.delegateHandle, tabsCtrl.hasActiveScope
                );


            $Bind(scope, attrs, {
                showBar: "=",
                selectFirst: "="
            });

            tabsCtrl.$scope = scope;
            tabsCtrl.$element = element;
            tabsCtrl.$tabsElement = $$(element[0].querySelector(".tabs"));

            scope.$watch(function () {
                return element[0].className;
            }, function (value) {
                var isTabsTop = value.indexOf("tabs-top") !== -1,
                    isHidden = value.indexOf("tabs-item-hide") !== -1;

                parentScope.$hasTabs = !isTabsTop && !isHidden;
                parentScope.$hasTabsTop = isTabsTop && !isHidden;
                parentScope.$emit("cps:tabs:top", parentScope.$hasTabsTop);
            });

            if(angular.isDefined(attrs.showBar)) {
                scope.$watch("showBar", function (newVal, oldVal) {
                    $Tab.showBar(newVal);
                });
            }

            scope.$on("cps:navView:beforeLeave", emitLifecycleEvent);
            scope.$on("cps:navView:afterLeave", emitLifecycleEvent);
            scope.$on("cps:navView:leave", emitLifecycleEvent);

            scope.$on("$destroy", function () {
                // variable to inform child tabs that they're all being blown away
                // used so that while destorying an individual tab, each one
                // doesn't select the next tab as the active one, which causes unnecessary
                // loading of tab views when each will eventually all go away anyway
                scope.$tabsDestroy = true;
                deregisterInstance();
                tabsCtrl.$tabsElement = tabsCtrl.$element = tabsCtrl.$scope = null;
                delete scope.$hasTabs;
                delete scope.$hasTabsTop;
            });

            function emitLifecycleEvent (ev, data) {
                ev.stopPropagation();
                var previousSelectedTab = tabsCtrl.previousSelectedTab();
                if (previousSelectedTab) {
                    previousSelectedTab.$broadcast(ev.name.replace("navView", "tabs"), data);
                }
            }
        }

        function postLink (scope, element, attrs, tabsCtrl) {
            if (!tabsCtrl.selectedTab()) {
                // all the tabs have been added
                // but one hasn't been selected yet
                if($Tab.showBar() && scope.selectFirst) {
                    tabsCtrl.select(0);
                }
            }
        }
    }

    tab.$inject = ["$compile", "$UiConfig", "$Bind", "$View", "$location"];
    function tab ($compile, $UiConfig, $Bind, $View, $location) {
        var tabDirective =  {
            restrict: "E",
            require: ["^cpsTabGroup", "cpsTab"],
            controller: "TabController",
            scope: true,
            compile: compile
        };
        return tabDirective;

        // Functions
        function compile (tElement, tAttrs) {
            // We create the tabNavTemplate in the compile phase so that the
            // attributes we pass down won"t be interpolated yet - we want
            // to pass down the "raw" versions of the attributes
            var tabNavTemplate = "<cps-tab-nav" +
                    attrStr("ng-click", tAttrs.ngClick) +
                    attrStr("title", tAttrs.title) +
                    attrStr("locale", tAttrs.localeId) +
                    attrStr("icon", tAttrs.icon) +
                    attrStr("icon-on", tAttrs.iconOn) +
                    attrStr("icon-off", tAttrs.iconOff) +
                    attrStr("badge", tAttrs.badge) +
                    attrStr("badge-style", tAttrs.badgeStyle) +
                    attrStr("hidden", tAttrs.hidden) +
                    attrStr("disabled", tAttrs.disabled) +
                    attrStr("class", tAttrs["class"]) +
                    "></cps-tab-nav>",
                tabContentEle = document.createElement("div"),
                childElementCount;

            // Remove the contents of the element so we can compile them later, if tab is selected
            for (var x = 0; x < tElement[0].children.length; x++) {
                tabContentEle.appendChild(tElement[0].children[x].cloneNode(true));
            }
            childElementCount = tabContentEle.childElementCount;
            tElement.empty();

            if (childElementCount) {
                if (childElementCount === 1) {
                    // make the 1 child element the primary tab content container
                    tabContentEle = tabContentEle.children[0];
                }
                tabContentEle.classList.add("tab-content");
            }

            return function link (scope, element, attrs, ctrls) {
                var tabsCtrl = ctrls[0],
                    tabCtrl = ctrls[1],
                    tabNavElement = $$(tabNavTemplate),
                    isTabContentAttached = false,
                    childScope,
                    childElement;
                scope.$tabSelected = false;

                $Bind(scope, attrs, {
                    onSelect: "&",
                    onDeselect: "&",
                    title: "@",
                    href: "@"
                });

                tabsCtrl.add(scope);

                // Events
                scope.$on("$destroy", function () {
                    if (!scope.$tabsDestroy) {
                        // if the containing cpsTabs directive is being destroyed
                        // then don"t bother going through the controllers remove
                        // method, since remove will reset the active tab as each tab
                        // is being destroyed, causing unnecessary view loads and transitions
                        tabsCtrl.remove(scope);
                    }
                    tabNavElement.isolateScope().$destroy();
                    tabNavElement.remove();
                    tabNavElement = tabContentEle = childElement = null;
                });

                scope.$watch("$tabSelected", tabSelected);

                scope.$on("cps:view:afterEnter", function () {
                    $View.viewEleIsActive(childElement, scope.$tabSelected);
                });

                scope.$on("cps:view:clearCache", function () {
                    if (!scope.$tabSelected) {
                        destroyTab();
                    }
                });
                scope.$on("$routeChangeSuccess", selectIfMatchesState);

                // Remove title attribute so browser-tooltip does not apear
                element[0].removeAttribute("title");
                element[0].removeAttribute("locale-id");
                selectIfMatchesState();

                // Sent controllers and compile
                tabNavElement.data("TabsController", tabsCtrl);
                tabNavElement.data("TabController", tabCtrl);
                tabsCtrl.$tabsElement.append($compile(tabNavElement)(scope));


                // Internal
                function selectIfMatchesState () {
                    if (tabCtrl.tabMatchesState()) {
                        tabsCtrl.select(scope, false);
                    } else {
                        tabsCtrl.deselect(scope);
                        tabCtrl.styleTab();
                    }
                }

                function tabSelected (isSelected) {
                    if (isSelected && childElementCount) {
                        // this tab is being selected

                        // check if the tab is already in the DOM
                        // only do this if the tab has child elements
                        if (!isTabContentAttached) {
                            // tab should be selected and is NOT in the DOM
                            // create a new scope and append it
                            childScope = scope.$new();
                            childElement = $$(tabContentEle);
                            $View.viewEleIsActive(childElement, true);
                            tabsCtrl.$element.append(childElement);
                            $compile(childElement)(childScope);
                            isTabContentAttached = true;
                        }

                        // remove the hide class so the tabs content shows up
                        $View.viewEleIsActive(childElement, true);

                    } else if (isTabContentAttached && childElement) {
                        // this tab should NOT be selected, and it is already in the DOM

                        if ($UiConfig.views.maxCache() > 0) {
                            // keep the tabs in the DOM, only css hide it
                            $View.viewEleIsActive(childElement, false);

                        } else {
                            // do not keep tabs in the DOM
                            destroyTab();
                        }

                    }
                }

                function destroyTab () {
                    if(childScope) {
                        childScope.$destroy();
                    }
                    if(isTabContentAttached && childElement) {
                        childElement.remove();
                    }
                    tabContentEle.innerHTML = "";
                    isTabContentAttached = childScope = childElement = null;
                }
            };
        }
        function attrStr (k, v) {
            // Returns " key="value"" if value exists
            return angular.isDefined(v) ? " " + k + "=\"" + v + "\"" : "";
        }
    }

    tabNav.$inject = ["$Logger"];
    function tabNav ($Logger) {
        var logger = $Logger.getInstance(),
            tabNavDirective = {
                restrict: "E",
                replace: true,
                template:
                "<a ng-class='{\"has-badge\":badge, \"tab-hidden\":isHidden()}' " +
                    " ng-disabled='disabled()' class='tab-item'>" +
                    "<span class='badge {{badgeStyle}}' ng-if='badge'>{{badge}}</span>" +
                    "<i class='icon'></i>" +
                    "<span class='tab-title' ng-bind-html='title' locale-id='{{locale}}'></span>" +
                    "</a>",
                scope: {
                    title: "@",
                    locale: "@",
                    icon: "@",
                    iconOn: "@",
                    iconOff: "@",
                    badge: "=",
                    hidden: "@",
                    disabled: "&",
                    badgeStyle: "@",
                    class: "@"
                },
                link: postLink
            };
        return tabNavDirective;

        // Functions
        function postLink (scope, element, attrs, ctrls) {
            var tabsCtrl = element.data("TabsController"),
                tabCtrl = element.data("TabController");

            if(!tabsCtrl || !tabCtrl) {
                logger.warn("tab", "cpsTabNav Directive require cpsTabGroup and cpsTab directives");
                return;
            }

            // Remove title attribute so browser-tooltip does not apear
            element[0].removeAttribute("title");

            // Public functions
            scope.selectTab = function (e) {
                e.preventDefault();
                tabsCtrl.select(tabCtrl.$scope, true);
            };
            if (!attrs.ngClick) {
                element.on("click", function (event) {
                    scope.$apply(function () {
                        scope.selectTab(event);
                    });
                });
            }

            scope.isHidden = function () {
                if (attrs.hidden === "true" || attrs.hidden === true) {
                    return true;
                }
                return false;
            };

            scope.getIconOn = function () {
                return scope.iconOn || scope.icon;
            };
            scope.getIconOff = function () {
                return scope.iconOff || scope.icon;
            };

            scope.isTabActive = function () {
                return tabsCtrl.selectedTab() === tabCtrl.$scope;
            };

            scope.deactivateTab = function () {
                tabsCtrl.deselect(tabCtrl.$scope);
                styleTab();
            };

            // Private
            function styleTab () {
                // check if tab if active
                if (tabsCtrl.selectedTab() === tabCtrl.$scope) {
                    element.addClass("tab-item-active");
                    element.find("i").removeClass(scope.getIconOff());
                    element.find("i").addClass(scope.getIconOn());
                }
                else {
                    element.removeClass("tab-item-active");
                    element.find("i").removeClass(scope.getIconOn());
                    element.find("i").addClass(scope.getIconOff());
                }
            }

            // Events
            scope.$watch("icon", function (newVal, oldVal) {
                if(newVal !== oldVal) {
                    element.find("i").removeClass(oldVal);
                }
                styleTab();
            });

            scope.$watch("iconOff", function () {
                styleTab();
            });

            scope.$watch("iconOn", function (newVal, oldVal) {
                if(newVal !== oldVal) {
                    element.find("i").removeClass(oldVal);
                }
                styleTab();
            });

            scope.$on("tabSelected", styleTab);

            tabCtrl.styleTab = styleTab;
            styleTab();
        }
    }

    module.exports = {
        tabGroup: tabGroup,
        tab: tab,
        tabNav: tabNav
    };
})();
