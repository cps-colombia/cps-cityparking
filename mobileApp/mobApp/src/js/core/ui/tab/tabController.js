/* global module */

// core/ui/tab/tabController.js
//
// controller for individula tab
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";
    var ld = require("lodash");

    TabController.$inject = [
        "$scope",
        "$attrs",
        "$location"
    ];
    function TabController ($scope, $attrs, $location) {
        var tab = this;
        tab.$scope = $scope;

        // Members
        tab.styleTab = ld.noop;
        tab.hrefMatchesState = hrefMatchesState;
        tab.navNameMatchesState = navNameMatchesState;
        tab.tabMatchesState = tabMatchesState;

        // Functions
        // All of these exposed for testing
        function hrefMatchesState () {
            return $attrs.href && $location.path().indexOf(
                $attrs.href.replace(/^#/, "/").replace(/\/$/, "")
            ) === 0;
        }

        function navNameMatchesState () {
            // return this.navViewName && $History.isCurrentStateNavView(this.navViewName);
        }

        function tabMatchesState () {
            return hrefMatchesState() || navNameMatchesState();
        }
    }

    // Export
    module.exports = TabController;

})();
