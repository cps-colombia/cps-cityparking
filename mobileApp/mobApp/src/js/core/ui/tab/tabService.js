/* global module */

// core/ui/tab/tabService.js
//
// Delegate for controlling the {@link directive:ionTabs} directive.
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    tabService.$inject = ["$Delegate"];
    function tabService ($Delegate) {
        return $Delegate.register([
            /**
             * @ngdoc method
             * @name select
             * @description Select the tab matching the given index.
             *
             * @param {number} index Index of the tab to select.
             */
            "select",
            /**
             * @ngdoc method
             * @name selectedIndex
             * @returns `number` The index of the selected tab, or -1.
             */
            "selectedIndex",
            /**
             * @ngdoc method
             * @name showBar
             * @description
             * Set/get whether the {@link directive:cpsTabs} is shown
             * @param {boolean} show Whether to show the bar.
             * @returns {boolean} Whether the bar is shown.
             */
            "showBar"
            /**
             * @ngdoc method
             * @name $getByHandle
             * @param {string} handle
             * @returns `delegateInstance` A delegate instance that controls only the
             * {@link directive:cpsTabs} directives with `delegate-handle` matching
             * the given handle.
             *
             * Example: `$Tab.$getByHandle('my-handle').select(0);`
             */
        ]);
    }

    // Exports
    module.exports = tabService;

})();
