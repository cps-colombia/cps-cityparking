(function () {
    "use strict";

    var angular = require("angular"),
        $$ = angular.element;

    scroll.$inject = ["$controller", "$Bind"];
    function scroll ($controller, $Bind) {
        var scrollDirective = {
            restrict: "AE",
            scope: true,
            controller: angular.noop,
            compile: compile
        };
        return scrollDirective;

        // Functions
        function compile (tElement, tAttrs) {
            tElement.addClass("scroll-view ionic-scroll");

            // We cannot transclude here because it breaks element.data() inheritance on compile
            var innerElement = $$("<div class='scroll'></div>");
            innerElement.append(tElement.contents());
            tElement.append(innerElement);

            return { pre: prelink };

            function prelink (scope, element, attrs) {
                var scrollOptions = {},
                    scrollCtrl,
                    isPaging;

                $Bind(scope, attrs, {
                    direction: "@",
                    paging: "@",
                    $onScroll: "&onScroll",
                    scroll: "@",
                    scrollbarX: "@",
                    scrollbarY: "@",
                    zooming: "@",
                    minZoom: "@",
                    maxZoom: "@"
                });
                scope.direction = scope.direction || "y";

                if (angular.isDefined(attrs.padding)) {
                    scope.$watch(attrs.padding, function (newVal) {
                        innerElement.toggleClass("padding", !!newVal);
                    });
                }
                if (scope.$eval(scope.paging) === true) {
                    innerElement.addClass("scroll-paging");
                }

                if (!scope.direction) {
                    scope.direction = "y";
                }
                isPaging = scope.$eval(scope.paging) === true;

                // Set options
                scrollOptions = {
                    el: element[0],
                    delegateHandle: attrs.delegateHandle,
                    locking: (attrs.locking || "true") === "true",
                    bouncing: scope.$eval(attrs.hasBouncing),
                    paging: isPaging,
                    scrollbarX: scope.$eval(scope.scrollbarX) !== false,
                    scrollbarY: scope.$eval(scope.scrollbarY) !== false,
                    scrollingX: scope.direction.indexOf("x") >= 0,
                    scrollingY: scope.direction.indexOf("y") >= 0,
                    zooming: scope.$eval(scope.zooming) === true,
                    maxZoom: scope.$eval(scope.maxZoom) || 3,
                    minZoom: scope.$eval(scope.minZoom) || 0.5,
                    preventDefault: true
                };

                if (isPaging) {
                    scrollOptions.speedMultiplier = 0.8;
                    scrollOptions.bouncing = false;
                }

                scrollCtrl = $controller("ScrollController", {
                    $scope: scope,
                    scrollOptions: scrollOptions
                });
                scope.scrollCtrl = scrollCtrl;

                scope.$on("$destroy", function () {
                    if (scrollOptions) {
                        delete scrollOptions.el;
                    }
                    innerElement = null;
                    scrollCtrl = null;
                    element = null;
                    attrs.$$element = null;
                });
            }
        }
    }

    // Export
    module.exports = {
        scroll: scroll
    };

})();
