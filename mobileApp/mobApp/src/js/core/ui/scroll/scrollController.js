/* global module */

// core/ui/scroll/scrollController.js
//
// scroll ui instance
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    ScrollController.$inject = [
        "$q",
        "$rootScope",
        "$window",
        "$document",
        "$timeout",
        "$location",
        "$scope",
        "$App",
        "$ScrollLogic",
        "$Scroll",
        "scrollOptions"
    ];
    function ScrollController ($q, $rootScope, $window, $document, $timeout,
                              $location, $scope, $App, $ScrollLogic, $Scroll, scrollOptions) {
        var scroll = this,
            readyCallbacks = [],
            isReady = false,
            resizeBind = ld.noop,
            rePosition = ld.noop,
            deregisterInstance = ld.noop,
            element,
            $element,
            scrollView;

        // Members
        scroll.ready = ready;
        scroll.init = init;
        scroll.destroy = destroy;
        scroll.isNative = isNative;
        scroll.getScrollView = getScrollView;
        scroll.getScrollPosition = getScrollPosition;
        scroll.resize = resize;
        scroll.scrollTop = scrollTop;
        scroll.scrollBottom = scrollBottom;
        scroll.scrollTo = scrollTo;
        scroll.zoomTo = zoomTo;
        scroll.zoomBy = zoomBy;
        scroll.scrollBy = scrollBy;
        scroll.anchorScroll = anchorScroll;
        scroll.freezeScroll = ld.noop;
        scroll.freezeScrollShut = ld.noop;
        scroll.freezeAllScrolls = freezeAllScrolls;
        scroll._setRefresher = _setRefresher;

        // Events
        rePosition = $scope.$on("$routeChangeStart", function (event, next) {
            // reposition scroll to top if view change but if have global scroll
            scrollTop();
        });
        $scope.$on("$destroy", function () {
            deregisterInstance();
            rePosition();
            if(scrollView && scrollView.__cleanup) {
                scrollView.__cleanup();
            }
            $$($window).off("resize", resizeBind);
            if($element && $element.off) {
                $element.off("scroll", scrollFunc);
            }
            scrollView = scroll.scrollView = scrollOptions = $element = scroll.$element = element = null;
        });

        // Run
        init();

        // Functions
        function ready (cb) {
            // run task is scroll is ready
            if (isReady) {
                cb();
            } else {
                //  will be called once the scroll is ready
                readyCallbacks.push(cb);
            }
        }
        function init () {
            var deferred = $q.defer();

            element = scroll.element = scrollOptions.el;
            $element = scroll.$element = $$(element);

            if (isNative()) {
                // TODO: scrollNative service
                scrollView = scroll.scrollView = null; // $ScrollNative(scrollOptions);
            } else {
                scrollView = scroll.scrollView = $ScrollLogic().initialize(scrollOptions);
            }


            // Attach self to element as a controller so other directives can require this controller
            // through `require: '$Scroll'
            // Also attach to parent so that sibling elements can require this
            ($element.parent().length ? $element.parent() : $element)
                .data("$ScrollController", scroll);

            deregisterInstance = $Scroll._registerInstance(
                scroll, scrollOptions.delegateHandle, function () {
                    // TODO: history service
                    // return $History.isActiveScope($scope);
                    return true;
                }
            );

            // Extend functions
            if(scrollView && scrollView.freeze) {
                scroll.freezeScroll = scrollView.freeze;
            }
            if(scrollView && scrollView.freezeShut) {
                scroll.freezeScrollShut = scrollView.freezeShut;
            }

            if (!angular.isDefined(scrollOptions.bouncing)) {
                $App.ready(function () {
                    if (scrollView && scrollView.options) {
                        scrollView.options.bouncing = true;
                        if ($App.isAndroid()) {
                            // No bouncing by default on Android
                            scrollView.options.bouncing = false;
                            // Faster scroll decel
                            scrollView.options.deceleration = 0.95;
                        }
                    }
                });
            }

            resizeBind = (scrollView.resize || ld.noop).bind(scrollView);
            angular.element($window).on("resize", resizeBind);

            $element.on("scroll", scrollFunc);

            $timeout(function () {
                if(scrollView && scrollView.run) {
                    scrollView.run();
                    onScrollReady();
                    deferred.resolve(scrollView);
                }
            });

            return deferred.promise;
        }

        function destroy () {
            if($scope && $scope.$destroy) {
                $scope.$destroy();
            }
        }

        function isNative () {
            return scrollOptions && !!scrollOptions.nativeScrolling;
        }

        function getScrollView () {
            return scrollView;
        }

        function getScrollPosition () {
            return scrollView.getValues();
        }

        function resize () {
            return $timeout(resizeBind, 0, false).then(function () {
                if($element) {
                    $element.triggerHandler("scroll-resize");
                }
            });
        }

        function scrollTop (shouldAnimate) {
            resize().then(function () {
                if(scrollView && scrollView.scrollTo) {
                    scrollView.scrollTo(0, 0, !!shouldAnimate);
                }
            });
        }

        function scrollBottom (shouldAnimate) {
            resize().then(function () {
                var max = scrollView && scrollView.getScrollMax();
                if(scrollView && scrollView.scrollTo) {
                    scrollView.scrollTo(max.left, max.top, !!shouldAnimate);
                }
            });
        }

        function scrollTo (left, top, shouldAnimate) {
            resize().then(function () {
                if(scrollView && scrollView.scrollTo) {
                    scrollView.scrollTo(left, top, !!shouldAnimate);
                }
            });
        }

        function zoomTo (zoom, shouldAnimate, originLeft, originTop) {
            resize().then(function () {
                if(scrollView && scrollView.zoomTo) {
                    scrollView.zoomTo(zoom, !!shouldAnimate, originLeft, originTop);
                }
            });
        }

        function zoomBy (zoom, shouldAnimate, originLeft, originTop) {
            resize().then(function () {
                if(scrollView && scrollView.zoomBy) {
                    scrollView.zoomBy(zoom, !!shouldAnimate, originLeft, originTop);
                }
            });
        }

        function scrollBy (left, top, shouldAnimate) {
            resize().then(function () {
                if(scrollView && scrollView.scrollBy) {
                    scrollView.scrollBy(left, top, !!shouldAnimate);
                }
            });
        }

        function anchorScroll (shouldAnimate) {
            resize().then(function () {
                var hash = $location.hash(),
                    elm = hash && $document[0].getElementById(hash);

                if (!(hash && elm)) {
                    scrollView.scrollTo(0, 0, !!shouldAnimate);
                    return;
                }
                var curElm = elm,
                    leftScroll = 0,
                    topScroll = 0;
                do {
                    if (curElm !== null) {
                        leftScroll += curElm.offsetLeft;
                    }
                    if (curElm !== null) {
                        topScroll += curElm.offsetTop;
                    }
                    curElm = curElm.offsetParent;
                } while (curElm.attributes !== scroll.element.attributes && curElm.offsetParent);
                scrollTo(leftScroll, topScroll, !!shouldAnimate);
            });
        }

        function freezeAllScrolls (shouldFreeze) {
            for (var i = 0; i < $Scroll._instances.length; i++) {
                $Scroll._instances[i].freezeScroll(shouldFreeze);
            }
        }

        // Private
        function _setRefresher (refresherScope, refresherElement, refresherMethods) {
            scroll.refresher = refresherElement;
            var refresherHeight = scroll.refresher.clientHeight || 60;
            scrollView.activatePullToRefresh(
                refresherHeight,
                refresherMethods
            );
        }

        function scrollFunc (e) {
            var detail = (e.originalEvent || e).detail || {};
            if($scope.$onScroll) {
                $scope.$onScroll({
                    event: e,
                    scrollTop: detail.scrollTop || 0,
                    scrollLeft: detail.scrollLeft || 0
                });
            }
        }

        function onScrollReady () {
            isReady = true;
            for (var x = 0; x < readyCallbacks.length; x++) {
                // fire off all the callbacks that were added before
                readyCallbacks[x]();
            }
            readyCallbacks = [];
        }
    }

    // Exports
    module.exports = ScrollController;

})();
