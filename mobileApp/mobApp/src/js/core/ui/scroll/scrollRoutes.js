/* global module */

// core/ui/scrollRoutes.js
//
// Routes for cps scroll module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    scrollRoutes.$inject = ["$provide"];
    function scrollRoutes ($provide) {

        // Append custom behavior
        $provide.decorator("$location", ["$delegate", "$timeout", $LocationDecorator]);

        // Functions
        function $LocationDecorator ($location, $timeout) {

            $location.__hash = $location.hash;
            // Fix: when window.location.hash is set, the scrollable area
            // found nearest to body's scrollTop is set to scroll to an element
            // with that ID.
            $location.hash = function (value) {
                if (angular.isDefined(value) && value.length > 0) {
                    $timeout(function () {
                        var scroll = document.querySelector(".scroll-content");
                        if (scroll) {
                            scroll.scrollTop = 0;
                        }
                    }, 0, false);
                }
                return $location.__hash(value);
            };

            return $location;
        }
    }

    module.exports = scrollRoutes;

})();
