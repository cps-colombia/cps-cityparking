/* global require */

// core/ui/scroll/index.js
//
// index function scroll ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        scrollController = require("./scrollController"),
        scrollDirective = require("./scrollDirective"),
        scrollRoutes = require("./scrollRoutes"),
        scrollRun = require("./scrollRun"),
        scrollLogicService = require("./scrollLogicService"),
        scrollService = require("./scrollService");

    angular.module("cpsScroll", []);
    var cpsScroll = angular.module("cpsScroll");

    // Controller
    cpsScroll.controller("ScrollController", scrollController);

    // Service
    cpsScroll.factory("$ScrollLogic", scrollLogicService);
    cpsScroll.service("$Scroll", scrollService);

    // Routes
    cpsScroll.config(scrollRoutes);
    cpsScroll.run(scrollRun);

    // Directive
    cpsScroll.directive("cpsScroll", scrollDirective.scroll);

})();
