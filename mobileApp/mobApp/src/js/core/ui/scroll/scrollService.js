/* global module */

// core/ui/scroll/scrollService.js
//
// Delegate for controlling scrollViews (created by
// {@link directive:cpsContent} and
// {@link directive:cpsScroll} directives).
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    scrollService.$inject = ["$Delegate"];
    function scrollService ($Delegate) {
        return $Delegate.register([
            /**
             * @ngdoc method
             * @name resize
             * @description Tell the scrollView to recalculate the size of its container.
             */
            "resize",
            /**
             * @ngdoc method
             * @name scrollTop
             * @param {boolean=} shouldAnimate Whether the scroll should animate.
             */
            "scrollTop",
            /**
             * @ngdoc method
             * @name scrollBottom
             * @param {boolean=} shouldAnimate Whether the scroll should animate.
             */
            "scrollBottom",
            /**
             * @ngdoc method
             * @name scrollTo
             * @param {number} left The x-value to scroll to.
             * @param {number} top The y-value to scroll to.
             * @param {boolean=} shouldAnimate Whether the scroll should animate.
             */
            "scrollTo",
            /**
             * @ngdoc method
             * @name scrollBy
             * @param {number} left The x-offset to scroll by.
             * @param {number} top The y-offset to scroll by.
             * @param {boolean=} shouldAnimate Whether the scroll should animate.
             */
            "scrollBy",
            /**
             * @ngdoc method
             * @name zoomTo
             * @param {number} level Level to zoom to.
             * @param {boolean=} animate Whether to animate the zoom.
             * @param {number=} originLeft Zoom in at given left coordinate.
             * @param {number=} originTop Zoom in at given top coordinate.
             */
            "zoomTo",
            /**
             * @ngdoc method
             * @name zoomBy
             * @param {number} factor The factor to zoom by.
             * @param {boolean=} animate Whether to animate the zoom.
             * @param {number=} originLeft Zoom in at given left coordinate.
             * @param {number=} originTop Zoom in at given top coordinate.
             */
            "zoomBy",
            /**
             * @ngdoc method
             * @name getScrollPosition
             * @returns {object} The scroll position of this view, with the following properties:
             *  - `{number}` `left` The distance the user has scrolled from the left (starts at 0).
             *  - `{number}` `top` The distance the user has scrolled from the top (starts at 0).
             */
            "getScrollPosition",
            /**
             * @ngdoc method
             * @name anchorScroll
             * @description Tell the scrollView to scroll to the element with an id
             * matching window.location.hash.
             *
             * If no matching element is found, it will scroll to top.
             *
             * @param {boolean=} shouldAnimate Whether the scroll should animate.
             */
            "anchorScroll",
            /**
             * @ngdoc method
             * @name freezeScroll
             * @description Does not allow this scroll view to scroll either x or y.
             * @param {boolean=} shouldFreeze Should this scroll view be prevented from scrolling or not.
             * @returns {boolean} If the scroll view is being prevented from scrolling or not.
             */
            "freezeScroll",
            /**
             * @ngdoc method
             * @name freezeAllScrolls
             * @description Does not allow any of the app's scroll views to scroll either x or y.
             * @param {boolean=} shouldFreeze Should all app scrolls be prevented from scrolling or not.
             */
            "freezeAllScrolls",
            /**
             * @ngdoc method
             * @name getScrollView
             * @returns {object} The scrollView associated with this delegate.
             */
            "getScrollView"
            /**
             * @ngdoc method
             * @name $getByHandle
             * @param {string} handle
             * @returns `delegateInstance` A delegate instance that controls only the
             * scrollViews with `delegate-handle` matching the given handle.
             *
             * Example: `$Scroll.$getByHandle('my-handle').scrollTop();`
             */
        ]);
    }

    // Exports
    module.exports = scrollService;

})();
