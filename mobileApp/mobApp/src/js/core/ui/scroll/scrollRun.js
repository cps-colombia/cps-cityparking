/* global module */

// core/ui/scroll/scrollRun.js
//
// Run config for cps scroll module
//
// 2015, CPS - Cellular Parking Systems
(function () {
    "use strict" ;

    scrollRun.$inject = ["$rootScope", "$timeout"];
    function scrollRun ($rootScope, $timeout) {

        $rootScope.scroll = {};
        $rootScope.$on("$routeChangeStart", function (event, next) {
            $timeout(function () {
                var scroll = document.querySelector(".scroll-content");
                if (scroll) {
                    scroll.scrollTop = 0;
                }
            }, 0, false);
        });
    }

    // Exports
    module.exports = scrollRun;
})();
