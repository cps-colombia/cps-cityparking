/* global module */

// core/ui/list/listDirective.js
//
// directives function for ui list module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        isDefined = angular.isDefined,
        $$ = angular.element;

    var ITEM_TPL_CONTENT_ANCHOR = "<a class='item-content' ng-href='{{$href()}}' target='{{$target()}}'></a>",
        ITEM_TPL_CONTENT = "<div class='item-content'></div>";

    cpsList.$inject = [];
    function cpsList () {
        var listDirective = {
            template: "<div class='scroll-content'><ul class='scroll' ng-transclude></ul></div>",
            replace: true,
            transclude: true,
            restrict: "E",
            compile: function (tElement, tAttrs) {
                tElement.addClass("list");
            }
        };
        return listDirective;
    }

    function cpsItem () {
        var itemDirective = {
            restrict: "E",
            controller: ["$scope", "$element", function ($scope, $element) {
                this.scope = $scope;
                this.element = $element;
            }],
            template: "<li ng-transclude></li>",
            replace: true,
            transclude: true,
            scope: true,
            compile: function (tElement, tAttrs) {
                var isAnchor = isDefined(tAttrs.href) ||
                        isDefined(tAttrs.ngHref) ||
                        isDefined(tAttrs.uiSref),
                    isComplexItem = isAnchor ||
                        // Lame way of testing, but we have to know at compile what to do with the element
                        /cps-(delete|option|reorder)-button/i.test(tElement.html()),
                    innerElement = $$(isAnchor ? ITEM_TPL_CONTENT_ANCHOR : ITEM_TPL_CONTENT);

                innerElement.append(tElement.contents());
                tElement.append(innerElement);

                if (isComplexItem) {
                    tElement.addClass("item item-complex");
                } else {
                    tElement.addClass("item");
                }

            }
        };
        return itemDirective;
    }

    function cpsDeleteItem () {
        var ITEM_TPL_DELETE_BUTTON =
                "<div class='item-left-edit item-delete enable-pointer-events'>" +
                "</div>",
            deleteItemDirective = {
                restrict: "E",
                require: ["^^cpsItem", "^?cpsList"],
                // Run before anything else, so we can move it before other directives process
                // its location (eg ngIf relies on the location of the directive in the dom)
                priority: Number.MAX_VALUE,
                compile: compile
            };
        return deleteItemDirective;

        // Functions
        function compile (tElement, tAttrs) {
            // Add the classes we need during the compile phase, so that they stay
            // even if something else like ngIf removes the element and re-addss it
            tAttrs.$set("class", (tAttrs["class"] || "") + " button icon button-icon", true);

            return function (scope, element, attr, ctrls) {
                var itemCtrl = ctrls[0],
                    listCtrl = ctrls[1],
                    container = $$(ITEM_TPL_DELETE_BUTTON);

                container.append(element);
                if(itemCtrl) {
                    itemCtrl.$element.append(container).addClass("item-left-editable");
                }

                // Don't bubble click up to main .item
                element.on("click", stopPropagation);
                scope.$on("cps.reconnectScope", init);

                init();
                function init () {
                    listCtrl = listCtrl || element.controller("cpsList");
                    if (listCtrl && listCtrl.showDelete()) {
                        container.addClass("visible active");
                    }
                }
            };
        }

        function stopPropagation (ev) {
            ev.stopPropagation();
        }
    }

    // Exports
    module.exports = {
        list: cpsList,
        item: cpsItem,
        deleteItem: cpsDeleteItem
    };

})();
