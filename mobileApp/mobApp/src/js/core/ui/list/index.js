/* global require */

// core/ui/list/index.js
//
// index function list ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        listDirective = require("./listDirective");

    angular.module("cpsList", []);
    var cpsList = angular.module("cpsList");


    // Directive
    cpsList.directive("cpsList", listDirective.list);
    cpsList.directive("cpsItem", listDirective.item);
    cpsList.directive("cpsDeleteItem", listDirective.deleteItem);

})();
