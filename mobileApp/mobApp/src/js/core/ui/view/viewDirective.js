/* global module */

// core/ui/view/viewDirective.js
//
// drirectives for view ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;


    cpsPage.$inject = ["$rootScope", "$animate", "$location", "$Dom"];
    function cpsPage ($rootScope, $animate, $location, $Dom) {
        var pageDirective = {
            restrict: "E",
            scope: {
                section: "@"
            },
            priority: 1300,
            transclude: "element",
            multiElement: true,
            link: postLink
        };
        return pageDirective;

        // Function
        function postLink (scope, element, attrs, ctrl, $transclude) {
            var selectedElements = [],
                previousLeaveAnimations = [],
                selectedScopes = [];

            var spliceFactory = function (array, index) {
                return function () {
                    array.splice(index, 1);
                };
            };

            scope.$watch(function () {
                return $rootScope.$viewName;
            }, function cpsPageAction (viewName) {
                var block = {},
                    urlPath, selected,
                    promise, anchor, i, ii;

                for (i = 0, ii = previousLeaveAnimations.length; i < ii; ++i) {
                    $animate.cancel(previousLeaveAnimations[i]);
                }
                previousLeaveAnimations.length = 0;

                for (i = 0, ii = selectedScopes.length; i < ii; ++i) {
                    selected = $Dom.getBlockNodes(selectedElements[i].clone);
                    selectedScopes[i].$destroy();

                    promise = previousLeaveAnimations[i] = $animate.leave(selected);
                    promise.then(spliceFactory(previousLeaveAnimations, i));
                }

                selectedElements.length = 0;
                selectedScopes.length = 0;
                urlPath = $location.path() || "";

                if (ld.isUndefined(scope.section) || scope.section === viewName ||
                    scope.section === urlPath || ("/" + scope.section) === urlPath) {
                    $transclude(function (caseElement, selectedScope) {
                        selectedScopes.push(selectedScope);
                        anchor = element;

                        caseElement[caseElement.length++] = document.createComment(" end cpsPage: ");
                        block = { clone: caseElement };

                        selectedElements.push(block);
                        caseElement.addClass("page");
                        $animate.enter(caseElement, anchor.parent(), anchor);
                    });
                }
            });
        }
    }

    function cpsView () {
        var viewDirective = {
            restrict: "E",
            scope: true,
            compile: function (tElement, tAttr) {
                tElement.addClass("view");
            }
        };
        return viewDirective;
    }

    cpsViewContent.$inject = ["$animate", "$location", "$timeout", "$Dom"];
    function cpsViewContent ($animate, $location, $timeout, $Dom) {
        var viewDirective = {
            restrict: "E",
            controller: "ViewController",
            compile: compile
        };
        return viewDirective;

        // Functions
        function compile (tElement, tAttr) {
            tElement.addClass("view-container view-slide");
            return {
                post: postLink
            };
        }
        function postLink (scope, element, attrs) {
            var viewData = {},
                enter = false,
                leave = false,
                afterEnter = false,
                afterLeave = false; // TODO: set view data
            // TODO: implement history service

            // Enter
            scope.$emit("cps:view:loaded", viewData);

            scope.$on("$viewContentLoaded", function (e, data) {
                scope.$emit("cps:view:beforeEnter", viewData);
            });

            $animate.on("enter", element, function (data, phase) {
                if(phase === "start" && !enter) {
                    enter = true;
                    scope.$broadcast("cps:view:enter", viewData);
                    element.addClass("view-" + ld.kebabCase($location.path()));
                } else if(phase === "close" && !afterEnter) {
                    afterEnter = true;
                    scope.$broadcast("cps:view:afterEnter", viewData);
                }
            });

            $Dom.animationComplete(element[0]).then(function () {
                scope.$broadcast("cps:view:completeEnter", viewData);
            });

            // Leave
            scope.$on("$destroy", function () {
                scope.$broadcast("cps:view:unloaded", viewData);
            });

            $animate.on("leave", element, function (data, phase) {
                if(phase === "start" && !leave) {
                    leave = true;
                    scope.$broadcast("cps:view:beforeLeave", viewData);
                } else if(phase === "close" && !afterLeave) {
                    afterLeave = true;
                    scope.$broadcast("cps:view:afterLeave", viewData);
                    $timeout(function () {
                        scope.$broadcast("cps:view:completeLeave", viewData);
                    }, 100);
                }
            });

            element.on("$destroy", function () {
                scope.$broadcast("cps:view:leave", viewData);
            });
        }
    }

    cpsViewNav.$inject = ["$rootScope", "$timeout", "$Bind", "$View", "$Locale", "$Loading"];
    function cpsViewNav ($rootScope, $timeout, $Bind, $View, $Locale, $Loading) {
        var cpsViewNavDirective = {
            restrict: "E",
            template: "<div nav-bar-transition='ios'>" +
                "<h1 class='title title-center header-item hide' ng-class='titleClass' ng-bind='viewNav.title'>" +
                "</h1>" +
                "</div>",
            compile: compile
        };
        return cpsViewNavDirective;

        // Functions
        function compile (tElement, tAttrs) {
            tElement.addClass("view-nav");
            return {
                post: postLink
            };
        }
        function postLink (scope, element, attrs) {
            var titleEl = $$(element).find("h1"),
                onLocaleChange = ld.noop,
                asBar = attrs.asBar == "true",
                isShown,
                timeClean,
                changeTitle;

            // Create spinner
            $Loading.spinner({
                id: "viewNavSpinner",
                scope: scope,
                appendTo: element,
                default: true,
                startHidden: true
            });

            scope.titleClass = "";
            scope.viewNav = {
                title: "",
                locale: ""
            };
            scope.sectionLocale = "";

            // Watch change section
            changeTitle = scope.$watch(function () {
                return $rootScope.sectionTitle;
            }, function (newVal, oldVal) {
                if(newVal !== oldVal) {
                    scope.titleClass = "title-section";
                    scope.viewNav.locale = newVal;
                    $$(titleEl).attr("locale-id", scope.viewNav.locale);

                    scope.viewNav.title = $Locale.get(scope.viewNav.locale, null, newVal);
                    timeClean = $timeout(function () {
                        $$(titleEl).removeClass("hide");
                        scope.titleClass = "";
                    }, 1);
                }
            });

            if(asBar) {
                scope.$hasViewNav = true;
                element.addClass("bar");
            } else {
                element.removeClass("bar");
            }

            scope.$watch(function () {
                return element[0].className;
            }, function (value) {
                isShown = value.indexOf("ng-hide") === -1;
                scope.$hasViewNav = isShown && asBar;
            });

            onLocaleChange = scope.$on("cps:localeChange", function (lang) {
                scope.viewNav.title = $Locale.get(scope.viewNav.locale, null, scope.viewNav.title);
            });

            // Clean scope
            scope.$on("$destroy", function () {
                // Clean watch
                if(changeTitle) {
                    changeTitle();
                }
                $timeout.cancel(timeClean);
                // Cancel Listeners
                onLocaleChange();
                delete scope.$hasViewNav;
            });


        }
    }

    function cpsContainer () {
        var containerDirective = {
            restrict: "E",
            controller: "MainController",
            controllerAs: "main",
            scope: true,
            compile: function (tElement, tAttr) {
                tElement.addClass("view-container");
            }
        };
        return containerDirective;
    }

    cpsContent.$inject = ["$controller", "$timeout", "$window", "$Dom", "$Bind", "$UiConfig", "$Gestures"];
    function cpsContent ($controller, $timeout, $window, $Dom, $Bind, $UiConfig, $Gestures) {
        var contentDirective = {
            restrict: "E",
            scope: true,
            priority: 800,
            require: "^?cpsViewContent",
            compile: compile
        };
        return contentDirective;

        // Functions
        function compile (tElement, tAttr) {
            var scrollOptions = {},
                nativeScrolling,
                parentScope,
                parentEl,
                innerElement,
                scrollCtrl;

            if(tAttr.relative !== "true") {
                tElement.addClass("scroll-content ionic-scroll");
            } else {
                tElement.addClass("scroll-content scroll-relative ionic-scroll");
            }

            if (tAttr.scroll !== "false") {
                // We cannot use normal transclude here because it breaks element.data()
                // inheritance on compile
                innerElement = $$("<div class='scroll'></div>");
                innerElement.append(tElement.contents());
                tElement.append(innerElement);
            } else {
                tElement.addClass("scroll-content-false");
            }

            // collection-repeat requires JS scrolling
            nativeScrolling = tAttr.overflowScroll !== "false" &&
                (tAttr.overflowScroll === "true" || !$UiConfig.scrolling.jsScrolling());
            if (nativeScrolling) {
                nativeScrolling = !tElement[0].querySelector("[cps-repeat]");
            }

            return { pre: prelink };

            function prelink (scope, element, attrs, ViewContent) {
                parentEl = element.parent();
                parentScope = scope.$parent || scope;

                scope.$watch(function () {
                    return (parentScope.$hasHeader ? " has-header" : "") +
                        (parentScope.$hasSubheader ? " has-subheader" : "") +
                        (parentScope.$hasViewNav ? " has-view-nav" : "") +
                        (parentScope.$hasFooter ? " has-footer" : "") +
                        (parentScope.$hasSubfooter ? " has-subfooter" : "") +
                        (parentScope.$hasTabs ? " has-tabs" : "") +
                        (parentScope.$hasTabsTop ? " has-tabs-top" : "");
                }, function (className, oldClassName) {
                    element.removeClass(oldClassName);
                    element.addClass(className);
                });

                // Only this cpsContent should use these variables from parent scopes
                scope.$hasHeader = scope.$hasSubheader =
                    scope.$hasFooter = scope.$hasSubfooter =
                    scope.$hasTabs = scope.$hasTabsTop = scope.$hasViewNav =
                    false;

                $Bind(scope, attrs, {
                    $onScroll: "&onScroll",
                    $onScrollComplete: "&onScrollComplete",
                    hasBouncing: "@",
                    padding: "@",
                    direction: "@",
                    scrollbarX: "@",
                    scrollbarY: "@",
                    startX: "@",
                    startY: "@",
                    scrollEventInterval: "@"
                });
                scope.direction = scope.direction || "y";

                if (angular.isDefined(attrs.padding)) {
                    scope.$watch(attrs.padding, function (newVal) {
                        (element || innerElement).toggleClass("padding", !!newVal);
                    });
                }

                if(attrs.relative === "true") {
                    scope.relativeContentSize = ld.setDebounce(relativeContentSize, 10, true);
                    // $$($window).on('resize orientationchange', scope.relativeContentSize);
                    // scope.relativeContentSize();

                    scope.$watch(function () {
                        return $$(parentEl).outerHeight();
                    }, function (newVal, oldVal) {
                        if(newVal && newVal != oldVal) {
                            scope.relativeContentSize(newVal);
                        }
                    });
                }

                if (attrs.scroll !== "false") {
                    scrollOptions = {};

                    // determined in compile phase above
                    if (nativeScrolling) {
                        // use native scrolling
                        element.addClass("overflow-scroll");

                        scrollOptions = {
                            el: element[0],
                            delegateHandle: attrs.delegateHandle,
                            startX: scope.$eval(scope.startX) || 0,
                            startY: scope.$eval(scope.startY) || 0,
                            nativeScrolling: true
                        };

                    } else {
                        // Use JS scrolling
                        scrollOptions = {
                            el: element[0],
                            delegateHandle: attrs.delegateHandle,
                            locking: (attrs.locking || "true") === "true",
                            bouncing: scope.$eval(scope.hasBouncing),
                            startX: scope.$eval(scope.startX) || 0,
                            startY: scope.$eval(scope.startY) || 0,
                            scrollbarX: scope.$eval(scope.scrollbarX) !== false,
                            scrollbarY: scope.$eval(scope.scrollbarY) !== false,
                            scrollingX: scope.direction.indexOf("x") >= 0,
                            scrollingY: scope.direction.indexOf("y") >= 0,
                            scrollEventInterval: parseInt(scope.scrollEventInterval, 10) || 10,
                            scrollingComplete: onScrollComplete
                        };
                    }

                    // init scroll controller with appropriate options
                    scrollCtrl = $controller("ScrollController", {
                        $scope: scope,
                        scrollOptions: scrollOptions
                    });
                    scope.scrollCtrl = scrollCtrl;

                    scope.$on("$destroy", function () {
                        if (scrollOptions) {
                            scrollOptions.scrollingComplete = ld.noop;
                            delete scrollOptions.el;
                        }
                        // if(scope.relativeContentSize) {
                        // $$($window).off('resize orientationchange', scope.relativeContentSize);
                        // }
                        innerElement = null;
                        scrollCtrl = null;
                        element = null;
                        attrs.$$element = null;
                    });
                }

                // Functions internal
                function onScrollComplete () {
                    scope.$onScrollComplete({
                        scrollTop: scrollCtrl.scrollView.__scrollTop,
                        scrollLeft: scrollCtrl.scrollView.__scrollLeft
                    });
                }

                function relativeContentSize (contentHeight) {
                    $Dom.requestAnimationFrame(function () {
                        scope.$evalAsync(function () {
                            var restHeight = attrs.restHeight || 0,
                                top = parentEl.css("top") &&  parseInt(parentEl.css("top")) || 0,
                                bottom = parentEl.css("bottom") &&  parseInt(parentEl.css("bottom")) || 0,
                                content = contentHeight || $$(parentEl).outerHeight();

                            ld.forEach($$(parentEl).children(), function (el, key) {
                                if(el && !$$(el).attr("relative")) {
                                    restHeight = restHeight + ($$(el).outerHeight() || 0);
                                }
                            });
                            element.height(content - restHeight - top - bottom);
                            element.css({top: 0, bottom: 0});
                        });
                    });
                }
            }
        }
    }

    // Exports
    module.exports = {
        page: cpsPage,
        view: cpsView,
        cpsViewContent: cpsViewContent,
        viewNav: cpsViewNav,
        container: cpsContainer,
        content: cpsContent
    };
})();
