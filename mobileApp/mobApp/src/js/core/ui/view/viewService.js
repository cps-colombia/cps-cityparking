(function () {
    "use strict";


    viewService.$inject = ["$Dom"];
    function viewService ($Dom) {
        var NAV_VIEW_ATTR = "nav-view",
            VIEW_STATUS_ACTIVE = "active",
            VIEW_STATUS_CACHED = "cached";

        // Memebres
        var View = {
            getCss: getCss,
            viewEleIsActive: viewEleIsActive,
            navViewAttr: navViewAttr,
            isActiveScope: isActiveScope
        };
        return View;

        // Functions
        function getCss () {
            var keys = [],
                CSS = {},
                i;

            // transform
            keys = [
                "webkitTransform", "transform", "-webkit-transform", "webkit-transform",
                "-moz-transform", "moz-transform", "MozTransform", "mozTransform", "msTransform"
            ];
            for (i = 0; i < keys.length; i++) {
                if (document.documentElement.style[keys[i]] !== undefined) {
                    CSS.TRANSFORM = keys[i];
                    break;
                }
            }
            // transition
            keys = ["webkitTransition", "mozTransition", "msTransition", "transition"];
            for (i = 0; i < keys.length; i++) {
                if (document.documentElement.style[keys[i]] !== undefined) {
                    CSS.TRANSITION = keys[i];
                    break;
                }
            }
            // The only prefix we care about is webkit for transitions.
            var isWebkit = CSS.TRANSITION.indexOf("webkit") > -1;
            // transition duration
            CSS.TRANSITION_DURATION = (isWebkit ? "-webkit-" : "") + "transition-duration";
            // To be sure transitionend works everywhere, include *both* the webkit and non-webkit events
            CSS.TRANSITIONEND = (isWebkit ? "webkitTransitionEnd " : "") + "transitionend";

            return CSS;
        }

        function viewEleIsActive (viewEle, isActiveAttr) {
            navViewAttr(viewEle, isActiveAttr ? VIEW_STATUS_ACTIVE : VIEW_STATUS_CACHED);
        }

        function navViewAttr (ele, value) {
            if (arguments.length > 1) {
                $Dom.cachedAttr(ele, NAV_VIEW_ATTR, value);
            } else {
                return $Dom.cachedAttr(ele, NAV_VIEW_ATTR);
            }
        }

        function isActiveScope (scope) {
            var climbScope = scope;

            if (!scope) {
                return false;
            }
            while (climbScope) {
                return !climbScope.$$disconnected;
            }
        }
    }

    // Exports
    module.exports = viewService;

})();
