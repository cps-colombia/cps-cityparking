/* global require */

// core/ui/view/index.js
//
// index function view ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        viewRun = require("./viewRun"),
        viewController = require("./viewController"),
        viewService = require("./viewService"),
        viewDirective = require("./viewDirective");

    angular.module("cpsView", []);
    var cpsView = angular.module("cpsView");

    // Controller
    cpsView.controller("ViewController", viewController);

    // Directive
    cpsView.directive("cpsPage", viewDirective.page);
    cpsView.directive("cpsView", viewDirective.view);
    cpsView.directive("cpsViewContent", viewDirective.cpsViewContent);
    cpsView.directive("cpsViewNav", viewDirective.viewNav);
    cpsView.directive("cpsContainer", viewDirective.container);
    cpsView.directive("cpsContent", viewDirective.content);

    // Service
    cpsView.factory("$View", viewService);

    cpsView.run(viewRun);

})();
