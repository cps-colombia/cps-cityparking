/* global module */

// core/ui/view/viewRun.js
//
// Run config for cps view module
//
// 2015, CPS - Cellular Parking Systems
(function () {
    "use strict" ;


    // Restricting route access
    viewRun.$inject = [
        "$timeout",
        "$rootScope",
        "$window",
        "$Mobile",
        "$App",
        "$Logger",
        "$Login",
        "AUTH_EVENTS",
        "USER_ROLES"
    ];
    function viewRun ($timeout, $rootScope, $window, $Mobile, $App, $Logger, $Login, AUTH_EVENTS, USER_ROLES) {
        var config = $App.config;
        $rootScope.sectionTitle = config.appName;

        $rootScope.$on("$routeChangeStart", function (event, next) {
            var sectionTitle = next.data ? next.data.title || config.appName : config.appName;
            $timeout(function () {
                $rootScope.sectionTitle = sectionTitle;
            });
        });
    }

    // Exports
    module.exports = viewRun;
})();
