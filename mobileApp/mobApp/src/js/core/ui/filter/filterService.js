/* global module */

// core/ui/modal/modalService.js
//
// provide service for ui modal module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    filterService.$inject = [
        "$document",
        "$rootScope",
        "$compile",
        "$timeout",
        "$filter",
        "$Hardware",
        "$Dom",
        "$App",
        "$Logger",
        "$Mobile",
        "$UiConfig",
        "$Modal",
        "$Scroll",
        "$Keyboard"
    ];
    function filterService ($document, $rootScope, $compile, $timeout, $filter, $Hardware,
                           $Dom, $App, $Logger, $Mobile, $UiConfig, $Modal, $Scroll, $Keyboard) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance(),
            isShown = false,
            body = $document[0].body,
            templateConfig = {
                theme: $UiConfig.filter.theme(),
                transition: $UiConfig.filter.transition(),
                back: $UiConfig.backButton.icon(),
                clear: $UiConfig.filter.clear(),
                favorite: $UiConfig.filter.favorite(),
                search: $UiConfig.filter.search(),
                backdrop: $UiConfig.filter.backdrop(),
                placeholder: $UiConfig.filter.placeholder(),
                close: $UiConfig.filter.close(),
                done: $UiConfig.filter.done(),
                reorder: $UiConfig.filter.reorder(),
                remove: $UiConfig.filter.remove(),
                add: $UiConfig.filter.add()
            };

        // Members
        var Filter = {
            show: filterBar
        };
        return Filter;

        // Functions
        /**
         * @name filterBar
         * @description
         * Load and return a new filter bar.
         *
         * A new isolated scope will be created for the filter bar and the new filter bar will be appended to the
         * body, covering the header bar.
         * @param {object} opts
         *
         * @returns {function} `hideFilterBar` A function which, when called, hides & cancels the filter bar.
         */
        function filterBar (opts) {
            var backdropShown = false,
                isKeyboardShown = false,
                barConfig = ld.clone(templateConfig, true),
                scope;

            // if filterBar is already shown return
            if (isShown) {
                return;
            }

            scope = $rootScope.$new(true);
            isShown = true;
            opts = opts || {};

            // if container option is set, determine the container element by querying for the container class
            if (opts.container) {
                opts.container = body.querySelector(opts.container);
            }

            // extend templateConfig with supplied config options
            ld.extend(barConfig, opts.config);
            delete opts.config;

            // extend scope defaults with supplied options
            ld.extend(scope, {
                filterConfig:  barConfig,
                $deregisterBackButton: ld.noop,
                update: ld.noop,
                cancel: ld.noop,
                done: ld.noop,
                scrollDelegate: $Scroll,
                filter: $filter("filter"),
                filterProperties: null,
                expression: null,
                comparator: null,
                debounce: true,
                delay: 300,
                cancelText: _("cancel"),
                placeholderText: _(templateConfig.placeholder),
                cancelOnStateChange: true,
                container: body,
                favoritesTitle: _("favoriteSearches"),
                favoritesAddPlaceholder: _("addSearchTerm"),
                favoritesEnabled: false,
                favoritesKey: "filter_favorites"
            }, opts);

            scope.data = {filterText: ""};

            if(!(scope.container && scope.container.querySelector)) {
                logger.error("filter", "Invalid container");
                return ld.noop;
            }
            // if no custom theme was configured, get theme of containers bar-header
            if (!scope.filterConfig.theme) {
                scope.filterConfig.theme = getBarTheme(scope.container);
            }

            // Compile the template
            var element = scope.element = $compile("<cps-filter-bar class='filter-bar'></cps-filter-bar>")(scope),
                // Grab required jQLite elements
                filterWrapperEl = element.children().eq(0),
                input = filterWrapperEl.find("input")[0],
                backdropEl = element.children().eq(1),
                // get scrollView
                scrollView = scope.scrollDelegate.getScrollView(),
                canScroll = !!scrollView,
                // get the scroll container if scrolling is available
                scrollContainer = canScroll ? scrollView.__container : null,
                stateChangeListenDone = scope.cancelOnStateChange ?
                    $rootScope.$on("$stateChangeSuccess", function () { scope.cancelFilterBar(); }) : ld.noop;

            // Focus the input which will show the keyboard.
            var showKeyboard = function () {
                if (!isKeyboardShown) {
                    isKeyboardShown = true;
                    if(input) {
                        input.focus();
                        $Keyboard.show();
                    }
                }
            };

            // Blur the input which will hide the keyboard.
            // Even if we need to bring in keyboard in the future, blur is preferred so keyboard animates out.
            var hideKeyboard = function () {
                if (isKeyboardShown) {
                    isKeyboardShown = false;
                    if(input) {
                        input.blur();
                        $Keyboard.hide();
                    }
                }
            };

            // When the filtered list is scrolled, we want to hide the keyboard as long as it's not already hidden
            var handleScroll = function () {
                if (scrollView.__scrollTop > 0) {
                    hideKeyboard();
                }
            };

            // Scrolls the list of items to the top via the scroll delegate
            scope.scrollItemsTop = function () {
                if (canScroll && scrollView.__scrollTop > 0 && scope.scrollDelegate.scrollTop) {
                    scope.scrollDelegate.scrollTop();
                }
            };

            // Set isKeyboardShown to force showing keyboard on search focus.
            scope.focusInput = function () {
                isKeyboardShown = false;
                showKeyboard();
            };

            // Hide the filterBar backdrop if in the DOM and not already hidden.
            scope.hideBackdrop = function () {
                if (backdropEl.length && backdropShown) {
                    backdropShown = false;
                    backdropEl.removeClass("active").css("display", "none");
                }
            };

            // Show the filterBar backdrop if in the DOM and not already shown.
            scope.showBackdrop = function () {
                if (backdropEl.length && !backdropShown) {
                    backdropShown = true;
                    backdropEl.css("display", "block").addClass("active");
                }
            };

            scope.showModal = function () {
                var filterModalTemplate = "<div ng-controller='FilterController as filter' class='filter-bar-modal'>" +
                        "<cps-header class='bar bar-{{::filterConfig.theme}} disable-user-behavior'>" +
                        "<button class='button button-icon {{::filterConfig.close}}' " +
                        "ng-click='filter.closeModal()'></button>" +
                        "<h1 class='title' ng-bind-html='::favoritesTitle'></h1>" +
                        "<button ng-if='filter.searches.length > 1' class='button button-icon' " +
                        "ng-class='filter.displayData.showReorder ? filterConfig.done : filterConfig.reorder' " +
                        "ng-click='filter.displayData.showReorder = !filter.displayData.showReorder'></button>" +
                        "</cps-header>" +
                        "<cps-content>" +
                        "<cps-list show-reorder='filter.displayData.showReorder' delegate-handle='searches-list'>" +
                        "<cps-item ng-repeat='item in filter.searches' class='item-remove-animate' " +
                        "ng-class='{reordered: item.reordered}' ng-click='filter.itemClicked(item.text, $event)'>" +
                        "<span ng-bind-html='item.text'></span>" +
                        "<cps-option-button class='button-assertive icon {{::filterConfig.remove}}' " +
                        "ng-click='filter.deleteItem(item)'></cps-option-button>" +
                        "<cps-reorder-button class='{{::filterConfig.reorder}}' " +
                        "on-reorder='filter.moveItem(item, $fromIndex, $toIndex)'></cps-reorder-button>" +
                        "</cps-item>" +
                        "<div class='item item-input'>" +
                        "<input type='text' ng-model='filter.newItem.text' " +
                        "placeholder='{{::favoritesAddPlaceholder}}'/>" +
                        "<button class='button button-icon icon {{::filterConfig.add}}' " +
                        "ng-click='filter.addItem(filter.newItem)'></button>" +
                        "</div>" +
                        "</cps-list>" +
                        "</cps-content>" +
                        "</div>";

                scope.modal = $Modal.fromTemplate(filterModalTemplate, {
                    scope: scope
                });
                scope.modal.show();
            };

            // Filters the supplied list of items via the supplied filterText.
            // How items are filtered depends on the supplied filter object, and expression
            // Filtered items will be sent to update
            scope.filterItems = function (filterText) {
                var filterExp, filteredItems;

                // pass back original list if filterText is empty.
                // Otherwise filter by expression, supplied properties, or filterText.
                if (!filterText.length) {
                    filteredItems = scope.items;
                } else {
                    if (scope.expression) {
                        filterExp = angular.bind(this, scope.expression, filterText);
                    } else if (ld.isArray(scope.filterProperties)) {
                        filterExp = {};
                        ld.forEach(scope.filterProperties, function (property) {
                            filterExp[property] = filterText;
                        });
                    } else if (scope.filterProperties) {
                        filterExp = {};
                        filterExp[scope.filterProperties] = filterText;
                    } else {
                        filterExp = filterText;
                    }

                    filteredItems = scope.filter(scope.items, filterExp, scope.comparator);
                }

                $timeout(function () {
                    scope.update(filteredItems, filterText);
                    scope.scrollItemsTop();
                });
            };

            // registerBackButtonAction returns a callback to deregister the action
            scope.$deregisterBackButton = $Hardware.registerBackButtonAction(
                function () {
                    $timeout(scope.cancelFilterBar);
                }, $Hardware.BACK_PRIORITY.modal + 50
            );

            // Removes the filterBar from the body and cleans up vars/events.
            // Once the backdrop is hidden we can invoke done
            scope.removeFilterBar = function (done) {
                if (scope.removed) {
                    return;
                }

                scope.removed = true;

                // animate the filterBar out, hide keyboard and backdrop
                $Dom.requestAnimationFrame(function () {
                    filterWrapperEl.removeClass("filter-bar-in");
                    hideKeyboard();
                    scope.hideBackdrop();

                    // Wait before cleaning up so element isn't removed before filter bar animates out
                    $timeout(function () {
                        scope.scrollItemsTop();
                        scope.update(scope.items);

                        scope.$destroy();
                        element.remove();
                        scope.cancelFilterBar.$scope = scope.modal = scrollContainer = scrollView =
                            filterWrapperEl = backdropEl = input = null;
                        isShown = false;
                        (done || ld.noop)();
                    }, 350);
                });

                $timeout(function () {
                    // wait to remove this due to a 300ms delay native
                    // click which would trigging whatever was underneath this
                    scope.container.classList.remove("filter-bar-open");
                }, 400);

                scope.$deregisterBackButton();
                stateChangeListenDone();

                // unbind scroll event
                if (scrollContainer) {
                    scrollContainer.removeEventListener("scroll", handleScroll);
                }
            };

            // Appends the filterBar to the body.  Once the backdrop is hidden we can invoke done
            scope.showFilterBar = function (done) {
                if (scope.removed) {
                    return;
                }

                scope.container.appendChild(element[0]);
                scope.container.classList.add("filter-bar-open");

                // scroll items to the top before starting the animation
                scope.scrollItemsTop();

                // start filterBar animation, show backrop and focus the input
                $Dom.requestAnimationFrame(function () {
                    if (scope.removed) {
                        return;
                    }

                    $timeout(function () {
                        filterWrapperEl.addClass("filter-bar-in");
                        $Dom.animationComplete(filterWrapperEl[0]).then(function () {
                            scope.focusInput();
                            scope.showBackdrop();
                            (done || ld.noop)();
                        });
                    }, 20, false);
                });

                if (scrollContainer) {
                    scrollContainer.addEventListener("scroll", handleScroll);
                }
            };

            // called when the user presses the backdrop, cancel/back button, changes state
            scope.cancelFilterBar = function () {
                // after the animation is out, call the cancel callback
                scope.removeFilterBar(scope.cancel);
            };

            scope.showFilterBar(scope.done);

            // Expose the scope on $Filter's return value for the sake of testing it.
            scope.cancelFilterBar.$scope = scope;

            return scope.cancelFilterBar;
        }

        // Private
        function getBarTheme (container) {
            var themes = [
                    "primary",
                    "secondary",
                    "light",
                    "stable",
                    "positive",
                    "calm",
                    "balanced",
                    "energized",
                    "assertive",
                    "royal",
                    "dark"],
                cpsBar = container && container.querySelector &&
                    (container.querySelector(".bar.bar-header") || container),
                classList = cpsBar && cpsBar.classList;

            if (!classList) {
                return;
            }

            for (var i = 0; i < themes.length; i++) {
                if (classList.contains("bar-" + themes[i])) {
                    return themes[i];
                }
            }
        }
    }

    // Exports
    module.exports = filterService;

})();
