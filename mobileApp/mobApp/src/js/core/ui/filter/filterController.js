/* global module */

// core/ui/filter/filterController.js
//
// This controller acts as an intermediary
// between directive and services
// for ui filter module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    FilterController.$inject = [
        "$window",
        "$scope",
        "$timeout"
    ];
    function FilterController ($window, $scope, $timeout) {
        var filter = this,
            searchesKey = $scope.$parent.favoritesKey;

        // Membres
        filter.addItem = addItem;
        filter.moveItem = moveItem;
        filter.deleteItem = deleteItem;
        filter.itemClicked = itemClicked;
        filter.closeModal = closeModal;

        filter.displayData = {showReorder: false};
        filter.searches = angular.fromJson($window.localStorage.getItem(searchesKey)) || [];
        filter.newItem = {text: ""};

        // Functions
        function addItem () {
            if (filter.newItem.text) {
                filter.searches.push({
                    text: filter.newItem.text
                });
                filter.newItem.text = "";
            }
        }

        function moveItem (item, fromIndex, toIndex) {
            item.reordered = true;
            filter.searches.splice(fromIndex, 1);
            filter.searches.splice(toIndex, 0, item);

            $timeout(function () {
                delete item.reordered;
            }, 500);
        }

        function deleteItem (item) {
            var index = filter.searches.indexOf(item);
            filter.searches.splice(index, 1);
        }

        function itemClicked (filterText, $event) {
            var isOptionButtonsClosed = !!$event.currentTarget.querySelector(".item-options.invisible");

            if (isOptionButtonsClosed) {
                filter.closeModal();
                $scope.$parent.hideBackdrop();
                $scope.$parent.data.filterText = filterText;
                $scope.$parent.filterItems(filterText);
            }
            // else {
            //     //$ionicListDelegate.$getByHandle("searches-list").closeOptionButtons();
            // }
        }

        function closeModal () {
            $window.localStorage.setItem(searchesKey, angular.toJson(filter.searches));
            $scope.$parent.modal.remove();
        }
    }

    // Exports
    module.exports = FilterController;

})();
