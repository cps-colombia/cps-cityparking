/* global module */

// core/ui/bar/barDirective.js
//
// directives function for ui bar module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";
    var angular = require("angular"),
        $$ = angular.element;

    cpsFilterBar.$inject = ["$timeout", "$document", "$Gestures", "$Dom", "$App"];
    function cpsFilterBar ($timeout, $document, $Gestures, $Dom, $App) {
        var filterBarDirective = {
            restrict: "E",
            scope: true,
            link: postLink,
            template: getTemplate
        };

        return filterBarDirective;

        // Function
        function getTemplate () {
            // create platform specific filterBar template using filterConfig items
            if ($App.isAndroid()) {
                return "<div class='filter-bar-wrapper filter-bar-{{::filterConfig.theme}} " +
                    "filter-bar-transition-{{::filterConfig.transition}}'>" +
                    "<div class='bar bar-header bar-{{::filterConfig.theme}} item-input-inset'>" +
                    "<button class='filter-bar-cancel button button-icon icon {{::filterConfig.back}}'></button>" +
                    "<label class='item-input-wrapper'>" +
                    "<input type='search' class='filter-bar-search' ng-model='data.filterText' " +
                    "placeholder='{{::placeholderText}}' />" +
                    "<button class='filter-bar-clear button button-icon icon' " +
                    "ng-class='getClearButtonClass()'></button>" +
                    "</label>" +
                    "</div>" +
                    "</div>";
            } else {
                return "<div class='filter-bar-wrapper filter-bar-{{::filterConfig.theme}} " +
                    "filter-bar-transition-{{::filterConfig.transition}}'>" +
                    "<div class='bar bar-header bar-{{::filterConfig.theme}} item-input-inset'>" +
                    "<label class='item-input-wrapper'>" +
                    "<i class='icon {{::filterConfig.search}} placeholder-icon'></i>" +
                    "<input type='search' class='filter-bar-search' ng-model='data.filterText' " +
                    "placeholder='{{::filterConfig.placeholder}}'/>" +
                    "<button class='filter-bar-clear button button-icon icon' " +
                    "ng-class='getClearButtonClass()'></button>" +
                    "</label>" +
                    "<button class='filter-bar-cancel button button-clear' ng-bind-html='::cancelText'></button>" +
                    "</div>" +
                    "</div>";
            }
        }

        function postLink (scope, element, attrs) {
            var el = element[0],
                clearEl = el.querySelector(".filter-bar-clear"),
                cancelEl = el.querySelector(".filter-bar-cancel"),
                inputEl = el.querySelector(".filter-bar-search"),
                filterTextTimeout,
                swipeGesture,
                backdropClick,
                filterWatch,
                backdrop;

            // Action when filter bar is cancelled via backdrop click/swipe or cancel/back buton click.
            // Invokes cancel function defined in filterBar service
            var cancelFilterBar = function () {
                scope.cancelFilterBar();
            };

            // If backdrop is enabled, create and append it to filter, then add click/swipe listeners to cancel filter;
            if (scope.filterConfig.backdrop) {
                backdrop = $$("<div class='filter-bar-backdrop'></div>");
                element.append(backdrop);

                backdropClick = function (e) {
                    if (e.target === backdrop[0]) {
                        cancelFilterBar();
                    }
                };

                backdrop.bind("click", backdropClick);
                swipeGesture = $Gestures.hammer(backdrop[0], "swipe", backdropClick);
            }

            // Sure we could have had 1 function that also checked for favoritesEnabled..
            // but no need to keep checking a var that wont change
            if (scope.favoritesEnabled) {
                scope.getClearButtonClass = function () {
                    return scope.data.filterText.length ? scope.filterConfig.clear : scope.filterConfig.favorite;
                };
            } else {
                scope.getClearButtonClass = function () {
                    return scope.data.filterText.length ? scope.filterConfig.clear : "filter-bar-element-hide";
                };
            }

            // When clear button is clicked, clear filterText, hide clear button, show backdrop, and focus the input
            var clearClick = function () {
                if (clearEl.classList.contains(scope.filterConfig.favorite)) {
                    scope.showModal();
                } else {
                    $timeout(function () {
                        scope.data.filterText = "";
                        $Dom.requestAnimationFrame(function () {
                            scope.showBackdrop();
                            scope.scrollItemsTop();
                            scope.focusInput();
                        });
                    });
                }
            };

            // Bind touchstart so we can regain focus of input even while scrolling
            var inputClick = function () {
                scope.scrollItemsTop();
                scope.focusInput();
            };

            // When a non escape key is pressed, show/hide backdrop/clear button based on filterText length
            var keyUp = function (e) {
                if (e.which === 27 || e.which === "27") {
                    cancelFilterBar();
                } else if (scope.data.filterText && scope.data.filterText.length) {
                    scope.hideBackdrop();
                } else {
                    scope.showBackdrop();
                }
            };

            // Event Listeners
            cancelEl.addEventListener("click", cancelFilterBar);
            // Since we are wrapping with label, need to bind touchstart rather than click.
            // Even if we use div instead of label need to bind touchstart.
            // Click isn't allowing input to regain focus quickly
            clearEl.addEventListener("touchstart", clearClick);
            clearEl.addEventListener("mousedown", clearClick);

            inputEl.addEventListener("touchstart", inputClick);
            inputEl.addEventListener("mousedown", inputClick);

            $document[0].addEventListener("keyup", keyUp);

            // Calls the services filterItems function with the filterText to filter items
            var filterItems = function () {
                scope.filterItems(scope.data.filterText);
            };

            // Clean up when scope is destroyed
            scope.$on("$destroy", function () {
                element.remove();
                $document[0].removeEventListener("keyup", keyUp);
                if (backdrop && swipeGesture) {
                    swipeGesture.off("swipe", backdropClick);
                    swipeGesture.destroy();
                }
                filterWatch();
            });

            // Watch for changes on filterText and call filterItems when filterText has changed.
            // If debounce is enabled, filter items by the specified or default delay.
            // Prefer timeout debounce over ng-model-options so if filterText is cleared,
            // initial items show up right away with no delay
            filterWatch = scope.$watch("data.filterText", function (newFilterText, oldFilterText) {
                var delay;

                if (filterTextTimeout) {
                    $timeout.cancel(filterTextTimeout);
                }

                if (newFilterText !== oldFilterText) {
                    delay = (newFilterText.length && scope.debounce) ? scope.delay : 0;
                    filterTextTimeout = $timeout(filterItems, delay, false);
                }
            });
        }
    }

    // Exports
    module.exports = {
        cpsFilterBar: cpsFilterBar
    };

})();
