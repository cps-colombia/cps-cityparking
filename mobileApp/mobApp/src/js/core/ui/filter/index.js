/* global require */

// core/ui/filter/index.js
//
// index function filter ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        filterDirective = require("./filterDirective"),
        filterService = require("./filterService"),
        filterController = require("./filterController");

    angular.module("cpsFilter", []);
    var cpsFilter = angular.module("cpsFilter");

    // Controller
    cpsFilter.controller("FilterController", filterController);

    // Service
    cpsFilter.factory("$Filter", filterService);

    // Directive
    cpsFilter.directive("cpsFilterBar", filterDirective.cpsFilterBar);

})();
