// constants.js
//
// Ui constants for ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    function priorityConstant () {
        var priority = {
            view: 100,
            sideMenu: 150,
            modal: 200,
            actionSheet: 300,
            dialog: 400,
            loading: 500
        };
        return priority;
    }

    // Exports
    module.exports = {
        priorityConstant: priorityConstant
    };

})();
