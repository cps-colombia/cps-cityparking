(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    dialogService.$inject = [
        "$q",
        "$rootScope",
        "$timeout",
        "$compile",
        "$Body",
        "$Dom",
        "$Backdrop",
        "$Template",
        "$Hardware",
        "$UiConfig"
    ];
    function dialogService ($q, $rootScope, $timeout, $compile, $Body, $Dom,
                           $Backdrop, $Template, $Hardware, $UiConfig) {
        // TODO allow this to be configured
        var dialogStack = [],
            dialogOptions = ld.extend({
                closeBtn: ld.noop,
                stackPushDelay: ld.noop
            }, $UiConfig.dialog),
            DIALOG_TPL =
                "<cps-dialog id='{{id}}' class='popup-container' ng-class='cssClass'>" +
                "<div class='popup'>" +
                "<div class='popup-head' ng-if='title || subTitle'>" +
                "<button ng-show='closeBtn' class='button button-clear button-close' " +
                "locale-id='close' ng-click='buttonTapped(closeBtn, $event)'>Close</button>" +
                "<h3 class='popup-title'' ng-bind-html='title' ng-if='title'></h3>" +
                "<h5 class='popup-sub-title' ng-bind-html='subTitle'' ng-if='subTitle'></h5>" +
                "</div>" +
                "<div class='popup-body'>" +
                "</div>" +
                "<div class='popup-buttons' ng-show='buttons.length'>" +
                "<button ng-repeat='button in buttons' hm-tap='buttonTapped(button, $event)' " +
                "class='button' ng-class='setClass(button)' " +
                "ng-bind-html='button.text' cps-hold='button.hold'></button>" +
                "</div>" +
                "</div>" +
                "</cps-dialog>",
            _backButtonActionDone;

        // Membres
        var Dialog = {
            isShown: isShown,
            show: showDialog,
            alert: showAlert,
            confirm: showConfirm,
            prompt: showPrompt,
            // fot testing
            _createDialog: createDialog,
            _dialogStack: dialogStack
        };

        return Dialog;

        function createDialog (options) {
            var self = {},
                closeIndex;

            options = angular.extend({
                scope: null,
                title: "",
                buttons: []
            }, options || {});

            self.viewType = "dialog";
            self.scope = (options.scope || $rootScope).$new();
            self.element = $$(DIALOG_TPL);
            self.responseDeferred = $q.defer();

            self.id = self.scope.id = options.id || self.viewType + self.scope.$id;

            // Add close button
            if(dialogOptions.closeBtn()) {
                closeIndex = ld.findIndex(options.buttons, {isClose: true});
                if(closeIndex > -1) {
                    self.scope.closeBtn = options.buttons[closeIndex];
                    ld.pullAt(options.buttons, closeIndex);
                }
            }

            // Custom css class
            self.scope.cssClass = [];
            self.scope.cssClass.push(ld.kebabCase(self.id));
            if(ld.isString(options.cssClass)) {
                self.scope.cssClass.push(options.cssClass);
            }

            // Compile
            $Body.get().appendChild(self.element[0]);
            $compile(self.element)(self.scope);

            ld.extend(self.scope, {
                viewType: self.viewType,
                title: options.title,
                buttons: options.buttons,
                subTitle: options.subTitle,
                hold: options.hold || false,
                buttonTapped: function (button, event) {
                    var result = (button.onTap || angular.noop)(event);
                    event = event.originalEvent || event.srcEvent || event; // jquery events

                    if (!event.defaultPrevented) {
                        self.responseDeferred.resolve(result);
                    }
                },
                setClass: function (button) {
                    var cssClass = (button.type || "button-default");
                    return cssClass  + " " + (button.isClose ? "button-close" : "button-action");
                }
            });

            $q.when(
                options.templateUrl ?
                    $Template.load(options.templateUrl) :
                    (options.template || options.content || "")
            ).then(function (template) {
                var dialogBody = $$(self.element[0].querySelector(".popup-body"));
                if (template) {
                    dialogBody.html(template);
                    $compile(dialogBody.contents())(self.scope);
                } else {
                    dialogBody.remove();
                }
            });

            self.show = function () {
                if (self._isShown || self._removed) {
                    return;
                }

                self._isShown = true;
                $Backdrop.setOpen(self);

                $Dom.requestAnimationFrame(function () {
                    // if hidden while waiting for raf, don't show
                    if (!self._isShown) {
                        return;
                    }

                    self.element.removeClass("popup-hidden");
                    self.element.addClass("popup-showing active");
                    focusInput(self.element);
                });
            };

            self.hide = function (callback) {
                callback = callback || angular.noop;

                if (!self._isShown) {
                    return callback();
                }

                self._isShown = false;
                $Backdrop.setOpen(self);

                self.element.removeClass("active");
                self.element.addClass("popup-hidden");
                $timeout(callback, 250, false);
            };

            self.remove = function () {
                if (self._removed) {
                    return;
                }

                self.hide(function () {
                    self.element.remove();
                    self.scope.$destroy();
                });

                self._removed = true;
            };

            self.close = function (result) {
                if (!self._removed) {
                    self.responseDeferred.resolve(result);
                }
            };

            self.isShown = function () {
                return !!this._isShown;
            };

            self.scope._dialog = self;
            return self;
        }

        /**
         * @name isShown
         * @description Return if some dialog are show
         * @param dataObj options to get instance like viewType and id
         * @return {boolean} true or false is show
         */
        function isShown (dataObj) {
            return $Backdrop.isOpen(dataObj);
        }

        /**
         * @name showDialog
         * @description Show a complex dialog. This is the master show function for all dialogs.
         * @param {object} options The options for the new dialog, of the form:
         *
         * ```
         * {
         *   title: '', // String. The title of the dialog.
         *   cssClass: '', // String, The custom CSS class name
         *   subTitle: '', // String (optional). The sub-title of the dialog.
         *   template: '', // String (optional). The html template to place in the dialog body.
         *   templateUrl: '', // String (optional). The URL of an html template to place in the dialog   body.
         *   scope: null, // Scope (optional). A scope to link to the dialog content.
         *   buttons: [{ // Array[Object] (optional). Buttons to place in the dialog footer.
         *     text: 'Cancel',
         *     type: 'button-default',
         *     onTap: function(e) {
         *       // e.preventDefault() will stop the dialog from closing when tapped.
         *       e.preventDefault();
         *     }
         *   }, {
         *     text: 'OK',
         *     type: 'button-positive',
         *     onTap: function(e) {
         *       // Returning a value will cause the promise to resolve with the given value.
         *       return scope.data.response;
         *     }
         *   }]
         * }
         * ```
         *
         * @returns {object} A promise which is resolved when the dialog is closed. Has an additional
         * `close` function, which can be used to programmatically close the dialog.
         */
        function showDialog (options) {
            var dialog = createDialog(options),
                showDelay = 0;

            if (dialogStack.length > 0) {
                dialogStack[dialogStack.length - 1].hide();
            } else {
                // Add dialog-open & backdrop if this is first dialog
                $Body.addClass("popup-open");
                $Backdrop.retain();
                // only show the backdrop on the first dialog
                _backButtonActionDone = $Hardware.registerBackButtonAction(
                    onHardwareBackButton,
                    $Hardware.BACK_PRIORITY.dialog
                );
            }

            // Expose a 'close' method on the returned promise
            dialog.responseDeferred.promise.close = function dialogClose (result) {
                if (!dialog._removed) {
                    dialog.responseDeferred.resolve(result);
                }
            };

            doShow(dialog, showDelay);

            return dialog.responseDeferred.promise;
        }

        /**
         * @name showAlert
         * @description Show a simple alert dialog with a message and one button that the user can
         * tap to close the dialog.
         * @param {object} options The options for showing the alert, of the form:
         *
         * ```
         * {
         *   title: '', // String. The title of the dialog.
         *   cssClass: '', // String, The custom CSS class name
         *   subTitle: '', // String (optional). The sub-title of the dialog.
         *   template: '', // String (optional). The html template to place in the dialog body.
         *   templateUrl: '', // String (optional). The URL of an html template to place in the dialog   body.
         *   okText: '', // String (default: 'OK'). The text of the OK button.
         *   okType: '', // String (default: 'button-positive'). The type of the OK button.
         * }
         * ```
         *
         * @returns {object} A promise which is resolved when the dialog is closed.
         */
        function showAlert (opts) {
            return showDialog(angular.extend({
                buttons: [{
                    text: opts.okText || "OK",
                    type: opts.okType || "button-primary",
                    onTap: function () {
                        return true;
                    }
                }]
            }, opts || {}));
        }


        /**
         * @name showConfirm
         * @description
         * Show a simple confirm dialog with a Cancel and OK button.
         * @param {object} options The options for showing the confirm dialog, of the form:
         *
         * ```
         * {
         *   title: '', // String. The title of the dialog.
         *   cssClass: '', // String, The custom CSS class name
         *   subTitle: '', // String (optional). The sub-title of the dialog.
         *   template: '', // String (optional). The html template to place in the dialog body.
         *   templateUrl: '', // String (optional). The URL of an html template to place in the dialog   body.
         *   cancelText: '', // String (default: 'Cancel'). The text of the Cancel button.
         *   cancelType: '', // String (default: 'button-default'). The type of the Cancel button.
         *   okText: '', // String (default: 'OK'). The text of the OK button.
         *   okType: '', // String (default: 'button-positive'). The type of the OK button.
         * }
         * ```
         *
         * @returns {object} A promise which is resolved when the dialog is closed.
         */
        function showConfirm (opts) {
            return showDialog(angular.extend({
                buttons: [{
                    text: opts.cancelText || "Cancel",
                    type: opts.cancelType || "button-default",
                    isClose: opts.cancelIsClose,
                    onTap: function () { return false; }
                }, {
                    text: opts.okText || "OK",
                    type: opts.okType || "button-primary",
                    onTap: function () { return true; }
                }]
            }, opts || {}));
        }


        /**
         * @name showPrompt
         * @description Show a simple prompt dialog, which has an input, OK button, and Cancel button.
         * @param {object} options The options for showing the prompt dialog, of the form:
         *
         * ```
         * {
         *   title: '', // String. The title of the dialog.
         *   cssClass: '', // String, The custom CSS class name
         *   subTitle: '', // String (optional). The sub-title of the dialog.
         *   template: '', // String (optional). The html template to place in the dialog body.
         *   templateUrl: '', // String (optional). The URL of an html template to place in the dialog   body.
         *   inputType: // String (default: 'text'). The type of input to use
         *   inputPlaceholder: // String (default: ''). A placeholder to use for the input.
         *   cancelText: // String (default: 'Cancel'. The text of the Cancel button.
         *   cancelType: // String (default: 'button-default'). The type of the Cancel button.
         *   okText: // String (default: 'OK'). The text of the OK button.
         *   okType: // String (default: 'button-positive'). The type of the OK button.
         * }
         * ```
         *
         * @returns {object} A promise which is resolved when the dialog is closed.
         */
        function showPrompt (opts) {
            var scope = $rootScope.$new(true),
                text = "";
            scope.data = {};
            if (opts.template && /<[a-z][\s\S]*>/i.test(opts.template) === false) {
                text = "<span>" + opts.template + "</span>";
                delete opts.template;
            }
            return showDialog(angular.extend({
                template: text + "<input ng-model=\"data.response\" type=\"" + (opts.inputType || "text") +
                    "\" placeholder=\"" + (opts.inputPlaceholder || "") + "\">",
                scope: scope,
                buttons: [{
                    text: opts.cancelText || "Cancel",
                    type: opts.cancelType || "button-default",
                    isClose: opts.cancelIsClose,
                    onTap: ld.noop
                }, {
                    text: opts.okText || "OK",
                    type: opts.okType || "button-primary",
                    onTap: function () {
                        return scope.data.response || "";
                    }
                }]
            }, opts || {}));
        }

        // Private
        function doShow (dialog, showDelay) {
            dialogStack.push(dialog);
            $timeout(dialog.show, showDelay, false);

            dialog.responseDeferred.promise.then(function (result) {
                var index = dialogStack.indexOf(dialog);
                if (index !== -1) {
                    dialogStack.splice(index, 1);
                }

                if (dialogStack.length > 0) {
                    dialogStack[dialogStack.length - 1].show();
                } else {
                    $Backdrop.release();
                    // Remove popup-open & backdrop if this is last dialog
                    $timeout(function () {
                        // wait to remove this due to a 300ms delay native
                        // click which would trigging whatever was underneath this
                        if (!dialogStack.length) {
                            $Body.removeClass("popup-open");
                        }
                    }, 400, false);
                    (_backButtonActionDone || angular.noop)();
                }

                dialog.remove();

                return result;
            });
        }

        function focusInput (element) {
            var focusOn = element[0].querySelector("[autofocus]");
            if (focusOn) {
                focusOn.focus();
            }
        }

        function onHardwareBackButton () {
            var last = dialogStack[dialogStack.length - 1];
            if(last) {
                last.responseDeferred.resolve();
            }
        }

    }

    // Exports
    module.exports = dialogService;

})();
