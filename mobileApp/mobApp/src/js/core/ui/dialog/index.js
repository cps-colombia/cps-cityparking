/* global require */

// core/ui/dialog/index.js
//
// index function dialog ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        dialogDirective = require("./dialogDirective"),
        dialogService = require("./dialogService");

    angular.module("cpsDialog", []);
    var cpsDialog = angular.module("cpsDialog");

    // Service
    cpsDialog.factory("$Dialog", dialogService);

    // Directive
    cpsDialog.directive("cpsDialog", dialogDirective.dialog);

})();
