/* global module */

// core/ui/dialog/dialogDirective.js
//
// directives function for ui dialog module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    cpsDialog.$inject = ["$Logger", "$Dialog", "$rootScope"];
    function cpsDialog ($Logger, $Dialog, $rootScope) {
        var dialogDirective = {
            restrict: "E",
            link: postLink
        };
        return dialogDirective;

        // Functions
        function postLink (scope, element, attrs, ctrl) {
            // Clean prevent memory leaks
            scope.$on("$destroy", function () {
                if(scope._dialog && scope._dialog.close) {
                    scope._dialog.close();
                }
            });
        }
    }

    // Exports
    module.exports = {
        dialog: cpsDialog
    };

})();
