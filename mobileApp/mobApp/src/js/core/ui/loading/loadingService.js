/* global require */

// core/ui/loading/loadingService.js
//
// service factory loading ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    loadingService.$inject = [
        "$q",
        "$timeout",
        "$compile",
        "$rootScope",
        "$Body",
        "$Dom",
        "$Logger",
        "$Template",
        "$Backdrop",
        "$Hardware",
        "BACK_PRIORITY"
    ];
    function loadingService ($q, $timeout, $compile, $rootScope, $Body, $Dom,
                             $Logger, $Template, $Backdrop, $Hardware, BACK_PRIORITY) {
        var logger = $Logger.getInstance(),
            deregisterBackAction = ld.noop,
            deregisterStateListener1 = ld.noop,
            deregisterStateListener2 = ld.noop,
            loadingShowDelay = $q.when(),
            spinnertemplate = "<cps-spinner></cps-spinner>",
            loadingConfig = {
                template: spinnertemplate
            },
            LOADING_TPL =
                "<div class='loading-container'>" +
                "<div class='loading'>" +
                "</div>" +
                "</div>",
            _isShow = false,
            spinnerInstance,
            loaderInstance;

        // Members
        var Loading = {
            isShow: isShow,
            show: showLoader,
            hide: hideLoader,
            spinner: getSpinner,
            setSpinner: setSpinner,
            _getLoader: getLoader // For testing
        };
        return Loading;

        // Functions
        /**
         *  @name isShow
         *
         * @return {boolean} current status of loader
         */
        function isShow () {
            return _isShow;
        }

        /**
         * @name show
         *
         * @description Shows a loading indicator. If the indicator is already shown,
         * it will set the options given and keep the indicator shown.
         * @param {object} options The options for the loading indicator. Available properties:
         *  - `{string=}` `template` The html content of the indicator.
         *  - `{string=}` `templateUrl` The url of an html template to load as the content of the indicator.
         *  - `{object=}` `scope` The scope to be a child of. Default: creates a child of $rootScope.
         *  - `{boolean=}` `noBackdrop` Whether to hide the backdrop. By default it will be shown.
         *  - `{boolean=}` `hideOnStateChange` Whether to hide the loading spinner when navigating
         *    to a new state. Default false.
         *  - `{number=}` `delay` How many milliseconds to delay showing the indicator. By default there is no delay.
         *  - `{number=}` `duration` How many milliseconds to wait until automatically
         *  hiding the indicator. By default, the indicator will be shown until `.hide()` is called.
         */
        function showLoader (options) {
            options = ld.extend({}, loadingConfig || {}, options || {});
            var delay = options.delay || options.showDelay || 0;

            deregisterStateListener1();
            deregisterStateListener2();
            if (options.hideOnStateChange) {
                deregisterStateListener1 = $rootScope.$on("$routeChangeSuccess", hideLoader);
                deregisterStateListener2 = $rootScope.$on("$routeChangeError", hideLoader);
            }

            // If loading.show() was called previously, cancel it and show with our new options
            $timeout.cancel(loadingShowDelay);
            loadingShowDelay = $timeout(ld.noop, delay);
            loadingShowDelay.then(getLoader).then(function (loader) {
                return loader.show(options);
            });
        }

        /**
         * @name hide
         *
         * @description Hides the loading indicator, if shown.
         */
        function hideLoader () {
            deregisterStateListener1();
            deregisterStateListener2();
            $timeout.cancel(loadingShowDelay);
            getLoader().then(function (loader) {
                loader.hide();
            });
        }

        function setSpinner (_spinner) {
            // Check if a spinner instance
            if(_spinner && _spinner.show) {
                spinnerInstance = _spinner;
            }
        }
        /**
         * @name getSpinner
         *
         * @description get or append spinner by id
         **/
        function getSpinner (options) {
            var onLoadCallback = [],
                promise = $q.defer(),
                spinner,
                element;

            options = ld.extend({
                template: spinnertemplate
            }, options);

            if(options.appendTo) {
                if(options.icon) {
                    options.template = $$(options.template).attr("icon", options.icon);
                }
                // Compile template
                promise = $Template.compile({
                    locals: options.locals || {},
                    scope: options.scope,
                    controller: options.controller,
                    template: options.template,
                    templateUrl: options.templateUrl,
                    appendTo: options.appendTo
                }).then(function (self) {
                    if(options.id) {
                        self.element.attr("id", options.id);
                    } else if (!self.element.attr("id")) {
                        self.element.attr("id", "spinner" + self.scope.$id);
                    }
                    if(options.class) {
                        self.element.addClass(options.class);
                    }
                    self.element.addClass("spinner-" + self.element.attr("id").toLowerCase().replace("spinner", ""));

                    return self.element.controller("cpsSpinner");
                });
            } else if(options.id) {
                // Get by id already compiled
                element = $$("#" + options.id);
                if(!element) {
                    logger.warn("loading", "spinner by id not found");
                    return promise;
                }
                promise = $q.when(element.controller("cpsSpinner"));
            } else if(spinnerInstance) {
                promise = $q.when(spinnerInstance);
            } else {
                promise = promise.promise;
            }

            promise.then(function (_spinner) {
                spinner = _spinner;

                if(options.startHidden) {
                    spinner.hide();
                }
                if(options.default) {
                    spinnerInstance = spinner;
                }
                for (var x = 0; x < onLoadCallback.length; x++) {
                    // fire off all the callbacks that were spinner load
                    onLoadCallback[x]();
                }
                onLoadCallback = [];
            });

            // Attach basic function to promise
            promise.hide = function hide () {
                return spinner ? spinner.hide() :  onLoadCallback.push(promise.hide);
            };
            promise.show = function show () {
                return spinner ? spinner.show() :  onLoadCallback.push(promise.show);
            };

            // Promise
            return promise;
        }

        // Private
        function getLoader () {
            if (!loaderInstance) {
                loaderInstance = $Template.compile({
                    template: LOADING_TPL,
                    appendTo: $Body.get()
                }).then(function (self) {
                    self.show = function (options) {
                        var templatePromise = options.templateUrl ?
                                $Template.load(options.templateUrl) :
                                // options.content: deprecated
                                $q.when(options.template || options.content || "");

                        self.scope = options.scope || self.scope;

                        if (!self.isShown) {
                            // options.showBackdrop: deprecated
                            self.hasBackdrop = !options.noBackdrop && options.showBackdrop !== false;
                            if (self.hasBackdrop) {
                                $Backdrop.retain();
                                $Backdrop.getElement().addClass("backdrop-loading");
                            }
                        }

                        if (options.duration) {
                            $timeout.cancel(self.durationTimeout);
                            self.durationTimeout = $timeout(
                                angular.bind(self, self.hide),
                                    +options.duration
                            );
                        }

                        deregisterBackAction();
                        // Disable hardware back button while loading
                        deregisterBackAction = $Hardware.registerBackButtonAction(
                            ld.noop,
                            BACK_PRIORITY.loading
                        );

                        templatePromise.then(function (html) {
                            if (html) {
                                var loading = self.element.children();
                                loading.html(html);
                                $compile(loading.contents())(self.scope);
                            }

                            // Don't show until template changes
                            if (self.isShown) {
                                self.element.addClass("visible");
                                $Dom.requestAnimationFrame(function () {
                                    if (self.isShown) {
                                        self.element.addClass("active");
                                        $Body.addClass("loading-active");
                                    }
                                });
                            }
                        });

                        _isShow = self.isShown = true;
                    };
                    self.hide = function () {
                        var loading;

                        deregisterBackAction();
                        if (self.isShown) {
                            if (self.hasBackdrop) {
                                $Backdrop.release();
                                $Backdrop.getElement().removeClass("backdrop-loading");
                            }
                            self.element.removeClass("active");
                            $Body.removeClass("loading-active");
                            $Dom.requestAnimationFrame(function () {
                                if(!self.isShown) {
                                    self.element.removeClass("visible");
                                }
                            });
                        }
                        $timeout.cancel(self.durationTimeout);
                        _isShow = self.isShown = false;

                        loading = self.element.children();
                        if(loading) {
                            loading.html("");
                        }
                    };

                    return self;
                });
            }
            return loaderInstance;
        }

    }

    // Exports
    module.exports = loadingService;

})();
