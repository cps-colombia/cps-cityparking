/* global require */

// core/ui/loading/index.js
//
// index function loading ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        loadingController = require("./loadingController"),
        loadingDirective = require("./loadingDirective"),
        loadingService = require("./loadingService");

    angular.module("cpsLoading", []);
    var cpsLoading = angular.module("cpsLoading");

    cpsLoading.controller("LoadingController", loadingController);

    // Service
    cpsLoading.factory("$Loading", loadingService);

    // Directive
    cpsLoading.directive("cpsSpinner", loadingDirective.spinner);
    cpsLoading.directive("cpsPreload", loadingDirective.preload);

})();
