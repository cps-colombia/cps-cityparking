// core/ui/loading/loadingDirective.js
//
// directive function loading ui module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    /**
     * @name cpsSpinner
     * @restrict E
     *
     * @description The `cpsSpinner` directive provides a variety of animated spinners.
     * Spinners enables you to give your users feedback that the app is
     * processing/thinking/waiting/chillin' out, or whatever you'd like it to indicate.
     * While font icons are great for simple or stationary graphics, they're not suited to
     * provide great animations, which is why Ionic uses SVG instead.
     */
    cpsSpinner.$inject = [];
    function cpsSpinner () {
        var cpsSpinnerDirective = {
            restrict: "E",
            scope: {
                processing: "="
            },
            controller: "LoadingController",
            link: postLink
        };
        return cpsSpinnerDirective;

        // Functions
        function postLink (scope, element, attrs, ctrl) {
            var spinnerName = ctrl.init();
            element.addClass("spinner spinner-" + spinnerName);

            if(attrs.processing) {
                scope.$watch("processing", function (newVal, oldVal) {
                    if(newVal) {
                        ctrl.show();
                    } else {
                        ctrl.hide();
                    }
                });
            }

            scope.$on("$destroy", function () {
                element.remove();
            });
            element.on("$destroy", function onDestroy () {
                ctrl.stop();
            });
        }
    }

    cpsPreload.$inject = ["$timeout", "$animate", "$App", "$UiConfig"];
    function cpsPreload ($timeout, $animate, $App, $UiConfig) {
        var preloadDirective = {
            restrict: "E",
            link: postLink
        };
        return preloadDirective;

        // Functions
        function postLink (scope, element, attrs) {
            $App.ready(function () {
                $timeout(function () {
                    $animate.addClass(element, "disabled").then(function () {
                        element.addClass("ng-hide") ;
                    });
                }, $UiConfig.preload.duration());
            });
        }
    }

    // Exports
    module.exports = {
        spinner: cpsSpinner,
        preload: cpsPreload
    };

})();
