/* global module */

// core/locale/localeDirective.js
//
// Internationalization directives functions
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    cpsLocalize.$inject = ["$timeout","$Bind", "$Logger", "$Locale"];
    function cpsLocalize ($timeout, $Bind, $Logger, $Locale) {
        var logger = $Logger.getInstance(),
            localize = {
                restrict: "A",
                scope: true,
                compile: compile
            };
        return localize;

        // Functions
        function compile (tElement, tAttrs) {
            return {
                pre: preLink,
                post: postLink
            };
        }
        function preLink (scope, element, attrs) {
            $Bind(scope, attrs, {
                localeArgs: "@"
            });
        }
        function postLink (scope, element, attrs) {
            var localeObj = attrs.localeObj,
                localeKey = attrs.localeKey,
                locales,
                localeString;

            if(localeObj) {
                try {
                    locales = scope.$eval(localeObj);
                } catch (e) {
                    logger.warn("could not parse locales");
                }

                if(ld.isObject(locales) && localeKey) {
                    // console.log(locales);
                    locales = locales[$Locale.getLanguage()] || {};
                    localeString = locales[localeKey];
                    if(localeString) {
                        attrs.$set("locale-fallback", localeString);
                    }
                }
            }

            scope.$evalAsync(function () {
                if(scope.localeArgs) {
                    // Wait complete angular digest render if have args
                    $timeout(function () {
                        $Locale.translate(element[0]);
                    });
                } else {
                    // Run fast if no have args
                    $Locale.translate(element[0]);
                }
            });
            // Watch localeArgs
            scope.$watch("localeArgs", function (newVal, oldVal) {
                if(newVal && newVal !== oldVal) {
                    $Locale.translate(element[0]);
                }
            });

        }
    }

    cpsSetlocale.$inject = ["$Mobile", "$Locale", "$document"];
    function cpsSetlocale ($Mobile, $Locale, $document) {
        var locales = {
            restrict: "E",
            scope: {
                saveChange: "@"
            },
            template: "<select ng-model='locales.changeLang' ng-init='locales.currentLang = locales.currentLang'>" +
                "<option ng-repeat='value in locales.availableLang' locale-id='{{value}}' value='{{value}}'" +
                "ng-init='locales.changeLang = locales.currentLang'>" +
                "{{value}}" +
                "</option>",
            controller: "LocaleController",
            controllerAs: "locales",
            link: function postLink (scope, element, attrs, controller) {
                var action = scope.saveChange ? "save" : null;
                // Watch change
                scope.$watch("locales.changeLang", function (newValue, oldValue) {
                    scope.$evalAsync(function () {
                        if($Locale.getLanguage() !== newValue && newValue !== oldValue) {
                            $Locale.setLanguage(newValue, action);
                        }
                    });
                });
            }
        };
        return locales;
    }

    function cpsLocalizeForm () {
        var localizeForm = {
            restrict: "E",
            templateUrl: "app/views/localizeForm.html"
        };
        return localizeForm;
    }

    // Exports
    module.exports = {
        cpsLocalize: cpsLocalize,
        cpsSetlocale: cpsSetlocale,
        cpsLocalizeForm:  cpsLocalizeForm
    };

})();
