/* global require */

// core/locale/index.js
//
// index function for Internationalization
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        localeService = require("./localeService"),
        localeDirective = require("./localeDirective"),
        localeFilter = require("./localeFilter"),
        localeController = require("./localeController");

    angular.module("cpsLocale", []);
    var cpsLocale = angular.module("cpsLocale");


    // Controller
    cpsLocale.controller("LocaleController", localeController);

    // Service
    cpsLocale.factory("$Locale", localeService);

    // Directives
    cpsLocale.directive("cpsSetlocale", localeDirective.cpsSetlocale);
    cpsLocale.directive("localeId", localeDirective.cpsLocalize);
    cpsLocale.directive("localeKey", localeDirective.cpsLocalize);
    cpsLocale.directive("cpsLocalizeForm", localeDirective.cpsLocalizeForm);

    // Filters
    cpsLocale.filter("cpsGetLocale", localeFilter.cpsGetLocale);

})();
