/* global module */

// app/core/locale/localeFilter.js
//
// filters for locale module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    // Members
    var localeFilter = {
        cpsGetLocale: cpsGetLocale
    };

    // Functions
    /**
     * @name cpsLocale
     *
     * @description locale filter
     *
     * @param {string} key string uniq id to get translate
     * @param {object} args aditional arguments to translation
     * @param {string} fallbackString text when id not exist
     *
     * @return {string} locale translation
     */
    cpsGetLocale.$inject = ["$Locale"];
    function cpsGetLocale ($Locale) {
        var localeFilter = locale;
        return localeFilter;

        // Function
        function locale (key, args, fallbackString) {
            return $Locale.get(key, args, fallbackString);
        }
    }

    // Exports
    module.exports = localeFilter;

})();
