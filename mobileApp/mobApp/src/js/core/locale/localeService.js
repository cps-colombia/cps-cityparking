/* global module */

// core/locale/localeService.js
//
// locale factory for cps Internationalization module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    localeService.$inject = ["$q", "$timeout", "$rootScope", "$window", "$document", "$Logger", "CPS_CONFIG"];
    function localeService ($q, $timeout, $rootScope, $window, $document, $Logger, CPS_CONFIG) {
        var _ = $document[0].webL10n,
            config = CPS_CONFIG,
            logger = $Logger.getInstance(),
            localeConfig = ld.pick(config , ["locales", "defaultLocale"]),
            localeReadyCallbacks = [],
            localeReady = false;

        // Members
        var Locale = {
            options: localeConfig,
            ready: ready,
            translate: translate,
            initLanguage: initLanguage,
            getLanguage: getLanguage,
            setLanguage: setLanguage,
            get: getLocale
        };

        return Locale;

        // Functions
        function ready (cb) {
            // run through tasks to complete now that the device is ready
            if (localeReady) {
                cb();
            } else {
                // the platform isn't ready yet, add it to this array
                // which will be called once the platform is ready
                localeReadyCallbacks.push(cb);
            }
        }

        /**
         * Tranlate all text with attr locale-id or custom element
         * @param element to tranlate (optional)
         *
         * @Return {String} ISO-639-1 code of the current locale
         */
        function translate (element) {
            ready(function () {
                if(element) {
                    _.translate(element);
                } else {
                    _.translate();
                }
            });
        }

        function initLanguage () {
            var deferred = $q.defer(),
                lang = storageLanguage();

            // Set l10n debug
            _.setDebug($Logger.isEnabled);
            // Set languaje and fire!
            setLanguage(lang, function () {
                $timeout(function () {
                    // Call callback
                    onLocaleReady();
                    // Response
                    deferred.resolve(lang);
                    logger.info("locale", "Init Language {0}", [lang]);
                }, 100);
            });

            return deferred.promise;
        }

        /**
         * Get Internationalization
         *
         * @Return {String} ISO-639-1 code of the current locale
         */
        function getLanguage () {
            return _.getLanguage() || config.defaultLocale || "en";
        }

        /**
         * Set Internationalization
         * @param lang Language to change ISO-639-1 code
         * @param callback action after set lang (optional)
         *
         * @Return {String} text tranlate modify DOM
         */
        function setLanguage (lang, action, callback) {
            if(!callback && ld.isFunction(action)) {
                callback = action;
                action = null;
            }
            _.setLanguage(lang, function (err, newLang) {
                if(newLang) {
                    $rootScope.$broadcast("cps:localeChange", lang);
                    if(action === "save") {
                        $window.localStorage.lang = lang;
                        $rootScope.$broadcast("cps:localeChange:update", lang);
                    }
                }

                if(callback) {
                    callback(err, newLang);
                }
            });
        }

        /**
         * Get Internationalization by key/id
         * @param {string/object} id name or id to text will be tranlate
         * @param {object} args aditional arguments like {name} (optional)
         * @param {string} fallbackString default string (optional)
         *
         * @Return {String} text tranlate
         */
        function getLocale (id, args, fallbackString) {
            var localeObj;
            if(ld.isObject(id) && id.key && id.locales) {
                localeObj = id.locales[getLanguage()];
                id = localeObj && localeObj[id.key || id.keyFallback] || fallbackString;
            } else if(ld.isObject(id)) {
                return id[getLanguage()] || id[config.defaultLocale] || id.en;
            }
            if(ld.isString(args)) {
                fallbackString = args;
                args = {};
            }
            return _.get(id, args, fallbackString);
        }

        // Private
        function storageLanguage () {
            var localLang = $window.localStorage.lang,
                defLang = CPS_CONFIG.defaultLocale || "es";

            if(!localLang || localLang === "undefined") {
                return defLang;
            } else if (localLang === "0" || localLang === "1") {
                // Support old language storage
                setLanguage(defLang, "save", function () {
                    logger.info("locale", "Set fallback Language {0}", [defLang]);
                });
                return defLang;
            } else {
                return localLang;
            }
        }

        /**
         * onLocaleReady
         *
         * When locale init fire all callback on hold
         *
         */
        function onLocaleReady () {
            // same as appSeevice.js#onPlatformReady
            localeReady = true;
            for (var x = 0; x < localeReadyCallbacks.length; x++) {
                localeReadyCallbacks[x]();
            }
            localeReadyCallbacks = [];
        }
    }

    // Exports
    module.exports = localeService;

})();
