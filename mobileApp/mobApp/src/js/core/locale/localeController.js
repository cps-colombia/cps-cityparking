// app/core/locale/LocaleController.js
//
// localization/Internationalization i18n/l10n
// function for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    LocaleController.$inject = ["$Locale"];
    function LocaleController ($Locale) {
        var locale = this;
        locale.availableLang = $Locale.options.locales;
        locale.changeLang = "";
        locale.currentLang = $Locale.getLanguage();
    }

    // Exports
    module.exports = LocaleController;

})();
