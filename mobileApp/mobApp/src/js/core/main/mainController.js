/* global module */

// core/main/mainDirective.js
//
// main directives function
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    MainController.$inject = [
        "$App",
        "$Logger",
        "$Mobile",
        "$Login",
        "$Main"
    ];
    function MainController ($App, $Logger, $Mobile, $Login, $Main) {
        var main = this,
            config = $App.config;

        // Members
        main.sendFeedback = sendFeedback;

        // Feedback
        main.formSuccess = false;
        main.dataFeedback = {
            sug: ""
        };

        // Public Basic Data
        main.appName = config.appName;
        main.client = config.client;
        main.appVersion = config.version;
        main.options = config.options;
        main.showActions = true;
        main.language = $Mobile.getLanguage;

        // TODO: move to Login Directive
        main.userRole = $Login.isRole();
        main.logOut = $Login.logOut;
        main.isMinRole = $Login.isMinRole;

        // Extend main with basic functions
        main = ld.extend(main, $Mobile, ld.omit($App, ["config"]));

        // Functions
        function sendFeedback () {
            $Main.sendFeedback(main.dataFeedback).then(function () {
                main.formSuccess = true;
            });
        }


    }

    module.exports = MainController;

})();
