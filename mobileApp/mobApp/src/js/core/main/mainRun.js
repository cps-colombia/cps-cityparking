(function () {
    "use strict";

    var ld = require("lodash");

    mainRun.$inject = [
        "$rootScope",
        "$window",
        "$document",
        "$timeout",
        "$location",
        "$Main",
        "$Mobile",
        "$Hardware",
        "$Geo",
        "$App",
        "$Logger",
        "$Login",
        "$Locale",
        "$Socket",
        "$Body",
        "$Statusbar",
        "$Keyboard"
    ];
    function mainRun ($rootScope, $window, $document, $timeout, $location, $Main, $Mobile, $Hardware,
                     $Geo, $App, $Logger, $Login, $Locale, $Socket, $Body, $Statusbar, $Keyboard) {
        var logger = $Logger.getInstance(),
            config = $App.config,
            options = config.options,
            platform = {},
            platformOpts = {},
            statusBarOpts = {},
            isHomeVisited = false,
            prevSection,
            section;

        // Run App
        logger.debug("main", "is Cordova: {0}", [$Mobile.isCordova().toString()]);

        $App.runApp().then(function (run) {
            ld.extend(platform, run.platform);
            // Extend platform options
            platformOpts = options[platform.name] || options;
            statusBarOpts = ld.get(options, "statusBar") || ld.get(platformOpts, "statusBar") || {};

            // set screen style
            $Statusbar.setDefaults(statusBarOpts);
            $App.fullScreen(options.fullScreen || platformOpts.fullScreen,
                            statusBarOpts.visible);

            // Control keyboard
            $Keyboard.load();
            $Mobile.splashscreen("hide");

            // Register Basic backHistory
            $Hardware.registerBackButtonAction(function (event) {
                $Main.backButton(event, section, prevSection);
            }, $Hardware.BACK_PRIORITY.view);

            // Start Analytics
            $App.initAnalytics();

            // Migration storage
            $Main.storageRestore().then(function (restore) {
                $Main.baseData().then(function (base) {
                    if(base && base.version) {
                        $Mobile.checkVersion(base.version);
                    }
                });
                if(config.migration) {
                    $Main.migration().then(function () {
                        $Login.checkAuthenticated();
                    });
                } else {
                    $Login.checkAuthenticated();
                }
                if(restore > 0) {
                    $timeout(function () {
                        $Mobile.pageChange(config.homePage);
                    }, 300);
                }

                // Event
                $rootScope.$on("cps:baseData:update", function (event) {
                    if(event.defaultPrevented) {
                        return false;
                    }
                    event.preventDefault();
                    // Request update baseData
                    $Main.baseData({
                        action: "update"
                    });
                });
                $rootScope.$on("cps:baseData", function (event, data) {
                    if(event.defaultPrevented) {
                        return false;
                    }
                    event.preventDefault();
                    // Request update baseData
                    $Main.baseData(data);
                });

                logger.log("main", "Run App in {0} version {1}", [platform.name, platform.version]);
            });
        }).catch(function () {
            logger.warn("main", "Error to run app!!");
        });

        // Events
        $rootScope.$on("cps:localeChange:update", function (event, locale) {
            $Socket.emit("$cps:userLanguage:update", {locale: locale}, function (res) {
                logger.info("locale", "Update user language {0}", [JSON.stringify(res)]);
            });
        });

        $rootScope.$on("$routeChangeStart", function (event, next) {
            var homePage = (config.homePage || "").replace("#", "/"),
                path = ($location.path() || "").replace("#", "/");
            if(path == homePage) {
                ld.extend(next,  {
                    $home: homePage,
                    $path: path,
                    event: event
                });
                $rootScope.$broadcast("cps:routeChangeHome", next);
                if(!isHomeVisited) {
                    isHomeVisited = true;
                    $rootScope.$broadcast("cps:routeStartHome", next);
                }
            }
            // Cancel geo
            $Geo.clearGeo();
        });

        $rootScope.$on("cps:view:beforeEnter", function () {
            // TODO:  history service
            // always reset the keyboard state when change stage
            $Keyboard.hide();
        });

        $rootScope.$on("$routeChangeSuccess", function () {
            // TODO: implement history service
            prevSection = section;
            section = $location.path();

            // Change section
            if(section !== prevSection) {
                $rootScope.$viewName = ld.kebabCase(section);
                // Add class section in body
                $timeout(function () {
                    $Body.removeClass("section-" + ld.kebabCase(prevSection));
                    $Body.addClass("section-" + $rootScope.$viewName);
                    // Send page view to Analytics
                    $Mobile.ga("send", "pageview", section);
                }, 200);
            }
        });
    }

    // Exports
    module.exports = mainRun;

})();
