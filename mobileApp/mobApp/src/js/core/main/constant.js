/* global require */

// constants.js
//
// Core constants for cps App
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    const ld = require("lodash");

    // Constant Config
    function configConstant (confFile) {
        var env = confFile.env ? confFile.env : (confFile.debug === true ? "debug" : "dist");
        var CONFIG = {
            enviroment: env,
            debug: confFile.debug,
            nologger: (typeof confFile.nologger === "undefined") ? !confFile.debug : confFile.nologger,
            migration: (typeof confFile.migration === "undefined") ? true : confFile.migration,
            displayName: confFile.displayName,
            appName: confFile.appName || confFile.displayName,
            version: confFile.version || confFile.versionPkg,
            options: confFile.modulesOptions || {},
            versionPkg: confFile.versionPkg,
            platforms: confFile.platform,
            webservices: confFile.webservices[env] || {},
            cpsMob: confFile.webservices[env].cps.url,
            apptoken: confFile.webservices[env].cps.token,
            appToken: confFile.webservices[env].cps.token,
            appsecret: confFile.webservices[env].cps.secret,
            appSecret: confFile.webservices[env].cps.secret,
            homePage: confFile.homePage || "/home",
            analyticsId: confFile.analyticsId,
            modules: confFile.modules,
            locales: confFile.locales || "es",
            defaultLocale: confFile.defaultLocale || "es",
            defaultCurrency: confFile.defaultCurrency || "es-CO",
            socketEnabled: confFile.socket && confFile.socket[env] ?
                (typeof confFile.socket.enabled === "undefined" ?
                 true : confFile.socket.enabled) : false,
            socketUrl: "",
            minRole: confFile.minRole || "user",
            clientDomain: confFile.clientDomain, // DEPRECATED use client
            client: confFile.client || {}
        };

        CONFIG.clientDomain = CONFIG.client.domain || CONFIG.clientDomain || ld.kebabCase(CONFIG.appName) + ".com";
        CONFIG.client = ld.extend({
            domain: CONFIG.clientDomain,
            page: "http://" +  CONFIG.clientDomain,
            email: "info@" + CONFIG.clientDomain
        }, confFile.client);

        if(CONFIG.socketEnabled) {
            CONFIG.socketUrl = confFile.socket[env].cps.url;
        }

        return CONFIG;
    }

    // Constant WS
    function opsConstant () {
        var OPS = {
            baseData: "basic_data",
            mainFeedback: "feedback",
            mainVersion: "version",
            loginSend: "login_send",
            loginRecovery: "login_recovery",
            loginChange: "login_pass_change",
            loginPass2: "login_pass2",
            logoutSend: "logout_send",
            registerSend: "register_send",
            profileGet: "profile_get",
            profileUpdate: "profile_update",
            balanceTransfer: "balance_transfer",
            balanceRechargeLink: "virtual_payment",
            balanceRechargePin: "pin_recharge",
            zonesByCity: "zones_bycity",
            zonesByPosition: "zones_byposition",
            paymentHistory: "payment_history",
            paymentCode: "parking_payment",
            walletPockets: "get_pockets",
            walletCheckBond: "check_bonus",
            parkingVehicles: "vehicle",
            parkingStart: "parking_start",
            parkingActive: "parking_active",
            parkingBooking: "booking",
            parkingEnd: "parking_end",
            driverService: "driver",
            soapGetBalance: "consultarSaldo"
        };
        return OPS;
    }

    // Exports
    module.exports = {
        configConstant: configConstant,
        opsConstant: opsConstant
    };

})();
