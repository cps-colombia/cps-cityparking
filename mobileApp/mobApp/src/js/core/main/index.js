/* global require */

// core/main/index.js
//
// index function for main module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        constant = require("./constant"),
        mainController = require("./mainController"),
        mainService = require("./mainService"),
        mainDirective = require("./mainDirective"),
        mainConfig = require("./mainConfig"),
        mainRun = require("./mainRun");
    // mainService = require("./mainService");

    function initApp (appData) {
        // Create core module and add dependencies
        angular.module("cpsMain", appData.modules);
        var cpsMain = angular.module("cpsMain");

        // Define config app
        cpsMain.constant("CPS_CONFIG", constant.configConstant(appData.config));
        cpsMain.constant("CPS_MODULES", appData.modules);

        // Constant webServices
        cpsMain.constant("WS_OP", constant.opsConstant());

        // Main Controller
        cpsMain.controller("MainController", mainController);

        // Main Services
        cpsMain.factory("$Main", mainService);

        // Directive
        cpsMain.directive("cpsFeedbackForm", mainDirective.feedbackForm);

        // Set config core module
        cpsMain.config(mainConfig);

        // When Run App!
        cpsMain.run(mainRun);

        return cpsMain;
    }

    module.exports = {
        initApp: initApp
    };

})();
