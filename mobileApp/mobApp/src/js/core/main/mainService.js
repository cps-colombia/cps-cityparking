(function () {
    "use strict";

    var ld = require("lodash");

    mainService.$inject = [
        "$q",
        "$window",
        "$rootScope",
        "$Encrypt",
        "$Request",
        "$App",
        "$Mobile",
        "$Logger",
        "$Login",
        "$Storage"
    ];
    function mainService ($q, $window, $rootScope, $Encrypt, $Request, $App, $Mobile, $Logger, $Login, $Storage) {
        var _ = $Mobile.locale,
            op = $App.ops,
            config = $App.config,
            userHash = $Login.getUserHash,
            userData = $Login.getUserSession,
            logger = $Logger.getInstance(),
            navigator = $window.navigator || {};

        // Memebres
        var Main = {
            sendFeedback: sendFeedback,
            baseData: baseData,
            backButton: backButton,
            storageRestore: storageRestore,
            migration: migration
        };
        return Main;
        // Functions
        function sendFeedback (dataObj) {
            $Mobile.loading("show");
            var deferred = $q.defer(),
                req = { // Request login params
                    params: {
                        op: op.mainFeedback,
                        user_token: userHash().ut,
                        user_secret: userHash().us
                    }
                };

            ld.extend(req.params, dataObj);

            $Request.http(req).then(function (res) {
                logger.info("mian", "feedback: {0}", [JSON.stringify(res.data)]);

                $Mobile.loading("hide");

                if(res.data && res.data.statusCode === 200) {
                    $Mobile.alert(_("feedbackSent"));
                    deferred.resolve(res.data);
                } else {
                    deferred.reject(res);
                }
            }).catch(function (err) {
                $Mobile.loading("hide");
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function baseData (options) {
            var deferred = $q.defer(),
                today = new Date(),
                appData = $window.localStorage.ad,
                updateData = $window.localStorage.baseData;

            options = ld.extend({}, options);

            // Get Basic data in brackgroud
            if(!updateData || !appData ||
               today.setHours(0, 0, 0, 0) !== new Date(updateData).setHours(0, 0, 0, 0) ||
               options.action === "update") {

                httpBaseData().then(function (res) {
                    $rootScope.$broadcast("cps:baseData:success", res);
                    deferred.resolve(res);
                }).catch(function (err) {
                    deferred.reject(err);
                });
            } else if(options.action === "clean") {
                logger.info("main", "Clean base data");
                ld.forEach([
                    "ad", "baseData", "countries"
                ], function (key) {
                    $window.localStorage.removeItem(key);
                });
                deferred.resolve();
            } else {
                logger.info("main", "Get storage base data");
                deferred.resolve(baseDataStorage());
            }

            return deferred.promise;
        }

        function backButton (event, currentSection, prevSection) {
            var historyLength = $window.history && $window.history.length || 0,
                rootPage = "/login";
            // TODO: implement history service
            if (currentSection === rootPage ||
                (currentSection === config.homePage && prevSection === rootPage) ||
                (currentSection === config.homePage && prevSection === config.homePage) ||
                (prevSection === config.homePage && historyLength <= 2) || !prevSection) {
                event.preventDefault();
                // send to resume app
                if(config.backExitApp) {
                    $Mobile.confirm(_("alertexit")).then(
                        function (buttonIndex) {
                            if (buttonIndex == 1) {
                                if(navigator.app) {
                                    navigator.app.exitApp();
                                }
                            }
                        }
                    );
                } else {
                    if(navigator.Backbutton) {
                        navigator.Backbutton.goHome();
                    }
                }
            } else {
                if (navigator.app) {
                    navigator.app.backHistory();
                } else {
                    $window.history.back();
                }
            }
        }

        /**
         * @name storageRestore
         * @description resote sync localStorage data from indexedDB or WebSQL,
         * fix IOS clean local storage when phone need space
         *
         * @return {promise}
         */
        function storageRestore () {
            var deferred = $q.defer(),
                localStorage = $window.localStorage,
                restoreCount = 0;
            if(!ld.isNotdefined(localStorage.getItem("logueado"))) {
                // Pass
                deferred.resolve(restoreCount);
                return deferred.promise;
            }
            $Storage.iterate(function (value, key) {
                // Resulting key/value pair -- this callback
                // will be executed for every item in the
                // database.
                if(!localStorage.getItem(key)) {
                    if(key == "us") {
                        localStorage.setItem("ut", value.ut);
                        localStorage.setItem("us", value.st);
                    } else {
                        localStorage.setItem(key, (ld.isObject(value) ? JSON.stringify(value) : value));
                    }
                    restoreCount++;
                }
            }).then(function () {
                logger.info("main", "Restore finish, restore {0} keys", [restoreCount]);
                if(restoreCount > 0) {
                    $Mobile.ga("send", "event", "error", "missing:keys", "user_id:" + userData.idu);
                }
                deferred.resolve(restoreCount);
            }).catch(function (err) {
                logger.warn("main", err);
                deferred.resolve(restoreCount);
            });
            // Promise
            return deferred.promise;
        }

        function migration () {
            var deferred = $q.defer(),
                appData = $window.localStorage.ad,
                isAuthenticated = $window.localStorage.logueado,
                user = $window.localStorage.login,
                pass = $window.localStorage.pass;

            if(appData) {
                appData = $Encrypt.decrypt(appData, function (err) {
                    if(err) {
                        deferred.reject();
                        return deferred.promise;
                    }
                });
            }
            if(isAuthenticated === "1" && user && pass) {
                $Login.initLogin(user, pass).then(function (res) {
                    ld.forEach([
                        "login","pass",
                        "countryData", "countryGet"
                    ], function (key) {
                        $window.localStorage.removeItem(key);
                    });
                    // Redirect
                    $Mobile.pageChange(config.homePage);

                    deferred.resolve(res);
                }).catch(function () {
                    deferred.reject();
                });
            } else {
                deferred.resolve();
            }
            // Promise
            return deferred.promise;
        }

        // Private
        function httpBaseData () {
            var deferred = $q.defer(),
                req = {
                    segTimeout: 25,
                    background: true,
                    params: {
                        op: op.baseData,
                        deviceid: $App.getPlatform().platformId,
                        app_token: config.apptoken,
                        app_secret: config.appsecret
                    }
                };

            $Request.http(req).then(function (res) {
                var resData = res.data || {},
                    statusCode = resData.statusCode || res.status,
                    data = resData.data || {},
                    dateSave = new Date(),
                    appData = {},
                    saveContries = [],
                    saveZones = [];

                logger.info("main", "Get server base data {0}", [JSON.stringify(resData)]);

                if(statusCode === 200 && !ld.isEmpty(data)) {
                    saveZones = data.zones || [];

                    if(!(data.version && data.version.currVersion)) {
                        data.version =  {currVersion: config.version};
                    }

                    if(data.countries) {
                        if(ld.isString(ld.first(data.countries).idcountry)) {
                            ld.map(data.countries, function (country) {
                                country.idcountry = parseInt(country.idcountry);
                            });
                        }
                        saveContries = data.countries || [];
                    }

                    if(data.commerces) {
                        ld.forEach(data.commerces, function (value) {
                            ld.forEach(value.commerceBranches, function (zone) {
                                zone.idzone = zone.idbranch;
                                saveZones.push(zone);
                            });
                        });
                    }

                    // Set app data
                    appData = ld.extend({
                        countries: saveContries,
                        zones: saveZones,
                        date: dateSave
                    }, data);

                    // Save in storage
                    $window.localStorage.ad = $Encrypt.aes(ld.omit(appData, [
                        "zones", "countries", "commerces"
                    ]));
                    $window.localStorage.baseData = dateSave;
                    $window.localStorage.countries = JSON.stringify(saveContries);

                    if(saveZones.length) {
                        $window.localStorage.zones = JSON.stringify(saveZones);
                    }

                    // Return success
                    deferred.resolve(appData);
                } else {
                    deferred.reject(res);
                }
            });

            return deferred.promise;
        }

        function baseDataStorage () {
            var date = $window.localStorage.baseData,
                saveContries = $window.localStorage.countries ? JSON.parse($window.localStorage.countries) : {},
                saveZones = $window.localStorage.zones ? JSON.parse($window.localStorage.zones) : [],
                dataStorage = $window.localStorage.ad,
                appData = {};

            dataStorage = $Encrypt.decrypt(dataStorage);
            appData = ld.extend(dataStorage, {
                countries: saveContries,
                zones: saveZones,
                date: date
            });
            return appData;
        }

    }
    // Exports
    module.exports = mainService;

})();
