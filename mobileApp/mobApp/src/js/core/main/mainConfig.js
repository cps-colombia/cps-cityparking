/* global module */

// core/main/mainConfig.js
//
// main config to set providers and defined
// base routes
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";


    mainConfig.$inject = [
        "$compileProvider",
        "$AppProvider",
        "$LoggerProvider",
        "$locationProvider",
        "$qProvider",
        "$SocketProvider",
        "$routeProvider",
        "$httpProvider",
        "CPS_CONFIG",
        "USER_ROLES"
    ];
    function mainConfig ($compileProvider, $AppProvider, $LoggerProvider, $locationProvider, $qProvider,
                         $SocketProvider, $routeProvider, $httpProvider, CPS_CONFIG, USER_ROLES) {
        let config = CPS_CONFIG;
        let withLogger = (config.debug || !config.nologger);
        let isCordova = window.cps && window.cps.isCordova;

        // Enable or disable logger
        $LoggerProvider.enabled(withLogger);
        $compileProvider.debugInfoEnabled(withLogger);

        // Remove prefix for angular 1.6
        $locationProvider.hashPrefix("");

        // silence unhandled promise rejections (not recommended, though) for angular 1.6
        $qProvider.errorOnUnhandledRejections(false);

        // Set isCordova true or false
        $AppProvider.setIsCordova(isCordova);

        // Set Analytics ID
        $AppProvider.analyticsId = config.analyticsId;

        // Routes defualt config
        $routeProvider.otherwise("/login");
        $routeProvider.when("/about", {
            templateUrl: "template/about.html",
            data: {
                title: "about",
                roles: [USER_ROLES.guest]
            }
        }).when("/configuration", {
            templateUrl: "template/configuration.html",
            data: {
                title: "configuration"
            }
        });
    }


    // Exports
    module.exports = mainConfig;

})();
