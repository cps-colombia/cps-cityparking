(function () {
    "use strict";

    // Members
    var mainDirective = {
        feedbackForm: feedbackForm
    };

    function feedbackForm () {
        var feedbackDirective = {
            restrict: "E",
            templateUrl: "app/views/feedbackForm.html"
        };
        return feedbackDirective;
    }

    // Exports
    module.exports = mainDirective;

})();
