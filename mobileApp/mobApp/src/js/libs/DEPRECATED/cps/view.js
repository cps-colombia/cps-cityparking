// view.js
//
// All view functions that the app need
//

/*global $, LANG, Swiper */

/**
 * Basic
 */
// Initial Load jquey Mobile
$('[data-role="listview"]').listview();
$('footer[data-role="footer"]').toolbar({
    hideDuringFocus: "input, textarea, select",
    tapToggle: false
});
$('header[data-role="header"]').toolbar({
    tapToggle: false
});
$('nav[data-role="panel"]').panel({
    animate: true,
    swipeClose: true,
    display: "push",
    position: "left"
});

//Swipe options
$.event.special.swipe.horizontalDistanceThreshold = 10;

// Global variables
var pageName,
    numSlide,
    popupId,
    currentPage,
    titleMargin,
    swiperApp,
    swiperFrame,
    completeAnimate = false,
    loadContent = "#loadContent",
    pageEnabled = [ 'security', 'configuration', 'about',
                    'zones_view', 'zones', 'vehicles',
                    'balance', 'parking', 'profile',
                    'register', 'pass_recovery', 'login'
                  ] ;

/**
 * Load page
 */
// Load Page in the DOM
$.each( pageEnabled, function( pkey, pvalue ) {
    $.get( "app/view/"+pvalue+".html", function( data ) {
        $(data).filter("[data-role='page']").each(function(index, value) {
            //Check if page aready loaded
            if(!$(this).hasClass("ui-page-init")){
                $(this).addClass("ui-page-init");
                //Init dynamic page load
                pageName = $(this).attr("id");
                numSlide = 0;

                $(loadContent).after(this).enhanceWithin();

                if($(data).filter("[data-role='popup-external']").hasClass("ui-external-popup")){
                    $(data).filter("[data-role='popup-external']").each(function() {
                        if(!$(this).hasClass("ui-external-init")){
                            $(this).addClass("ui-external-init");
                            //Init external popup
                            $(loadContent).after(this);
                            $("#"+$(this).filter("[data-role='popup-external']").attr("id")).enhanceWithin();

                            //Set options
                            popupId = $(this).find(".ui-popup").attr("id");
                            $("#"+popupId).popup( "option", "transition", "pop" );
                            $("#"+popupId).popup( "option", "positionTo", "window" );
                            $("#"+popupId).popup( "option", "overlayTheme", "b" );
                            $("#"+popupId).popup( "option", "dismissible", false );
                        }
                    });
                }

                //set size content
                cpsView.setSizeContent(pageName);

                //Set slide number
                $('#'+pageName+' .swiper-slide').each(function(){
                    $(this).attr('data-slide', numSlide);
                    numSlide++;
                });

                //Custom actions for pages
                switch (pageName){
                case 'registerPage':
                    rampgap.generateDateSelect("#registerDay", "#registerMonth", "#registerYear");
                    break;
                case 'profilePage':
                    rampgap.generateDateSelect("#profileDay", "#profileMonth", "#profileYear");
                    break;
                case 'vehiclesPage':
                    $("#vehiclesPage #vehiclesList").listview();
                    break;
                }

            }
        });
    });
});

/**
 * View constructor
 */
var cpsView = {
    beforeShow: function (currentPage){
        var pageName  = currentPage,
            navFooterContent = "";

        // active section
        $("#main-nav .ui-listview .ui-btn").each(function () {
            $(this).removeClass( "ui-btn-current" );
            if ($(this).attr('data-parent-section') === $("#"+currentPage).attr('data-parent-section')) {
                $(this).addClass("ui-btn-current");
            }
        });

        // Load sections in nav footer
        //$(".nav-section-page").html("");
        var uiblock = "a",
            iconSection = $("#main-nav .ui-btn-current .icon-ui-btn").attr("class");
        $("#"+pageName+" .swiper-slide").each(function(){
            var sectionTitle = $(this).find(".page-subtitle").text();
            if(sectionTitle.length > 0 && $("#"+pageName+" .swiper-slide").length > 1){
                navFooterContent += "<li class='ui-block-"+uiblock+"'>"+
                    "<a href='#' class='ui-link ui-btn nav-slide' data-slide='"+$(this).attr("data-slide")+"'>"+
                    "<span>"+$.trim(sectionTitle)+"</span>"+
                    "</a>"+
                    "</li>";
                uiblock = String.fromCharCode(uiblock.charCodeAt(0) + 1);
            }
        });
        // Prevent animation repeat
        //if(!completeAnimate){
        //completeAnimate = true;
        $(".nav-section-page").html(navFooterContent);
        $(".ui-icon-section").removeAttr("class").addClass(iconSection+" ui-link ui-btn ui-icon-section");
        // setTimeout(function(){
        //     $(".ui-icon-section").removeClass("nd").addClass("animated bounceInUp");
        //     $(".ui-icon-section").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function(){
        //         $(".ui-icon-section").removeClass("animated bounceInUp");
        //     });
        //     completeAnimate = false;
        // }, 700);
        //}
    },
    swiperShow: function(currentPage){
        var pageName  = currentPage,
            valuesProgress = [];

        // swipe navigation
        swiperApp = new Swiper('#'+pageName+' .swiper-container', {
            roundLengths: true,
            watchActiveIndex: true,
            //calculateHeight: true,
            onSlideChangeStart: function(swiper, direction){
                $("#"+currentPage+" .swiper-slide").removeClass("swiper-slide-current");
                $("#"+currentPage+" .swiper-slide-active").addClass("swiper-slide-current");
                $(".nav-slide").removeClass("ui-btn-active");
                $(".nav-slide[data-slide="+swiper.activeIndex+"]").addClass("ui-btn-active");

                cpsView.setTitleSlider(currentPage);
                cpsView.setSizeWraperSlide(pageName);
            },
            progress:true,
            onProgressChange: function(swiper){

                for (var i = 0; i < swiper.slides.length; i++){
                    var slide = swiper.slides[i],
                        progress = slide.progress,
                        currentSlide = parseInt($('#'+currentPage+' .swiper-slide-active').attr('data-slide')) || 0,
                        nextSlide = currentSlide+1 > swiper.slides.length ? currentSlide : currentSlide+1,
                        prevSlide = currentSlide === 0 ? currentSlide-1 : 1;

                    ///call array 0 for fix new progress values after alert confirm
                    valuesProgress[i] = progress;

                    // TODO: Add support for 3d transformr in title

                    // Enable 3d cover transition
                    //swiper.setTransform(slide,'translate3d(0px,0,'+(-Math.abs(progress*1500))+'px)');

                    // $($(slide).find(".swiper-animation")).css({
                    //     '-webkit-transform' : 'translate3d('+(Math.abs(progress*400))+'px,'+(-Math.abs(progress*100))+'px,'+(-Math.abs(progress*1500))+'px)',
                    //     '-moz-transform'    : 'translate3d('+(Math.abs(progress*400))+'px,'+(-Math.abs(progress*100))+'px,'+(-Math.abs(progress*1500))+'px)',
                    //     '-ms-transform'     : 'translate3d('+(Math.abs(progress*400))+'px,'+(-Math.abs(progress*100))+'px,'+(-Math.abs(progress*1500))+'px)',
                    //     '-o-transform'      : 'translate3d('+(Math.abs(progress*400))+'px,'+(-Math.abs(progress*100))+'px,'+(-Math.abs(progress*1500))+'px)',
                    //     'transform'         : 'translate3d('+(Math.abs(progress*400))+'px,'+(-Math.abs(progress*100))+'px,'+(-Math.abs(progress*1500))+'px)'
                    // });

                    if(progress % 1 !== 0){
                        $('#'+currentPage+' .swiper-slide-active')
                            .find('.page-subtitle')
                            .css({
                                'left': (((valuesProgress[0]-currentSlide)*titleMargin)*-1)+'px',
                                'font-size': (2-Math.abs(valuesProgress[0]-currentSlide))+'em'
                            });

                        $('#'+currentPage+' .swiper-slide-active')
                            .next().find('.page-subtitle')
                            .css({
                                'left': "-"+(titleMargin-(Math.abs(valuesProgress[0]-currentSlide)*titleMargin))+'px',
                                'font-size': (1+Math.abs(valuesProgress[0]-currentSlide))+'em'
                            });

                        $('#'+currentPage+' .swiper-slide-active')
                            .prev().find('.page-subtitle')
                            .css({
                                'left': titleMargin-(Math.abs(valuesProgress[0]-currentSlide)*titleMargin)+'px',
                                'font-size': (1+Math.abs(valuesProgress[0]-currentSlide))+'em'
                            });

                    } else {
                        cpsView.setTitleSlider(currentPage);
                    }

                }
            },
            onSwiperCreated: function(swiper){
                if($("#"+currentPage).find(".swiper-slide").length > 0){
                    console.log("swiper created");
                    $(".ui-select select, .ui-input-text input, .ui-input-search input").off('swiperight, swipeleft');
                    $(".ui-select select, .ui-input-text input, .ui-input-search input").on('swipeleft', function(event){
                        swiperApp.swipeNext();
                    });
                    $(".ui-select select, .ui-input-text input, .ui-input-search input").on('swiperight', function(event){
                        swiperApp.swipePrev();
                    });

                    if($("#"+currentPage).find(".swiper-slide-current").length <= 0){
                        $("#"+currentPage+" .swiper-slide-active").addClass("swiper-slide-current");
                    }
                    $(".nav-slide").removeClass("ui-btn-active");
                    $(".nav-slide[data-slide="+$("#"+pageName+" .swiper-slide-current").attr("data-slide")+"]").addClass("ui-btn-active");
                    cpsView.setTitleSlider(currentPage);
                }
            },
            onTouchStart:function(swiper){
                for (var i = 0; i < swiper.slides.length; i++){
                    swiper.setTransition(swiper.slides[i], 0);
                }
                cpsView.setTitleSlider(currentPage);
            },
            onSetWrapperTransition: function(swiper, speed) {
                for (var i = 0; i < swiper.slides.length; i++){
                    swiper.setTransition(swiper.slides[i], speed);
                }
                cpsView.setTitleSlider(currentPage);
            }
        });

        //Set title and size wrapper slide
        this.setSizeWraperSlide(pageName);
        this.setTitleSlider(currentPage);

        //Scroll
        $('.scroll').mCustomScrollbar();
    },
    swipeTo: function(index){
        if(swiperApp.id !== undefined){
            swiperApp.swipeTo(index);
        }
    },
    setSizeContent: function(page){
        var docHeigth = $(document).height(),
            heaHeigth = $('.ui-header').outerHeight( true ),
            padPage = parseInt($('.ui-page').css('padding-bottom'), 10),

            wraHeigth = docHeigth-heaHeigth-padPage,
            wraWidth = $(document).width(),

            paddingLeft = parseInt($('.ui-content').css('padding-left'), 10),
            paddingRight = parseInt($('.ui-content').css('padding-right'),10);

        titleMargin = Math.floor(wraWidth / 1.8);

        //$('.swiper-container').height(wraHeigth);
        //$('#'+page).height(wraHeigth);
        $('#'+page+' .swiper-container').width(wraWidth);
        //$('.swiper-slide').width(wraWidth);
        //$('.swiper-slide').width(wraHeigth-docHeigth);
        //$('#main-nav .ui-panel-inner').height(docHeigth-heaHeigth);
        //$('.swiper-slide').css('height', 'auto');
    },
    setSizeWraperSlide: function(pageName){
        $("#"+pageName+" .swiper-wrapper").height(
            rampgap.getContentHeigth()-(
                $("#"+pageName+" .page-title").outerHeight(true)+
                    Math.abs(parseInt($("#"+pageName+" .page-title").css("top") || 0))+
                    Math.abs(parseInt($("#"+pageName+" .ui-swiper").css("padding-top") || 0))+
                    Math.abs(parseInt($("#"+pageName+" .ui-swiper").css("padding-bottom") || 0))
            )
        );
    },
    setTitleSlider: function(currentPage){
        $('#'+currentPage+' .swiper-slide-current')
            .find('.page-subtitle')
            .css({
                'left': 'auto',
                'font-size': '2em'
            });

        $('#'+currentPage+' .swiper-slide-current')
            .prev().find('.page-subtitle')
            .css({
                'left': titleMargin+'px',
                'font-size': '1em'
            });

        $('#'+currentPage+' .swiper-slide-current')
            .next().find('.page-subtitle')
            .css({
                'left': '-'+titleMargin+'px',
                'font-size': '1em'
            });
    }

};

/**
 * View Actions
 */

//Fix height menu when open in big pages
$("#main-nav").on( "panelbeforeopen", function(event, ui) {
    // $('.header').animate({
    //     left: '17em'
    // }, 'normal');
    $("input, textarea").blur();
    $("#"+currentPage).height(rampgap.getContentHeigth());
    $("#main-nav .data-vehicles-count").text(myCars.length);
    $(".main-nav-btn").addClass("ui-btn-active");
});
$("#main-nav").on( "panelbeforeclose", function(event, ui) {
    // $('.header').animate({
    //     left: '0'
    // }, 'fast');
    $("#"+currentPage).height('auto');
    $(".main-nav-btn").removeClass("ui-btn-active");
});

// Close main-menu if have in current section
$("#main-nav .ui-listview .ui-btn").on('tap', function () {
    var pageName  = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;
    if ($(this).attr('data-parent-section') === $("#"+pageName).attr('data-parent-section')) {
        $("#main-nav").panel('close');
    }
});

// Notifications
$(document).on('tap', '.header .notify-icon', function(){
    $('.notification-float').toggleClass( "open" );
});
$(document).on('tap', '.notification-float li', function(){
    $(this).addClass('delete').on( "webkitTransitionEnd transitionend otransitionend", function() {
        $(this).remove();
    });
});

//Nav Footer
$(document).on('tap', ".nav-slide", function(e){
    if(swiperApp !== undefined && swiperApp.id !== undefined){
        swiperApp.swipeTo($(this).attr("data-slide"));
    }
});
$(document).on('tap', "#btnNavMore", function(e){
    if($(".cps-footer").hasClass("footer-fix-height")){
        $(".cps-footer").addClass("footer-expand-height").removeClass("footer-fix-height");
    } else {
        $(".cps-footer").addClass("footer-fix-height").removeClass("footer-expand-height");
    }
});

// Close submenu in nav footer
$(document).on('tap', ".ui-page, .nav-slide, .cps-header, cps-header a, input", function(){
    $(".cps-footer").addClass("footer-fix-height").removeClass("footer-expand-height");
});

// When resize
$(window).on('resize orientationchange', function(){
    cpsView.setSizeContent(currentPage);
    cpsView.setTitleSlider(currentPage);
});

// Deletw item list
$(document).on('taphold', '.ui-allow-delete li', function( event ) {
    var listitem = $( this ),
        dir = "left",
        // Check if the browser supports the transform (3D) CSS transition
        transition = $.support.cssTransform3d ? dir : false;

    listitem.append(' <a href="#" data-transition="'+transition+'" class="deleteItem ui-btn ui-btn-icon-notext ui-icon-delete">Delete</a>');
    listitem.addClass('ui-li-has-alt');
    listitem.addClass('taphold');
    listitem.children('.ui-btn').removeClass('ui-btn-icon-right ui-icon-carat-r');
    $('.ui-allow-delete').listview( "refresh" );
});

$(document).on('taphold', '.ui-allow-delete li.taphold', function(event){
    var listitem = $( this );
    listitem.removeClass('ui-li-has-alt');
    listitem.removeClass('taphold');
    listitem.find('.deleteItem').remove();
    listitem.children('.ui-btn').addClass('ui-btn-icon-right ui-icon-carat-r');
});

// Remove list item TODO: used?
function confirmDeleteItem( listitem, transition ) {
    listitem.children( '.ui-btn' ).addClass( 'ui-btn-active' );
    $( '#confirmDelete .topic' ).remove();
    listitem.find('.topic').clone().insertAfter( "#questionDelete" );
    // Show the confirmation popup
    $( "#confirmDelete" ).popup( "open" );

    $( "#confirmDelete #yesDelete" ).on( 'tap', function() {
        if ( transition ) {
            listitem.addClass( transition )
            // When the transition is done...
                .on( "webkitTransitionEnd transitionend otransitionend", function() {
                    listitem.remove();
                    $( ".ui-allow-delete" ).listview( "refresh" ).find( ".border-bottom" ).removeClass( "border-bottom" );
                })
            // During the transition the previous button gets bottom border
                .prev( "li" ).children( "a" ).addClass( "border-bottom" )
                .end().end().children( ".ui-btn" ).removeClass( "ui-btn-active" );
        }
        // If it's not a touch device or the CSS transition isn't supported just remove the list item and refresh the list
        else {
            listitem.remove();
            $( ".ui-allow-delete li" ).listview( "refresh" );
        }
    });
    // Remove active state and unbind when the cancel button is taped
    $( "#confirmDelete #cancelDelete" ).on( 'tap', function() {
        listitem.children( ".ui-btn" ).removeClass( "ui-btn-active" );
        $( "#confirmDelete #yesDelete" ).off();
    });
}
