var SPANISH = 0;
var ENGLISH = 1;

var defaultLang = SPANISH;

var LANG_EN = {
	viewmapcar: {clase: "viewmapcar", text: "Find Car"},
    alertnewupdate:{text: "There is an available update, please update your application before continue."},
	alertbirthday: {text: "Please check your birthday"},
	resetPassword:{clase: "resetPassword", text: "Forgot your password?"},
	selectlangtext:{clase: "English"},
    alertinvaliduser:{text: "Please check your user, without special characters or spaces"},
    alertinvalidphone: {text: "Please type a valid mobile number"},
	alertcheckemail: {text: "Please check your email"},
	alerttypeemail: {text: "Please type your email"},
	alertresetsend: {text: "Your request has been sent"},
	alertmsgempty: {text: "Please write a message"},
	alertcheckcountry: {text: "Please check your Country/City selection"},
	alertfields: {text: "Please fill all fields"},
	alertpasswords: {text: "Passwords must be equals"},
	alertcarplate: {text: "Please enter the car plate"},
    alertcarplatenovalid: {text: "Enter a valid plate, without special characters or space please"},
    alertcarbrand: {text: "Please select the car brand"},
    alertcartype: {text: "Please select the car type"},
	alertpassequals: {text: "Check that the new passwords are equal"},
	alertconfirmmodifyinfo: {text: "Are you sure you want to modify your info?"},
    alertconfirmunparing: {text: "Are you sure you want unparking?"},
    alertlogout: {text: "Are you sure you want to logout?"},
    alertexit: {text: "Are you sure you want to exit?"},
	alerrtypepin: {text: "Please type a PIN number"},
	alertinvalidpin: {text: "Invalid"},
	alertreloadok: {text: "Reload Succesfull"},
	alertcheckcu: {text: "Please check CU and CU Retype"},
	alertamount: {text: "Please type an amount"},
	alertcheckdata: {text: "Please check your data"},
    alertinvalidcu: {text: "Invalid destination unique code (UC), Please check it."},
	alertgiveok: {text: "Balance transferred successfully"},
	alertusername: {text: "Invalid user or password"},
    alertemptyusername: {text: "Complete user and password fields"},
	alertalreadyregister: {text: "Some of these data have already been registered"},
	alertregisterok: {text: "Register Succesfully"},
	alertnozone: {text: "You are not close to any zone"},
	alertalreadyparked: {text: "This vehicle it's already parked"},
	alertvehiclenoregistered: {text: "This vehicle it's not registered"},
	alertplacewrong: {text: "This place code it's wrong"},
	alertplacenot: {text: "This place it's not available"},
	alertplacenotexist: {text: "Place does not exist"},
	alertnotparking: {text: "You don't have active parkings"},
	alertnocars: {text: "you don't have registereds cars"},
	alertnobalance: {text: "you don't have balance"},
	alertconfirmcardelete: {text: "Are you sure you want to delete this vehicle?"},
    alertconfirmcardeletetitle: {text: "Borrar vehículo"},
	alertsorry: {text: "Sorry, please try again later"},
    alerttimeouterror: {text: "Exceeded timeout, please try again"},
    alertnetworkerror: {text: "There is a communication error, check your network connection and try again"},
    alertnotfounderror: {text: "Your request could not be completed, please try again"},
    alertservererror: {text: "There was an internal error, please try again"},
    alertgeneralerror: {text: "There was an unexpected error, please try again"},
    alertnetworkconnection: {text: "The application requires internet for proper operation, check your connection"},
    alertgeonotsupported: {text: "The geolocation is not active, please turn it on."},
	alertvehicleexist: {text: "Sorry, this car it's already registered"},
    alertvehiclelimit: {text: "Sorry, exceeded the limit of allowed vehicles"},
	alertvehicleadded: {text: "Vehicle successfully added"},
	alertcheckpassword: {text: "Please check your typed passwords"},
	alertpasschangeok: {text: "Your password was changed successfully"},
	alertfiveleft: {text: "CPS/PARKING\nJust 5 minutes to remove your vehicle"},
	alertoneleft: {text: "CPS/PARKING\nJust 1 minute to remove your vehicle"},
	alertvehiclefined: {text: "CPS/PARKING\nFrom this point you will be fined for not removing your vehicle"},
    alertconfirmtitle: {text: "CPS PARKING"},
    alerttitle: {text: "CPS PARKING"},

    btnOk: {clase: "oktxt", text: "Ok"},
    btnCancel: {clase: "canceltxt", text: "Cancel"},
	backBtn: {clase: "backBtn", text: "Back"},
	edittxt: {clase: "edittxt", text: "Edit"},
	savetxt: {clase: "savetxt", text: "Save"},
	sendtxt: {clase: "sendtxt", text: "Send"},
    closetxt: {clase: "closetxt", text: "Close"},
    agreedtxt: {clase: "agreedtxt", text: "Agreed"},
    ckecksessiontxt: {clase: "ckecksessiontxt", text: "Checking user credentials"},

	/*LOGIN*/
	userName: {clase: "userName", text: "Username"},
	password: {clase: "password", text: "Password"},
	login: {clase: "login", text: "Login"},
    loginfulltxt: {clase: "loginfulltxt", text: "LogIn"},
	register: {clase: "register", text: "Register"},
    registerWithtxt: {clase: "registerWithtxt", text: "or Connect with"},

	/*END LOGIN*/

	/*REGISTER*/
    jointxt: {clase: "jointxt", text: "Join Us"},
	name: {clase: "nametxt", text: "Name"},
	lastName: {clase: "lastName", text: "Last Name"},
	mobile: {clase: "mobile", text: "Mobile No."},
	address: {clase: "address", text: "Address"},
	birthday: {clase: "birthday", text: "BirthDay"},
	year: {clase: "year", text: "Year"},
	month: {clase: "month", text: "Month"},
	day: {clase: "day", text: "Day"},
	repassword: {clase: "repassword", text: "Re-Password"},
	email: {clase: "email", text: "E-Mail"},
	country: {clase: "country", text: "Country"},
	city: {clase: "city", text: "City"},
	cancel: {clase: "cancel", text: "Cancel"},
	loginUs: {clase: "loginUs", text: "Login"},
	pageNameRegister: {clase: "pnreg", text: "CPS - Register"},

    january:{clase: "month1txt", text: "January"},
    february:{clase: "month2txt", text: "February"},
    march:{clase: "month3txt", text: "March"},
    april:{clase: "month4txt", text: "April"},
    may:{clase: "month5txt", text: "May"},
    june:{clase: "month6txt", text: "June"},
    july:{clase: "month7txt", text: "July"},
    august:{clase: "month8txt", text: "August"},
    september:{clase: "month9txt", text: "September"},
    october:{clase: "month10txt", text: "October"},
    november:{clase: "month11txt", text: "November"},
    december:{clase: "month12txt", text: "December"},

    additionaltxt: {clase: "additionaltxt", text: "Additional info"},
    additionalinfotxt: {clase: "aditionalinfotxt", text: "Help us complete this information to enjoy the benefits of CPS"},

    alertlongfield: {text: "Check the length of the fields"},
    alertcityfield: {text: "Please check your City"},
    alertcountryfield: {text: "Please check your Country"},

	/*END REGISTER*/

	/* MAIN MENU*/
	zonestxt: {clase: "zonestxt", text: "Zones"},
	parkingtxt: {clase: "parkingtxt", text: "Parking"},
	vehiclestxt: {clase: "vehiclestxt", text: "Vehicles"},
	usertxt: {clase: "usertxt", text: "User Account"},
	balancetxt: {clase: "balancetxt", text: "Balance"},
    balancecurrenttxt: {clase: "balancecurrenttxt", text: "Balance"},
	pnmain: {clase: "pnmain", text: "Main Menu"},
	settingstxt: {clase: "settingstxt", text: "Settings"},
	pnsetlang: {clase: "pnsetlang", text: "Set Language"},
	suggestionstxt: {clase: "suggestionstxt", text: "Suggestions"},
    suggestiontxt: {clase: "suggestiontxt", text: "Suggestion"},
	suggestionsend: {text: "Suggestion sent"},
    abouttxt: {clase: "abouttxt", text: "About"},
    helptxt: {clase: "helptxt", text: "Help"},

	/* ZONES*/
	viewzonestxt: {clase: "viewzonestxt", text: "View Zones"},
	locatezonestxt: {clase: "locatezonestxt", text: "Locate Zones"},
	pnzones: {clase: "pnzones", text: "Zones"},
	viewtxt: {clase: "viewtxt", text: "View"},
    detailtxt: {clase: "detailtxt", text: "Detail"},
	canceltxt: {clase: "canceltxt", text: "Cancel"},
	pnviewzones: {clase: "pnviewzones", text: "View Zones"},
	pnzoneresult: {clase: "pnzoneresult", text: "Zones Result"},
	viewdaystxt: {clase: "viewdaystxt", text: "View Days"},
	zoneoptionstxt: {clase: "zoneoptionstxt", text: "Zone Options"},
	pnzonemap: {clase: "pnzonemap", text: "Zone Map"},
	pnzonedays: {clase: "pnzonedays", text: "Zone Days"},
	pnzoneinfo: {clase: "pnzoneinfo", text: "Zone Info"},
	pricetxt: {clase: "price", text: "Price:"},
	placestxt: {clase: "placestxt", text: "Places"},
	viewmaptxt: {clase: "viewmaptxt", text: "View Map"},
	viewpricestxt: {clase: "viewpricestxt", text: "View Prices"},
	maxtimetxt: {clase: "maxtimetxt", text: "Max Time"},
	minutepricetxt: {clase: "minutepricetxt", text: "Minute Price"},
	currenttimetxt: {clase: "currenttimetxt", text: "Current Time"},
	zonetxt: {clase: "zonetxt", text: "Zone"},
	opentimetxt: {clase: "opentimetxt", text: "Open Time"},
	closetimetxt: {clase: "closetimetxt", text: "Close Time"},
    searchtxt: {clase: "searchtxt", text: "Search"},
    filterzonetxt: {clase: "filterzonetxt", text: "Filter zone"},

    //Carmen
    zoneCOS01desctxt: {text: "Alrededores del INS\n Alrededores del Hospital Calderon Guardia\n Alrededores del Tribunal Supremo de Elecciones\n Alrededores de Cuesta de Moras\n Alrededores de la Asamblea Legislativa"},
    //Merced
    zoneCOS02desctxt: {text: "Alrededores de La Corte\n Alrededores de La Clinica Biblica"},
    //Hospital
    zoneCOS03desctxt: {text: "No tiene descripción"},
    //Catedral
    zoneCOS04desctxt: {text: "Alrededores de la Municipalidad de San José\n Alrededores de Tributacion Directa\n Alrededores de Torre Mercedes\n Parque Maria Auxiliadora\n Alrededores del centro Colon"},
    //Uruca
    zoneCOS07desctxt: {text: "Alrededor del Hospital Mexico\n  Migracion y Extrangeria\n Hospital del Trauma\n Cenare."},

	/* PARKING*/
	startparkingtxt: {clase: "startparkingtxt", text: "Start Parking"},
	activeparkingtxt: {clase: "activeparkingtxt", text: "Active Parking"},
    activetxt: {clase: "activetxt", text: "Active"},
	pnparking: {clase: "pnparking", text: "Parking"},
	zoneplacetxt: {clase: "zoneplacetxt", text: "Zone Place"},
	smsnotitxt: {clase: "smsnotitxt", text: "SMS Notification"},
	parktxt: {clase: "parktxt", text: "Park"},
	pnstartparking: {clase: "pnstartparking", text: "Start Parking"},
	pnparkingdone: {clase: "pnparkingdone", text: "Parking Done"},
	pnparkingoptions: {clase: "pnparkingoptions", text: "Parking Options"},
	pnendparking: {clase: "pnendparking", text: "End Parking"},
	endparkingtxt: {clase: "endparkingtxt", text: "End Parking"},
	endtimetxt: {clase: "endtimetxt", text: "End Time"},
	totaltimetxt: {clase: "totaltimetxt", text: "Total Time"},
	selectplatetxt: {clase: "selectplatetxt", text: "Select Plate"},
    notparkingtxt: {clase: "notparkingtxt", text: "You don't have active parkings"},
    infoparktxt: {clase: "infoparktxt", text: "The park space is the number that is painted in these by parking space" },

	/* VEHICLES */
	addvechicletxt: {clase: "addvechicletxt", text: "Add Vehicle"},
	viewvehiclestxt: {clase: "viewvehiclestxt", text: "View Vehicles"},
	addtxt: {clase: "addtxt", text: "Add"},
	carplatetxt: {clase: "carplatetxt", text: "Car Plate:"},
	pnaddvehicles: {clase: "pnaddvehicles", text: "Add Vechicles:"},
	vehicletxt: {clase: "vehicletxt", text: "Vehicle"},
    vehicletypetxt: {clase: "vehicletypetxt", text: "Vehicle Type"},
    vehiclebrandtxt: {clase: "vehiclebrandtxt", text: "Vehicle Brand"},
    vehicleslisttxt: {clase:"vehicleslisttxt", text: "List"},
    searchvehiclestxt: {clase:"searchvehicletxt", text: "Search Vehicule"},
	starttimetxt: {clase: "starttimetxt", text: "Start Time"},
    unparkingtxt: {clase: "unparkingtxt", text: "Unparking"},
	placetxt: {clase: "placetxt", text: "Place"},
	parkinginfotxt: {clase: "parkinginfotxt", text: "Parking Information"},
    nocarstxt: {clase: "nocarstxt", text: "you don't have registereds cars"},


	/* USER ACCOUNT*/
	profiletxt: {clase: "profiletxt", text: "Profile"},
    yourdatatxt: {clase: "yourdatatxt", text: "Your data"},
	changepasstxt: {clase: "changepasstxt", text: "Change Password"},
	pnuserinfo: {clase: "pnuserinfo", text: "User Info"},
	oldpasstxt: {clase: "oldpasstxt", text: "Old Password"},
	newpasstxt: {clase: "newpasstxt", text: "New Password"},
    dobtxt: {clase: "dobtxt", text: "Date of Birth"},

    /* SETTINGS */
    logouttxt: {clase: "logouttxt", text: "Logout"},
    securitytxt: {clase: "securitytxt", text:"Security"},
    languagetxt: {clase: "languagetxt", text: "Language"},
    optionstxt: {clase: "optionstxt", text: "Options"},
    versiontxt: {clase: "versiontxt", text: "Version"},
    changetxt: {clase: "changetxt", text: "Change"},

	/* BALANCE*/
	reloadbalancemsjtxt: {clase: "reloadbalancemsjtxt", text: "Reload with card"},
	reloadbalancetxt: {clase: "reloadbalancetxt", text: "Reload PIN"},
	viewbalancetxt: {clase: "viewbalancetxt", text: "View Balance"},
	givebalancetxt: {clase: "givebalancetxt", text: "Transfer"},
	paypalreloadtxt: {clase: "paypalreloadtxt", text: "Credit card"},
	pinnumbertxt: {clase: "pinnumbertxt", text: "Pin Number"},
	reloadtxt: {clase: "reloadtxt", text: "Reload"},
	pnbalanceinfo: {clase: "pnbalanceinfo", text: "Balance Info"},
	amounttxt: {clase: "amounttxt", text: "Amount"},
	givetxt: {clase: "givetxt", text: "Transfer"},
	pngivebalance: {clase: "pngivebalance", text: "Transfer"},
    cumobiletxt: {clase: "cumobiletxt", text: "CU or Cel Number"},
    cumobilerepeattxt: {clase: "cumobilerepeattxt", text: "Repetir CU or Cel Number"},

	menuZones: {clase: "zones", text: "Zones"},
	menuParking: {clase: "park", text: "Parking"},
	alertError: {text: "xx"},

    //RECOVERY
    recoverytxt: {clase: "recoverytxt", text: "Recovery"},
    recoverypasstxt: {clase: "recoverypasstxt", text: "Password recovery"},

    // SHARE
    sharetxt: {clase: "sharetxt", text: "Share"},
    shareMessage: {text: "The best #parking app  City Parking ¡DOWNLOAD HERE! #Android #Iphone #Wp"},
    shareTitle: {text: "CityParking - Cellular Parking Systems"},
    shareLink: {text: "http://goo.gl/W26B8H"}
};

var LANG_ES = {
	viewmapcar: {clase: "viewmapcar", text: "Buscar Carro"},
    alertnewupdate:{text: "Hay una actualización pendiente, para poder continuar usted debe actualizar la aplicación."},
	alertbirthday: {text: "Por favor revisa tu fecha de nacimiento"},
	selectlangtext:{clase: "Español"},
	resetPassword: {clase: "resetPassword", text: "Olvidaste tu contraseña?"},
    alertinvaliduser: {text: "Por favor revisa tu usuario que no tenga caracteres especiales o espacios"},
    alertinvalidphone: {text: "Por favor ingresa un numero de celular valido"},
	alertcheckemail: {text: "Por favor revisa tu correo"},
	alerttypeemail: {text: "Por favor ingresa tu correo"},
	alertresetsend: {text: "Se ha enviado tu solicitud"},
	alertmsgempty: {text: "Por favor escribe un mensaje"},
	alertcheckcountry: {text: "Por favor revisa la ciudad y/o pais"},
	alertfields: {text: "Por favor llena todos los campos"},
	alertpasswords: {text: "Las contraseñas deben ser iguales"},
	alertcarplate: {text: "Por favor ingresa la placa"},
    alertcarplatenovalid: {text: "Ingresa una placa válida, sin caracteres especiales o espacios por favor"},
    alertcarbrand: {text: "Por favor selecciona la marca"},
    alertcartype: {text: "Por favor selecciona el tipo de vehiculo"},
	alertpassequals: {text: "Revisa que las contraseñas sean iguales"},
	alertconfirmmodifyinfo: {text: "¿Deseas modificar tu informacion?"},
    alertconfirmunparing: {text: "¿Esta seguro que desea desparquear?"},
	alertlogout: {text: "¿Desea cerrar sesión?"},
    alertexit: {text: "¿Desea salir?"},
	alerrtypepin: {text: "Por favor ingresa un numero PIN"},
	alertinvalidpin: {text: "Pin Invalido"},
	alertreloadok: {text: "Recarga realizada"},
	alertcheckcu: {text: "Por favor revisa el CU y Re-CU"},
	alertamount: {text: "Por favor ingresa una cantidad"},
	alertcheckdata: {text: "Por favor revisa tus datos"},
    alertinvalidcu: {text: "Codigo unico (CU) de destino invalido, por favor verifiquelo."},
	alertgiveok: {text: "Saldo transferido con éxito"},
	alertusername: {text: "Usuario o contraseña invalido"},
    alertemptyusername: {text: "Complete los campos usuario y contraseña"},
	alertalreadyregister: {text: "Algunos de estos datos ya fueron registrados"},
	alertregisterok: {text: "Registro realizado"},
	alertnozone: {text: "No estas cerca de ninguna zona"},
	alertalreadyparked: {text: "Este vehiculo ya fue parqueado"},
	alertvehiclenoregistered: {text: "Este vehiculo no esta registrado"},
	alertplacewrong: {text: "Placa invalida"},
	alertplacenot: {text: "Este numero espacio no esta disponible"},
	alertplacenotexist: {text: "El numero espacio no existe"},
	alertplaceclosed: {text: "Zona cerrada por Horario"},
	alertnotparking: {text: "No tienes parqueos activos"},
	alertnocars: {text: "No tienes vehiculos registrados"},
	alertnobalance: {text: "No tienes saldo suficiente"},
	alertconfirmcardelete: {text: "Deseas borrar este vehiculo?"},
    alertconfirmcardeletetitle: {text: "Borrar vehículo"},
	alertsorry: {text: "Lo sentimos, intenta nuevamente"},
    alerttimeouterror: {text: "Supero el tiempo de espera, por favor intente de nuevo"},
    alertnetworkerror: {text: "Ocurrido un error en la solicitud, revise su conexión e intente de nuevo"},
    alertnotfounderror: {text: "Su solicitud no se pudo completar, por favor intente de nuevo"},
    alertservererror: {text: "Hubo un error interno, por favor intente de nuevo"},
    alertgeneralerror: {text: "Hubo un error inesperado, por favor intente de nuevo"},
    alertnetworkconnection: {text: "La aplicación necesita internet para su correcto funcionamiento, revise su conexión"},
    alertgeonotsupported: {text: "La Geolocalización no está activa, por favor enciendalo."},
	alertvehicleexist: {text: "Este vehiculo ya existe"},
    alertvehiclelimit: {text: "Lo sentimos, supero la cantidad de vehiculos permitidos"},
	alertvehicleadded: {text: "Vehiculo agregado"},
	alertcheckpassword: {text: "Por favor revisa tu contraseña"},
	alertpasschangeok: {text: "Contraseña actualizada"},
	alertfiveleft: {text: "CPS/PARKING\nJTe quedan 5 minutos para retirar tu vehiculo"},
	alertoneleft: {text: "CPS/PARKING\nTe quedan 1 minuto para retirar tu vehiculo"},
	alertvehiclefined: {text: "CPS/PARKING\nTu vehiculo recibira una multa por no retirarlo"},
    alertconfirmtitle: {text: "CPS PARKING"},
    alerttitle: {text: "CPS PARKING"},

    btnOk: {clase: "oktxt", text: "Aceptar"},
    btnCancel: {clase: "canceltxt", text: "Cancelar"},
	backBtn: {clase: "backBtn", text: "Atras"},
	edittxt: {clase: "edittxt", text: "Editar"},
	savetxt: {clase: "savetxt", text: "Guardar"},
	sendtxt: {clase: "sendtxt", text: "Enviar"},
    closetxt: {clase: "closetxt", text: "Cerrar"},
    agreedtxt: {clase: "agreedtxt", text: "Entendido"},
    ckecksessiontxt: {clase: "ckecksessiontxt", text: "Comprobando credenciales de usuario"},

	/*LOGIN*/
	userName: {clase: "userName", text: "Usuario"},
	password: {clase: "password", text: "Contraseña"},
	login: {clase: "login", text: "Iniciar"},
    loginfulltxt: {clase: "loginfulltxt", text: "Iniciar Sesión"},
	ingresar: {clase: "ingresar", text: "Iniciar"},
	register: {clase: "register", text: "Registro"},
    registerWithtxt: {clase: "registerWithtxt", text: "o Conectate con"},
	/*END LOGIN*/

	/*REGISTRO*/
    jointxt: {clase: "jointxt", text: "Únete a CPS"},
	name: {clase: "nametxt", text: "Nombres"},
	lastName: {clase: "lastName", text: "Apellidos"},
	mobile: {clase: "mobile", text: "Movil"},
	address: {clase: "address", text: "Direccion"},
	birthday: {clase: "birthday", text: "Fecha Nacimiento"},
	year: {clase: "year", text: "Año"},
	month: {clase: "month", text: "Mes"},
	day: {clase: "day", text: "Dia"},
	repassword: {clase: "repassword", text: "Reingresa Contraseña"},
	email: {clase: "email", text: "Correo"},
	country: {clase: "country", text: "Pais"},
	city: {clase: "city", text: "Ciudad"},
	cancel: {clase: "cancel", text: "Cancelar"},
	loginUs: {clase: "loginUs", text: "Usuario"},
	pageNameRegister: {clase: "pnreg", text: "CPS - Registro"},

    january:{clase: "month1txt", text: "Enero"},
    february:{clase: "month2txt", text: "Febrero"},
    march:{clase: "month3txt", text: "Marzo"},
    april:{clase: "month4txt", text: "Abril"},
    may:{clase: "month5txt", text: "Mayo"},
    june:{clase: "month6txt", text: "Junio"},
    july:{clase: "month7txt", text: "Julio"},
    august:{clase: "month8txt", text: "Agosto"},
    september:{clase: "month9txt", text: "Septiembre"},
    october:{clase: "month10txt", text: "Octubre"},
    november:{clase: "month11txt", text: "Noviembre"},
    december:{clase: "month12txt", text: "Diciembre"},

    additionaltxt: {clase: "additionaltxt", text: "Infromación Adicional"},
    additionalinfotxt: {clase: "aditionalinfotxt", text: "Ayudanos a completar esta información para gozar los beneficios de CPS"},

    alertlongfield: {text: "Compruebe la longitud de los campos"},
    alertcountryfield: {text: "Por favor revisa el País"},
    alertcityfield: {text: "Por favor revisa la Ciudad"},
	/*END REGISTER*/

	/* MENU PRINCIPAL*/
	zonestxt: {clase: "zonestxt", text: "Zonas"},
	parkingtxt: {clase: "parkingtxt", text: "Parqueo"},
	vehiclestxt: {clase: "vehiclestxt", text: "Vehiculos"},
	usertxt: {clase: "usertxt", text: "Cuenta de Usuario"},
	balancetxt: {clase: "balancetxt", text: "Saldo"},
    balancecurrenttxt: {clase: "balancecurrenttxt", text: "Saldo"},
	pnmain: {clase: "pnmain", text: "Menu"},
	settingstxt: {clase: "settingstxt", text: "Configuracion"},
	pnsetlang: {clase: "pnsetlang", text: "Cambiar Idioma"},
	suggestionstxt: {clase: "suggestionstxt", text: "Sugerencias"},
	suggestiontxt: {clase: "suggestiontxt", text: "Sugerencia"},
	suggestionsend: {text: "Sugerencia enviada"},
    abouttxt: {clase: "abouttxt", text: "Acerca"},
    helptxt: {clase: "helptxt", text: "Ayuda"},

	/* ZONAS*/
	viewzonestxt: {clase: "viewzonestxt", text: "Ver Zonas"},
	locatezonestxt: {clase: "locatezonestxt", text: "Localizar Zonas"},
	pnzones: {clase: "pnzones", text: "Zonas"},
	viewtxt: {clase: "viewtxt", text: "Ver"},
    detailtxt: {clase: "detailtxt", text: "Detalle"},
	canceltxt: {clase: "canceltxt", text: "Cancelar"},
	pnviewzones: {clase: "pnviewzones", text: "Ver Zonas"},
	pnzoneresult: {clase: "pnzoneresult", text: "Zonas"},
	viewdaystxt: {clase: "viewdaystxt", text: "Ver Horarios"},
	zoneoptionstxt: {clase: "zoneoptionstxt", text: "Opciones de Zona"},
	pnzonemap: {clase: "pnzonemap", text: "Mapa de Zona"},
	pnzonedays: {clase: "pnzonedays", text: "Horarios"},
	pnzoneinfo: {clase: "pnzoneinfo", text: "Info Zona"},
	pricetxt: {clase: "pricetxt", text: "Precio:"},
	placestxt: {clase: "placestxt", text: "Lugares"},
	viewmaptxt: {clase: "viewmaptxt", text: "Ver Mapa"},
	viewpricestxt: {clase: "viewpricestxt", text: "Ver Precios"},
	maxtimetxt: {clase: "maxtimetxt", text: "Tiempo Maximo"},
	minutepricetxt: {clase: "minutepricetxt", text: "Precio Minuto"},
	currenttimetxt: {clase: "currenttimetxt", text: "Tiempo Actual"},
	zonetxt: {clase: "zonetxt", text: "Zona"},
	opentimetxt: {clase: "opentimetxt", text: "Apertura"},
	closetimetxt: {clase: "closetimetxt", text: "Cierre"},
	sundaytxt: {clase: "sundaytxt", text: "Domingo"},
	mondaytxt: {clase: "mondaytxt", text: "Lunes"},
	tuesdaytxt: {clase: "tuesdaytxt", text: "Martes"},
	wednesdaytxt: {clase: "wednesdaytxt", text: "Miercoles"},
	thursdaytxt: {clase: "thursdaytxt", text: "Jueves"},
	fridaytxt: {clase: "fridaytxt", text: "Viernes"},
	saturdaytxt: {clase: "saturdaytxt", text: "Sabado"},
	hometxt: {clase: "hometxt", text: "Volver"},
    filterzonetxt: {clase: "filterzonetxt", text: "Filtrar zona"},
    searchtxt: {clase: "searchtxt", text: "Buscar"},

    //Carmen
    zoneCOS01desctxt: {text: "Alrededores del INS\n Alrededores del Hospital Calderon Guardia\n Alrededores del Tribunal Supremo de Elecciones\n Alrededores de Cuesta de Moras\n Alrededores de la Asamblea Legislativa"},
    //Merced
    zoneCOS02desctxt: {text: "Alrededores de La Corte\n Alrededores de La Clinica Biblica"},
    //Hospital
    zoneCOS03desctxt: {text: "No tiene descripción"},
    //Catedral
    zoneCOS04desctxt: {text: "Alrededores de la Municipalidad de San José\n Alrededores de Tributacion Directa\n Alrededores de Torre Mercedes\n Parque Maria Auxiliadora\n Alrededores del centro Colon"},
    //Uruca
    zoneCOS07desctxt: {text: "Alrededor del Hospital Mexico\n  Migracion y Extrangeria\n Hospital del Trauma\n Cenare."},

	/* PARQUEO*/
	startparkingtxt: {clase: "startparkingtxt", text: "Parquear"},
	activeparkingtxt: {clase: "activeparkingtxt", text: "Parqueos Activos"},
    activetxt: {clase: "activetxt", text: "Activos"},
	pnparking: {clase: "pnparking", text: "Parqueo"},
	zoneplacetxt: {clase: "zoneplacetxt", text: "Numero Espacio"},
	smsnotitxt: {clase: "smsnotitxt", text: "Notificacion SMS"},
	parktxt: {clase: "parktxt", text: "Parquear"},
	pnstartparking: {clase: "pnstartparking", text: "Parquear"},
	pnparkingdone: {clase: "pnparkingdone", text: "Parqueo Hecho"},
	pnparkingoptions: {clase: "pnparkingoptions", text: "Opciones de Parqueo"},
	pnendparking: {clase: "pnendparking", text: "Fin de Parqueo"},
	endparkingtxt: {clase: "endparkingtxt", text: "Fin Parqueo"},
	endtimetxt: {clase: "endtimetxt", text: "Hora Final"},
	totaltimetxt: {clase: "totaltimetxt", text: "Tiempo Total"},
	selectplatetxt: {clase: "selectplatetxt", text: "Selecciona Placa"},
    notparkingtxt: {clase: "notparkingtxt", text: "No tienes parqueos activos"},
    infoparktxt: {clase: "infoparktxt", text: "El espacio de parqueo es el número que esta pintado en el lugar que estas parqueando" },

	/* VEHICULOS*/
	addvechicletxt: {clase: "addvechicletxt", text: "Agregar Vehiculo"},
	viewvehiclestxt: {clase: "viewvehiclestxt", text: "Ver Vehiculos"},
	addtxt: {clase: "addtxt", text: "Agregar"},
	carplatetxt: {clase: "carplatetxt", text: "Placa:"},
	pnaddvehicles: {clase: "pnaddvehicles", text: "Agregar Vehiculo"},
	vehicletxt: {clase:"vehicletxt", text: "Vehiculo"},
    vehicletypetxt: {clase: "vehicletypetxt", text: "Tipo de Vehiculo"},
    vehiclebrandtxt: {clase: "vehiclebrandtxt", text: "Marca del Vehiculo"},
    vehicleslisttxt: {clase:"vehicleslisttxt", text: "Lista"},
    searchvehiclestxt: {clase:"searchvehicletxt", text: "Buscar Vehiculo"},
	starttimetxt: {clase: "starttimetxt", text: "Hora Inicio"},
    unparkingtxt: {clase: "unparkingtxt", text: "Desparquear"},
	placetxt: {clase: "placetxt", text: "Numero Espacio"},
	parkinginfotxt: {clase: "parkinginfotxt", text: "Información de Parqueo"},
    nocarstxt: {clase: "nocarstxt", text: "No tienes vehiculos registrados"},


	/* USER ACCOUNT*/
	profiletxt: {clase: "profiletxt", text: "Perfil"},
    yourdatatxt: {clase: "yourdatatxt", text: "Tus datos"},
	changepasstxt: {clase: "changepasstxt", text: "Cambiar Contraseña"},
	pnuserinfo: {clase: "pnuserinfo", text: "Info de Usuario"},
	oldpasstxt: {clase: "oldpasstxt", text: "Contraseña Anterior"},
	newpasstxt: {clase: "newpasstxt", text: "Contraseña Nueva"},
    dobtxt: {clase: "dobtxt", text: "Fecha de Nacimiento"},

    /* SETTINGS */
	logouttxt: {clase: "logouttxt", text: "Cerrar Sesión"},
    securitytxt: {clase: "securitytxt", text:"Seguridad"},
    languagetxt: {clase: "languagetxt", text: "Idioma"},
    optionstxt: {clase: "optionstxt", text: "Opciones"},
    versiontxt: {clase: "versiontxt", text: "Versión"},
    changetxt: {clase: "changetxt", text: "Cambiar"},

	/* BALANCE*/
	reloadbalancemsjtxt: {clase: "reloadbalancemsjtxt", text: "Recarga con Tarjeta"},
	reloadbalancetxt: {clase: "reloadbalancetxt", text: "Recarga PIN"},
	viewbalancetxt: {clase: "viewbalancetxt", text: "Ver Saldo"},
	givebalancetxt: {clase: "givebalancetxt", text: "Transferir"},
	paypalreloadtxt: {clase: "paypalpaymenttxt", text: "Tarjeta de crédito"},
	pinnumbertxt: {clase: "pinnumbertxt", text: "Numero Pin"},
	reloadtxt: {clase: "reloadtxt", text: "Recargar"},
	pnbalanceinfo: {clase: "pnbalanceinfo", text: "Saldo"},
	amounttxt: {clase: "amounttxt", text: "Cantidad"},
	givetxt: {clase: "givetxt", text: "Transferir"},
	pngivebalance: {clase: "pngivebalance", text: "Transferir"},
    cumobiletxt: {clase: "cumobiletxt", text: "CU ó Celular"},
    cumobilerepeattxt: {clase: "cumobilerepeattxt", text: "Repetir CU ó Celular"},

	menuZones: {clase: "zones", text: "Zonas"},
	menuParking: {clase: "park", text: "Parqueo"},
	alertError: {text: "xx"},

    //RECOVERY
    recoverytxt: {clase: "recoverytxt", text: "Recuperar"},
    recoverypasstxt: {clase: "recoverypasstxt", text: "Recuperar Contraseña"},

    // SHARE
    sharetxt: {clase: "sharetxt", text: "Compartir"},
    shareMessage: {text: "La mejor aplicación de #Parqueo City Parking, que esperas? ¡DESCÁRGALA YA! #Android #Iphone #Wp"},
    shareTitle: {text: "CityParking - Cellular Parking Systems"},
    shareLink: {text: "http://goo.gl/W26B8H"}
};

var LANG = defaultLang == SPANISH ? LANG_ES : LANG_EN;

function initialize(){
	for(var key in LANG){
		if( LANG[key].clase != null ){
			var elem = $("."+LANG[key].clase);
			elem.text(LANG[key].text);
			var ell = elem.attr('placeholder');
			if( typeof ell != undefined ){
				elem.attr('placeholder', LANG[key].text);
			}
		}
	}
}

function setLanguage(lang){
	defaultLang = lang;
	LANG = defaultLang == SPANISH ? LANG_ES : LANG_EN;
}

function cambiarIngles(){
	setLanguage(ENGLISH);
	initialize();
}

function cambiarEspaniol(){
	setLanguage(SPANISH);
	initialize();
}
