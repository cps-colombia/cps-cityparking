/*global $, LANG */

///// GLOBAL //////
/**
 * current server url
 */
//Production
//var url = 'https://cpsparking.ca/CAN-CPS-MOB/MobileServer';

//Debug
var url = 'http://96.45.176.18/CAN-CPS-MOB/MobileServer';
//var url = 'http://localhost:9999/CAN-CPS-MOB/MobileServer';
//var url = 'http://localhost:8081/CAN-CPS-MOB/MobileServer';
//var url = 'http://steelxhome.no-ip.org:8081/CAN-CPS-MOB/MobileServer';

/**
 * Control variables
 */
// TODO: migrate some to configuration file
var myData = [],
    myCars = [],
    zones = [],
    debug = 0,
    logueado = 0,
    session = 0,
    countryGet,
    countryData,
    arrayIndex,
    activeParkings = [],
    homePage = "parkingPage",
    pageTransition = "slidefade",
    filterPlate= /^([a-zA-Z0-9]{6})+$/,
    _hidesplash = false,
    _deviceReady = false,
    _initAnalytics = false,
    _mapsLoaded = false,
    _networkOnline = true,
    _currentPage,
    _getPlatform;

/**
 * initial  and global ajax setup
 */
$.ajaxSetup({
    url: url,
    type: "POST",
    dataType: "json",
    timeout: 30000,
    error: function(response, textStatus, errorThrown) {
        var errorSend = response.status;
        if (textStatus == "timeout" || textStatus == 'abort'){
            generalAlert(LANG.alerttimeouterror.text);
            errorSend = textStatus;
        } else if (response.status === 0 || errorThrown == "NetworkError") {
            generalAlert(LANG.alertnetworkerror.text);
        } else if (response.status == 404) {
            generalAlert(LANG.alertnetworkerror.text);
            //generalAlert(LANG.alertnotfounderror.text);
        } else if (response.status == 500) {
            generalAlert(LANG.alertservererror.text);
        } else {
            errorSend = textStatus;
            generalAlert(LANG.alertgeneralerror.text);
        }

        console.log(response);
        console.log(textStatus + " - " + errorThrown);

        if(_initAnalytics === true){
            //Send error network event register to Analytics
            rampgap.ga('send', 'event', 'error', errorSend);
            rampgap.ga('send', 'exception', errorSend, true);
        }
        _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;
        if(_currentPage == "preloadPage"){
            $(":mobile-pagecontainer").pagecontainer("change", "#login", {
                transition: "fade"
            });
        }

        $.mobile.loading('hide');
        return false;
    }
});
//// End GLOBAL /////

/**
 * Wait for device API libraries to load
 */

function onBodyLoad() {
    console.log("Load body");
    if(debug === 0){
        document.addEventListener("deviceready", onDeviceReady, false);
        document.addEventListener("offline", onOffline, false);
        document.addEventListener("online", onOnline, false);

        //Keyboard
        document.addEventListener('hidekeyboard', keyboardHideHandler, false);
        document.addEventListener('showkeyboard', keyboardShowHandler, false);
    }
}

/**
 * Check internet connectionx
 */
function onOffline() {
    console.log("Offline: "+navigator.connection.type);
    navigator.notification.alert(
        LANG.alertnetworkconnection.text, //'No network connection',
        alertDismissed,
        LANG.alerttitle.text,
        LANG.btnOk.text
    );
    _networkOnline = false;
}

function onOnline() {
    console.log("Online: "+navigator.connection.type);
    _networkOnline = true;
    if(_mapsLoaded === false && _deviceReady === true){
        // initMapsIframe();
    }
}
/**
 * Device is ready to rock!!
 */
function onDeviceReady() {

    var getPlatform = function() {
        var platform,
            platformId,
            protocol = 'file://',
            devicePlatform = $.trim(device.platform).toLowerCase();

        if (devicePlatform == 'android' ) {
            platformId = "1";
        } else if (devicePlatform == 'ios') {
            platformId = "2";
            if (navigator.userAgent.match(/(iPad.*|iPhone.*|iPod.*);.*CPU.*OS 7_\d/i)) {
                $('body').addClass('standalone');
                if ($('meta[name="apple-mobile-web-app-status-bar-style"]').attr('content') == 'black-translucent') {
                    $('body').addClass('translucent-statusbar');
                }
            }
        } else if (devicePlatform == 'win32nt' || devicePlatform == 'wince' || devicePlatform == 'windows') {
            platformId = "3";
            protocol = 'ms-appx-web://';
        } else {
            platformId = "1";
            devicePlatform = 'android';
        }
        platform = {
            platformId: platformId,
            platform: devicePlatform,
            protocol: protocol
        };
        window.localStorage.setItem('platform', JSON.stringify(platform));
        console.log("Get platform");
        return platform;
    }();

    _getPlatform = rampgap.getPlatform() || getPlatform();
    _deviceReady = true;

    //start app basic components
    getCountries();
    checkAppVersion();

    //Init Analytics  @params clientId
    if(_initAnalytics === false){
        rampgap.initAnalytics(device.uuid);
        _initAnalytics = true;
    }

    //Init the plugin getUserData when cordova ready to prevent not fire
    if(_getPlatform.platformId == "1"){
        window.getUserData.init();
    }

    //Event backbutton
    if(_getPlatform.platformId != "3"){
        document.addEventListener("backbutton", onBackKeyDown, false);
    } else {
        WinJS.Navigation.addEventListener("navigating", onBackKeyDown);
    }
}


/**
 * Backbutton event callback
 */
function onBackKeyDown(e) {
    _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;

    if(_getPlatform.platformId != "3"){

        if (_currentPage == "login" || _currentPage == homePage) {
            e.preventDefault();

            navigator.notification.confirm(
                LANG.alertexit.text,
                function(buttonIndex){
                    if (buttonIndex == 2) {
                        return false;
                    }
                    navigator.app.exitApp();
                },
                LANG.alerttitle.text,
                [LANG.btnOk.text, LANG.btnCancel.text]
            );
        } else {
            navigator.app.backHistory();
        }
    } else {
        //wp historyBack
        if(_currentPage == "login" || _currentPage == homePage){
            WinJS.Navigation.history.backStack = [];
        } else {
            if(e.detail.delta == -1){
                if(e.detail.location == "#"+homePage){
                    //prevent back to de login
                    if(_currentPage != "registerPage" && _currentPage != "resetPasswordPage"){
                        $(":mobile-pagecontainer").pagecontainer('change', e.detail.location, {
                            transition: "slide"
                        });
                    } else {
                        $(":mobile-pagecontainer").pagecontainer('change', "#login", {
                            transition: "slide"
                        });
                    }
                    WinJS.Navigation.history.backStack = [];
                } else {
                    historyBack();
                }
            }
        }

    }
}


/**
 * Notifications
 */
function alertDismissed() {
    // do something
}

function generalAlert(msg) {
    if (debug !== 0 && _deviceReady === false && _getPlatform.platformId != "3" ) {
        alert(msg);
    } else {
        navigator.notification.alert(
            msg,
            alertDismissed,
            LANG.alerttitle.text,
            LANG.btnOk.text
        );
    }
}

/**
 * load login page
 */
function showLogin(){
    $( ":mobile-pagecontainer" ).pagecontainer( 'change', "#login", { transition: "slide" });
}
/**
 * go to page back
 */
function historyBack() {
    window.history.back();
}


/**
 * KeyBoard Events
 */
function keyboardShowHandler(e){
    if($(document).find(".ui-popup-container").hasClass('ui-popup-active')){
        var popId = $(document).find(".ui-popup-active .ui-popup").attr("id");
        $("#"+popId+"-container .ui-popup-screen.in").height($(".ui-page-active").height()+$(".ui-header").outerHeight(true));
    }
    $(".cps-footer").addClass("invisible");
}

function keyboardHideHandler(e){
    if($(document).find(".ui-popup-container").hasClass('ui-popup-active')){
        var popId = $(document).find(".ui-popup-active .ui-popup").attr("id");
        $("#"+popId+"-container .ui-popup-screen.in").height($(document).height());
        // $(window).scrollTop(0);
    }

    $(".cps-footer").removeClass("invisible");
}

/**
 * put all initial things here after html document finish to load.
 */
$(document).ready(function(e) {
    onBodyLoad();

    //Version app
    $(".data-version").html("v. "+rampgap.getAppVersion());

    //Set language
    if (window.localStorage.getItem("lang")!== null){
        setLanguage(window.localStorage.getItem("lang"));
        $('#langSelect').val(window.localStorage.getItem("lang"));
        $('#langSelect [value='+window.localStorage.getItem("lang")+']').prop('selected', true).change();
    } else {
        window.localStorage.setItem("lang", "0");
    }

    //Preload
    $("#preloadPage").height($(document).height()-$('.ui-header').outerHeight( true ) || 0);
    $("#preloadPage .ui-content").height(rampgap.getContentHeigth());
    $( ".cps-header" ).addClass('invisible');

    //Init language
    initialize();

    //Debug
    if(debug !== 0){
        _getPlatform = {
            platformId: '1',
            platform: 'Android',
            protocol: 'file://'
        };
    }

    // PAGE EVENT
    $(document).on("pagecontainerbeforeshow", function(event, ui) {

        var imessage,
            prevPage = typeof ui.prevPage[0] != 'undefined' ? ui.prevPage[0].id : null;

        _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;

        // Send to veiw
        cpsView.beforeShow(_currentPage);
        cpsView.swiperShow(_currentPage);


        // Debug
        console.log("Pre-page: "+prevPage);
        console.log("Page: "+_currentPage);

        if($('#'+_currentPage).hasClass( "tutorial-page" )){
            $( ".footer-principal" ).toolbar( "hide" );
        }

        switch (_currentPage) {
        case 'preloadPage':
            // if (logueado === 1) {
            //     $.mobile.loading('show');
            //     $(":mobile-pagecontainer").pagecontainer("change", "#"+homePage, {
            //         transition: "slide"
            //     });
            // } else {
            //     $.mobile.loading('show');
            //     $(":mobile-pagecontainer").pagecontainer("change", "#login", {
            //         transition: "slide"
            //     });
            // }
            break;
        case 'login':
            //Clear input's
            rampgap.clearInput(_currentPage);

            //Check session
            if (logueado === 1) {
                $.mobile.loading('show');
                $(":mobile-pagecontainer").pagecontainer('change', "#"+homePage, {
                    transition: pageTransition
                });
            } else {
                rampgap.checkSessionPermit(logueado);
                setTimeout(function() {
                    navigator.splashscreen.hide();
                    _hidesplash = true;
                }, 2000);
            }
            if($( ".cps-header" ).hasClass('invisible')){
                $( ".cps-header" ).removeClass('invisible');
            }
            break;
        case 'parkingPage':
            if(_hidesplash === false){
                setTimeout(function() {
                    navigator.splashscreen.hide();
                    _hidesplash = true;
                }, 2000);
            }
            //Check session
            if (logueado != 1) {
                $.mobile.loading('show');
                rampgap.checkSessionPermit(logueado);
                $(":mobile-pagecontainer").pagecontainer('change', "#login", {
                    transition: pageTransition
                });
            }
            //Clear input's
            rampgap.clearInput(_currentPage);
            if($( ".cps-header" ).hasClass('invisible')){
                $( ".cps-header" ).removeClass('invisible');
            }
            // Check Additional info
            if(window.localStorage.fb_connect == "yes" && window.localStorage.fb_phone != "yes"){
                $("#popupAdditional").popup();
            }
            break;
        case 'vehiclesPage':
            //Clear input's
            rampgap.clearInput(_currentPage);
            break;
        case 'balancePage':
            //Clear input's
            rampgap.clearInput(_currentPage);
            break;
        case 'zonesViewPage':
            //initializeMap('static/images/cps-flag.png');
            break;
        case 'registerPage':
            //Clear input's
            rampgap.clearInput(_currentPage);
            //Get data info from user phone
            if(_getPlatform.platformId == "1"){
                if(window.getUserData.available){
                    $("#email").val(window.getUserData.email);

                    if(window.getUserData.displayname){
                        var gdDisplayName = window.getUserData.displayname.trim().split(/\s+/);
                        if(gdDisplayName.length >= 3){
                            $("#name").val(gdDisplayName[0]);
                            $("#last").val(gdDisplayName[2]);
                        } else if (gdDisplayName.length == 2){
                            $("#name").val(gdDisplayName[0]);
                            $("#last").val(gdDisplayName[1]);
                        } else {
                            $("#name").val(window.getUserData.displayname);
                        }
                    }

                    if(window.getUserData.phone){
                        $("#msisdn").val(window.getUserData.phone);
                    } else {
                        $("#msisdn").val(window.getUserData.profilephone);
                    }
                }
            }
            getCountries();
            break;
        case 'profilePage':
            getCountries();
            $("#btnCancelEditProfile").trigger('tap');
            break;
        }
    });

    $(document).on("pagecontainershow", function(event, ui) {
        _currentPage  = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;

        cpsView.swipeTo(0);

        switch (_currentPage) {
        case 'login':
        case homePage:
            //Enable header
            if($( ".cps-header" ).hasClass('invisible')){
                $( ".cps-header" ).removeClass('invisible');
            }
            break;
        case 'zonesViewPage':
            initializeMapSingle('static/images/cps-flag.png');
            break;
        }
        // Send page visit to Analytics
        rampgap.ga('send', 'pageview', {'page': _currentPage});
    });

    //Facebook TODO: put in a separate file
    function fbConnectLoginSuccess(userData) {

        facebookConnectPlugin.getLoginStatus( function (status) {

            console.log("current status FB: " + JSON.stringify(status.status));

            if(status.status == 'connected' ){

                facebookConnectPlugin.api("/me", ["public_profile", "user_birthday", "email","user_location","user_website"], function (fbdata) {
                    console.log("Result: " + JSON.stringify(fbdata));

                    var name = fbdata.first_name,
                        last = fbdata.last_name,
                        email = fbdata.email,
                        msisdn = fbdata.id > 15 ? fbdata.id.slice(-15) : fbdata.id,
                        pass = Math.random().toString(36).slice(-8),
                        user = fbdata.email.split("@")[0],
                        fbdate = new Date(fbdata.birthday),
                        d = fbdate.getDate(),
                        m = fbdate.getMonth() + 1,
                        y = fbdate.getFullYear(),
                        bd = y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d),
                        country = 2, //Default costa rica
                        city = 3, //Default san jose
                        checkInput = true,
                        facebookUrl = fbdata.link,
                        authResponse = status.authResponse,
                        fbLocation,
                        fbCountry,
                        fbCity;

                    window.localStorage.setItem( 'fb_connect', 'yes');
                    window.localStorage.setItem( 'fb_phone', 'no');
                    window.localStorage.setItem( 'fb_status', status.status);
                    window.localStorage.setItem( 'fb_userid', authResponse.userID);
                    window.localStorage.setItem( 'fb_session_key', authResponse.session_key);
                    window.localStorage.setItem( 'fb_accessToken', authResponse.accessToken);
                    window.localStorage.setItem( 'fb_expiresIn', authResponse.expiresIn);
                    window.localStorage.setItem( 'fb_secret', authResponse.secret);
                    window.localStorage.setItem( 'fb_sig', authResponse.sig);
                    window.localStorage.setItem( 'fb_link', facebookUrl);

                    // Get phone fromdevice if available
                    if(_getPlatform.platformId == "1"){
                        if(window.getUserData.available){
                            if(window.getUserData.phone){
                                $("#msisdn").val(window.getUserData.phone);
                                window.localStorage.setItem( 'fb_phone', 'yes');
                            } else {
                                $("#msisdn").val(window.getUserData.profilephone);
                                window.localStorage.setItem( 'fb_phone', 'yes');
                            }
                        }
                    }

                    if(typeof(fbdata.location) !== null && typeof(fbdata.location) !== undefined){
                        fbLocation = fbdata.location.name;
                    } else {
                        if(typeof(fbdata.hometown) !== null && typeof(fbdata.hometown) !== undefined){
                            fbLocation = fbdata.hometown.name;
                        }
                    }

                    if(fbLocation !== undefined){
                        fbLocation.split(",");
                        if(fbLocation.length > 0){
                            fbCity = $.trim(fbLocation[0]);
                            fbCountry = $.trim(fbLocation[fbLocation.length-1]);
                        }
                    }

                    var dataString = 'user='+ user + '&pass=' + encrypt(pass) + '&name=' + name + '&last_name=' +
                            last + '&email=' + email + '&msisdn=' + msisdn + '&country=' + country + '&city=' + city + '&bd=' + bd;

                    var options = {
                        method: 'feed',
                        name: 'CPS Cellular Parking Systems!',
                        link: 'http://www.cpsparking.ca/',
                        picture: 'https://www.cpsparking.ca/portal/images/slide1.jpg',
                        caption: '¡Ya descargué la app que hace más fácil parquear!',
                        description: 'Descarga la aplicación de CPS y disfruta de nuestros servicios al alcance de tu mano'+
                            'Disponible para: Android, iOS, Blackberry OS 5 y superior y Symbian.'
                    };
                    facebookConnectPlugin.showDialog(options, function (result) {
                        console.log("Posted. " + JSON.stringify(result));
                        $.mobile.loading("hide");
                        userRegister(dataString);
                    }, function (e) {
                        console.log("Fb Failed: " + e);
                        $.mobile.loading("hide");
                        userRegister(dataString);
                    });

                }, function (error) {
                    console.log("Fb connect Failed: " + error);
                    $.mobile.loading("hide");
                });

            } else {
                console.log("Fb connect status: "+status.status);
                $.mobile.loading("hide");
            }
        });

    }

    function fbConnectLogin() {
        facebookConnectPlugin.login(["public_profile", "user_birthday", "email","user_location","user_website"], fbConnectLoginSuccess, function (error) {
            console.log(error);
            if(error !== null){
                rampgap.ga('send', 'event', 'error', "FB "+error.errorMessage);
                rampgap.ga('send', 'exception', "FB "+error.errorMessage, true);
            }
            $.mobile.loading("hide");
        });
    }

    //SOCIAL BUTTONS

    //data for wp8 - socialsharing not support wp8.1
    function shareLinkHandler(e) {
        var request = e.request;
        request.data.properties.title = LANG.shareTitle.text;
        request.data.properties.description = LANG.shareMessage.text;
        request.data.setText(LANG.shareMessage.text+' - '+new Windows.Foundation.Uri(LANG.shareLink.text));
        //request.data.setWebLink(new Windows.Foundation.Uri(LANG.shareLink.text));
    }

    $(document).on('tap', '.cps-share', function (e) {

        if (_getPlatform.platformId == "3") {
            var dataTransferManager = Windows.ApplicationModel.DataTransfer.DataTransferManager.getForCurrentView();
            dataTransferManager.addEventListener("datarequested", shareLinkHandler);
            Windows.ApplicationModel.DataTransfer.DataTransferManager.showShareUI();
            rampgap.ga('send', 'event', 'share', 'wp');
        } else {
            window.plugins.socialsharing.share(
                LANG.shareMessage.text,
                LANG.shareTitle.text,
                null,
                LANG.shareLink.text,
                function () {
                    console.log('share ok');
                    // Send event share success to Analytics
                    rampgap.ga('send', 'event', 'share', 'success');
                },
                function (msg) {
                    console.log('error: ' + msg);
                    // Send event share error to Analytics
                    rampgap.ga('send', 'event', 'share', 'error');
                }
            );
        }
    });

    $(document).on('tap', '.btn-fb', function(e){
        rampgap.social('https://facebook.com/cpsparking');
    });

    $(document).on('tap', '.btn-tw', function(e){
        rampgap.social('https://twitter.com/cpsparking');
    });


    //Menu settings
    $(document).on('tap', '#settingsMenu', function(){//ajustes v1.0.1
        $('#languageSelect').find('form').find('span:first').html(LANG.selectlangtext.text);
    });


    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    $("#formEmailReset").on('submit', function(e) {
        e.preventDefault();
        var msg = $('#emailRecovery').val();
        if (!filter.test(msg)) {
            rampgap.alert (LANG.alerttypeemail.text);
            return false;
        }

        $.mobile.loading( 'show' );
        $.ajax({
            data: 'op=23&mail='+msg,
            dataType: "html",
            success: function(response){
                if (response == null){
                    $.mobile.loading( 'hide' );
                    return false;
                }else{
                    if (response == -117){
                        rampgap.alert (LANG.alertresetsend.text);
                        $('#emailRecovery').val('');
                        //$.mobile.changePage($("#login"));
                        $( "body" ).pagecontainer( 'change', "#login", { transition: pageTransition });
                        return false;
                    }
                    if (response == -118){
                        $.mobile.loading( 'hide' );
                        rampgap.alert (LANG.alertcheckemail.text);
                        return false;
                    }
                    $('#emailRecovery').val('');
                    $.mobile.loading( 'hide' );
                    return false;
                }
            }

        });
    });

    //LOGIN - REGISTER
    $(document).on('tap', "#btnExitApp", function(){
        navigator.notification.confirm(
            LANG.alertexit.text,
            function(buttonIndex){
                if (buttonIndex == 2) {
                    return false;
                }
                navigator.app.exitApp();
            },
            LANG.alerttitle.text,
            [LANG.btnOk.text, LANG.btnCancel.text]
        );
    });

    $("#formLogin").on('submit', function(e) {
        e.preventDefault();
        login($('#userName').val(),encrypt($('#userPass').val()));
    });

    $(document).on('tap', '#btnRegister', function(e) {
        getCountries();
        getCitiesRegister(1,0);
        $( "body" ).pagecontainer( 'change', "#registerPage", { transition: pageTransition });

    });
    $(document).on('change', '#registerCountry', function(e) {
        getCitiesRegister($('#registerCountry').val());
    });

    $(document).on('change', '#citiesSelect', function(e) {
        $('#viewZones').find('form').find('span:first').html(  $("#citiesSelect option:selected").text()  );
    });

    $(document).on('tap', "#btnConnectFb", function(){
        fbConnectLogin();
        $.mobile.loading("show");
    });

    $(document).on('tap', '#register-submit', function(){

        var name = $("#name").val(),
            last = $("#last").val(),
            email = $("#email").val(),
            msisdn = $("input#msisdn").val(),
            pass = $("#pass").val(),
            pass1 = $("#pass1").val(),
            user = $("#user").val(),
            // var address = $("#address").val(),
            // var bd = $("#registerYear").val()+'-'+$("#registerMonth").val()+'-'+$("#registerDay").val(), // (yyyy-MM-dd)
            bd = "2000-01-01", //Default date
            // var country = $('#registerCountry').val(),
            // var city = $("#registerCity").val(),
            country = 2, //Default costa rica
            city = 3, //Default san jose
            checkInput = true;

        var selectorInput = '#registerPage input[type=tel], #registerPage input[type=text], #registerPage input[type=email], #registerPage input[type=password]';

        $(selectorInput).removeClass('formError');
        $('#registerPage select').parent().removeClass('formError');

        $(selectorInput).each(function() {
            if ($(this).val().length <= 2) {
                $('#' + $(this).attr('id')).addClass('formError');
                checkInput = false;
            }
        });

        if (checkInput === false) {
            $.mobile.loading('hide');
            generalAlert(LANG.alertfields.text);
            return false;
        }else{
            // if ($("#registerYear").val() === "" || $("#registerMonth").val() === "" || $("#registerDay").val() === ""){
            //     rampgap.alert (LANG.alertbirthday.text);
            //     return false;
            // }

            var filterUser = /^([a-zA-Z0-9ñÑ_\.\-])+$/;
            if (!filterUser.test(user)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertinvaliduser.text);
                $("#user").addClass('formError');
                return false;
            }
            if (!filter.test(email)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alerttypeemail.text);
                $("#email").addClass('formError');
                return false;
            }

            var filterPhone = /^([0-9])+$/;
            if (msisdn.length > 10 || msisdn.length < 8 || !filterPhone.test(msisdn)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertinvalidphone.text);
                $("#registerPage input#msisdn").addClass('formError');
                return false;
            }

            // || address.length > 45 ||
            if (user.length > 12 || pass.length > 15 || name.length > 30 || last.length > 30  || email.length > 45) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertlongfield.text);

                if (user.length > 12) {
                    $("#user").addClass('formError');
                }
                if (pass.length > 15) {
                    $("#pass").addClass('formError');
                }
                if (name.length > 30) {
                    $("#name").addClass('formError');
                }
                if (last.length > 30) {
                    $("#last").addClass('formError');
                }
                // if (address.length > 45) {
                //     $("#address").addClass('formError');
                // }
                if (email.length > 45) {
                    $("#email").addClass('formError');
                }
                return false;
            }

            // if (country === 0 || country == "0" || country === "") {
            //     $.mobile.loading('hide');
            //     generalAlert(LANG.alertcountryfield.text);
            //     $('#registerCountry').parent().addClass('formError');
            //     return false;
            // }
            // if (city === 0 || city == "0" || city === "") {
            //     $.mobile.loading('hide');
            //     generalAlert(LANG.alertcityfield.text);
            //     $('#registerCity').parent().addClass('formError');
            //     return false;
            // }

            if(pass == pass1){
                //var dataString = 'user='+ user + '&pass=' + encrypt(pass) + '&name=' + name + '&last_name=' + last + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city;
                var dataString = 'user='+ user + '&pass=' + encrypt(pass) + '&name=' + name + '&last_name=' +
                        last + '&email=' + email + '&msisdn=' + msisdn + '&country=' + country + '&city=' + city + '&bd=' + bd;

                userRegister(dataString);
            }else{
                $.mobile.loading('hide');
                rampgap.alert(LANG.alertpasswords.text);
                $("#pass1").addClass('formError');
            }
        }
    });

    //ZONES SUB BUTTONS
    $(document).on('tap', '#btnZones', function() {
        //myData.country
        getCities(2, "#zonesPage");
    });

    $(document).on('tap', '#btnViewCityPrices', function() {
        viewZonePrices(arrayIndex,$('#daysSelect').val());
    });
    $(document).on('tap', '#btnViewCityZones', function() {
        viewZones($('#citiesSelect').val());
    });
    $(document).on('tap', '#btnLocateZones', function() {
        locateZones();
    });
    $(document).on('tap', '.viewZoneInfo', function(e) {
        arrayIndex = $(this).attr('data-place');
        viewZoneInfo(arrayIndex);
    });

    //PARKING SUB BUTTONS
    $("#btnZonesParking").on('tap', function(e){
        if(myCars.length < 1){
            $("#popupParking").popup('close');
            $(":mobile-pagecontainer").pagecontainer( 'change', "#parkingPage", { transition: pageTransition });
        } else {
            rampgap.clearInput("popupParking");
            $("#parkingPage #carPlate [value='0'], #carPlatePop [value='0']").prop('selected', true).change();
            $("#popupParking").popup('open');
        }
    });

    $(document).on('change', '#smsCheck', function() {
        if ($('#smsCheck').val() == "yes"){
            $('.notification').show();
        }else{
            $('.notification').hide();
        }

        $('#parkingSMS').val(myData.msisdn);
    });

    $(document).on('tap', '#btnParking', function() {
        myVehicles("parkingPage");
        activeParking();
    });

    $(document).on('tap', ".btnUnParking", function(){
        var me = this;
        navigator.notification.confirm(
            LANG.alertconfirmunparing.text+"\n\n"+
                LANG.vehicletxt.text+": \u0007"+activeParkings[$(me).attr("data-index")][10]+"\n"+
                LANG.starttimetxt.text+": \u0007"+activeParkings[$(me).attr("data-index")][6]+"\n"+
                LANG.placetxt.text+": \u0007"+activeParkings[$(me).attr("data-index")][8]+"\n"+
                LANG.currenttimetxt.text+": \u0007"+activeParkings[$(me).attr("data-index")][1]+"\n"+
                LANG.minutepricetxt.text+": \u0007"+"CRC "+activeParkings[$(me).attr("data-index")][4]+"\n",
            function(buttonIndex) {
                endParking(buttonIndex, activeParkings[$(me).attr("data-index")][10]);
            },
            LANG.alertconfirmtitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
        );
    });

    $("#parkingFormPopup, #parkingForm").on('submit', function(e){
        e.preventDefault();

        var formId = $(this).attr("id"),
            sms = 0,
            msisdnAux = myData.msisdn,
            carPlate = formId == "parkingFormPopup" ? "#carPlatePop" : "#carPlate",
            parkingPlace = formId == "parkingFormPopup" ? "#parkingPlacePop" : "#parkingPlace";


        if ($(carPlate).val() === "" || $(carPlate).val() == "0" || $(parkingPlace).val() === ""){
            rampgap.alert(LANG.alertfields.text);
            return false;
        }else{
            if ($('.notification').is(':visible')){
                if ($('#parkingSMS').val() === ""){
                    rampgap.alert(LANG.alertfields.text);
                    return false;
                }else{
                    sms = 1;
                    msisdnAux = $('#parkingSMS').val();
                }
            }else{
                sms = 0;
            }
            $("input, textarea").blur();
            startParking($(carPlate).val(),msisdnAux,$(parkingPlace).val(),sms);
        }
    });


    $(document).on('tap', "#btnPark", function(e){
        var _v = $("#btnPark");
        if(_v.attr('data-buttonindex') !== undefined || _v.attr('data-buttonindex') != 'undefined' ){
            confirmParking(_v.attr('data-buttonindex'), _v.attr('data-plate'),
                           _v.attr('data-num'), _v.attr('data-idp'), _v.attr('data-sms'));
        }
    });


    //VEHICLES SUB BUTTONS
    $(document).on('tap', '#btnVehicles', function(){
        myVehicles("vehiclesPage");
    });

    $(document).on('tap', "#btnLinkAddvehicle", function(e){
        rampgap.clearInput("popupAddVehicle");
        $("#popupAddVehicle").popup('open');
    });

    $("#formAddVehiclePop, #formAddVehicle").on('submit', function(e){
        e.preventDefault();
        console.log($(this).attr("id"));
        var formId = $(this).attr("id"),
            newPlateInput = formId == "formAddVehiclePop" ? "#newPlatePop" : "#newPlate",
            newTypeInput = formId == "formAddVehiclePop" ? "#newTypePop" : "#newType",
            newBrandInput = formId == "formAddVehiclePop" ? "#newBrandPop" : "#newBrand";

        if ($(newPlateInput).val() === ""){
            rampgap.alert(LANG.alertcarplate.text);
        } else if (!filterPlate.test($(newPlateInput).val())) {
            rampgap.alert(LANG.alertcarplatenovalid.text);
        } else if ($(newBrandInput).val() === ""){
            rampgap.alert(LANG.alertcarbrand.text);
        } else if ($(newTypeInput).val() === ""){
            rampgap.alert(LANG.alertcartype.text);
        }else{
            addVehicle($(newPlateInput).val().toUpperCase(),
                       $(newTypeInput).val().toUpperCase(),
                       $(newBrandInput).val().toUpperCase());
            $("input, textarea").blur();
        }
    });

    $(document).on('tap', ".deleteItem", function(e) {
        deleteCar($(this).parent().attr('data-plate'), $(this).parent());
    });

    //ACCOUNT BUTTONS
    $(document).on('tap', "#btnProfile", function(){
        //$('#profilePage select').prop('disabled', true);
        viewAccountInfo();
    });

    $("#formChangePass").on('submit', function(e){
        e.preventDefault();
        if($('#passCurrent').val() === "" || $('#passNew').val() === "" || $('#passNew').val() != $('#repassNew').val()){
            rampgap.alert(LANG.alertpassequals.text);
        }else{
            savePassword($('#passCurrent').val(),$('#passCurrent').val(),$('#passNew').val());
        }
        rampgap.clearInput("formChangePass");
    });
    $(document).on('click', '#btnEditProfile', function(){
        $('#profileForm').find('input,select').each(function() {
            $(this).removeAttr('disabled');
            $(this).removeClass('ui-state-disabled');
        });
        //$('#userInfoForm select').prop('disabled', false);

        //Fix select city
        var currentCity = $('#profileCity').parent().find('span').text();
        $('#profileCity option').each(function() {
            if ($(this).text() == currentCity) {
                $(this).prop('selected', true).change();
            }
        });

        $('#profileCU').attr('readonly', 'readonly').removeClass('ui-state-disabled');
        $('#profileLogin').attr('readonly', 'readonly').removeClass('ui-state-disabled');

        $('#profileForm').trigger("refresh");
        $('#btnEditProfile').addClass('nd');
        $('#btnSaveProfile,#btnCancelEditProfile').removeClass('nd');
    });
    $(document).on('click', '#btnCancelEditProfile', function(){
        viewAccountInfo();
        $('#profileForm').find('input,select').each(function() {
            $(this).attr("disabled","disabled");
            $(this).addClass('ui-state-disabled');
        });

        $('#profileCU').attr('readonly', 'readonly').removeClass('ui-state-disabled');
        $('#profileLogin').attr('readonly', 'readonly').removeClass('ui-state-disabled');

        $('#userInfoForm').trigger("refresh");
        $('#btnEditProfile').removeClass('nd');
        $('#btnSaveProfile,#btnCancelEditProfile').addClass('nd');
    });

    $(document).on('click', '#btnSaveProfile', function(){
        navigator.notification.confirm(
            LANG.alertconfirmmodifyinfo.text,
            saveUserUpdate,
            LANG.alertconfirmtitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
        );
    });

    $(document).on('tap', '#btnChangeUser', function(){
        navigator.notification.confirm(
            LANG.alertlogout.text,
            changeUser,
            LANG.alertconfirmtitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
        );
    });

    //BALANCE
    $(document).on('tap', "#btnBalance", function(){
        viewBalance("#balancePage");
    });

    function viewBalance(page){
        $.mobile.loading( 'show' );
        $.ajax({
            data: 'op=2&user='+myData.login+'&pass='+myData.pass,
            success: function(response){
                if (response == null){
                    $.mobile.loading( 'hide' );
                    return false;
                }else{
                    if (response == null){
                        $.mobile.loading( 'hide' );
                        return false;
                    }
                    $.mobile.loading( 'hide' );
                    //NT - Actualizando saldo de usuario en SESSION
                    rampgap.updateBalance(response.balance);
                    var balance = '<span>'+LANG.balancecurrenttxt.text+': CRC '+response.balance+'</span>';
                    $('#profileBalanceContent').html(balance);

                    if(page !== null && page !== undefined){
                        $(":mobile-pagecontainer").pagecontainer( 'change', page, { transition: pageTransition });
                    }

                }
            }
        });
    }

    $(document).on('tap', '#reloadBalancePin', function() {
        var pin = $('#pinNumber').val();
        pin = pin.replace(' ','');
        if (pin === "" ){
            rampgap.alert (LANG.alerrtypepin.text);
            return false;
        }

        $.mobile.loading( 'show' );
        $.ajax({
            data: 'op=13&user='+myData.login+'&pass='+myData.pass+'&pin='+pin,
            success: function(response){
                console.log(response);
                if (response === null){
                    $.mobile.loading( 'hide' );
                    return false;
                }else{
                    if (response === ""){
                        $.mobile.loading( 'hide' );
                        return false;
                    }
                    if (response.code == -101){
                        $.mobile.loading( 'hide' );
                        rampgap.alert(LANG.alertinvalidpin.text);
                        return false;
                    }
                    $('#pinNumber').val('');
                    //NT - Actualizando saldo de usuario en SESSION
                    rampgap.updateBalance(response.obj.balance);
                    $.mobile.loading( 'hide' );
                    rampgap.alert (LANG.alertreloadok.text);

                    viewBalance();

                }
            }
        });
    });

    $(document).on('tap', "#btnAddbalanceMSJ", function(){
        reloadMSJ();
    });

    /**
     * Button to fire give balance event
     */
    $("#formGiveBalance").on('submit', function(e) {
        e.preventDefault();
        var repass = $('#givePassInput').val();
        var cu = $('#giveCUInput').val();
        var recu = $('#regiveCUInput').val();
        var amount = $('#giveAmountInput').val();

        cu = $.trim(cu);
        recu = $.trim(recu);
        amount = $.trim(amount);
        amount = amount.replace('.','');
        amount = amount.replace(',','');

        if (cu === "" || recu === "" || cu != recu ){
            rampgap.alert (LANG.alertcheckcu.text);
            return false;
        }else{
            if (amount === ""){
                rampgap.alert(LANG.alertamount.text);
                return false;
            }
        }

        $.mobile.loading( 'show' );
        $.ajax({
            data: 'op=14&user='+myData.login+'&pass='+myData.pass+'&cu='+cu+'&amm='+amount+'&repass='+encrypt(repass),
            success: function(response){
                if (response === null){
                    $.mobile.loading( 'hide' );
                    return false;
                }else{
                    if (response == "") {
                        $.mobile.loading('hide');
                        rampgap.alert(LANG.alertcheckdata.text);
                        return false;
                    } else if(response.code == -1 ) {
                        $.mobile.loading('hide');
                        rampgap.alert(LANG.alertinvalidcu.text);
                        return false;
                    } else if (response.code == -2) {
                        $.mobile.loading('hide');
                        rampgap.alert(LANG.alertcheckdata.text);
                        return false;
                    } else if(response.code == -112){
                        //NT - Actualizando saldo de usuario en SESSION
                        rampgap.updateBalance(response.obj.balance);

                        $('#givePassInput').val('');
                        $('#giveCUInput').val('');
                        $('#regiveCUInput').val('');
                        $('#giveAmountInput').val('');
                        $.mobile.loading('hide');
                        $("input").blur();
                        rampgap.alert(LANG.alertgiveok.text);

                        viewBalance();
                    }

                }
            }
        });
    });

    /**
     * pupulate city wheter country
     */
    $('#profileCountry').change(function(e) {
        getCitiesRegister($('#profileCountry').val());
    });


    $(document).on('change', "#carPlate", function(e) {
        $('#startParking').find('form').find('span:first').html($(this).val());
    });

    /**
     * Button to change language
     */
    $(document).on('tap', "#saveLang", function(e){

        e.preventDefault();
        $.mobile.loading( 'show' );
        window.localStorage.setItem("lang", $('#langSelect').val());
        setLanguage($('#langSelect').val());
        $('#langSelect [value='+window.localStorage.getItem("lang")+']').prop('selected', true).change();
        initialize();
        $.mobile.loading( 'hide' );

        $(":mobile-pagecontainer").pagecontainer( 'change', "#configPage", { transition: pageTransition });

    });
    /**
     * on click sends suggestion
     */

    $("#btnAboutSugge").on('tap', function(){
        $("#suggestiontxt").val("");
        $("#popupSendSugge").popup('open');
    });

    $("#formSugge").on('submit', function(e) {
        e.preventDefault();
        var msg = $('#suggestiontxt').val();
        if ($.trim(msg).length <= 3){
            rampgap.alert (LANG.alertmsgempty.text);
            return false;
        }

        $.mobile.loading( 'show' );
        $.ajax({
            data: 'op=19&user='+myData.login+'&pass='+myData.pass+'&sug='+msg,
            dataType: "html",
            success: function(response){
                if (response == null){
                    $.mobile.loading( 'hide' );
                    return false;
                }else{
                    if (response == ""){
                        $.mobile.loading( 'hide' );
                        return false;
                    }
                    if (response == "[]"){
                        $.mobile.loading( 'hide' );
                        rampgap.alert (LANG.alertmsgempty.text);
                        return false;
                    }
                    $('#suggestiontxt').val('');
                    $.mobile.loading( 'hide' );
                    rampgap.alert (LANG.suggestionsend.text);

                    $("#popupSendSugge").popup('close');

                }
            }
        });
    });
    //****************************************************************************************************************************************************** END READY************************************************
});

// LOGIN -----------------------------------------------------------------

function changeUser(buttonIndex) {

    if (buttonIndex == 2) {
        return false;
    }

    rampgap.setItem('login', null);
    rampgap.setItem('pass', null);

    window.localStorage.setItem('logueado', "0");
    window.localStorage.removeItem('login');
    window.localStorage.removeItem('pass');

    session = 0;
    logueado = 0;

    rampgap.checkSessionPermit(logueado);
    rampgap.destroySession();
    $(":mobile-pagecontainer").pagecontainer( 'change', "#login", { transition: pageTransition });
}

function checkAppVersion() {
    console.log(_getPlatform);
    var returnCheck = false,
        platformId = _getPlatform.platformId;

    $.mobile.loading('show');

    checkSession();
    returnCheck = true;
    // $.ajax({
    //     data: 'op=28&deviceid=' + platformId + '&version=' + rampgap.getAppVersion(),
    //     async: false,
    //     success: function(response) {
    //         console.log('New version?:' + response);
    //         if (response == false) {
    //             checkSession();
    //             returnCheck = true;
    //         } else if (response == "") {
    //             generalAlert(LANG.alerttimeouterror.text);
    //         } else {

    //             setTimeout(function() {
    //                 navigator.notification.alert(
    //                     LANG.alertnewupdate.text,
    //                     function() {
    //                         _storeLink(platformId);
    //                     },
    //                     'CPS PARKING',
    //                     'Ok'
    //                 );
    //             }, 100);
    //             returnCheck = false;
    //         }
    //         $.mobile.loading('hide');
    //     }

    // });
    return returnCheck;
}

/**
 * Connect to APP Store
 */
function _storeLink(platformId) {
    var myURL;

    switch (platformId) {
    case "1":
        CanOpen('market://', function (isInstalled) {
            if (isInstalled) {
                myURL = encodeURI('market://details?id=appandroidcity.cps');
            } else {
                myURL = encodeURI('https://play.google.com/store/apps/details?id=appandroidcity.cps');
            }
            window.open(myURL, '_system');
        });
        break;
    case "2":
        myURL = encodeURI('https://itunes.apple.com/us/app/cityparking/id868140927?ls=1');
        window.open(myURL, '_system');
        break;
    case "3":
        myURL = encodeURI('zune:navigate?appid=e76a319a-665f-4d09-9e15-2eaab2f660d4');
        window.open(myURL, '_system');
        break;
    }
}

/**
 * verify if current user has a valid session and localStorage
 */
function checkSession(){
    console.log("Logueado?: "+window.localStorage.getItem('logueado'));

    if (rampgap.getItem("login") !== null && rampgap.getItem("login") && rampgap.getItem("pass") !== null &&
        rampgap.getItem("login") != 'null' && rampgap.getItem("pass") != 'null'){

        var _passSession = rampgap.getItem("pass");
        //Migration to pass encrypted from the start
        if(_passSession.length < 29){
            _passSession = encrypt(_passStorage);
        }
        login(rampgap.getItem("login"),_passSession);

    } else if (window.localStorage.getItem('logueado') == "1") {

        var _loginStorage = window.localStorage.getItem('login'),
            _passStorage = window.localStorage.getItem('pass');
        //Migration to pass encrypted from the start
        if(_passStorage.length < 29){
            _passStorage = encrypt(_passStorage);
        }
        login(_loginStorage, _passStorage);

    }else{
        rampgap.checkSessionPermit(logueado);
        $(":mobile-pagecontainer").pagecontainer( 'change', "#login", { transition: "fade" });
        $.mobile.loading( 'hide' );
    }
}

/**
 * Request for login from server. on success go to menu and keep a valid session
 * @param user valid username
 * @param pass valid password
 *
 */
function login(user,pass){
    /*
     SUCCES
     {"idsysUser":"3","address":"Kra 41G #47-12","balance":"$57.925","birthDate":"1984-11-19","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
     */
    $.mobile.loading( 'show' );

    if (user === "" || pass.length == 14) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertemptyusername.text);
        return false;
    }

    $.ajax({
        data: 'op=0&user=' + user + '&pass=' + pass + '&device=' + _getPlatform.platform,
        success: function(response){
            if (response == null){
                console.log("Login null");
                $.mobile.loading( 'hide' );
                rampgap.alert(LANG.alertusername.text);

                window.localStorage.setItem('logueado', "0");
                session = 0;
                logueado = 0;

                if(_currentPage != "login"){
                    $(":mobile-pagecontainer").pagecontainer("change", "#login", {
                        transition: pageTransition
                    });
                }
                return false;
            }else{
                console.log("Login valid...");
                console.log('Success - Hi: ' + response.login);
                //NT - Seteando values de usuario
                rampgap.setUserSession(response.login, pass, response.balance);
                //NT - Sincronizando Active Parking
                rampgap.synchActiveParking();
                //User Data
                myData.country = response.countryIdcountry;
                myData.city = response.city_idcity.idcity;
                myData.msisdn = response.favoriteMsisdn;
                myData.idu = response.idsysUser;
                myData.address = response.address;
                myData.balance = response.balance;
                myData.cu = response.cu;
                myData.email = response.email;
                myData.usertype = response.idsysUserType;
                myData.name = response.name;
                myData.lastname = response.lastName;
                myData.login = response.login;
                myData.pass = pass;

                $.mobile.loading( 'hide' );

                //Set data user in menu and home
                $('.data-cu').text(myData.cu);
                $('.data-fullname').text(myData.name.split(/[\s,]+/)[0]+" "+myData.lastname.split(/[\s,]+/)[0]);

                // Save in session
                if (session === 0){
                    rampgap.setItem("login",  response.login);
                    rampgap.setItem("pass",  pass);
                    session = 1;
                }
                //Save login in localStorage
                if (window.localStorage.getItem('logueado') != '1') {
                    window.localStorage.setItem('logueado', "1");
                }

                if(logueado === 0){
                    window.localStorage.setItem('login', response.login);
                    window.localStorage.setItem('pass', pass);
                    logueado = 1;
                }

                //Update vehicles count
                //myVehicles(null, 'updateCount');
                myVehicles();
                activeParking();

                //Change page for login users
                if(_currentPage == "preloadPage"){
                    $(":mobile-pagecontainer").pagecontainer( 'change', "#"+homePage, { transition: "fade" });
                } else {
                    $(":mobile-pagecontainer").pagecontainer( 'change', "#"+homePage, { transition: pageTransition });
                }

                //Enable content for login user
                rampgap.checkSessionPermit(logueado);

                // Start Session and set user login in Analytics
                rampgap.ga('set', 'uid', response.login);
            }
        },
        error: function(response, textStatus, errorThrown) {
            console.log('ERROR' + JSON.stringify(response));
            if (response.status == 1001 || response.responseText === "") {
                generalAlert(LANG.alertusername.text);
                if(_currentPage != "login"){
                    $(":mobile-pagecontainer").pagecontainer("change", "#login", {
                        transition: "fade"
                    });
                }
                window.localStorage.setItem('logueado', "0");
                session = 0;
                logueado = 0;
                rampgap.checkSessionPermit(logueado);
                if(_initAnalytics === true){
                    rampgap.ga('send', 'event', 'error', 'login'+response.status);
                    rampgap.ga('send', 'exception', 'Error login: '+response.status, true);
                }
            }
            $.mobile.loading('hide');
        }
    });
}
/**
 * register a new user in the System,
 * @param dataString contains different parameter information
 */
function userRegister(dataString){

    /*DATOS PARA REGISTRO
     user
     pass
     name
     last_name
     address
     email
     bd (yyyy-MM-dd)
     country
     msisdn
     city
     */

    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=1&'+dataString,
        success: function(response){
            if (response === null){
                $.mobile.loading( 'hide' );
                rampgap.alert(LANG.alertalreadyregister.text);
                $("#user, #email, #msisdn").addClass('formError');

                //Send error event register to Analytics
                rampgap.ga('send', 'event', 'register', 'error');
                return false;
            }else{
                $.mobile.loading( 'hide' );
                rampgap.alert(LANG.alertregisterok.text);

                $('#registerPage input[type=tel], #registerPage input[type=text], #registerPage input[type=email], #registerPage input[type=password]').val('');
                $('#registerCity [value="0"]').prop('selected', true).change();
                $(":mobile-pagecontainer").pagecontainer('change', "#login", {
                    transition: pageTransition
                });

                login(response.obj.login, encrypt(response.obj.pass));

                //Send success event register to Analytics
                rampgap.ga('send', 'event', 'register', 'success');

            }
        },
        error: function(response, textStatus, errorThrown) {
            //if(response.status==1001 || response.status==0)
            //{
            rampgap.alert(LANG.alertalreadyregister.text);
            $("#user, #email, #msisdn").addClass('formError');
            rampgap.ga('send', 'event', 'register', 'error');
            //}
            $.mobile.loading('hide');
        }
    });
}

// ------------------------------------------------------------------------------ ZONES ------------------------------------------------------------------------------------------
/**
 * return cities by country code
 * @param countryCode, the country code.
 */
function getCities(countryCode, page){
    $.mobile.loading( 'show' );
    var cities = '';
    $.ajax({
        data: 'op=8&idc='+countryCode,
        success: function(response){
            if (response == null) {
                $.mobile.loading('hide');
                return false;
            } else {
                for (var i = 0; i < response.length; i++) {
                    if (i == 0) {
                        cities += '<option value="' + response[i].idcity + '" selected="selected">' + response[i].name + '</option> ';
                        var currentCity = response[i].name; //ajustes v1.0.1
                    } else {
                        cities += '<option value="' + response[i].idcity + '">' + response[i].name + '</option> ';
                    }
                }

                $.mobile.loading('hide');

                //Fix error append wp8.1
                cities = rampgap.toStatic(cities);

                $(page + ' #citiesSelect').html(cities);
                //myData.city
                $(page + ' #citiesSelect [value='+3+']').prop('selected', true).change();

                $(":mobile-pagecontainer").pagecontainer('change', page, {
                    transition: pageTransition
                });

            }
        }
    });
}

/**
 *
 * @param cityCode
 */
function viewZones(cityCode){
    $.mobile.loading( 'show' );
    var results = '';
    $.ajax({
        data: 'op=7&user='+myData.login+'&pass='+myData.pass+'&idc='+myData.country+'&idcity='+cityCode,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                for (var i = 0; i < response.length; i++) {
                    zones[i]= new Array(response[i].idzone,response[i].clientIdclient,response[i].closeTime,response[i].minuteValue,response[i].latitude,response[i].longitude,response[i].name,response[i].openTime,response[i].places);

                    results += '<li>'+
                        '<a href="#" data-place="'+i+'" data-place-name="'+response[i].name+'" class="viewZoneInfo" >'+
                        response[i].name +
                        '<span class="price">'+LANG.pricetxt.text+' CRC'+response[i].minuteValue+'</span>'+
                        '<span class="ui-li-count">'+response[i].places+'</span>'+
                        '</a>'+
                        '</li>';
                }

                $('#zoneResultList').html(results);
                $('#zoneResultList').listview( "refresh" );
                $.mobile.loading( 'hide' );
            }
        }
    });
}

var weekDays = new Array ('','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

function viewZonePrices(zoneArrayIndex,zonePricesDay){
    $.mobile.loading( 'show' );
    var pricesR = '';
    $.ajax({
        data: 'op=9&user='+myData.login+'&pass='+myData.pass+'&idc='+myData.country+'&idz='+zones[zoneArrayIndex][0],
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                for (var i = 0; i < response.length; i++) {
                    if (response[i].day == zonePricesDay){
                        pricesR += '<table cellspacing="0" cellpadding="0">';
                        pricesR += '<tr class="title"><td>'+response[i].startTime+' to '+response[i].endTime+'</td></tr>';
                        if (response[i].maxMinutes == -1){
                            pricesR += '<tr><td><strong>'+LANG.maxtimetxt.text+': </strong>No restriction</td></tr>';
                        }else{
                            pricesR += '<tr><td><strong>'+LANG.maxtimetxt.text+': </strong>'+response[i].maxMinutes+'</td></tr>';
                        }
                        pricesR += '<tr><td><strong>'+LANG.minutepricetxt.text+': </strong>CRC '+response[i].price+'</td></tr>';
                        pricesR += '</table>';
                    }
                }

                $('#zonePrices').html(pricesR);
                $.mobile.loading( 'hide' );
                $('#viewZonePrices').find('.footer').html('<h4 class="ui-title" tabindex="0" role="heading" aria-level="1">'+weekDays[zonePricesDay]+'</h4>');
                //.mobile.changePage($("#viewZonePrices"));
                $( "body" ).pagecontainer( 'change', "#viewZonePrices", { transition: pageTransition });

            }
        }
    });
}

function viewZoneInfo(zoneArrayIndex){
    $('#zonesViewPage .map-info .name').html(zones[zoneArrayIndex][6]);
    $('#zonesViewPage .content-info .hora').html(zones[zoneArrayIndex][7].toLowerCase()+" - "+zones[zoneArrayIndex][2].toLowerCase() + " L-S");
    $('#zonesViewPage .content-info .price').html(LANG.minutepricetxt.text+": CRC "+zones[zoneArrayIndex][3]);
    $('#zonesViewPage .content-info .description p').html(LANG["zone"+zones[zoneArrayIndex][0]+"desctxt"].text);

    viewZoneMap(zoneArrayIndex);
}

function viewZoneMap(zoneArrayIndex){
    rampgap.setCurrentLatLong(zones[zoneArrayIndex][4]+","+zones[zoneArrayIndex][5]);

    $(":mobile-pagecontainer").pagecontainer('change', "#zonesViewPage", {
        transition: pageTransition
    });
}

function viewZoneMapCar(carlat,carlong){
    rampgap.setCurrentLatLong(carlat+","+carlong);
    rampgap.showCarMap();
}

function locateZones(){
    rampgap.verifyGPSIsActive();
}

function afterGPSGetLocation(clat, clong){
    var results = '';
    $.ajax({
        data: 'op=16&user='+myData.login+'&pass='+myData.pass+'&lat='+clat+'&lon='+clong,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                if (response == ""){
                    rampgap.alert (LANG.alertnozone.text);
                    $.mobile.loading( 'hide' );
                    return false;
                }else{
                    for (var i = 0; i < response.length; i++) {

                        //results += '<a class="btn-big arrow result viewZoneOptions" rel="'+i+'" title="'+response[i].name+'" href="#"><p><strong>Name: </strong>'+response[i].name+'<br/><strong>Places: </strong>'+response[i].places+'<br/><strong>Price: $</strong>'+response[i].hourValue+'</p></a>';

                        zones[i]= new Array(response[i].idzone,response[i].clientIdclient,response[i].closeTime,response[i].minuteValue,response[i].latitude,response[i].longitude,response[i].name,response[i].openTime,response[i].places);
                        //results += '<a class="btn-big arrow result viewZoneOptions" rel="'+i+'" title="'+response[i].name+'" href="#"><p><strong>Name: </strong>'+response[i].name+'<br/><strong>Places: </strong>'+response[i].places+'<br/><strong>Price: $</strong>'+response[i].hourValue+'</p></a>';
                        results += '<li data-theme="a" class="ui-first-child ui-last-child ui-li-has-thumb">';
                        results += '    <a href="#" rel="'+i+'" title="'+response[i].name+'" class="ui-btn ui-btn-icon-right ui-icon-carat-r viewZoneOptions">';
                        results += '        <img src="img/ico-zones.png" class="ui-li-thumb">';
                        results += '        <h3 class="ui-li-heading">'+response[i].name+'</h3>';
                        results += '        <p class="ui-li-desc"><strong>'+LANG.placestxt.text+': </strong>'+response[i].places+'<br><strong>'+LANG.pricetxt.text+': CRC</strong>'+response[i].minuteValue+'</p>';
                        results += '    </a>';
                        results += '</li>';
                    }
                    $('#zoneResultList').html(results);
                    $.mobile.loading( 'hide' );
                    //$.mobile.changePage($("#viewZonesResult"));
                    $( "body" ).pagecontainer( 'change', "#viewZonesResult", { transition: pageTransition });

                    $('.viewZoneOptions').click(function() {
                        //viewZoneOptions($(this).attr('rel'),$(this).attr('title'));
                    });
                }
            }
        }
    });
}


// ------------------------------------------------------------------------------ /ZONES ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------ PARKING ------------------------------------------------------------------------------------------

function startParking(plate,num,idp,sms){
    /*
     SUCCES
     {"code":0,"description":"OK","obj":{"totalMins":0,"maxMins":30,"minuteValue":"0.03","msisdn":"3006658900","startTime":"2011-11-08 02:36:06","endTime":"2011-11-08 02:36:06","idPlace":"01","vehiclePlate":"BJJ118","total":"0.00","userBalance":"204.52","balance":"204.52","minValue":"0.03"}}

     PARQUEADO
     {"code":-113,"description":"Error"}

     VEHICULO NO EXISTE
     {"code":-121,"description":"Error"}

     ZONA NO EXISTE
     {"code":-106,"description":"Error"}

     */

    plate = plate.replace(' ','');
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=24&user='+myData.login+'&pass='+myData.pass+'&plate='+plate+'&num='+num+'&idc='+myData.country+'&idp='+idp+'&idu='+myData.idu+'&sms='+sms,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false
            }else{

                $.mobile.loading( 'hide' );
                if (response.code == 0){
                    //NT - Agregando ActiveParking para procesamiento de Notificaciones
                    rampgap.addActiveParking(
                        response.obj.vehiclePlate,
                        response.obj.minuteValue,
                        response.obj.maxMins
                    );

                    $(".value_vehicle").text(response.obj.vehiclePlate.toUpperCase());
                    $(".value_starttime").text(response.obj.startTime);
                    $(".value_place").text(response.obj.idPlace);
                    $(".value_balance").text(LANG.balancecurrenttxt.text+": "+response.obj.userBalance);
                    $(".value_minuteprice").text(response.obj.minuteValue);

                    $("#btnPark").attr({
                        'data-buttonindex': 1,
                        'data-plate': plate,
                        'data-num': num,
                        'data-idp': idp,
                        'data-sms': sms
                    });


                    if($("#popupParking").parent().hasClass("ui-popup-active")){
                        $("#popupParking").popup('close');
                        $("#popupParking").on( "popupafterclose", function( event, ui ) {
                            $("#popupConfirParking").popup('open');
                            $("#popupParking").off( "popupafterclose");
                        });
                    } else {
                        $("#popupConfirParking").popup('open');
                    }

                }
                if (response.code == -113){
                    rampgap.alert(LANG.alertalreadyparked.text);
                    return false;
                }
                else if (response.code == -121){
                    rampgap.alert(LANG.alertvehiclenoregistered.text);
                    return false;
                }
                else if (response.code == -120){
                    rampgap.alert(LANG.alertplaceclosed.text);
                    return false;
                }
                else if (response.code == -106){
                    rampgap.alert(LANG.alertplacewrong.text);
                    return false;
                }
                else if (response.code == -127){
                    rampgap.alert(LANG.alertplacenotexist.text);
                    return false;
                }
                else if (response.code == -128){
                    rampgap.alert(LANG.alertplacenot.text);
                    return false;
                }
                else if(response.code == -103){
                    rampgap.alert(LANG.alertnobalance.text);
                    $(":mobile-pagecontainer").pagecontainer('change', "#balancePage", { transition: pageTransition });
                    return false;
                }
            }
        }
    });
}




function confirmParking(buttonIndex, plate,num,idp,sms){

    /*
     SUCCES
     {"code":0,"description":"OK","obj":{"totalMins":0,"maxMins":30,"minuteValue":"0.03","msisdn":"3006658900","startTime":"2011-11-08 02:36:06","endTime":"2011-11-08 02:36:06","idPlace":"01","vehiclePlate":"BJJ118","total":"0.00","userBalance":"204.52","balance":"204.52","minValue":"0.03"}}

     PARQUEADO
     {"code":-113,"description":"Error"}

     VEHICULO NO EXISTE
     {"code":-121,"description":"Error"}

     ZONA NO EXISTE
     {"code":-106,"description":"Error"}

     */

    if (buttonIndex === 2) {
        return false;
    }

    plate = plate.replace(' ','');
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=6&user='+myData.login+'&pass='+myData.pass+'&plate='+plate+'&num='+num+'&idc='+myData.country+'&idp='+idp+'&idu='+myData.idu+'&sms='+sms,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{

                $.mobile.loading( 'hide' );
                if (response.code == 0){
                    //NT - Agregando ActiveParking para procesamiento de Notificaciones
                    rampgap.addActiveParking(
                        response.obj.vehiclePlate,
                        response.obj.minuteValue,
                        response.obj.maxMins
                    );

                    var parkDoneResult = LANG.pnparkingdone.text+"\n\n"+
                            LANG.vehicletxt.text+": \u0007"+response.obj.vehiclePlate+"\n"+
                            LANG.starttimetxt.text+": \u0007"+response.obj.startTime+"\n"+
                            LANG.placetxt.text+": \u0007"+response.obj.idPlace+"\n"+
                            LANG.balancetxt.text+": \u0007"+response.obj.userBalance+"\n"+
                            LANG.minutepricetxt.text+": \u0007"+response.obj.minuteValue+"\n";

                    $("#popupConfirParking").popup('close');

                    $("#parkingPage #carPlate [value='0'], #carPlatePop [value='0']").prop('selected', true).change();
                    rampgap.clearInput('parkingPage');
                    rampgap.alert(parkDoneResult);
                    activeParking();

                    //$("#popupParking").off( "popupafterclose");

                }
                if (response.code == -113){
                    rampgap.alert (LANG.alertalreadyparked.text);
                    return false;
                }
                else if (response.code == -121){
                    rampgap.alert (LANG.alertvehiclenoregistered.text);
                    return false;
                }
                else if (response.code == -120){
                    rampgap.alert (LANG.alertplaceclosed.text);
                    return false;
                }
                else if (response.code == -106){
                    rampgap.alert (LANG.alertplacewrong.text);
                    return false;
                }
                else if (response.code == -127){
                    rampgap.alert (LANG.alertplacenotexist.text);
                    return false;
                }
                else if (response.code == -128){
                    rampgap.alert (LANG.alertplacenot.text);
                    return false;
                }
                else if(response.code == -103){
                    rampgap.alert(LANG.alertnobalance.text);
                    return false;
                }
            }
        }
    });
}

function activeParking(){
    /*
     idUser = request.getParameter("user");
     op = 4

     ACTIVE PARKING
     [{"idactiveParking":"1","currentMins":1039,"enableSMSNotif":0,"maxMins":3598,"minuteValue":0.025,"platformIdplatform":1,"startTime":"Aug 18, 2011 5:15:23 PM","zoneIdzone":"01","idHistory":"78","vehiclePlate":"BJJ118","idSysUser":"3"}]

     VARIOUS PARKINGS
     [{"idactiveParking":"12","currentMins":0,"enableSMSNotif":0,"maxMins":2456,"minuteValue":0.025,"platformIdplatform":1,"startTime":"Aug 19, 2011 10:39:15 AM","zoneIdzone":"01","idHistory":"89","vehiclePlate":"KJJ493","idSysUser":"3"},{"idactiveParking":"13","currentMins":0,"enableSMSNotif":0,"maxMins":2456,"minuteValue":0.025,"platformIdplatform":1,"startTime":"Aug 19, 2011 10:39:27 AM","zoneIdzone":"01","idHistory":"90","vehiclePlate":"BJJ118","idSysUser":"3"}]
     */
    var resultP = '';
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=4&user='+myData.login+'&pass='+myData.pass,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                if (response == ""){
                    $.mobile.loading( 'hide' );
                    $("#parkingPage [data-section='parkActivePage'] .notparkinginfo").removeClass("nd");
                    $("#parkingActiveList [data-role='collapsible']").remove();
                    //rampgap.alert(LANG.alertnotparking.text);
                }else {
                    //NT - Sincronizando Active Parking
                    rampgap.synchActiveParking();
                    for (var i = 0; i < response.length; i++) {
                        activeParkings[i]= new Array(response[i].idactiveParking,response[i].currentMins,
                                                     response[i].enableSMSNotif,response[i].maxMins,
                                                     response[i].minuteValue,response[i].platformIdplatform,
                                                     response[i].startTime,response[i].zoneIdzone,
                                                     response[i].placeIdplace, response[i].idHistory,
                                                     response[i].vehiclePlate,response[i].idSysUser);

                        resultP += '<div data-role="collapsible" data-plate="'+response[i].vehiclePlate+'">'+
                            '<h4>'+response[i].vehiclePlate+'</h4>'+
                            '<div id="mapLocalize1" class="mapLocalize"></div>'+
                            '<section class="parkingResume section-content">'+
                            '<p class="th date">'+LANG.starttimetxt.text+': <span class="td">'+response[i].startTime+'</span></p>'+
                            '<p class="th">'+LANG.placetxt.text+': <span class="td">'+response[i].placeIdplace+'</span></p>'+
                            '<p class="th">'+LANG.currenttimetxt.text+': <span class="td">'+response[i].currentMins+'</span></p>'+
                            '<p class="th">'+LANG.maxtimetxt.text+': <span class="td">'+
                            (response[i].maxMins == '-1' || response[i].maxMins == -1 ? 'NO' : response[i].maxMins+' mins' )+
                            '</span></p>'+
                            '<p class="th">'+LANG.minutepricetxt.text+': <span class="td">CRC '+response[i].minuteValue+'</span></p>'+
                            '<div class="text-center">'+
                            '<a data-role="button" href="#" class="btnSpecial btnUnParking ui-btn-inline ui-btn ui-shadow ui-corner-all" data-inline="true" data-index="'+i+'" >'+
                            LANG.unparkingtxt.text+
                            '</a>'+
                            '</div>'+
                            '</section>'+
                            '</div>';
                    }

                    $('#parkingActiveList').html(resultP);
                    $("#parkingActiveList").collapsibleset( "refresh" );
                    $("#parkingPage [data-section='parkActivePage'] .notparkinginfo").addClass("nd");

                    $.mobile.loading( 'hide' );
                }
            }
        }
    });
}

function activeParkingOptions(activeArrayIndex,carPlate){
    $.mobile.loading( 'show' );


    $.ajax({
        data: 'op=22&act='+activeParkings[activeArrayIndex][0]+'&user='+myData.login+'&pass='+myData.pass,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                if (response == ""){
                    $.mobile.loading( 'hide' );
                    return false;
                }else {
                    //cargar opciones
                    var infoP = '<table cellspacing="0" cellpadding="0">';
                    infoP += '<tr class="title"><td>'+LANG.vehicletxt.text+': '+activeParkings[activeArrayIndex][9]+'</td></tr>';
                    infoP += '<tr><td><strong>'+LANG.zonetxt.text+': </strong>'+activeParkings[activeArrayIndex][7]+'</td></tr>';
                    infoP += '<tr><td><strong>'+LANG.starttimetxt.text+': </strong>'+activeParkings[activeArrayIndex][6]+'</td></tr>';
                    infoP += '<tr><td><strong>'+LANG.currenttimetxt.text+': </strong>'+activeParkings[activeArrayIndex][1]+' mins</td></tr>';
                    infoP += '<tr><td><strong>'+LANG.minutepricetxt.text+': </strong>CRC '+activeParkings[activeArrayIndex][4]+'</td></tr>';
                    infoP += '<tr><td><strong>'+LANG.maxtimetxt.text+': </strong>'+(activeParkings[activeArrayIndex][3] == '-1' || activeParkings[activeArrayIndex][3] == -1 ? 'NO' : activeParkings[activeArrayIndex][3]+' mins' )+'</td></tr>';
                    if (activeParkings[activeArrayIndex][2] == 1){
                        infoP += '<tr><td><strong>'+LANG.smsnotitxt.text+': </strong>On</td></tr>';
                    }else{
                        infoP += '<tr><td><strong>'+LANG.smsnotitxt.text+': </strong>Off</td></tr>';
                    }
                    infoP += '</table>';
                    infoP += '<a data-inline="true" data-role="button" href="#" data-theme="c" class="ui-btn ui-btn-inline ui-btn-corner-all ui-shadow ui-btn-up-c viewMapCar"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">'+LANG.viewmapcar.text+'</span></span></a>';
                    infoP += '<a data-inline="true" data-role="button" href="#" data-theme="c" class="ui-btn ui-btn-inline ui-btn-corner-all ui-shadow ui-btn-up-c endParking"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">'+LANG.endparkingtxt.text+'</span></span></a>';
                    $('#activeInfo').html(infoP);
                    //$.mobile.changePage($("#viewActiveParkingInfo"));
                    $(":mobile-pagecontainer").pagecontainer( 'change', "#viewActiveParkingInfo", { transition: pageTransition });

                    $('.viewMapCar').click(function(){
                        viewZoneMapCar(response.latitude,response.longitude);
                    });

                    // $('.endParking').click(function(){
                    //     endParking(activeParkings[activeArrayIndex][9]);
                    // });


                    $.mobile.loading( 'hide' );

                }
            }
        }
    });

}

function endParking(buttonIndex, plate){
    /*
     idUser = request.getParameter("user");
     String plate = request.getParameter("plate"); 16:27
     op = 5

     SUCCES
     {"totalMins":12,"maxMins":3596,"minuteValue":"$0.025","startTime":"2011-08-18 05:17:00","endTime":"2011-08-18 05:28:42","zoneIdzone":"CAN01","vehiclePlate":"KJJ493","total":"$0.325","userBalance":"$89.325"}
     */
    if (buttonIndex === 2) {
        $.mobile.loading( 'hide' );
        return false;
    }

    plate = plate.replace(' ','');
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=5&user='+myData.login+'&pass='+myData.pass+'&plate='+plate,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                //NT - Removiendo active parking del listado
                rampgap.removeActiveParking(response.vehiclePlate);

                $("#parkingActiveList [data-plate='"+plate+"']").collapsible('collapse');
                $("#parkingActiveList [data-plate='"+plate+"']").addClass("animated fadeOutRightBig fl full-width");

                activeParking();

                $("#parkingActiveList [data-plate='"+plate+"']").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $(this).remove();
                });

                $.mobile.loading( 'hide' );


            }
        }
    });
}
// ------------------------------------------------------------------------------ /PARKING ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------ VEHICLES ------------------------------------------------------------------------------------------
/**
 * Request update vehicles in list,section and array myCars
 * @param page to change without #
 * @param acttion to run, example update only array myCars
 *
 */
function myVehicles(page, action){
    /*
     http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
     op = 4
     SUCCES
     [{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

     */
    var i,
        resultCars = "",
        selectCars = "";

    if(action != 'updateCount'){
        $.mobile.loading( 'show' );
    }
    $.ajax({
        data: 'op=15&user='+myData.login+'&pass='+myData.pass,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                if (response == ""){
                    $(".nocarsinfo").removeClass("nd");
                    if(action != 'updateCount'){
                        //rampgap.alert(LANG.alertnocars.text);
                        $.mobile.loading( 'hide' );
                        // List
                        $('#vehiclesList').html("");
                        $('#vehiclesList').listview("refresh");
                        // Select
                        $('#carPlate, #carPlatePop').html("");
                        $("#carPlate [value='0'], #carPlatePop [value='0']").prop('selected', true).change();

                        if(page){
                            $(":mobile-pagecontainer").pagecontainer( 'change', "#"+page, { transition: pageTransition });
                        }
                        myCars = [];
                    } else {
                        myCars = [];
                    }
                    return false;
                }else {
                    myCars = [];
                    if(action != 'updateCount'){
                        for (i = 0; i < response.length; i++) {
                            myCars[i]= [response[i].plate,response[i].dateReg,response[i].owner];

                            // List
                            resultCars += '<li data-plate="'+response[i].plate+'" data-owner="'+response[i].owner+'">'+
                                '<a href="#">'+
                                '<span class="topic">'+response[i].plate+'</span>'+
                                '</a>'+
                                '</li>';

                            // Select
                            if (i === 0){
                                selectCars += '<option value="0" selected="selected">'+LANG.vehiclestxt.text+'</option> ';
                                selectCars += '<option value="'+response[i].plate+'">'+response[i].plate+'</option> ';
                            }else{
                                selectCars += '<option value="'+response[i].plate+'">'+response[i].plate+'</option> ';
                            }
                        }

                        // List
                        $('#vehiclesList').html(resultCars);
                        $('#vehiclesList').listview("refresh");
                        // Select
                        $('#carPlate, #carPlatePop').html("");
                        $('#carPlate, #carPlatePop').html(selectCars);
                        $("#carPlate [value='0'], #carPlatePop [value='0']").prop('selected', true).change();

                        $(".nocarsinfo").addClass("nd");

                        $.mobile.loading( 'hide' );

                        if(page){
                            $(":mobile-pagecontainer").pagecontainer( 'change', "#"+page, { transition: pageTransition });
                        }
                    } else {
                        for (i = 0; i < response.length; i++) {
                            myCars[i]= [response[i].plate,response[i].dateReg,response[i].owner];

                        }
                    }


                }
            }
        }
    });
}

function loadVehicles(change){
    /*
     http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
     op = 4
     SUCCES
     [{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

     */
    var cars = '';
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=15&user='+myData.login+'&pass='+myData.pass,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                if (response == ""){
                    rampgap.alert (LANG.alertnocars.text);
                    if(change !== undefined){
                        $(":mobile-pagecontainer").pagecontainer( 'change', "#"+change, { transition: pageTransition });
                    }
                    $.mobile.loading( 'hide' );
                    return false;
                }else {
                    for (var i = 0; i < response.length; i++) {
                        if (i == 0){
                            cars += '<option value="0" selected="selected">'+LANG.vehiclestxt.text+'</option> ';
                            cars += '<option value="'+response[i].plate+'">'+response[i].plate+'</option> ';
                        }else{
                            cars += '<option value="'+response[i].plate+'">'+response[i].plate+'</option> ';
                        }
                    }

                    $('#carPlate, #carPlatePop').html('');
                    $('#carPlate, #carPlatePop').html(cars);
                    $("#parkingPage #carPlate [value='0']").prop('selected', true).change();

                    $.mobile.loading( 'hide' );

                    if(change !== undefined){
                        $(":mobile-pagecontainer").pagecontainer( 'change', "#"+change, { transition: pageTransition });
                    }

                }
            }
        }
    });
}


function deleteCar(plate, listitem){
    /*
     op = 17
     idu, plate

     SUCCES
     {"code":-126,"description":"OK"}
     */
    listitem.children('.ui-btn').addClass('ui-btn-active');

    navigator.notification.confirm(
        LANG.alertconfirmcardelete.text,
        function(buttonIndex) {
            _deleteCar(buttonIndex, plate, listitem);
        },
        LANG.alertconfirmcardeletetitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
    );

}

function _deleteCar(buttonIndex, plate, listitem){
    if (buttonIndex === 2) {
        listitem.children( ".ui-btn" ).removeClass( "ui-btn-active" );
        return false;
    }
    $.ajax({
        data: 'op=17&user='+myData.login+'&pass='+myData.pass+'&plate='+plate,
        success: function(response){
            if (response == null){
                rampgap.alert(LANG.alertsorry.text);
            }else{
                var transition = listitem.find('.deleteItem').attr("data-transition");
                if (transition) {

                    listitem.addClass("animated fadeOutLeft fl full-width")
                    // When the transition is done...
                        .one( "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
                            console.log("finish");
                            listitem.remove();
                            $( ".ui-allow-delete" ).find( ".border-bottom" ).removeClass( "border-bottom" );
                            $('#vehiclesList').listview( "refresh" );
                        })
                        .prev( "li" ).children( "a" ).addClass( "border-bottom" )
                        .end().end().children( ".ui-btn" ).removeClass( "ui-btn-active" );
                }else {
                    listitem.remove();
                    $('#vehiclesList').listview( "refresh" );
                }

                myVehicles();
            }
        }
    });
}

/**
 * Add new vehicle
 * @param plate number of vehicle plate
 * @param type  vehicle type, like comercial, moto etc
 * @param brand vehicle brand like chevrolet
 */
function addVehicle(plate, type, brand){
    /*
     op = 12
     idu, pass, plate
     IDU = LOGIN // OJO CORREGIR
     SUCCES
     {"code":-100,"description":"OK"}

     FAILED
     {"code":1,"description":"Failed"}

     */
    plate = plate.replace(' ','');
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=12&user='+myData.login+'&pass='+myData.pass+'&plate='+plate+'&type='+type+'&brand='+brand,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                if (response.code == -102){
                    rampgap.alert(LANG.alertvehicleexist.text);
                    $.mobile.loading( 'hide' );
                    return false;
                }else if(response.code == -130){
                    rampgap.alert(LANG.alertvehiclelimit.text);
                    $.mobile.loading( 'hide' );
                    return false;
                }else {
                    if (response.code != -100){
                        rampgap.alert(LANG.alertsorry.text);
                        $.mobile.loading( 'hide' );
                        return false;
                    }else{
                        rampgap.alert(LANG.alertvehicleadded.text);
                        $.mobile.loading( 'hide' );

                        if($('#popupAddVehicle').length == 1 && _currentPage == "parkingPage"){
                            $('#popupAddVehicle').popup('close');
                            $('#newPlatePop').val("");
                            $('#addDescriptionPop').val("");
                            myVehicles();
                        }
                        $('#newPlate').val("");
                        $('#addDescription').val("");
                        myVehicles();
                    }
                }
            }
        }
    });
}

// USER ACCOUNT -----------------------------------------------------------------


function viewAccountInfo(){
    /*
     OP=2 & USER

     SUCCES
     {"idsysUser":"3","address":"Kra 41G #47-12","balance":57.9,"birthDate":"Nov 19, 1984","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
     */
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=2&user='+myData.login+'&pass='+myData.pass,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                //Update Array myData
                myData.country = response.countryIdcountry;
                myData.city = response.city_idcity.idcity;
                myData.city_name = response.city_idcity.name;
                myData.msisdn = response.favoriteMsisdn;
                myData.idu = response.idsysUser;
                myData.address = response.address;
                myData.balance = response.balance;
                myData.cu = response.cu;
                myData.login = response.login;
                myData.email = response.email;
                myData.usertype = response.idsysUserType;
                myData.name = response.name;
                myData.lastname = response.lastName;

                //User Data
                var d = new Date(""+response.birthDate);
                var year = d.getFullYear();
                var month = d.getMonth()+1;
                var day = d.getDate();
                var formatted = year+"-"+month+"-"+day;

                $('#profileCU').val(response.cu);
                $('#profileLogin').val(response.login);
                $('#profileName').val(response.name);
                $('#profileLast').val(response.lastName);
                $('#profileAddress').val(response.address);
                $('#profileYear [value="'+year+'"]').prop('selected', true).change();
                $('#profileMonth [value="'+month+'"]').prop('selected', true).change();
                $('#profileDay [value="'+day+'"]').prop('selected', true).change();
                $('#profileMobile').val(response.favoriteMsisdn);
                $('#profileEmail').val(response.email);
                $('#profileCountry [value="' + response.city_idcity.idcountry + '"]').prop('selected', true).change();

                getCitiesRegister(response.city_idcity.idcountry,response.city_idcity.idcity);

                $.mobile.loading( 'hide' );
                $( ":mobile-pagecontainer" ).pagecontainer( 'change', "#profilePage", { transition: pageTransition });

            }
        }
    });
}

function getCountries() {
    /*
     SUCCFES
     [{"idcountry":"1","countryPrefix":"CAN","name":"Canada","latitude":"12365478","longitude":"12365445","lang":"en"}]
     */
    var today = new Date();

    countryGet = window.localStorage.getItem('countryGet');

    if(!countryGet || today > new Date(countryGet)){

        console.log('GET country');

        $.ajax({
            data: 'op=3',
            success: function(response) {
                console.log(response);
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {

                    var dateSave = new Date();
                    dateSave.setDate(dateSave.getDate() + 15);

                    window.localStorage.setItem('countryGet', dateSave);
                    window.localStorage.setItem('countryData', JSON.stringify(response));

                    countryData = JSON.parse(window.localStorage.getItem('countryData'));
                    pushCountryList(countryData);
                    console.log(countryData);
                }
            }
        });

    } else {
        countryData = JSON.parse(window.localStorage.getItem('countryData'));
        pushCountryList(countryData);
        console.log('Load Country from localStorage');
    }
}

/**
 *
 * @param countryData - append country data in select list
 */
function pushCountryList(countryData){
    var countries = "";
    for (var i = 0; i < countryData.length; i++) {

        if (i === 0) {
            countries += '<option value="0" selected="selected">---</option> ';
            countries += '<option value="' + countryData[i].idcountry + '">' + countryData[i].name + '</option> ';
        } else {
            countries += '<option value="' + countryData[i].idcountry + '">' + countryData[i].name + '</option> ';
        }

    }

    //Fix error append wp8.1
    countries = rampgap.toStatic(countries);

    $('#profileCountry,#registerCountry').html(countries);
    if(myData.country){
        $('#profileCountry [value="' + myData.country + '"]').prop('selected', true).change();
    }
    $.mobile.loading('hide');
}

function getCitiesRegister(countryCode,cityCode){
    if (countryCode==0) {
        return false;
    }

    $.mobile.loading( 'show' );
    var cities = '';
    $.ajax({
        data: 'op=8&idc='+countryCode,
        success: function(response){
            if (response == null){
                $.mobile.loading( 'hide' );
                return false;
            }else{
                for (var i = 0; i < response.length; i++) {
                    if (i == 0){
                        cities += '<option value="0" selected="selected">---</option> ';
                        cities += '<option value="'+response[i].idcity+'">'+response[i].name+'</option> ';
                    }else{
                        cities += '<option value="'+response[i].idcity+'">'+response[i].name+'</option> ';
                    }
                }
                $.mobile.loading( 'hide' );
                //Fix error append wp8.1
                cities = rampgap.toStatic(cities);

                $('#registerCity,#profileCity').html(cities);

                if (cityCode !== 0 && cityCode != undefined && cityCode != 'undefined') {
                    $('#profileCity [value="' + cityCode + '"]').prop('selected', true).change();
                }
            }
        }
    });
}

function saveUserUpdate(buttonIndex) {

    if (buttonIndex == 2) {
        return false;
    }
    /*
     OP=10
     name = request.getParameter("name");
     last_name = request.getParameter("last_name");
     address = request.getParameter("address");
     email = request.getParameter("email");
     birthdate = request.getParameter("bd");
     country = request.getParameter("country");
     msisdn = request.getParameter("msisdn");
     city = request.getParameter("city");

     SUCCES
     */
    $.mobile.loading( 'show' );

    var address = $("#profileAddress").val();
    var email = $("#profileEmail").val();
    var bd = $("#profileYear").val()+'-' +$("#profileMonth").val()+'-'+$("#profileDay").val();// (yyyy-MM-dd)
    var country = $("#profileCountry").val();
    myData.country=country;//ajustes v1.0.1
    var msisdn = $("#profileMobile").val();
    var city = $("#profileCity").val();
    var name = $("#profileName").val();
    var last_name = $("#profileLast").val();

    var checkInput = true;
    var selectorInput = '#userInfoForm input[type=text],#userInfoForm input[type=tel],#userInfoForm input[type=email], #userInfoForm input[type=password]';

    $(selectorInput).removeClass('formError');
    $('#profileForm select').parent().removeClass('formError');

    $(selectorInput).each(function() {
        if ($(this).val().length <= 2) {
            console.log($(this).attr('placeholder'));
            $('#' + $(this).attr('id')).addClass('formError');
            checkInput = false;
        }
    });

    if (checkInput === false) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertfields.text);
        return false;
    }

    if ($("#profileYear").val() === "" || $("#profileMonth").val() === "" || $("#profileDay").val() === ""){
        rampgap.alert(LANG.alertbirthday.text);
        return false;
    }

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //ajustes v1.0.1
    if (!filter.test(email)) {
        $.mobile.loading('hide');
        generalAlert(LANG.alerttypeemail.text);
        $("#profileEmail").addClass('formError');
        return false;
    }

    var filterPhone = /^([0-9])+$/;
    if (msisdn.length > 10 || msisdn.length < 8 || !filterPhone.test(msisdn)) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertinvalidphone.text);
        $("#profileMobile").addClass('formError');
        return false;
    }

    if (name.length > 30 || last_name.length > 30 || address.length > 45 || email.length > 45) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertlongfield.text);

        if (name.length > 30) {
            $("#profileName").addClass('formError');
        }
        if (last_name.length > 30) {
            $("#profileLast").addClass('formError');
        }
        if (address.length > 45) {
            $("#profileAddress").addClass('formError');
        }
        if (email.length > 45) {
            $("#profileEmail").addClass('formError');
        }
        return false;
    }

    if (country === 0 || country == "0" || country == "") {
        $.mobile.loading('hide');
        generalAlert(LANG.alertcountryfield.text);
        $('#profileCountry').parent().addClass('formError');
        return false;
    }
    if (city === 0 || city == "0" || city == "") {
        $.mobile.loading('hide');
        generalAlert(LANG.alertcityfield.text);
        $('#profileCity').parent().addClass('formError');
        return false;
    }

    var dataString = '&name=' + name + '&last_name=' + last_name + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city;


    $.ajax({
        data: 'op=10&user='+myData.login+'&pass='+myData.pass+dataString,
        success: function(response){
            console.log(response);
            if (response === null){
                $.mobile.loading( 'hide' );
                generalAlert(LANG.alertcheckdata.text);
                return false;
            }else{
                $.mobile.loading( 'hide' );
                generalAlert(LANG.alertregisterok.text);
                $('#btnCancelEditProfile').trigger('tap');
                viewAccountInfo();
            }
        }
    });
}

function savePassword(pass,oldPass,newPass){
    $.mobile.loading( 'show' );
    $.ajax({
        data: 'op=11&user='+myData.login+'&pass='+encrypt(pass)+'&oldpass='+encrypt(oldPass)+'&npass='+encrypt(newPass),
        success: function(response){
            if (response == null){
                rampgap.alert(LANG.alertcheckpassword.text);
                $.mobile.loading( 'hide' );
                return false;
            }else{
                if (response.code == -117){
                    myData.pass = encrypt(newPass);

                    rampgap.setItem("pass", encrypt(newPass));
                    window.localStorage.setItem("pass", encrypt(newPass));

                    rampgap.alert (LANG.alertpasschangeok.text);
                    $.mobile.loading( 'hide' );
                    $( ":mobile-pagecontainer" ).pagecontainer( 'change', "#configPage", { transition: pageTransition });

                }
            }
        }
    });
}

function payPal(){
    $.mobile.loading( 'show' );
    var url = "https://cpsparking.ca/cpspaypalmobile/faces/balance/reloadPayPalMobile.xhtml?userId="+encrypt(myData['idu'])+"&device=Android";
    rampgap.openURL(url, "balance");
    $.mobile.loading( 'hide' );
}

function reloadMSJ(){
    $.mobile.loading( 'show' );
    var url = "https://www.msj.go.cr/msj_mobile/servicios_mobile.aspx";
    rampgap.openURL(url, "balance");
    $.mobile.loading( 'hide' );
}

function processNotificationMessage(m){
    if( m == '5' ){
        rampgap.alert(LANG.alertfiveleft.text);
        rampgap.notifySoundAndVibrate();
        activeParking();
    }
    else if( m == '1' ){
        rampgap.alert (LANG.alertoneleft.text);
        rampgap.notifySoundAndVibrate();
    }
    else if( m == '0' ){
        rampgap.alert(LANG.alertvehiclefined.text);
        rampgap.notifySoundAndVibrate();
    }
    else{
        rampgap.alert( m );
        rampgap.notifySoundAndVibrate();
    }
}

//---------------------------------------------------------------------------//
