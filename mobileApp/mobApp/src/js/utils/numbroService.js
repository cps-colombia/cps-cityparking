/* global require */

// utils/nombroFormat.js
//
// custom language and config for numbro module
//
// 2015, CPS - Cellular Parking Systems
(function () {
    "use strict";

    var angular = require("angular"),
        numbro = require("numbro"),
        ld = require("lodash");

    numbroService.$inject = ["$rootScope", "$App", "$Locale"];
    function numbroService ($rootScope, $App, $Locale) {
        var config = $App.config,
            currencyFormat = config.options && config.options.currencyFormat || "0,0";

        // Listen events
        $rootScope.$on("cps:localeChange", function (event, lang) {
            setLanguage(lang, config.defaultLocale);
        });

        // numbro.js language configuration
        var languageConfig = [
            /**
             * language : Spanish
             * locale: Colombia
             */
            {
                langLocaleCode: "es-CO",
                delimiters: {
                    thousands: ".",
                    decimal: ","
                },
                abbreviations: {
                    thousand: "m",
                    million: "mm",
                    billion: "b",
                    trillion: "t"
                },
                ordinal: function (number) {
                    var b = number % 10;
                    return (b === 1 || b === 3) ? "er" :
                        (b === 2) ? "do" :
                        (b === 7 || b === 0) ? "mo" :
                        (b === 8) ? "vo" :
                        (b === 9) ? "no" : "to";
                },
                currency: {
                    symbol: "$",
                    position: "prefix"
                },
                defaults: {
                    currencyFormat: ",0000 a"
                },
                formats: {
                    fourDigits: "0000 a",
                    fullWithTwoDecimals: ",0.00 $",
                    fullWithTwoDecimalsNoCurrency: ",0.00",
                    fullWithNoDecimals: ",0 $"
                }
            },
            /**
             * language : Spanish
             * locale: Costa Rica
             */
            {
                langLocaleCode: "es-CR",
                delimiters: {
                    thousands: ".",
                    decimal: ","
                },
                abbreviations: {
                    thousand: "m",
                    million: "mm",
                    billion: "b",
                    trillion: "t"
                },
                ordinal: function (number) {
                    var b = number % 10;
                    return (b === 1 || b === 3) ? "er" :
                        (b === 2) ? "do" :
                        (b === 7 || b === 0) ? "mo" :
                        (b === 8) ? "vo" :
                        (b === 9) ? "no" : "to";
                },
                currency: {
                    symbol: "₡",
                    position: "prefix"
                },
                defaults: {
                    currencyFormat: ",0000 a"
                },
                formats: {
                    fourDigits: "0000 a",
                    fullWithTwoDecimals: ",0.00 $",
                    fullWithTwoDecimalsNoCurrency: ",0.00",
                    fullWithNoDecimals: ",0 $"
                }
            }
        ];

        // Members
        var Numbro = numbro; // set function from numbro
        // Own fucntions
        Numbro.init = init;
        Numbro.hasLanguage = hasLanguage;
        Numbro.formatCurrency = formatCurrency;

        // Init and register languages
        init();

        return Numbro;

        function init () {
            // Register aditional languajes
            angular.forEach(languageConfig, function (language, key) {
                numbro.language(language.langLocaleCode, language);
            });
            setLanguage(numbro.languages(), config.defaultLocale);
            numbro.defaultCurrencyFormat(currencyFormat);
        }

        /**
         * @name formatCurrency
         *
         * @description custom way to get currency format with default format
         * when app render before set defaultFormat no re-convert
         * for this use this custom fucction
         *
         * @return {string} currency format
         */
        function formatCurrency (value, format) {
            return numbro(value).formatCurrency(format || currencyFormat);
        }

        /**
         * @name setLanguage
         *
         * @description fix set language when de browser language is unsupported
         * This function allow the user to set a new language with a fallback if
         * the language does not exist. If no fallback language is provided,
         * it fallbacks to english.
         */
        function setLanguage (newLanguage, fallbackLanguage) {
            if(hasLanguage(config.defaultCurrency)) {
                numbro.setLanguage(config.defaultCurrency);
            } else {
                try {
                    numbro.setLanguage(newLanguage, fallbackLanguage);
                } catch(e) {
                    numbro.setLanguage(fallbackLanguage, "en-US");
                }
            }
            $rootScope.$broadcast("cps:formatChange", numbro.language());
        }

        function hasLanguage (lang) {
            return (ld.keys(numbro.languages()).indexOf(config.defaultCurrency) > -1);
        }
    }

    // Exports
    module.exports = numbroService;

})();
