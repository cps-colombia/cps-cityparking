/* global module */

// utils/hardwareService.js
//
// hardware provider module for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    function hardwareService () {
        var $backButtonActions = {};
        var _hasBackButtonHandler = false;

        // $get injector
        $get.$inject = [
            "$q",
            "$document",
            "$App",
            "BACK_PRIORITY"
        ];

        // setter and getter
        var HardwareServices = {
            $get: $get
        };
        return HardwareServices;

        // Getter
        function $get ($q, $document, $App, BACK_PRIORITY) {
            // Members
            var Hardware = {
                onHardwareBackButton: onHardwareBackButton,
                offHardwareBackButton: offHardwareBackButton,
                registerBackButtonAction: registerBackButtonAction,
                BACK_PRIORITY: BACK_PRIORITY
            };
            return Hardware;

            // Functions
            /**
             * @name onHardwareBackButton
             * @description
             * Some platforms have a hardware back button, so this is one way to
             * bind to it.
             * @param {function} cb callback the callback to trigger when this event occurs
             *
             * @return {function} unregister backbutton function
             */
            function onHardwareBackButton (cb) {
                $App.ready(function () {
                    $document[0].addEventListener("backbutton", cb, false);
                });
                return function unregister () {
                    offHardwareBackButton(cb);
                };
            }

            /**
             * @name offHardwareBackButton
             * @description
             * Remove an event listener for the backbutton.
             * @param {function} fn callback The listener function that was
             * originally bound.
             */
            function offHardwareBackButton (fn) {
                $App.ready(function () {
                    $document[0].removeEventListener("backbutton", fn);
                });
            }

            /**
             * @name registerBackButtonAction
             * @description
             * Register a hardware back button action. Only one action will execute
             * when the back button is clicked, so this method decides which of
             * the registered back button actions has the highest priority.
             *
             * For example, if an actionsheet is showing, the back button should
             * close the actionsheet, but it should not also go back a page view
             * or close a modal which may be open.
             *
             * The priorities for the existing back button hooks are as follows:
             *   Return to previous view = 100
             *   Close side menu = 150
             *   Dismiss modal = 200
             *   Close action sheet = 300
             *   Dismiss popup = 400
             *   Dismiss loading overlay = 500
             *
             * Your back button action will override each of the above actions
             * whose priority is less than the priority you provide. For example,
             * an action assigned a priority of 101 will override the 'return to
             * previous view' action, but not any of the other actions.
             *
             * @param {function} fn callback Called when the back button is pressed,
             * if this listener is the highest priority.
             * @param {number} priority Only the highest priority will execute.
             * @param {*=} actionId The id to assign this action. Default: a
             * random unique id.
             * @returns {function} A function that, when called, will deregister
             * this backButtonAction.
             */
            function registerBackButtonAction (fn, priority, actionId) {
                if (!_hasBackButtonHandler) {
                    // add a back button listener if one hasn't been setup yet
                    $backButtonActions = {};
                    onHardwareBackButton(hardwareBackButtonClick);
                    _hasBackButtonHandler = true;
                }

                var action = {
                    id: (actionId ? actionId : ld.nextUid()),
                    priority: (priority ? priority : 0),
                    fn: fn
                };
                $backButtonActions[action.id] = action;

                // return a function to de-register this back button action
                return function () {
                    delete $backButtonActions[action.id];
                };
            }

            /**
             * @private
             */

            /**
             * @name hardwareBackButtonClick
             * @description loop through all the registered back button actions
             * and only run the last one of the highest priority
             * @param {object} event backbutton
             * @returns {object} register backbutton action
             */
            function hardwareBackButtonClick (event) {
                var priorityAction, actionId;

                for (actionId in $backButtonActions) {
                    if (!priorityAction || $backButtonActions[actionId].priority >= priorityAction.priority) {
                        priorityAction = $backButtonActions[actionId];
                    }
                }

                if (priorityAction) {
                    priorityAction.fn(event);
                    return priorityAction;
                }
            }

        }
    }

    // Exports
    module.exports = hardwareService;

})();
