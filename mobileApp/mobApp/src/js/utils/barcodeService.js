/* global module */

// utils/barcodeService.js
//
// scan/encode barcodes for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    const ld = require("lodash");
    const QRious = require("qrious");

    barcodeService.$inject = [
        "$q",
        "$window"
    ];
    function barcodeService ($q, $window) {
        var barcode = ld.get($window.cordova, "plugins.barcodeScanner");

        // Memebrs
        var Barcode = {
            scan: scan,
            encode: encode,
            types: ld.extend({
                QR: "QR"
            }, barcode && barcode.Encode)
        };

        return Barcode;

        // Functions
        /**
         * @name scan
         * @param {object} config
         *
         * @return {promise}
         */
        function scan (config) {
            var deferred = $q.defer();
            if(!barcode) {
                return deferred.reject(new Error(
                    "Need 'cordova.plugins.barcodeScanner' plugin installed"
                ));
            }

            barcode.scan(function (result) {
                deferred.resolve(result);
            }, function (err) {
                deferred.reject(err);
            }, config);

            return deferred.promise;
        }

        /**
         * @name encode
         * @param {string} type
         * @param {string} data
         *
         * @return {promise}
         */
        function encode (data, opts) {
            var deferred = $q.defer();

            opts = ld.extend({
                type: "QR"
            }, opts);
            if(!barcode) {
                opts.type = "QR";
            }

            if (opts.type == Barcode.types.QR) {
                opts.value = data;
                var result = new QRious(opts);
                deferred.resolve(result);

            } else {
                barcode.encode(opts.type, data, function (result) {
                    deferred.resolve(result);
                }, function (err) {
                    deferred.reject(err);
                });
            }

            return deferred.promise;
        }

    }

    // Export
    module.exports = barcodeService;
})();
