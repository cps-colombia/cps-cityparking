/* global require */
/* exported cpsDecorators, mixinLodash*/

// utils/index.js
//
// index function for utils
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        cpsDecorators = require("./decorators"), // eslint-disable-line
        loggerService = require("./loggerService"),
        mobileService = require("./mobileService"),
        appService = require("./appService"),
        hardwareService = require("./hardwareService"),
        inBrowserService = require("./inBrowserService"),
        requestService = require("./requestService"),
        storageService = require("./storageService"),
        soapService = require("./soapService"),
        encryptService = require("./encryptService"),
        oauthService = require("./oauthService"),
        responseService = require("./responseService"),
        delegateService = require("./delegateService"),
        geoService = require("./geoService"),
        bodyService = require("./bodyService"),
        domService = require("./domService"),
        utilsService = require("./utilsService"),
        bindService = require("./bindService"),
        networkService = require("./networkService"),
        templateService = require("./templateService"),
        positionService = require("./positionService"),
        statusbarService = require("./statusbarService"),
        keyboardService = require("./keyboardService"),
        backgroundService = require("./backgroundService"),
        barcodeService = require("./barcodeService"),
        elementService = require("./elementService"),
        numbroService = require("./numbroService"),
        momentService = require("./momentService"),
        utilsDirective = require("./utilsDirective"),
        utilsFilter = require("./utilsFilter"),
        utilsRun = require("./utilsRun"),
        mixinLodash = require("./mixin"); // eslint-disable-line

    angular.module("cpsUtils", []);
    var cpsUtil = angular.module("cpsUtils");

    // Service
    cpsUtil.provider("$Logger", loggerService);
    cpsUtil.provider("$App", appService);
    cpsUtil.provider("$Hardware", hardwareService);
    cpsUtil.provider("$InBrowser", inBrowserService);
    cpsUtil.service("$Encrypt", encryptService);
    cpsUtil.service("$Oauth", oauthService);
    cpsUtil.service("$Response", responseService);
    cpsUtil.service("$Delegate", delegateService);
    cpsUtil.factory("$Request", requestService);
    cpsUtil.factory("$Storage", storageService);
    cpsUtil.factory("$Soap", soapService);
    cpsUtil.factory("$Mobile", mobileService);
    cpsUtil.factory("$Geo", geoService);
    cpsUtil.factory("$Body", bodyService);
    cpsUtil.factory("$Dom", domService);
    cpsUtil.factory("$Utils", utilsService);
    cpsUtil.factory("$Bind", bindService);
    cpsUtil.factory("$Network", networkService);
    cpsUtil.factory("$Template", templateService);
    cpsUtil.factory("$Position", positionService);
    cpsUtil.factory("$Statusbar", statusbarService);
    cpsUtil.factory("$Keyboard", keyboardService);
    cpsUtil.factory("$Background", backgroundService);
    cpsUtil.factory("$Barcode", barcodeService);
    cpsUtil.factory("$Element", elementService);
    cpsUtil.factory("$Numbro", numbroService);
    cpsUtil.factory("$Moment", momentService);

    // Directive
    cpsUtil.directive("cpsValidated", utilsDirective.cpsValidated);
    cpsUtil.directive("cpsEvent", utilsDirective.cpsEvent);
    cpsUtil.directive("cpsFormat", utilsDirective.cpsFormat);
    cpsUtil.directive("cpsCleanNumber", utilsDirective.cpsCleanNumber);
    cpsUtil.directive("cpsFormSuccess", utilsDirective.cpsFormSuccess);
    cpsUtil.directive("cpsMatch", utilsDirective.cpsMatch);
    cpsUtil.directive("cpsDates", utilsDirective.cpsDates);
    cpsUtil.directive("cpsIsHold", utilsDirective.cpsIsHold);
    cpsUtil.directive("cpsHold", utilsDirective.cpsHold);
    cpsUtil.directive("cpsFullHeight", utilsDirective.cpsFullHeight);
    cpsUtil.directive("cpsSection", utilsDirective.cpsSection);
    cpsUtil.directive("cpsExitApp", utilsDirective.cpsExitApp);
    cpsUtil.directive("cpsModule", utilsDirective.cpsModule);
    cpsUtil.directive("cpsOpen", utilsDirective.cpsOpenLink);
    cpsUtil.directive("cpsTimeAgo", utilsDirective.cpsTimeAgo);
    cpsUtil.directive("cpsSelect", utilsDirective.cpsSelect);
    cpsUtil.directive("select", utilsDirective.cpsSelect);
    cpsUtil.directive("cpsKeyboardAttach", utilsDirective.cpsKeyboardAttach);
    cpsUtil.directive("cpsKeyboardHandler", utilsDirective.cpsKeyboardHandler);

    // Filter
    cpsUtil.filter("cpsLodash", utilsFilter.cpsLodash);
    cpsUtil.filter("cpsCamelCase", utilsFilter.cpsCamelCase);
    cpsUtil.filter("cpsCapitalize", utilsFilter.cpsCapitalize);
    cpsUtil.filter("cpsStartCase", utilsFilter.cpsStartCase);
    cpsUtil.filter("cpsTitleCase", utilsFilter.cpsTitleCase);
    cpsUtil.filter("cpsWhere", utilsFilter.cpsWhere);
    cpsUtil.filter("cpsInteger", utilsFilter.cpsInteger);
    cpsUtil.filter("cpsDates", utilsFilter.cpsDates);
    cpsUtil.filter("cpsIsRoute", utilsFilter.cpsIsRoute);
    cpsUtil.filter("cpsFormat", utilsFilter.cpsFormat);
    cpsUtil.filter("cpsDateFormat", utilsFilter.cpsDateFormat);
    cpsUtil.filter("cpsDurationFormat", utilsFilter.cpsDurationFormat);
    cpsUtil.filter("cpsDurationHumanize", utilsFilter.cpsDurationHumanize);
    cpsUtil.filter("cpsFlatObject", utilsFilter.cpsFlatObject);

    // Run
    cpsUtil.run(utilsRun);
})();
