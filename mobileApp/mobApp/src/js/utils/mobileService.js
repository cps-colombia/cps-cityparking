/* global module */

// app/utils/mobileService.js
//
// mobile factory module for cps mobile app
// diferents utils for cordova
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    mobileService.$inject = [
        "$q",
        "$rootScope",
        "$timeout",
        "$window",
        "$document",
        "$location",
        "$Locale",
        "$Logger",
        "$App",
        "$Loading",
        "$UiConfig",
        "CPS_CONFIG"
    ];
    function mobileService ($q, $rootScope, $timeout, $window, $document,
                           $location, $Locale, $Logger, $App, $Loading, $UiConfig, CPS_CONFIG) {
        var _ = locale,
            cvDialogs = $window.navigator.notification,
            logger = $Logger.getInstance(),
            lastAlert = null;

        // Members
        var Mobile = {
            alert: alertCps,
            confirm: confirmCps,
            prompt: promptCps,
            open: cpsOpen,
            canOpen: canOpen,
            isCordova: isCordova,
            getPlatform: getPlatform,
            loading: loading,
            spinner: spinner,
            splashscreen: splashscreen,
            locale: locale,
            getLanguage: getLanguage,
            share: share,
            exitApp: exitApp,
            pageChange: pageChange,
            ga: sendAnalytics,
            cleanInputs: cleanInputs,
            emailValidator: emailValidator,
            checkVersion: checkVersion,
            openStore: openStore
        };

        return Mobile;

        // Functions

        /**
         * Alert notification for cordova and web
         * @param msg menssage to show in cvDialogs
         * @param callback optional response callback
         */
        function alertCps (msg, callback, options) {
            var deferred = $q.defer(),
                defOptions = {
                    title: _("alertTitle"),
                    btnOk: _("btnOk")
                };
            // Extend
            callback = callback || angular.noop;
            if(options) {
                angular.extend(defOptions, options);
            }
            // Check
            if(msg == lastAlert) {
                logger.warn("mobile", "prevent same alert");
                deferred.reject();
                return deferred.promise;
            }
            lastAlert = msg;

            if($App.isCordova()) {
                $App.ready(function () {
                    cvDialogs.alert(
                        msg,
                        function () {
                            cleanTimer();
                            callback();
                            deferred.resolve();
                        },
                        defOptions.title,
                        defOptions.btnOk);
                });
            } else {
                $window.alert(msg);
                cleanTimer();
                callback();
                deferred.resolve();
            }

            function cleanTimer () {
                $timeout(function () {
                    lastAlert = null;
                }, 300);
            }
            // Promise
            return deferred.promise;
        }

        /**
         * Confirm cvDialogs for cordova and web
         * @param msg menssage to show in cvDialogs
         * @param callback optional response callback
         */
        function confirmCps (msg, callback, options) {
            var deferred = $q.defer(),
                defOptions = {
                    title: _("alertConfirmTitle"),
                    btnOk: _("btnOk"),
                    btnCancel: _("btnCancel")
                };

            // Extend
            callback = callback || angular.noop;
            if(options) {
                angular.extend(defOptions, options);
            }

            // Check
            if($App.isCordova()) {
                $App.ready(function () {
                    cvDialogs.confirm(
                        msg,
                        function (buttonIndex) {
                            buttonIndex = parseInt(buttonIndex);
                            // Traditional callback
                            callback(buttonIndex);
                            // Promise callback
                            deferred.resolve(buttonIndex);
                        },
                        defOptions.title,
                        [
                            defOptions.btnOk,
                            defOptions.btnCancel
                        ]);
                });
            } else {
                if($window.confirm(msg)) {
                    callback(1);
                    deferred.resolve(1);
                } else {
                    callback(2);
                    deferred.resolve(2);
                }
            }

            // Promise
            return deferred.promise;
        }

        function promptCps (msg, callback, options) {
            var deferred = $q.defer(),
                defOptions = {
                    title: _("alertPromptTitle"),
                    btnOk: _("btnOk"),
                    btnCancel: _("btnCancel"),
                    labelText: ""
                };
            // Extend
            callback = callback || angular.noop;
            if(options) {
                angular.extend(defOptions, options);
            }
            // Check
            if($App.isCordova()) {
                $App.ready(function () {
                    cvDialogs.prompt(
                        msg,
                        function (result) {
                            callback(result);
                            deferred.resolve(result);
                        },
                        defOptions.title,
                        [
                            defOptions.btnOk,
                            defOptions.btnCancel
                        ],
                        defOptions.labelText
                    );
                });
            } else {
                var res = $window.prompt(msg, defOptions.labelText),
                    _prompt = {input1: res, buttonIndex: 1};
                if(res) {
                    callback(_prompt);
                    deferred.resolve(_prompt);
                } else {
                    _prompt.buttonIndex = 2;
                    callback(_prompt);
                    deferred.resolve(_prompt);
                }
            }

            // Promise
            return deferred.promise;
        }

        function cpsOpen (myUrl, optionsString) {
            if($App.isCordova()) {
                $window.open(myUrl, "_system", optionsString);
            } else {
                $window.open(myUrl, "_blank", optionsString);
            }
        }

        function canOpen (toCheck, callback) {
            callback = callback || angular.noop;

            if($window.CanOpen) {
                $window.CanOpen(toCheck, function (isInstalled) {
                    callback(isInstalled);
                });
            } else {
                callback(false);
            }
        }

        // Return if run cordava
        function isCordova () {
            return $App.isCordova();
        }

        // Get platform from $App service
        function getPlatform () {
            return $App.getPlatform();
        }

        /**
         * @name loading
         *
         * @description Abstract layer for $Loading service
         * @param {string} event hide or show
         * @param {object} options
         */
        function loading (event, options) {
            options = ld.extend({
                noBackdrop: $UiConfig.loading.noBackdrop()
            }, options);

            if(!options.background) {
                $Loading[event](options);
            }
        }

        /**
         * @name spinner
         *
         * @description Abstract layer for $Spinner service
         * @param {object} options
         */
        function spinner (options) {
            return $Loading.spinner(options);
        }

        function splashscreen (event) {
            var screen = $window.navigator && $window.navigator.splashscreen || {},
                splash = {
                    hide: screen.hide,
                    show: screen.show
                }[event];

            if(ld.isFunction(splash)) {
                splash();
            }
        }

        /**
         * $Locale mediatory get Internationalization by key/id
         * @param key name or ide to text will be tranlate
         * @param args aditional arguments like {name}
         * @param fallbackString default string
         *
         * @return {string} text tranlate
         */
        function locale (key, args, fallbackString) {
            return $Locale.get(key, args, fallbackString);
        }

        /**
         * $Locale mediatory get current language
         *
         * @return {string} current language in ISO code
         */
        function getLanguage () {
            return $Locale.getLanguage();
        }

        function share (msg, title, img, link) {
            msg = msg || "shareMessage";
            title = title || "shareTitle";
            link = link || "shareLink";
            if($window.plugins && isCordova()) {
                $window.plugins.socialsharing.share(
                    _(msg),
                    _(title),
                    img,           // Image
                    _(link),
                    function (res) {
                        logger.debug("share", "share ok");
                        // Send event share success to Analytics
                        sendAnalytics("send", "event", "share", "success");
                    },
                    function (err) {
                        logger.debug("share", "error:" + err);
                        // Send event share error to Analytics
                        sendAnalytics("send", "event", "share", "error");
                    }
                );
            } else {
                alertCps(title + " : " + msg + " - " + link);
            }
        }

        function exitApp () {
            confirmCps(
                _("alertExitApp"),
                function (buttonIndex) {
                    if (buttonIndex === 2) {
                        return false;
                    }
                    if($window.navigator && $window.navigator.app) {
                        $window.navigator.app.exitApp();
                    }
                }
            );
        }

        /**
         * Navigate and change page
         * @param page path like /login or #login
         */
        function pageChange (page) {
            if(page) {
                // don't let it start with a #, messes with $location.url()
                if (page.indexOf("#") === 0) {
                    page = page.replace("#", "/");
                }
                if (page !== $location.url()) {
                    // we've got a good URL, ready GO!
                    $location.url(page);
                }
            }
        }

        // Send Analytics
        function sendAnalytics (action, type, content, label) {
            if($App.checkAnalytics()) {
                $App.sendAnalytics(action, type, content, label);
            }
        }

        // Clear forms DEPRECATED
        function cleanInputs (scope, formName, objectName) {
            // TODO: better way from directive
            // scope[formName].$setPristine();

            angular.forEach(objectName, function (name, key) {
                angular.forEach(scope[name], function (values, vkey) {
                    scope[name][vkey] = "";
                });
            });

            $$("[name=" + formName + "] .item-input").removeClass("ng-input-valid ng-input-invalid");
        }

        // Validator
        function emailValidator (email) {
            var validator = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return validator.test(email);
        }

        /**
         * checkVersion
         *
         * Check if exist new app version for launch update
         * @param {string} version current version string
         *
         * @return {boolean} true or false if no exist new version
         */
        function checkVersion (version) {
            version = ld.isObject(version) ? version.currVersion : version;

            if(CPS_CONFIG.versionPkg < version) {
                $timeout(function () {
                    alertCps(_("alertNewUpdate"), function () {
                        openStore();
                        $rootScope.$emit("cps:baseData", {
                            action: "clean"
                        });
                    });
                }, 1000);
            }
        }

        function openStore () {
            var platformId = $App.getPlatform().platformId,
                platform = CPS_CONFIG.platforms[getPlatform().platform] || {},
                storeLink = {
                    "1": getAndroidStore(),
                    "2": encodeURI(platform.store),
                    "3": encodeURI(platform.store)
                }[platformId];

            if(storeLink) {
                cpsOpen(storeLink);
            }

            function getAndroidStore () {
                var _url;
                canOpen("market://", function (isInstalled) {
                    if (isInstalled) {
                        _url = encodeURI("market://details?id=" + platform.packageId);
                    } else {
                        _url = encodeURI("https://play.google.com/store/apps/details?id=" + platform.packageId);
                    }
                });
                return _url;
            }
        }
    }

    // Exports
    module.exports = mobileService;

})();
