/* global require */

// utils/mixin.js
//
// register mixin for lodash/underscore
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    const ld = require("lodash");

    var nextId = 0;

    // Set mixin in lodash
    ld.mixin({ nextUid: nextUid });
    ld.mixin({ slenderizeObject: slenderizeObject });
    ld.mixin({ toCapitalize: toCapitalize });
    ld.mixin({ toStartCase: toStartCase });
    ld.mixin({ toTitleCase: toTitleCase });
    ld.mixin({ cleanObject: cleanObject });
    ld.mixin({ toPlainObjectDeep: toPlainObjectDeep });
    ld.mixin({ hasIn: hasIn });
    ld.mixin({ isJSON: isJSON });
    ld.mixin({ isNotdefined: isNotdefined });
    ld.mixin({ move: move });
    ld.mixin({ setDebounce: setDebounce });

    // Functions
    function nextUid () {
        return "cps" + (nextId++);
    }

    function slenderizeObject (fatObject, options) {
        var _propertyIdentifiers = [];
        var _slenderObject = {};

        processNode(fatObject, _propertyIdentifiers, _slenderObject, options);

        return _slenderObject;
    }

    function toCapitalize (input) {
        if(ld.isString(input)) {
            return ld.capitalize(input.toLowerCase());
        } else {
            return ld.capitalize(input);
        }
    }

    function toStartCase (input) {
        return ld.startCase(ld.camelCase(input));
    }

    function toTitleCase (input) {
        return input.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    function cleanObject (obj) {
        return ld.omit(obj, function (v) {
            return ld.isUndefined(v) || ld.isNull(v);
        });
    }

    function toPlainObjectDeep (obj) {
        return ld.reduce(ld.toPlainObject(obj), function (result, value, key) {
            if(ld.isObject(value)) {
                value = ld.toPlainObject(value);
            }
            result[key] = value;
            return result;
        }, {});
    }

    function hasIn (obj, required) {
        return ld.every(required, ld.partial(ld.has, obj));
    }

    function isJSON (str) {
        // checks if a string is a valid JSON
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    function isNotdefined (value) {
        return value === null || value === undefined || value === void 0;
    }

    function move (array, fromIndex, toIndex) {
        if((array.length - 1) >= toIndex && fromIndex !== toIndex) {
            array.splice(toIndex, 0, array.splice(fromIndex, 1)[0]);
        }
        return array;
    }

    /**
     * Only call a function once in the given interval.
     *
     * @param func {Function} the function to call
     * @param wait {int} how long to wait before/after to allow function calls
     * @param immediate {boolean} whether to call immediately or after the wait interval
     */
    function setDebounce (func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        return function () {
            context = this;
            args = arguments;
            timestamp = new Date();
            var later = function () {
                var last = (new Date()) - timestamp;
                if (last < wait) {
                    timeout = setTimeout(later, wait - last);
                } else {
                    timeout = null;
                    if (!immediate) {
                        result = func.apply(context, args);
                    }
                }
            };
            var callNow = immediate && !timeout;
            if (!timeout) {
                timeout = setTimeout(later, wait);
            }
            if (callNow) {
                result = func.apply(context, args);
            }
            return result;
        };
    }

    // Private
    function processNode (theNode, _propertyIdentifiers, _slenderObject, _options) {
        var ret = {};
        var options = ld.extend({
            separator: "_"
        }, _options);
        var keyString;
        var myKeys;
        var result;

        theNode = theNode || {};
        _propertyIdentifiers = _propertyIdentifiers || [];

        result = ld(theNode).map(function (value, key) {
            myKeys = ld.clone(_propertyIdentifiers);
            ret = {};

            myKeys.push(key);

            // if value is a string, number or boolean
            if (ld.isString(value) || ld.isNumber(value) || ld.isBoolean(value)) {
                // build a keyString to use as a property identifier
                keyString = myKeys.join(options.separator);
                // add a property with that identifier and childNode as the value to our return object
                ret[keyString] = _slenderObject[keyString] = value;
            } else {
                // Call processNode recursively if value isn't a leaf node type (string, number or boolean)
                processNode(value, myKeys, _slenderObject, _options);
                ret = value;
            }

            return ret;
        }).value();

        return result;
    }

    // Export
    module.exports = {
        nextUid: nextUid,
        slenderizeObject: slenderizeObject,
        toCapitalize: toCapitalize,
        toStartCase: toStartCase,
        toTitleCase: toTitleCase,
        cleanObject: cleanObject,
        toPlainObjectDeep: toPlainObjectDeep,
        hasIn: hasIn,
        isJSON: isJSON,
        isNotdefined: isNotdefined,
        move: move,
        setDebounce: setDebounce
    };

})();
