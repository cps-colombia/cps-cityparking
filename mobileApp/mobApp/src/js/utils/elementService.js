/* global module */

// utils/elementService.js
//
// element utils factory for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";


    elementService.$inject = ["$App"];
    function elementService ($App) {
        // Membres
        var Element = {
            isTextInput: isTextInput,
            isDateInput: isDateInput,
            isKeyboardElement: isKeyboardElement
        };
        return Element;
        // Functions
        function isTextInput (ele) {
            return !!ele &&
                (ele.tagName === "TEXTAREA" ||
                 ele.contentEditable === "true" ||
                 (ele.tagName === "INPUT" &&
                  !(/^(radio|checkbox|range|file|submit|reset|color|image|button)$/i).test(ele.type)));
        }

        function isDateInput (ele) {
            return !!ele &&
                (ele.tagName === "INPUT" && (/^(date|time|datetime-local|month|week)$/i).test(ele.type));
        }

        function isKeyboardElement (ele) {
            if ($App.isIOS() || $App.isIPad()) {
                return isTextInput(ele) && !isDateInput(ele);
            } else {
                return isTextInput(ele) || (!!ele && ele.tagName === "SELECT");
            }
        }
    }
    // Exports
    module.exports = elementService;
})();
