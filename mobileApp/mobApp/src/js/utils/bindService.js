/* global require */

// utils/bindService.js
//
// Bind adapter from angular.js, allow multiple directives
// with bind scopes
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular");

    bindService.$inject = ["$parse", "$interpolate"];
    function bindService ($parse, $interpolate) {

        var LOCAL_REGEXP = /^\s*([@=&])(\??)\s*(\w*)\s*$/;
        return function (scope, attrs, bindDefinition) {
            angular.forEach(bindDefinition || {}, function (definition, scopeName) {
                // Adapted from angular.js $compile
                var match = definition.match(LOCAL_REGEXP) || [],
                    attrName = match[3] || scopeName,
                    mode = match[1], // @, =, or &
                    parentGet,
                    unwatch;

                switch (mode) {
                case "@":
                    if (!attrs[attrName]) {
                        return;
                    }
                    attrs.$observe(attrName, function (value) {
                        scope[scopeName] = value;
                    });
                    // we trigger an interpolation to ensure
                    // the value is there for use immediately
                    if (attrs[attrName]) {
                        scope[scopeName] = $interpolate(attrs[attrName])(scope);
                    }
                    break;

                case "=":
                    if (!attrs[attrName]) {
                        return;
                    }
                    unwatch = scope.$watch(attrs[attrName], function (value) {
                        scope[scopeName] = value;
                    });
                    // Destroy parent scope watcher when this scope is destroyed
                    scope.$on("$destroy", unwatch);
                    break;

                case "&":
                    if (attrs[attrName] && attrs[attrName].match(new RegExp(scopeName + "\(.*?\)"))) {
                        throw new Error("& expression binding '" + scopeName +
                                        "' looks like it will recursively call '" + attrs[attrName] +
                                        "' and cause a stack overflow! Please choose a different scopeName.");
                    }
                    parentGet = $parse(attrs[attrName]);
                    scope[scopeName] = function (locals) {
                        return parentGet(scope, locals);
                    };
                    break;
                }
            });
        };
    }

    // Exports
    module.exports = bindService;

})();
