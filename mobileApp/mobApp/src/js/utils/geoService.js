/* global module, Windows */

// utils/geoService.js
//
// factory service for geolocation module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash"),
        asyncLib = require("async");

    geoService.$inject = [
        "$q",
        "$rootScope",
        "$window",
        "$document",
        "$timeout",
        "$Mobile",
        "$App",
        "$Logger",
        "$Network"
    ];
    function geoService ($q, $rootScope, $window, $document, $timeout, $Mobile, $App, $Logger, $Network) {
        var  _ = $Mobile.locale,
            config = $App.config,
            geolocation = $window.navigator.geolocation,
            diagnostic = ld.get($window.cordova, "plugins.diagnostic"),
            locationAccuracy = ld.get($window.cordova, "plugins.locationAccuracy"),
            servicesAvailable = ld.get($window.cordova, "plugins.deviceData.servicesAvailable"),
            logger = $Logger.getInstance(),
            networkInfo = $Network,
            geoStack = [],
            watchActive = {
                id: null,
                success: ld.noop,
                error: ld.noop
            },
            geoOptions = ld.extend({
                enable: true
            }, config.options.geo),
            enableAccuracy = true, // Enable HighAccuracy by default[GPS] - !$Network.onLine(),
            availableRetry = 0,
            userReject = 0,
            userPosition;

        // Events
        $rootScope.$on("cps:network:offline", function (event, type) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            enableAccuracy = true;
        });

        $rootScope.$on("cps:network:online", function (event, type) {
            if(event.defaultPrevented) {
                return false;
            }
            event.preventDefault();
            enableAccuracy = true;
        });

        $App.ready(function () {
            if($App.isAndroid() && servicesAvailable) {
                servicesAvailable(function (services) {
                    geolocation = services.googlePlay ?
                        ld.get($window.cordova, "plugins.locationServices.geolocation") || geolocation : geolocation;
                });
            }
            $document[0].addEventListener("resume", function () {
                enableWatch(true);
            }, false);
            $document[0].addEventListener("pause", function () {
                enableWatch(false);
            }, false);
        });

        // Members
        var Geo = {
            options: geoOptions,
            isEnable: isEnable,
            clearGeo: clearGeo,
            getUserPosition: getUserPosition,
            hasUserPosition: hasUserPosition,
            setUserPosition: setUserPosition,
            getCurrentPosition: getCurrentPosition,
            stackGetPosition: stackGetPosition,
            hasStackGetPosition: hasStackGetPosition,
            clearGetPosition: clearGetPosition,
            cancelGetPosition: cancelGetPosition,
            watchPosition: watchPosition,
            clearWatch: clearWatch,
            enableWatch: enableWatch,
            getWatchId: getWatchId,
            isAccuracy: isAccuracy,
            setAccuracy: setAccuracy

        };
        return Geo;

        // Functions
        function isEnable () {
            return geoOptions.enable && !!geolocation;
        }
        function clearGeo () {
            clearWatch();
            clearGetPosition();
        }

        function getUserPosition () {
            return userPosition;
        }

        function hasUserPosition () {
            return userPosition && userPosition.coords ? true : false;
        }

        function setUserPosition (position) {
            if(ld.has(position, "coords")) {
                // Set shortcut
                position.lat =  position.coords.latitude;
                position.lng =  position.coords.longitude;
                userPosition = position;
            }
        }

        function getCurrentPosition () {
            var deferred = $q.defer(),
                platform = $App.getPlatform() || {},
                id;

            // Extend promise
            deferred.promise.cancel = function () {
                cancelGetPosition(id);
                deferred.reject({canceled: true});
            };

            asyncLib.series([
                function (next) {
                    if(!geolocation) {
                        $Mobile.alert(_("alertGeoNotSupported", _("alertgeonotsupported")));
                        return next({
                            code: 4,
                            message: "Geolocation is not supported by this device."
                        });
                    }
                    next();
                },
                function (next) {
                    if(!diagnostic) {
                        if($App.isCordova()) {
                            logger.warn("geo", "for better behavior plugin 'cordova.plugins.diagnostic' is required");
                        }
                        return next();
                    }
                    diagnostic.isLocationAuthorized(function (enabled) {
                        logger.debug("geo", "Location is " + (enabled ? "authorized" : "unauthorized"));
                        if(!enabled) {
                            var timerAuthorization = $timeout(function () {
                                logger.debug("geo", "Timeout waiting authorization request");
                                next();
                            }, 20000);

                            diagnostic.requestLocationAuthorization(function (status) {
                                logger.debug("geo", "Location request response: {0} ", [status]);
                                $timeout.cancel(timerAuthorization);

                                if(status == diagnostic.permissionStatus.DENIED ||
                                   status == diagnostic.permissionStatus.DENIED_ALWAYS) {
                                    $Mobile.alert(_("alertLocationAuthorizedReject"));
                                    next({code: 4, message: status});
                                } else {
                                    next();
                                }
                            }, function (err) {
                                logger.debug("geo", "Location request error: {0} ",
                                             [ld.isObject(err) ? JSON.stringify(err) : err]);
                                $timeout.cancel(timerAuthorization);
                                next(err);
                            }, ld.get(diagnostic, "locationAuthorizationMode.ALWAYS"));
                        } else {
                            next();
                        }

                    }, next);
                },
                function (next) {
                    if(!diagnostic) {
                        return next();
                    }
                    diagnostic.isLocationEnabled(function (enabled) {
                        logger.debug("geo", "Location is " + (enabled ? "enabled" : "disabled"));
                        if(!enabled) {
                            requestLocationEnable().then(function () {
                                next();
                            }, next);
                        } else {
                            next();
                        }
                    }, next);
                },
                function (next) {
                    // Check if Location is Available
                    if(!diagnostic) {
                        return next();
                    }
                    diagnostic.isLocationAvailable(function (available) {
                        if(!available) {
                            availableRetry++;
                            if(availableRetry > 3) {
                                $Mobile.alert(_("alertLocationUnavailable"));
                                availableRetry = 0;
                                return next({
                                    code: 1,
                                    message: "Location is Unavailable"
                                });
                            }
                        }
                        next();
                    }, next);
                }
            ], function (err) {
                if(err) {
                    $Mobile.loading("hide");
                    err = ld.isString(err) ? {code: 2, message: err} : err;
                    deferred.reject(err);
                } else if (geoStack.length > 2) {
                    if(userPosition) {
                        deferred.resolve(userPosition);
                    } else {
                        deferred.reject({backgroundJob: true});
                    }
                    logger.debug("geo", "have {0} getCurrentPosition operation active", [geoStack.length]);
                } else {
                    if(parseInt(platform.id) === 3) {
                        networkInfo = Windows.Networking.Connectivity.NetworkInformation.getInternetConnectionProfile();
                        if(networkInfo.isWlanConnectionProfile === false) {
                            enableAccuracy = true;
                        }
                    }

                    // save time current getPosition
                    id = Date.now();
                    geoStack.push({
                        id: id,
                        active: true,
                        deferred: deferred,
                        promise: deferred.promise
                    });

                    // Get position
                    geolocation.getCurrentPosition(function (position) {
                        // Clean stack
                        cancelGetPosition(id);

                        // Success
                        position = ld.toPlainObjectDeep(position);
                        setUserPosition(position);
                        deferred.resolve(position);

                    }, function (err) {
                        if(!ld.isObject(err)) {
                            err = {code: 4, message: err || "Unknown error" };
                        }
                        logger.debug("geo", "getCurrentPosition: message: {0} - code: {1}", [err.message, err.code]);

                        onFallbackPosition(err).then(function (positionFb) {
                            cancelGetPosition(id);

                            // Success fallback
                            positionFb = ld.toPlainObjectDeep(positionFb);
                            setUserPosition(positionFb);
                            deferred.resolve(positionFb);

                        }).catch(function (err) {
                            $Mobile.loading("hide");

                            var msgError = {
                                1: _("alertGeoPermission", _("alertgeopermission")),
                                2: _("alertGeoUnavailable", _("alertgeounavailable")),
                                3: _("alertGeoTimeout", _("alertgeotimeout")),
                                default: _("alertUnknownError")
                            }[err.code];

                            if(stackGetPosition(id).active && geoStack.length <= 1) {
                                if(err.code === 1) {
                                    switchToLocationSettings({
                                        textConfirm: _("alertLocationPermission", _("alertLocationRequest"))
                                    });
                                } else {
                                    $Mobile.alert(msgError ? msgError : msgError["default"]);
                                }
                            }

                            cancelGetPosition(id);
                            deferred.reject(err);
                        });
                    }, {
                        timeout: 27000,
                        enableHighAccuracy: enableAccuracy
                    });
                }
            });
            return deferred.promise;
        }

        function stackGetPosition (gid) {
            return ld.find(geoStack, {id: gid}) || {};
        }

        function hasStackGetPosition () {
            return geoStack && geoStack.length >= 1;
        }

        function cancelGetPosition (gid) {
            var index = ld.findIndex(geoStack, {id: gid});
            if(index > -1) {
                ld.pullAt(geoStack, index);
            }
        }

        function clearGetPosition () {
            var geoActive = geoStack.length ? ld.filter(geoStack, {active: true}) : [];
            if(geoActive.length) {
                ld.forEach(geoStack, function (value, index) {
                    if(value && value.active) {
                        value.active = false;
                    }
                    geoStack[index] = value;
                    if(ld.has(geoStack[index], "promise.cancel")) {
                        geoStack[index].promise.cancel();
                    }
                });
            }
            return geoStack;
        }

        function watchPosition (success, error) {
            if(watchActive.id) {
                clearWatch(watchActive.id);
            }
            watchActive.id = geolocation.watchPosition(function (position) {
                position = ld.toPlainObjectDeep(position);
                setUserPosition(position);

                if(ld.isFunction(success)) {
                    success(position);
                }
            }, function (err) {
                if(!ld.isObject(err)) {
                    err = {code: 4, message: err || "Unknown error" };
                }
                if(ld.isFunction(error)) {
                    error(err);
                }
                logger.debug("geo", "watchPosition: message: {0} - code: {1}", [err.message, err.code]);
            }, {
                timeout: 27000,
                enableHighAccuracy: enableAccuracy
            });
            watchActive.success = success || ld.noop;
            watchActive.error = error || ld.noop;

            return watchActive.id;
        }

        function clearWatch (wId) {
            var id = wId || watchActive.id;
            if(id) {
                geolocation.clearWatch(id);
                watchActive = {
                    id: null,
                    success: ld.noop,
                    error: ld.noop
                };
            }
        }

        function enableWatch (_enable) {
            if(_enable && watchActive.id) {
                watchPosition(watchActive.success, watchActive.error);
            } else if(watchActive.id) {
                geolocation.clearWatch(watchActive.id);
            }
        }

        function getWatchId () {
            return watchActive.id;
        }

        function isAccuracy () {
            return enableAccuracy;
        }

        function setAccuracy (accuracy) {
            enableAccuracy = accuracy;
        }

        // Private
        function requestLocationEnable () {
            var deferred = $q.defer();

            if(!diagnostic) {
                deferred.resolve();
                return deferred.promise;
            }

            if(!locationAccuracy.request) {
                return switchToLocationSettings();
            }

            asyncLib.series([
                function (next) {
                    locationAccuracy.request(function (success) {
                        success = success || {
                            code: 1,
                            message: "User agreed to make required location settings changes."
                        };

                        logger.debug("geo", "Accuracy request success: {0}", [JSON.stringify(success)]);
                        next(null, success);
                    }, function (reject) {
                        reject = reject || {code: 2, message: "Cannot turn on Location or user disagreed"};
                        if(ld.isString(reject)) {
                            reject = reject === "Location services is already enabled" ? {code: 0, message: reject} :
                            (reject === "Location is already being requested" ? {code: -1, message: reject} :
                             {code: 2, message: reject});
                        }

                        logger.warn("geo", "Accuracy request failed, error code: {0} - error message: {1}", [
                            reject.code, reject.message
                        ]);

                        if(reject.code == locationAccuracy.ERROR_ALREADY_REQUESTING) {
                            next(reject);
                        } else {
                            $Mobile.ga("send", "event", "error", "geo: accuracy request error: " +
                                       reject.message + "- code: " + reject.code);
                            next({toLocationSettings: true});
                        }
                    }, locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY || 3);
                }
            ], function (err, results) {

                if(ld.get(err, "toLocationSettings")) {
                    switchToLocationSettings().then(deferred.resolve, deferred.reject);
                } else if(err) {
                    deferred.reject(err);
                } else {
                    results = ld.last(results) || {code: 0, message: "All location settings are satisfied."};
                    deferred.resolve(results);
                }
            });

            return deferred.promise;
        }

        function switchToLocationSettings (opts) {
            var deferred = $q.defer();
            opts = ld.extend({
                textConfirm: _("alertLocationRequest")
            }, opts);

            if(!diagnostic) {
                deferred.resolve();
                return deferred.promise;
            }

            $Mobile.confirm(opts.textConfirm).then(function (buttonIndex) {
                if(buttonIndex == 1) {
                    if(diagnostic.switchToLocationSettings) {
                        diagnostic.switchToLocationSettings();
                    } else {
                        diagnostic.switchToSettings();
                    }
                    deferred.resolve({
                        code: 1,
                        message: "User agreed to make required location settings changes."
                    });
                } else {
                    if(userReject === 0) {
                        userReject++;
                        $Mobile.alert(_("alertLocationReject"));
                    }
                    deferred.reject({
                        code: 4,
                        message: "User chose not to make required location settings changes."
                    });
                }
            });
            return deferred.promise;
        }

        function onFallbackPosition (onError) {
            var deferred = $q.defer();

            geolocation.getCurrentPosition(function (position) {
                deferred.resolve(position);
            }, function (err) {
                if(!ld.isObject(err)) {
                    err = {code: 4, message: err || "Unknown error" };
                }
                logger.debug("geo", "getCurrentPosition: message: {0} - code: {1}", [err.message, err.code]);

                deferred.reject(err);
            }, {
                timeout: 27000,
                enableHighAccuracy: !enableAccuracy
            });
            return deferred.promise;
        }
    }

    // Exports
    module.exports = geoService;

})();
