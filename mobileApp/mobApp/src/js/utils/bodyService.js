/* global module */

// utils/bodyService.js
//
// body utils factory for cps mobile app
// from Ionic
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    bodyService.$inject = ["$document"];
    function bodyService ($document) {
        // Members
        var Body = {
            addClass: addClass,
            removeClass: removeClass,
            enableClass: enableClass,
            append: append,
            get: get
        };

        return Body;

        // Functions
        /**
         * add
         *
         * Add a class to the document's body element.
         * @param {string} class Each argument will be added to the body element.
         *
         * @returns {$Body} The $Body service so methods can be chained.
         */
        function addClass () {
            var addClassFun = function (args) {
                for (var x = 0; x < args.length; x++) {
                    $document[0].body.classList.add(args[x]);
                }
                return this;
            };

            return addClassFun(arguments);
        }
        /**
         * removeClass
         *
         * Remove a class from the document's body element.
         * @param {string} class Each argument will be removed from the body element.
         *
         * @returns {$Body} The $Body service so methods can be chained.
         */
        function removeClass () {
            var removeClassFun = function (args) {
                for (var x = 0; x < args.length; x++) {
                    $document[0].body.classList.remove(args[x]);
                }
                return this;
            };

            return removeClassFun(arguments);
        }

        /**
         * enableClass
         *
         * Similar to the `add` method, except the first parameter accepts a boolean
         * value determining if the class should be added or removed. Rather than writing user code,
         * such as "if true then add the class, else then remove the class", this method can be
         * given a true or false value which reduces redundant code.
         * @param {boolean} shouldEnableClass A true/false value if the class should be added or removed.
         * @param {string} class Each remaining argument would be added or removed depending on
         * the first argument.
         *
         * @returns {$Body} The $Body service so methods can be chained.
         */
        function enableClass (shouldEnableClass) {
            var enableClassFun = function (_arguments) {
                var args = Array.prototype.slice.call(_arguments).slice(1);
                if (shouldEnableClass) {
                    addClass.apply(this, args);
                } else {
                    removeClass.apply(this, args);
                }
                return this;
            };

            return enableClassFun(arguments);
        }

        /**
         * append
         *
         * Append a child to the document's body.
         * @param {element} element The element to be appended to the body. The passed in element
         * can be either a jqLite element, or a DOM element.
         *
         * @returns {$Body} The $Body service so methods can be chained.
         */
        function append (el) {
            var appendFun = function (ele) {
                $document[0].body.appendChild(ele.length ? ele[0] : ele);
                return this;
            };

            return appendFun(el);
        }

        /**
         * get
         *
         * Get the document's body element.
         *
         * @returns {element} Returns the document's body element.
         */
        function get () {
            return $document[0].body;
        }
    }

    // Exports
    module.exports = bodyService;

})();
