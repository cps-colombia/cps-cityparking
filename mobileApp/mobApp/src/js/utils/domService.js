/* global module */

// utils/domService.js
//
// dom utils factory for cps mobile app
// some by Ionic
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        $$ = angular.element;

    domService.$inject = [
        "$q",
        "$document",
        "$window"
    ];
    function domService ($q, $document, $window) {

        var readyCallbacks = [],
            isDomReady = $document[0].readyState === "complete" || $document[0].readyState === "interactive",
            _cancelAnimationFrame = $window.cancelAnimationFrame ||
                $window.webkitCancelAnimationFrame ||
                $window.mozCancelAnimationFrame ||
                $window.webkitCancelRequestAnimationFrame;

        if (!isDomReady) {
            $document[0].addEventListener("DOMContentLoaded", domReady);
        }

        // From the man himself, Mr. Paul Irish.
        // The requestAnimationFrame polyfill
        // Put it on window just to preserve its context
        // without having to use .call
        $window._rAF = (function () {
            return $window.requestAnimationFrame ||
                $window.webkitRequestAnimationFrame ||
                $window.mozRequestAnimationFrame ||
                function (callback) {
                    $window.setTimeout(callback, 16);
                };
        })();

        // Members
        var Dom = {
            domReady: domReady,
            ready: ready,
            contains: contains,
            requestAnimationFrame: requestAnimationFrame,
            cancelAnimationFrame: cancelAnimationFrame,
            animationComplete: animationComplete,
            animationFrameThrottle: animationFrameThrottle,
            getChildWithClass: getChildWithClass,
            getParentWithClass: getParentWithClass,
            getParentOrSelfWithClass: getParentOrSelfWithClass,
            getPositionInParent: getPositionInParent,
            getOffsetTop: getOffsetTop,
            getTextBounds: getTextBounds,
            getBlockNodes: getBlockNodes,
            querySelectorAll: querySelectorAll,
            querySelector: querySelector,
            CustomEvent: customEvent(),
            trigger: trigger,
            off: off,
            on: on,
            blurAll: blurAll,
            cachedAttr: cachedAttr,
            cachedStyles: cachedStyles
        };
        return Dom;

        // Functions
        /**
         * @name ready
         * @description Call a function when the DOM is ready, or if it is already ready
         * call the function immediately.
         * @param {function} callback The function to be called.
         */
        function domReady () {
            isDomReady = true;
            for (var x = 0; x < readyCallbacks.length; x++) {
                requestAnimationFrame(readyCallbacks[x]);
            }
            readyCallbacks = [];
            $document[0].removeEventListener("DOMContentLoaded", domReady);
        }

        function ready (cb) {
            if (isDomReady) {
                requestAnimationFrame(cb);
            } else {
                readyCallbacks.push(cb);
            }
        }


        function contains (parentNode, otherNode) {
            var current = otherNode;
            while (current) {
                if (current === parentNode) {
                    return true;
                }
                current = current.parentNode;
            }
        }

        function requestAnimationFrame (cb) {
            return $window._rAF(cb);
        }

        function cancelAnimationFrame (requestId) {
            _cancelAnimationFrame(requestId);
        }

        function animationComplete (element, cb) {
            var deferred = $q.defer(),
                endEvent = whichEndEvent(),
                isAnimation = detectCSSFeature("animation", element),
                event = isAnimation ? endEvent.anim : endEvent.trans,
                fireEvent = false;

            cb = cb || angular.noop;

            if(element && event) {
                element.addEventListener(event, function () {
                    if(!fireEvent) {
                        fireEvent = true;
                        cb();
                        deferred.resolve(event);
                    }
                });
            } else {
                cb();
                deferred.resolve(event);
            }
            return deferred.promise;
        }

        /**
         * @name animationFrameThrottle
         * @description When given a callback, if that callback is called 100 times between
         * animation frames, adding Throttle will make it only run the last of
         * the 100 calls.
         * @param {function} callback a function which will be throttled to
         * requestAnimationFrame
         *
         * @returns {function} A function which will then call the passed in callback.
         * The passed in callback will receive the context the returned function is
         * called with.
         */
        function animationFrameThrottle (cb) {
            var isQueued, context, animationFrameContext;

            // Context
            animationFrameContext = function animationFrameContext (args) {
                context = this;
                if (!isQueued) {
                    isQueued = true;
                    requestAnimationFrame(function () {
                        cb.apply(context, args);
                        isQueued = false;
                    });
                }
            };

            return animationFrameContext(arguments);
        }

        /**
         * @name getChildWithClass
         * @param {DOMElement} element
         * @param {string} className
         * @returns {DOMElement} The child of element matching the
         * className, or null.
         */
        function getChildWithClass (e, className, depth) {
            depth = depth || 10;
            while (e.childNode && depth++) {
                if (e.childNode.classList && e.childNode.classList.contains(className)) {
                    return e.childNode;
                }
                e = e.childNode;
            }
            return null;
        }

        /**
         * @name getParentWithClass
         * @param {DOMElement} element
         * @param {string} className
         * @returns {DOMElement} The closest parent of element matching the
         * className, or null.
         */
        function getParentWithClass (e, className, depth) {
            depth = depth || 10;
            while (e.parentNode && depth--) {
                if (e.parentNode.classList && e.parentNode.classList.contains(className)) {
                    return e.parentNode;
                }
                e = e.parentNode;
            }
            return null;
        }

        /**
         * @name getParentOrSelfWithClass
         * @param {DOMElement} element
         * @param {string} className
         *
         * @returns {DOMElement} The closest parent or self matching the
         * className, or null.
         */
        function getParentOrSelfWithClass (e, className, depth) {
            depth = depth || 10;
            while (e && depth--) {
                if (e.classList && e.classList.contains(className)) {
                    return e;
                }
                e = e.parentNode;
            }
            return null;
        }

        /**
         * @name querySelectorAll
         * @description Polyfill returns a list of the elements within the document
         * (using depth-first pre-order traversal of the document's nodes)
         * that match the specified group of selectors. The object returned is a NodeList.
         * @param {string} selectors is a string containing one or more CSS selectors separated by commas
         * @param {DOMElement} element The element to find the offset of.
         * @param {integer} maxCount max elements to return
         * @returns {DOMElement} Elements match
         */
        function querySelectorAll (selectors, context, maxCount) {
            context = context || $document[0];

            if (context.querySelectorAll) {
                return context.querySelectorAll(selectors);
            } else {
                var style = context.createElement("style"), elements = [], element;
                context.documentElement.firstChild.appendChild(style);
                context._qsa = [];

                style.styleSheet.cssText = selectors + "{x-qsa:expression(document._qsa && document._qsa.push(this))}";
                $window.scrollBy(0, 0);
                style.parentNode.removeChild(style);

                while (context._qsa.length) {
                    element = context._qsa.shift();
                    element.style.removeAttribute("x-qsa");
                    elements.push(element);
                    if (elements.length > maxCount) {
                        break;
                    }
                }
                context._qsa = null;
                return elements;
            }
        }

        function querySelector (selectors, context) {
            context = context || $document[0];

            if (context.querySelector) {
                return context.querySelector(selectors);
            } else {
                var elements = querySelectorAll(selectors, context, 1);
                return (elements.length) ? elements[0] : null;
            }
        }

        /**
         * @name getPositionInParent
         * @description
         * Find an element's scroll offset within its container.
         * @param {DOMElement} element The element to find the offset of.
         * @returns {object} A position object with the following properties:
         *   - `{number}` `left` The left offset of the element.
         *   - `{number}` `top` The top offset of the element.
         */
        function getPositionInParent (el) {
            return {
                left: el.offsetLeft,
                top: el.offsetTop
            };
        }

        function getOffsetTop (el) {
            var curtop = 0;
            if (el.offsetParent) {
                do { // eslint-disable-line
                    curtop += el.offsetTop;
                } while ((el = el.offsetParent));
                return curtop;
            }
        }

        /**
         * @name getTextBounds
         * @description
         * Get a rect representing the bounds of the given textNode.
         *
         * @param {DOMElement} textNode The textNode to find the bounds of.
         * @returns {object} An object representing the bounds of the node. Properties:
         *   - `{number}` `left` The left position of the textNode.
         *   - `{number}` `right` The right position of the textNode.
         *   - `{number}` `top` The top position of the textNode.
         *   - `{number}` `bottom` The bottom position of the textNode.
         *   - `{number}` `width` The width of the textNode.
         *   - `{number}` `height` The height of the textNode.
         */
        function getTextBounds (textNode) {
            if (document.createRange) {
                var range = document.createRange();
                range.selectNodeContents(textNode);
                if (range.getBoundingClientRect) {
                    var rect = range.getBoundingClientRect();
                    if (rect) {
                        var sx = window.scrollX;
                        var sy = window.scrollY;

                        return {
                            top: rect.top + sy,
                            left: rect.left + sx,
                            right: rect.left + sx + rect.width,
                            bottom: rect.top + sy + rect.height,
                            width: rect.width,
                            height: rect.height
                        };
                    }
                }
            }
            return null;
        }

        /**
         * @name getBlockNodes
         * @description Return the DOM siblings between the first and last node in the given array.
         * @param {Array} array like object
         *
         * @returns {Array} the inputted object or a jqLite collection containing the nodes
         */
        function getBlockNodes (nodes) {
            // From angular
            // TODO(perf): update `nodes` instead of creating a new object?
            var slice = [].slice,
                node = nodes[0],
                endNode = nodes[nodes.length - 1],
                blockNodes,
                i;

            for (i = 1; node !== endNode && (node = node.nextSibling); i++) { // eslint-disable-line
                if (blockNodes || nodes[i] !== node) {
                    if (!blockNodes) {
                        blockNodes = $$(slice.call(nodes, 0, i));
                    }
                    blockNodes.push(node);
                }
            }

            return blockNodes || nodes;
        }

        /**
         * @name customEvent
         * @description Custom event polyfill

         * @return {DOMEvent} custom dom event
         */
        function customEvent () {
            if(typeof $window.CustomEvent === "function") {
                return $window.CustomEvent;
            }

            var _customEvent = function (event, params) {
                var evt;
                params = params || {
                    bubbles: false,
                    cancelable: false,
                    detail: undefined
                };
                try {
                    evt = document.createEvent("CustomEvent");
                    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
                } catch (error) {
                    // fallback for browsers that don't support createEvent('CustomEvent')
                    evt = document.createEvent("Event");
                    for (var param in params) {
                        if(params.hasOwnProperty(param)) {
                            evt[param] = params[param];
                        }
                    }
                    evt.initEvent(event, params.bubbles, params.cancelable);
                }
                return evt;
            };
            _customEvent.prototype = $window.Event.prototype;
            return _customEvent;
        }

        /**
         * @name trigger
         * @description Trigger a new event
         *
         * @param {string} eventType The event to trigger.
         * @param {object} data The data for the event. Hint: pass in
         * `{target: targetElement}`
         * @param {boolean=} bubbles Whether the event should bubble up the DOM.
         * @param {boolean=} cancelable Whether the event should be cancelable.
         */
        function trigger (eventType, data, bubbles, cancelable) {
            var event = new Dom.CustomEvent(eventType, {
                detail: data,
                bubbles: !!bubbles,
                cancelable: !!cancelable
            });

            // Make sure to trigger the event on the given target, or dispatch it from
            // the window if we don't have an event target
            if(data && data.target && data.target.dispatchEvent) {
                data.target.dispatchEvent(event);
            } else {
                $window.dispatchEvent(event);
            }
        }

        /**
         * @name off
         * @description Remove an event listener.
         *
         * @param {string} type
         * @param {function} callback
         * @param {DOMElement} element
         */
        function off (type, callback, element) {
            element.removeEventListener(type, callback);
        }

        /**
         * @name on
         * @description Add an event listener.
         *
         * @param {string} type
         * @param {function} callback
         * @param {DOMElement} element
         */
        function on (type, callback, element) {
            element = element || $window;
            element.addEventListener(type, callback);
        }

        /**
         * @name blurAll
         * @description Blurs any currently focused input element
         *
         * @returns {DOMElement} The element blurred or null
         */
        function blurAll () {
            if ($document[0].activeElement && $document[0].activeElement !== $document[0].body) {
                $document[0].activeElement.blur();
                return $document[0].activeElement;
            }
            return null;
        }

        function cachedAttr (ele, key, value) {
            ele = ele && ele.length && ele[0] || ele;
            if (ele && ele.setAttribute) {
                var dataKey = "$attr-" + key;
                if (arguments.length > 2) {
                    if (ele[dataKey] !== value) {
                        ele.setAttribute(key, value);
                        ele[dataKey] = value;
                    }
                } else if (typeof ele[dataKey] === "undefined") {
                    ele[dataKey] = ele.getAttribute(key);
                }
                return ele[dataKey];
            }
        }

        function cachedStyles (ele, styles) {
            ele = ele && ele.length && ele[0] || ele;
            if (ele && ele.style) {
                for (var prop in styles) {
                    if (ele["$style-" + prop] !== styles[prop]) {
                        ele.style[prop] = ele["$style-" + prop] = styles[prop];
                    }
                }
            }
        }

        // Private
        function whichEndEvent () {
            var el = document.createElement("vue"),
                defaultEvent = "transitionend",
                events = {
                    "webkitTransition" : "webkitTransitionEnd",
                    "transition"       : defaultEvent,
                    "mozTransition"    : defaultEvent
                },
                ret = {},
                name;

            for (name in events) {
                if (el.style[name] !== undefined) {
                    ret.trans = events[name];
                    break;
                }
            }

            ret.anim = el.style.animation === "" ?
                "animationend" : "webkitAnimationEnd";

            return ret;
        }

        function detectCSSFeature (featurename, element) {
            var elm = element || document.createElement("div"),
                elementFeature = element ? "" : null,
                domPrefixes = "Webkit Moz ms".split(" "),
                featurenameCapital = null,
                feature = null;

            featurename = featurename.toLowerCase();

            if(elm.style[featurename] !== undefined && elm.style[featurename] !== elementFeature) {
                feature = true;
            }

            if(feature === false) {
                featurenameCapital = featurename.charAt(0).toUpperCase() + featurename.substr(1);
                for(var i = 0; i < domPrefixes.length; i++) {
                    if(elm.style[domPrefixes[i] + featurenameCapital ] !== undefined &&
                        elm.style[domPrefixes[i] + featurenameCapital ] !== elementFeature) {
                        feature = true;
                        break;
                    }
                }
            }
            return feature;
        }

    }

    // Exports
    module.exports = domService;

})();
