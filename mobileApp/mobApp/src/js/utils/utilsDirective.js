/* global module */

// app/utils/utilsDirective.js
//
// utils directives function
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    // Members
    var utilsDirective =  {
        cpsValidated: cpsValidated,
        cpsEvent: cpsEvent,
        cpsFormat: cpsFormat,
        cpsCleanNumber: cpsCleanNumber,
        cpsFormSuccess: cpsFormSuccess,
        cpsMatch: cpsMatch,
        cpsDates: cpsDates,
        cpsIsHold: cpsIsHold,
        cpsHold: cpsHold,
        cpsFullHeight: cpsFullHeight,
        cpsSection: cpsSection,
        cpsExitApp: cpsExitApp,
        cpsModule: cpsModule,
        cpsOpenLink: cpsOpenLink,
        cpsTimeAgo: cpsTimeAgo,
        cpsSelect: cpsSelect,
        cpsKeyboardAttach: cpsKeyboardAttach,
        cpsKeyboardHandler: cpsKeyboardHandler
    };

    // Validated Directive
    cpsValidated.$inject = ["$parse", "$Bind"];
    function cpsValidated ($parse, $Bind) {
        var validated =  {
            restrict: "AC",
            require: "^form",
            link: postLink
        };

        return validated;

        // Function
        function postLink (scope, element, attrs, form) {
            var inputs = element.find("*"),
                cpsIsInvalid = attrs.cpsIsInvalid;

            $Bind(scope, attrs, {
                cpsIsInvalid: "="
            });

            // TODO: change to check all form or only field
            ld.forEach(inputs, function (input, key) {
                var inputAttributes = input.attributes,
                    field;

                if (!ld.isNotdefined(inputAttributes.getNamedItem("ng-model")) &&
                    !ld.isNotdefined(inputAttributes.getNamedItem("name"))) {

                    field = form[inputAttributes.name.value];
                    if (!ld.isNotdefined(field)) {
                        window.inputAttributes = field;
                        if(!ld.isNotdefined(cpsIsInvalid)) {
                            scope.$watch("cpsIsInvalid", function (newVal, oldVal) {
                                if(newVal) {
                                    field.$valid = false;
                                }
                            });
                        }
                        scope.$watchCollection(function () {
                            return {
                                $valid: field.$valid,
                                $submitted: form.$submitted
                            };
                        }, function (newVal, oldVal) {
                            if(newVal.$submitted) {
                                if (newVal.$valid) {
                                    if(scope.cpsIsInvalid) {
                                        cpsIsInvalid = $parse(cpsIsInvalid);
                                        cpsIsInvalid.assign(scope, false);
                                    }
                                    element.removeClass("ng-input-invalid").addClass("ng-input-valid");
                                } else {
                                    element.removeClass("ng-input-valid").addClass("ng-input-invalid");
                                }
                            } else {
                                element.removeClass("ng-input-invalid ng-input-valid");
                            }
                        });
                    }
                }
            });
        }
    }

    cpsEvent.$inject = ["$rootScope", "$Bind", "$Gestures"];
    function cpsEvent ($rootScope, $Bind, $Gestures) {
        var cpsEventDirective = {
            restrict: "A",
            scope: true,
            link: postLink
        };
        return cpsEventDirective;

        // Functions
        function postLink (scope, element, attrs) {
            var emiter = ld.noop;
            $Bind(scope, attrs, {
                cpsEvent: "@"
            });

            $Gestures.hammer(element[0], "tap", function onTap (e) {
                e.preventDefault();
                emiter = $rootScope.$emit("cps:" + scope.cpsEvent, {
                    scope: scope,
                    element: element,
                    attrs: attrs,
                    event: e
                });
            });

            scope.$on("$destroy", function () {
                if(ld.isFunction(emiter)) {
                    emiter();
                }
            });
        }
    }

    // Insert currency format to show and return integer value to model
    cpsFormat.$inject = ["$rootScope", "$Logger", "$Numbro"];
    function cpsFormat ($rootScope, $Logger, $Numbro) {
        var cpsFormatDirective = {
            restrict: "A",
            require: "?ngModel",
            link: postLink
        };

        // Functions
        function postLink (scope, element, attrs, ngModel) {
            var minlength = attrs.ngMinlength || attrs.minlength || 3,
                varArr = attrs.cpsFormat && attrs.cpsFormat.split(":") || [], // get options, style:type
                format = varArr.length && ld.first(varArr) || "0,0",
                type = varArr.length >= 2 && varArr[1] || "format",
                pattern = attrs.ngPattern || attrs.pattern || /[1-9]+([\.,][0-9]+)*/,
                regex = new RegExp((ld.isString(pattern) ? pattern.replace(/\//g, "") : pattern)),
                plainNumber;

            // Event listen
            $rootScope.$on("cps:formatChange", function (event, isoLang) {
                applyFormat();
            });

            // Run
            applyFormat();

            // Initial formating fucntion
            function applyFormat () {
                if(ngModel) {

                    // Convert return to model
                    ngModel.$parsers.push(function (value) {
                        return type === "string" ? plainNumber : value;
                    });
                    // Convert before set model
                    ngModel.$formatters.unshift(function (a) {
                        if(ngModel.$modelValue) {
                            return $Numbro(ngModel.$modelValue).format(format);
                        } else {
                            return ngModel.$modelValue;
                        }
                    });

                    // when you type in input
                    ngModel.$parsers.unshift(function (viewValue) {
                        // Return to model
                        plainNumber = viewValue ? viewValue.replace(/[^\d|\-+]/g, "") : viewValue;
                        // Show in input
                        if(plainNumber && plainNumber.length) {
                            if(type === "string") {
                                element.val(plainNumber);
                            } else if(type === "currency") {
                                element.val($Numbro(plainNumber).formatCurrency(format));
                            } else {
                                element.val($Numbro(plainNumber).format(format));
                            }
                        } else {
                            element.val(plainNumber);
                        }
                        // Set isValid
                        ngModel.$setValidity("format", (
                            regex.test(plainNumber) && !(plainNumber && plainNumber.length < parseInt(minlength))
                        ));

                        return plainNumber;
                    });
                } else {
                    scope.$watch(function () {
                        return attrs.cpsFormatValue;
                    }, function (newVal, oldVal) {
                        if(newVal) {
                            if(type === "string") {
                                element.text(plainNumber);
                            } else if(type === "currency") {
                                element.text($Numbro(newVal).formatCurrency(format));
                            } else {
                                element.text($Numbro(newVal).format(format));
                            }
                        } else {
                            element.text("--");
                        }
                    });
                }
            }
        }

        return cpsFormatDirective;
    }

    // Clean pint and comma in number input
    function cpsCleanNumber () {
        var cleanNumberDirective = {
            restrict: "A",
            require: "?ngModel",
            link: postLink
        };
        return cleanNumberDirective;

        // Functions
        function postLink (scope, element, attrs, ngModel) {
            var clean;
            if(!ngModel) {
                return;
            }

            ngModel.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    val = "";
                } else {
                    val = val.toString();
                }
                if(val.length) {
                    clean = val.replace(/[^0-9]+/g, "");
                    if (val !== clean) {
                        ngModel.$setViewValue(clean);
                        ngModel.$render();
                    }
                    return clean;
                }
            });

            element.bind("keypress", function (event) {
                if(event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    }

    // Clean form directive
    cpsFormSuccess.$inject = ["$parse", "$Mobile"];
    function cpsFormSuccess ($parse, $Mobile) {
        var _ = $Mobile.locale,
            successFormDirective = {
                restrict: "A",
                require: "^form",
                scope: {
                    success: "=cpsFormSuccess",
                    cpsCleanData: "@"
                },
                link: postLink
            };
        return successFormDirective;

        // Functions
        function postLink (scope, element, attrs, form) {
            var originData = {},
                scopeParent = scope.$parent,
                inputs = element.find("*"),
                successForm = attrs.cpsFormSuccess,
                isCleanData = false,
                inputModelValue,
                inputModelPath,
                attributes,
                setContent,
                inputOmit;

            isCleanData = scope.cpsCleanData ?
                (scope.cpsCleanData === "true" || scope.cpsCleanData === true ? true : false) : false;

            element.on("submit",function (eventl) {
                if(form.$invalid) {
                    $Mobile.alert(_("alertEmptyFields"));
                }
            });

            scope.$watch("success", function (newVal, oldVal) {
                // if(form.$submitted){
                if(newVal && !oldVal) {
                    // if keep data no set originalData
                    if(isCleanData) {
                        // if form send success clean form
                        ld.forEach(ld.clone(originData, true), function (value, key) {
                            // get originData and assign
                            setContent = $parse(key);
                            setContent.assign(scopeParent, value);
                        });
                    }
                    // Reset estatus
                    successForm = $parse(successForm);
                    successForm.assign(scopeParent, false);
                    form.$setPristine();
                    form.$setUntouched();
                }
                // }
            });

            if(isCleanData) {
                // Save original data
                ld.forEach(inputs, function (input, key) {
                    attributes = input.attributes;
                    if (!ld.isNotdefined(attributes.getNamedItem("ng-model")) &&
                        !ld.isNotdefined(attributes.getNamedItem("name"))) {

                        inputOmit = attributes.getNamedItem("cps-omit-clean") ?
                            !!attributes.getNamedItem("cps-omit-clean").value : false;
                        // check if input omit cleaning
                        if (!inputOmit) {
                            inputModelPath =  attributes["ng-model"].value.split(".");
                            // get parent object path
                            if(inputModelPath.length > 1) {
                                inputModelPath.pop();
                            }
                            inputModelPath = inputModelPath.join(".");
                            // parse path and get values
                            setContent = $parse(inputModelPath);
                            inputModelValue = ld.clone(setContent(scopeParent));
                            // save original values
                            if(!originData[inputModelPath]) {
                                ld.extend(originData, buildObj(inputModelPath, inputModelValue));
                            }
                        }
                    }
                });
            }

            function buildObj (key, value) {
                var obj = {};
                obj[key] = value;
                return obj;
            }
            // Clean scope
            scope.$on("$destroy", function () {
                originData = {};
            });
        }
    }

    // Match Directive
    cpsMatch.$inject = ["$parse", "$Bind", "$Logger"];
    function cpsMatch ($parse, $Bind, $Logger) {
        var logger = $Logger.getInstance(),
            match = {
                restrict: "A",
                require: "?ngModel",
                scope: true,
                compile: compile
            };
        return match;

        // Functions
        function compile (tElement, tAttrs) {
            return {
                pre: preLink
            };
        }
        function preLink (scope, element, attrs, model) {
            if(!model) {
                logger.warn("Match validation requires ngModel to be on the element");
                return;
            }

            $Bind(scope, attrs, {
                cpsMatch: "="
            });

            model.$validators.cpsMatch = function (modelValue) {
                return modelValue === scope.cpsMatch;
            };
            scope.$watch("cpsMatch", function (newVal, oldVal) {
                if(newVal && model.$dirty) {
                    model.$validate();
                }
            });
        }
    }

    // Dates select directive
    cpsDates.$inject = ["$compile", "$timeout", "$Utils"];
    function cpsDates ($compile, $timeout, $Utils) {
        // Return the directive configuration.
        var cpsDatesDirective = ({
            restrict: "E",
            require: "?ngModel",
            transclude: true,
            replace: true,
            compile: compile,
            template: template
        });
        return cpsDatesDirective;

        // Functions
        function template (tElement, tAttrs) {
            var element = tAttrs.dateElement || "select",
                htmlTemplate = "<" + element + "></" + element + ">";
            return htmlTemplate;
        }

        // When we compile the directive, we have to remove the pre-interpolation
        function compile (tElement, tAttrs) {
            var type = tAttrs.dateType,
                genDate = $Utils.genDates(type),
                dateElement = tAttrs.dateElement || "select",
                isSelect = dateElement === "select",
                dateSubEl = isSelect ? "option" : (dateElement === "ul" ? "li" : "div"),
                unknownOption = isSelect ? "<option class='ng-option-empty' value='' label=''></option>" : "",
                dataHtml = "",
                i;

            // Clear the value so the attribute-interpolation directive won't
            // bind a watcher.
            tAttrs.$set("dateType", null);
            tAttrs.$set("dateElement", null);

            // Return the linking function.
            return {
                post: postLink
            };

            // Fucntions
            function postLink (scope, element, attrs, ngModel) {
                $timeout(function () {
                    if(genDate) {
                        // Create date
                        // TODO: Create element with pure js, more elegant
                        for (i = 0; i < genDate.length; i++) {
                            dataHtml += "<" + dateSubEl + " " +
                                (genDate[i].locale ? "locale-id='" + genDate[i].locale + "' " : "") +
                                " label='" + (genDate[i].label || genDate[i].value) + "'>" + genDate[i].value +
                                "</" + dateSubEl + ">";
                        }
                    }

                    if(ngModel && ld.isUndefined(ngModel.$viewValue)) {
                        dataHtml = unknownOption + dataHtml;
                    }

                    // Append new content
                    element.html(dataHtml);
                    $compile(element.contents())(scope);
                });
            }
        }
    }

    // DEPRECATED use cpsHold
    cpsIsHold.$inject = ["$timeout", "$Loading"];
    function cpsIsHold ($timeout, $Loading) {
        var cpsIsHoldDirective = {
            restrict: "A",
            compile: compile,
            scope: true
        };
        return cpsIsHoldDirective;

        // Functions
        function compile (tElement, tAttrs) {
            if(tAttrs.ngDisabled) {
                tAttrs.$set("ngDisabled", "isDisabled");
            }
            return {
                post: postLink
            };
        }
        function postLink (scope, element, attrs) {
            scope.isDisabled = false;
            $timeout(function () {
                if(!attrs.ngDisabled) {
                    attrs.$set("disabled", false);
                }
            });
            // Watch when Loading show disable btn
            scope.$watch(function () {
                return $Loading.isShow();
            }, function (newVal, oldVal) {
                scope.isDisabled = newVal;
                if(!attrs.ngDisabled) {
                    element.attr("disabled", newVal);
                }
            });

            scope.$on("$destroy", function () {
                scope.isDisabled = false;
                if(!attrs.ngDisabled) {
                    element.attr("disabled", false);
                }
            });
        }
    }

    cpsHold.$inject = ["$timeout", "$Bind", "$Loading"];
    function cpsHold ($timeout, $Bind, $Loading) {
        var cpsHoldDirective = {
            restrict: "A",
            compile: compile,
            scope: true
        };
        return cpsHoldDirective;

        // Functions
        function compile (tElement, tAttrs) {
            if(tAttrs.ngDisabled) {
                tAttrs.$set("ngDisabled", "isDisabled");
            }
            return {
                pre: preLink,
                post: postLink
            };
        }
        function preLink (scope, element, attrs) {
            $Bind(scope, attrs, {
                cpsHold: "="
            });
        }
        function postLink (scope, element, attrs) {
            scope.isDisabled = false;
            $timeout(function () {
                if(!attrs.ngDisabled) {
                    attrs.$set("disabled", false);
                }
            });
            // Watch when Loading show disable btn
            scope.$watch(function () {
                return $Loading.isShow();
            }, function (newVal, oldVal) {
                if(scope.cpsHold !== "false" && scope.cpsHold !== false) {
                    scope.isDisabled = newVal;
                    if(!attrs.ngDisabled) {
                        element.attr("disabled", newVal);
                    }
                }
            });

            scope.$on("$destroy", function () {
                scope.isDisabled = false;
                if(!attrs.ngDisabled) {
                    element.attr("disabled", false);
                }
            });
        }
    }

    cpsFullHeight.$inject = ["$window", "$Dom", "$Gestures"];
    function cpsFullHeight ($window, $Dom, $Gestures) {
        var cpsFullHeightDirective = {
            restrict: "A",
            link: postLink
        };
        return cpsFullHeightDirective;
        // Functions
        function postLink (scope, element, attrs) {
            var rootEl = element.parents(attrs.rootElement || "cps-content"),
                parentEl = element.parent(),
                events = "orientationchange",
                restHeight = 0,
                content = 0,
                padding = 0,
                margin = 0;

            if(attrs.resize === "true") {
                events = events + " resize";
            }
            // Resize
            scope.initializeContentSize = ld.setDebounce(initializeContentSize, 10, true);
            // Gesture call only in real resize change $$ call any change
            $Gestures.on($window, events, scope.initializeContentSize);

            scope.initializeContentSize();

            // Listen native keyboard hide to fix resize parentEl
            scope.$on("cps:keyboard:hide", scope.initializeContentSize);

            // Public function
            function initializeContentSize () {
                $Dom.requestAnimationFrame(function () {
                    if(rootEl && rootEl.outerHeight) {
                        restHeight = attrs.restHeight || 0;
                        content = rootEl.outerHeight();

                        if(parentEl && parentEl.css) {
                            padding = (parseInt(parentEl.css("padding-top") || 0) +
                                       parseInt(parentEl.css("padding-bottom") || 0));
                            margin = (parseInt(parentEl.css("margin-top") || 0) +
                                      parseInt(parentEl.css("margin-bottom") || 0));
                            restHeight = restHeight + (padding + margin);
                        }

                        ld.forEach(rootEl.find(".rest-height"), function (el, key) {
                            if(el) {
                                restHeight = restHeight + ($$(el).outerHeight() || 0);
                            }
                        });
                        element.height(content - restHeight);
                    }
                });
            }

            // Clean
            scope.$on("$destroy", function () {
                $Gestures.off($window, events, scope.initializeContentSize);
                restHeight = 0;
                content = 0;
            });
        }
    }

    cpsSection.$inject = ["$location"];
    function cpsSection ($location) {
        var cpsSectionDirective = {
            restrict: "A",
            compile: compile
        };
        return cpsSectionDirective;

        // Functions
        function compile (tElement, tAttrs) {
            tElement.addClass(sectionClass($location.path()));
            return postLink;
        }
        function postLink (scope, element, attrs) {
            // Watch section
            scope.$watch(function () {
                return $location.path();
            }, function (newVal, oldVal) {
                if(newVal !== oldVal) {
                    element.removeClass(sectionClass(oldVal));
                    element.addClass(sectionClass(newVal));
                }
            });
        }
        function sectionClass (section) {
            return "section-" + ld.kebabCase(section);
        }
    }

    cpsExitApp.$inject = ["$App", "$Mobile"];
    function cpsExitApp ($App, $Mobile) {
        var cpsExitAppDirective = {
            restrict: "E",
            replace: true,
            template: "<a hm-tap='exitApp($event)'><i class='icon ion-power'></i>" +
                "<span locale-id='close'>Close</span></a>",
            compile: compile
        };
        return cpsExitAppDirective;

        // Functions
        function compile (tElement, tAttr) {
            return {
                pre: preLink,
                post: postLink
            };
        }
        function preLink (scope, element, attr) {
            if($App.isIOS()) {
                element.remove();
            }
        }
        function postLink (scope, element, attr) {
            scope.exitApp = $Mobile.exitApp;
        }
    }

    cpsModule.$inject = ["$controller", "CPS_MODULES"];
    function cpsModule ($controller, CPS_MODULES) {
        var cpsModuleDirective = {
            restrict: "A",
            compile: compile
        };
        return cpsModuleDirective;

        // Functions
        function compile (tElement, tAttrs) {
            var hasModule = false,
                module = tAttrs.cpsModule,
                cpsController = tAttrs.cpsController;

            if(CPS_MODULES.indexOf(module) > -1) {
                hasModule = true;
            } else {
                if(tAttrs.moduleFallback === "invisible") {
                    tElement.addClass("invisible");
                } else {
                    tElement.addClass("ng-hide");
                }
            }
            return {
                post: function postLink (scope, element, attrs) {
                    if(!hasModule && !attrs.moduleFallback) {
                        element.remove();
                    } else if(hasModule && cpsController) {

                        tAttrs.$set("ngController", cpsController);
                        tElement.removeAttr("cps-controller");

                        var controller = $controller(
                            cpsController, {
                                $scope: scope
                            }
                        );
                        element.children().data("$ngControllerController", controller);

                    }
                }
            };
        }


    }

    cpsOpenLink.$inject = ["$Mobile", "$Bind", "$Gestures"];
    function cpsOpenLink ($Mobile, $Bind, $Gestures) {
        var cpsOpenLinkDirective = {
            restrict: "A",
            scope: true,
            link: postLink
        };
        return cpsOpenLinkDirective;

        // Functions
        function postLink (scope, element, attrs) {
            $Bind(scope, attrs, {
                cpsOpen: "@"
            });

            $Gestures.hammer(element[0], "tap", function onTap (e) {
                e.preventDefault();
                $Mobile.open(scope.cpsOpen);
            });

            scope.$on("$destroy", function () {
                element.off("click");
            });
        }
    }

    /**
     * @ngdoc directive
     * @name cpsTimeAgo
     * @module Moment
     * TODO: Move to moment independent module
     * @restrict A
     */
    cpsTimeAgo.$inject = ["$window", "$App", "$Moment"];
    function cpsTimeAgo ($window, $App, $Moment) {
        var cpsTimeAgoDirective = {
            restrict: "A",
            link: postLink
        };
        return cpsTimeAgoDirective;

        // Functions
        function postLink (scope, element, attr) {
            var options = $Moment.options,
                activeTimeout = null,
                withoutSuffix = options.withoutSuffix,
                titleFormat = options.titleFormat,
                fullDateThreshold = options.fullDateThreshold,
                fullDateFormat = options.fullDateFormat,
                fullDateThresholdUnit = options.fullDateThresholdUnit,
                localDate = new Date().getTime(),
                modelName = attr.cpsTimeAgo,
                isTimeElement = (element[0].nodeName.toUpperCase() === "TIME"),
                setTitleTime = !element.attr("title"),
                currentFrom,
                currentValue;

            if(!ld.isNotdefined(attr.suffix)) {
                withoutSuffix = attr.suffix == "false";
            }
            // Events
            scope.$watch(modelName, function (value) {
                if (ld.isNotdefined(value) || (value === "")) {
                    cancelTimer();
                    if (currentValue) {
                        element.text("");
                        updateDateTimeAttr("");
                        currentValue = null;
                    }
                    return;
                }

                currentValue = value;
                updateMoment();
            });

            if (angular.isDefined(attr.cpsFrom)) {
                scope.$watch(attr.cpsFrom, function (value) {
                    if (ld.isNotdefined(value) || (value === "")) {
                        currentFrom = null;
                    } else {
                        currentFrom = $Moment(value);
                    }
                    updateMoment();
                });
            }

            if (angular.isDefined(attr.cpsWithoutSuffix)) {
                scope.$watch(attr.cpsWithoutSuffix, function (value) {
                    if (typeof value === "boolean") {
                        withoutSuffix = value;
                        updateMoment();
                    } else {
                        withoutSuffix = options.withoutSuffix;
                    }
                });
            }

            attr.$observe("cpsFullDateThreshold", function (newValue) {
                fullDateThreshold = newValue;
                updateMoment();
            });

            attr.$observe("cpsFullDateFormat", function (newValue) {
                fullDateFormat = newValue;
                updateMoment();
            });

            attr.$observe("cpsFullDateThresholdUnit", function (newValue) {
                fullDateThresholdUnit = newValue;
                updateMoment();
            });

            scope.$on("$destroy", function () {
                cancelTimer();
            });

            scope.$on("cps:localeChange", function () {
                updateMoment();
            });

            // Internal
            function getNow () {
                var now,
                    localNow,
                    nowMillis;
                if (currentFrom) {
                    now = currentFrom;
                } else if (options.serverTime) {
                    localNow = new Date().getTime();
                    nowMillis = localNow - localDate + options.serverTime;
                    now = $Moment(nowMillis);
                }
                else {
                    now = $Moment();
                }
                return now;
            }

            function cancelTimer () {
                if (activeTimeout) {
                    $window.clearTimeout(activeTimeout);
                    activeTimeout = null;
                }
            }

            function updateTime (momentInstance) {
                var timeAgo = getNow().diff(momentInstance, fullDateThresholdUnit),
                    showFullDate = fullDateThreshold && timeAgo >= fullDateThreshold;

                if (showFullDate) {
                    element.text(momentInstance.format(fullDateFormat));
                } else {
                    element.text(momentInstance.from(getNow(), withoutSuffix));
                }

                if (titleFormat && setTitleTime) {
                    element.attr("title", momentInstance.format(titleFormat));
                }

                if (!showFullDate) {
                    var howOld = Math.abs(getNow().diff(momentInstance, "minute")),
                        secondsUntilUpdate = 3600;
                    if (howOld < 1) {
                        secondsUntilUpdate = 1;
                    } else if (howOld < 60) {
                        secondsUntilUpdate = 30;
                    } else if (howOld < 180) {
                        secondsUntilUpdate = 300;
                    }

                    activeTimeout = $window.setTimeout(function () {
                        updateTime(momentInstance);
                    }, secondsUntilUpdate * 1000);
                }
            }

            function updateDateTimeAttr (value) {
                if (isTimeElement) {
                    element.attr("datetime", value);
                }
            }

            function updateMoment () {
                var momentValue;
                cancelTimer();
                if (currentValue) {
                    momentValue = $Moment.get(currentValue);
                    updateTime(momentValue);
                    updateDateTimeAttr(momentValue.toISOString());
                }
            }
        }
    }

    /**
     * @name cpsSelect
     * @restrict E
     *
     * @description 'cps-select' or 'select' Handling Select/options iOS Keyboards
     * show/hide keyboard accessory bar for ios
     */
    cpsSelect.$inject = ["$Keyboard"];
    function cpsSelect ($Keyboard) {
        var selectDirective = {
            restrict: "E",
            require: "?ngModel",
            compile: compile
        };
        return selectDirective;

        // Functions
        function compile (tElement, tAttrs) {
            if ($$(tElement.find("option")[0]).val() !== "") {
                tElement.prepend("<option class='ng-option-empty' value='' " +
                                 "locale-id='selectOption'>-- Select --</option>");
            }
            return {
                post: postLink
            };
        }
        function postLink (scope, element, attrs, ngModel) {
            var _isInt = false;
            // / Hide accessory bar on ios
            element.on("touchstart focus", function (e) {
                $Keyboard.hideAccessoryBar(false);
            });
            element.on("blur", function (e) {
                $Keyboard.hideAccessoryBar(true);
            });
            // Parse number
            if(ngModel && !attrs.ngOptions) {
                ngModel.$parsers.push(function (val) {
                    if(_isInt) {
                        return parseInt(val, 10);
                    }
                    return val;
                });
                ngModel.$formatters.push(function (val) {
                    if(ld.isFinite(val)) {
                        _isInt = true;
                        return "" + val;
                    }
                    return val;
                });
            }

            scope.$watch(function () {
                return $$(element).hasClass("ng-empty");
            }, function (newVal, oldVal) {
                if (newVal && ngModel) {
                    // Add empty option
                    registerUnknownOption(element);
                } else {
                    removeUnknownOption({target: element[0]});
                }
            });
            element.on("change", removeUnknownOption);
        }

        function registerUnknownOption (element) {
            var parent = element.parent();
            if (parent && parent.addClass) {
                parent.addClass("ng-option-unknown");
            }
        }

        function removeUnknownOption (event) {
            var target = event.target && $$(event.target),
                unknownOption = target && $$(target).find(".ng-option-empty"),
                parent = target && target.parent();

            if (unknownOption && $$("option:selected", target).val() !== unknownOption.val()) {
                // Clear DOM
                unknownOption.remove();
                if (target && target.off) {
                    if (parent && parent.removeClass) {
                        parent.removeClass("ng-option-unknown");
                    }
                    target.off("change", removeUnknownOption);
                }

            }
        }

    }

    /**
     * @name cpsKeyboardAttach
     * @restrict A
     *
     * @description cps-keyboard-attach is an attribute directive which will cause an element to float above
     * the keyboard when the keyboard shows. Currently only supports the 'cps-footer'
     */
    cpsKeyboardAttach.$inject = ["$window", "$Dom", "$App"];
    function cpsKeyboardAttach ($window, $Dom, $App) {
        var keyboardAttachDirective = {
            restrict: "A",
            link: postLink
        };
        return keyboardAttachDirective;

        // Functions
        function postLink (scope, element, attrs) {
            var scrollCtrl,
                keyboardHeight;
            $Dom.on("native.keyboardshow", onShow, $window);
            $Dom.on("native.keyboardhide", onHide, $window);

            function onShow (e) {
                if ($App.isAndroid() && !$App.isFullScreen()) {
                    return;
                }

                // for testing
                keyboardHeight = e.keyboardHeight || (e.detail && e.detail.keyboardHeight);
                element.css("bottom", keyboardHeight + "px");
                scrollCtrl = element.controller("ScrollController");
                if (scrollCtrl) {
                    scrollCtrl.scrollView.__container.style.bottom = keyboardHeight +
                        keyboardAttachGetClientHeight(element[0]) + "px";
                }
            }

            function onHide () {
                if ($App.isAndroid() && !$App.isFullScreen()) {
                    return;
                }

                element.css("bottom", "");
                if (scrollCtrl) {
                    scrollCtrl.scrollView.__container.style.bottom = "";
                }
            }

            scope.$on("$destroy", function () {
                $Dom.off("native.keyboardshow", onShow, $window);
                $Dom.off("native.keyboardhide", onHide, $window);
            });
        }

        function keyboardAttachGetClientHeight (element) {
            return element.clientHeight;
        }
    }

    /**
     * @name cpsKeyboardHandler
     * @restrict A
     *
     * @description cps-keyboard-handler is an attribute directive control keyboard actions
     * (show, close, hide)
     */
    cpsKeyboardHandler.$inject = ["$Keyboard", "$Gestures"];
    function cpsKeyboardHandler ($Keyboard, $Gestures) {
        var keyboardAttachDirective = {
            restrict: "A",
            scope: {
                actionHandler: "@cpsKeyboardHandler"
            },
            link: postLink
        };
        return keyboardAttachDirective;
        // functions
        function postLink (scope, element, attrs) {
            let action = {
                show: $Keyboard.show,
                close: $Keyboard.clode,
                hide: $Keyboard.hide
            };

            $Gestures.hammer(element[0], "tap", (e) => {
                e.preventDefault();
                (action[scope.actionHandler] || ld.noop)();
            });
        }
    }

    // Exports
    module.exports = utilsDirective;

})();
