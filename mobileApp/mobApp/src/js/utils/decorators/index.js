/* global require */

// utils/decorators/index.js
//
// custom/fix decorators for angular.js
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ngIOS9UIWebViewPatch = require("./angular-ios9.patch");

    // Exports
    module.exports = {
        ngIOS9UIWebViewPatch: ngIOS9UIWebViewPatch
    };
})();
