/* global require */

// utils/nombroFormat.js
//
// custom language and config for moment module
//
// 2015, CPS - Cellular Parking Systems
(function () {
    "use strict";

    var angular = require("angular"),
        moment = require("moment"),
        durationHumanize = require("humanize-duration"),
        ld = require("lodash");

    // Extra moment locales
    require("moment/locale/es");

    momentService.$inject = ["$rootScope", "$App", "$Logger", "$Locale"];
    function momentService ($rootScope, $App, $Logger, $Locale) {
        var config = $App.config,
            logger = $Logger.getInstance(),
            options = ld.extend({
                preprocess: null,
                timezone: null,
                format: null,
                statefulFilters: true,
                withoutSuffix: false,
                serverTime: null,
                titleFormat: null,
                fullDateThreshold: null,
                fullDateFormat: null,
                fullDateThresholdUnit: "day"
            }, config.time),
            defaultTimezone = options.timezone;

        // Listen events
        $rootScope.$on("cps:localeChange", function (event, lang) {
            changeLocale(lang);
        });

        // Members
        var Moment = moment; // set function from moment

        Moment.options = options;
        Moment.durationHumanize = durationHumanize;
        Moment.changeLocale = changeLocale;
        Moment.changeTimezone = changeTimezone;
        Moment.get = getMoment;

        window.Moment = Moment;
        return Moment;


        // Functions
        /**
         * @ngdoc function
         * @name changeLocale
         * @description
         * Changes the locale for moment.js and updates all the cps-time-ago directive instances
         * with the new locale. Also broadcasts an `cps:localeChanged` event on $rootScope.
         *
         * @param {string} locale Locale code (e.g. en, es, ru, pt-br, etc.)
         * @param {object} customization object of locale strings to override
         */
        function changeLocale (locale, customization) {
            var result = moment.locale(locale, customization),
                fallbackLocale = ld.isString(locale) ? locale.split("-")[0] : "en",
                hSupport = durationHumanize.getSupportedLanguages();

            if(hSupport.indexOf(locale) > -1) {
                durationHumanize.language = locale;
            } else {
                durationHumanize.language = hSupport.indexOf(fallbackLocale) > -1 ? fallbackLocale : "es";
            }

            if (angular.isDefined(locale)) {
                $rootScope.$broadcast("cps:momentChange", moment.locale());

            }
            return result;
        }

        /**
         * @ngdoc function
         * @name changeTimezone
         * @description
         * Changes the default timezone for cpsCalendar, cpsDateFormat and cpsTimeAgo. Also broadcasts an
         * `cps:timezoneChanged` event on $rootScope.
         *
         * Note: this method works only if moment-timezone > 0.3.0 is loaded
         *
         * @param {string} timezone Timezone name (e.g. UTC)
         */
        function changeTimezone (timezone) {
            if (moment.tz && moment.tz.setDefault) {
                moment.tz.setDefault(timezone);
                $rootScope.$broadcast("cps:timezoneChanged");
            } else {
                logger.warn("angular-moment: changeTimezone() works only with moment-timezone.js v0.3.0 or greater.");
            }
            defaultTimezone = timezone;
        }

        /**
         * @ngdoc function
         !n* @name getMoment
         * @description
         * Preprocess a given value and convert it into a Moment instance appropriate for use in the
         * cps-time-ago directive and the filters. The behavior of this function can be overriden by
         * setting `options.preprocess`.
         *
         * @param {*} value The value to be preprocessed
         * @return {Moment} A `moment` object
         */
        function getMoment (value) {
            // Configure the default timezone if needed
            if (defaultTimezone !== options.timezone) {
                changeTimezone(options.timezone);
            }

            if (options.preprocess) {
                return options.preprocess(value);
            }

            if (!isNaN(parseFloat(value)) && isFinite(value)) {
                // Milliseconds since the epoch
                return moment(parseInt(value, 10));
            }

            if(!ld.isDate(value)) {
                value = new Date(value);
            }
            // else just returns the value as-is.
            return moment(value);
        }

    }

    // Exports
    module.exports = momentService;

})();
