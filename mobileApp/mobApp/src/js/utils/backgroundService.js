
/* global module */

// utils/backgroundService.js
//
// enable/disable backgrounds mode for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");
    backgroundService.$inject = [
        "$q",
        "$window",
        "$rootScope",
        "$Logger"
    ];
    function backgroundService ($q, $window, $rootScope, $Logger) {
        var backgroundMode = ld.get($window.cordova, "plugins.backgroundMode"),
            logger = $Logger.getInstance();

        // Memebrs
        var Background = {
            enable: enable,
            disable: disable,
            isEnabled: isEnabled,
            isActive: isActive,
            getDefaults: getDefaults,
            setDefaults: setDefaults,
            configure: configure
        };

        // Events
        if(backgroundMode) {
            backgroundMode.onactivate = onactivate;
            backgroundMode.ondeactivate = ondeactivate;
            backgroundMode.onfailure = onfailure;
        }

        return Background;

        // Functions
        function enable () {
            var deferred = $q.defer();
            if(backgroundMode) {
                deferred.resolve(
                    backgroundMode.enable()
                );
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function disable () {
            var deferred = $q.defer();
            if(backgroundMode) {
                deferred.resolve(
                    backgroundMode.disable()
                );
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function isEnabled () {
            var deferred = $q.defer();
            if(backgroundMode) {
                deferred.resolve(
                    backgroundMode.isEnabled()
                );
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function isActive () {
            var deferred = $q.defer();
            if(backgroundMode) {
                deferred.resolve(
                    backgroundMode.isActive()
                );
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function getDefaults () {
            if(backgroundMode) {
                return backgroundMode.getDefaults();
            }
        }

        function setDefaults (Object) {
            if(backgroundMode) {
                backgroundMode.setDefaults(Object);
            }
        }

        function configure (Object) {
            if(backgroundMode) {
                backgroundMode.configure(Object);
            }
        }

        /**
         * @private
         */
        function onactivate () {
            var res = {active: isActive(), enabled: isEnabled()};

            logger.debug("background", "status activate");
            $rootScope.$emit("cps:background:activate", res);
        }
        function ondeactivate () {
            var res = {active: isActive(), enabled: isEnabled()};

            logger.debug("background", "status deactivate");
            $rootScope.$emit("cps:background:deactivate", res);
        }
        function onfailure (errorCode) {
            var err = {code: errorCode};
            logger.debug("background", "error: {0}", [JSON.stringify(err)]);
            $rootScope.$emit("cps:background:error", err);
        }
    }

    // Export
    module.exports = backgroundService;
})();
