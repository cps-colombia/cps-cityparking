/* global module */

// app/core/utils/utilsRun.js
//
// Run config for cps utils module
//
// 2015, CPS - Cellular Parking Systems
(function () {
    "use strict" ;

    var angular = require("angular"),
        $$ = angular.element;

    // Restricting route access
    utilsRun.$inject = [
        "$document",
        "$Mobile",
        "$Background"
    ];
    function utilsRun ($document, $Mobile, $Background) {
        // Listen post-compile href to open in external browser
        $$($document[0]).on("click", "[cps-href]", function (e) {
            e.preventDefault();
            $Mobile.open($$(e.target).attr("cps-href"), $$(e.target).attr("cps-href-options"));
        });

        // Backgound mode
        $Background.setDefaults({
            silent: true
        });
        $document[0].addEventListener("resign", $Background.enable, false); // iOS
        $document[0].addEventListener("active", $Background.disable, false); // iOS
        $document[0].addEventListener("pause", $Background.enable, false);
        $document[0].addEventListener("resume", $Background.disable, false);

    }

    // Exports
    module.exports = utilsRun;
})();
