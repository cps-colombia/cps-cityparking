/* global module */

// utils/requestService.js
//
// core logic for http requests for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    requestService.$inject = [
        "$q",
        "$rootScope",
        "$timeout",
        "$http",
        "$Mobile",
        "$Logger",
        "$App",
        "$Utils",
        "$Network",
        "AUTH_EVENTS"
    ];
    function requestService ($q, $rootScope, $timeout, $http, $Mobile, $Logger, $App, $Utils, $Network, AUTH_EVENTS) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance(),
            config = $App.config,
            currentRequest = null,
            timeoutHttp;

        // Members
        var Request = {
            http: http,
            hasErrorRequest: hasErrorRequest
        };
        return Request;

        // Functions
        /**
         * http request facilitates communication with
         * the remote HTTP servers via the browser's
         * XMLHttpRequest object or via JSONP
         * @param req like url, method, params
         * see angular $http docs
         * @return promise
         */
        function http (extendReq) {
            var deferred = $q.defer(),
                timeout = $q.defer(),
                isTimedOut = false,
                statusCode,
                errSend,
                req = {
                    withCredentials: false,
                    background: false,
                    url: config.cpsMob,
                    method: "POST",
                    segTimeout: 40,
                    timeout: timeout.promise
                };

            // Send request when app is ready
            $App.ready(function () {
                // Set background options
                if(extendReq.params) {
                    if(ld.has(extendReq, "params.background")) {
                        req.background = extendReq.params.background;
                        delete extendReq.params.background;
                    }
                }

                // Extend defaults
                angular.extend(req, extendReq);

                // Set custom header for cps MOB
                if(req.url === config.cpsMob && req.method === "POST") {
                    req.headers = angular.extend(angular.copy($http.defaults.headers.post), {
                        "x-validation": $App.getValidation()
                    });
                }
                // TODO: force all request porst send data in body data not as params
                // if(req.method == "POST") {
                //     if(req.params) {
                //         req.data = req.params;
                //         delete req.params;
                //     }
                // }

                // Set TimeOut
                timeoutHttp = $timeout(function () {
                    isTimedOut = true;
                    timeout.resolve();
                }, (1000 * req.segTimeout));

                // Prevent similar Request
                if(currentRequest && ld.isEqual(currentRequest, req)) {
                    deferred.reject({status: 409, statusText: "Active similar request"});
                } else {
                    currentRequest = req;
                }

                // Request
                $http(req).then(function (res) {
                    httpComplete(timeoutHttp);
                    if(ld.has(res, "data.statusCode")) {
                        res.data.status_code = res.data.statusCode;
                    }
                    statusCode = ld.get(res, "data.status_code") || res.status;
                    if(!ld.isFinite(statusCode)) {
                        statusCode = parseInt(statusCode);
                    }
                    errSend = {
                        req: req,
                        res: res,
                        statusCode: statusCode
                    };

                    if(hasErrorRequest(errSend)) {
                        deferred.reject(res);
                    } else {
                        deferred.resolve(res);
                    }

                }).catch(function (err) {
                    httpComplete(timeoutHttp);
                    if(ld.has(err, "data.statusCode")) {
                        err.data.status_code = err.data.statusCode;
                    }
                    statusCode = ld.get(err, "data.status_code") || err.status;

                    if(!ld.isFinite(statusCode)) {
                        statusCode = parseInt(statusCode) || "Empty";
                    }
                    errSend = {
                        req: req,
                        res: err,
                        statusCode: statusCode
                    };
                    $Mobile.loading("hide");

                    if (isTimedOut) {
                        if(!req.background) {
                            $Mobile.alert(_("alertTimeout"));
                        }
                        err = {
                            status: 408,
                            statusText: "Error Timeout after " + req.segTimeout + " seconds."
                        };
                    } else if(hasErrorRequest(errSend)) {
                        $Mobile.ga("send", "exception", "Request catch: " + err.status + "-" + err.statusText, true);
                    }

                    logger.warn(
                        "request",
                        "Error $http with status: {0}, statusText: {1}, statusCode: {2}, data: {3}, config: {4}", [
                            err.status, err.statusText,
                            statusCode, ld.truncate(JSON.stringify((err.data || {})), 200),
                            JSON.stringify(ld.pick(err.config, [
                                "url", "method", "background",
                                "params", "segTimeout", "withCredentials"
                            ]))
                        ]
                    );

                    deferred.reject(err);
                });
            });

            return deferred.promise;
        }

        function hasErrorRequest (data) {
            var req = data.req || {},
                res = data.res || {},
                statusCode = data.statusCode,
                error = false;

            if(!statusCode) {
                statusCode = data.statusCode = res.status || 500;
            }
            if(!ld.isFinite(statusCode)) {
                statusCode = parseInt(statusCode);
            }

            $Mobile.loading("hide");
            if(!res.data) {
                error = true;
                if($Network.hasInternet()) {
                    $Utils.internalError(data);
                } else if(!req.background) {
                    $Mobile.alert(_("alertNetworkConnection"));
                }
            } else if((statusCode >=  400 && statusCode !== 408 && statusCode !== 409) || statusCode === 50) {
                error = true;
                $Utils.internalError(data);
            } else if(statusCode === 51 || statusCode === 52) {
                error = true;
                $Utils.invalidTokes(data);
            } else if (statusCode === 103) {
                error = true;
                $Utils.userBlocked(data);
            }
            return error;
        }

        // Private
        function httpComplete (timeoutHttp) {
            $timeout.cancel(timeoutHttp);
            currentRequest = null;
        }
    }

    // exports
    module.exports = requestService;

})();
