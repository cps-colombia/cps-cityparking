/* global module */

// utils/keyboradService.js
//
// keyborad factory service for cps mobile keyborad
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    keyboardService.$inject = [
        "$window",
        "$document",
        "$timeout",
        "$rootScope",
        "$App",
        "$Dom",
        "$Element"
    ];
    function keyboardService ($window, $document, $timeout, $rootScope, $App, $Dom, $Element) {
        var keyboardPlugin = keyboardGetPlugin(),
            keyboardCurrentViewportHeight = getViewportHeight(),
            keyboardPortraitViewportHeight = 0,
            keyboardLandscapeViewportHeight = 0,
            wasOrientationChange = false,
            debouncedKeyboardFocusIn = ld.setDebounce(keyboardFocusIn, 200, true),
            debouncedKeyboardNativeShow = ld.setDebounce(keyboardNativeShow, 100, true),
            SCROLL_CONTAINER_CSS = "scroll-content",
            KEYBOARD_OPEN_CSS = "keyboard-open",
            _isOpen = false,
            _isClosing = false,
            _isOpening = false,
            _height = 0,
            _isLandscape = false,
            _isInitialized = false,
            lastKeyboardActiveElement,
            keyboardActiveElement,
            waitForResizeTimer,
            keyboardFocusOutTimer,
            scrollView;

        // Members
        var Keyboard = {
            isOpen: isOpen,
            isClosing: isClosing,
            isOpening: isOpening,
            height: height,
            isLandscape: isLandscape,
            isInitialized: isInitialized,
            load: load,
            show: show,
            close: close,
            hide: hide,
            enable: enable,
            disable: disable,
            hideAccessoryBar: hideAccessoryBar,
            disableScroll: disableScroll,
            isVisible: isVisible,
            clearShowWatch: clearShowWatch,
            clearHideWatch: clearHideWatch
        };

        return Keyboard;

        // Funtions

        /**
         * @name isOpen
         * @description Whether the keyboard is open or not.
         */
        function isOpen () {
            return _isOpen;
        }

        /**
         * @name isClosing
         * @description Whether the keyboard is closing or not.
         */
        function isClosing () {
            return _isClosing;
        }

        /**
         * @name isOpening
         * @description Whether the keyboard is opening or not.
         */
        function isOpening () {
            return _isOpening;
        }

        /**
         * @name height
         * @description The height of the keyboard in pixels, as reported by the keyboard plugin.
         * If the plugin is not available, calculated as the difference in
         * window.innerHeight after the keyboard has shown.
         */
        function height () {
            return _height;
        }

        /**
         * @name isLandscape
         * @description Whether the device is in landscape orientation or not.
         */
        function isLandscape () {
            return _isLandscape;
        }

        /**
         * @name isInitialized
         * @description Whether the keyboard event listeners have been added or not
         */
        function isInitialized () {
            return _isInitialized;
        }

        function load () {
            $App.ready(function () {
                keyboardInitViewportHeight();

                $window.addEventListener("orientationchange", keyboardOrientationChange);

                // Default event listener with scope event
                $window.addEventListener("native.keyboardshow", keyboardShowEvent, false);
                $window.addEventListener("native.keyboardhide", keyboardHideEvent, false);

                // Default
                hideAccessoryBar(true);
                disableScroll(true);

                // if orientation changes while app is in background, update on resuming
                /*
                  if ( $App.isWebView() ) {
                  $document[0].addEventListener('resume', keyboardInitViewportHeight);
                  if ($App.isAndroid()) {
                  //TODO: onbackpressed to detect keyboard close without focusout or plugin
                  }
                  }
                */

                // if orientation changes while app is in background, update on resuming
                /*  if ( $App.isWebView() ) {
                    $document[0].addEventListener('pause', function() {
                    $window.removeEventListener('orientationchange', keyboardOrientationChange);
                    })
                    $document[0].addEventListener('resume', function() {
                    keyboardInitViewportHeight();
                    $window.addEventListener('orientationchange', keyboardOrientationChange)
                    });
                    }*/

                // Android sometimes reports bad innerHeight on $window.load
                // try it again in a lil bit to play it safe
                $timeout(keyboardInitViewportHeight, 999);

                // only initialize the adjustments for the virtual keyboard
                // if a touchstart event happens
                if ($window.navigator.msPointerEnabled) {
                    $document[0].addEventListener("MSPointerDown", keyboardInit, false);
                } else {
                    $document[0].addEventListener("touchstart", keyboardInit, false);
                }
            });
        }
        /**
         * @name show
         * @description An alias for cordova.plugins.Keyboard.show(). If the keyboard plugin
         * is installed, show the keyboard.
         */
        function show () {
            return keyboardPlugin && keyboardPlugin.show &&
                keyboardPlugin.show();
        }

        /**
         * @name close
         * @description close the keyboard, if it is open but withour blur.
         */
        function close () {
            return keyboardPlugin && keyboardPlugin.close &&
                keyboardPlugin.close();
        }

        /**
         * @name hide
         * @description Hide the keyboard, if it is open and blur input.
         */
        function hide () {
            close();
            if(keyboardActiveElement) {
                keyboardActiveElement.blur();
            }
        }

        /**
         * @name enable
         * @description Alias for keyboardInit, initialize all keyboard related event listeners.
         */
        function enable () {
            keyboardInit();
        }

        /**
         * @name disable
         * @description Remove all keyboard related event listeners, effectively disabling
         * keyboard adjustments.
         */
        function disable () {
            if (keyboardHasPlugin()) {
                $window.removeEventListener("native.keyboardshow", debouncedKeyboardNativeShow);
                $window.removeEventListener("native.keyboardhide", keyboardFocusOut);
            } else {
                $document[0].body.removeEventListener("focusout", keyboardFocusOut);
            }

            $document[0].body.removeEventListener("cps:focusin", debouncedKeyboardFocusIn);
            $document[0].body.removeEventListener("focusin", debouncedKeyboardFocusIn);

            $window.removeEventListener("orientationchange", keyboardOrientationChange);

            if ($window.navigator.msPointerEnabled) {
                $document[0].removeEventListener("MSPointerDown", keyboardInit);
            } else {
                $document[0].removeEventListener("touchstart", keyboardInit);
            }
            _isInitialized = false;
        }

        /**
         * @name hideAccessoryBar
         * @description Hide the keyboard accessory bar with the next, previous and done buttons (IOS)
         */
        function hideAccessoryBar (bool) {
            return keyboardPlugin && keyboardPlugin.hideKeyboardAccessoryBar &&
                keyboardPlugin.hideKeyboardAccessoryBar(bool);
        }

        /**
         * @name disableScroll
         * @description Disable native scrolling, useful if you are using JavaScript to scroll
         */
        function disableScroll (bool) {
            return keyboardPlugin && keyboardPlugin.disableScroll &&
                keyboardPlugin.disableScroll(bool);
        }

        /**
         * @name isVisible
         * @description return if a native keyboard is visible or not
         */
        function isVisible () {
            return keyboardPlugin && keyboardPlugin.isVisible;
        }

        /**
         * @name clearShowWatch
         * @description clean listener with scope event when keyboard show
         */
        function clearShowWatch () {
            $document[0].removeEventListener("native.keyboardshow", keyboardShowEvent);
            $rootScope.$$listeners["cps:keyboard:show"] = [];
        }

        /**
         * @name clearHideWatch
         * @description clean listener with scope event when keyboard hide
         */
        function clearHideWatch () {
            $document[0].removeEventListener("native.keyboardhide", keyboardHideEvent);
            $rootScope.$$listeners["cps:keyboard:hide"] = [];
        }

        // Private
        /**
         * Broadcast Events when keyboard hide/show
         */
        function keyboardShowEvent () {
            $rootScope.$evalAsync(function () {
                $rootScope.$broadcast("cps:keyboard:show");
            });
        }
        function keyboardHideEvent () {
            $rootScope.$evalAsync(function () {
                $rootScope.$broadcast("cps:keyboard:hide");
            });
        }

        /* Event handlers */
        /* ------------------------------------------------------------------------- */

        /**
         * Event handler for first touch event, initializes all event listeners
         * for keyboard related events. Also aliased by Keyboard.enable.
         */
        function keyboardInit () {
            if (_isInitialized) {
                return;
            }

            if (keyboardHasPlugin()) {
                $window.addEventListener("native.keyboardshow", debouncedKeyboardNativeShow);
                $window.addEventListener("native.keyboardhide", keyboardFocusOut);
            } else {
                $document[0].body.addEventListener("focusout", keyboardFocusOut);
            }

            $document[0].body.addEventListener("cps:focusin", debouncedKeyboardFocusIn);
            $document[0].body.addEventListener("focusin", debouncedKeyboardFocusIn);

            if ($window.navigator.msPointerEnabled) {
                $document[0].removeEventListener("MSPointerDown", keyboardInit);
            } else {
                $document[0].removeEventListener("touchstart", keyboardInit);
            }

            _isInitialized = true;
        }

        /**
         * Event handler for 'native.keyboardshow' event, sets _height to the
         * reported height and _isOpening to true. Then calls
         * keyboardWaitForResize with keyboardShow or keyboardUpdateViewportHeight as
         * the callback depending on whether the event was triggered by a focusin or
         * an orientationchange.
         */
        function keyboardNativeShow (e) {
            $timeout.cancel(keyboardFocusOutTimer);
            // console.log("keyboardNativeShow fired at: " + Date.now());
            // console.log("keyboardNativeshow window.innerHeight: " + window.innerHeight);

            if (!_isOpen || _isClosing) {
                _isOpening = true;
                _isClosing = false;
            }

            _height = e.keyboardHeight;
            // console.log('nativeshow _height:' + e.keyboardHeight);

            if (wasOrientationChange) {
                keyboardWaitForResize(keyboardUpdateViewportHeight, true);
            } else {
                keyboardWaitForResize(keyboardShow, true);
            }
        }

        /**
         * Event handler for 'focusin' and 'cps:focusin' events. Initializes
         * keyboard state (keyboardActiveElement and _isOpening) for the
         * appropriate adjustments once the window has resized.  If not using the
         * keyboard plugin, calls keyboardWaitForResize with keyboardShow as the
         * callback or keyboardShow right away if the keyboard is already open.  If
         * using the keyboard plugin does nothing and lets keyboardNativeShow handle
         * adjustments with a more accurate _height.
         */
        function keyboardFocusIn (e) {
            $timeout.cancel(keyboardFocusOutTimer);
            // console.log("keyboardFocusIn from: " + e.type + " at: " + Date.now());

            if (!e.target ||
                e.target.readOnly ||
                !$Element.isKeyboardElement(e.target) ||
                !(scrollView = $Dom.getParentWithClass(e.target, SCROLL_CONTAINER_CSS))) {
                if (keyboardActiveElement) {
                    lastKeyboardActiveElement = keyboardActiveElement;
                }
                keyboardActiveElement = null;
                return;
            }

            keyboardActiveElement = e.target;

            // if using JS scrolling, undo the effects of native overflow scroll so the
            // scroll view is positioned correctly
            if (!scrollView.classList.contains("overflow-scroll")) {
                $document[0].body.scrollTop = 0;
                scrollView.scrollTop = 0;
                $Dom.requestAnimationFrame(function () {
                    $document[0].body.scrollTop = 0;
                    scrollView.scrollTop = 0;
                });

                // any showing part of the document that isn't within the scroll the user
                // could touchmove and cause some ugly changes to the app, so disable
                // any touchmove events while the keyboard is open using e.preventDefault()
                if ($window.navigator.msPointerEnabled) {
                    $document[0].addEventListener("MSPointerMove", keyboardPreventDefault, false);
                } else {
                    $document[0].addEventListener("touchmove", keyboardPreventDefault, false);
                }
            }

            if (!_isOpen || _isClosing) {
                _isOpening = true;
                _isClosing = false;
            }

            // attempt to prevent browser from natively scrolling input into view while
            // we are trying to do the same (while we are scrolling) if the user taps the
            // keyboard
            $document[0].addEventListener("keydown", keyboardOnKeyDown, false);

            // if we aren't using the plugin and the keyboard isn't open yet, wait for the
            // window to resize so we can get an accurate estimate of the keyboard size,
            // otherwise we do nothing and let nativeShow call keyboardShow once we have
            // an exact _height
            // if the keyboard is already open, go ahead and scroll the input into view
            // if necessary
            if (!_isOpen && !keyboardHasPlugin()) {
                keyboardWaitForResize(keyboardShow, true);

            } else if (_isOpen) {
                keyboardShow();
            }
        }

        /**
         * Event handler for 'focusout' events. Sets _isClosing to true and
         * calls keyboardWaitForResize with keyboardHide as the callback after a small
         * timeout.
         */
        function keyboardFocusOut () {
            $timeout.cancel(keyboardFocusOutTimer);
            // console.log("keyboardFocusOut fired at: " + Date.now());
            // console.log("keyboardFocusOut event type: " + e.type);

            if (_isOpen || _isOpening) {
                _isClosing = true;
                _isOpening = false;
            }

            // Call keyboardHide with a slight delay because sometimes on focus or
            // orientation change focusin is called immediately after, so we give it time
            // to cancel keyboardHide
            keyboardFocusOutTimer = $timeout(function () {
                $Dom.requestAnimationFrame(function () {
                    // focusOut during or right after an orientationchange, so we didn't get
                    // a chance to update the viewport height yet, do it and keyboardHide
                    // console.log("focusOut, wasOrientationChange: " + wasOrientationChange);
                    if (wasOrientationChange) {
                        keyboardWaitForResize(function () {
                            keyboardUpdateViewportHeight();
                            keyboardHide();
                        }, false);
                    } else {
                        keyboardWaitForResize(keyboardHide, false);
                    }
                });
            }, 50);
        }

        /**
         * Event handler for 'orientationchange' events. If using the keyboard plugin
         * and the keyboard is open on Android, sets wasOrientationChange to true so
         * nativeShow can update the viewport height with an accurate _height.
         * If the keyboard isn't open or keyboard plugin isn't being used,
         * waits for the window to resize before updating the viewport height.
         *
         * On iOS, where orientationchange fires after the keyboard has already shown,
         * updates the viewport immediately, regardless of if the keyboard is already
         * open.
         */
        function keyboardOrientationChange () {
            // console.log("orientationchange fired at: " + Date.now());
            // console.log("orientation was: " + (_isLandscape ? "landscape" : "portrait"));

            // toggle orientation
            _isLandscape = !_isLandscape;
            // //console.log("now orientation is: " + (_isLandscape ? "landscape" : "portrait"));

            // no need to wait for resizing on iOS, and orientationchange always fires
            // after the keyboard has opened, so it doesn't matter if it's open or not
            if ($App.isIOS()) {
                keyboardUpdateViewportHeight();
            }

            // On Android, if the keyboard isn't open or we aren't using the keyboard
            // plugin, update the viewport height once everything has resized. If the
            // keyboard is open and we are using the keyboard plugin do nothing and let
            // nativeShow handle it using an accurate _height.
            if ($App.isAndroid()) {
                if (!_isOpen || !keyboardHasPlugin()) {
                    keyboardWaitForResize(keyboardUpdateViewportHeight, false);
                } else {
                    wasOrientationChange = true;
                }
            }
        }

        /**
         * Event handler for 'keydown' event. Tries to prevent browser from natively
         * scrolling an input into view when a user taps the keyboard while we are
         * scrolling the input into view ourselves with JS.
         */
        function keyboardOnKeyDown (e) {
            if ($rootScope.scroll.isScrolling) {
                keyboardPreventDefault(e);
            }
        }

        /**
         * Event for 'touchmove' or 'MSPointerMove'. Prevents native scrolling on
         * elements outside the scroll view while the keyboard is open.
         */
        function keyboardPreventDefault (e) {
            if (e.target.tagName !== "TEXTAREA") {
                e.preventDefault();
            }
        }

        /* Private API */
        /* -------------------------------------------------------------------------- */

        /**
         * Polls window.innerHeight until it has updated to an expected value (or
         * sufficient time has passed) before calling the specified callback function.
         * Only necessary for non-fullscreen Android which sometimes reports multiple
         * window.innerHeight values during interim layouts while it is resizing.
         *
         * On iOS, the window.innerHeight will already be updated, but we use the 50ms
         * delay as essentially a timeout so that scroll view adjustments happen after
         * the keyboard has shown so there isn't a white flash from us resizing too
         * quickly.
         *
         * @param {Function} callback the function to call once the window has resized
         * @param {boolean} isOpening whether the resize is from the keyboard opening
         * or not
         */
        function keyboardWaitForResize (callback, isOpening) {
            clearInterval(waitForResizeTimer);
            var count = 0,
                initialHeight = getViewportHeight(),
                viewportHeight = initialHeight,
                maxCount;

            // console.log("waitForResize initial viewport height: " + viewportHeight);
            // var start = Date.now();
            // console.log("start: " + start);

            // want to fail relatively quickly on modern android devices, since it's much
            // more likely we just have a bad _height
            if ($App.isAndroid() && $App.getPlatform().version < 4.4) {
                maxCount = 30;
            } else if ($App.isAndroid()) {
                maxCount = 10;
            } else {
                maxCount = 1;
            }

            // poll timer
            waitForResizeTimer = setInterval(function () {
                viewportHeight = getViewportHeight();

                // height hasn't updated yet, try again in 50ms
                // if not using plugin, wait for maxCount to ensure we have waited long enough
                // to get an accurate _height
                if (++count < maxCount &&
                    ((!isPortraitViewportHeight(viewportHeight) &&
                      !isLandscapeViewportHeight(viewportHeight)) ||
                     !_height)) {
                    return;
                }

                // infer the _height from the resize if not using the keyboard plugin
                if (!keyboardHasPlugin()) {
                    _height = Math.abs(initialHeight - $window.innerHeight);
                }

                // set to true if we were waiting for the keyboard to open
                _isOpen = isOpening;

                clearInterval(waitForResizeTimer);
                // var end = Date.now();
                // console.log("waitForResize count: " + count);
                // console.log("end: " + end);
                // console.log("difference: " + ( end - start ) + "ms");

                // console.log("callback: " + callback.name);
                callback();

            }, 50);

            return maxCount; // for tests
        }

        /**
         * On keyboard close sets keyboard state to closed, resets the scroll view,
         * removes CSS from body indicating keyboard was open, removes any event
         * listeners for when the keyboard is open and on Android blurs the active
         * element (which in some cases will still have focus even if the keyboard
         * is closed and can cause it to reappear on subsequent taps).
         */
        function keyboardHide () {
            $timeout.cancel(keyboardFocusOutTimer);
            // console.log("keyboardHide");

            _isOpen = false;
            _isClosing = false;

            if (keyboardActiveElement || lastKeyboardActiveElement) {
                $Dom.trigger("resetScrollView", {
                    target: keyboardActiveElement || lastKeyboardActiveElement
                }, true);
            }

            $Dom.requestAnimationFrame(function () {
                $document[0].body.classList.remove(KEYBOARD_OPEN_CSS);
            });

            // the keyboard is gone now, remove the touchmove that disables native scroll
            if ($window.navigator.msPointerEnabled) {
                $document[0].removeEventListener("MSPointerMove", keyboardPreventDefault);
            } else {
                $document[0].removeEventListener("touchmove", keyboardPreventDefault);
            }
            $document[0].removeEventListener("keydown", keyboardOnKeyDown);

            if ($App.isAndroid()) {
                // on android closing the keyboard with the back/dismiss button won't remove
                // focus and keyboard can re-appear on subsequent taps (like scrolling)
                if (keyboardHasPlugin()) {
                    keyboardPlugin.close();
                }
                if(keyboardActiveElement) {
                    keyboardActiveElement.blur();
                }
            }

            keyboardActiveElement = null;
            lastKeyboardActiveElement = null;
        }

        /**
         * On keyboard open sets keyboard state to open, adds CSS to the body
         * indicating the keyboard is open and tells the scroll view to resize and
         * the currently focused input into view if necessary.
         */
        function keyboardShow () {
            var details = {
                keyboardHeight: keyboardGetHeight(),
                viewportHeight: keyboardCurrentViewportHeight
            };
            var elementBounds;

            _isOpen = true;
            _isOpening = false;

            if (keyboardActiveElement) {
                details.target = keyboardActiveElement;

                elementBounds = keyboardActiveElement.getBoundingClientRect();

                details.elementTop = Math.round(elementBounds.top);
                details.elementBottom = Math.round(elementBounds.bottom);

                details.windowHeight = details.viewportHeight - details.keyboardHeight;
                // console.log("keyboardShow viewportHeight: " + details.viewportHeight +
                // ", windowHeight: " + details.windowHeight +
                // ", keyboardHeight: " + details.keyboardHeight);

                // figure out if the element is under the keyboard
                details.isElementUnderKeyboard = (details.elementBottom > details.windowHeight);
                // console.log("isUnderKeyboard: " + details.isElementUnderKeyboard);
                // console.log("elementBottom: " + details.elementBottom);

                // send event so the scroll view adjusts
                $Dom.trigger("scrollChildIntoView", details, true);
            }

            $timeout(function () {
                $document[0].body.classList.add(KEYBOARD_OPEN_CSS);
            }, 400);

            return details; // for testing
        }

        /* eslint no-unused-vars:0 */
        function keyboardGetHeight () {
            // check if we already have a _height from the plugin or resize calculations
            if (_height) {
                return _height;
            }

            if ($App.isAndroid()) {
                // should be using the plugin, no way to know how big the keyboard is, so guess
                if ($App.isFullScreen) {
                    return 275;
                }
                // otherwise just calculate it
                var contentHeight = $window.innerHeight;
                if (contentHeight < keyboardCurrentViewportHeight) {
                    return keyboardCurrentViewportHeight - contentHeight;
                } else {
                    return 0;
                }
            }

            // fallback for when it's the webview without the plugin
            // or for just the standard web browser
            // TODO: have these be based on device
            if ($App.isIOS()) {
                if (_isLandscape) {
                    return 206;
                }

                if (!$App.isWebView()) {
                    return 216;
                }

                return 260;
            }

            // safe guess
            return 275;
        }

        function isPortraitViewportHeight (viewportHeight) {
            return !!(!_isLandscape &&
                      keyboardPortraitViewportHeight &&
                      (Math.abs(keyboardPortraitViewportHeight - viewportHeight) < 2));
        }

        function isLandscapeViewportHeight (viewportHeight) {
            return !!(_isLandscape &&
                      keyboardLandscapeViewportHeight &&
                      (Math.abs(keyboardLandscapeViewportHeight - viewportHeight) < 2));
        }

        function keyboardUpdateViewportHeight () {
            wasOrientationChange = false;
            keyboardCurrentViewportHeight = getViewportHeight();

            if (_isLandscape && !keyboardLandscapeViewportHeight) {
                // console.log("saved landscape: " + keyboardCurrentViewportHeight);
                keyboardLandscapeViewportHeight = keyboardCurrentViewportHeight;

            } else if (!_isLandscape && !keyboardPortraitViewportHeight) {
                // console.log("saved portrait: " + keyboardCurrentViewportHeight);
                keyboardPortraitViewportHeight = keyboardCurrentViewportHeight;
            }

            if (keyboardActiveElement) {
                $Dom.trigger("resetScrollView", {
                    target: keyboardActiveElement
                }, true);
            }

            if (_isOpen && $Element.isTextInput(keyboardActiveElement)) {
                keyboardShow();
            }
        }

        function keyboardInitViewportHeight () {
            var viewportHeight = getViewportHeight();
            // console.log("Keyboard init VP: " + viewportHeight + " " + $window.innerWidth);
            // can't just use $window.innerHeight in case the keyboard is opened immediately
            if ((viewportHeight / $window.innerWidth) < 1) {
                _isLandscape = true;
            }
            // console.log("_isLandscape is: " + _isLandscape);

            // initialize or update the current viewport height values
            keyboardCurrentViewportHeight = viewportHeight;
            if (_isLandscape && !keyboardLandscapeViewportHeight) {
                keyboardLandscapeViewportHeight = keyboardCurrentViewportHeight;
            } else if (!_isLandscape && !keyboardPortraitViewportHeight) {
                keyboardPortraitViewportHeight = keyboardCurrentViewportHeight;
            }
        }

        function getViewportHeight () {
            var windowHeight = $window.innerHeight;
            // console.log('$window.innerHeight is: ' + windowHeight);
            // console.log('kb height is: ' + _height);
            // console.log('kb isOpen: ' + _isOpen);

            // TODO: add iPad undocked/split kb once kb plugin supports it
            // the keyboard overlays the window on Android fullscreen
            if (!($App.isAndroid() && $App.isFullScreen) &&
                (_isOpen || _isOpening) &&
                !_isClosing) {

                return windowHeight + keyboardGetHeight();
            }
            return windowHeight;
        }

        function keyboardGetPlugin () {
            return $window.cordova && $window.cordova.plugins && $window.cordova.plugins.Keyboard;
        }

        function keyboardHasPlugin () {
            return !!(keyboardGetPlugin());
        }
    }

    // Exports
    module.exports = keyboardService;
})();
