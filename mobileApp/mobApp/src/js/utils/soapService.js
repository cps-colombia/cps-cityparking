/* global module */

// utils/soapService.js
//
// core logic for http soaps for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var soap = require("soapclient"),
        ld = require("lodash");

    soapService.$inject = ["$q", "$App", "$Logger", "$Mobile", "$Response", "$Request"];
    function soapService ($q,  $App, $Logger, $Mobile, $Response, $Request) {
        var logger = $Logger.getInstance();

        // Members
        var Soap = {
            post: post,
            setCredentials: setCredentials
        };
        return Soap;

        // Functions
        function post (url, action, params) {
            var deferred = $q.defer(),
                soapParams = new soap.SOAPClientParameters(), // Create SOAPClientParameters
                success = false,
                resData = {
                    config: {
                        url: url,
                        action: action,
                        params: params,
                        method: "POST"
                    }
                },
                param,
                err;

            for(param in params) {
                if(params.hasOwnProperty(param)) {
                    soapParams.add(param, params[param]);
                }
            }

            // Create Callback
            var soapCallback = function (res, req) {
                if(res.status) {
                    res.status = parseInt(res.status);
                }
                resData = ld.extend({
                    data: $Response.make(res),
                    res: res,
                    req: req
                }, resData);
                resData.statusCode = resData.data && parseInt(resData.data.statusCode) ||
                    (res.status || "Empty");

                if(!res || res.constructor.toString().indexOf("function Error()") !== -1) {
                    resData.data = $Response.make([500, "An error has occurred."]);
                }

                logger.debug("soap", JSON.stringify(resData));

                if($Request.hasErrorRequest(resData)) {
                    err = resData;
                } else {
                    success = true;
                    deferred.resolve(resData);
                }

                if(!success) {
                    $Mobile.loading("hide");

                    logger.warn("soap", JSON.stringify(err.data));
                    deferred.reject(err);
                }
            };

            soap.SOAPClient.invoke(url, action, soapParams, true, soapCallback);

            return deferred.promise;
        }

        function setCredentials (username, password) {
            soap.SOAPClient.username = username;
            soap.SOAPClient.password = password;
        }
    }

    // Export
    module.exports = soapService;

})();
