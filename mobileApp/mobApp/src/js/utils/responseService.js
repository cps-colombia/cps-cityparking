/* global module */

// utils/reponseService.js
//
// response creation with cps standar service module for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    responseService.$inject = ["$Logger"];
    function responseService ($Logger) {
        var logger = $Logger.getInstance(),
            objResponse = {
                statusCode: 200,
                description: "Success",
                data: {}
            },
            RESPONSE = {
                200: "Success",
                400: "Bad request",
                401: "Unauthorized",
                498: "Token expired/invalid",
                429: "Too Many Requests",
                500: "Internal error"
            };

        // Members
        var Response = {
            make: makeResponse
        };
        return Response;

        // Functions
        /**
         * makeResponse
         *
         * Create response standar object for cps
         * depending on what value you send, object, array or string
         *
         * @param {Object/Array/String} values print in response
         * @return {Object} response standar for cps
         */
        function makeResponse (values) {
            // verifies that kind of response building
            if(ld.isString(values)) {
                objResponse.description = values;
            } else if(ld.isNumber(values) || isFinite(values)) {
                objResponse.statusCode = parseInt(values);
                objResponse.description = RESPONSE[values] || "";
            } else if(ld.isObject(values) && values.length === undefined) {
                // If cps mob objResponse
                if(values.statusCode) {
                    ld.extend(objResponse, values);
                } else if(values.code) {
                    objResponse.statusCode = values.code.length > 3 ? 500 : values.code;
                    objResponse.description = values.code ? values.code : "Internal Error";

                } else if(values.status || values.statusText) {
                    objResponse.data = values.data || {};
                    objResponse.statusCode = values.status || 500;
                    objResponse.description = values.statusText || "Internal Error";
                } else {
                    objResponse.data = values;
                }
            } else if(ld.isArray(values)) {
                if(values.length > 3) {
                    // if arrya is invalid
                    logger.warn("Reponse need array with max 3 values, code, description and obj.");
                } else {
                    // checks to put parts of array
                    ld.forEach(values, function (value, key) {
                        if(ld.isObject(value) || ld.isArray(value)) {
                            objResponse.data = value;
                        } else {
                            if(ld.isNumber(value) || isFinite(value)) {
                                objResponse.statusCode = value;
                            } else if(ld.isString(value)) {
                                objResponse.description = value;
                            }
                        }
                    });
                }
            }

            return objResponse;
        }
    }
    // Exports
    module.exports = responseService;

})();
