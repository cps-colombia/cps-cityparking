/* global module */

// utils/oauthService.js
//
// service of utils to OAuth authentication
// genToken, signature, list params etc
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash"),
        crypto = require("crypto-js");

    oauthService.$inject =  [
        "$window",
        "$Logger"
    ];
    function oauthService ($window, $Logger) {
        var logger = $Logger.getInstance();

        // Members
        var Oauth = {
            init: CpsOAuth,
            parametersElement: ParametersElement
        };

        // Functions
        /**
         * @constructor
         *
         * @description Class to get tokens and signatires OAuth
         *
         */
        function CpsOAuth () {
            // constrtuctor
        }

        CpsOAuth.prototype.signature = function (httpMethod, url, parameters, consumerSecret, tokenSecret, options) {
            var paramOAuth = {},
                defOptions = {
                    encodeSignature: false
                },
                signatureBaseString;

            ld.forEach(parameters, function (value, key) {
                if(key !== "oauth_signature" && value) {
                    paramOAuth[key] = value;
                }
            });
            signatureBaseString = new SignatureBaseString(httpMethod, url, paramOAuth).generate();

            if (options) {
                ld.extend(defOptions, options);
            }
            defOptions.type = paramOAuth.oauth_signature_method;

            return new HmacSignature(signatureBaseString, consumerSecret, tokenSecret, defOptions).generate();
        };

        // Specification: http://oauth.net/core/1.0/#anchor14
        // url: if the scheme is missing, http will be added automatically
        function SignatureBaseString (httpMethod, url, parameters) {
            this._httpMethod = new HttpMethodElement(httpMethod).get();
            this._url = new UrlElement(url).get();
            this._parameters = new ParametersElement(parameters).get();
            this._rfc3986 = new Rfc3986();
        }

        SignatureBaseString.prototype.generate = function () {
            // HTTP_METHOD & url & parameters
            return this._rfc3986.encode(this._httpMethod) + "&" +
                this._rfc3986.encode(this._url) + "&" +
                this._rfc3986.encode(this._parameters);
        };


        function HttpMethodElement (httpMethod) {
            this._httpMethod = httpMethod || "";
        }

        HttpMethodElement.prototype.get = function () {
            return this._httpMethod.toUpperCase();
        };

        function UrlElement (url) {
            this._url = url || "";
        }

        UrlElement.prototype = {
            get : function () {
                // The following is to prevent js-url from loading the window.location
                if (!this._url) {
                    return this._url;
                }

                if (this._url.indexOf("://") < 0) {
                    logger.warn("OAuth signature need url sheme");
                    this._url = "http://" + this._url;
                }

                // Handle parsing the url in node or in browser
                var parsedUrl = this.parseInNode(),
                    scheme = (parsedUrl.scheme || "http").toLowerCase(),
                    authority = (parsedUrl.authority || "").toLocaleLowerCase(),
                    path = parsedUrl.path || "",
                    port = parseInt(parsedUrl.port) || "",
                    baseUrl = scheme + "://" + authority;

                if ((port === 80 && scheme === "http") || (port === 443 && scheme === "https")) {
                    port = "";
                }

                baseUrl = baseUrl + (!port ? ":" + port : "");
                if (path === "/" && this._url.indexOf(baseUrl + path) === -1) {
                    path = "";
                }
                this._url = (scheme ? scheme + "://" : "") +
                    authority + (port ? ":" + port : "") + path;

                return this._url;
            },
            parseInNode : function () {
                var url = require("url"),
                    parsedUri = url.parse(this._url),
                    scheme = parsedUri.protocol;
                // strip the ":" at the end of the scheme added by the url module
                if (scheme.charAt(scheme.length - 1) === ":") {
                    scheme = scheme.substring(0, scheme.length - 1);
                }
                return {
                    scheme : scheme,
                    authority : parsedUri.hostname,
                    port : parsedUri.port,
                    path : parsedUri.pathname
                };
            }
        };

        function ParametersElement (parameters) {
            // Parameters format: { "key": ["value 1", "value 2"] };
            this._parameters = parameters ? new ParametersLoader(parameters).get() : {};
            this._sortedKeys = [];
            this._normalizedParameters = [];
            this._rfc3986 = new Rfc3986();
            this._sortParameters();
            this._concatenateParameters();
        }

        ParametersElement.prototype = {
            _sortParameters : function () {
                var key,
                    encodedKey;
                for (key in this._parameters) {
                    if (this._parameters.hasOwnProperty(key)) {
                        encodedKey = this._rfc3986.encode(key);
                        this._sortedKeys.push(encodedKey);
                    }
                }
                this._sortedKeys.sort();
            },
            _concatenateParameters : function () {
                var i;
                for (i = 0; i < this._sortedKeys.length; i++) {
                    this._normalizeParameter(this._sortedKeys[i]);
                }
            },
            _normalizeParameter : function (encodedKey) {
                var i,
                    key = this._rfc3986.decode(encodedKey),
                    values = this._parameters[key],
                    encodedValue;
                values.sort();
                for (i = 0; i < values.length; i++) {
                    encodedValue = this._rfc3986.encode(values[i]);
                    this._normalizedParameters.push(encodedKey + "=" + encodedValue);
                }
            },
            get : function () {
                return this._normalizedParameters.join("&");
            }
        };

        function ParametersLoader (parameters) {
            // Format: { "key": ["value 1", "value 2"] }
            this._parameters = {};
            this._loadParameters(parameters || {});
        }

        ParametersLoader.prototype = {
            _loadParameters : function (parameters) {
                if (parameters instanceof Array) {
                    this._loadParametersFromArray(parameters);
                } else if (typeof parameters === "object") {
                    this._loadParametersFromObject(parameters);
                }
            },
            _loadParametersFromArray : function (parameters) {
                var i;
                for (i = 0; i < parameters.length; i++) {
                    this._loadParametersFromObject(parameters[i]);
                }
            },
            _loadParametersFromObject : function (parameters) {
                var key;
                for (key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        this._loadParameterValue(key, parameters[key] || "");
                    }
                }
            },
            _loadParameterValue : function (key, value) {
                var i;
                if (value instanceof Array) {
                    for (i = 0; i < value.length; i++) {
                        this._addParameter(key, value[i]);
                    }
                    if (value.length === 0) {
                        this._addParameter(key, "");
                    }
                } else {
                    this._addParameter(key, value);
                }
            },
            _addParameter : function (key, value) {
                if (!this._parameters[key]) {
                    this._parameters[key] = [];
                }
                this._parameters[key].push(value);
            },
            get : function () {
                return this._parameters;
            }
        };

        function Rfc3986 () {
            // constructor
        }

        Rfc3986.prototype = {
            encode : function (decoded) {
                if (!decoded) {
                    return "";
                }
                // using implementation from:
                // https://developer.mozilla.org > http://tinyurl.com/lgpnjkc
                return encodeURIComponent(decoded)
                    .replace(/[!"()]/g, $window.escape)
                    .replace(/\*/g, "%2A");
            },
            decode : function (encoded) {
                if (!encoded) {
                    return "";
                }
                return decodeURIComponent(encoded);
            }
        };

        function HmacSignature (signatureBaseString, consumerSecret, tokenSecret, options) {
            this._rfc3986 = new Rfc3986();
            this._text = signatureBaseString;
            this.encode = options.encodeSignature;
            this.type = options.type;

            if(tokenSecret) {
                this._key = this._rfc3986.encode(consumerSecret) + "&" + this._rfc3986.encode(tokenSecret);
            } else {
                this._key = this._rfc3986.encode(consumerSecret);
            }
            this._base64EncodedHash = new Hmac(this._text, this._key, this.type).getBase64EncodedHash();
        }

        HmacSignature.prototype.generate = function () {
            return this.encode ?
                this._rfc3986.encode(this._base64EncodedHash) : this._base64EncodedHash;
        };

        function Hmac (text, key, type) {
            this._text = text || "";
            this._key = key || "";
            this._hmacType = {
                "HMAC-MD5": crypto.HmacMD5,
                "HMAC-SHA1": crypto.HmacSHA1,
                "HMAC-SHA256": crypto.HmacSHA256
            }[type || "HMAC-SHA1"];

            this.hash = this._hmacType(this._text, this._key);
        }

        Hmac.prototype.getBase64EncodedHash = function () {
            return this.hash.digest("base64");
        };

        // Return here for prototype
        return Oauth;
    }

    // Exports
    module.exports =  oauthService;

})();
