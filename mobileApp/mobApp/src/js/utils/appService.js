/* global module, GoogleAnalytics */

// utils/appService.js
//
// app provider module for cps mobile app
// core function to run app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var aes = require("crypto-js/aes"),
        encry = require("crypto-js/md5"),
        encUtf8 = require("crypto-js/enc-utf8"),
        encb64 = require("crypto-js/enc-base64"),
        asyncLib = require("async"),
        ld = require("lodash");

    function appService () {
        var _isFullScreen = false,
            _isCordova = false,
            _platforms = null,
            _isReady = false,
            _grade = null,
            analyticsInit = false,
            readyCallbacks = [],
            IOS = "ios",
            ANDROID = "android",
            WINDOWS_PHONE = "windowsphone",
            EDGE = "edge",
            CROSSWALK = "crosswalk";

        // $get injector
        $get.$inject = [
            "$q",
            "$rootScope",
            "$document",
            "$window",
            "$Logger",
            "$Locale",
            "$Body",
            "$Dom",
            "$Scroll",
            "$Statusbar",
            "$Response",
            "$Encrypt",
            "CPS_CONFIG",
            "WS_OP"
        ];

        // setter and getter
        var AppProvider = {
            setIsCordova: setIsCordova,
            analyticsId: "",
            $get: $get
        };
        return AppProvider;

        // set cordova value
        function setIsCordova (isCordova) {
            _isCordova = !!isCordova;
        }

        // Getter
        function $get ($q, $rootScope, $document, $window, $Logger, $Locale, $Body,
                      $Dom, $Scroll, $Statusbar, $Response, $Encrypt, CPS_CONFIG, WS_OP) {
            var logger = $Logger.getInstance(),
                navigator = $window.navigator,
                deviceExtra = ld.get($window.cordova, "plugins.deviceData"),
                appOptions = ld.get(CPS_CONFIG, "options") || {},
                ua = $window.navigator.userAgent;

            // Memebers
            var App = {
                ops: WS_OP,
                config: CPS_CONFIG, // Get config from constant
                options: appOptions,
                deviceData: {},
                runApp: runApp,
                ready: ready,
                isReady: isReady,
                isCordova: isCordova,
                isFullScreen: isFullScreen,
                on: on,
                initPlatform: initPlatform,
                getPlatform: getPlatform,
                getValidation: getValidation,
                getDeviceData: getDeviceData,
                getCountries: getCountries,
                getCities: getCities,
                loginType: loginType(),
                initAnalytics: initAnalytics,
                checkAnalytics: checkAnalytics,
                sendAnalytics: sendAnalytics,
                is: isPlatform,
                isPlatform: isPlatform,
                isWebView: isWebView,
                isIPad: isIPad,
                isIOS: isIOS,
                isAndroid: isAndroid,
                isWindowsPhone: isWindowsPhone,
                isEdge: isEdge,
                isCrosswalk: isCrosswalk,
                fullScreen: fullScreen
            };

            return App;

            // Functions
            function runApp () {
                var deferred = $q.defer(),
                    msg = isCordova() ? "Run with cordova" : "Run without cordova",
                    errors = [];

                $window.addEventListener("statusTap", function () {
                    $Scroll.scrollTop(true);
                });

                // run app if cordova or not with callback
                asyncLib.series([
                    function (next) {
                        $Locale.initLanguage().then(function (locale) {
                            next(null, locale);
                        }).catch(function (err) {
                            errors.push(err);
                            next();
                        });
                    },
                    function (next) {
                        initPlatform().then(function (platform) {
                            next(null, platform);
                        }).catch(function (err) {
                            errors.push(err);
                            next();
                        });
                    },
                    function (next) {
                        getDeviceData().then(function (deviceData) {
                            next(null, deviceData);
                        }).catch(function (err) {
                            errors.push(err);
                            next();
                        });
                    }
                ], function (err, results) {
                    if(err || errors.length) {
                        err = err || errors;
                        msg = msg + " but with errores";
                        logger.warn("app", (ld.isObject(err) ? JSON.stringify(err) : err));
                    }
                    onPlatformReady();

                    deferred.resolve({
                        isCordova: isCordova(),
                        locale: results[0],
                        platform: results[1],
                        deviceData: results[2]
                    });
                    logger.info("app", msg);

                });

                return deferred.promise;
            }

            /**
             * @name ready
             * @description
             * Trigger a callback once the device is ready,
             * or immediately if the device is already ready.
             * @param {function=} callback The function to call.
             * @returns {promise} A promise which is resolved when the device is ready.
             */
            function ready (cb) {
                var deferred = $q.defer(),
                    callback = function () {
                        deferred.resolve();
                        if(cb) {
                            cb();
                        }
                    };
                // run through tasks to complete now that the device is ready
                if (_isReady) {
                    callback();
                } else {
                    // the platform isn't ready yet, add it to this array
                    // which will be called once the platform is ready
                    readyCallbacks.push(callback);
                }
                // Promise
                return deferred.promise;
            }

            /**
             * @name isReady
             *
             * @description If device is ready to rock!
             *
             * @returns {boolean} is ready.
             */
            function isReady () {
                return _isReady;
            }

            /**
             * @name isCordova
             *
             * @description Same to cps.js#detectCordova but static
             *
             * @returns {boolean} true or false Cordova.
             */
            function isCordova () {
                return _isCordova;
            }

            /**
             * isFullScreen
             *
             * @returns {boolean} Whether the device is fullscreen.
             */
            function isFullScreen () {
                return _isFullScreen;
            }

            /**
             * @name on
             *
             * @description Add Cordova event listeners, such as `pause`, `resume`, `volumedownbutton`, `batterylow`,
             * `offline`, etc. More information about available event types can be found in
             * [Cordova's event documentation](http://tinyurl.com/hutmr9f).
             * @param {string} type Cordova [event type](http://tinyurl.com/hutmr9f).
             * @param {function} callback Called when the Cordova event is fired.
             * @returns {function} Returns a deregistration function to remove the event listener.
             */
            function on (type, cb) {
                ready(function () {
                    $document[0].addEventListener(type, cb, false);
                });
                return function () {
                    ready(function () {
                        $document[0].removeEventListener(type, cb);
                    });
                };
            }


            /**
             * @name initPlatform
             *
             * @description Construct platform info
             *
             * @returns {promise} platform info.
             */
            function initPlatform () {
                var platform = getPlatform("update"),
                    deferred = $q.defer();

                $window.localStorage.platform = JSON.stringify(platform);
                if($window.localStorage.platform) {
                    deferred.resolve(platform);
                    logger.log("app", "Get platform");
                } else {
                    deferred.reject($Response.make(500));
                }
                return deferred.promise;
            }

            // Get Platform
            function getPlatform (action) {
                var platform = {},
                    storePlatform = $window.localStorage.platform,
                    deviceId = getDeviceId(),
                    devicePlatform = getDeviceName(),
                    platformId = getPlatformId(devicePlatform),
                    platformVersion = getDeviceVersion(),
                    protocol = parseInt(platformId) === 3 ? "ms-appx-web://" : "file://";

                if(storePlatform && action !== "update") {
                    platform = JSON.parse(storePlatform);
                } else {
                    platform = {
                        id: platformId,
                        uuid: deviceId,
                        name: devicePlatform,
                        protocol: protocol,
                        version: platformVersion,
                        deviceId: deviceId,
                        platformId: platformId,
                        platform: devicePlatform
                    };
                }

                return platform;
            }

            // Get Validation
            function getValidation () {
                var validator,
                    // TODO: fix async getDeviceData().activity,
                    activity = ld.upperFirst(ld.camelCase(CPS_CONFIG.appName)),
                    platforms = CPS_CONFIG.platforms[getPlatform().platform],
                    platformId = getPlatform().platformId,
                    store = platforms && platforms.packageId;

                if(CPS_CONFIG.debug) {
                    validator = "debug";
                } else if(activity && store) {
                    validator = encry(activity + ":" + store + ":" + platformId).toString();
                } else {
                    validator = encry(getDeviceId() + ":" + store + ":" + platformId).toString();
                    logger.warn("app", "Unable to get validator: activity: {0}, store: {1}", [activity, store]);
                }

                return validator;
            }

            // Device and user data
            function getDeviceData () {
                var deferred = $q.defer(),
                    dataStorage,
                    updateData,
                    udd;

                if(!deviceExtra && isCordova() && isAndroid()) {
                    logger.warn("app", "'com.cps.plugin.getdevicedata' is required");
                    deferred.reject({
                        code: 500,
                        message: "'com.cps.plugin.getdevicedata' > 0.8 is required for Android"
                    });
                    return deferred.promise;
                }

                if(ld.isEmpty(App.deviceData) && deviceExtra) {
                    udd = $window.localStorage.udd;
                    if(udd && updateData) { // Disabled
                        // TODO: update storage to work
                        // Get from storage if exist
                        try {
                            dataStorage = JSON.parse(aes.decrypt(udd, CPS_CONFIG.appsecret).toString(encUtf8));
                            ld.extend(App.deviceData, dataStorage);
                            logger.info("app", "Success deviceData from storage");

                            deferred.resolve(App.deviceData);
                        } catch(err) {
                            $window.localStorage.clear();
                            deferred.reject(err);
                        }
                    } else if(!deviceExtra.available) {
                        logger.info("app", "Get deviceData");

                        deviceExtra.get(function (deviceData) {
                            setDeviceData(deviceData);
                            logger.info("app", "Success deviceData from plugin");

                            deferred.resolve(App.deviceData);
                        }, function (err) {
                            deferred.reject(err);
                        });
                    } else {
                        setDeviceData(deviceExtra);
                        logger.info("app", "Success deviceData from plugin");

                        deferred.resolve(App.deviceData);
                    }
                } else {
                    deferred.resolve(App.deviceData);
                }
                // Promise
                return deferred.promise;
            }

            // Get countries in localStorage
            function getCountries () {
                var countryStorage = $window.localStorage.countries,
                    countries = countryStorage ? JSON.parse(countryStorage) : null;

                if(!countries || ld.size(countries) <= 0) {
                    $rootScope.$emit("cps:baseData:update", "update");
                }

                return countries;
            }

            // Get cities by country in localStorage
            function getCities (id) {
                var countries = getCountries(),
                    country,
                    cities;

                if(countries && id) {
                    if(ld.isArray(countries)) {
                        country = ld.find(countries, {idcountry: parseInt(id)}) ||
                            ld.find(countries, {idcountry: String(id)});
                    } else {
                        country = countries["countryId" + id];
                    }
                    if(country) {
                        cities = country.cities;
                    }
                }
                return cities;
            }

            /**
             * loginType
             *
             * Get if login need numeric, alpha-numeric or custom
             *
             * @return {object} with regex and type
             */
            function loginType () {
                var moduleOptions = CPS_CONFIG.options || {},
                    optLoginType = moduleOptions.loginType,
                    defaultRegex = {
                        numeric: /^[0-9]+$/,
                        alphanumeric: /^[0-9a-zA-Z]+$/
                    },
                    optDefaults = {
                        name: "numeric",
                        regex: defaultRegex.numeric
                    };

                if(ld.isObject(optLoginType) && !ld.isEmpty(optLoginType)) {
                    optDefaults.name = optLoginType.regex ?
                        "custom" : (optLoginType.numeric ? "numeric" : "alphanumeric");
                    optDefaults.regex = optDefaults.name === "custom" ?
                        new RegExp(optLoginType.regex) : defaultRegex[optDefaults.name];
                }
                return optDefaults;
            }

            // Analitics Init
            function initAnalytics () {
                var platform = App.getPlatform(),
                    analytics = $window.navigator.analytics,
                    trackingId = AppProvider.analyticsId,
                    platformId = parseInt(platform.platformId),
                    options;

                if(trackingId && _isCordova && !CPS_CONFIG.debug) {
                    if(platformId === 3 && $window.GoogleAnalytics) {
                        logger.info("app", "Analytics WP Init");

                        options = new GoogleAnalytics.EasyTrackerConfig();
                        options.trackingId = trackingId;
                        options.appName = CPS_CONFIG.displayName;
                        options.appVersion = CPS_CONFIG.version;
                        GoogleAnalytics.EasyTracker.current.config = options;

                        analyticsInit = true;
                    } else if(analytics) {
                        analytics.setTrackingId(
                            trackingId, function (response) {
                                analyticsInit = true;
                                logger.info("app", "Analytics General Init");
                            }, function (error) {
                                analyticsInit = false;
                                logger.warn("app", "Error Analytics General Init: {0}", [error]);
                            });
                    } else {
                        logger.warn("app", "Analytics unsupported in platform id {0}", [platformId]);
                    }
                }
            }

            // Check if init Analytics
            function checkAnalytics () {
                return analyticsInit;
            }

            // Send Analytics stadistics
            function sendAnalytics (action, type, content, label, value, code) {
                var platform = App.getPlatform().platformId,
                    analytics = $window.navigator.analytics ||
                    (GoogleAnalytics && GoogleAnalytics.EasyTracker.getTracker()) || {},
                    Fields    = analytics.Fields,
                    HitTypes  = analytics.HitTypes,
                    params    = {};

                code = isFinite(code) ? parseInt(code) : 0;

                if(analyticsInit && _isCordova && !CPS_CONFIG.debug) {
                    if(action === "send") {
                        if(type === "pageview") {
                            if(analytics.sendAppView) {
                                analytics.sendAppView(content);
                            } else if(analytics.sendView) {
                                analytics.sendView(content);
                            }
                        } else if (type === "event") {
                            analytics.sendEvent(content, label, (value || content), code);
                            switch(content) {
                            case "social":
                            case "share":
                                switch(platform) {
                                case "3":
                                    analytics.sendSocial(label, content, (value || label));
                                    break;
                                default:
                                    params[Fields.HIT_TYPE] = HitTypes.SOCIAL;
                                    params[Fields.SOCIAL_ACTION] = content;
                                    params[Fields.SOCIAL_NETWORK] = label;
                                    params[Fields.SOCIAL_TARGET] = (value || label);
                                    analytics.send(params);
                                }
                                break;
                            }
                        } else if (type === "exception") {
                            analytics.sendException(content, label);
                            analytics.sendEvent("error", "Exception - " + content, (value || "exception"), code);
                        }
                    } else if (action === "set") {
                        if(type === "uid") {
                            switch(platform) {
                            case "3":
                                // wp not send Useid
                                break;
                            default:
                                params[Fields.USER_ID]  = content;
                                analytics.send(params);
                            }
                        }
                    }
                }
            }

            /**
             * @name isPlatform
             * @param {string} type Platform name.
             *
             * @return {boolean} Whether the platform name provided is detected.
             */
            function isPlatform (type) {
                var pName;
                type = type.toLowerCase();
                // check if it has an array of platforms
                if (_platforms) {
                    for (var x = 0; x < _platforms.length; x++) {
                        if (_platforms[x] === type) {
                            return true;
                        }
                    }
                }
                // exact match
                pName = getPlatform().platform;
                if (pName) {
                    return pName === type.toLowerCase();
                }

                // A quick hack for to check userAgent
                return ua.toLowerCase().indexOf(type) >= 0;
            }

            /**
             * @name isWebView
             * @return {boolean} Check if we are running within a WebView (such as Cordova).
             */
            function isWebView () {
                return !(!window.cordova && !window.PhoneGap && !window.phonegap && !window.forge);
            }

            /**
             * @name isIpad
             *
             * @return {boolean} Whether we are running on iPad.
             */
            function isIPad () {
                if (/iPad/i.test(navigator.platform)) {
                    return true;
                }
                return /iPad/i.test(ua);
            }
            /**
             * @name isIOS
             *
             * @return {boolean} Whether we are running on iOS.
             */
            function isIOS () {
                return isPlatform(IOS);
            }
            /**
             * @name isAndroid
             *
             * @return {boolean} Whether we are running on Android.
             */
            function isAndroid () {
                return isPlatform(ANDROID);
            }
            /**
             * @name isWindowsPhone
             *
             * @return {boolean} Whether we are running on Windows Phone.
             */
            function isWindowsPhone () {
                return isPlatform(WINDOWS_PHONE);
            }

            /**
             * @name isEdge
             * @return {boolean} Whether we are running on MS Edge/Windows 10 (inc. Phone)
             */
            function isEdge () {
                return isPlatform(EDGE);
            }

            /**
             * @name isCrosswalk
             * @return {boolean} Whether we are running on Crosswalk Android enginer
             */
            function isCrosswalk () {
                return isPlatform(CROSSWALK);
            }

            /**
             * @name fullScreen
             *
             * @description Sets whether the app is fullscreen or not (in Cordova).
             *
             * @param {boolean=} showFullScreen Whether or not to set the app to fullscreen.
             *  Defaults to true. Requires `cordova plugin add org.apache.cordova.statusbar`
             * @param {boolean=} showStatusBar Whether or not to show the device's status bar.
             *  Defaults to false.
             */
            function fullScreen (_showFullScreen, _showStatusBar) {
                // showFullScreen: default is true if no param provided
                _isFullScreen = (_showFullScreen === true);

                // add/remove the fullscreen classname to the body
                ready(function () {
                    // run this only when or if the DOM is ready
                    $Dom.requestAnimationFrame(function () {
                        if (isFullScreen()) {
                            document.body.classList.add("fullscreen");
                        } else {
                            document.body.classList.remove("fullscreen");
                        }
                    });
                    // showStatusBar: default is false if no param provided and if fullScreen
                    _showStatusBar = !_isFullScreen && ld.isUndefined(_showStatusBar) ?
                        true : _showStatusBar;
                    // Only useful when run within cordova
                    $Statusbar.visible(_showStatusBar);
                });
            }

            // Private

            /**
             * @name onPlatformReady
             *
             * @description When platform is ready fire all callback on hold
             *
             */
            function onPlatformReady () {
                // the device is all set to go, init our own stuff then fire off our event
                _isReady = true;
                platformInBody();
                for (var x = 0; x < readyCallbacks.length; x++) {
                    // fire off all the callbacks that were added before the platform was ready
                    readyCallbacks[x]();
                }
                readyCallbacks = [];
                $Dom.requestAnimationFrame(function () {
                    $Body.addClass("platform-ready");
                });
            }

            /**
             * @name platformInBody
             *
             * @description Add class depending on platform
             *
             */
            function platformInBody () {
                checkPlatforms();

                $Dom.requestAnimationFrame(function () {
                    $Body.addClass("app-" + ld.kebabCase(CPS_CONFIG.appName));
                    // only add to the body class if we got platform info
                    for (var i = 0; i < _platforms.length; i++) {
                        $Body.addClass("platform-" + _platforms[i]);
                    }
                });
            }

            function checkPlatforms () {
                var platform = getDeviceName(),
                    version = getDeviceVersion();

                _platforms = [];
                _grade = "a";

                if (isWebView()) {
                    _platforms.push("webview");
                    if (isCordova()) {
                        _platforms.push("cordova");
                    } else if ($window.forge) {
                        _platforms.push("trigger");
                    }
                } else {
                    _platforms.push("browser");
                }
                if (isIPad()) {
                    _platforms.push("ipad");
                }

                if (platform) {
                    _platforms.push(platform);

                    if (version) {
                        var v = version.toString();
                        if (v.indexOf(".") > 0) {
                            v = v.replace(".", "_");
                        } else {
                            v += "_0";
                        }
                        _platforms.push(platform + v.split("_")[0]);
                        _platforms.push(platform + v);

                        if (isAndroid() && version < 4.4) {
                            _grade = version < 4 ? "c" : "b";
                        } else if (isWindowsPhone()) {
                            _grade = "b";
                        }
                    }
                }

                setGrade(_grade);
            }

            /**
             * setGrade
             *
             * Set the grade of the device: 'a', 'b', or 'c'. 'a' is the best
             * (most css features enabled), 'c' is the worst.  By default, sets the grade
             * depending on the current device.
             * @param {string} grade The new grade to set.
             *
             */
            function setGrade (grade) {
                var oldGrade = _grade;
                _grade = grade;

                $Dom.requestAnimationFrame(function () {
                    if (oldGrade) {
                        $Body.removeClass("grade-" + oldGrade);
                    }
                    $Body.addClass("grade-" + grade);
                });
            }

            /**
             * getDeviceName
             *
             * Get name of platform from device plugin cordava or User Agent
             *
             * @return {string} platform name
             */
            function getDeviceName () {
                var platformName,
                    defaultName = "android",
                    cpsDevice = $window.device,
                    na = $window.navigator.platform;

                if(!isCordova()) {
                    platformName = defaultName;
                } else if (cpsDevice) {
                    platformName = cpsDevice.platform;
                } else if (ua.indexOf("Android") > -1) {
                    platformName = defaultName;
                } else if (/iPhone|iPad|iPod/.test(ua)) {
                    platformName = "ios";
                } else if (ua.indexOf("Windows Phone") > -1) {
                    platformName = "windows";
                } else {
                    platformName = na && na.platform || "";
                }
                return platformName.toLowerCase().trim().split(/(\s+)/)[0];
            }

            /**
             * getPlatformId
             *
             * Get ID device type for CPS
             * @param {string} devicePlatform name of platform
             *
             * @return {string} platform ID
             */
            function getPlatformId (devicePlatform) {
                var platformId;

                if (devicePlatform === "android") {
                    platformId = "1";
                } else if (devicePlatform === "ios") {
                    platformId = "2";
                } else if (devicePlatform === "win32nt" ||
                           devicePlatform === "wince" ||
                           devicePlatform === "windows" ||
                           devicePlatform === "windowsphone") {
                    platformId = "3";
                } else {
                    platformId = "1";
                }
                return platformId;
            }

            /**
             *  getDeviceId
             *
             * Get unique id by device
             *
             * @return {integer} device uuid
             */
            function getDeviceId () {
                var cpsDevice = $window.device,
                    nav = $window.navigator,
                    screen = $window.screen,
                    guid = "",
                    deviceId;

                if(cpsDevice) {
                    deviceId = cpsDevice.uuid;
                } else {
                    guid += (nav.mimeTypes || nav.cpuClass || "").length;
                    guid += nav.userAgent.replace(/\D+/g, "");
                    guid += (nav.plugins || nav.appMinorVersion || "").length;
                    guid += screen.height || "";
                    guid += screen.width || "";
                    guid += screen.pixelDepth || "";

                    deviceId = encry(guid).toString(encb64);
                }
                return deviceId;
            }
            /**
             * getDeviceVersion
             *
             * Get version device
             *
             * @return {integer} device version
             */
            function getDeviceVersion () {
                var ua = $window.navigator.userAgent,
                    cpsDevice = $window.device,
                    platformVersion = 0,
                    deviceVersion;

                if (cpsDevice) {
                    deviceVersion = cpsDevice.version.split(".");
                    deviceVersion = parseFloat(deviceVersion[0] + "." +
                                               (deviceVersion.length > 1 ? deviceVersion[1] : 0));
                    if (!isNaN(deviceVersion)) {
                        platformVersion = deviceVersion;
                    }
                } else {
                    // fallback to user-agent checking
                    var pName = getDeviceName(),
                        versionMatch = {
                            "android": /Android (\d+).(\d+)?/,
                            "ios": /OS (\d+)_(\d+)?/,
                            "windows": /Windows Phone (\d+).(\d+)?/
                        };
                    if (versionMatch[pName]) {
                        deviceVersion = ua.match(versionMatch[pName]);
                        if (deviceVersion &&  deviceVersion.length > 2) {
                            platformVersion = parseFloat(deviceVersion[1] + "." + deviceVersion[2]);
                        }
                    }
                }
                return platformVersion;
            }

            function setDeviceData (deviceData) {
                if(ld.isObject(deviceData)) {
                    if(deviceData.displayName) {
                        var gdDisplayName = deviceData.displayName.trim().split(/\s+/);
                        if(gdDisplayName.length >= 3) {
                            deviceData.name = gdDisplayName[0];
                            deviceData.lastName = gdDisplayName[2];
                        } else if (gdDisplayName.length === 2) {
                            deviceData.name = gdDisplayName[0];
                            deviceData.lastName = gdDisplayName[1];
                        } else {
                            deviceData.name = deviceData.displayName;
                        }
                    }
                    // Extend data
                    deviceData.phone = deviceData.phone || deviceData.profilePhone;
                    ld.extend(App.deviceData, ld.toPlainObjectDeep(deviceData));

                    // Save en storage
                    $window.localStorage.udd = aes.encrypt(JSON.stringify(App.deviceData), CPS_CONFIG.appsecret);
                }
            }
        }
    }

    // Exports
    module.exports = appService;

})();
