/* global module */

// app/utils/utilsFilter.js
//
// filters functions for cps App
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require ("lodash");

    // Members
    var utilsFilter = {
        cpsLodash: cpsLodash,
        cpsCamelCase: cpsCamelCase,
        cpsCapitalize: cpsCapitalize,
        cpsStartCase: cpsStartCase,
        cpsTitleCase: cpsTitleCase,
        cpsWhere: cpsWhere,
        cpsInteger: cpsInteger,
        cpsDates: cpsDates,
        cpsIsRoute: cpsIsRoute,
        cpsFormat: cpsFormat,
        cpsDateFormat: cpsDateFormat,
        cpsDurationFormat: cpsDurationFormat,
        cpsDurationHumanize: cpsDurationHumanize,
        cpsFlatObject: cpsFlatObject
    };

    // Functions
    /**
     * @name cpsLodash
     *
     * @description lodash utils filter
     *
     * @param {string/array/object} input data for lodash
     * @param {string} func lodash function to call
     *
     * @return {string/array/object} lodash return
     */
    function cpsLodash () {
        var lodashFilter = lodash;
        return lodashFilter;

        // Function
        function lodash (input, func, args) {
            return ld[func] ? ld[func](input, args) : input;
        }
    }

    /**
     * @name cpsCamelCase
     *
     * @description Convert string to camelCase
     *
     * @param {string} input text to convert
     *
     * @return {string} with text to camelCase
     */
    function cpsCamelCase () {
        var camelCaseFilter = camelCase;
        return camelCaseFilter;

        // Function
        function camelCase (input) {
            return ld.camelCase(input);
        }
    }

    /**
     * @name cpsCapitalize
     *
     * @description convert to lowercase and
     * capitalize firt letter
     *
     * @param {string} input text to convert
     *
     * @return {string} with text capitalize
     */
    function cpsCapitalize () {
        var capitalizeFilter = capitalize;
        return capitalizeFilter;

        // Function
        function capitalize (input) {
            // From custom mixin
            return ld.toCapitalize(input);
        }
    }

    /**
     * @name cpsStartCase
     *
     * @description convert css style capitalize
     * all firts latter in capitalize with lodash.startCase
     *
     * @param {string} input text to convert
     *
     * @return {string} with text capitalize
     */
    function cpsStartCase () {
        var startCaseFilter = startCase;
        return startCaseFilter;

        // Function
        function startCase (input) {
            // From custom mixin
            return ld.toStartCase(input);
        }
    }

    /**
     * @name cpsTitleCase
     *
     * @description capitalize all firts letter as a Ttile
     *
     * @param {string} input text to convert
     *
     * @return {string} with text as a Title
     */
    function cpsTitleCase () {
        var titleCaseFilter = titleCase;
        return titleCaseFilter;

        // Function
        function titleCase (input) {
            // From custom mixin
            return ld.toTitleCase(input);
        }
    }

    /**
     * @name cpsWhere
     *
     * @description convert finite string to integer
     *
     * @param {array} input array to reduce or filter
     * @param {array/object} dataFilter data to filter input array
     * if array need key value
     * @param {string} key to create object filter
     * @param {string} type optional convert value to 'string' or 'number'
     *
     * @return {integer} with number converted
     */
    cpsWhere.$inject = ["$Logger"];
    function cpsWhere ($Logger) {
        var logger = $Logger.getInstance(),
            whereFilter = where,
            toType = {
                string: String,
                number: Number,
                default: function (val) {
                    return val;
                }
            },
            obj = {},
            result;
        return whereFilter;

        // Function
        function where (input, dataFilter, key, type) {
            type = toType[type] || toType["default"];
            obj = {};
            result = [];

            if(ld.isArray(input)) {
                if(ld.isArray(dataFilter) && key) {
                    result = ld.reduce(input, function (newval, allow, index) {
                        obj = {};
                        obj[key] = type(allow[key]);
                        if(ld.find(dataFilter, obj)) {
                            newval.push(allow);
                        }
                        return newval;
                    }, []);
                } else if(ld.isObject(dataFilter)) {
                    result = ld.find(input, dataFilter);
                } else {
                    logger.debug("cpsWhere filter need array or object");
                }
            }

            return result;
        }
    }

    /**
     * @name cpsInteger
     *
     * @description convert finite string to integer
     *
     * @param {string} input number
     *
     * @return {integer} with number converted
     */
    function cpsInteger () {
        var integerFilter = toInteger;
        return integerFilter;

        // Function
        function toInteger (input) {
            return isFinite(input) ? parseInt(input, 10) : input;
        }
    }

    /**
     * @name cpsDates
     *
     * @description Generates days, months and years from service $Utils.genDates
     *
     * @param {array} input object or array push result like day in []
     * @param {string} type date type to get, days, years or months
     *
     * @return {array} with date object values, index, value and/or locale
     */
    cpsDates.$inject = ["$Utils"];
    function cpsDates ($Utils) {
        var genDatesFilter = genDates;
        return genDatesFilter;

        // function
        function genDates (input, type) {
            return $Utils.genDates(type);
        }
    }

    /**
     * @name cpsIsRoute
     *
     * @description Filter check if route is equal
     *
     * @param {string} route path route to be compared
     * @param {string} params aditional path params
     *
     * @return {boolean} true or false
     */
    cpsIsRoute.$inject = ["$location", "$route"];
    function cpsIsRoute ($location, $route) {
        var isRouteFilter = isRoute;
        return isRouteFilter;

        // Function
        function isRoute (route, params) {
            // TODO: check params
            return ld.kebabCase($location.path()) === ld.kebabCase(route);
        }
    }

    /**
     * @name cpsFormat
     *
     * @description Filter to convert currency and number to custom or localization formats
     *
     * @param {string} value original value
     * @param {string} formatStyle format to convert
     *
     * @return {string} new format type
     */
    cpsFormat.$inject = ["$Numbro"];
    function cpsFormat ($Numbro) {
        var newFormatFilter = newFormat;
        return newFormatFilter;

        // functions
        function newFormat (value, formatStyle) {
            var format = formatStyle || "0,0";

            return $Numbro(value).formatCurrency(format);
        }
    }

    /**
     * @name cpsDateFormat
     *
     * @description Filter to convert time by custom format or localization
     *
     * @param {string} value original value
     * @param {string} formatStyle format to convert
     * TODO: Independent modulo for moment
     * @return {string} new moment type
     */
    cpsDateFormat.$inject = ["$Moment"];
    function cpsDateFormat ($Moment) {
        var newMomentFilter = newMoment;
        newMomentFilter.$stateful = $Moment.options.statefulFilters;

        return newMomentFilter;

        // functions
        function newMoment (value, momentStyle) {
            var format = momentStyle || "h:mm a",
                date;

            if (ld.isNotdefined(value)) {
                return "";
            }

            date = $Moment.get(value);
            if (!date.isValid()) {
                return "";
            }

            return date.format(format);
        }
    }

    /**
     * @name cpsDurationFormat
     *
     * @param {string} value original value
     * @param {string} format format to convert
     * @param {string} suffix show to the end
     * TODO: Independent modulo for moment
     * @return {string} moment duration
     */
    cpsDurationFormat.$inject = ["$Moment"];
    function cpsDurationFormat ($Moment) {
        var durationFormatFilter = cpsDurationFormatFilter;
        durationFormatFilter.$stateful = $Moment.options.statefulFilters;

        return durationFormatFilter;

        // Functions
        function cpsDurationFormatFilter (value, format, suffix) {
            if (ld.isNotdefined(value)) {
                return "";
            }

            return $Moment.duration(value, format).humanize(suffix);
        }
    }

    /**
     * @name cpsDurationHumanize
     *
     * @param {string} value original value
     * @param {object} options humanize cutomization
     * TODO: Independent modulo for moment
     * @return {string} humanize duration
     */
    cpsDurationHumanize.$inject = ["$Moment"];
    function cpsDurationHumanize ($Moment) {
        var durationHumanizeFilter = cpsDurationHumanizeFilter;
        durationHumanizeFilter.$stateful = $Moment.options.statefulFilters;

        return durationHumanizeFilter;

        // Functions
        function cpsDurationHumanizeFilter (value, options) {
            if (ld.isNotdefined(value)) {
                return "";
            }
            return $Moment.durationHumanize(value, options);
        }
    }

    /**
     * @name cpsFlatObject
     *
     * @description Filter to convert multi-level object to single-level object
     *
     * @param {object} value original value
     * @param {string} separator to value object
     *
     * @return {object} single-level object
     */
    cpsFlatObject.$inject = ["$Logger", "$Numbro"];
    function cpsFlatObject ($Logger, $Numbro) {
        var flatObjectFilter = flatObject;

        return flatObjectFilter;

        // functions
        function flatObject (value, options) {
            var parseValue = {},
                defPick = [
                    "name",
                    "date",
                    "amount",
                    "by_user",
                    "user_name",
                    "branch_name",
                    "commerce_name"
                ];

            // Extend options
            options = ld.extend({
            }, options);

            // Extend pick values
            if(ld.isArray(options.pick)) {
                defPick = defPick.push(options.pick);
            }
            // slenderize
            if(ld.isObject(value)) {
                parseValue = ld.pick(ld.slenderizeObject(value, options), defPick);

                if(!ld.isArray(options.currency)) {
                    options.currency = options.currency ? [options.currency] : [];
                }

                if(options.currency.length) {
                    ld.forEach(options.currency, function (currency, index) {
                        if(parseValue[currency]) {
                            parseValue[currency] = $Numbro.formatCurrency(parseValue[currency]);
                        }
                    });
                }

            } else {
                parseValue = value;
                $Logger.warn("locale-args need object");
            }

            return parseValue;
        }
    }

    // Exports
    module.exports = utilsFilter;

})();
