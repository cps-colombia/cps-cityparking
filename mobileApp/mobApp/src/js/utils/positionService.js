/* global module */

// utils/positionService.js
//
// factory service of utility methods that
// can be use to retrieve position of DOM elements.
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        $$ = angular.element;

    positionService.$inject =  [
        "$document",
        "$window"
    ];
    function positionService ($document, $window) {

        var Position = {
            position: position,
            offset: offset
        };
        return Position;

        // Functions
        /**
         * @name position
         * @description Get the current coordinates of the element, relative to the offset parent.
         * Read-only equivalent of [jQuery's position function](http://api.jquery.com/position/).
         * @param {element} element The element to get the position of.
         * @returns {object} Returns an object containing the properties top, left, width and height.
         */
        function position (element) {
            var elBCR = offset(element),
                offsetParentBCR = { top: 0, left: 0 },
                offsetParentEl = parentOffsetEl(element[0]),
                boundingClientRect = element[0].getBoundingClientRect();

            if (offsetParentEl !== $document[0]) {
                offsetParentBCR = offset($$(offsetParentEl));
                offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
                offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
            }

            return {
                width: boundingClientRect.width || element.prop("offsetWidth"),
                height: boundingClientRect.height || element.prop("offsetHeight"),
                top: elBCR.top - offsetParentBCR.top,
                left: elBCR.left - offsetParentBCR.left
            };
        }

        /**
         * @name offset
         * @description Get the current coordinates of the element, relative to the document.
         * Read-only equivalent of [jQuery's offset function](http://api.jquery.com/offset/).
         * @param {element} element The element to get the offset of.
         * @returns {object} Returns an object containing the properties top, left, width and height.
         */
        function offset (element) {
            var boundingClientRect = element[0].getBoundingClientRect();

            return {
                width: boundingClientRect.width || element.prop("offsetWidth"),
                height: boundingClientRect.height || element.prop("offsetHeight"),
                top: boundingClientRect.top + ($window.pageYOffset || $document[0].documentElement.scrollTop),
                left: boundingClientRect.left + ($window.pageXOffset || $document[0].documentElement.scrollLeft)
            };
        }


        // Private
        function getStyle (el, cssprop) {
            if (el.currentStyle) { // IE
                return el.currentStyle[cssprop];
            } else if ($window.getComputedStyle) {
                return $window.getComputedStyle(el)[cssprop];
            }
            // finally try and get inline style
            return el.style[cssprop];
        }

        /**
         * Checks if a given element is statically positioned
         * @param element - raw DOM element
         */
        function isStaticPositioned (element) {
            return (getStyle(element, "position") || "static") === "static";
        }

        /**
         * returns the closest, non-statically positioned parentOffset of a given element
         * @param element
         */
        function parentOffsetEl (element) {
            var docDomEl = $document[0],
                offsetParent = element.offsetParent || docDomEl;

            while (offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent)) {
                offsetParent = offsetParent.offsetParent;
            }
            return offsetParent || docDomEl;
        }
    }

    // Exports
    module.exports =  positionService;

})();
