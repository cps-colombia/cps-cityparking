/* global module */

// utils/inBrowserService.js
//
// provider service for inAppBrowser cordova plugin.
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash");

    function inBrowserService () {
        var defaultOptions = {
            zoom: "no",
            location: "no",
            withHeader: true
        };

        // $get injector
        $get.$inject = [
            "$q",
            "$rootScope",
            "$window",
            "$timeout",
            "$Logger",
            "$App",
            "$Mobile",
            "$Request"
        ];

        // setter and getter
        var HardwareServices = {
            setOptions: setOptions,
            $get: $get
        };
        return HardwareServices;

        // Setter
        function setOptions (config) {
            defaultOptions = angular.extend(defaultOptions, config);
        }

        // Getter
        function $get ($q, $rootScope, $window, $timeout, $Logger, $App, $Mobile, $Request) {
            var _ = $Mobile.locale,
                logger = $Logger.getInstance(),
                isCordova = $App.isCordova(),
                config = $App.config,
                cssString,
                ref;

            // Members
            var InBrowser = {
                init: init,
                open: open,
                close: close,
                show: show,
                executeScript: executeScript,
                insertCSS: insertCSS,
                loadCSS: loadCSS,
                codeInBrowser: codeInBrowser
            };

            return InBrowser;

            // Functions
            function init () {
                loadCSS();
            }

            function open (url, target, requestOptions) {
                var deferred = $q.defer(),
                    opt = [],
                    optionsString,
                    options;

                if (requestOptions && !ld.isObject(requestOptions)) {
                    deferred.reject("options must be an object");
                    return deferred.promise;
                }

                options = ld.extend({}, defaultOptions, requestOptions);

                // Options to string
                ld.forEach(options, function (value, key) {
                    opt.push(key + "=" + value);
                });
                optionsString = opt.join();

                ref = $window.open(url, target, optionsString);

                if(ref && isCordova) {
                    ref.addEventListener("loadstart", function (event) {
                        $timeout(function () {
                            var url = event.url || "",
                                isOut = url.indexOf("CloseIAB");

                            if(isOut >= 0) {
                                close();
                            } else {
                                $rootScope.$broadcast("cps:inBrowser:loadstart", event);
                            }
                        });
                    }, false);

                    ref.addEventListener("loadstop", function (event) {
                        if(options && options.withHeader) {
                            executeScript({
                                code: "(" + String(codeInBrowser) + ")('" + _("close") + "');"
                            }).then(function (res) {
                                logger.warn("inBrowser", "Success executeScript");
                            }).catch(function (err) {
                                err = ld.isObject(err) ? JSON.stringify(err) : err;
                                logger.warn("inBrowser", "Error executeScript, {0}", [err]);
                                $Mobile.ga("send", "exception", "inBrowser: Error executeScript - " + err, true);
                            });
                        }
                        insertCSS({code: String(cssString)}).then(function (res) {
                            logger.warn("inBrowser", "Success insertCss");
                        }).catch(function (err) {
                            err = ld.isObject(err) ? JSON.stringify(err) : err;
                            logger.warn("inBrowser", "Error insertCss, {0}", [err]);
                            $Mobile.ga("send", "exception", "inBrowser: Error insertCss - " + err, true);
                        });

                        deferred.resolve(event);
                        $timeout(function () {
                            $rootScope.$broadcast("cps:inBrowser:loadstop", event);
                        });
                    }, false);

                    ref.addEventListener("loaderror", function (event) {
                        deferred.reject(event);
                        $timeout(function () {
                            $rootScope.$broadcast("cps:inBrowser:loaderror", event);
                        });
                    }, false);

                    ref.addEventListener("exit", function (event) {
                        $timeout(function () {
                            $rootScope.$broadcast("cps:inBrowser:exit", event);
                        });
                    }, false);
                } else {
                    deferred.resolve(ref);
                }

                return deferred.promise;
            }

            function close () {
                if(ref && ref.close) {
                    ref.close();
                    ref = null;
                } else {
                    ref = null;
                }
            }

            function show () {
                if(ref && ref.show) {
                    ref.show();
                }
            }

            function executeScript (details) {
                var deferred = $q.defer();

                if(ref && ref.executeScript) {
                    ref.executeScript(details, function (result) {
                        deferred.resolve(result);
                    });
                } else {
                    deferred.reject();
                }

                return deferred.promise;
            }

            function insertCSS (details) {
                var deferred = $q.defer();

                if(ref && ref.insertCSS) {
                    ref.insertCSS(details, function (result) {
                        deferred.resolve(result);
                    });
                } else {
                    deferred.reject();
                }

                return deferred.promise;
            }

            function loadCSS () {
                var fileCss = config.options.inBrowserCss || "static/css/bundle.min.css";

                if(!cssString) {
                    $Request.http({
                        url: fileCss,
                        background: true,
                        method: "GET"
                    }).then(function (res) {
                        if(res && res.data) {
                            cssString = res.data;
                        }
                    });
                }
            }

            /**
             * @name codeInBrowser
             * @description this code append to inAppBrowser as string
             * with executeScript, this have pure js without angular or external libs
             * for compatibility with external instance
             *
             */
            function codeInBrowser (btnText) {
                var el = document.body,
                    div = document.createElement("div"),
                    html = "<header class='bar bar-header bar-primary'>" +
                        "<h1 class='title title-center np logo-app'>" +
                        "</h1>" +
                        "<button class='button button-icon icon ion-close' onclick='closeInAppBrowser()'>" +
                        "<span>" + btnText + "</span>" +
                        "</button>" +
                        "</header>",
                    vpContent = "user-scalable=no, initial-scale=1, maximum-scale=1, " +
                        "width=device-width, height=device-height",
                    viewport,
                    fistDiv,
                    tagEl;
                // TODO: Load custom html with $Template.compile
                div.innerHTML = html;
                // Custom class body
                el.className = (el.className && el.className.length) ?
                    el.className + " inapp-browser" : "inapp-browser";

                // el.insertBefore(div, el.firstChild);

                while (div.children.length > 0) {
                    tagEl = div.firstElementChild;
                    if(tagEl && tagEl.tagName.toLowerCase() === "footer") {
                        el.insertBefore(tagEl, el.lastChild);
                    } else {
                        el.insertBefore(tagEl, el.firstChild);
                    }
                }
                // Add Container class
                fistDiv = document.getElementsByTagName("div");
                if(fistDiv && fistDiv.length) {
                    fistDiv[0].className = fistDiv[0].className + " has-header container";
                }

                // Check if have ViewPort set or create
                viewport = getViewport();
                if(viewport && viewport.setAttribute) {
                    viewport.setAttribute("content", vpContent);
                } else {
                    viewport = document.createElement("meta");
                    viewport.name = "viewport";
                    viewport.content = vpContent;
                    document.getElementsByTagName("head")[0].appendChild(viewport);
                }

                // Internal function
                function closeInAppBrowser () {
                    window.location.href = window.location.href + "&CloseIAB";
                }
                function getViewport () {
                    var metas = document.getElementsByTagName("meta"),
                        i;
                    for (i = 0; i < metas.length; i++) {
                        if (metas[i].getAttribute("name") === "viewport") {
                            return metas[i];
                        }
                    }
                    return null;
                }

                // Put global close
                window.closeInAppBrowser = closeInAppBrowser;
            }

        }

    }

    // Exports
    module.exports = inBrowserService;
})();
