/* global module */

// app/utils/encryptService.js
//
// encrypt service module for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var enco = require("crypto-js/enc-utf8"),
        aes = require("crypto-js/aes"),
        ld = require("lodash");

    encryptService.$inject = [
        "$window",
        "$Logger",
        "$Response",
        "CPS_CONFIG"
    ];
    function encryptService ($window, $Logger, $Response, CPS_CONFIG) {
        var logger = $Logger.getInstance(),
            config = CPS_CONFIG,
            separators = "AEIOU",
            alpha = "BCDFGHJKLMNPQRSTVWXYZ";

        // Members
        var Encrypt = {
            aes: aesEncrypt,
            encrypt: aesEncrypt,
            decrypt: aesDecrypt,
            genEncrypt: genEncrypt
        };

        return Encrypt;

        // Functions
        function aesEncrypt (data, key, opts) {
            var dataSave = data;
            opts = opts || {};

            try {
                if(ld.isObject(data)) {
                    dataSave = JSON.stringify(data);
                } else if(ld.isNumber(data)) {
                    dataSave = data.toString();
                }
            } catch(e) {
                // pass
            }
            if (ld.isObject(key)) {
                opts = key;
                key = null;
            }
            return aes.encrypt(dataSave, (key || config.appsecret), opts).toString();
        }

        function aesDecrypt (data, callback) {
            var dataReturn;

            callback = callback || ld.noop;
            try {
                dataReturn = aes.decrypt(data, config.appsecret).toString(enco);
            } catch(e) {
                $window.localStorage.clear();
                logger.warn("encrypt", "Inconsistent secret signature");
                callback($Response.make(498));
                return;
            }
            try {
                dataReturn = JSON.parse(dataReturn);
            } catch (e) {
                // pass
            }

            callback(null, dataReturn);
            return dataReturn;
        }

        function genEncrypt (data) {
            var key = new Date().getTime(),
                time = reverse(key) + "" + separators.charAt((Math.random() * separators.length)),
                encrypted = mask(time),
                rk,
                i;

            for(i = 0;i < data.length;i++) {
                rk = data.charCodeAt(i) * key;
                encrypted += mask(rk);
                encrypted += separators.charAt((Math.random() * separators.length));
            }
            return encrypted;
        }

        // Private
        function reverse (data) {
            var dat = data.toString(),
                rev = "",
                i;
            for(i = dat.length - 1;i >= 0;i--) {
                rev += dat.charAt(i);
            }
            return rev;
        }

        function mask (txt) {
            var pr = txt.toString(),
                mask2 = "",
                index,
                rand,
                next,
                j;

            for(j = 0;j < pr.length;j++) {
                next = pr.charAt(j);
                if(j % 2 === 0) {
                    index = pr.charAt(j);
                    next = alpha.charAt(index);
                    rand = (Math.random() * separators.length);
                    if(parseInt(rand) % 2 === 0) {
                        next = next.toLowerCase();
                    }
                }
                mask2 += next;
            }
            return mask2;
        }

    }

    // Exports
    module.exports = encryptService;

})();
