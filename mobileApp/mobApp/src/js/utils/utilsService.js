/* global module */

// utils/utilsService.js
//
// factory service with several utils
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    utilsService.$inject = [
        "$rootScope",
        "$locale",
        "$Mobile",
        "$Logger",
        "$Network",
        "AUTH_EVENTS"
    ];
    function utilsService ($rootScope, $locale, $Mobile, $Logger, $Network, AUTH_EVENTS) {
        var _ = $Mobile.locale,
            logger = $Logger.getInstance();

        // Members
        var Utils = {
            genDates: genDates,
            dateObject: dateObject,
            parseDate: parseDate,
            nextUid: nextUid,
            disconnectScope: disconnectScope,
            reconnectScope: reconnectScope,
            isScopeDisconnected: isScopeDisconnected,
            internalError: internalError,
            invalidTokes: invalidTokes,
            userBlocked: userBlocked
        };
        return Utils;

        // Functions
        /**
         * genDates
         *
         * Generates days, months and years
         * @param {string} type date type to get, days, years or months
         *
         * @return {array} with date object values, index, value and/or locale
         */
        function genDates (type) {
            $locale.id = "es-us";

            var date = new Date(),
                monthName = $locale.DATETIME_FORMATS.MONTH,
                getDateData = {},
                days = [],
                years = [],
                months = [],
                hours = [],
                dataReturn,
                monthValue,
                i;

            getDateData = {
                years: getYears,
                months: getMonths,
                days: getDays,
                hour: getHours
            }[type];

            if(getDateData) {
                dataReturn = getDateData();
            }

            // Create year
            function getYears () {
                for (i = date.getFullYear() - 1; i > 1900; i--) {
                    years.push({value: i});
                }
                return years;
            }
            // Create month
            function getMonths () {
                for(i = 1; i <= monthName.length;) {
                    monthValue = monthName[i - 1];
                    months.push({
                        value: (i > 9 ? i : "0" + i),
                        label: monthValue,
                        locale: ld.camelCase(monthValue)
                    });
                    i++;
                }
                return months;
            }
            // Days
            function getDays () {
                for(i = 1; i <= 31;) {
                    days.push({value: (i > 9 ? i : "0" + i), label: i});
                    i++;
                }
                return days;
            }
            // Hours
            function getHours () {
                for(i = 0; i < 24; i++) {
                    hours.push({value: String("0" + i + ":00").slice(-5)},
                               {value: String("0" + i + ":15").slice(-5)},
                               {value: String("0" + i + ":30").slice(-5)});
                }
                return hours;
            }

            return dataReturn;
        }

        function dateObject (date) {
            var newDate = date ? new Date(date) : new Date(),
                dateObj = {};
            if (newDate) {
                dateObj = ld.extend(dateObj, {
                    year: newDate.getFullYear(),
                    month:  String("0" + (newDate.getMonth() + 1)).slice(-2),
                    day: String("0" + newDate.getDate()).slice(-2),
                    hour: String(
                        "0" + newDate.getHours() +
                            (newDate.getMinutes() < 15 ? ":15" : ":30")
                    ).slice(-5)
                });
            }
            return dateObj;
        }

        function parseDate (dateObj, options) {
            var newDate = new Date(),
                dateString;
            options = ld.extend({withHour: true}, options);

            if(!ld.isEmpty(dateObj)) {
                dateString = (dateObj.year || newDate.getFullYear()) +
                    "/" + (dateObj.month || newDate.getMonth() + 1) +
                    "/" + (dateObj.day || newDate.getDay() + 1) +
                    (options.withHour ? " " + (dateObj.hour || newDate.getHours()) : "");
                newDate = new Date(dateString);
                if(options.asTime) {
                    newDate = newDate.getTime();
                }
            }
            return newDate;
        }


        function nextUid () {
            return ld.nextUid();
        }

        function disconnectScope (scope) {
            if (!scope) {
                return;
            }

            if (scope.$root === scope) {
                return; // we can't disconnect the root node;
            }
            var parent = scope.$parent;
            scope.$$disconnected = true;
            scope.$broadcast("$ionic.disconnectScope", scope);

            // See Scope.$destroy
            if (parent.$$childHead === scope) {
                parent.$$childHead = scope.$$nextSibling;
            }
            if (parent.$$childTail === scope) {
                parent.$$childTail = scope.$$prevSibling;
            }
            if (scope.$$prevSibling) {
                scope.$$prevSibling.$$nextSibling = scope.$$nextSibling;
            }
            if (scope.$$nextSibling) {
                scope.$$nextSibling.$$prevSibling = scope.$$prevSibling;
            }
            scope.$$nextSibling = scope.$$prevSibling = null;
        }

        function reconnectScope (scope) {
            if (!scope) {
                return;
            }

            if (scope.$root === scope) {
                return; // we can't disconnect the root node;
            }
            if (!scope.$$disconnected) {
                return;
            }
            var parent = scope.$parent;
            scope.$$disconnected = false;
            scope.$broadcast("$ionic.reconnectScope", scope);
            // See Scope.$new for this logic...
            scope.$$prevSibling = parent.$$childTail;
            if (parent.$$childHead) {
                parent.$$childTail.$$nextSibling = scope;
                parent.$$childTail = scope;
            } else {
                parent.$$childHead = parent.$$childTail = scope;
            }
        }

        function isScopeDisconnected (scope) {
            var climbScope = scope;
            while (climbScope) {
                if (climbScope.$$disconnected) {
                    return true;
                }
                climbScope = climbScope.$parent;
            }
            return false;
        }

        function internalError (data) {
            var req = data.req || {},
                res = data.res || {},
                statusCode = data.statusCode;

            if(!req.background) {
                $Mobile.alert(_("alertInternalError", {
                    code: statusCode || res.status || 500
                }));
            }
            // Send error event register to Analytics
            $Mobile.ga("send", "exception", "Error Request: " + statusCode + " - " + res.status +
                       " - network: " + $Network.getNetwork() +
                       " - onLine: " + $Network.onLine(), true);
            logger.warn("warn", "Error $http with status {0} and statusCode {1}", [ res.status, statusCode ]);
        }

        function invalidTokes (data) {
            var req = data.req,
                res = data.res,
                statusCode = data.statusCode;

            $rootScope.$emit(AUTH_EVENTS.invalidTokens, res);
            $Mobile.alert(_("alertInvalidToken"), data.desciption);
            // Register in Analytics
            $Mobile.ga("send", "event", "error", "InvalidToken: " + statusCode + " - " + res.status);
            logger.warn("utils", "Invalid tokens req: {0}", [JSON.stringify(req)]);
        }

        function userBlocked (data) {
            var req = data.req,
                res = data.res,
                statusCode = data.statusCode;

            $rootScope.$emit(AUTH_EVENTS.userBlocked, res);
            $Mobile.alert(_("alertUserBlock"), data.desciption);
            // Register in Analytics
            $Mobile.ga("send", "event", "error", "UserBlocked: " + statusCode + " - " + res.status);
            logger.warn("utils", "User blocked req: {0}", [JSON.stringify(req)]);
        }
    }

    // Exports
    module.exports = utilsService;

})();
