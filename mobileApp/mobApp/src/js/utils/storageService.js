/* global module */

// utils/storageService.js
//
// storages for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var localforage = require("localforage"),
        ld = require("lodash");

    storageService.$inject = [
        "$q",
        "$window",
        "CPS_CONFIG"
    ];
    function storageService ($q, $window, CPS_CONFIG) {
        var localStorage = $window.localStorage;
        localforage.config({
            name: ld.camelCase(CPS_CONFIG.appName)
        });

        // Members
        var Storage = ld.clone(localforage, true);
        Storage.getItem = getItem;
        Storage.setItem = setItem;
        Storage.removeItem = removeItem;
        Storage.clear = clear;

        return Storage;

        // Function
        function getItem (key, callback, migration) {
            var oldVal = migration ? localStorage.getItem(key) : false,
                deferred;
            if(ld.isBoolean(callback)) {
                migration = callback;
                callback = ld.noop;
            }
            if(oldVal) {
                deferred = $q.defer();
                callback = callback || ld.noop;
                try {
                    oldVal = JSON.parse(oldVal);
                } catch (e) {
                    // pass
                }
                localforage.setItem(key, oldVal).then(function (newVal) {
                    deferred.resolve(newVal);
                    callback(null, newVal);
                }).catch(function (err) {
                    deferred.reject(err);
                    callback(err);
                });
                return deferred.promise;
            }
            return localforage.getItem(key, callback);
        }

        function setItem (key, value, callback, migration) {
            migration = migration !== false; // Default save in localStorage for migration
            if(ld.isBoolean(callback)) {
                migration = callback;
                callback = ld.noop;
            }
            if(key == "us" && migration) {
                localStorage.setItem("ut", value.ut);
                localStorage.setItem("us", value.st);
            } else if(migration) {
                localStorage.setItem(key, (ld.isObject(value) ? JSON.stringify(value) : value));
            }
            return localforage.setItem(key, value, callback);
        }

        function removeItem (key, callback, migration) {
            migration = migration !== false; // Default save in localStorage for migration
            if(ld.isBoolean(callback)) {
                migration = callback;
                callback = ld.noop;
            }
            if(migration) {
                localStorage.removeItem(key);
            }
            return localforage.removeItem(key, callback);
        }

        function clear (callback, migration) {
            migration = migration !== false; // Default save in localStorage for migration
            if(ld.isBoolean(callback)) {
                migration = callback;
                callback = ld.noop;
            }
            if(migration) {
                localStorage.clear();
            }
            return localforage.clear(callback);
        }
    }

    // exports
    module.exports = storageService;
})();
