/* global module */

// utils/statusbarService.js
//
// show/hide/style statusbars for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var ld = require("lodash");

    statusbarService.$inject = [
        "$window",
        "$Dom",
        "$Body"
    ];
    function statusbarService ($window, $Dom, $Body) {
        var statusBar = $window.StatusBar,
            STYLES = {
                DEFAULT: 0,
                LIGHT_CONTENT: 1,
                BLACK_TRANSLUCENT: 2,
                BLACK_OPAQUE: 3
            },
            defaultOpts = {
                visible: true,
                style: 0,
                color: null,
                colorHex: null,
                overlaysWebView: false
            },
            showStatusBar;

        // Memebrs
        var Statusbar = {
            STYLES: STYLES,
            overlaysWebView: overlaysWebView,
            style: style,
            styleColor: styleColor,
            styleHex: styleHex,
            hide: hide,
            show: show,
            visible: visible,
            isVisible: isVisible,
            getDefaults: getDefaults,
            setDefaults: setDefaults
        };

        return Statusbar;

        // Functions

        /**
         * @param {boolean} bool
         */
        function overlaysWebView (bool) {
            if(statusBar) {
                return statusBar.overlaysWebView(!!bool);
            }
        }

        /**
         * @param {number} style
         */
        function style (style) {
            if(!statusBar) {
                return 0;
            }
            switch (style) {
                // Default
            case 0:
                return statusBar.styleDefault();

                // LightContent
            case 1:
                return statusBar.styleLightContent();

                // BlackTranslucent
            case 2:
                return statusBar.styleBlackTranslucent();

                // BlackOpaque
            case 3:
                return statusBar.styleBlackOpaque();

            default:
                return statusBar.styleDefault();
            }
        }

        // supported names:
        // black, darkGray, lightGray, white, gray, red, green,
        // blue, cyan, yellow, magenta, orange, purple, brown
        function styleColor (color) {
            if(statusBar) {
                return statusBar.backgroundColorByName(color);
            }
        }

        function styleHex (colorHex) {
            if(statusBar) {
                return statusBar.backgroundColorByHexString(colorHex);
            }
        }

        function hide () {
            $Dom.requestAnimationFrame(function () {
                $Body.addClass("status-bar-hide");
                if(statusBar) {
                    return statusBar.hide();
                }
            });
        }

        function show () {
            $Dom.requestAnimationFrame(function () {
                $Body.removeClass("status-bar-hide");
                if(statusBar) {
                    return statusBar.show();
                }

            });
        }

        /**
         * @name visible
         * @description Shows or hides the device status bar (in Cordova).
         * Requires `cordova plugin add org.apache.cordova.statusbar`
         * @param {boolean} shouldShow Whether or not to show the status bar.
         */
        function visible (shouldShow) {
            showStatusBar = shouldShow;

            if (showStatusBar) {
                // they do not want it to be full screen
                show();
            } else {
                // it should be full screen
                hide();
            }
        }


        function isVisible () {
            if(statusBar) {
                return statusBar.isVisible;
            }
        }

        function setDefaults (opts) {
            defaultOpts = ld.extend(defaultOpts, opts);

            if(ld.isBoolean(defaultOpts.overlaysWebView)) {
                overlaysWebView(defaultOpts.overlaysWebView);
            }
            if(ld.isBoolean(defaultOpts.visible)) {
                visible(defaultOpts.visible);
            }
            if(!ld.isNotdefined(defaultOpts.style)) {
                style(defaultOpts.style);
            }
            if(defaultOpts.color) {
                styleColor(defaultOpts.color);
            } else if(defaultOpts.colorHex) {
                styleHex(defaultOpts.colorHex);
            }
        }

        function getDefaults () {
            return defaultOpts;
        }
    }

    // Export
    module.exports = statusbarService;
})();
