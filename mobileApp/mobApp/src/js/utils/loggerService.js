/* global module */

// app/utils/loggerService.js
//
// logger provider module for cps mobile app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    function loggerService () {
        var isEnabled = true;

        // $get Injector
        $get.$inject = ["$log"];

        // Public methods set and get
        var LoggerServices = {
            enabled: enabled,
            $get: $get

        };
        return LoggerServices;

        // set enabled or disabled
        function enabled (_isEnabled) {
            isEnabled = !!_isEnabled;
        }

        // Getter
        function $get ($log) {
            // Members
            var Logger = {
                isEnabled: isEnabled,
                getInstance: getInstance
            };

            // set provider context
            var InitLogger = function (context) {
                this.context = context;
            };

            // Prototype
            /**
             * Logger
             * Prototype contruct logger function
             * @param {string} originalFn original function with message to print
             * @param {array} args arguments to send in message
             * @usage
             * logger.log('This is a log');
             * logger.warn('warn', 'This is a warn');
             * logger.error('This is a {0} error! {1}', [ 'big', 'just kidding' ]);
             * logger.debug('debug', 'This is a debug for line {0}', [ 8 ]);
             *
             * @return {string} print logger message in console
             */
            InitLogger.prototype = {
                _log: function (originalFn, args) {
                    if (!isEnabled) {
                        return;
                    }
                    var self = this,
                        now = getFormattedTimestamp(new Date()),
                        message = "", supplantData = [];

                    switch (args.length) {
                    case 1:
                        message = supplant("{0} - {1}: {2}", [ now, self.context, args[0] ]);
                        break;
                    case 3:
                        supplantData = args[2];
                        message = supplant("{0} - {1}::{2}(\"{3}\")", [ now, self.context, args[0], args[1] ]);
                        break;
                    case 2:
                        if (typeof args[1] === "string") {
                            message = supplant("{0} - {1}::{2}(\"{3}\")", [ now, self.context, args[0], args[1] ]);
                        } else {
                            supplantData = args[1];
                            message = supplant("{0} - {1}: {2}", [ now, self.context, args[0] ]);
                        }
                        break;
                    }
                    $log[originalFn].call(null, supplant(message, supplantData));
                },
                log: function () {
                    this._log("log", arguments);
                },
                info: function () {
                    this._log("info", arguments);
                },
                warn: function () {
                    this._log("warn", arguments);
                },
                debug: function () {
                    this._log("debug", arguments);
                },
                error: function () {
                    this._log("error", arguments);
                }
            };

            return Logger;

            // Functions
            function getInstance (context) {
                context = context === undefined ? "cpsApp" : context;
                return new InitLogger(context);
            }
            function supplant (str, o) {
                return str.replace(
                        /\{([^{}]*)\}/g,
                    function (a, b) {
                        var r = o[b];
                        return typeof r === "string" || typeof r === "number" ? r : a;
                    }
                );
            }
            function getFormattedTimestamp (date) {
                return supplant("{0}:{1}:{2}:{3}", [
                    date.getHours(),
                    date.getMinutes(),
                    date.getSeconds(),
                    date.getMilliseconds()
                ]);
            }

        }
    }

    // Exports
    module.exports = loggerService;

})();
