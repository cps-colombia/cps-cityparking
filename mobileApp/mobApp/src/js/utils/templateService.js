/* global require */

// utils/templateService.js
//
// factory service for create and loade templates
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    var angular = require("angular"),
        ld = require("lodash"),
        $$ = angular.element;

    templateService.$inject = ["$q", "$compile", "$controller", "$http", "$rootScope", "$templateCache"];
    function templateService ($q, $compile, $controller, $http, $rootScope, $templateCache) {

        // Members
        var TemplateLoader = {
            load: fetchTemplate,
            compile: loadAndCompile
        };
        return TemplateLoader;

        // Functions
        function fetchTemplate (url) {
            return $http.get(url, {cache: $templateCache})
                .then(function (response) {
                    return response.data && response.data.trim();
                });
        }

        function loadAndCompile (options) {
            options = angular.extend({
                template: "",
                templateUrl: "",
                scope: null,
                controller: null,
                controllerAs: null,
                locals: {},
                replaceTo: null,
                appendTo: null
            }, options);

            var templatePromise = options.templateUrl ?
                    fetchTemplate(options.templateUrl) :
                    $q.when(options.template);

            return templatePromise.then(function (template) {
                var controller,
                    scope = options.scope || $rootScope.$new(),
                    // Incase template doesn't have just one root element, do this
                    element = $$("<div>").html(template).contents();

                if (options.controller) {
                    if(options.controllerAs && options.controller.match(new RegExp(" as "))) {
                        options.controllerAs = null;
                    }
                    controller = $controller(
                        options.controller +
                            (options.controllerAs ? " as " + options.controllerAs : ""),
                        ld.extend(options.locals, {
                            $scope: scope
                        })
                    );
                    element.children().data("$ngControllerController", controller);
                }
                if (options.appendTo) {
                    $$(options.appendTo).append(element);
                } else if(options.replaceTo) {
                    $$(options.replaceTo).html(element);
                }

                $compile(element)(scope);

                return {
                    element: element,
                    scope: scope
                };
            });
        }
    }

    // Exports
    module.exports = templateService;

})();
