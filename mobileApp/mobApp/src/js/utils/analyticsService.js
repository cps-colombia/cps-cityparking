/* global module, GoogleAnalytics */

// utils/appService.js
//
// app provider module for cps mobile app
// core function to run app
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    analyticsService.$inject = [];
    function analyticsService () {
        // $get injector
        $get.$inject = ["$q", "$window", "$Logger", "$App"];

        // setter and getter
        var AnalyticsProvider = {
            analyticsId: "",
            $get: $get
        };
        return AnalyticsProvider;

        // Getter
        function $get ($q, $window, $Logger, $App, $Response) {
            var logger = $Logger.getInstance(),
                config = $App.config,
                analytics = $window.navigator && $window.navigator.analytics || GoogleAnalytics,
                // HitTypes  = analytics.HitTypes || {},
                LogLevel  = analytics.LogLevel || {},
                Fields    = analytics.Fields || {};

            // Members
            var Analytics = {
                startTracking: startTracking,
                setTrackingId: setTrackingId,
                setUserId: setUserId,
                debugMode: debugMode,
                trackView: trackView,
                trackEvent: trackEvent,
                trackException: trackException,
                trackTiming: trackTiming,
                addCustomDimension: addCustomDimension
            };
            return Analytics;

            // Functions
            function startTracking () {
                var deferred = $q.defer();

                if(AnalyticsProvider.analyticsId && $App.isCordova() && !config.debug) {
                    setTrackingId(AnalyticsProvider.analyticsId).then(function (res) {
                        deferred.resolve(res);
                    }).catch(function (err) {
                        deferred.resolve(err);
                    });
                } else {
                    deferred.resolve($Response.make(200));
                }
                return deferred.promise;
            }

            function setTrackingId (trackId) {
                var deferred = $q.defer(),
                    startTrackerWithId = analytics.setTrackingId || analytics.startTrackerWithId,
                    options;

                if(analytics.EasyTrackerConfig) {
                    logger.info("analytics", "Analytics WP Init");

                    options = new analytics.EasyTrackerConfig();
                    options.trackingId = trackId;
                    options.appName = config.displayName;
                    options.appVersion = config.version;
                    GoogleAnalytics.EasyTracker.current.config = options;

                    deferred.resolve(options);

                } else if(startTrackerWithId) {
                    startTrackerWithId(trackId, function (response) {
                        logger.info("analytics", "Analytics General Init");
                        deferred.resolve(response);
                    }, function (error) {
                        logger.warn("analytics", "Error Analytics General Init: {0}", [JSON.stringify(error)]);
                        deferred.reject(error);
                    });

                } else {
                    logger.info("analytics", "Analytics no have function to set trackerId");
                    deferred.reject($Response.make(500));
                }

                return deferred.promise;
            }

            function setUserId (userId) {
                var deferred = $q.defer(),
                    params = {};

                if(analytics.setUserId) {
                    analytics.setUserId(userId, function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.reject(error);
                    });
                } else if(analytics.send) {
                    params[Fields.USER_ID]  = userId;
                    analytics.send(params, function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.reject(error);
                    });
                } else {
                    deferred.reject($Response.make({description: "setUserId unsupported"}));
                }

                return deferred.promise;
            }

            function debugMode () {
                var deferred = $q.defer();

                if(analytics.debugMode) {
                    analytics.debugMode(function (response) {
                        deferred.resolve(response);
                    }, function () {
                        deferred.reject();
                    });
                } else if(analytics.setLogLevel) {
                    analytics.setLogLevel(LogLevel.VERBOSE, function (response) {
                        deferred.resolve(response);
                    }, function (err) {
                        deferred.reject(err);
                    });
                } else {
                    deferred.reject($Response.make({description: "debugMode unsupported"}));
                }

                return deferred.promise;
            }

            function trackView (screenName) {
                var deferred = $q.defer(),
                    trackViewFn = analytics.sendAppView || analytics.sendView || analytics.trackView;

                if(trackViewFn) {
                    trackViewFn(screenName, function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.reject(error);
                    });
                } else {
                    deferred.reject($Response.make({description: "trackView unsupported"}));
                }

                return deferred.promise;
            }

            function trackEvent (category, action, label, value) {
                var deferred = $q.defer(),
                    trackEventFn = analytics.sendEvent || analytics.trackEvent;

                if(trackEventFn) {
                    trackEventFn(category, action, label, value, function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.reject(error);
                    });
                } else {
                    deferred.reject($Response.make({description: "trackEvent unsupported"}));
                }

                return deferred.promise;
            }

            function trackException (description, fatal) {
                var deferred = $q.defer(),
                    trackExceptionFn = analytics.sendException || analytics.trackException;

                if(trackExceptionFn) {
                    trackExceptionFn(description, fatal, function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.reject(error);
                    });
                } else {
                    deferred.reject($Response.make({description: "trackException unsupported"}));
                }
                return  deferred.promise;
            }

            function trackTiming (category, milliseconds, variable, label) {
                var deferred = $q.defer(),
                    trackTimingFn = analytics.sendTiming || analytics.trackTiming;

                if(trackTimingFn) {
                    analytics.trackTiming(category, milliseconds, variable, label, function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.reject(error);
                    });
                } else {
                    deferred.reject($Response.make({description: "trackTiming unsupported"}));
                }

                return deferred.promise;
            }

            function addCustomDimension (key, value) {
                var deferred = $q.defer(),
                    customDimensionFn = analytics.customDimension || analytics.addCustomDimension;

                if(customDimensionFn) {
                    analytics.addCustomDimension(key, value, function () {
                        deferred.resolve();
                    }, function (error) {
                        deferred.reject(error);
                    });
                } else {
                    deferred.reject($Response.make({description: "customDimension unsupported"}));
                }

                return deferred.promise;
            }

        }
    }

    // Exports
    module.exports = analyticsService;

})();
