/* global module, Connection*/

// utils/network/networkService.js
//
// provide factory service for network module
//
// 2015, CPS - Cellular Parking Systems

(function () {
    "use strict";

    networkService.$inject = [
        "$q",
        "$sce",
        "$window",
        "$document",
        "$timeout",
        "$http",
        "$rootScope",
        "$App",
        "$Logger"
    ];
    function networkService ($q, $sce, $window, $document, $timeout, $http, $rootScope, $App, $Logger) {

        var logger = $Logger.getInstance(),
            networkState = $window.navigator.connection ? $window.navigator.connection : $window.navigator,
            ipAddress;

        // Events
        $App.ready(function () {
            $document[0].addEventListener("offline", offlineEvent, false);
            $document[0].addEventListener("online", onlineEvent, false);
            getIPAddress();
        });

        // Members
        var Network = {
            getNetwork: getNetwork,
            isOnline: isOnline,
            isOffline: isOffline,
            onLine: onLine,
            hasInternet: hasInternet,
            offlineEvent: offlineEvent,
            clearOfflineWatch: clearOfflineWatch,
            clearOnlineWatch: clearOnlineWatch,
            setIPAddress: setIPAddress,
            getIPAddress: getIPAddress,
            getIP: getIP
        };
        return Network;

        // Functions
        function getNetwork () {
            return networkState.type;
        }

        function isOnline () {
            if(networkState.type) {
                return networkState.type !== Connection.UNKNOWN && networkState.type !== Connection.NONE;
            } else {
                return networkState.onLine;
            }
        }

        function isOffline () {
            if(networkState.type) {
                return networkState.type === Connection.UNKNOWN || networkState.type === Connection.NONE;
            } else {
                return !networkState.onLine;
            }
        }

        function onLine () {
            return $window.navigator.onLine;
        }

        function hasInternet () {
            return !isOffline() || onLine();
        }

        function offlineEvent () {
            $timeout(function () {
                $rootScope.$emit("cps:network:offline", networkState.type);
            });
        }

        function onlineEvent () {
            $timeout(function () {
                $rootScope.$emit("cps:network:online", networkState.type);
            });
        }

        function clearOfflineWatch () {
            $document[0].removeEventListener("offline", offlineEvent);
            $rootScope.$$listeners["cps:network:offline"] = [];
        }

        function clearOnlineWatch () {
            $document[0].removeEventListener("online", onlineEvent);
            $rootScope.$$listeners["cps:network:online"] = [];
        }

        function setIPAddress (ip) {
            ipAddress = ip;
        }

        function getIPAddress () {
            if($window.networkinterface) {
                $window.networkinterface.getIPAddress(function (ip) {
                    logger.debug("network", "IP Client: {0}", [ip]);
                    ipAddress = ip;
                }, function (err) {
                    logger.warn("network", "error to get IP: {0}, call get fallback IP", [err]);

                    fallbackGetIP().then(function (ipFallback) {
                        ipAddress = ipFallback;
                    });
                });
            } else {
                fallbackGetIP().then(function (ipFallback) {
                    ipAddress = ipFallback;
                });
            }
        }

        function fallbackGetIP () {
            var deferred = $q.defer();
            var ipFallback;
            var url = $sce.trustAsResourceUrl("https://api.ipify.org?format=jsonp");

            $http.jsonp(url).then(function (res) {
                ipFallback = res.data ? res.data.ip : null;
                deferred.resolve(ipFallback);
                logger.debug("network", "Fallback, IP Client: {0}", [ipFallback]);
            }).catch(function (err) {
                deferred.reject(err);
                logger.warn("network", "Fallback, error to get IP: {0}", [err]);
            });

            return deferred.promise;
        }

        function getIP () {
            return ipAddress;
        }

    }
    // Exports
    module.exports = networkService;

})();
