// CPS cli installer project app
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var path = require("path"),
        ld = require("lodash"),
        shell = require("shelljs"),
        asyncJs = require("async"),
        cpsUtil = require("./util"),
        gruntCli = require("./grunt"),
        Plugin = require("./plugin"),
        Platform = require("./platform");

    function cliInstall (options, callback) {
        var rootPath = cpsUtil.rootPath(),
            parentPath = cpsUtil.parentPath(),
            jsonConfBase = path.resolve(rootPath, "./templates/cpsConfig_example.json"),
            jsonConfDest = path.resolve(parentPath, "./cpsConfig.json"),
            jsonConfRoot = path.resolve(rootPath, "./cpsConfig.json"),
            xmlConfBase = path.resolve(rootPath, "./templates/config_base.xml"),
            xmlConfDest = path.resolve(rootPath, "./config.xml"),
            symlinkInstall = ["platforms", "plugins", "www", "res"],
            dataCopyJson = {},
            platforms = [],
            config = {},
            msgLog = "",
            argv;

        callback = callback || ld.noop;
        options = ld.extend({}, options);

        // Set config
        ld.extend(config, options.config);

        argv = options.argv;
        // Set platforms
        platforms = argv._.slice(1);

        // Start tasks
        asyncJs.series([
            function (callback) {
                console.info("CPS installer starts...\n".info);
                // Copye base files
                cpsUtil.copyFiles({src: xmlConfBase, dest: xmlConfDest}, function (err) {
                    if(err) {
                        console.error(err);
                        console.error("Error to copy config file".error);
                        callback(err);
                    } else {
                        console.log("Success, Copy base config.xml file".help);

                        if(shell.which(jsonConfDest)) {
                            msgLog = "Success, Read JSON config file";
                            dataCopyJson = {src: jsonConfDest, dest: jsonConfRoot};
                        } else {
                            msgLog = "Success, Copy JSON example config file";
                            dataCopyJson = {src: jsonConfBase, dest: jsonConfDest};
                        }

                        cpsUtil.copyFiles(dataCopyJson, function (errJson) {
                            if(errJson) {
                                console.error(errJson);
                                console.error("Error to copy JSON example config file".error);
                                callback(err);
                            } else {
                                console.info(msgLog.info);
                                callback(null, jsonConfDest);
                            }
                        });
                    }
                });
            },
            function (callback) {
                if(!cpsUtil.isNpm()) {
                    // Check dependencies
                    shell.exec("npm install" , function (code, output) {
                        if(!cpsUtil.errorHandler(code)) {
                            console.info("Success, 'npm install' for check new dependencies".info);
                            callback(null, output);
                        } else {
                            callback({code: code, error: output});
                        }
                    });
                } else {
                    callback(null, "is NPM package");
                }
            }, function (callback) {
                // Generate executable script file
                cpsUtil.generateExec(function (err, res) {
                    callback(err, res);
                });
            }, function (callback) {
                // Run install grunt task
                gruntCli.run("install", {async: false}, function (err, res) {
                    if (err) {
                        callback(err);
                    } else {
                        console.info("\nCreating Symlink.\n".info);
                        asyncJs.each(symlinkInstall, function (pathName, next) {

                            shell.ln("-s", path.join(parentPath, pathName), path.join(rootPath, pathName));
                            err = shell.error();
                            if(err) {
                                console.error("Error Creating Symlink '" + pathName + "' -> " + err + " ".error);
                                next({code: 6, error: err});
                            } else {
                                console.log("Creating Symlink. " + path.relative(rootPath, pathName) + " ...OK".debug);
                                next(null, pathName);
                            }

                        }, function (errSym) {
                            callback(errSym, res);
                        });
                    }
                });
            }, function (callback) {
                if(argv.base) {
                    return callback();
                }
                // Install or update platforms
                if(platforms && platforms.length) {
                    console.info("\nCheck project Platforms".info);
                    new Platform(config.cordovaPlatforms, {
                        platforms: platforms
                    }).install(function (err, res) {
                        if(err) {
                            console.log("Some errors in platform install".warn);
                            callback({code: 6, error: err});
                        } else {
                            console.log("Platforms install success".help);
                            callback(null, res);
                        }
                    });
                } else {
                    callback(null);
                }
            }, function (callback) {
                if(argv.base) {
                    return callback();
                }
                // Install plugins
                console.info("\nCheck project Plugins".info);
                new Plugin(config.cordovaPlugins).install({
                    platforms: platforms
                }, function (err, res) {
                    if(err) {
                        console.log("Some errors in plugin install".warn);
                        callback({code: 6, error: err});
                    } else {
                        console.log("Plugins install success".help);
                        callback(null, res);
                    }
                });
            }
        ],function (err, results) {
            callback(err, results);
        });
    }

    // Exports
    module.exports = {
        launch: cliInstall
    };
})();
