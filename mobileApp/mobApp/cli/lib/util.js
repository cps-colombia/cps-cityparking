// management project cli utils
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var fs = require("fs"),
        util = require("util"),
        path = require("path"),
        ld = require("lodash"),
        colors = require("colors"),
        cordovaLib = require("cordova-lib"),
        cordovaCommon = require("cordova-common"),
        packageJson = require("../../package.json");

    // Members
    var cpsUtil = {
        events: cordovaLib.events,
        binname: cordovaLib.binname,
        logger: cordovaCommon.CordovaLogger.get(),
        PlatformJson: cordovaCommon.PlatformJson,
        CordovaError: cordovaLib.CordovaError,
        ConfigParser: cordovaLib.ConfigParser,
        PluginInfo: cordovaLib.PluginInfo,
        isWindows: process.platform === "win32",
        isMac: process.platform === "darwin",
        isLinux: process.platform === "linux",
        version: version,
        isNpm: isNpm,
        colors: cliColors,
        packageConfig: packageConfig,
        cpsConfig: cpsConfig,
        appPackage: appPackage,
        rootPath: rootPath,
        cordovaBin: cordovaBin,
        parentPath: parentPath,
        parsePath: parsePath,
        buildCmd: buildCmd,
        cmdName: cmdName,
        errorHandler: errorHandler,
        copyFiles: copyFiles,
        generateExec: generateExec,
        createSymlink: createSymlink
    };

    // Functions
    function version () {
        console.log(packageJson.version + "\n".info);
    }

    function isNpm () {
        try {
            return fs.statSync(path.resolve(rootPath(), "../../package.json")) ? true : false;
        } catch(e) {
            return false;
        }
    }

    function cliColors () {
        colors.setTheme({
            silly: "rainbow",
            input: "grey",
            verbose: "cyan",
            prompt: "grey",
            info: "green",
            data: "grey",
            help: "blue",
            warn: "yellow",
            debug: "white",
            error: "red"
        });
        return colors;
    }

    function packageConfig () {
        return packageJson || {};
    }

    function cpsConfig (filePath) {
        var configfile = filePath || path.resolve(parentPath(), "./cpsConfig.json");
        var configJson = {};
        var appPackageJson = appPackage();

        // Cps Project config
        if (fs.existsSync(configfile)) {
            try {
                configJson = ld.extend(configJson, JSON.parse(fs.readFileSync(configfile, "utf8")));

                // Remove others enviroments
                let enviroment = configJson.env ? configJson.env :
                    (ld.has(configJson, "debug") ?
                     (configJson.debug && "debug" || "dist") :
                     process.env.NODE_ENV);

                ld.forEach(configJson, (value, key) => {
                    if (ld.has(value, enviroment)) {
                        configJson[key] = ld.pick(value, enviroment);
                    }
                });

                // Set values from packageJson
                if (!ld.isEmpty(appPackageJson)) {
                    configJson.versionPkg = !configJson.versionPkg ? appPackageJson.version : configJson.versionPkg;
                    configJson.appName = !configJson.appName ? appPackageJson.name : configJson.appName;
                }
                return configJson;
            } catch (err) {
                console.error(err);
                throw new cpsUtil.CordovaError("Error to parse cpsConfig.json file".warn);
            }
        } else {
            console.info("No have cpsConfig.json file".info);
            return configJson;
        }
    }

    function appPackage () {
        var packagefile = path.resolve(parentPath(), "./package.json");
        var appPackageJson = {};
        // App package JSON
        if (fs.existsSync(packagefile)) {
            try {
                appPackageJson = ld.extend(appPackageJson, JSON.parse(fs.readFileSync(packagefile, "utf8")));
                return appPackageJson;
            } catch (err) {
                console.error(err);
                throw new cpsUtil.CordovaError("Error to parse package.json file".warn);
            }
        } else {
            return appPackageJson;
        }
    }

    function rootPath () {
        return path.resolve(path.dirname(__dirname), "../");
    }

    function cordovaBin () {
        return path.resolve(rootPath(), "./node_modules/.bin/cordova");
    }

    function parentPath (relative) {
        var parent = isNpm() ? "../.." : "..";

        if(relative) {
            return parent;
        }
        return path.resolve(rootPath(), parent + "/");
    }

    function parsePath (fullPath) {
        // fix windows path with quoted, so it reads: c:\"Program Files"\mobApp\bin/cps
        if (cpsUtil.isWindows && fullPath.indexOf(" ") !== -1) {
            return ld.map(fullPath.split("\\"), function (part) {
                if (part.indexOf(" ") !== -1) {
                    return "\"" + part.replace(/"/g, "\\\"") + "\"";
                }
                return part;
            }).join("\\");
        }
        return fullPath;
    }

    function buildCmd (command, argv) {
        return [parsePath(command)].concat(argv).join(" ").trim();
    }

    function cmdName (cmdLine) {
        var command = cmdLine.split(" ");
        return ld.compact(ld.pullAt(command, 2, 3), undefined).join("_");
    }

    function errorHandler (err, options) {
        var hasError = false;
        var message, code;

        options = ld.extend({
            exit: true
        }, options);

        if (ld.isObject(err)) {
            message = err.message;
            code = err.code || 6;
        } else {
            code = err;
        }

        if(code !== null && code !== 0) {
            hasError = true;
            if (message) {
                console.log(message);
            }
            console.error("Error: CPS failed".error);
            console.error("Exit code:".data, code);
            if(options.exit) {
                process.exit(1);
            }
        }
        return hasError;
    }

    function copyFiles (options, callback) {
        var originFile, destFile;

        originFile = fs.createReadStream(options.src);
        originFile.on("error", function (err) {
            callback(err);
        });
        destFile = fs.createWriteStream(options.dest);
        destFile.on("error", function (err) {
            callback(err);
        });
        destFile.on("close", function (ex) {
            callback(null, ex);
        });

        // CopyFile
        originFile.pipe(destFile);
    }

    function generateExec (callback) {
        var binPath = path.relative(parentPath(), path.join(rootPath(), "bin/cps")),
            note = "This file is autogenerated, any change will be overwritten",
            comment = "Script to run 'cps' cli framework",
            legals = "2015, CPS - Cellular Payment Systems",
            errMsg = "ERROR: Could not find 'cps' script in 'bin' folder, aborting...>&2",
            unixScript = "#!/bin/bash \n# %s \n# %s \n# \n# %s \n" +
            "set +v \n" +
            "path_script=\"`dirname \"$0\"`/%s\" \n" +
            "if [ -e $path_script ]; then\n" +
            "    node $path_script $@ \n" +
            "else \n" +
            "    echo \n" +
            "    echo %s \n" +
            "    exit 1 \n" +
            "fi",
            winScript = ":: %s \n:: %s \n:: \n:: %s \n" +
            "@ECHO OFF \n" +
            "SET path_script=\"%~dp0%s\"\n" +
            "IF EXIST %path_script% ( \n" +
            "    node %path_script% %* \n" +
            ") ELSE ( \n" +
            "    ECHO. \n" +
            "    ECHO %s \n" +
            "    EXIT /B 1 \n" +
            ")",
            scriptName = "cps",
            scriptFile = unixScript,
            pathScript;

        if(cpsUtil.isWindows) {
            scriptName = "cps.bat";
            scriptFile = winScript;
        }
        callback = callback || ld.noop;
        scriptFile = util.format(scriptFile, note, comment, legals, binPath, errMsg);
        pathScript = path.join(parentPath(), scriptName);

        fs.writeFile(pathScript, scriptFile, function (err) {
            if (err) {
                console.error(err);
                return callback(new cpsUtil.CordovaError("Error to write script file: " + pathScript + "".error));
            }
            fs.chmod(pathScript, "755", function (err) {
                if(err) {
                    console.log(err);
                    return callback(err);
                }
                console.log("Generate exec script: " + pathScript + "".debug);
                callback(null, pathScript);
            });
        });
    }

    function createSymlink (target, dest) {
        fs.stat(dest, function (err, exist) {
            if(!exist) {
                fs.symlink(target, dest, function (err, res) {
                    if(err) {
                        console.log(err);
                    }
                });
            }
        });
    }

    // Enable colors
    cliColors();

    // Exports
    module.exports = cpsUtil;

})();
