// Run grunt commands in local environment
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var path = require("path"),
        ld = require("lodash"),
        shell = require("shelljs"),
        cpsUtil = require("./util");

    function runGrunt (gruntArgv) {
        var args = arguments,
            gruntBin = gruntCli(),
            options = {async:false},
            callback = args[2] || ld.noop;

        // change to bin directory
        shell.cd(cpsUtil.rootPath());

        // Set params
        if(ld.isObject(args[1])) {
            options = ld.extend(options, args[1]);
        } else if(ld.isFunction(args[1])) {
            callback = args[1];
        }

        // Enable CLI color
        cpsUtil.colors();

        // Check if grunt exist
        if (!shell.which(gruntBin)) {
            shell.echo("Sorry, please install grunt-cli for use cpsApp".warn);
            shell.exit(1);
        }

        // TODO: user pure grunt module
        shell.exec(cpsUtil.buildCmd(gruntBin, gruntArgv.concat(" --color")), options, function (code, output) {
            if (!cpsUtil.errorHandler(code, {exit: true})) {
                console.info("Grunt CPS Finish".debug);
                callback(null, output);
            } else {
                callback({code: code, error: output});
            }
        });
    }

    function gruntCli () {
        return path.resolve(cpsUtil.rootPath(), "./node_modules/.bin/grunt");
    }

    // Exports
    module.exports = {
        run: runGrunt,
        cli: gruntCli
    };
})();
