// Clean project from build artifacts
// or folder before install
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path"),
        ld = require("lodash"),
        shell = require("shelljs"),
        asyncLib = require("async"),
        cordova = require("cordova"),
        cpsUtil = require("./util"),

        cordovaLib = cordova.cordova_lib,
        CordovaError = cordovaLib.CordovaError;

    function cliClean (options, callback) {
        var known_platforms = ld.keys(cordovaLib.cordova_platforms),
            rootPath = cpsUtil.rootPath(),
            parentPath = cpsUtil.parentPath(),
            platformsPath,
            badPlatforms;

        callback = callback || ld.noop;
        options = ld.extend({
            argv: {}
        }, options);

        // Check platforms
        if(fs.existsSync(path.resolve(rootPath, "./mobApp/platforms"))) {
            platformsPath = options.platformsPath = path.resolve(rootPath, "./mobApp/platforms");
        } else if(fs.existsSync(path.resolve(parentPath, "./platforms"))) {
            platformsPath = options.platformsPath = path.resolve(parentPath, "./platforms");
        }
        badPlatforms = ld.difference(options.platforms, known_platforms);

        if(!platformsPath) {
            callback(new CordovaError("Not have platforms directory"));
            return;
        }

        if(options.platforms[0] == "all") {
            options.cleanAll = true;
            options.platforms = fs.readdirSync(platformsPath).filter(function (platform) {
                if(fs.statSync(path.join(platformsPath, platform)).isDirectory()) {
                    return platform;
                }
            });
            badPlatforms = [];
        }

        if(!ld.isEmpty(badPlatforms)) {
            callback(new CordovaError("Unknown platforms: " + badPlatforms.join(", ")));
            return;
        }

        // Start task by option args
        if(options.argv.full) {
            cleanBuilds(options, function (err, res) {
                if(!err) {
                    cleanProject(options, function (perr, pres) {
                        callback(perr, pres);
                    });
                } else {
                    callback(err, res);
                }
            });
        } else if(options.argv.builds) {
            cleanBuilds(options, function (err, res) {
                callback(err, res);
            });
        } else if(options.argv.project) {
            cleanProject(options, function (err, res) {
                callback(err, res);
            });
        } else {
            callback(new CordovaError("Nothing to clean, need options --builds, --project or --all"));
            return;
        }
    }

    function cleanProject (options, callback) {
        var rootPath = cpsUtil.rootPath(),
            parentPath = cpsUtil.parentPath(),
            directoryClean = {
                ios: [
                    "platform_www/", "www/", "cpsApp/Plugins/",
                    "cpsApp/Resources/*.bundle", "ios.json", "frameworks.json"
                ],
                android: [
                    "platform_www/","assets/www/", "src/",
                    "android.json", "project.properties"
                ]
            },
            pluginsPath = [],
            deletedFiles = [],
            excludePath = [],
            onlyRemove,
            pluginFile,
            packageId;

        options = ld.extend({}, options);

        if(options.platformsPath) {
            asyncLib.each(options.platforms, function (platform, next) {
                // Check if have anything to clean
                if(directoryClean[platform]) {
                    // Exlude some paths by packageId (useful for android)
                    if(options.config && options.config.platform && options.config.platform[platform]) {
                        packageId = options.config.platform && options.config.platform[platform].packageId || "";
                        ld.map(packageId.split("."), function (value) {
                            excludePath.push(value);
                        });
                    }
                    // Set plugins path
                    pluginsPath.push(path.resolve(parentPath, "./plugins"));
                    pluginsPath.push(path.resolve(rootPath, "./plugins"));

                    // Start clean
                    asyncLib.each(directoryClean[platform], function (toClean, cbClean) {
                        var cleanVal = toClean.split("*");
                        toClean = path.resolve(options.platformsPath, "./" + platform, "./" + cleanVal[0]);

                        if(!fs.existsSync(toClean)) {
                            cbClean();
                            return;
                        }

                        if(fs.statSync(toClean).isFile()) {
                            removeFile(toClean);
                            cbClean();
                        } else {
                            fs.readdir(toClean, function (err, files) {
                                // Get sub files/directories
                                if(files && files.length) {
                                    asyncLib.each(files, function (file, cbFiles) {

                                        if(ld.indexOf(excludePath, file) <= -1) {
                                            file = path.join(toClean, file);

                                            if(fs.statSync(file).isFile()) {
                                                // If file copy backup
                                                console.log("Backup file: ".debug, ld.last(file.split("/")));
                                                shell.cp("-f", file, file + ".back");
                                            }

                                            // Check if remove only some files
                                            onlyRemove = cleanVal.length > 1 ? ld.last(cleanVal) : null;
                                            if(onlyRemove && onlyRemove.length) {
                                                if(!file.match(onlyRemove)) {
                                                    file = null; // Not remove file
                                                }
                                            }

                                            if(file) {
                                                deletedFiles.push(file);
                                                removeFile(file);
                                            }

                                            cbFiles();
                                        } else {
                                            cbFiles();
                                        }
                                    }, function (err) {
                                        cbClean(err);
                                    });
                                } else {
                                    cbClean();
                                }
                            });
                        }
                    }, function (err) {
                        if(!err) {
                            pluginFile = path.resolve(pluginsPath[0], "./" + platform + ".json");
                            if(fs.existsSync(pluginFile)) {
                                shell.mv("-f", pluginFile, pluginFile + ".back");
                            }
                            next();
                        } else {
                            next(err);
                        }
                    });
                } else {
                    next();
                }
            }, function (err) {
                if(err) {
                    callback(err);
                } else {
                    if(options.cleanAll) {
                        // Remove and create backup plugins
                        if(fs.existsSync(pluginsPath[0])) {
                            console.log("Remove plugin path".debug);
                            shell.mv("-f", pluginsPath[0], pluginsPath[0] + ".back");
                            shell.rm("-rf", pluginsPath[0]);
                        }
                        // Remove symlink
                        if(fs.existsSync(pluginsPath[1])) {
                            shell.rm("-rf", pluginsPath[1]);
                        }
                    }
                    console.log((deletedFiles.length + " cleansed").help);
                    callback(null, deletedFiles);
                }
            });
        } else {
            callback(new CordovaError("no platforms, could not perform cleanup"));
            return;
        }

    }

    function cleanBuilds (options, callback) {
        var cordovaBin = cpsUtil.cordovaBin();
        shell.exec(cordovaBin + " " + options.argvString, function (code, output) {
            if(!cpsUtil.errorHandler(code, {exit: true})) {
                console.info("Clean Project Finish".debug);
                callback(null, output);
            } else {
                callback({code: code, error: output});
            }
        });
    }

    function removeFile (filePath) {
        if(fs.existsSync(filePath)) {
            console.log("Remove: ".debug, ld.last(filePath.split("/")));
            shell.rm("-rf", filePath);
        }
        return filePath;
    }

    // Exports
    module.exports = cliClean;

})();
