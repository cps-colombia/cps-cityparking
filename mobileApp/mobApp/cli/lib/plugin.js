// management project plugins
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path"),
        q = require("q"),
        ld = require("lodash"),
        shell = require("shelljs"),
        cordova = require("cordova"),
        cordovaUtil = require("cordova-lib/src/cordova/util"),
        cordovaConfig = require("cordova-lib/src/cordova/config"),
        CordovaHooksRunner = require("cordova-lib/src/hooks/HooksRunner"),
        cordovaPlugman = require("cordova-lib/src/plugman/plugman"),
        plugmanMetadata = require("cordova-lib/src/plugman/util/metadata"),
        cordovaPluginMapper  = require("cordova-registry-mapper").newToOld,
        cpsUtil = require("./util"),
        packageJson = require("../../package.json"),

        CordovaError = cpsUtil.CordovaError,
        ConfigParser = cpsUtil.ConfigParser,
        events = cpsUtil.events;

    /**
     * @constructor
     */
    function CpsPlugin (extraPlugins) {
        // change to root directory
        shell.cd(cpsUtil.rootPath());

        // Set local vars
        this.projectRoot = cordovaUtil.cdProjectRoot();
        this.pluginPath = path.join(this.projectRoot, "plugins");
        this.installedPlugins = getPlugins();
        this.plugins = ld.union(packageJson.cordovaPlugins, extraPlugins) || [];
        this.errors = null;

        return this;
    }

    CpsPlugin.exec = function (command, targets, opts, callback) {
        var cordovaBin = cpsUtil.cordovaBin();
        var cordovaPlugins = opts.config && opts.config.cordovaPlugins;
        var cpsPlugin = new CpsPlugin(cordovaPlugins);
        var i;

        if (!targets || !targets.length) {
            if (command == "add" || command == "rm" || command == "remove") {
                callback(new CordovaError(
                    "You need to qualify `" + cordovaUtil.binname +
                        " plugin add` or `" + cordovaUtil.binname +
                        " plugin remove` with one or more plugins!"));
            } else {
                targets = [];
            }
        }

        opts = opts || {};
        if (opts.length) {
            // This is the case with multiple targets as separate arguments and opts is not opts but another target.
            targets = Array.prototype.slice.call(arguments, 1);
            opts = {};
        }
        if (!Array.isArray(targets)) {
            // This means we had a single target given as string.
            targets = [targets];
        }


        opts.options = opts.options || [];
        opts.plugins = [];

        // Split targets between plugins and options
        // Assume everything after a token with a '-' is an option
        for (i = 0; i < targets.length; i++) {
            if (targets[i].match(/^-/)) {
                opts.options = targets.slice(i);
                break;
            } else {
                opts.plugins.push(targets[i]);
            }
        }

        switch(command) {
        case "install":
        case "uninstall":
            cpsPlugin[command](opts, callback);
            break;
        case "add":
        case "update":
            cpsPlugin[command](targets, opts, callback);
            break;
        case "rm":
        case "remove":
            cpsPlugin.remove(targets, opts, callback);
            break;
        default:
            shell.exec(cpsUtil.buildCmd(cordovaBin, opts.argv), callback);
        }
    };

    CpsPlugin.prototype.add = function (targets, opts, callback) {
        // rewrite for select single platform
        var self = this;
        var _listPlatforms = cordovaUtil.listPlatforms(self.projectRoot);
        var platformsTarget = opts.platforms && opts.platforms.length ? opts.platforms : null;

        if (platformsTarget) {
            // ungly way to prevent install plugin in all plataforms
            shell.cd(path.join(self.projectRoot, "platforms"));
            ld.remove(_listPlatforms, function (_platform) {
                return platformsTarget.indexOf(_platform) > -1;
            });
            // move platform directory for disabled
            _listPlatforms.map(function (_platform) {
                shell.mv("-n", _platform, _platform + ".disable");
            });
            shell.cd(cpsUtil.rootPath());
        }

        if (opts.collection !== true) {
            var targetString = [];
            targets = ld.reduce(targets, function (results, plugin) {
                if (ld.isObject(plugin)) {
                    results.push(plugin);
                } else {
                    targetString.push(plugin);
                }
                return results;
            }, []);
            targets.push(targetString.join(" "));
        }

        self.plugins = targets;
        self.command = "add";

        self.processPlugin(0, function (err, res) {
            if (platformsTarget) {
                // restore platforms directory
                shell.cd(path.join(self.projectRoot, "platforms"));
                _listPlatforms.map(function (_platform) {
                    shell.mv("-n", _platform + ".disable", _platform);
                });
                // back to root project
                shell.cd(cpsUtil.rootPath());
            }
            (callback || ld.noop)(err, res);
        });
    };

    CpsPlugin.prototype.remove = function (targets, opts, callback) {
        var self = this;
        var shouldRunPrepare = false;
        var shouldUninstall = true;
        var hooksRunner = new CordovaHooksRunner(self.projectRoot);
        var configJson = cordovaConfig.read(self.projectRoot);
        var platformList = cordovaUtil.listPlatforms(self.projectRoot);
        var platformTarget = opts.platforms && opts.platforms.length ? opts.platforms : platformList;

        // Massage plugin name(s) / path(s)
        var pluginPath = path.join(self.projectRoot, "plugins");
        var plugins = cordovaUtil.findPlugins(pluginPath);

        // console.log(plugins);
        // console.log(self.installedPlugins);
        // Rewrite remove for support single platform
        if (!targets || !targets.length) {
            throw new CordovaError(
                "No plugin specified. Please specify a plugin to remove. See `" +
                    cordovaUtil.binname + " plugin list`.");
        }
        opts.cordova = { plugins: plugins };
        return hooksRunner.fire("before_plugin_rm", opts).then(function () {
            return opts.plugins.reduce(function (soFar, target) {
                var validatedPluginId = validatePluginId(target, plugins);
                if (!validatedPluginId && opts.throw !== false) {
                    throw new CordovaError(
                        "Plugin \"" + target + "\" is not present in the project. See `" +
                            cordovaUtil.binname + " plugin list`.");
                }
                target = validatedPluginId;

                // Iterate over all installed platforms and uninstall.
                // If this is a web-only or dependency-only plugin, then
                // there may be nothing to do here except remove the
                // reference from the platform's plugin config JSON.
                return platformTarget.reduce(function (soFar, platform) {
                    return soFar.then(function () {
                        var platformRoot = path.join(self.projectRoot, "platforms", platform);
                        var platformJson = cpsUtil.PlatformJson.load(pluginPath, platform);
                        var platformPlugins = platformJson.root.installed_plugins || {};
                        var options = {
                            force: opts.force || false
                        };

                        events.emit("verbose", "Calling plugman.uninstall on plugin \"" + target +
                                    "\" for platform \"" + platform + "\"");

                        if (!platformPlugins[target] && opts.throw !== false) {
                            throw new CordovaError(
                                "Plugin \"" + target + "\" not installed for " + platform +
                                    " platforms. See `" + cordovaUtil.binname + " plugin list`.");
                        }

                        return cordovaPlugman.raw.uninstall.uninstallPlatform(
                            platform, platformRoot,
                            target, pluginPath, options
                        ).then(function (didPrepare) {
                            // If platform does not returned anything we'll need
                            // to trigger a prepare after all plugins installed}
                            if (!didPrepare) {
                                shouldRunPrepare = true;
                            }
                        });
                    });
                }, q()).then(function () {
                    return platformList.reduce(function (soFar, platform) {
                        return soFar.then(function () {
                            var platformJson = cpsUtil.PlatformJson.load(pluginPath, platform);
                            var platformPlugins = platformJson.root.installed_plugins || {};

                            if (platformPlugins[target]) {
                                // only uninstall plugin if any platform has enabled
                                shouldUninstall = false;
                            }
                        });
                    }, q()).then(function () {
                        // only uninstallPlugin when no platforms have it.
                        return !shouldUninstall ? q() :
                            cordovaPlugman.raw.uninstall.uninstallPlugin(target, pluginPath, opts);
                    });
                }).then(function () {
                    // remove plugin from config.xml
                    if(saveToConfigXmlOn(configJson, opts)) {
                        events.emit("log", "Removing plugin " + target + " from config.xml file...");
                        var configPath = cordovaUtil.projectConfig(self.projectRoot);
                        if(fs.existsSync(configPath)) {// should not happen with real life but needed for tests
                            var configXml = new ConfigParser(configPath);
                            configXml.removePlugin(target);
                            configXml.write();
                        }
                    }
                }).then(function () {
                    // Remove plugin from fetch.json
                    events.emit("verbose", "Removing plugin " + target + " from fetch.json");
                    plugmanMetadata.remove_fetch_metadata(pluginPath, target);
                });
            }, q());
        }).then(function () {
            // CB-11022 We do not need to run prepare after plugin install until shouldRunPrepare flag is set to true
            if (!shouldRunPrepare) {
                return q();
            }

            return cordova.raw.prepare.preparePlatforms(platformTarget, self.projectRoot, opts)
        }).then(function () {
            opts.cordova = { plugins: cordovaUtil.findPlugins(pluginPath) };
            return hooksRunner.fire("after_plugin_rm", opts).then(function (res) {
                callback(null, res);
                return q();
            }, function (err) {
                callback(err);
                return q();
            });
        }, function (err) {
            callback(err);
            return q();
        });
    };

    CpsPlugin.prototype.install = function (opts, callback) {
        if (ld.isFunction(opts)) {
            callback = opts;
            opts = {};
        }
        opts = ld.extend({
            collection: true
        }, opts);

        if(this.plugins) {
            this.add(this.plugins, opts, callback);
        }
    };

    CpsPlugin.prototype.update = function (targets, opts, callback) {
        var self = this;
        var plugins = targets && targets.length ? targets : self.plugins;

        if (ld.isFunction(opts)) {
            callback = opts;
            opts = {};
        }

        if(!ld.isArray(self.plugins)) {
            this.plugins = [self.plugins];
        }

        targets = ld.map(plugins, function (plugin) {
            return createPluginCommand("rm", getPluginData(self.plugins, plugin), {
                cmd: false
            });
        });
        opts.plugins = targets;
        opts.throw = false;

        self.remove(targets, opts, function (err, res) {
            targets = ld.map(plugins, function (plugin) {
                return createPluginCommand("add", getPluginData(self.plugins, plugin), {
                    cmd: false
                });
            });
            self.add(targets, opts, callback);
        });
    };

    CpsPlugin.prototype.upgrade = function (opts, callback) {
        // TODO: Implement
    };

    CpsPlugin.prototype.uninstall = function (opts, callback) {
        if(this.plugins) {
            var targets = ld.map(self.plugins, function (plugin) {
                return createPluginCommand("rm", getPluginData(targets, plugin), {
                    cmd: false
                });
            });
            opts.plugins = targets;
            opts.throw = false;
            this.remove(targets, opts, callback);
        }
    };


    CpsPlugin.prototype.processPlugin = function (index, callback, options) {
        var self = this;
        var pluginCommand;
        var plugin;

        callback = callback || ld.noop;

        if (!ld.isArray(self.plugins)) {
            self.plugins = [self.plugins];
        }
        if(index >= self.plugins.length) {
            callback((self.errors ? {
                code: 6,
                errors: self.errors
            } : null), {
                last: true,
                index: index,
                plugins: self.plugins,
                command: self.command
            });
            return;
        }

        options = ld.extend({}, options);

        plugin = self.plugins[index];
        pluginCommand = createPluginCommand(self.command, plugin);

        if (self.plugins.length > 1) {
            console.log(pluginCommand.split("cordova ")[1].debug);
        }

        shell.exec(pluginCommand, {async: true}, function (code, output) {
            if(cpsUtil.errorHandler(code, {exit: false})) {
                if(!ld.isArray(self.errors)) {
                    self.errors = [];
                }
                self.errors.push({
                    code: code,
                    output: output
                });
            }
            if(options.every) {
                callback(self.errors, {
                    index: index,
                    plugin: plugin,
                    command: self.command
                });
            }
            // Add new plugin
            self.processPlugin(index + 1, callback);
        });
    };

    /**
     * @private
     */
    function getPlugins () {
        var plugins = [];
        if(!cordova.findProjectRoot()) {
            console.warn("Invalid project path".warn);
            cpsUtil.errorHandler(6);
            return plugins;
        }
        plugins = cpsUtil.PluginInfo.loadPluginsDir(
            path.resolve(cordova.findProjectRoot(), "./plugins")
        ) || [];

        return plugins;
    }


    function getPluginData (plugins, id) {
        var pluginData;
        ld.forEach(plugins, function (plugin, index) {
            if(ld.isString(plugin) && plugin.replace(/-/g, ".") == id) {
                pluginData = plugin;
                return;
            } else if(ld.isObject(plugin) && (plugin.id == id || plugin.name == id)) {
                pluginData = plugin;
                return;
            }
        });
        return pluginData || id;
    }

    function createPluginCommand (command, plugin, opts) {
        var pluginCmd = opts && opts.cmd === false ? "" :
            cpsUtil.buildCmd(cpsUtil.cordovaBin(), ["plugin", command]) + " ";
        var isLocator = false;
        var location;

        if(ld.isString(plugin)) {
            if (command === "rm") {
                plugin = plugin.split("@")[0];
            }
            pluginCmd += plugin;
        } else {
            if(plugin.id || plugin.location) {
                if(plugin.location) {
                    location = plugin.location;
                    isLocator = true;
                } else {
                    location = plugin.id;
                }

                if(command === "add") {
                    pluginCmd += location;
                    pluginCmd += plugin.spec ? (isLocator ? "#" : "@") + plugin.spec + " " : " ";

                    if(plugin.variables) {
                        Object.keys(plugin.variables).forEach(function (variable) {
                            pluginCmd += "--variable " + variable + "='" + plugin.variables[variable] + "' ";
                        });
                    }
                } else {
                    pluginCmd += (plugin.id || plugin.name).split("@")[0];
                }
            } else {
                console.warn("Plugin config need id or location".warn);
            }
        }

        return pluginCmd;
    }

    function validatePluginId (pluginId, installedPlugins) {
        if (installedPlugins.indexOf(pluginId) >= 0) {
            return pluginId;
        }

        var oldStylePluginId = cordovaPluginMapper[pluginId];
        if (oldStylePluginId) {
            events.emit("log", "Plugin \"" + pluginId +
                        "\" is not present in the project. Checking for legacy id \"" +
                        oldStylePluginId + "\".");
            return installedPlugins.indexOf(oldStylePluginId) >= 0 ? oldStylePluginId : null;
        }

        if (pluginId.indexOf("cordova-plugin-") < 0) {
            return validatePluginId("cordova-plugin-" + pluginId, installedPlugins);
        }
    }

    function saveToConfigXmlOn (config_json, options) {
        options = options || {};
        var autosave =  config_json.auto_save_plugins || false;
        return autosave || options.save;
    }

    // Export
    module.exports = CpsPlugin;

})();
