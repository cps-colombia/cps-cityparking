// management project platfoms
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var fs = require("fs"),
        path = require("path"),
        ld = require("lodash"),
        shell = require("shelljs"),
        mkdirp = require("mkdirp"),
        asyncJs = require("async"),
        cpsUtil = require("./util"),
        packageConfig = require("../../package.json");

    /**
     * @constructor
     */
    function CpsPlatform (extraPlatforms, options) {
        var self = this,
            platforms = [];
        // change to bin directory
        shell.cd(cpsUtil.rootPath());
        options = ld.extend({}, options);

        // Platforms
        self.platforms = unionPlatforms(packageConfig.cordovaPlatforms, extraPlatforms);
        if(options.platforms && options.platforms.indexOf("all") <= -1) {
            ld.forEach(options.platforms, function (platform, index) {
                platform = getPlatform(self.platforms, platform);
                if(platform) {
                    platforms.push(platform);
                }
            });
            self.platforms = platforms;
        }
        self.installedPlatforms = getPlatforms();
        self.errors = null;

        return this;
    }

    CpsPlatform.prototype.install = function (callback) {
        var self = this;
        if(self.platforms) {
            self.command = "add";
            self.processPlatform(callback);
        }
    };

    CpsPlatform.prototype.processPlatform = function (callback) {
        var self = this,
            limit = 1,
            platformCommand;

        asyncJs.eachLimit(self.platforms, limit, function (platform, cb) {
            if(self.installedPlatforms[getPlatformName(platform)] && self.command == "add") {
                self.command = "update";
            }
            platformCommand = createPlatformCommand(self.command, platform);

            console.log(platformCommand.split("cordova ")[1].help);

            asyncJs.series([
                function (next) {
                    if(self.command == "update") {
                        beforeProcess(platform, function (err, res) {
                            if(err) {
                                self.errors.push(err);
                                next(err);
                            } else {
                                next(null, res);
                            }
                        });
                    } else {
                        next();
                    }
                }, function (next) {
                    shell.exec(platformCommand, {async: true}, function (code, output) {
                        if(cpsUtil.errorHandler(code, {exit: false})) {
                            if(!ld.isArray(self.errors)) {
                                self.errors = [];
                            }
                            self.errors.push(output);
                        }
                        next(); // not stop task if have error
                    });
                }
            ], function (err, results) {
                cb(err, results);
            });

        }, function (err) {
            callback(self.errors); // return internal error if have one
        });
    };

    function getPlatform (platforms, platformName) {
        var dataReturn = platformName;
        ld.forEach(platforms, function (dataPlatform, key) {
            if((ld.isObject(dataPlatform) && dataPlatform.id == platformName || dataPlatform.name == platformName) ||
               key == platformName || ld.indexOf(dataPlatform, platformName) > -1) {
                dataReturn = dataPlatform;
                return;
            }
        });
        return dataReturn;
    }

    function getPlatforms () {
        var rootPath = cpsUtil.rootPath(),
            platformfFile = path.resolve(rootPath, "./platforms/platforms.json"),
            installPlatforms = {};

        // Get platforms
        if (fs.existsSync(platformfFile)) {
            try {
                installPlatforms = ld.extend(installPlatforms, JSON.parse(fs.readFileSync(platformfFile)));
            } catch (err) {
                console.error(err);
            }
        }
        return installPlatforms;
    }

    function getPlatformName (platform) {
        if(ld.has(platform, "id") || ld.has(platform, "name")) {
            return platform.id || platform.name;
        } else if(ld.isObject(platform)) {
            return ld.keys(platform)[0];
        } else {
            return platform;
        }
    }

    function getPlatformData (platform) {
        return {
            id: platform.id || platform.name,
            spec: (platform.spec || platform.version) + ""
        };
    }

    function unionPlatforms (platforms, extendPlatforms) {
        extendPlatforms = extendPlatforms || [];
        ld.forEach(extendPlatforms, function (addPlatform) {
            addPlatform = getPlatformData(addPlatform);
            ld.forEach(platforms, function (_platform, index) {
                _platform = getPlatformData(_platform);
                if(_platform.id !== addPlatform.id) {
                    extendPlatforms.push(_platform);
                }
            });
        });

        return extendPlatforms.length ? extendPlatforms : platforms;
    }

    function beforeProcess (platform, callback) {
        var platformName = getPlatformName(platform),
            rootPath = cpsUtil.rootPath(),
            xmlConfig = path.resolve(rootPath, "./config.xml"),
            pathPlatform = {
                android: {
                    config: "res/xml/"
                },
                ios: {
                    config: "cpsApp/"
                }
            },
            filesList = [],
            results = [],
            xmlConfigDest;

        callback = callback || ld.noop;

        if (!fs.existsSync(xmlConfig)) {
            xmlConfig = path.resolve(rootPath, "./config_base.xml");
        }

        // Copy XML config by platform
        if(ld.has(pathPlatform[platformName], "config")) {
            xmlConfigDest = path.resolve(rootPath, "./platforms/" + platformName, "./" +
                                         pathPlatform[platformName].config,  "./config.xml");

            // Create path if not exist
            if(!fs.existsSync(path.dirname(xmlConfigDest))) {
                mkdirp.sync(path.dirname(xmlConfigDest));
            }

            filesList.push({
                src: xmlConfig,
                dest: xmlConfigDest
            });
        }

        asyncJs.each(filesList, function (dataFile, next) {
            cpsUtil.copyFiles({
                src: dataFile.src,
                dest: dataFile.dest
            }, function (err, res) {
                if(err) {
                    console.error(err);
                    console.error(("Error to copy " + dataFile.src + " to " + dataFile.dest).error);
                    next(err);
                } else {
                    console.info(("Success, Copy " + path.basename(dataFile.dest) +
                                  "file for " + platformName).debug);
                    results.push(res);
                    next(null);
                }
            });
        }, function (err) {
            callback(err, results);
        });
    }

    function createPlatformCommand (command, platform) {
        var platformCmd = cpsUtil.buildCmd(cpsUtil.cordovaBin(), ["platform", command]) + " ",
            isLocator = false,
            location;

        if(ld.isString(platform)) {
            platformCmd += platform;
        } else if(ld.has(platform, "id") || ld.has(platform, "location")) {
            if(platform.location) {
                location = platform.location;
                isLocator = true;
            } else {
                location = platform.id;
            }

            if(command === "add" || command == "update") {
                platformCmd += location;
                platformCmd += platform.spec ? (isLocator ? "#" : "@") + platform.spec + " " : " ";

                if(platform.variables) {
                    Object.keys(platform.variables).forEach(function (variable) {
                        platformCmd += "--variable " + variable + "='" + platform.variables[variable] + "' ";
                    });
                }
            } else {
                platformCmd += platform.id;
            }
        } else {
            console.warn("Platform config need id or location".warn);
        }

        return platformCmd;
    }

    // Export
    module.exports = CpsPlatform;

})();
