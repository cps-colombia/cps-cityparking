// bin to run cordova commands in local environment
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var ld = require("lodash"),
        yargs = require("yargs"),
        shell = require("shelljs"),
        cordova = require("cordova"),
        gruntCli = require("./lib/grunt"),
        cpsUtil = require("./lib/util"),
        cpsClean = require("./lib/clean"),
        cpsInstall = require("./lib/install"),
        cpsPlugin = require("./lib/plugin"),

        logger = cpsUtil.logger,
        events = cpsUtil.events;

    function runCps (processArgv) {
        var rootPath = cpsUtil.rootPath();
        var cordovaBin = cpsUtil.cordovaBin();
        var args = yargs.parse(processArgv.slice(2));
        var unparsedArgs = processArgv.slice(2,10).join(" ").replace(/\$|,/g, " ");
        var config = cpsUtil.cpsConfig();
        var undashed = args._ || [];
        var command = undashed[0];
        var subcommand = undashed[1];
        var targets = undashed.slice(2) || [];
        var opts = {
            options: [],
            config: config,
            argv: unparsedArgs,
            verbose: args.verbose || false,
            silent: args.silent || false,
            searchpath : args.searchpath,
            noregistry : args.noregistry,
            nohooks : args.nohooks,
            browserify: args.browserify || false,
            fetch: args.fetch || false,
            link: args.link || false,
            save: args.save || false,
            shrinkwrap: args.shrinkwrap || false,
            force: args.force || false
        };
        var cliVars = {};


        // Enabled CLI colors
        cpsUtil.colors();

        // change to bin directory
        shell.cd(rootPath);

        // Logger
        logger.subscribe(events);
        if (args.silent) {
            logger.setLevel("error");
        }

        if (args.verbose) {
            logger.setLevel("verbose");
        }

        // Basic commands
        if ((args.version || args.v) && !args._.length) {
            console.log("CPS Mobile Framework mobApp".help);
            cpsUtil.version();

            console.log("Cordova".help);
            cordova.cli(processArgv);

            return;
        } else if (args.grunt) {
            // Run grun tasks
            gruntCli.run(unparsedArgs.replace("--grunt", ""));
            return;
        }

        if (args.variable) {
            if (!ld.isArray(args.variable)) {
                args.variable = [args.variable];
            }
            args.variable.forEach(function (s) {
                // CB-9171
                console.log(s);
                var eq = s.indexOf("=");
                if (eq == -1) {
                    throw new cpsUtil.CordovaError("invalid variable format: " + s);
                }
                var key = s.substr(0, eq).toUpperCase();
                var val = s.substr(eq + 1, s.length);
                cliVars[key] = val;
            });
        }
        opts.cli_variables = cliVars;

        // Pass nopt-parsed args to PlatformApi through opts.options
        opts.options = args;

        // Check if cordovaBin exist
        if (!shell.which(cordovaBin)) {
            console.error("Sorry, please install cordovaBin for use cpsApp".error);
            process.exit(1);
        }

        switch(command) {
        case "devel":
            // Run grun devel mode
            gruntCli.run(command);
            break;
        case "clean":
            cpsClean({
                argv: args,
                argvString: unparsedArgs,
                config: config,
                platforms: undashed.slice(1) // All options without dashes are assumed to be platform names
            }, function (err, res) {
                if(err) {
                    console.warn("CPS Clean Error".warn);
                    cpsUtil.errorHandler((err || 6));
                } else {
                    console.info("CPS Clean Finish".info);
                }
            });
            break;
        case "install":
            // Install
            cpsInstall.launch({
                config: config,
                argv: args
            }, function (err, res) {
                if(err) {
                    cpsUtil.errorHandler((err.code || 6));
                } else {
                    console.info("CPS Install Finish".info);
                }
            });
            break;
        case "plugin":
        case "plugins":
            opts.platforms = args.platforms ? args.platforms.split(",") : null;
            cpsPlugin.exec(subcommand, targets, opts, function (err, res) {
                cpsUtil.errorHandler(err);
                console.info("Cordova CPS Finish".info);
            });
            break;
        default:
            shell.exec(cpsUtil.buildCmd(cordovaBin, unparsedArgs), function (code, output) {
                cpsUtil.errorHandler(code);
                console.info("Cordova CPS Finish".info);
            });
            break;
        }

        // Events

        // For CordovaError print only the message without stack trace unless we
        // are in a verbose mode.
        process.on("uncaughtException", function (err) {
            logger.error(err);
            process.exit(1);
        });
    }

    // Exports
    module.exports = {
        runCps: runCps
    };

})();
