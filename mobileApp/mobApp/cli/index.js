// Expose cli CPS
//
// 2015, CPS - Cellular Payment Systems

(function () {
    "use strict";

    var cpsCli = require("./cli");

    module.exports = cpsCli;
})();
