CPS Mobile Framework
==============================
**Version 2.1.0**
[![build status](http://104.155.190.166/cps/mobApp/badges/develop/build.svg)](http://104.155.190.166/cps/mobApp/commits/develop)
[![coverage report](http://104.155.190.166/cps/mobApp/badges/develop/coverage.svg)](http://104.155.190.166/cps/mobApp/commits/develop)

CPS Framework for diferentes mobile project, it uses the system modules with angular.js for more scalability and socket system with socket.io.

Usage
-------
Cps Mobile Framework has its own way of starting and compiling, first concat and minfied js, css etc.
then starts a hook for assign id and display name by platform
and copy the file to platfom.

All 'cordova' commands are inherited and others own.

The Basic commands are:

```shell
./cps run [platform]
```

Install
---------

### Requirements ###
- Node.js v4.x or higher

### For new project ###

Create and enter to project directory
```shell
mkdir cps_project && cd cps_project
```

Add git submodule
```shell
git submodule add git@96.45.176.18:cps/mobApp.git
```

### For existing project ####

Enter to project directory
```shell
cd cps_project
```

init, sync and update git submodules

```shell
git submodule init
git submodule sync
git submodule update
```

### Run cps install ###
```shell
mobApp/bin/cps install
```
This install dependencies with 'npm' and create symlink to 'cps' executable in parent directory and necessary basic directories

```shell
./cps prepare
```
Compile and copy files, If you have no platform adds default platforms

```shell
./cps platform update [platform]@[version]
```
If exist version, update for create files by platform


### Commands ###

#### Devel ####
`cps devel`
Concat, minfied, copy and watch for change, useful for development and test in browser

#### Prepare ####
`cps prepare [platform]`
Concat, minfied and copy file to platform

#### Build ####
`cps build [platform]`
Compile and buid platform and generates the package

#### Run ####
`cps run [platform]`
prepare, build and push app in device or emulator

### Rest Example ###
To see all the available queries and example see [CPS.rest](http://96.45.176.18:4000/cps/mobApp/blob/master/CPS.rest)

### CHANGELOG ###
To see visible changes recently visit [CHANGELOG](http://96.45.176.18:4000/cps/mobApp/blob/master/CHANGELOG.md)
