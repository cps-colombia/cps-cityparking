(() => {
    "use strict";

    const appAsync = require("helpers/main-test").appAsync;

    // Mocked login response
    const LOGIN_SUCCESS = {
        "statusCode": 200,
        "description": "Success",
        "data": {
            "login": "usertest"
        }
    };

    describe("$Login service", () => {
        var $httpBackend, $Login, $Encrypt, ops;

        beforeEach((done) => {
            angular.mock.module("cpsMain", "cpsLogin");

            inject((_$httpBackend_, _$Encrypt_, _$Login_, WS_OP) => {
                $httpBackend = _$httpBackend_;
                $Login = _$Login_;
                $Encrypt = _$Encrypt_;
                ops = WS_OP;
            });
            appAsync(done);
        });

        it("should exist", () => {
            expect($Login).toBeDefined();
        });

        describe("initLogin()", () => {
            var result;

            beforeEach(() => {
                // Initialize our local result object to an empty object before each test
                result = {};
                // Spy on our service call but allow it to continue to its implementation
                spyOn($Login, "initLogin").and.callThrough();
            });


            it("should have a return promise success when user login", (done) => {
                let user = "usertest";
                let pass = $Encrypt.genEncrypt("cps");

                // Verified constant
                expect(ops.loginSend).toBeDefined();

                // Login send endpoint with mocked return values
                $httpBackend.whenPOST(new RegExp("op=" + ops.loginSend)).respond(200, LOGIN_SUCCESS);

                expect($Login.initLogin).not.toHaveBeenCalled();
                expect(result).toEqual({});

                $Login.initLogin(user, pass).then((res) => {
                    result = res.data;

                    expect(result.statusCode).toEqual(200);
                    expect(result.data.login).toEqual(user);
                    done();
                });
                // Flush pending HTTP requests
                $httpBackend.flush();

                expect($Login.initLogin).toHaveBeenCalledWith(user, pass);
            });
        });

    });
})();
