(() => {
    "use strict";

    function appAsync (done) {
        inject((_$timeout_, _$httpBackend_, _$App_) => {
            var $timeout = _$timeout_;
            var $httpBackend = _$httpBackend_;
            var $App = _$App_;
            var ops = $App.ops;

            $httpBackend.whenPOST(new RegExp("op=" + ops.baseData)).respond({});
            $httpBackend.whenJSONP(/ipify/).respond({});

            $App.ready(() => {
                setTimeout(done, 100);
            });
            $timeout.flush();
        });
    }

    function beforeEachApp () {
        beforeEach((done) => {
            appAsync(done);
        });
    }

    module.exports = {
        appAsync: appAsync,
        beforeEachApp: beforeEachApp
    };
})();
