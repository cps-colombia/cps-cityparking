(() => {
    "use strict";

    const ld = require("lodash");
    const appAsync = require("helpers/main-test").appAsync;


    const REGISTER_SUCCESS = {
        "statusCode":200,
        "description":"Success",
        "data":{
            "idCityDriver":"293"
        }
    }
    // const LIST_SUCCESS = {
    //     "statusCode":200,
    //     "description":"Success",
    //     "data":{
    //         "drivers":[{
    //             "idCityDriver":293,
    //             "sysUser":{
    //                 "idsysUser":1111,
    //                 "address":"Calle siempre viva 1233",
    //                 "balance":49900.00,
    //                 "birthDate":"ago 23, 2014",
    //                 "cu":"47707",
    //                 "email":"cquimbayo@cpsparking.ca",
    //                 "idsysUserType":2,
    //                 "lastName":"Prueba2",
    //                 "login":"cquimbayo",
    //                 "name":"Camilo",
    //                 "favoriteMsisdn":"3178795959","status":"a",
    //                 "dateReg":"Aug 25, 2014 9:08:59 AM",
    //                 "emailNotification":1
    //             },
    //             "contactName":"andres",
    //             "contactLastName":"pereira",
    //             "contactPhone":"72626262",
    //             "addressOrigin":"Colombia Mayor - Ac. 24 #43A-34, Bogotá, Colombia",
    //             "addressDest":"Unicentro Bogotá - Cra. 15 #124-30, Bogotá, Cundinamarca, Colombia",
    //             "plate":"345sdf",
    //             "documents":1,
    //             "dateService":"Nov 24, 2016 10:30:00 AM",
    //             "state":1,
    //             "idCityStatus":{
    //                 "idCityStatus":336,
    //                 "stops":0,
    //                 "stopsPrice":"0.0",
    //                 "dateReg":"Nov 24, 2016 10:28:19 AM",
    //                 "citydriverOperators":[]
    //             },
    //             "comments":"",
    //             "dateReg":"Nov 24, 2016 10:28:19 AM",
    //             "fee":0.0,
    //             "pocket":{
    //                 "pocketId":2,
    //                 "description":"paymentez",
    //                 "createdDate":"Sep 29, 2016 12:26:15 PM",
    //                 "pocketType": {
    //                     "idPocketType":3,
    //                     "description":"Créditos"
    //                 },
    //                 "estado":"A",
    //                 "accountNumber":"5699000",
    //                 "createdBy":871
    //             },
    //             "referenceId":"4736519837142017906"
    //         }]
    //     }
    // }
    describe("$Driver service", () => {
        var $Driver, $httpBackend, ops;

        beforeEach((done) => {
            // Before each test load cpsMain module
            angular.mock.module("cpsMain", "cpsDriver");

            // Before each test set our injected $Driver to local variable
            inject((_$Driver_, _$httpBackend_, WS_OP) => {
                $Driver = _$Driver_;
                $httpBackend = _$httpBackend_;
                ops = WS_OP;
            });
            appAsync(done);
        });

        // A simple test to verify the Driver service exists
        it("should exist", () => {
            expect($Driver).toBeDefined();
        });

        describe("register()", () => {
            var dataRequest = {
                plate: "345sdf",
                phone: 3178795959,
                name: "Camilo",
                last_name: "Quimbayo",
                display_name: "Camilo Quimbayo",
                contact_name: "Andres",
                contact_last_name: "Pereira",
                contact_phone: 72626262,
                date_service: 1480001400000,
                orig: "Calle 53 #43A-34, Bogotá, Colombia",
                dest: "Unicentro Bogotá - Cra. 15 #124-30, Bogotá, Cundinamarca, Colombia",
                comments: "hello world",
                pocket_id: 2,
                pocket_reference: 4736519837142017906,
                documents: 1,
                legals: true,
                rtm: true,
                soat: true,
                stops: 0
            }
            var result;

            beforeEach(() => {
                // Initialize our local result object to an empty object before each test
                result = {};
                // Spy on our service call but allow it to continue to its implementation
                spyOn($Driver, "register").and.callThrough();
            });


            it("should have a return promise success in new service", (done) => {
                // Verified constant
                expect(ops.driverService).toBeDefined();

                // Login send endpoint with mocked return values
                $httpBackend.whenPOST(new RegExp("op=" + ops.driverService)).respond(200, REGISTER_SUCCESS);

                expect($Driver.register).not.toHaveBeenCalled();
                expect(result).toEqual({});

                $Driver.register(dataRequest).then((res) => {
                    result = res;
                    expect(ld.isObject(result)).toBeTruthy();
                    expect(ld.has(res, "data")).toBeTruthy();
                    expect(result.statusCode).toEqual(200);
                    expect(result.data.idCityDriver).toEqual("293");
                    done();
                });
                // Flush pending HTTP requests
                $httpBackend.flush();

                expect($Driver.register).toHaveBeenCalledWith(dataRequest);
            });
        });
    });

})();
