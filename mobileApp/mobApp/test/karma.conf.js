/* global  module*/
// Karma configuration
// Generated on Wed Feb 17 2016 17:32:26 GMT-0500 (COT)

(function () {
    "use strict";

    var path = require("path");

    function karmaConfig (config) {
        config.set({

            // base path that will be used to resolve all patterns (eg. files, exclude)
            basePath: "",


            // frameworks to use
            // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
            frameworks: ["browserify", "jasmine"],


            // list of files / patterns to load in the browser
            files: [
                "../src/js/core/cps.js",
                "../src/js/**/index.js",
                "./helpers/ng-mock.js",
                "./**/*.spec.js"
            ],


            // list of files to exclude
            exclude: ["../src/js/libs/**/*.js"],

            // preprocess matching files before serving them to the browser
            // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
            preprocessors: {
                "../src/js/core/cps.js": [ "browserify" ],
                "../src/js/**/index.js": [ "browserify" ],
                "./helpers/ng-mock.js": [ "browserify" ],
                "./**/*.spec.js": [ "browserify" ]
            },

            // test results reporter to use
            // possible values: "dots", "progress"
            // available reporters: https://npmjs.org/browse/keyword/karma-reporter
            reporters: ["spec", "coverage"],


            // web server port
            port: 9876,


            // enable / disable colors in the output (reporters and logs)
            colors: true,


            // level of logging
            // possible values: config.LOG_DISABLE || config.LOG_ERROR ||
            // config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
            logLevel: config.LOG_INFO,


            // enable / disable watching file and executing tests whenever any file changes
            autoWatch: true,


            // start these browsers
            // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
            browsers: ["PhantomJS"],
            // to avoid DISCONNECTED messages
            browserDisconnectTimeout: 10000, // default 2000
            browserDisconnectTolerance: 1, // default 0
            browserNoActivityTimeout: 100000, // default 10000


            // Continuous Integration mode
            // if true, Karma captures browsers, runs the tests and exits
            singleRun: false,

            // Concurrency level
            // how many browser should be started simultaneous
            concurrency: Infinity,

            // browserify configuration
            browserify: {
                debug: true,
                transform: [
                    ["browserify-babel-istanbul", {
                        // instrumenterConfig: { embedSource: true }
                    }],
                    ["babelify", {
                        presets: ["es2015"], compact: false
                    }],
                    ["pkgify", {
                        packages: {
                            helpers: "./helpers"
                        }, relativeTo: __dirname
                    }],
                    ["../scripts/browserify-cpsconfig.js", {
                        cpsConfig: path.resolve(__dirname, "./cpsConfig_test.json")
                    }]
                ]
            },
            // coverage reporter
            coverageReporter: {
                dir: "../coverage/",
                reporters: getCoverageReporters(config)
            }
        });
    }

    function getCoverageReporters (config) {
        var reporters = [
            { type: "text-summary"}
        ];
        if (config.coverageReporters) {
            var coverageReportersAvailable = {
                html: { type: "html" },
                text: { type: "text", file: "report-text.txt" }
            };
            config.coverageReporters.split(",").forEach(function (reporter) {
                if (coverageReportersAvailable[reporter]) {
                    reporters.push(coverageReportersAvailable[reporter]);
                }
            });
        }
        return reporters;
    }

    // Exports
    module.exports = karmaConfig;
})();
