:: bin to run cps framework commands
::
:: 2015, CPS - Cellular Payment Systems
@ECHO OFF
SET script_path="%~dp0cps"
IF EXIST %script_path% (
node %script_path% %*
) ELSE (
ECHO.
ECHO ERROR: Could not find 'cps' script in 'bin' folder, aborting...>&2
EXIT /B 1
)
