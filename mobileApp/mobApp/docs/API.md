Documentación Api CPS V1
=====================================

-   [INTRODUCCIÓN](source/content/1-introduccion.rst)
    -   [Características](source/content/1-introduccion.rst#caracteristicas)
    -   [URL Base](source/content/1-introduccion.rst#url-base)

-   [Administración y Creación de Tokens de Aplicación](source/content/2-admin_tokens.rst)
    -   [Panel Creación Token Aplicación](source/content/2-admin_tokens.rst#panel-creacion-token-aplicacion)
    -   [Datos Requeridos](source/content/2-admin_tokens.rst#datos-requeridos)

-   [OAuth](source/content/3-oauth.rst)
    -   [Parámetros requeridos en el Header](source/content/3-oauth.rst#parametros-requeridos-en-el-header)
    -   [Solicitud de Token](source/content/3-oauth.rst#solicitud-de-token)
    -   [Token de Acceso](source/content/3-oauth.rst#token-de-acceso)

-   [OAuth2 (Solo Aplicaciones)](source/content/6-oauth2.rst)
    -   [Obtener Token de Acceso Bearer Oauth2](source/content/6-oauth2.rst#obtener-token-de-acceso-bearer-oauth2)
    -   [Token de Acceso OAuth2](source/content/6-oauth2.rst#token-de-acceso-oauth2)

-   [Recursos Disponibles](source/content/8-recursos_disponibles.rst)
    -   [Registrar Usuario](source/content/8-recursos_disponibles.rst#registrar-usuario)
    -   [Saldo Usuario](source/content/8-recursos_disponibles.rst#saldo-usuario)
    -   [Recarga Saldo](source/content/8-recursos_disponibles.rst#recarga-saldo)
    -   [Pago por Confirmación](source/content/8-recursos_disponibles.rst#pago-por-confirmacion)

-   [Códigos de Respuesta](source/content/9-codigos_respuesta.rst)
