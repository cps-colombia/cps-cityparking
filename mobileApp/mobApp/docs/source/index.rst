######################
Documentación Api CPS
######################

.. toctree::
   :maxdepth: 2

   content/0-changelog
   content/1-introduccion
   content/2-admin_tokens
   content/3-oauth
   content/6-oauth2
   content/8-recursos_disponibles
   content/9-codigos_respuesta
   content/10-ejemplos

.. only:: html

   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
