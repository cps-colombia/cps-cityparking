.. raw:: latex

   \begin{table}
   \vspace{5cm}
   \begin{tabu} to \textwidth{C{15.5cm}}
   \rowcolor{tabletitle}
   \color{tablehead}
   \textbf{
   DOCUMENTO DE ESPECIFICACIONES TÉCNICAS (DTE)      } \T\B        \\\color{gray}
   Código: 003                                                     \\\color{gray}
   Publicación: \version                                           \\\color{gray}
   Autor: Camilo Quimbayo                                          \\\color{gray}
   Aprobado: Ing. Andres Pereira                                   \\

   \end{tabu}
   \end{table}

.. only:: html

   CHANGELOG
   ==========

|

.. tabularcolumns:: |p{5cm}|p{10cm}|

======================= ============================
 | Versión Publicación   | Cambios en Documento
======================= ============================
 0.9                    * Descripción de ejemplos,
                          instalación y uso

 0.8                    * Agregado parametro y
                          descripción de bolsillo
                          en recurso *recargas*
                        * Eliminado parametro
                          commerce_id (redundante),
                          en pagos y recargas

 0.7                    * Cambios en parametros
                          en Pago por Confirmar
                        * Nuevos valores en respuesta
                          en Pago por confirmar

 0.6                    * Nuevo recursos del API
                        * Actualización parámetros
                          consulta y respuesta
                        * Actualización códigos
                          respuesta

 0.5                    * Versión Inicial
======================= ============================

..
   Useful raw params latex

.. raw:: latex

   \setlength{\tabcolsep}{1em} % for the horizontal padding
   \def\arraystretch{2.0} % for the vertical padding
