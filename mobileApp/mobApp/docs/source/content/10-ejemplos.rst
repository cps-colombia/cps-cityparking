Ejemplos API
====================

Desde CPS proveemos algunos ejemplos de como consumir los distintos recursos del **Api REST CPS**,
los ejemplos puede ser descargados en: http://96.45.176.18:4000/cps/apiExamples


Luego de descargado encontrara tres carpetas con ejemplos de autenticación oauth y oauth2 en distintos lenguajes/frameworks

- **rails**: Ejemplo sobre Ruby on Rails
- **php**: Ejemplo de autenticación sobre php(laravel)
- **django**: ejemplo de autenticación sobre Django

Dentro de cada carpeta se encontrara su respectivo **README** con las instrucciones de instalación y uso,
en general cualquiera librería con implementación de autenticación oauth/oauth2 servirá para conectarse al Api REST.



Ejemplo Ruby on Rails
----------------------

**Requerimientos:**

- Ruby 1.9 o superior
- Ruby on Rails 4.2 o superior
- Sqlite3


**Instalación:**

Instalar Ruby on Rails

.. code-block:: bash

   gem install rails

Entrar al proyecto rails

.. code-block:: bash

   cd rails/api_connect

Instale dependencias

.. code-block:: bash

   bundle install

..
   Other content
.. raw:: latex

   \newpage

**Uso:**

Arranque la aplicación

.. code-block:: bash

   bin/rails server

Para ver la aplicación en acción, abra el navegador y en *http://localhost:3000* vera la pagina para realizar la autenticación.



Ejemplo PHP
----------------------

**Requerimientos:**

- PHP 5.4+
- Servidor Apache o Nginx con php habilitado
- MySQL


**Instalación:**

Copie la **php/api_connect** a la carpeta de su servidor

.. code-block:: bash

   mv php/api_connect /www/http/apicps

**Uso:**

Abra el navegador y en *http://localhost/apicps* vera la pagina para realizar la autenticación.



Ejemplo Django
----------------------

**Requerimientos:**

- Python 2.7
- Pip 7.1 o superior
- Python virtualenv 13.1
- Sqlite3


**Instalación:**

Instale python-virtualenv si no lo tiene

.. code-block:: bash

   pip install virtualenv

Ingrese a la carpeta django y active virtualenv

.. code-block:: bash

   cd django
   virtualenv .
   source ./bin/activate

..
   Other content
.. raw:: latex

   \newpage

Instale dependencias

.. code-block:: bash

   pip install -r requirements.txt


**Uso:**

Entre al projecto y aranque la aplicación

.. code-block:: bash

   cd api_connect
   python manage.py runserver 8080

Para ver la aplicación en acción, abra el navegador y en *http://localhost:8080* vera la pagina para realizar la autenticación.
