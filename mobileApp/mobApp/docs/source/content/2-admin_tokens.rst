Administración y Creación de Tokens de Aplicación
=================================================

Panel Creación Token Aplicación
""""""""""""""""""""""""""""""""

`http://96.45.176.18/walletservice/faces/admin/appdev <http://96.45.176.18/walletservice/faces/admin/appdev>`_


Datos Requeridos
""""""""""""""""""

*   Ip/Dominio para el caso Web

*   App Bundle / App ID + Activity en el caso IOS/Android

*   URL de Callback - en el caso de integración con externos
