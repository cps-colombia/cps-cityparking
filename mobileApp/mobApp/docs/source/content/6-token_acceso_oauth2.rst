Token de Acceso OAuth2
----------------------

| **Método: POST**
| Ruta: /oauth2/token_access
| Scope: N/A
| Respuesta: JSON

**Petición:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+-------------------------------------+
| **Parámetro** | **Descripción**                     |
|               |                                     |
+---------------+-------------------------------------+
| expiration    | Tiempo en el que expira el token    |
|               |                                     |
|               | *   1h                              |
|               | *   1d                              |
|               | *   1w                              |
|               | *   30d                             |
|               | *   never - por defecto             |
|               |                                     |
+---------------+-------------------------------------+
| grant_type    | Solo se soporta: client_credentials |
|               |                                     |
+---------------+-------------------------------------+
| scopes        | Permisos adicionales que solicita   |
|               | la aplicación                       |
|               |                                     |
|               | **Ver Scopes**                      |
|               |                                     |
+---------------+-------------------------------------+

.. raw:: latex

   \vspace{5mm}

**Respuesta:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+--------------------------------------+
| **Respuesta** | **Descripción**                      |
|               |                                      |
+---------------+--------------------------------------+
| token_type    | Tipo de token oauth2, soportado solo |
|               | bearer                               |
|               |                                      |
+---------------+--------------------------------------+
| access_token  | Token de Acceso para futuras         |
|               | consultas GET                        |
|               |                                      |
+---------------+--------------------------------------+
| expires_in    | Tiempo en el que expira en segundos  |
|               |                                      |
+---------------+--------------------------------------+
