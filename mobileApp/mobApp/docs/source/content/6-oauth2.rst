OAuth2 (Solo Aplicaciones)
==========================


Por medio de este protocolo se podrá acceder a solo algunos **scopes**
de lectura y podrá hacer consultar solo por el método **GET**.

Por medio de *OAuth2* no requiere autorización por parte del usuario y
esta pensada solo para aplicaciones que no requiere permisos de usuario o no tiene la posibilidad de un callback,
luego de autenticar la aplicación recibirá un **access_token** con que el realizara las consultar, puede ser
incluido en el ``header`` o como parte de los parámetros del ``body``, se recomienda el header.

.. raw:: latex

   \linebreak
   \newline

**Header**

.. code-block:: rest

   Authorization: Bearer FHGHGHJHKUGGJKJLKKLGNNJBYY7676


**Body**

.. code-block:: rest

   access_token=FHGHGHJHKUGGJKJLKKLGNNJBYY7676



Este método es útil cuando se requiere operaciones de solo consulta, como asesores, puntos de consulta
de saldo, también esta limitado a los permisos (**scopes**) que desde el panel CPS se den.

El token de acceso puede tener expiración, por defecto **'never'** pero se puede pasar el parámetro
**expiration** con ``1h, 1d, 1w, 30d`` donde **d=day, w=week y h=hour**.

.. raw:: latex

   \linebreak
   \newline

Obtener Token de Acceso Bearer Oauth2
-------------------------------------

Para este paso necesita los **oauth_customer_token** y **oauth_customer_secret**


* Codificar los customer tokens en RFC 1738 (generalmente no cambia)
* Unir separado por ``:`` los customer tokens en el orden

  **oauth_customer_token:oauth_customer_secret**

* Codificar el resultado de la unión en **base64**

.. code-block:: rest

   base64 resultado:
   jgYT7MWV2RlM0d0VFUFRHRUZQSEJvZzpMOHFxOVBaeVJnNmllS0dFS2hab2xHQzB2SlZz==


.. raw:: latex

   \newpage


* Enviar petición **POST** a **/v1/oauth2/token_access**, con el **header Autorization**

.. code-block:: rest

   POST /v1/oauth2/token_access
   HTTP/1.1
   Connection: keep
   User-Agent: CPS

   Authorization: Basic jgYT7 MWV2RlM0d0VFUFRHRUZQSEJvZzpMOHFxOVBaeVJnNmllS0dFS2QzB2SlZz==


.. include:: 6-token_acceso_oauth2.rst
