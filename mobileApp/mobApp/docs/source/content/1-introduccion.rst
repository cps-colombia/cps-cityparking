******************
INTRODUCCIÓN
******************

El API Rest CPS esta construida siguiendo el estándar OAuth y OAuth 2,
las cuales proveen un flujo para la conexión con terceros brindando de seguridad a todas las partes,
permitiendo al desarollador tercero o proveedor de servicio acceder a la información de usuario con el
consentimiento del mismo pero sin que el usuario revele datos privados y vulnerables como la contraseña.


El proveedor
puede utilizar el Api Rest de CPS para el consumo de métodos de transferencia, recarga, consulta de saldo,
acceder al perfil de usuario, todo según los **permisos (scopes)** solicitados en el Request inicial y
otorgados en la generación del **Customer Token.**


Así mismo también lo puede utilizar para que el usuario pueda Identificarse en otras plataformas de pago sin
necesidad de registrarse en estas.

Características
---------------

* **Seguro**

  - Los usuarios no están obligados a compartir sus contraseñas con aplicaciones
    de terceros, aumento la seguridad de la cuenta.

* **Estándar**

  - Una gran cantidad de bibliotecas de cliente y código de ejemplo son compatibles con la aplicación de OAuth de CPS.


URL Base
--------

`http://96.45.176.18:3000/api/v1/ <http://96.45.176.18:3000/api/v1/>`_
