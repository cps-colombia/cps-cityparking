Recursos Disponibles
====================

Para todos los métodos es necesario los parámetros **OAuth en el header**, Excepto los métodos
``GET`` que puede ser consultado por medio de **Oauth2**, *o según permisos asignados*.


Registrar Usuario
-----------------

| **Método: POST**
| Ruta: /users
| Scope: users
| Respuesta: JSON

Crea usuario en el sistema CPS.


**Petición:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+------------------------------------------------------------------------------------------+
| **Parámetro** | **Descripción**                                                                          |
+===============+==========================================================================================+
| login         | Nro. de Identificación del usuario con el que ingresara en las plataformas CPS           |
+---------------+------------------------------------------------------------------------------------------+
| name          | Nombre(s) del usuario                                                                    |
+---------------+------------------------------------------------------------------------------------------+
| last_name     | Apellido(s) del usuario                                                                  |
+---------------+------------------------------------------------------------------------------------------+
| address       | Dirección residencia o trabajo                                                           |
+---------------+------------------------------------------------------------------------------------------+
| email         | Email del usuario con el que se podrá realizar diferentes consultas como consultar saldo |
+---------------+------------------------------------------------------------------------------------------+
| phone         | Numero celular del usuario                                                               |
+---------------+------------------------------------------------------------------------------------------+
| dob           | Fecha de nacimiento del usuario en formato milisegundos timestamp Unix                   |
+---------------+------------------------------------------------------------------------------------------+
| country_id    | ID de país habilitados en la plataforma                                                  |
|               | **(ver countries)**                                                                      |
+---------------+------------------------------------------------------------------------------------------+
| city_id       | ID de ciudad habilitados en la plataforma                                                |
|               | **(ver cities)**                                                                         |
+---------------+------------------------------------------------------------------------------------------+



**Respuesta:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+------------------------------------------------+
| **Respuesta** | **Descripción**                                |
|               |                                                |
+===============+================================================+
| cu            | Código Único del usuario                       |
|               |                                                |
+---------------+------------------------------------------------+
| login         | Nro. de Identificación del usuario             |
|               |                                                |
+---------------+------------------------------------------------+
| name          | Nombre(s) del usuario                          |
|               |                                                |
+---------------+------------------------------------------------+
| last_name     | Apellido(s) del usuario                        |
|               |                                                |
+---------------+------------------------------------------------+
| display_name  | Nombre a mostrar del usuario                   |
|               |                                                |
+---------------+------------------------------------------------+
| address       | Dirección residencia o trabajo                 |
|               |                                                |
+---------------+------------------------------------------------+
| email         | Email registrado en la plataforma CPS          |
|               |                                                |
+---------------+------------------------------------------------+
| phone         | Numero celular del usuario                     |
|               |                                                |
+---------------+------------------------------------------------+
| dob           | Fecha de nacimiento del usuario en formato     |
|               | string                                         |
+---------------+------------------------------------------------+
| city          | Información del Ciudad registrada              |
|               |                                                |
|               | * id: ID de ciudad                             |
|               | * name: Nombre de la ciudad                    |
|               | * country : Información del País de registro   |
|               |                                                |
|               |   * id: ID de país                             |
|               |   * name: Nombre del país                      |
|               |                                                |
+---------------+------------------------------------------------+


.. raw:: latex

   \newpage


Saldo Usuario
-------------

| **Método: GET**
| Ruta: /users/:identificador/balances
| Scope: balance
| Respuesta: JSON

Saldo total del usuario en los distintos **Bolsillo**, ``:identificador`` se refiere a CU,
login, celular o correo del usuario.

.. raw:: latex

   \\[5mm]


**Petición:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+-----------------------------------------------+
| **Parámetro** | **Descripción**                               |
|               |                                               |
+===============+===============================================+
| N/A           | N/A, solo se necesita el parámetro en la URL. |
|               |                                               |
+---------------+-----------------------------------------------+

.. raw:: latex

   \vspace{5mm}

**Respuesta:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+---------------------------------------------------------------+
| **Respuesta** | **Descripción**                                               |
|               |                                                               |
+===============+===============================================================+
| login         | Nro. de Identificación                                        |
|               |                                                               |
|               | con el que se autentica                                       |
|               |                                                               |
+---------------+---------------------------------------------------------------+
| name          | Nombre(s) del usuario                                         |
|               |                                                               |
+---------------+---------------------------------------------------------------+
| last_name     | Apellido(s) del usuario                                       |
|               |                                                               |
+---------------+---------------------------------------------------------------+
| display_name  | Nombre a mostrar del usuario                                  |
|               |                                                               |
+---------------+---------------------------------------------------------------+
| balance       | Saldo total del usuario en todos los *bolsillo*               |
|               |                                                               |
+---------------+---------------------------------------------------------------+
| last_update   | Fecha en formato string de ultima actualización del usuario.  |
|               |                                                               |
|               | | Si nunca ha actualizado el valor sera **“never”**           |
|               |                                                               |
+---------------+---------------------------------------------------------------+


.. raw:: latex

   \newpage


Recarga Saldo
-------------

| **Método: PUT**
| Ruta: /recharges
| Scope: transaction
| Respuesta: JSON

Este método se utiliza para recarga al **Bolsillo Personal** desde un establecimiento asociado.

**Petición:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

.. raw:: latex

   \def\arraystretch{1.1}
   \newline
   \newline

+---------------+---------------------------------------------+
| **Parámetro** | **Descripción**                             |
|               |                                             |
+===============+=============================================+
| reference_id  | Numero de referencia de la recarga          |
|               |                                             |
+---------------+---------------------------------------------+
| identifier    | Código único, email, o usuario              |
|               |                                             |
+---------------+---------------------------------------------+
| date          | Fecha de recarga en formato timestamp unix  |
|               |                                             |
+---------------+---------------------------------------------+
| branch_id     | ID sucursal donde se va efectuar la recarga |
|               |                                             |
+---------------+---------------------------------------------+
| pocket_id     | ID del bolsillo que se va a recarga         |
|               |                                             |
+---------------+---------------------------------------------+
| means         | ID, Método de pago.                         |
|               |                                             |
|               | * 1 | Efectivo                              |
|               | * 2 | Tarjeta crédito                       |
|               | * 3 | Tarjeta débito                        |
|               |                                             |
+---------------+---------------------------------------------+
| amount        | Monto a recargar                            |
|               |                                             |
+---------------+---------------------------------------------+
| status        | Estado de la transacción, Opcional,         |
|               | Por defecto **“completed”**                 |
|               |                                             |
|               | * completed                                 |
|               | * started                                   |
+---------------+---------------------------------------------+

.. raw:: latex

   \vspace{4mm}


**Respuesta:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+---------------------------------------------+
| **Respuesta** | **Descripción**                             |
|               |                                             |
+===============+=============================================+
| reference_id  | Id de referencia del pago                   |
|               |                                             |
+---------------+---------------------------------------------+
| transaction_id| Número Único generado por transación        |
|               |                                             |
+---------------+---------------------------------------------+
| means         | Método de pago                              |
|               |                                             |
+---------------+---------------------------------------------+
| status        | Estado de la transacción                    |
|               |                                             |
+---------------+---------------------------------------------+
| branch        | Información de la sucursal                  |
|               |                                             |
|               | * id : Id sucursal                          |
|               |                                             |
+---------------+---------------------------------------------+
| currency      | ISO de la Moneda                            |
|               |                                             |
+---------------+---------------------------------------------+
| amount        | Cantidad recargada                          |
|               |                                             |
+---------------+---------------------------------------------+
| date          | Fecha de transacción                        |
|               |                                             |
+---------------+---------------------------------------------+
| user          | Información Básica del usuario que recargo. |
|               |                                             |
|               | * email                                     |
|               |                                             |
+---------------+---------------------------------------------+


.. raw:: latex

   \newpage
   \def\arraystretch{2.0}

.. _pago-por-confirmacion:

Pago por Confirmación
---------------------

| **Método: POST**
| Ruta: /payments
| Scope: transaction
| Respuesta: JSON

Método para enviar pago desde un establecimiento (físico o web/móvil) que requiere aprobación del usuario.

.. raw:: latex

   \def\arraystretch{1.5}

**Petición:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+---------------------------------------------------+
| **Parámetro** | **Descripción**                                   |
|               |                                                   |
+===============+===================================================+
| branch_id     | ID Sucursal desde donde se enviá el pago.         |
|               |                                                   |
+---------------+---------------------------------------------------+
| reference_id  | Numero de referencia asociado al pago a realizar  |
|               |                                                   |
+---------------+---------------------------------------------------+
| amount        | Monto a recargar                                  |
|               |                                                   |
+---------------+---------------------------------------------------+
| phone         | Numero de celular del usuario que realiza el pago |
|               |                                                   |
+---------------+---------------------------------------------------+

.. raw:: latex

   \vspace{5mm}


**Respuesta:**

.. tabularcolumns:: |p{5cm}|p{10cm}|

+---------------+--------------------------------------------------------------------------+
| **Respuesta** | **Descripción**                                                          |
|               |                                                                          |
+===============+==========================================================================+
| transaction_id| Identificador único del pago                                             |
|               |                                                                          |
+---------------+--------------------------------------------------------------------------+
| reference_id  | Numero de referencia asociado al pago enviado                            |
|               |                                                                          |
+---------------+--------------------------------------------------------------------------+
| user          | Información Básica del usuario al que se le enviá el pago por confirmar. |
|               |                                                                          |
|               | * email                                                                  |
|               | * phone                                                                  |
|               |                                                                          |
+---------------+--------------------------------------------------------------------------+
| date          | Contiene la fecha de envio                                               |
|               |                                                                          |
|               | * send: fecha de envio en UTC                                            |
|               |                                                                          |
+---------------+--------------------------------------------------------------------------+
| branch        | Información sucursal que enviá el pago                                   |
|               |                                                                          |
|               | * id: ID de la sucursal que enviá el pago                                |
|               | * name: Nombre de la sucursal                                            |
|               | * allow_pockets: Array de bolsillos que acepta la sucursal               |
|               | * commerce: Información del comercio                                     |
|               |                                                                          |
|               |   * id: ID del comercio que enviá el pago                                |
|               |                                                                          |
+---------------+--------------------------------------------------------------------------+
| amount        | Monto del pago que se solicito al usuario                                |
|               |                                                                          |
+---------------+--------------------------------------------------------------------------+
| status        | Estado del pago que pueden ser: *pending, approved, rejected, expired*   |
|               |                                                                          |
+---------------+--------------------------------------------------------------------------+
