Token de Acceso
---------------

| **Método: POST**
| Ruta: /token_access
| Scope: N/A
| Respuesta: JSON

Este método se utiliza para solicitar generar los **token de usuario**
finales que podrán ser usado para consultas posteriores, **son requeridos todo los parámetros Oauth**
y deben ser **firmados con el oauth_token_secret y oauth_customer_secret**

.. code-block:: rest

   POST /v1/token_request
   HTTP/1.1
   Accept: */*
   Connection: keep
   User-Agent: OAuth
   Content-Type: application/x-www-form-urlencoded

   Authorization:
       OAuth oauth_consumer_key="Y0ZFabxZFabxSWbWovY3uabxSWbWovYSWbWovY3u",
             oauth_nonce="YyLbNEtIKZeRNFsbWovY3uYSQ2pTgmZe",
             oauth_signature="tnnArxj06cJSH3736Cs1OSKk%2FjLYisi3D",
             oauth_signature_method="HMAC-SHA1",
             oauth_token="HGcudjsh1yU5T3jshs86CotJ6cnetKth",
             oauth_timestamp="1433519047727",
             oauth_version="1.0"
   Content-Length: 76

   oauth_verifier=ssjsHGKS37383JSkdkdsosp

.. raw:: latex

   \vspace{5mm}

**Petición:**

+----------------+--------------------------------------------+
| **Parámetro**  | **Descripción**                            |
|                |                                            |
+----------------+--------------------------------------------+
| oauth_verifier | Token de verificación                      |
|                | que llego con el callback de autorización. |
|                |                                            |
+----------------+--------------------------------------------+

.. raw:: latex

   \vspace{5mm}

**Respuesta:**

+--------------------+---------------------------------+
| **Respuesta**      | **Descripción**                 |
|                    |                                 |
+--------------------+---------------------------------+
| oauth_token        | Token finales de usuario.       |
|                    |                                 |
+--------------------+---------------------------------+
| oauth_token_secret | Token Secreto final de usuario. |
|                    |                                 |
+--------------------+---------------------------------+
