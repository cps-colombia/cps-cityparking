Solicitud de Token
------------------

| **Método: POST**
| Ruta: /token_request
| Scope: N/A
| Respuesta: JSON


Este método se utiliza para solicitar los **token de usuario**,
necesita todos los parámetros OAuth sin el **oauth_token** y firmado
solo con el **oauth_customer_token** adicional a **oauth_callback**
que puede estar en el header o en el body y debe ser URL-encoded.


Ejemplo header
"""""""""""""""
.. code-block:: rest

   POST /v1/token_request
   HTTP/1.1
   Accept: */*
   Connection: keep
   User-Agent: OAuth
   Content-Type: application/x-www-form-urlencoded

   Authorization:
         OAuth oauth_consumer_key="Y0ZFabxZFabxSWbWovY3uabxSWbWovYSWbWovY3u",
               oauth_nonce="YyLbNEtIKZeRNFsbWovY3uYSQ2pTgmZe",
               oauth_signature="tnnArxj06cWHq44gCs1OSKk%2FjLY%3D",
               oauth_signature_method="HMAC-SHA1",
               oauth_timestamp="1433519047727",
               oauth_version="1.0"
               Content-Length: 76

   oauth_callback=http%3A%2F%2Fexito.com.co%2Fuser
   scopes=user_profile%2Crecharge%2Cbithday


Luego de la solicitud retornara el **oauth_token** y **oauth_token_secret** provisionales y
seguido de que el usuario apruebe la aplicación se generara los token de usuario finales.

.. raw:: latex

   \\[15mm]


**Petición:**

+----------------+--------------------------------------------------------------------------+
| **Parámetro**  | **Descripción**                                                          |
|                |                                                                          |
+----------------+--------------------------------------------------------------------------+
| oauth_callback | Url de retorno luego de la autorización o rechazo por                    |
|                | parte del usuario.                                                       |
+----------------+--------------------------------------------------------------------------+
| scopes         | Permisos que solicita la aplicación al usuario.                          |
|                |                                                                          |
|                | **Ver Scopes**                                                           |
|                |                                                                          |
+----------------+--------------------------------------------------------------------------+

.. raw:: latex

   \newpage

**Respuesta:**

+--------------------------+------------------------------------------------------+
| **Respuesta**            | **Descripción**                                      |
|                          |                                                      |
+--------------------------+------------------------------------------------------+
| oauth_token              | Token provisional de usuario.                        |
|                          |                                                      |
+--------------------------+------------------------------------------------------+
| oauth_token_secret       | Token Secreto provisional de usuario.                |
|                          |                                                      |
+--------------------------+------------------------------------------------------+
| oauth_callback_confirmed | Si la url ha sido confirmada                         |
|                          |                                                      |
+--------------------------+------------------------------------------------------+
| url                      | La Url de autorización para redireccionar al usuario |
|                          |                                                      |
+--------------------------+------------------------------------------------------+

.. raw:: latex

   \vspace{5mm}

Redireccione al usuario a la url generada que sera algo como:

https://96.45.176.18:3000/api/v1/authorize?oauth_token=HGcudjsh1yU5T3jshs86CotJ6cnetKth


Luego de que el usuario autorice o rechace retornara a la url del callback con dos
parámetros en ella los cuales son:

.. raw:: latex

   \\[10mm]

**Parámetros en Callback:**

+---------------+--------------------------------------------------------------------------------------------+
| **Respuesta** | **Descripción**                                                                            |
|               |                                                                                            |
+---------------+--------------------------------------------------------------------------------------------+
| oauth_token   | El token de usuario provisional suministrado anteriormente                                 |
|               |                                                                                            |
+---------------+--------------------------------------------------------------------------------------------+
| oauth_verifier| Token de verificación usado en el siguiente paso para la generación de los tokens finales. |
|               |                                                                                            |
+---------------+--------------------------------------------------------------------------------------------+

Si el callback no tiene el parámetro **oauth_verifier** en el query significa que el usuario
no autorizo o no realizo el login con CPS.


.. raw:: latex

   \newpage
