OAuth
=====


Por medio de este protocolo se podrá acceder a todo tipo de **scopes** tanto de leer como de escribir
y podrá hacer consultar de métodos GET, POST, PUT o DELETE.


Todas las consultas deben incluir en el **header** los parámetros de validación y en el body del Request
los parámetros del método a consultar y **Content Type: application/x-www-form-urlencoded**

|flujo_oauth| [#f1]_


Parámetros requeridos en el Header
----------------------------------

.. tabularcolumns:: |m{5cm}|m{10cm}|

============================= =================================================================

 **Parámetro OAuth**            **Descripción**

============================= =================================================================
 oauth_consumer_key            El token del cliente/app, otorgado y creado en el panel de CPS

 oauth_signature_method        El metodo con el que se firma la solicitud,

                               | Actualmente soportados
                               | HMAC-SHA1, HMAC-MD5 y HMAC-SHA256

 oauth_token                   Son los tokens del usuario generados por aplicación.

 oauth_signature               La firma de la solicitud  que contiene todos los elementos
                               del header y el  oauth_secret y oauth_consumer_secret.

                               | **Ver: Crear Firmado**

 oauth_timestamp               El registro del tiempo en el que se envió la solicitud en
                               formato de segundos en unix *(timestamp)*

                               | **Ver: Nonce y Timestamp**


 oauth_nonce                   Es una muestra única que se debe generar para cada solicitud,
                               este valor se utiliza para determinar si la solicitud se ha
                               enviado varias veces.


 oauth_version                 Opcional Versión del Oauth, por defecto 1.0
============================= =================================================================

Token Aplicación/Cliente:
~~~~~~~~~~~~~~~~~~~~~~~~~

Se compone de un **oauth_consumer_key** y **oauth_consumer_secret**

los cuales son generados desde el panel de administración en la página CPS Wallet, estos provee acceso al consumo del API.

Una aplicación puede ser escrita en cualquier lenguaje de programación y para cualquier plataforma, puede ser consumida
por un sitio web, una aplicación móvil o una aplicación de escritorio etc.


Token Usuario
~~~~~~~~~~~~~

Se compone de un **oauth_token** y **oauth_secret**.

El **Token de Usuario** va asociado al **Token de Aplicación**

permitiendo un mayor control desde donde se consume el API.

El usuario podrá revocar estos tokens o el administrador en caso de alguna vulnerabilidad blindando de seguridad al API.


Nonce y Timestamp
~~~~~~~~~~~~~~~~~
Estos parámetros son usados para asegurar y validar que cada petición sea única y prevenir posibles ataques, es una muestra
única que debe generar para cada solicitud.

El valor de esta solicitud es un base64 codificado a 32 bytes de datos aleatorios, pero cualquier método que produce una cadena
alfanumérica relativamente aleatoria debería estar bien aquí y el  **timestamp** es la muestra única del tiempo en el que se envió
la consulta en formato de segundos en formato UNIX.

Crear Firmado
~~~~~~~~~~~~~

El firmado puede ser generado por medio de HMAC-SHA1, HMAC-MD5 y HMAC-SHA256y debe ser especificado en **oauth_signature_method**
para el firmado se deben incluir todos los parámetros **oauth_** y todos los parámetros adicionales de la petición.


Ejemplo Petición y Firmado
###########################

.. code-block:: rest

   POST /v1/recharges
   HTTP/1.1

   Accept: */*
   Connection: keep
   User-Agent: OAuth
   Content-Type: application/x-www-form-urlencoded

   Authorization:
   OAuth oauth_consumer_key="Y0ZFabxZFabxSWbWovY3uabxSWbWovYSWbWovY3u",
         oauth_nonce="YyLbNEtIKZeRNFsbWovY3uYSQ2pTgmZe",
         oauth_signature="tnnArxj06cWHq44gCs1OSKk%2FjLY%3D",
         oauth_signature_method="HMAC-SHA1",
         oauth_timestamp="1433519047727",
         oauth_token="sjsuUjsjs-GmHxMAgMKPR9EyMZeS9weJAEb",
         oauth_version="1.0"

   Content-Length: 76

   id=1263573
   cu=2345
   amount=5000
   paytype=1
   idbranch=2

1. Unir y codificar los parámetros

   * Utilice RFC 3986 para codificar los parámetros
   * Clasificar la lista de parámetros alfabéticamente, por clave codificada [1]_
   * Agregue = Para cada par clave / valor
   * Separe los parámetros por medio de &

.. [1] En caso de dos parámetros con la misma llave codificada, CPS no acepta claves duplicadas en las solicitudes del API.

   **La Salida sera algo como:**

.. code-block:: rest

      amount=5000&cu=2345&id=1263573&idbranch=2&oauth_consumer_key=Y0ZFabxZFabxS
      WbWovY3uabxSWbWovYSWbWovY3u&paytype=&oauth_nonce=YyLbNEtIKZeRNFsbWovY3uYSQ2
      pTgmZe&oauth_signature_method=HMAC-SHA1&oauth_timestamp=
      1433519047727&oauth_token=sjsuUjsjs-GmHxMAgMKPR9EyMZeS9weJAEb


2. Agregue el método HTTP y la URL Base para generar una sola cadena de texto.

   * Convierta el método HTTP en mayúsculas y agregue & al final
   * Codifique la URL Base en RFC 3986 y agregue & al final
   * Codifique en RFC 3986 la cadena de texto con los parámetros.

   **La Salida sera algo como:**

.. code-block:: rest

      POST&https%3A%2F%2F96.45.176.18%3A3000%2Fapi%2Fv1%2Fbalance_recharge&amoun
      t%3D5000%26cu%3D2345%26id%3D1263573%26idbranch%3D2%26oauth_consumer_key%3D
      Y0ZFabxZFabxSWbWovY3uabxSWbWovYSWbWovY3u%26paytype%3D1%26oauth_nonce%3DYyL
      bNEtIKZeRNFsbWovY3uYSQ2pTgmZe%26oauth_signature_method%3DHMAC-SHA1%
      26oauth_timestamp%3D1433519047727%26oauth_token%3DsjsuUjsjs-GmHxMAgMKPR9EyMZeS9weJAEb


3. Utilice el **oauth_token_secret** y el **oauth_customer_secret** para la generación del firmado.

   * El método HMAC pide una llave para generar el firmado, para esta se usara los oauth secret, excepto en el método **token_request**
     el cual solo se usara el oauth_token_secret, el orden es **oauth_customer_secret&oauth_token_secret.**

.. code-block:: rest

      21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3e7zjz7ZN2U4ZRhfV3WpwPAoE3&wwdoUaIvS8ltyTt5
      jkRh4J50vUPVVHtR2YP


4. Luego se generara, se convierte en base64 y la salida seria algo como:

.. code-block:: rest

   Signature sngFrxj04cWHq55gCs2ODKk/lFZ=


.. |flujo_oauth| image:: /_static/images/flujo_oauth.png
    :width: 6.5453in

.. [#f1] Imagen flujo tomada de: http://oauth.net/core/1.0/

..
   Other content
.. raw:: latex

   \newpage

.. include:: 3-solicitud_token.rst
.. include:: 3-token_acceso.rst
