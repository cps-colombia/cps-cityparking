Changelog
==============

<a name="2.1.0"></a>
# [2.1.0](http://104.155.190.166/cps/mobApp/compare/v2.0.10...v2.1.0) (2016-12-01)


### Bug Fixes

* **balance:** clean pin input when close dialog ([4a1e2a6](http://104.155.190.166/cps/mobApp/commits/4a1e2a6))
* **balance:** prevent send transfer to same user ([d78d590](http://104.155.190.166/cps/mobApp/commits/d78d590))
* **balance:** set balance after pin recharge ([a54a260](http://104.155.190.166/cps/mobApp/commits/a54a260))
* **balance:** transfer form diferent locale amount ([516f5ea](http://104.155.190.166/cps/mobApp/commits/516f5ea))
* **cli:** check if file existe and return resolve ([d1bacde](http://104.155.190.166/cps/mobApp/commits/d1bacde))
* **cli:** get platform on install ([fde485c](http://104.155.190.166/cps/mobApp/commits/fde485c))
* **core:** make async startup services ([c457c01](http://104.155.190.166/cps/mobApp/commits/c457c01))
* **history:** show correct information in old history response ([e7551d5](http://104.155.190.166/cps/mobApp/commits/e7551d5))
* **login:** change page on cgange password ([f2c09f1](http://104.155.190.166/cps/mobApp/commits/f2c09f1))
* **login:** emit event when user blocked ([9468bb2](http://104.155.190.166/cps/mobApp/commits/9468bb2))
* **login:** ensure country and city as int ([e4d47d2](http://104.155.190.166/cps/mobApp/commits/e4d47d2))
* **login:** Fix login storage and new storare restore ([003cda1](http://104.155.190.166/cps/mobApp/commits/003cda1))
* **main:** parse idcountry if come as string ([1d3de75](http://104.155.190.166/cps/mobApp/commits/1d3de75))
* **main:** Prevent js exeption when clean basicData ([2bec70b](http://104.155.190.166/cps/mobApp/commits/2bec70b))
* **maps:** prevent close(re-create) marker if alredy open ([89c0846](http://104.155.190.166/cps/mobApp/commits/89c0846))
* **maps:** set map instance in markers js after first init ([e426229](http://104.155.190.166/cps/mobApp/commits/e426229))
* **maps:** white map in android <4.3 ([b56e474](http://104.155.190.166/cps/mobApp/commits/b56e474))
* **modal:** diferent class if title is disabled ([a76a101](http://104.155.190.166/cps/mobApp/commits/a76a101))
* **notify:** empty user notifications as success ([46670f1](http://104.155.190.166/cps/mobApp/commits/46670f1))
* **notify:** fire action when click in notifications local/push [ios] ([4d5fd3f](http://104.155.190.166/cps/mobApp/commits/4d5fd3f))
* **notify:** improve registration push ([7eb2b81](http://104.155.190.166/cps/mobApp/commits/7eb2b81))
* **parking:** not show loader in active parking  backgound mode ([9af67d5](http://104.155.190.166/cps/mobApp/commits/9af67d5))
* **payment:** get history by filter ([658107e](http://104.155.190.166/cps/mobApp/commits/658107e))
* **payment:** history onlye show dates with transaction ([1d7835d](http://104.155.190.166/cps/mobApp/commits/1d7835d))
* **payment:** history order time ([0cefbf4](http://104.155.190.166/cps/mobApp/commits/0cefbf4))
* **payment:** prevent duplicate history data ([e633e4b](http://104.155.190.166/cps/mobApp/commits/e633e4b))
* **profile:** fix set cities and countries ([1b143dd](http://104.155.190.166/cps/mobApp/commits/1b143dd))
* **profile:** get cities on change contry ([8e02cf4](http://104.155.190.166/cps/mobApp/commits/8e02cf4))
* **socket:** inprove socket timeout and limit wait cb ([5a5b677](http://104.155.190.166/cps/mobApp/commits/5a5b677))
* **socket:** iOS keep socket connected but events not are received ([5b0ec3a](http://104.155.190.166/cps/mobApp/commits/5b0ec3a))
* **socket:** Prevent duplicate socket request ([f574047](http://104.155.190.166/cps/mobApp/commits/f574047))
* **tab:** Deselect tab if any tab is active ([d699b9c](http://104.155.190.166/cps/mobApp/commits/d699b9c))
* **test:** jshint errors ([6f5c660](http://104.155.190.166/cps/mobApp/commits/6f5c660))
* **ui:** popmenu close when change view ([454b6ec](http://104.155.190.166/cps/mobApp/commits/454b6ec))
* **ui:** remove old class in dynamic icon tab ([a78311d](http://104.155.190.166/cps/mobApp/commits/a78311d))
* **ui:** set options for Popmenu module ([7c832ba](http://104.155.190.166/cps/mobApp/commits/7c832ba))
* **ui:** view better resize content relative ([b616178](http://104.155.190.166/cps/mobApp/commits/b616178))
* **ui/utils:** in android <4.3 transitionend not fire force ([d76e908](http://104.155.190.166/cps/mobApp/commits/d76e908))
* **utils:** call event to force background mode [ios] ([3651329](http://104.155.190.166/cps/mobApp/commits/3651329))
* **utils:** cpsOptions parse integer numbers ([62153d9](http://104.155.190.166/cps/mobApp/commits/62153d9))
* **utils:** encrypt return string ([1ec5cb8](http://104.155.190.166/cps/mobApp/commits/1ec5cb8))
* **utils:** encryptService detect if number ([ae71f6b](http://104.155.190.166/cps/mobApp/commits/ae71f6b))
* **utils:** fullHeight directive detect parent padding/maring ([e729f81](http://104.155.190.166/cps/mobApp/commits/e729f81))
* **utils:** geo ask active location and check if enabled ([1a53136](http://104.155.190.166/cps/mobApp/commits/1a53136))
* **utils:** geo check if GPS is active and get data after enabled ([191138d](http://104.155.190.166/cps/mobApp/commits/191138d))
* **utils:** geo service switch to setting [ios] ([9128a2c](http://104.155.190.166/cps/mobApp/commits/9128a2c))
* **utils:** impove pageChange ([1fda724](http://104.155.190.166/cps/mobApp/commits/1fda724))
* **utils:** improve geo service to check location [ios] ([b3f1eb3](http://104.155.190.166/cps/mobApp/commits/b3f1eb3))
* **utils:** listen post-compile href to open in external brwoser ([51359f1](http://104.155.190.166/cps/mobApp/commits/51359f1))
* **utils:** mobile prevent duplicate alerts ([603cb6b](http://104.155.190.166/cps/mobApp/commits/603cb6b))
* **utils:** native dialog fix promise ([5e698e1](http://104.155.190.166/cps/mobApp/commits/5e698e1))
* **utils:** request after run app and get locales ([56201df](http://104.155.190.166/cps/mobApp/commits/56201df))
* **utils:** update inBrowser plugin ([6e279b6](http://104.155.190.166/cps/mobApp/commits/6e279b6))
* **utils:** update section class when change view ([c2695bc](http://104.155.190.166/cps/mobApp/commits/c2695bc))
* **view:** view nav directive without scope ([c5fa341](http://104.155.190.166/cps/mobApp/commits/c5fa341))
* **wallet:** set correct title on confirm operation ([a72f971](http://104.155.190.166/cps/mobApp/commits/a72f971))
* **wallet:** set title in operation confirm ([05f9c1b](http://104.155.190.166/cps/mobApp/commits/05f9c1b))
* **zones:** get city/country by id as string ([0dfdf70](http://104.155.190.166/cps/mobApp/commits/0dfdf70))


### Features

* **balance:** Total implement recharge pin ([6097cad](http://104.155.190.166/cps/mobApp/commits/6097cad))
* **balance:** update balance or get after transfer ([f8283d4](http://104.155.190.166/cps/mobApp/commits/f8283d4))
* **balance:** update new params for transfer ([2e14ca4](http://104.155.190.166/cps/mobApp/commits/2e14ca4))
* **cli:** add browserSync task ([709f6c4](http://104.155.190.166/cps/mobApp/commits/709f6c4))
* **cli:** expose cpsConfig file as module ([2b28786](http://104.155.190.166/cps/mobApp/commits/2b28786))
* **core:** 'showStatusBar' options to show un fullScreen ([b40b118](http://104.155.190.166/cps/mobApp/commits/b40b118))
* **dialog:** custom class fot close button ([a0dabe8](http://104.155.190.166/cps/mobApp/commits/a0dabe8))
* **loader:** new preload styles double and scaleout ([9b6623f](http://104.155.190.166/cps/mobApp/commits/9b6623f))
* **loading:** preload 'duration' option ([7b3dc78](http://104.155.190.166/cps/mobApp/commits/7b3dc78))
* **locale:** fire event when save new lang ([c96da84](http://104.155.190.166/cps/mobApp/commits/c96da84))
* **login:** add alert when pass are blocked ([86aae83](http://104.155.190.166/cps/mobApp/commits/86aae83))
* **login:** userInfo directive now show CU ([6ec4102](http://104.155.190.166/cps/mobApp/commits/6ec4102))
* **main:** add event when chenge to homePage ([be5eacf](http://104.155.190.166/cps/mobApp/commits/be5eacf))
* **main:** Analytics event when restore user ssession ([1819834](http://104.155.190.166/cps/mobApp/commits/1819834))
* **main:** expose $viewName ([81af0cd](http://104.155.190.166/cps/mobApp/commits/81af0cd))
* **main:** expose languge ([533e181](http://104.155.190.166/cps/mobApp/commits/533e181))
* **main:** update user locale with socket ([a852a44](http://104.155.190.166/cps/mobApp/commits/a852a44))
* **maps:** expose in controller last infowindow open ([1db54af](http://104.155.190.166/cps/mobApp/commits/1db54af))
* **maps:** general improve infowindow ([d68cdaa](http://104.155.190.166/cps/mobApp/commits/d68cdaa))
* **notify:** add badge support ([8cd6013](http://104.155.190.166/cps/mobApp/commits/8cd6013))
* **notify:** add generic events ([53e7870](http://104.155.190.166/cps/mobApp/commits/53e7870))
* **notify:** add new params event and action ([b3b4ca9](http://104.155.190.166/cps/mobApp/commits/b3b4ca9))
* **notify:** Implement push notification from gcm/apn ([a03b6ba](http://104.155.190.166/cps/mobApp/commits/a03b6ba))
* **parking:** add vehicle from select option ([6b288be](http://104.155.190.166/cps/mobApp/commits/6b288be))
* **parking:** Total implement park/unpark/active ([6f9f075](http://104.155.190.166/cps/mobApp/commits/6f9f075))
* **parking:** vehicle add form directive ([e32ecec](http://104.155.190.166/cps/mobApp/commits/e32ecec))
* **payment:** add paymentCode alert to refund ([2f6d859](http://104.155.190.166/cps/mobApp/commits/2f6d859))
* **payment:** payment code by type ([2639f6c](http://104.155.190.166/cps/mobApp/commits/2639f6c))
* **payment:** payment code confirm with date format ([b8ffdf2](http://104.155.190.166/cps/mobApp/commits/b8ffdf2))
* **payment:** payment history new route ([e3dff02](http://104.155.190.166/cps/mobApp/commits/e3dff02))
* **payment:** send analitycs for paymentCode errors ([dd1c090](http://104.155.190.166/cps/mobApp/commits/dd1c090))
* **payment:** update new history structure response ([4207cad](http://104.155.190.166/cps/mobApp/commits/4207cad))
* **register:** add option to set defualt country/city ([1dd9e7e](http://104.155.190.166/cps/mobApp/commits/1dd9e7e))
* **register:** show or hide email ad check ([f7f47b5](http://104.155.190.166/cps/mobApp/commits/f7f47b5))
* **register|profile:** add validation by input ([e9fa6b0](http://104.155.190.166/cps/mobApp/commits/e9fa6b0))
* **socket:** add off function to stop listener event ([0a8021d](http://104.155.190.166/cps/mobApp/commits/0a8021d))
* **socket:** send platform to join in room ([eec851a](http://104.155.190.166/cps/mobApp/commits/eec851a))
* **ui:** add options to tabsGroup to show or hide dynamic ([bb241d8](http://104.155.190.166/cps/mobApp/commits/bb241d8))
* **ui:** dialog add custom class is button is Close ([5125d9e](http://104.155.190.166/cps/mobApp/commits/5125d9e))
* **ui:** new Tab module ([e96f860](http://104.155.190.166/cps/mobApp/commits/e96f860))
* **ui:** simple view page directive 'cps-page' ([a5e25d4](http://104.155.190.166/cps/mobApp/commits/a5e25d4))
* **utils:** enable or disable geo ([31e1baa](http://104.155.190.166/cps/mobApp/commits/31e1baa))
* **utils:** moment service, filter and directive ([c7292a2](http://104.155.190.166/cps/mobApp/commits/c7292a2))
* **utils:** numbro set only currency if set defaultCurrency option ([7ab9a9c](http://104.155.190.166/cps/mobApp/commits/7ab9a9c))
* **utils:** statusBar plugin ([ed68edd](http://104.155.190.166/cps/mobApp/commits/ed68edd))
* **utils:** support for background mode ([b0c59c4](http://104.155.190.166/cps/mobApp/commits/b0c59c4))
* **view:** enable/disable section ([31125f9](http://104.155.190.166/cps/mobApp/commits/31125f9))
* **zones:** better flow in list ([7eca79b](http://104.155.190.166/cps/mobApp/commits/7eca79b))
* **zones:** zones enable/disable zonesByPosition ([4dbfcb6](http://104.155.190.166/cps/mobApp/commits/4dbfcb6))



<a name="2.0.10"></a>
## [2.0.10](http://104.155.190.166/cps/mobApp/compare/v2.0.9...v2.0.10) (2016-03-30)


### Bug Fixes

* **cli:** extend platforms ([1b9d835](http://104.155.190.166/cps/mobApp/commits/1b9d835))
* **cli:** wacth and add external module ([6eaeff8](http://104.155.190.166/cps/mobApp/commits/6eaeff8))
* **locale/utils:** js crash when numbro try to set unsupporter language ([6afc4cd](http://104.155.190.166/cps/mobApp/commits/6afc4cd))
* **main:** validation new version ([540d2b4](http://104.155.190.166/cps/mobApp/commits/540d2b4))
* **maps:** count userPosition as bound point ([daa91b7](http://104.155.190.166/cps/mobApp/commits/daa91b7))
* **notify:** mapping withdrawal icon/Color ([045b51b](http://104.155.190.166/cps/mobApp/commits/045b51b))
* **profile:** remove One-time bindings ([14db54e](http://104.155.190.166/cps/mobApp/commits/14db54e))
* **register/profile:** allow only numbers input tel ([de5bf85](http://104.155.190.166/cps/mobApp/commits/de5bf85))
* **slide:** force get pagination container ([adaea02](http://104.155.190.166/cps/mobApp/commits/adaea02))
* **slide:** scope directive optional ([2d13904](http://104.155.190.166/cps/mobApp/commits/2d13904))
* **test:** copy example config file ([2689b12](http://104.155.190.166/cps/mobApp/commits/2689b12))
* **zones/maps:** fallback show user if no have points ([0f7da0f](http://104.155.190.166/cps/mobApp/commits/0f7da0f))


### Features

* **ui:** new slide module ([0595223](http://104.155.190.166/cps/mobApp/commits/0595223))
* **utils:** add lodash wrapper filter ([3446ec7](http://104.155.190.166/cps/mobApp/commits/3446ec7))
* **wallet:** add alert when transaction has expired ([b79a407](http://104.155.190.166/cps/mobApp/commits/b79a407))
* **wallet:** check if has pockets ([8d6fe99](http://104.155.190.166/cps/mobApp/commits/8d6fe99))



<a name="2.0.9"></a>
## [2.0.9](http://104.155.190.166/cps/mobApp/compare/v2.0.8...v2.0.9) (2016-03-04)


### Bug Fixes

* **balance:** allow leading zero in trasnfer ([9ecaaad](http://104.155.190.166/cps/mobApp/commits/9ecaaad)), closes [#421](http://104.155.190.166/cps/mobApp/issues/421)
* **balance:** set sum balance after payment or recharge ([81a289d](http://104.155.190.166/cps/mobApp/commits/81a289d)), closes [#415](http://104.155.190.166/cps/mobApp/issues/415)
* **button-bar:** borders back ([9c78254](http://104.155.190.166/cps/mobApp/commits/9c78254))
* **cli:** allow multi task grunt ([f47441b](http://104.155.190.166/cps/mobApp/commits/f47441b))
* **cli:** compatibility in cli for windows ([9bfb5ff](http://104.155.190.166/cps/mobApp/commits/9bfb5ff))
* **cli:** copy to correct path static and config ([0c1be9e](http://104.155.190.166/cps/mobApp/commits/0c1be9e))
* **cli:** force cli color and set mode script exec ([c036228](http://104.155.190.166/cps/mobApp/commits/c036228))
* **cli:** get project theme ([9984426](http://104.155.190.166/cps/mobApp/commits/9984426))
* **cli:** grunt warning in npm module ([5df3a9c](http://104.155.190.166/cps/mobApp/commits/5df3a9c))
* **cli:** set chmod ([3203a84](http://104.155.190.166/cps/mobApp/commits/3203a84))
* **cli:** update task for capatibility windows ([fc17835](http://104.155.190.166/cps/mobApp/commits/fc17835))
* **driver:** duplicate vehicles in driver form ([8e0ce34](http://104.155.190.166/cps/mobApp/commits/8e0ce34))
* **driver:** hide driver marker after hide controls ([1af0ca6](http://104.155.190.166/cps/mobApp/commits/1af0ca6))
* **driver:** justify text legals ([6be8ea5](http://104.155.190.166/cps/mobApp/commits/6be8ea5))
* **driver:** remove one-time binding for address ([0d49b33](http://104.155.190.166/cps/mobApp/commits/0d49b33))
* **driver:** update vehicle, add some form styles ([a3a2f8f](http://104.155.190.166/cps/mobApp/commits/a3a2f8f))
* **driver:** wrap long text in confirm dialog ([986b9a8](http://104.155.190.166/cps/mobApp/commits/986b9a8))
* **grunt:** get config and parent ([39cee73](http://104.155.190.166/cps/mobApp/commits/39cee73))
* **grunt:** get cpsConfig.json ([888d85a](http://104.155.190.166/cps/mobApp/commits/888d85a))
* **login:** check if exist user token/secret ([dae1173](http://104.155.190.166/cps/mobApp/commits/dae1173))
* **login:** try to fix 'undefined' user_token ([7931a43](http://104.155.190.166/cps/mobApp/commits/7931a43))
* **main:** ios aes encrypt bloking ([f9269d3](http://104.155.190.166/cps/mobApp/commits/f9269d3))
* **main:** ng bootstrap over body, prevent 'btstrpd' ([b9af48b](http://104.155.190.166/cps/mobApp/commits/b9af48b))
* **maps:** blue map when no get userPosition ([35424bd](http://104.155.190.166/cps/mobApp/commits/35424bd))
* **maps:** correct zoom for point ([414a704](http://104.155.190.166/cps/mobApp/commits/414a704))
* **maps:** disable SDK map by default for ios 9.2 or higer ([582d33f](http://104.155.190.166/cps/mobApp/commits/582d33f))
* **maps:** error remove event listener in js maps ([fc80a2e](http://104.155.190.166/cps/mobApp/commits/fc80a2e))
* **maps:** fix blank infowindow ios ([649c998](http://104.155.190.166/cps/mobApp/commits/649c998))
* **maps:** geocoder remove duplicate city in formatted_address ([80b03e5](http://104.155.190.166/cps/mobApp/commits/80b03e5))
* **maps:** infowindow content wrong display long texts ([6423d5a](http://104.155.190.166/cps/mobApp/commits/6423d5a)), closes [#433](http://104.155.190.166/cps/mobApp/issues/433)
* **maps:** ios/android 4.2 tap map ([465066e](http://104.155.190.166/cps/mobApp/commits/465066e))
* **maps:** keep infowindow in content ([f1ca5bd](http://104.155.190.166/cps/mobApp/commits/f1ca5bd))
* **maps:** long text html infowindow, ([6810ee3](http://104.155.190.166/cps/mobApp/commits/6810ee3))
* **maps:** removeEventListener in js map ([3988a0b](http://104.155.190.166/cps/mobApp/commits/3988a0b))
* **maps:** un resize map after login (ios) ([c787fef](http://104.155.190.166/cps/mobApp/commits/c787fef))
* **maps:** wait to digest finish, prevent white map ([c4b0003](http://104.155.190.166/cps/mobApp/commits/c4b0003)), closes [#84](http://104.155.190.166/cps/mobApp/issues/84)
* **parking:** max-length 'addVehicle' description ([de04964](http://104.155.190.166/cps/mobApp/commits/de04964))
* **parking:** vehicle icon by type ([c29568e](http://104.155.190.166/cps/mobApp/commits/c29568e))
* **parking:** wrao text in rates dialog ([57d1afd](http://104.155.190.166/cps/mobApp/commits/57d1afd)), closes [#431](http://104.155.190.166/cps/mobApp/issues/431)
* **paymentez:** prevent duplicate debit card ([c3d2788](http://104.155.190.166/cps/mobApp/commits/c3d2788)), closes [#409](http://104.155.190.166/cps/mobApp/issues/409)
* **paymentez:** sending installments in recharge ([f508893](http://104.155.190.166/cps/mobApp/commits/f508893))
* **request:** better code check ([88b5f4f](http://104.155.190.166/cps/mobApp/commits/88b5f4f))
* **scroll:** remove overflow scroll hidden for iOS ([d07c0db](http://104.155.190.166/cps/mobApp/commits/d07c0db))
* **socket:** catch values when check if request has error ([adfc91b](http://104.155.190.166/cps/mobApp/commits/adfc91b))
* **socket:** force reconnect and force new connection in latency network ([06b9a1b](http://104.155.190.166/cps/mobApp/commits/06b9a1b))
* **socket:** prevent send undefined user tokens ([3030068](http://104.155.190.166/cps/mobApp/commits/3030068))
* **socket:** set correct auth request ([e3f0388](http://104.155.190.166/cps/mobApp/commits/e3f0388))
* **ui:** check if menu has content ([59e62bb](http://104.155.190.166/cps/mobApp/commits/59e62bb))
* **ui:** correct display hline texts ([949150d](http://104.155.190.166/cps/mobApp/commits/949150d)), closes [#436](http://104.155.190.166/cps/mobApp/issues/436)
* **utils:** aditional validation for windows support ([9444643](http://104.155.190.166/cps/mobApp/commits/9444643))
* **utils:** check null or undefined ([2e8a72e](http://104.155.190.166/cps/mobApp/commits/2e8a72e))
* **utils:** correct resize parent in 'cps-full-height' ([db334a1](http://104.155.190.166/cps/mobApp/commits/db334a1))
* **utils:** disable by default resize event in 'cps-full-height' ([27e9e51](http://104.155.190.166/cps/mobApp/commits/27e9e51))
* **utils:** enable keyboard accessory for ios ([ade5626](http://104.155.190.166/cps/mobApp/commits/ade5626))
* **utils:** geo enable/disable watch on resume/pause app ([21b4cd6](http://104.155.190.166/cps/mobApp/commits/21b4cd6))
* **utils:** get keyboard plugin ([8380d60](http://104.155.190.166/cps/mobApp/commits/8380d60))
* **utils:** Handling Select/options in iOS Keyboards ([f8e2938](http://104.155.190.166/cps/mobApp/commits/f8e2938))
* **utils:** keyborad check to prevent execption ([b4bc82b](http://104.155.190.166/cps/mobApp/commits/b4bc82b))
* **utils:** uglify get files ([1374380](http://104.155.190.166/cps/mobApp/commits/1374380))
* **windows:** lib allow dinamyc html injection ([bc31c4a](http://104.155.190.166/cps/mobApp/commits/bc31c4a))
* **zones:** bounds last position ([e64b240](http://104.155.190.166/cps/mobApp/commits/e64b240))
* **zones:** Hide spinner on reques error ([890e4fe](http://104.155.190.166/cps/mobApp/commits/890e4fe))
* **zones:** prevent duplicate request 'zonesByPosition' ([b5224e8](http://104.155.190.166/cps/mobApp/commits/b5224e8))


### Features

* **balance:** add 'cu' reciver in transfer ([8f598cf](http://104.155.190.166/cps/mobApp/commits/8f598cf))
* **balance:** add reload button for get balance ([a64d0f8](http://104.155.190.166/cps/mobApp/commits/a64d0f8))
* **cli:** add feature to install only base ([4c6172e](http://104.155.190.166/cps/mobApp/commits/4c6172e))
* **cli:** concat js file to generate 'cps' bindle file ([d139749](http://104.155.190.166/cps/mobApp/commits/d139749))
* **cli:** enable colors grunt ([5d5e5c7](http://104.155.190.166/cps/mobApp/commits/5d5e5c7))
* **cli:** generate exec script for unix ([fe01cc7](http://104.155.190.166/cps/mobApp/commits/fe01cc7))
* **cli:** get cmd main name ([a7aff39](http://104.155.190.166/cps/mobApp/commits/a7aff39))
* **cli:** install platforms on demand ([b2505ff](http://104.155.190.166/cps/mobApp/commits/b2505ff))
* **cli:** new copy hooks ([cf6657b](http://104.155.190.166/cps/mobApp/commits/cf6657b))
* **cli:** npm install --base ([eaf448c](http://104.155.190.166/cps/mobApp/commits/eaf448c))
* **cli:** parent path ([a92f961](http://104.155.190.166/cps/mobApp/commits/a92f961))
* **cli:** send platform parameter in grunt ([1052ad2](http://104.155.190.166/cps/mobApp/commits/1052ad2))
* **core:** return platform data on startup ([3d0b9ef](http://104.155.190.166/cps/mobApp/commits/3d0b9ef))
* **driver:** add options to set default zoom in sdk map ([1702d6f](http://104.155.190.166/cps/mobApp/commits/1702d6f))
* **driver:** parse address with correct format ([c97cc01](http://104.155.190.166/cps/mobApp/commits/c97cc01))
* **main:** enable/disable fullScreen on startup ([2f9d012](http://104.155.190.166/cps/mobApp/commits/2f9d012))
* **maps:** add support 'panTo' in SDK map ([2c03fbd](http://104.155.190.166/cps/mobApp/commits/2c03fbd)), closes [#450](http://104.155.190.166/cps/mobApp/issues/450)
* **maps:** change 'setCenter' for 'panTo' in js map ([c41e566](http://104.155.190.166/cps/mobApp/commits/c41e566))
* **maps:** limit to bounds and fallback user position ([475877b](http://104.155.190.166/cps/mobApp/commits/475877b))
* **maps): padding in bounds - fix(driver:** better way to set ([997f8ea](http://104.155.190.166/cps/mobApp/commits/997f8ea))
* **menu:** better scroll restrict. ([a9c4c54](http://104.155.190.166/cps/mobApp/commits/a9c4c54))
* **notify:** use class to managment tray status ([356c68d](http://104.155.190.166/cps/mobApp/commits/356c68d))
* **parking:** add 'update vehicle' dialog and request ([eaae4be](http://104.155.190.166/cps/mobApp/commits/eaae4be))
* **parking:** add bindonce to directive ([3550324](http://104.155.190.166/cps/mobApp/commits/3550324))
* **parking:** re-enabled aditional data vehicles add ([12bd704](http://104.155.190.166/cps/mobApp/commits/12bd704))
* **paymentez:** add 'buyer_fiscal_number' in debitCard ([48dcdd9](http://104.155.190.166/cps/mobApp/commits/48dcdd9)), closes [#410](http://104.155.190.166/cps/mobApp/issues/410)
* **paymentez:** add Installments number in debit ([6cdbcdc](http://104.155.190.166/cps/mobApp/commits/6cdbcdc))
* **utils:** add option to disable resize event ([2d57ba2](http://104.155.190.166/cps/mobApp/commits/2d57ba2))
* **utils:** utilsService#internalError send code argument to alert ([73c1314](http://104.155.190.166/cps/mobApp/commits/73c1314))
* **wallet:** add pocket payment info ([e37e79d](http://104.155.190.166/cps/mobApp/commits/e37e79d))


<a name="2.0.8"></a>
# 2.0.8 (2015-11-25)

### Bug Fixes
* **maps:** Render map, marker
* **ui:** flex css animations [android]
* **main:** Create migration data
* **maps:** White map in android 4.3 or below [android]
* Fix Several bugfixed

### Features
* **maps:** Add route map
* **paymentez:** Add paymentez module
* **ui:** Modal allow multple modals
* **ui:** New ui options
* **core:** Several new options


<a name="2.0.7"></a>
# 2.0.7 (2015-08-15)

### Bug Fixes
* **utils:** Clean form in second send
* **ui:** flex css support for android 4.3 or below [android]
* **locale:** Support for old language storage
* **maps:** White map in android 4.3 or below [android]
* **core:** Add second password logic
* **wallet:** Add withdrawal support
* Fix Several bugfixed


### Features
* **utils:** Add numbro service with custom language support


<a name="2.0.6"></a>
# 2.0.6 (2015-01-16)

### Bug Fixes
* **utils:** Error when try to update user position

### Features
* **cli:** Add new cps installer
* **wallet:** New wallet module
* **ui:** New popmenu module


<a name="2.0.5"></a>
## 2.0.5 (2015-05-25)

### Features
* **cli:** New installer command 'cps install'

### Changed
* Better organized directories
* Totally independent Framework


<a name="2.0.0"></a>
# 2.0.0 (2015)

### Changed
* Migration to independent mobile framework
