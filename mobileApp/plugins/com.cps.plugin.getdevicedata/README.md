cordova deviceData
=====================

Cordova plugin to retrieve extra user and device info like (account, profile and sim)

## Installing the plugin
To add this plugin just type:
```./cps plugin add http://104.155.190.166/cps/cordova-plugin-getdevicedata.git```

To remove this plugin type:
```./cps plugin remove com.cps.plugin.getdevicedata```

The plugin has the following method only:

## Usage

### Methods
* [cordova.plugins.deviceData.get(successCb, errorCb)](#DeviceData.init)
Retrive data from user/device

* [cordova.plugins.deviceData.servicesAvailable(successCb, errorCb)](#DeviceData.servicesAvailable)
Checks services installed, currently only if Google Play Services is installed

### Objects (Read-Only)
* [cordova.plugins.deviceData](#DeviceData)
Check if plugin is available and return data like email, phone, displayName, servicesAvailable

## Version 0.8
Tested with Cordova >= 4.0
