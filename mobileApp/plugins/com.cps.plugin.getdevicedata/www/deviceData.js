var argscheck = require("cordova/argscheck"),
    utils = require("cordova/utils"),
    exec = require("cordova/exec"),
    cordova = require("cordova"),
    noop = function(){};

/**
 * This represents the user data and provides user info
 * @constructor
 */
function DeviceData() {
    this.available = false;
    this.email = null;
    this.phone = null;
    this.displayName = null;
    this.profilePhone = null;
    self.activity = null;
    self.servicesAvailable = {};
}

/**
 * @name DeviceData.get
 * @description Init plugin and get data
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
DeviceData.prototype.get = function(successCallback, errorCallback){
    var self = this;

    successCallback = successCallback || noop;
    errorCallback = errorCallback || noop;

    // get data
    getData(function(info) {
        self.available = true;
        self.email = info.email;
        self.phone = info.phone;
        self.displayName = info.displayName;
        self.profilePhone = info.profilePhone;
        self.activity = info.activity;
        self.services = info.services;
        // cb success
        successCallback(self);
    }, function(e) {
        self.available = false;
        // cb error
        console.warn("[ERROR] Error initializing Cordova: " + e);
        errorCallback(e);
    });
};
DeviceData.prototype.init = function(successCallback, errorCallback) {
    console.warn("[DEPRECATED] Use 'DeviceData.get' instead 'DeviceData.init'");
    this.get(successCallback, errorCallback);
}

/**
 * @name DeviceData.servicesAvailable
 * @description Get user/device info
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
DeviceData.prototype.servicesAvailable = function (successCallback, errorCallback) {
    successCallback = successCallback || noop;
    errorCallback = errorCallback || noop;

    argscheck.checkArgs("fF", "DeviceData.servicesAvailable", arguments);
    exec(successCallback, errorCallback, "DeviceData", "servicesAvailable", []);
};

/**
 * @private
 *
 * @name getData
 * @description Get user/device info
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
function getData(successCallback, errorCallback) {
    argscheck.checkArgs("fF", "getData", arguments);
    exec(successCallback, errorCallback, "DeviceData", "getData", []);
};

module.exports = new DeviceData();
