# Release v2.0.4 [04/03/2016]

- [feature] button refresh balance if no connect
- [fix] get balance from socket
- [fix] enable ajustPan and option fullscreen [android]
- [fix] android 6.0 compatibility [android]
- [fix] reduce font size to prevent break words [driver]

# Release v2.0.3 [12/02/2016]

- [feature] recharge/payment with installments
- [feature] Set default js map in IOS 9.2.x
- [fix] Several bug fixes IOS 9.2
- [fix] Several bug fixes in map

## Downloads
[CityParking-2.0.3-200080.ipa](/uploads/4de6485e6a40a32c8fbcdc8bd48ac89e/CityParking-2.0.3-200080.ipa)

[CityParking-2.0.3-200030-release.apk](/uploads/8df526328c974e40059212483bc81a85/CityParking-2.0.3-200030-release.apk)
