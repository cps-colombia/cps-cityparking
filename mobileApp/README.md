CityParking APP
======================

*Version 2.0.4*

Install
---------

### Requirements ###
- Node.js v0.10.x or higher

### Add mobApp Framework ###

Enter to project directory

```shell
cd cityparking_mobile
```
### As gitsubmodule
init, sync and update git submodules for install [CPS Mobile Framework](http://96.45.176.18:4000/cps/mobApp)

```shell
git submodule init
git submodule sync
git submodule update
```
### As npm package

```shell
npm install
```

### Installation ###
See installation instructions [here](http://96.45.176.18:4000/cps/mobApp/blob/master/README.md)
