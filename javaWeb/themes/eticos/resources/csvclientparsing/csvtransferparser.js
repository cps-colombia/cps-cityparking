function CsvTransferParser() {
	this.parserCsvFromFile = function(file) {

		var rowsToParse = 0;
		var rowsWithParseError = 0;
		var rowsProccessed = 0;

		var parseValidFile = function() {
			console.log("Procesando...");
			rowsProccessed = 0;
			Papa.parse(file, {
				delimiter : ",",
				newline : "",
				step : function(results, parser) {
					bbAnalizeTransferData([ {
						name : "data",
						value : JSON.stringify(results)
					}, {
						name : "rowsToParse",
						value : rowsToParse
					}, {
						name : "rowsProcessed",
						value : rowsProccessed++
					} ]);

				}
			})

		}

		if (file) {
			Papa.parse(file, {
				download : true,
				newline : "",
				complete : function(results, file) {
					rowsWithParseError = results.errors.length;
					rowsToParse = results.data.length - 1;
					console.log("Lineas a analizar :" + rowsToParse);
					console.log("Lienas con errores: " + rowsWithParseError);
					// Validate results
					if (rowsWithParseError > 0 || rowsToParse == 0) {
						PF('transferErrorDialogWg').show();
						return false;
					} else {
						parseValidFile();
					}
				}
			});
		}
	}
	this.processTransfer = function(xhr, status, args) {
		console.log(args.data);
		var data = JSON.parse(args.data);
		console.log(data.itemToProccess);
		if (data.itemToProccess <= data.totalItems) {
			bbTransferData([ {
				name : "itemToProccess",
				value : data.itemToProccess
			}, {
				name : "totalItems",
				value : data.totalItems
			} ])
		} else  { 
			PF('dlgFileSuccessFailTransfer').show();
		}
	}
}

this.csvtp = new CsvTransferParser();