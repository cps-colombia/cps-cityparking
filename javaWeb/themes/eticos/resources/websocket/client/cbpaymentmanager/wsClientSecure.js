function WsClientSecure() {
	var authData;
	var socket;
	var transactionData;
	var rechargeData;
	var socketUrl;

	this.requestForPayDrawConfirmation = function() {
		bbPrepareTransactionDataForJs();
	}

	var connectToWebSocket = function() {

		socket = io(socketUrl);
		sessionStorage.setItem('socket', socket);
		socket
				.on(
						"connect",
						function(data) {
							socket
									.emit(
											"$cps:authenticate",
											{
												user_token : authData.user_token,
												user_secret : authData.user_secret,
												app_token : authData.app_token,
												app_secret : authData.app_secret,
												validate : authData.validate
											},
											function(data) {
												if (data.statusCode != 200) {
													error = "Error en la autenticacin con el WS: "
															+ data.description
													// console.log(error);
													alert(error);
												} else {
													// Llamo procedimiento de
													// backbean para enviar los
													// datos de la compra
													console
															.log("Procedere a enviar los datos de la transaccion...");

													bbPrepareTransactionDataForJs();

												}
											});
						})
				.on("disconnect", function(data) {
					// console.log("Me acabo de desconectar :(");
				})
				.on("reconnect", function(data) {
					console.log("Se reconecto");
				})
				.on(
						"$cps:notify:new",
						function(data) {
							console
									.log("recibiendo datos de confirmacion del comprador: "
											+ JSON.stringify(data));
							if (data.data[0].full_data.status) {
								console
										.log("enviando confirmacion de notificacion de aceptacion o rechazo recibida");
								socket
										.emit(
												"$cps:notify:update",
												{
													status : "received",
													notify : {
														idsys_user : data.data[0].idsys_user,
														id : data.data[0].id
													}
												},
												function(data) {
													console
															.log("recibiendo datos  de update : "
																	+ JSON
																			.stringify(data))
												});
								/*
								 * hide the "waiting for response" dialog
								 */
								PF('dlgTransactionRequestReceivedByServer')
										.hide();
								/*
								 * show approve/reject notification dialog and
								 * print customer transaction ticket
								 */
								var pocketId = null;
								if (data.data[0].full_data.status != 'rejected') {
									pocketId = data.data[0].full_data.balance.pockets.pocket_id;
								}

								bbNotifyAceptReject([
										{
											name : "transactionType",
											value : data.data[0].op
										},
										{
											name : "transactionValue",
											value : data.data[0].full_data.reference_id
										},
										{
											name : "pocketNumber",
											value : pocketId
										},
										{
											name : "transactionResult",
											value : data.data[0].full_data.status
										} ]);
							}
						});
	}

	this.wsSetAuthData = function(xhr, status, args) {
		var wsAuthResponse = JSON.parse(args.wsAuthResponse);
		console.log(wsAuthResponse);
		if (wsAuthResponse.responseMsj == "OK") {
			authData = wsAuthResponse.responseObj;
			socketUrl = authData.wsUrl;
			// console.log("recibi: " + authData.user_token);
			connectToWebSocket();
		} else {
			alert("No se pudo conectar");
		}
	}

	var wsEmitTransactionDataToServer = function() {
		// console.log("Enviando datos de la transaccion al servidor");
		if (typeof socket === 'undefined') {
			bbPrepareAuthDataForJs();

		} else {

			var eventToSend = transactionData.operationType;
			var jsonTransactionData;
			if (eventToSend == "PAGO" || eventToSend == "RETIRO") {
				jsonTransactionData = {
					branch_id : transactionData.branch_id,
					reference_id : transactionData.reference_id,
					amount : transactionData.amount,
					phone : transactionData.phone
				}
			} else {
				jsonTransactionData = {
					date : transactionData.date_transaction,
					identifier : transactionData.identifier,
					amount : transactionData.amount,
					branch_id : transactionData.branch_id,
					pocket_id : transactionData.pocketId,
					means : transactionData.means,
					reference_id : transactionData.reference_id
				}
			}
			console.log(eventToSend);
			if (eventToSend == "PAGO") {
				console.log("enviando pago...")
				eventToSend = "$cps:paymentConfirm:send";
			} else if (eventToSend == "RETIRO") {
				console.log("enviando retiro...")
				eventToSend = "$cps:withdrawalConfirm:send";
			} else {
				console.log("enviando recarga...")
				eventToSend = "$cps:recharges:send";
			}

			socket
					.emit(
							eventToSend,
							jsonTransactionData,
							function(data) {
								console.log(JSON.stringify(data));
								if (data.statusCode == "200") {
									// if server received the request and send
									// and answer ...
									if (eventToSend == "$cps:recharges:send") {
										// and transaction type sended to server
										// was a recharge, then no more answer
										// expected
										bbNotifyAceptReject([ {
											name : "transactionType",
											value : "rechargue"
										}, {
											name : "transactionValue",
											value : data.data.reference_id
										}, {
											name : "transactionResult",
											value : "accepted"
										} ]);
										// alert('Recarga exitosa : por ' +
										// data.data.amount);
									} else {
										// else wait for customer response (the
										// response from customer will come like
										// a "$cps:notify:new" event)
										bbSuccessTransactionResponseListener([ {
											name : "wsSuccessTransactionResponse",
											value : JSON.stringify(data.data)
										} ])
									}
								} else {
									// Mostrar el error correspondiente ...
									alert(JSON
											.stringify("Ha ocurrido un error :"
													+ data.description));
									alert("Error despues del envio de los datos de la transacion : "
											+ data.statusCode
											+ ":"
											+ data.description);
								}
							});
		}
	}

	this.wsSetTransactionData = function(xhr, status, args) {
		var wsTransactionResponse = JSON.parse(args.wsTransactionResponse);
		// console.log(wsTransactionResponse);
		if (wsTransactionResponse.responseMsj == "OK") {
			transactionData = wsTransactionResponse.responseObj;
			// console.log("recibi: " + transactionData.amount);
			wsEmitTransactionDataToServer();
		} else {
			alert("Hubo un problema con los datos de la transaccion");
		}
	}
}
this.wcs = new WsClientSecure();

/**
 * 
 */
