/**
 * 
 */

var authData;
var transactionData
var socket;

function requestForPayDrawConfirmation() {
	// Esta es la funcion que llama al backbean de la vista para setear las
	// credenciales de usuario
	bbPrepareAuthDataForJs();
}

function connectToWebSocket() {

	socket = io("http://96.45.176.18:3030/cps");
	socket
			.on(
					"connect",
					function(data) {
						//console.log("evento");
						socket
								.emit(
										"$cps:authenticate",
										{
											user_token : authData.user_token,
											user_secret : authData.user_secret,
											app_token : authData.app_token,
											app_secret : authData.app_secret,
											validate : authData.validate
										},
										function(data) {
											if (data.statusCode != 200) {
												error = "Error en la autenticacin con el WS: "
														+ data.description
												//console.log(error);
												alert(error);
											} else {
												// Llamo procedimiento de
												// backbean para enviar los
												// datos de la compra
												console
														.log("Procedere a enviar los datos de la transaccion...");

												bbPrepareTransactionDataForJs();

											}
										});
					}).on("disconnect", function(data) {
				//console.log("Me acabo de desconectar :(");

			});

}

function wsSetAuthData(xhr, status, args) {
	//console.log(args);
	var wsAuthResponse = JSON.parse(args.wsAuthResponse);
	//console.log(wsAuthResponse);
	if (wsAuthResponse.responseMsj == "OK") {
		authData = wsAuthResponse.responseObj;
		//console.log("recibi: " + authData.user_token);
		connectToWebSocket();
	} else {
		alert("No se pudo conectar");
	}
}

function wsSetTransactionData(xhr, status, args) {
	//console.log(args);
	var wsTransactionResponse = JSON.parse(args.wsTransactionResponse);
	//console.log(wsTransactionResponse);
	if (wsTransactionResponse.responseMsj == "OK") {
		transactionData = wsTransactionResponse.responseObj;
		//console.log("recibi: " + transactionData.amount);
		wsEmitTransactionDataToServer();
	} else {
		alert("Hubo un problema con los datos de la transaccion");
	}
}

function wsEmitTransactionDataToServer() {
	//console.log("Enviando datos de la transaccion al servidor");
	socket
			.emit(
					"$cps:paymentConfirm:send",
					{
						commerce_id : transactionData.commerce_id,
						branch_id : transactionData.branch_id,
						pay_id : transactionData.pay_id,
						amount : transactionData.amount,
						phone : transactionData.phone
					},
					function(data) {
//						console
//								.log("Reciviendo datos del evento de confirmacion de la transaccion : "
//										+ JSON.stringify(data));
						if (data.statusCode == "200") {
							//success
							bbSuccessTransactionResponseListener([ {
								name : "wsSuccessTransactionResponse",
								value : JSON.stringify(data.data)
							} ])

						} else {
							// Mostrar el error correspondiente ...

						}

					});

}

//$(document).ready(requestForPayDrawConfirmation);
