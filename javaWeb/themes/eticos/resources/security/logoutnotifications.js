window.addEventListener("beforeunload", function(e) {
	setCookie("unloadtime", new Date().getTime(), 2) ;
	console.log("unload time: " + getCookie("unloadtime"));
});

window.onclick = function(e) {
	onclickToReload();
};

function onclickToReload() {
	setCookie("clicktime", new Date().getTime(), 2) ;
	console.log("click time: " + getCookie("clicktime"));
}

function onload() {
	var unloadtime = getCookie("unloadtime");
	var clicktime = getCookie("clicktime");
	var deltat = unloadtime - clicktime;
	console.log("Diferencia de tiempos unload-click(" + unloadtime + ","
			+ clicktime + "): " + deltat);
	if (deltat > 1000) {
		console.log("Detectado cierre...") ; 
		setCookie("clicktime", new Date().getTime(), 2) ; 
		notifyOnCloseBrowserToServer();
	}
}

function getCookie(cname) {
	var name = cname + "=";
	console.log(document.cookie);
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}

function setCookie(c_name, value, exdays) {

	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value)
			+ ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value + "; path=/";
}
