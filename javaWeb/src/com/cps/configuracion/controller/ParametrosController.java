package com.cps.configuracion.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


import org.apache.log4j.Logger;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Country;
import com.cps.remote.CountryBeanRemote;

@ManagedBean
@RequestScoped
public class ParametrosController {

	private CountryBeanRemote countryBeanRemote;
	private List<Country> listSelectCountries;
	private Logger LOGGER = Logger.getLogger(ParametrosController.class);
	
	

	public List<Country> getListCountries() {
		if (listSelectCountries == null) {
			try {
				listSelectCountries = new ArrayList<Country>();
				countryBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(CountryBeanRemote.class);
				listSelectCountries = countryBeanRemote.getCountrys();
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("No fue posible cargar el listado de paise", e);
				listSelectCountries = Collections.<Country>emptyList();
			}
		}
		return listSelectCountries;
	}
	
	/*
	public List<SelectItem> getListCountries() {
		if (listSelectCountries != null) {
			try {
				listSelectCountries = new ArrayList<SelectItem>();
				countryBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(CountryBeanRemote.class);
				List<Country> listCountries = countryBeanRemote.getCountrys();
				SelectItem item;
				for (Country country : listCountries) {
					item = new SelectItem(country.getIdcountry(), country.getName());
					listSelectCountries.add(item);
				}
			} catch (Exception e) {
				LOGGER.error("No fue posible cargar el listado de paise", e);
				listSelectCountries = Collections.<SelectItem>emptyList();
			}
		}
		return listSelectCountries;
	}
	*/

}
