package com.cps.configuracion.controller;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.cps.util.JsfUtil;

@ManagedBean
@SessionScoped
public class FormSettings implements Serializable {

	private static final long serialVersionUID = 1L;
	private Locale selectedLocale = Locale.ENGLISH;
	
	public Locale getLocale() {
		return selectedLocale;
	}
	
	public void swapLocale(ActionEvent event) {
		String id = event.getComponent().getId();
		selectedLocale = new Locale(id);
		JsfUtil.setSessionAtribute("multilang.code", id);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(selectedLocale);
	}
}