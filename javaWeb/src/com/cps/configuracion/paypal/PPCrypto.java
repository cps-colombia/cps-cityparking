/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.cps.configuracion.paypal;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.CollectionCertStoreParameters;
import sun.misc.BASE64Decoder;

import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.util.encoders.Base64;



/**
 * PayPal cryptography routines
 * @author PayPal SDK Team
 */
public class PPCrypto
{


    /**
     * Sign and Envelope the passed data string, returning a PKCS7 blob that can be posted to PayPal.
     * Make sure the passed data string is seperated by UNIX linefeeds (ASCII 10, '\n').
     * @param data button data
     * @param certificatePath location of certificate
     * @param paypalCertificatePath location of PayPal public certificate
     * @param privateKeyPassword private key password
     * @return encrypted button value in encrypted PKCS7 format
     * @throws IOException
     * @throws CertificateException
     * @throws KeyStoreException
     * @throws UnrecoverableKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws CertStoreException
     * @throws CMSException
     */
    public static byte[] getButtonEncryptionValue(byte[] data, String certificatePath,
            String paypalCertificatePath, String privateKeyPassword) throws IOException,
            CertificateException, KeyStoreException, UnrecoverableKeyException,
            InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException,
            CertStoreException, CMSException
    {

        // make sure bouncycastle has been installed as a security provider
        if (Security.getProvider("BC") == null)
        {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }

        // Read the Private Key
        KeyStore ks = KeyStore.getInstance("PKCS12", "BC");
        ks.load(new FileInputStream(certificatePath), privateKeyPassword.toCharArray());
        String keyAlias = null;
        Enumeration aliases = ks.aliases();
        while (aliases.hasMoreElements())
        {
            keyAlias = (String) aliases.nextElement();
        }

        // User certificate
        X509Certificate certificate = (X509Certificate) ks.getCertificate(keyAlias);

        // User private key
        PrivateKey privateKey = (PrivateKey) ks.getKey(keyAlias, privateKeyPassword.toCharArray());

        // Use CertificateFactory to read the public certs
        /*CertificateFactory cf = CertificateFactory.getInstance("X509", "BC");

         // merchant certificate
         X509Certificate certificate = (X509Certificate) cf.generateCertificate(
         new FileInputStream(certificatePath));*/

        // PayPal certificate
        CertificateFactory cf = CertificateFactory.getInstance("X509", "BC");
        X509Certificate paypalCertificate = (X509Certificate) cf
                .generateCertificate(new FileInputStream(paypalCertificatePath));

        // Sign the Data
        CMSSignedDataGenerator signedGenerator = new CMSSignedDataGenerator();
        signedGenerator.addSigner(privateKey, certificate, CMSSignedDataGenerator.DIGEST_SHA1);
        ArrayList certList = new ArrayList();
        certList.add(certificate);
        CertStore certStore = CertStore.getInstance("Collection",
                new CollectionCertStoreParameters(certList));
        signedGenerator.addCertificatesAndCRLs(certStore);

        CMSProcessableByteArray cmsByteArray = new CMSProcessableByteArray(data);

        CMSSignedData signedData = signedGenerator.generate(cmsByteArray, true, "BC");
        byte[] signed = signedData.getEncoded();

        CMSEnvelopedDataGenerator envGenerator = new CMSEnvelopedDataGenerator();
        envGenerator.addKeyTransRecipient(paypalCertificate);
        CMSEnvelopedData envData = envGenerator.generate(new CMSProcessableByteArray(signed),
                CMSEnvelopedDataGenerator.DES_EDE3_CBC, "BC");

        byte[] pkcs7Bytes = envData.getEncoded();

        // Return the encoded enveloped data as a PKCS7 PEM Blob
        return DERtoPEM(pkcs7Bytes, "PKCS7");
    } // getButtonEncryptionValue

    /**
     * Convert a DER byte array to a PEM Blob with the specifed Header
     * @param bytes DER byte array to convert
     * @param headfoot Header to include.  ie "PKCS7"
     * @return PEM byte array
     */
    public static byte[] DERtoPEM(byte[] bytes, String headfoot)
    {
        ByteArrayOutputStream pemStream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(pemStream);

        byte[] stringBytes = Base64.encode(bytes);
        String encoded = new String(stringBytes);

        if (headfoot != null)
        {
            writer.print("-----BEGIN " + headfoot + "-----\n");
        }

        // write 64 chars per line till done
        int i = 0;
        while ((i + 1) * 64 <= encoded.length())
        {
            writer.print(encoded.substring(i * 64, (i + 1) * 64));
            writer.print("\n");
            i++;
        }
        if (encoded.length() % 64 != 0)
        {
            writer.print(encoded.substring(i * 64)); // write remainder
            writer.print("\n");
        }
        if (headfoot != null)
        {
            writer.print("-----END " + headfoot + "-----\n");
        }
        writer.flush();
        return pemStream.toByteArray();
    } // DERtoPEM

} // PPCrypto