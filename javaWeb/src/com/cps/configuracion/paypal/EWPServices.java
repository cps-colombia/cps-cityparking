/*
 * Copyright 2005 PayPal, Inc. All Rights Reserved.
 */

package com.cps.configuracion.paypal;

/**
 * Provides methods for generating Encrypted Website Payments code.
 *
 */
public class EWPServices
{

    /**
     * Default constructor does nothing.
     */
    public EWPServices()
    {
    }

    public String encryptButton(byte[] contentbytes, String ewpCertPath, String privateKeyPWD, String paypalCertPath, String url, String buttonImage) throws Exception
    {

        try
        {
            String encrypted = new String(PPCrypto.getButtonEncryptionValue(contentbytes, ewpCertPath, paypalCertPath, privateKeyPWD));

            if ((encrypted != null) && (encrypted.length() > 0))
            {
                StringBuffer sb = new StringBuffer();
                sb.append("<form action=\"");
                sb.append(url);
                sb.append("/cgi-bin/webscr\" method=\"post\">");
                sb.append("<input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">");
                ;
                sb.append("<input type=\"image\" src=\"");
                sb.append(buttonImage);
                sb.append("\" border=\"0\" name=\"submit\" ");
                sb.append("alt=\"Make payments with PayPal - it's fast, free and secure!\">");
                sb.append("<input type=\"hidden\" name=\"encrypted\" value=\"");
                sb.append(encrypted);
                sb.append("\">");
                sb.append("</form>");
                return sb.toString();
            }
            else
            {
                throw new Exception(MessageResources.getMessage("ENCRYPTION_ERROR"));
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage(), e);
        }

    } // encryptButton
} // EWPServices