/**
 *
 */
package com.cps.seguridad.controller;

import java.util.Calendar;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.TokenCheck;
import com.cps.remote.SysUserBeanRemote;
import com.cps.security.RampENC;
import com.cps.util.CodeResponse;
import com.cps.util.JsfUtil;
import com.cps.util.MailSender;
import com.cps.util.Util;

/**
 * @author Jorge
 *
 */
@ManagedBean
@ViewScoped
public class ChangePasswordController {

	private String currentPass;
	private String newPass;
	private String reTypePass;
	private String recoverInfo = "";
	private SysUserBeanRemote sysUserBeanRemote;
	private String loginId;
	private boolean enableButton = false;

	public ChangePasswordController() {

	}

	public void changePass(ActionEvent event) {

		try {
			if (newPass.equals(reTypePass)) {
				SysUser sysUser = (SysUser) JsfUtil
						.getSessionAtribute("userSession");
				sysUserBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(SysUserBeanRemote.class);
				int codeResp = sysUserBeanRemote.setPassword(
						sysUser.getLogin(), currentPass, newPass);
				if (codeResp == CodeResponse.PASSWORD_SET_OK) {
					JsfUtil.addMessage("changePassSucces",
							FacesMessage.SEVERITY_INFO);
					String subject = JsfUtil.getMessage("changePassSubject",
							FacesMessage.SEVERITY_INFO).getSummary();
					String body = Util
							.getTemplateString("newsletter/emailPassword.xhtml")
							.replace("{cps_login}", sysUser.getLogin())
							.replace("{cps_newPass}", newPass);
					String sender = JsfUtil.getMessage("forgotMailFrom",
							FacesMessage.SEVERITY_INFO).getSummary();
					MailSender.sendMail(subject, body, sender,
							sysUser.getEmail());

				} else {
					JsfUtil.addMessage("changePassFailure",
							FacesMessage.SEVERITY_ERROR);
				}
			} else {
				JsfUtil.addMessage("changePassNotMatch",
						FacesMessage.SEVERITY_ERROR);
			}
			this.currentPass = null;
			this.newPass = null;
			this.reTypePass = null;

		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}

		// return "";
	}

	// <f:event type="preRenderView" listener="#{permissionManager.checkRoles}"
	// />

	public void checkValidData() {
		if (recoverInfo != null&&loginId==null) {
			try {
				sysUserBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(SysUserBeanRemote.class);

				String decodedInfo = RampENC.decrypt(recoverInfo);

				String[] decodedInfoArr = decodedInfo.split(",");

				long timeExp = Long.parseLong(decodedInfoArr[0]);
				loginId = decodedInfoArr[1];
				long currtime = Calendar.getInstance(
						TimeZone.getTimeZone("GMT")).getTimeInMillis();
				if (currtime >= timeExp||sysUserBeanRemote.tokenExist(recoverInfo)) {
					JsfUtil.addMessage("recoverPassFailure.token",
							FacesMessage.SEVERITY_ERROR);
				} else {

					sysUserBeanRemote.addToken(new TokenCheck(recoverInfo, timeExp));
					enableButton = true;

					RequestContext.getCurrentInstance().update("login");
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void changePassRecovery() {

		try {
			if (newPass.equals(reTypePass)) {

				sysUserBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(SysUserBeanRemote.class);
				SysUser sysUser = sysUserBeanRemote.getUser(loginId);
				int codeResp = sysUserBeanRemote.setPassword(sysUser, newPass);
				if (codeResp == CodeResponse.PASSWORD_SET_OK) {
					JsfUtil.addMessage("changePassSucces",
							FacesMessage.SEVERITY_INFO);
					String subject = JsfUtil.getMessage("changePassSubject",
							FacesMessage.SEVERITY_INFO).getSummary();
					String body = Util
							.getTemplateString("newsletter/emailPassword.xhtml")
							.replace("{cps_login}", sysUser.getLogin())
							.replace("{cps_newPass}", newPass);
					String sender = JsfUtil.getMessage("forgotMailFrom",
							FacesMessage.SEVERITY_INFO).getSummary();
					MailSender.sendMail(subject, body, sender,
							sysUser.getEmail());
					enableButton=false;
				} else {
					JsfUtil.addMessage("changePassFailure",
							FacesMessage.SEVERITY_ERROR);
				}
			} else {
				JsfUtil.addMessage("changePassNotMatch",
						FacesMessage.SEVERITY_ERROR);
			}
			this.currentPass = null;
			this.newPass = null;
			this.reTypePass = null;
		

		} catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}

	}

	public String getCurrentPass() {
		return currentPass;
	}

	public void setCurrentPass(String currentPass) {
		this.currentPass = currentPass;
	}

	public String getNewPass() {
		return newPass;
	}

	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}

	public String getReTypePass() {
		return reTypePass;
	}

	public void setReTypePass(String reTypePass) {
		this.reTypePass = reTypePass;
	}

	public String getRecoverInfo() {
		return recoverInfo;
	}

	public void setRecoverInfo(String recoverInfo) {
		this.recoverInfo = recoverInfo;
	}

	public boolean isEnableButton() {
		return enableButton;
	}

	public void setEnableButton(boolean enableButton) {
		this.enableButton = enableButton;
	}

}
