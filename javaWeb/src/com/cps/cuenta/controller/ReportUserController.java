/**
 * 
 */
package com.cps.cuenta.controller;

import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.SysUser;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;

/**
 * @author Juan Otero
 * 
 */
@ManagedBean
@ViewScoped
public class ReportUserController implements Serializable {

	private static final long serialVersionUID = 1L;

	private SysUserBeanRemote beanRemote;

	private Date fechaIni;
	private Date fechaFin;
	private int totalUsers;
//	private String name;
//	private String  lastname;
//	private String	address;
//	private int notificationmsj;
//	private String favoriteMsisdn;

	private String year;


	private ArrayList<SysUser> listReportUser;

	public ReportUserController() {
	

		

	}
	
	
	public void reportUsersByDate(ActionEvent event) {
		try {
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
		
			beanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);
			setListReportUser( beanRemote.getReportUsersList( fechaI  + " 00:01:01", fechaF + " 23:59:59" ) );
			
			totalUsers=	listReportUser.size();
//			for (ReportParking element : listReportParking) {
//				totalMinutos = totalMinutos + element.getMinutsRound();
//				totalMoney = totalMoney + element.getTotalParking();
//			}
			
		//	history = listReportParking != null && !listReportParking.isEmpty() ? true : false;

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	

	public void generarInforme(){
        try{
		
		String fechaI = DateHelper.format(getFechaIni(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getFechaFin(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "reportUsers_"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
	//	parameter.put("idZone", idZone);
		parameter.put("fechaIni", fechaI + " 00:01:01");
		parameter.put("fechaFin", fechaF + " 23:59:59");
		
		JasperReportHelper jrh = new JasperReportHelper("reportRegisteredUser", parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}
	}

	
	
	public void generarInforme2(){
        try{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		
		JasperReportHelper jrh = new JasperReportHelper("reportZoneMoreParking", parameter);
       	JasperReportHelper.formatos format = JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		//response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		jrh = null;
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	
	public void generarInformeTotalesVentas(){
        try{
		
		String fechaI = DateHelper.format(getFechaIni(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getFechaFin(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		String idUser = "%";
		ResourceBundle bundle = ResourceBundle.getBundle("cps");

		if (!bundle.getString("cps.user.admin").equals(
				"" + JsfUtil.getUserSession().getIdsysUserType())) {
			idUser = JsfUtil.getUserSession().getIdsysUser().toString();
		} 		
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "totalSalesZones_"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		
		parameter.put("idUser", idUser);
		parameter.put("fechaIni", fechaI + " 00:01:01");
		parameter.put("fechaFin", fechaF + " 23:59:59");
		
		JasperReportHelper jrh = new JasperReportHelper("reportSalesZonesDates", parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void generarInformeSalesMonth(){
        try{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "salesMonth"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		
		parameter.put("year", year);
		
		JasperReportHelper jrh = new JasperReportHelper("reportSalesMonth", parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}


	
	public boolean filterByName(Object value, Object filter, Locale locale) {
	    String filterText = (filter == null) ? null : filter.toString().trim();
	    if (filterText == null || filterText.equals("")) {
	        return true;
	    }

	    if (value == null) {
	        return false;
	    }

	    String carName = value.toString().toUpperCase();
	    filterText = filterText.toUpperCase();

	    if (carName.contains(filterText)) {
	        return true;
	    } else {
	        return false;
	    }
	}
	/*public List<ReportSales> getListReportSales() {
		return listReportSales;
	}

	public void setListReportSales(List<ReportSales> listReportSales) {
		this.listReportSales = listReportSales;
	}*/
	public int getTotalUsers( )
	{
		return totalUsers ;
	}
	
	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}



	public void setYear(String year) {
		this.year = year;
	}

	public String getYear() {
		return year;
	}


	public ArrayList<SysUser> getListReportUser()
	{
		return listReportUser;
	}


	public void setListReportUser( ArrayList<SysUser> listReportUser )
	{
		this.listReportUser = listReportUser;
	}

}
