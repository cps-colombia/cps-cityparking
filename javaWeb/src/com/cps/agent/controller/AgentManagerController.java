/**
 * 
 */
package com.cps.agent.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Agent;
import com.cps.remote.AgentBeanRemote;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 *
 */
@ManagedBean
@ViewScoped
public class AgentManagerController implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(AgentManagerController.class);
	private AgentBeanRemote agentBeanRemote;
	List<Agent> agents;
	private Agent agent = new Agent();
		
	public AgentManagerController(){
		try{
		agentBeanRemote = EJBServiceLocator.getInstace().getEjbService(AgentBeanRemote.class);
		agents = agentBeanRemote.getAgents();
		}catch (Exception e) {
			LOGGER.error("Error al obtener agentes", e);
		}
	}

	public void registerAgent(){
		try{
			agentBeanRemote = EJBServiceLocator.getInstace().getEjbService(AgentBeanRemote.class);
			agent.setStatus("");
			agentBeanRemote.registerAgent(agent);
			JsfUtil.addMessage("registerAgentSucces", FacesMessage.SEVERITY_INFO);

		}catch (Exception e) {
			LOGGER.error("Error al registrar agente", e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}

	}
	
	
	/**
	 * Registrar un nuevo client
	 * @param event
	 */
	public void deleteAgent(ActionEvent event){
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try {
			agentBeanRemote = EJBServiceLocator.getInstace().getEjbService(AgentBeanRemote.class);
			agent.setStatus("I");
			agentBeanRemote.updateAgent(agent);
			agents = agentBeanRemote.getAgents();
			JsfUtil.addMessage("myAgent.deletedAgent", FacesMessage.SEVERITY_INFO);
			flag = true;
		} catch (Exception e) {
            JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
            flag = false;
            e.printStackTrace();
		}
		context.addCallbackParam("flag", flag);
	}
	
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Agent getAgent() {
		return agent;
	}

	public List<Agent> getAgents() {
		return agents;
	}

	public void setAgents(List<Agent> agents) {
		this.agents = agents;
	}


}
