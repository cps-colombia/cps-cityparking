/**
 * 
 */
package com.cps.parking.controller;

import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.CityParkingTransactionUser;
import com.cps.entity.bean.Client;
import com.cps.entity.bean.Platform;
import com.cps.entity.bean.ReportParking;
import com.cps.entity.bean.Zone;
import com.cps.remote.ClientBeanRemote;
import com.cps.remote.ParkingBeanRemote;
import com.cps.remote.PlatformBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 * 
 */
@ManagedBean
@ViewScoped
public class ReportParkingController implements Serializable {

	private static final long serialVersionUID = 1L;

	private ZoneBeanRemote zoneBeanRemote;
	private ParkingBeanRemote parkingBeanRemote;
	private ClientBeanRemote clientBeanRemote;
	private PlatformBeanRemote platformBeanRemote;
	//private PaypalBeanRemote paypalBeanRemote;
	
	private List<ReportParking> listReportParking;
	//private List<ReportSales> listReportSales;
	private List<Zone> listZones;
	private List<Client> listClients;
	private List<Platform> listPlatform;

	private String idZone;
	private Date fechaIni;
	private Date fechaFin;
	private double totalMinutos;
	private double totalMoney;
	private String idClient;
	private boolean admin = false;
	private boolean history = false;
	private String year;
	private int platformId;

	private String cuFilter="";

	private String placeFilter="";

	public ReportParkingController() {
		try {

			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			  platformBeanRemote =  EJBServiceLocator.getInstace()
						.getEjbService(PlatformBeanRemote.class);
			  setListPlatform( platformBeanRemote.getPlatforms() );
			ResourceBundle bundle = ResourceBundle.getBundle("cps");
			if (bundle.getString("cps.user.admin").equals(
					""+JsfUtil.getUserSession().getIdsysUserType())) {
				clientBeanRemote = EJBServiceLocator.getInstace()
				.getEjbService(ClientBeanRemote.class);
		        setListClients(clientBeanRemote.getListClients());
		        setAdmin(true);
			} else {
				setListZones(zoneBeanRemote.getZonesClient(JsfUtil
						.getUserSession()));
			}
			this.setIdZone("%");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Muestra el parqueo activo para una zona.
	 * 
	 * @param event
	 */
	public void changeClient(ValueChangeEvent event) {
		try {
			String clientSelect = String.valueOf(event.getNewValue());
           
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			
			setListZones(zoneBeanRemote.getZonesByIdClient(clientSelect));

			listReportParking = Collections.emptyList(); 

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Muestra el parqueo activo para una zona.
	 * 
	 * @param event
	 */
	public void changePlatform(ValueChangeEvent event) {
		try {
			String clientSelect = String.valueOf(event.getNewValue());
           
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			
			setListZones(zoneBeanRemote.getZonesByIdClient(clientSelect));

			listReportParking = Collections.emptyList(); 

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Muestra el parqueo activo para una zona.
	 * 
	 * @param event
	 */
	public void reportParking(ActionEvent event) {
		try {
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			totalMinutos = 0;
			totalMoney = 0;
			parkingBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ParkingBeanRemote.class);
			listReportParking = parkingBeanRemote.getReportParking(idZone,
					fechaI + " 00:01:01", fechaF + " 23:59:59");
			for (ReportParking element : listReportParking) {
				totalMinutos = totalMinutos + element.getMinutsRound();
				totalMoney = totalMoney + element.getFee();
				
			}
			
			history = listReportParking != null && !listReportParking.isEmpty() ? true : false;

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	
	public void reportParkingAv(ActionEvent event) {
		try {
			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			totalMinutos = 0;
			totalMoney = 0;
			parkingBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ParkingBeanRemote.class);
			listReportParking = parkingBeanRemote.getReportParking(idZone,
					fechaI + " 00:01:01", fechaF + " 23:59:59", platformId);
			
			for (ReportParking element : listReportParking) {
				totalMinutos = totalMinutos + element.getMinutsRound();
				totalMoney = totalMoney + element.getTotalParking();
			}
			
			history = listReportParking != null && !listReportParking.isEmpty() ? true : false;

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	public boolean filterCU(Object value, Object filter, Locale locale) {
	    String filterText = (filter == null) ? null : filter.toString().trim();
	    if (filterText == null) {
	        return true;
	    }

	    if (value == null) {
	        return false;
	    }

	    cuFilter=filterText.toLowerCase();
	    String carName = value.toString().toLowerCase();
	    
 	   //System.out.println("carName...."+ carName);

	    totalMinutos=0;
	    totalMoney=0;
	   
	    if (carName.startsWith(cuFilter.toLowerCase()) || cuFilter.equals("")) {
	    	 applyFilterOperation(); 
	        return true;
	    } 
	      
	    
	    
	 	

	 	  
 	  return false;
	}
	
	public boolean filterPlace(Object value, Object filter, Locale locale) {
		
		System.out.println("filtering place....");
	    String filterText = (filter == null) ? null : filter.toString().trim();
	    if (filterText == null) {
	        return true;
	    }

	    if (value == null) {
	        return false;
	    }

	    placeFilter=filterText.toLowerCase();
	    String carName = value.toString().toLowerCase();
	    
 	   //System.out.println("carName...."+ carName);

	    totalMinutos=0;
	    totalMoney=0;
	   
	    if (carName.startsWith(placeFilter.toLowerCase()) || placeFilter.equals("")) {
	    	 applyFilterOperation(); 
	        return true;
	    } 
	      
	    
	    
	 	

	 	  
 	  return false;
	}
	
	private void applyFilterOperation()
	{
		
		
		  
		    	 
				for (ReportParking element : listReportParking) {
		   	    	if((element.getSysUser().getCu().startsWith(cuFilter)||cuFilter.equals(""))&&
		   	    			(element.getZone().getName().toLowerCase().startsWith(placeFilter)||placeFilter.equals("")))
		   	    			
		   	    	{
		   	    		
		   	    		totalMinutos = totalMinutos + element.getMinutsRound();
						totalMoney = totalMoney + element.getTotalParking();
		   	    	}
		   			//element.getSysUser().
		   			//element.setStateTransaction(getMessageFor("parameter.sales.state."+ element.getStateTransaction().toLowerCase()));
		   			
		   	     
		   			
		   		}
		    	 

		    	   //System.out.println("complete transaction"+ totalCompletedTransacMoney);
		    		 
		    	
				//setSales(listReportCitySales != null && !listReportCitySales.isEmpty() );
				//RequestContext.getCurrentInstance().update("contact-form:tbl:ftr");

		    	   RequestContext.getCurrentInstance().addCallbackParam("totalMinutos"," : "+totalMinutos);
			 	   RequestContext.getCurrentInstance().addCallbackParam("totalMoney"," : "+totalMoney);
			 	  
			 	 
			 	//  System.out.println("numberOfRejecttedTransac="+  getMessageFor("myHistory.numberofrejectedtransac")+" : "+numberOfRejecttedTransac);
		       
		    
		      
		    
	}
	
	
	private String getMessageFor(String key){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msgs");
		    String message = bundle.getString(key);
		    return message;
		}catch (Exception e) {
			return "";
		}
	}
	public void generarInforme(){
        try{
		
		String fechaI = DateHelper.format(getFechaIni(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getFechaFin(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "reportHistory_"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("idZone", idZone);
		parameter.put("fechaIni", fechaI + " 00:01:01");
		parameter.put("fechaFin", fechaF + " 23:59:59");
		String report=(idZone.equals("%"))?"reportParkingHistoryAllZones":"reportParkingHistory";
		JasperReportHelper jrh = new JasperReportHelper(report, parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	
	
	public void generarInforme2(){
        try{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		
		JasperReportHelper jrh = new JasperReportHelper("reportZoneMoreParking", parameter);
       	JasperReportHelper.formatos format = JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		//response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		jrh = null;
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	
	public void generarInformeTotalesVentas(){
        try{
		
		String fechaI = DateHelper.format(getFechaIni(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getFechaFin(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		String idUser = "%";
		ResourceBundle bundle = ResourceBundle.getBundle("cps");

		if (!bundle.getString("cps.user.admin").equals(
				"" + JsfUtil.getUserSession().getIdsysUserType())) {
			idUser = JsfUtil.getUserSession().getIdsysUser().toString();
		} 		
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "totalSalesZones_"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		
		parameter.put("idUser", idUser);
		parameter.put("fechaIni", fechaI + " 00:01:01");
		parameter.put("fechaFin", fechaF + " 23:59:59");
		
		JasperReportHelper jrh = new JasperReportHelper("reportSalesZonesDates", parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void generarInformeSalesMonth(){
        try{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "salesMonth"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		
		parameter.put("year", year);
		
		JasperReportHelper jrh = new JasperReportHelper("reportSalesMonth", parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}


	public void setListZones(List<Zone> listZones) {
		this.listZones = listZones;
	}

	public List<Zone> getListZones() {
		return listZones;
	}

	public void setIdZone(String idZone) {
		this.idZone = idZone;
	}

	public String getIdZone() {
		return idZone;
	}

	public List<ReportParking> getListReportParking() {
		return listReportParking;
	}

	public void setListReportParking(List<ReportParking> listReportParking) {
		this.listReportParking = listReportParking;
	}
	
	/*public List<ReportSales> getListReportSales() {
		return listReportSales;
	}

	public void setListReportSales(List<ReportSales> listReportSales) {
		this.listReportSales = listReportSales;
	}*/
	
	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public void setTotalMinutos(double totalMinutos) {
		this.totalMinutos = totalMinutos;
	}

	public double getTotalMinutos() {
		return totalMinutos;
	}

	public void setTotalMoney(double totalMoney) {
		this.totalMoney = totalMoney;
	}

	public double getTotalMoney() {
		return totalMoney;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	public List<Client> getListClients() {
		return listClients;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getIdClient() {
		return idClient;
	}

	public boolean isHistory() {
		return history;
	}

	public void setHistory(boolean history) {
		this.history = history;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getYear() {
		return year;
	}

	public int getPlatformId()
	{
		return platformId;
	}

	public void setPlatformId( int platformId )
	{
		this.platformId = platformId;
	}

	public List<Platform> getListPlatform()
	{
		return listPlatform;
	}

	public void setListPlatform( List<Platform> listPlatform )
	{
		this.listPlatform = listPlatform;
	}

}
