package com.cps.city;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Campaign;
import com.cps.entity.bean.CampaignLogReport;
import com.cps.remote.CampaignBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;

@ManagedBean
@ViewScoped
public class CampaignLogReportController {
	
	private CampaignBeanRemote camBeanRemote;
	private List<CampaignLogReport> listCampaigns;
    
	private  Date startDate;
	private  Date endDate;
	private boolean logs=false;

    
	private String email="";
	
	
	
	
	public void reportCampaingLog(ActionEvent event)
	{
		
		try {
			camBeanRemote  = EJBServiceLocator.getInstace().getEjbService(CampaignBeanRemote.class);
			String fechaI = DateHelper.format(getStartDate(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getEndDate(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String em= email.equals("")?"%":email;
			listCampaigns=camBeanRemote.listCampaignsLog(fechaI + " 00:00:01", fechaF + " 23:59:59",em);
			
			logs=listCampaigns!=null&&listCampaigns.size()>0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public void generarInforme(){
        try{
		
		String fechaI = DateHelper.format(getStartDate(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		String fechaF = DateHelper.format(getEndDate(),
				DateHelper.FORMATYYYYMMDD_HYPHEN);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		
		String formato = (String) externalContext.getRequestParameterMap().get("formato");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String filename = "campaingReport_"+formatter.format(new java.util.Date());
				
		HashMap<String, Object> parameter = new HashMap<String, Object>();
		String em= email.equals("")?"%":email;
		parameter.put("fechaIni", fechaI + " 00:01:01");
		parameter.put("fechaFin", fechaF + " 23:59:59");
		parameter.put("email",em );
		String reportname="campaingReport";
		/*if( isadmin&&useremail.equals("") )
		{		
			reportname="reportBalanceUsersTransaction";
		}*/
		
		JasperReportHelper jrh = new JasperReportHelper(reportname, parameter);
       	JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS : JasperReportHelper.formatos.PDF;  
       
       	byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

		OutputStream os = response.getOutputStream();

		response.setContentType("application/" + formato); // fill in

		response.setContentLength(pdfStreamByteArray.length);

		response.setHeader("Content-disposition", "attachment; filename=\""
				+ filename + "."+formato+"\"");

		os.write(pdfStreamByteArray); // fill in bytes

		os.flush();
		os.close();
		facesContext.responseComplete();
        }catch (Exception e) {
        	//LOGGER.error("No se pudo realizar el informe : ",e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
      
	}
	
	public List<CampaignLogReport> getListCampaigns() {
		return listCampaigns;
	}

	public void setListCampaigns(List<CampaignLogReport> listCampaigns) {
		this.listCampaigns = listCampaigns;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public boolean isLogs() {
		return logs;
	}


	public void setLogs(boolean logs) {
		this.logs = logs;
	}

	
	

}
