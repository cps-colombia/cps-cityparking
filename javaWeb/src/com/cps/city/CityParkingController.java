package com.cps.city;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseEvent;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import com.cps.cards.controller.CardsController;
import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.CityParkingTransaction;
import com.cps.entity.bean.PayItem;
import com.cps.entity.bean.SysUser;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.session.CityParkingBean;
import com.cps.util.JsfUtil;
import com.cps.wservice.payment.verification.ArrayOfPagosV3;
import com.cps.wservice.payment.verification.PagosV3;

import org.primefaces.util.Constants;

@ManagedBean
@RequestScoped
public class CityParkingController {

	private final Logger LOGGER = Logger.getLogger(CityParkingController.class);

	private CityParkingBeanRemote cityParkingBeanRemote;
	private double value;
	private String documentId;
	private String documentType;
	private SysUser sysUser;
	private    boolean pendings=false;
	private double minvalueLimit=1;
	

	public CityParkingController()
	{
		
		
	}
	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	 public void  initPayment(){
		 
		 try {
			 
			 if(value<minvalueLimit)
			 {
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage( FacesMessage.SEVERITY_ERROR,"Valor mínimo", "El valor mínimo es de $"+minvalueLimit) );
				 return;
			 }
			 cityParkingBeanRemote = EJBServiceLocator.getInstace()
					.getEjbService(CityParkingBeanRemote.class);
		
		// RequestContext context = RequestContext.getCurrentInstance();
	        sysUser = (SysUser) JsfUtil.getSessionAtribute("userSession");
	        String []response= cityParkingBeanRemote.initPayment(sysUser, value);
	        String initId=response[0];
	        String payId=response[1];
	        if(!initId.contains("-1")&&!payId.contains("-1"))
	        {
	        	cityParkingBeanRemote.registryTransaction( new CityParkingTransaction(payId,
	        			new Date(), sysUser.getIdsysUser().toString(),
	        			sysUser.getEmail(),
	        			"Started", value, "CO") );
	        	//String outcome= URLEncoder.encode("www.zonapagos.com/t_cityparkingmovil/pago.asp?estado_pago=iniciar_pago&identificador="+payId,"UTF-8");
	        String outcome = "https://www.zonapagos.com/t_cityparkingmovil/pago.asp?estado_pago=iniciar_pago&identificador="+initId; // Do your thing?
	        //HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
	        //	context.addCallbackParam("payId", payId);
	        	//context.addCallbackParam("outcome", outcome);
	        	//context.addCallbackParam("outcome", outcome);
	        	  FacesContext.getCurrentInstance().getExternalContext().redirect(outcome);
	        }else
	        {
	        
	        	  
	             LOGGER.error(initId);
	        	//TODO: error
	        }
	    
			//	response.sendRedirect(outcome);
			} catch (IOException e) {
				e.printStackTrace();
			}
	        //facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, outcome);
		//return "https://www.zonapagos.com/t_cityparkingmovil/pago.asp?estado_pago=iniciar_pago&identificador=49788220002442957";
		 	catch (Exception e) {
				e.printStackTrace();
			}
		 
	 }
	 public void isPendingPayments( )
	 {
		 try {
				cityParkingBeanRemote = EJBServiceLocator.getInstace()
							.getEjbService(CityParkingBeanRemote.class);
		        sysUser = (SysUser) JsfUtil.getSessionAtribute("userSession");

		        ArrayList<CityParkingTransaction> transactions = cityParkingBeanRemote.getTransactionsByUserId(Long.parseLong(sysUser.getIdsysUser()));
			
			

			
			 
			ArrayList<PagosV3> pendingList= new ArrayList<PagosV3>();
			for(CityParkingTransaction trans: transactions)
			{
				if(trans.getStateTransaction().toLowerCase().contains("pending"))
				{
					PayItem item = cityParkingBeanRemote.checkPayment(trans.getIdTransaction());
					if(item== null) continue;
					ArrayOfPagosV3 array = item.getResPagosV3();
				
					System.out.println("Check items- result="+item.getVerificarPagoV3Result());
					if (item.getVerificarPagoV3Result() > 0&& item.getIntError()==0
							&& array != null) {
						PagosV3 payment = array.getPagosV3().get(0);
						int payStatement = payment.getIntEstadoPago();
					
						String status = trans.getStateTransaction();
						if (payStatement == 999|| payStatement == 888) {
							payment.getStrIdPago();
							status = "Pending to end";
							pendingList.add(payment);
						} 
						
						System.out.println("Check items- status="+status);
						/*if (!tr.getStateTransaction().equals(status))
						{
						boolean updated =cityParkingBean.updateTransactionStatus(String.valueOf(tr.getIdTransaction()),
								status);
						System.out.println("Check items- updated="+updated);
						}*/

						
					}
					
				}
			}
			
				
			 	if(pendingList.size()>1)
				{
			 		pendings=true;
			 		String  []refNumbers = new String[pendingList.size()]; 
			 		String  []refTrans = new String[pendingList.size()]; ;

			 		for(int i=0; i<pendingList.size();i++)
			 		{
			 			refNumbers[i]=pendingList.get(i).getStrIdPago();
			 		
			 			refTrans[i]=pendingList.get(i).getStrCodigoTransaccion();
			 		}
			 		
			 		String transStr= Arrays.toString(refTrans).replace("[", " " ).replace("]", " ")+"”.";
			 		transStr= transStr.contains("-1")?"":": "+transStr;
					FacesContext.getCurrentInstance().addMessage("msg",							
							new FacesMessage(FacesMessage.SEVERITY_INFO,"Máximo de pagos Pendientes. ",
									"“El límite máximo de transacciones pendientes ha sido alcanzado. En este momento sus Números de Referencia"+Arrays.toString(refNumbers).replace("[", " " ).replace("]", " ")+" presentan un proceso de pago cuya" +
											" transacción se encuentra PENDIENTE de recibir confirmación" +
											" por parte de su entidad financiera, Por favor puede  esperar unos minutos y volver " +
											"a consultar mas tarde para verificar si su pago fue confirmado " +
											"de forma exitosa. Si desea mayor información sobre el estado actual de " +
											"sus operaciones puede comunicarse a nuestras líneas de atención al " +
											"cliente los  teléfonos (1) 6210372 (1) 6210373 o enviar sus inquietudes al correo soporte@zonavirtual.com " +
											"y preguntar por el estado de las transacciones " + transStr));  
					
				}else if(pendingList.size()>0)
				{
					
					String transStr= pendingList.get(0).getStrCodigoTransaccion();
			 		transStr= transStr.contains("-1")?"":": "+transStr;
					FacesContext.getCurrentInstance().addMessage("msg",
							new FacesMessage(FacesMessage.SEVERITY_INFO,"Pago Pendiente. ",
									"“En este momento su Numero de Referencia "+pendingList.get(0).getStrIdPago()+" presenta un proceso de pago cuya transacción se encuentra PENDIENTE " +
											"de recibir confirmación por parte de su entidad financiera, sin embargo le está permitido realizar otra transacción o " +
											"puede  esperar unos minutos y volver a consultar mas tarde para verificar " +
											"si su pago fue confirmado de forma exitosa. Si desea mayor información sobre el " +
											"estado actual de su operación puede comunicarse a nuestras líneas de atención al" +
											" cliente a los  teléfonos (1) 6210372 (1) 6210373 o enviar sus inquietudes al correo" +
											" soporte@zonavirtual.com y preguntar por el estado de la transacción "+transStr+"”."));  
					}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
	 }

	public boolean isPendings() {
		return !pendings;
	}

	public void setPendings(boolean pendings) {
		this.pendings = pendings;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}


	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	 
	 
	
}
