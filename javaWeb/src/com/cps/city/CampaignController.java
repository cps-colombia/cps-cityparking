package com.cps.city;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Campaign;
import com.cps.entity.bean.CampaignState;
import com.cps.remote.CampaignBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JsfUtil;

@ManagedBean
@ViewScoped
public class CampaignController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CampaignBeanRemote campaignbr;
	private List<Campaign> listCampaigns;
	private Campaign campaign = new Campaign();
	private Date currDate = new Date();

	public CampaignController() {
		try {
			campaignbr = EJBServiceLocator.getInstace().getEjbService(
					CampaignBeanRemote.class);
			setListCampaigns(campaignbr.listAllCampaigns());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addCampaign(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag=false;

		try {
			campaign.setCampaignState(new CampaignState(1, ""));
			if (campaign.getRangeAmountEnd().compareTo(
					campaign.getRangeAmountStart()) >0) {

				if (!campaignbr.addCampaign(campaign)) {
					JsfUtil.addMessage("campaign.error.insert",
							FacesMessage.SEVERITY_ERROR);
				}else
				{
					campaign = new Campaign();
					flag=true;
				}
			}else
			{
				JsfUtil.addMessage("campaign.error.range",
						FacesMessage.SEVERITY_ERROR);
			}
			setListCampaigns(campaignbr.listAllCampaigns());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		context.addCallbackParam( "flag", flag );
	}

	public void deleteCampaign(ActionEvent event) {
		try {
			campaignbr.deleteCampaign(campaign);
			setListCampaigns(campaignbr.listAllCampaigns());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<Campaign> getListCampaigns() {
		return listCampaigns;
	}

	public void setListCampaigns(List<Campaign> listCampaigns) {
		this.listCampaigns = listCampaigns;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public Date getCurrDate() {
		return currDate;
	}

	public void setCurrDate(Date currDate) {
		this.currDate = currDate;
	}

}
