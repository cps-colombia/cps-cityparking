package com.cps.city;

import java.io.Serializable;
import java.util.Iterator;

import javassist.convert.Transformer;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.log4j.Logger;

import com.cps.util.JsfUtil;

@ManagedBean
@ViewScoped
public class SupervisorController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(SupervisorController.class);
	
	private static String wsdlToken = "43697479252652652d5669727475616c";
	private static String serverURI = "http://190.0.226.97:8888/wservicecity/CPS/puente.php";

	private String codeUser;
	private String getSaldo;
	private String getLogin;
	private String getCu;
	private String getName;
	private String getLast;
	private String getPhone;
	private String getEmail;
	private String codeResponse;
	private boolean userExist = false;
	

	public String soapConsultarSaldo() {
	   try {
	        // Create SOAP Connection
	        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
	        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

	        // Send SOAP Message to SOAP Server
	        SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(codeUser), serverURI);

	        // get Data Response
	        SOAPBody sb = soapResponse.getSOAPBody();
	        codeResponse = sb.getElementsByTagName("codigoRespuesta").item(0).getTextContent();
	        
	        switch(codeResponse){
            	case "11":
            		userExist = false;
            		JsfUtil.addMessage("SupervisorUserNoExist", FacesMessage.SEVERITY_ERROR);
            		break;
            	case "12":
            	case "13":
            		userExist = true;
	        		getSaldo = sb.getElementsByTagName("saldo").item(0).getTextContent();
	        		getLogin = sb.getElementsByTagName("login").item(0).getTextContent();
	        		getCu = sb.getElementsByTagName("cu").item(0).getTextContent();
	        		getName = sb.getElementsByTagName("nombre").item(0).getTextContent();
	        		getLast = sb.getElementsByTagName("apellido").item(0).getTextContent();
            		getPhone = sb.getElementsByTagName("telefono").item(0).getTextContent();	
	        		getEmail = sb.getElementsByTagName("email").item(0).getTextContent();
	        		break;
            	case "51":
            	case "52":
            	case "53":
            	case "54":
            		userExist = false;
            		JsfUtil.addMessage("supervisorGeneralError", FacesMessage.SEVERITY_ERROR);
            		break;
	        }
        
        	soapConnection.close();
		} catch (Exception e) {
			userExist = false;
			LOGGER.error( "Error get SOAP Supervisor:", e );
			e.printStackTrace();
			JsfUtil.addMessage("supervisorGetError", FacesMessage.SEVERITY_ERROR);
		}
   		return null;
    }
   
   
    private static SOAPMessage createSOAPRequest(String codeUser) throws Exception {
	        MessageFactory messageFactory = MessageFactory.newInstance();
	        SOAPMessage soapMessage = messageFactory.createMessage();
	        SOAPPart soapPart = soapMessage.getSOAPPart();

	        // SOAP Envelope
	        SOAPEnvelope envelope = soapPart.getEnvelope();
	        envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
	        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");

	        // SOAP Body	       	    	
	        SOAPHeader sh = soapMessage.getSOAPHeader();
	        SOAPBody sb = soapMessage.getSOAPBody();
	        sh.detachNode();
	        QName bodyName = new QName(serverURI, "consultarSaldo", "urn");
	        SOAPBodyElement bodyElement = sb.addBodyElement(bodyName);
	        QName qtoken = new QName("token");
	        SOAPElement token = bodyElement.addChildElement(qtoken);
	        QName qcode = new QName("codigoCPS");
	        SOAPElement code = bodyElement.addChildElement(qcode);

	        token.addTextNode(wsdlToken);
	        code.addTextNode(codeUser);
	         
	        return soapMessage;
    }

	public String getCodeUser(){
		return codeUser;
	}
	public void setCodeUser(String codeUser){
		this.codeUser = codeUser;
	}
	
	public String getGetPhone(){
		return getPhone;
	}
	public void setGetPhone(String getPhone){
		this.getPhone = getPhone;
	}
	public String getGetSaldo(){
		return getSaldo;
	}
	public void setGetSaldo(String getSaldo){
		this.getSaldo = getSaldo;
	}	
	public String getGetCu(){
		return getCu;
	}
	public void setGetCu(String getCu){
		this.getCu = getCu;
	}	
	public String getGetLogin(){
		return getLogin;
	}
	public void setGetLogin(String getLogin){
		this.getLogin = getLogin;
	}		
	public String getGetEmail(){
		return getEmail;
	}
	public void setGetEmail(String getEmail){
		this.getEmail = getEmail;
	}	
	public String getGetName(){
		return getName;
	}
	public void setGetName(String getName){
		this.getName = getName;
	}	
	public String getGetLast(){
		return getLast;
	}
	public void setGetLast(String getLast){
		this.getLast = getLast;
	}	
	public boolean getUserExist(){
		return userExist;
	}
	public void setUserExist(boolean userExist){
		this.userExist = userExist;
	}	
}
