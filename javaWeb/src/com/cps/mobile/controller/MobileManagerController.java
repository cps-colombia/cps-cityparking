/**
 * 
 */
package com.cps.mobile.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.MsisdnUser;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.JsfUtil;

/**
 * @author Jorge
 *
 */
@ManagedBean
@ViewScoped
public class MobileManagerController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SysUserBeanRemote sysUserBeanRemote;
	private List<MsisdnUser> listMsisdn;
	private MsisdnUser msisdnUser; 
	private String msisdn;
	
	
	public MobileManagerController(){
		try {
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(SysUserBeanRemote.class);
			listMsisdn = sysUserBeanRemote.getMobileList(JsfUtil.getUserSession());
		} catch (Exception e) {
     		e.printStackTrace();
		}
	}

	public void registerMobile(ActionEvent event){
		RequestContext context = RequestContext.getCurrentInstance();
		boolean flag = false;
		try{
		  sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(SysUserBeanRemote.class);
		  sysUserBeanRemote.addMobile(getMsisdn(), JsfUtil.getUserSession());
		  listMsisdn = sysUserBeanRemote.getMobileList(JsfUtil.getUserSession());
		  JsfUtil.addMessage("myMobiles.registered", FacesMessage.SEVERITY_INFO);
		  flag = true;
		}catch (Exception e) {
			  JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
			  flag = false;
		}
		context.addCallbackParam("flag", flag);
	}
	
	public void removeMobileListener(ActionEvent event) {
		  try {
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(SysUserBeanRemote.class);
	        sysUserBeanRemote.removeMobile(msisdnUser.getMsisdn(), JsfUtil.getUserSession());
	        listMsisdn = sysUserBeanRemote.getMobileList(JsfUtil.getUserSession());
	        JsfUtil.addMessage("myMobiles.deleted", FacesMessage.SEVERITY_INFO);
	        
		  } catch (Exception e) {
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
    	}
	}

		

	public List<MsisdnUser> getListMsisdn() {
		return listMsisdn;
	}

	public void setListMsisdn(List<MsisdnUser> listMsisdn) {
		this.listMsisdn = listMsisdn;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public MsisdnUser getMsisdnUser() {
		return msisdnUser;
	}

	public void setMsisdnUser(MsisdnUser msisdnUser) {
		this.msisdnUser = msisdnUser;
	}

}
