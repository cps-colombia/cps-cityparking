package com.cps.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class PayPalHelper {
	
	private static Logger LOGGER = Logger.getLogger(PayPalHelper.class);
	private static Properties paypalProperties = new Properties();
	
	static{
        ResourceBundle bundle = ResourceBundle.getBundle("cps");
		try {
			paypalProperties.load(new FileInputStream(new File(
					bundle.getString("cps.payPalPath.properties"))));
		} catch (FileNotFoundException e) {
			LOGGER.error("No se pudieron cargar las propiedades para paypal",e);
		} catch (IOException e) {
			LOGGER.error("No se pudieron cargar las propiedades para paypal",e);		}
	}
	
	public static String getValue(String id){
		return paypalProperties.getProperty(id);
	}

}
