/**
 * 
 */
package com.cps.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * @author Jorge
 * 
 */
public class JasperReportHelper {

	public enum formatos {
		PDF, XLS, HTML
	};

	private String jasperReportFile;
	private HashMap<String, Object> parameters;
	private ByteArrayOutputStream stream;

	public JasperReportHelper(String jasperReportFile,
			HashMap<String, Object> parameters) {
		this.jasperReportFile = jasperReportFile;
		this.parameters = parameters;
	}

	public ByteArrayOutputStream obtenerStream(formatos formato)
			throws Exception {
		try {
			stream = new ByteArrayOutputStream();

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();

			JasperReport reporte = (JasperReport) JRLoader
					.loadObject(externalContext.getRealPath(File.separator
							+ "WEB-INF" + File.separator + "reports"
							+ File.separator + jasperReportFile + ".jasper"));

			parameters.put(
					"logo",
					externalContext.getRealPath(File.separator + "images"
							+ File.separator + "cps" + File.separator
							+ "cps_city.png"));
			parameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, facesContext
					.getApplication().getResourceBundle(facesContext, "msgs"));

				
			if (formatos.XLS == formato) 
				parameters.put("IS_IGNORE_PAGINATION", true);
			JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,
					parameters, Conexion.getConexion());

			if (formatos.XLS == formato) {
			
				JRXlsExporter exporterXLS = new JRXlsExporter();
				exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
						stream);
				//exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
				exporterXLS.exportReport();
			} else if (formatos.PDF == formato) {
				JasperExportManager
						.exportReportToPdfStream(jasperPrint, stream);
			} else if (formatos.HTML == formato) {
				JRHtmlExporter exporterHTML = new JRHtmlExporter();
				
				exporterHTML.setParameter(JRExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporterHTML.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM,
						stream);
				exporterHTML.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.TRUE);
				//exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_DIR_NAME, "/images/");
				
				exporterHTML.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);

				exporterHTML.setParameter(JRHtmlExporterParameter.IMAGES_URI,"/image?image=");
				exporterHTML.exportReport();
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			Conexion.close();
		}
		return stream;
	}

}
