package com.cps.util;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class Conexion {

	private static volatile Connection con;
	private static Logger LOGGER = Logger.getLogger(Conexion.class);
	private static Properties bdProperties = new Properties();
	
	static{
	try {
		ResourceBundle bundleCps = ResourceBundle.getBundle("cps");
		bdProperties.load(new FileInputStream(new File(
				bundleCps.getString("cps.bd.properties"))));
	} catch (Exception e) {
		LOGGER.error("No se pudo cargar la configuracion jndi para BD", e);
	}
	}
	
	
	public static Connection getConexion() {
		try {

			if (con != null && !con.isClosed())
				return con;

			Class.forName(bdProperties.getProperty("jdbcDriver")).newInstance();
			con = DriverManager.getConnection(bdProperties.getProperty("jdbcUrl"),
					bdProperties.getProperty("user"), bdProperties.getProperty("pass"));
		} catch (Exception e) {
			LOGGER.error("No se pudo establecer conexion con la bd", e);
		}
		return con;
	}

	public static void close() {
		if (con != null)
			try {
				con.close();
			} catch (SQLException e) {
				LOGGER.error("No se pudo cerrar la conexion con la bd", e);
			}
	}

}
