package com.cps.util;

import java.io.IOException;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

public class Util {


	public final static String chars[] = {
			"A", "B", "C" , "D", "E", "F", "G", "H", "I", "J",
			"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
			"V", "W", "X", "Y", "Z", "a", "b", "c" , "d", "e", "f", "g", "h", "i", "j",
			"k", "l", "m", "n", "o", "p", "q", "t", "s", "t", "u",
			"v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9"
			};

	public static String generatePassword(){
		int k = 0;
		String password = "";
		while( k < 6 ){
			password += String.valueOf(chars[(int)(Math.random()*chars.length)]);
			k++;
		}
		return password;
	}

	/**
	 * Rendered primeface template and convert to string
	 *
	 * @return
	 */
	public static String getTemplateString(String template) {

	    // initial values
	    FacesContext faces = FacesContext.getCurrentInstance();
	    ExternalContext context = faces.getExternalContext();
	    HttpServletResponse response = (HttpServletResponse)
	            context.getResponse();
	    ResponseCatcher catcher = new ResponseCatcher(response);
	    ViewHandler views = faces.getApplication().getViewHandler();

	    // render the message
	    try {
	        context.setResponse(catcher);
	        context.getRequestMap().put("emailClient", true);
	        views.renderView(faces, views.createView(faces, template));
	        context.getRequestMap().remove("emailClient");
	        context.setResponse(response);
	    } catch (IOException ioe) {
	        String msg = "Failed to render email internally";
	        faces.addMessage(null, new FacesMessage(
	                FacesMessage.SEVERITY_ERROR, msg, msg));
	        return null;
	    }
        return catcher.toString();
    }

}
