package com.cps.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

	public static final String FORMATDDMMYYYY_SLASH = "dd/MM/yyyy";
	public static final String FORMATYYYYMMDD_SLASH = "yyyy/MM/dd";
	public static final String FORMATDDMMYYYY_HYPHEN = "dd-MM-yyyy";
	public static final String FORMATYYYYMMDD_HYPHEN = "yyyy-MM-dd";
	
	private static SimpleDateFormat df;
	
	public static String format(Date date,String format){
		df = new SimpleDateFormat(format);
		return df.format(date);
	}
	
	

}


