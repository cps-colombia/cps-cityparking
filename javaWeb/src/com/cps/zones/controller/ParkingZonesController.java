package com.cps.zones.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.City;
import com.cps.entity.bean.Country;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.Zone;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.remote.CountryBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.JsfUtil;
import com.cps.wservice.Cvalorubicaws;
import com.ctc.wstx.io.EBCDICCodec;

@ManagedBean
@SessionScoped
public class ParkingZonesController {

	public static class InfoZone implements Serializable {

		public InfoZone(Zone zone, Integer quota) {
			super();
			this.zone = zone;
			this.quota = quota;
		}

		/**
			 * 
			 */
		private static final long serialVersionUID = 1L;

		private Zone zone;
		private Integer quota;

		public Zone getZone() {
			return zone;
		}

		public void setZone(Zone zone) {
			this.zone = zone;
		}

		public Integer getQuota() {
			return quota;
		}

		public void setQuota(Integer quota) {
			this.quota = quota;
		}

	}

	private ZoneBeanRemote zoneBeanRemote;
	private CountryBeanRemote countryBeanRemote;
	private List<Zone> listZone;
	private MapModel simpleModel;
	private Country country;
	private City city;
	private List<City> cities;
	private String lat = "0.0";
	private String lon = "0.0";
	private Marker marker;
	private CityParkingBeanRemote cityPBeanremote;
	private InfoZone infoZone;
	private SysUser su;

	public ParkingZonesController() {
		try {

			su = JsfUtil.getUserSession();

			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					CountryBeanRemote.class);
			cityPBeanremote = EJBServiceLocator.getInstace().getEjbService(
					CityParkingBeanRemote.class);

			country = countryBeanRemote.getCountry(su.getCountryIdcountry());
			setLat(country.getLatitude());
			setLon(country.getLongitude());
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					ZoneBeanRemote.class);
			simpleModel = new DefaultMapModel();
			listZone = zoneBeanRemote.getZones(su.getCountryIdcountry());

			cities = countryBeanRemote.getCities(su.getCountryIdcountry());
			simpleModel = new DefaultMapModel();

			// Map<String, Cvalorubicaws> infolist = cityPBeanremote
			// .getParkingsInfo(listZone);
			// for (Zone zone : listZone) {
			// Integer quota = null;
			// Cvalorubicaws value = infolist.get(zone.getIdzone());
			// if (value != null)
			// quota = value.getCupos();
			// simpleModel.addOverlay(new Marker(new LatLng(Double
			// .valueOf(zone.getLatitude()), Double.valueOf(zone
			// .getLongitude())), zone.getName(), new InfoZone(zone,
			// quota), "../images/icons/flag.png"));
			// }

			updateZones("-1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void changeZone(ValueChangeEvent event) {
		String citytrySelect = String.valueOf(event.getNewValue());
		updateZones(citytrySelect);
	}

	private void updateZones(String citytrySelect) {
		try {

			// city.

			if (citytrySelect != null) {

				if (citytrySelect.equals("-1")) {
					listZone = zoneBeanRemote
							.getZones(su.getCountryIdcountry());
				} else {

					countryBeanRemote = EJBServiceLocator.getInstace()
							.getEjbService(CountryBeanRemote.class);
					city = countryBeanRemote.getCity(citytrySelect);

					// zoneBeanRemote = EJBServiceLocator.getInstace()
					// .getEjbService(ZoneBeanRemote.class);
					listZone = zoneBeanRemote.getZonesByCity(su,
							city.getIdcountry(), city.getIdcity());
				}
				// simpleModel = new DefaultMapModel();
				Map<String, Cvalorubicaws> infolist = cityPBeanremote
						.getParkingsInfo(listZone);

				simpleModel.getMarkers().clear();
				String image="";
				for (Zone zone : listZone) {
					Integer quota = null;
					 image = "../images/icons/deactivateflag.png";
					if (zone.getActive() == 1) {
						Cvalorubicaws value = infolist.get(zone.getIdzone());
						if (value != null) {
							quota = value.getCupos();

							if (quota != null)
								image = "../images/icons/flag.png";
						} else {
							image = "../images/icons/offlineflag.png";
						}
					}
					simpleModel.addOverlay(new Marker(new LatLng(Double
							.valueOf(zone.getLatitude()), Double.valueOf(zone
							.getLongitude())), zone.getName(), new InfoZone(
							zone, quota), image));
				}
			} else {
				country = null;
				listZone = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	public void onMarkerSelect(OverlaySelectEvent event) {
		marker = (Marker) event.getOverlay();
		infoZone = (InfoZone) marker.getData();

	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public InfoZone getInfoZone() {
		return infoZone;
	}

	public void setInfoZone(InfoZone infoZone) {
		this.infoZone = infoZone;
	}

}
