package com.cps.zones.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.Country;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.Zone;
import com.cps.remote.CountryBeanRemote;
import com.cps.remote.ZoneBeanRemote;
import com.cps.util.JsfUtil;

@ManagedBean
public class UserZonesController {
	
	
	private ZoneBeanRemote zoneBeanRemote;
	private CountryBeanRemote countryBeanRemote;
	private List<Zone> listZone;
	private MapModel simpleModel;
	private Country country;
	
	public UserZonesController(){
		try{
			SysUser sysUser = JsfUtil.getUserSession();
			zoneBeanRemote = EJBServiceLocator.getInstace().getEjbService(ZoneBeanRemote.class);
			countryBeanRemote = EJBServiceLocator.getInstace().getEjbService(CountryBeanRemote.class);
			country = countryBeanRemote.getCountry(sysUser.getCountryIdcountry());
			listZone = zoneBeanRemote.getZones(sysUser.getCountryIdcountry());
			simpleModel = new DefaultMapModel();
			for (Zone zone  : listZone) {
				simpleModel.addOverlay(new Marker(new LatLng(Double.valueOf(zone.getLatitude()), Double.valueOf(zone.getLongitude())),zone.getName()));	
			}
			
			}catch(Exception e){
				e.printStackTrace();
			}
		
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	
}
