/**
 * 
 */
package com.cps.balance.controller;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.cps.bean.PayPalBean;

import com.cps.configuracion.paypal.EWPServices;
import com.cps.util.JsfUtil;
import com.cps.util.PayPalHelper;

/**
 * @author Jorge
 *
 */
@ManagedBean
@SessionScoped
public class BalancePPController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(BalancePPController.class);
	private PayPalBean payPalBean = new PayPalBean();
	private String button = null; 
	
	private final String DEFAULT_ENV = PayPalHelper.getValue("DEFAULT_ENV");
	private final String DEFAULT_EMAIL_ADDRESS = PayPalHelper.getValue("DEFAULT_EMAIL_ADDRESS");//"sdk-seller@sdk.com";
	private final String DEFAULT_EWP_CERT_PATH = PayPalHelper.getValue("DEFAULT_EWP_CERT_PATH"); //"WEB-INF/cert/sdk-ewp.p12";
	private final String DEFAULT_PRIVATE_KEY_PWD = PayPalHelper.getValue("DEFAULT_PRIVATE_KEY_PWD");
	private final String DEFAULT_CERT_ID = PayPalHelper.getValue("DEFAULT_CERT_ID"); //"KJAERUGBLVF6Y";
	private final String PAYPAL_CERT_PATH = PayPalHelper.getValue("PAYPAL_CERT_PATH"); //"WEB-INF/cert/sandbox_cert_pem.txt";
	private final String RETURN_URL = PayPalHelper.getValue("RETURN_URL"); 
	private final String CANCEL_URL = PayPalHelper.getValue("CANCEL_URL");
	private final String NOTIFY_URL = PayPalHelper.getValue("NOTIFY_URL");
	private final String BUTTON_IMAGE = PayPalHelper.getValue("BUTTON_IMAGE");
	

	public String continuePP(){
		String buttonStr = null;
		EWPServices ewp = new EWPServices();
		StringBuffer buffer = new StringBuffer("cmd=_xclick\n");
		buffer.append("business=" + DEFAULT_EMAIL_ADDRESS + "\n");
		buffer.append("cert_id=" + DEFAULT_CERT_ID + "\n");
		buffer.append("charset=UTF-8\n");
		buffer.append("item_name=" + payPalBean.getDescripcion() + "\n");
		buffer.append("item_number=" + payPalBean.getItem() + "\n");
		buffer.append("amount=" + payPalBean.getAmount() + "\n");
		buffer.append("currency_code=" + "CAD" + "\n");
		buffer.append("return=" + RETURN_URL + "\n");
		buffer.append("cancel_return=" + CANCEL_URL + "\n");
		buffer.append("notify_url=" + NOTIFY_URL + "\n");
		buffer.append("custom="+JsfUtil.getUserSession().getIdsysUser()+ "\n");
		buttonStr = buffer.toString();

		try {
			setButton(ewp.encryptButton(buttonStr.getBytes("UTF-8"), FacesContext.getCurrentInstance().getExternalContext().getRealPath(DEFAULT_EWP_CERT_PATH), DEFAULT_PRIVATE_KEY_PWD, FacesContext.getCurrentInstance().getExternalContext().getRealPath(PAYPAL_CERT_PATH), DEFAULT_ENV, BUTTON_IMAGE));
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Error encriptando info pp", e);
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error("Error creando boton pp", e);
			e.printStackTrace();
		}
			
		return "reloadPayPalConfirm";
	}


	public void setPayPalBean(PayPalBean payPalBean) {
		this.payPalBean = payPalBean;
	}


	public PayPalBean getPayPalBean() {
		return payPalBean;
	}


	public void setButton(String button) {
		this.button = button;
	}


	public String getButton() {
		return button;
	}



	
	
	

	
	
	
}
