/**
 * 
 */
package com.cps.balance.controller;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import org.apache.log4j.Logger;

/**
 * @author Jorge
 *
 */
@ManagedBean
public class PayPalReturnController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(PayPalReturnController.class);
	private String paymentStatus;
	
	public PayPalReturnController(){
			
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}
		
	
}
