package com.cps.balance.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.jboss.logging.Logger;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.CityParkingTransactionUser;
import com.cps.entity.bean.SysUser;
import com.cps.remote.CityParkingBeanRemote;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.CodeResponse;
import com.cps.util.JsfUtil;
import com.mysema.query.alias.Alias;
import com.mysema.query.collections.CollQueryFactory;

@ManagedBean(name = "balanceCurrentController")
@ViewScoped
public class BalanceCurrentController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(BalanceCurrentController.class);
	private SysUser sysUser;
	private SysUserBeanRemote sysUserBeanRemote;
	private List<SysUser> originUsers = new ArrayList<SysUser>();
	private List<SysUser> destUsers = new ArrayList<SysUser>();
	private List<SysUser> destUsersTemp = new ArrayList<SysUser>();
	private SysUser suAls = Alias.alias(SysUser.class, "su");
	private String userfilter;
	private BigDecimal amount;
	private DualListModel<SysUser> userlist;

	public BalanceCurrentController() {
		try {

			userlist = new DualListModel<SysUser>(originUsers, destUsers);
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);
			sysUser = sysUserBeanRemote.getUser(JsfUtil.getUserSession()
					.getIdsysUser().toString());

			JsfUtil.setSessionAtribute("userSession", sysUser);
		} catch (Exception e) {
			LOGGER.error("No se pudo obtener informacion de balance", e);
		}
	}

	public void onTransfer(TransferEvent event) {
		if (event.isAdd()) {
			destUsers.clear();
			for (Object item : event.getItems()) {
				SysUser su = (SysUser) item;

				destUsersTemp.add(su);

			}
			destUsers.addAll(destUsersTemp);
		} else if (event.isRemove()) {
			destUsers.clear();

			for (Object item : event.getItems()) {
				SysUser su = (SysUser) item;
				destUsersTemp.remove(su);

			}
			destUsers.addAll(destUsersTemp);

		}

	}

	public void filterUsers(AjaxBehaviorEvent event) {
		try {

			if (userfilter != null && userfilter.length() > 2) {

				sysUserBeanRemote = EJBServiceLocator.getInstace()
						.getEjbService(SysUserBeanRemote.class);

				userlist.getSource().clear();
				String[] exclusions = new String[destUsersTemp.size()];
				for (int i = 0; i < destUsersTemp.size(); i++) {
					SysUser su = destUsersTemp.get(i);
					exclusions[i] = su.getLogin();
				}

				userlist.getSource().addAll(
						sysUserBeanRemote.getUsersAndExclude(userfilter,
								exclusions));
				originUsers = userlist.getSource();
				// originUsers.addAll(sysUserBeanRemote.getUsers(userfilter));
			} else if (userfilter != null && userfilter.length() == 0) {
				userlist.getSource().clear();
			}
			userlist.getTarget().clear();
			userlist.getTarget().addAll(destUsersTemp);

		} catch (Exception e) {
			LOGGER.error("No se pudo obtener informacion de balance", e);
		}

	}

	public void executeTransaction(ActionEvent event) {

		if (userlist.getTarget().size() == 0) {

			JsfUtil.addMessage("transfer.pick.noUser",
					FacesMessage.SEVERITY_ERROR);

			return;
		}

		BigDecimal totalamount = BigDecimal.valueOf(0.0);
		SysUser cuser = null;
		try {
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);

			List<SysUser> target = destUsersTemp;

			BigDecimal fullamount = amount.multiply(BigDecimal.valueOf(target
					.size()));

			BigDecimal result = sysUser.getBalance().subtract(fullamount);

			if (result.doubleValue() < 0
					&& !sysUser.getLogin().equals("cityparking")) {
				JsfUtil.addMessage("transfer.balance.notenough",
						FacesMessage.SEVERITY_ERROR);
				return;
			}

			int errorTransfer = 0;
			StringBuilder strbBuilder = new StringBuilder();
			for (SysUser user : target) {

				if (sysUserBeanRemote.reloadBalance(user, amount) == CodeResponse.RELOAD_RESIDUE_OK)

				{

					try {

						sysUserBeanRemote.registryUserTransaction(
								sysUser.getIdsysUser(), user.getIdsysUser(),
								amount);
						totalamount = totalamount.add(amount);
					} catch (Exception e) {
						// TODO: handle exception

						strbBuilder.append(user.getLogin());
						strbBuilder.append(", ");
						errorTransfer++;
					}

				}

			}

			try {
				sysUser.setBalance(sysUser.getBalance().subtract(totalamount));
				sysUserBeanRemote.updateUser(sysUser);
				sysUser = sysUserBeanRemote.getUser(JsfUtil.getUserSession()
						.getIdsysUser().toString());

				JsfUtil.setSessionAtribute("userSession", sysUser);

				if (destUsersTemp.size() > 1)
					JsfUtil.addMessage("transfer.multiple.ok",
							FacesMessage.SEVERITY_INFO);

				else if (destUsersTemp.size() > 0)
					JsfUtil.addMessage("transfer.one.ok",
							FacesMessage.SEVERITY_INFO);

				if (errorTransfer > 1) {

					String msg = JsfUtil.getStringMessageFor(
							"transfer.multiple.error").replace(
							"{users}",
							strbBuilder.delete(strbBuilder.length() - 2,
									strbBuilder.length()));
					JsfUtil.addMessage(msg, null, FacesMessage.SEVERITY_ERROR);

				} else if (errorTransfer > 0) {
					String msg = JsfUtil.getStringMessageFor(
							"transfer.one.error").replace(
							"{user}",
							strbBuilder.delete(strbBuilder.length() - 2,
									strbBuilder.length()));
					JsfUtil.addMessage(msg, null, FacesMessage.SEVERITY_ERROR);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println(target);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			// JsfUtil.addMessage("transfer.pick.noUser",
			// FacesMessage.SEVERITY_ERROR);
			return;
		} finally {

			userfilter = "";
			userlist.getSource().clear();
			userlist.getTarget().clear();
			destUsersTemp.clear();
			amount = null;
		}

	}

	public List<SysUser> getOriginUsers() {
		return originUsers;
	}

	public void setOriginUsers(List<SysUser> originUsers) {
		this.originUsers = originUsers;
	}

	public List<SysUser> getDestUsers() {
		return destUsers;
	}

	public void setDestUsers(List<SysUser> destUsers) {
		this.destUsers = destUsers;
	}

	public String getUserfilter() {
		return userfilter;
	}

	public void setUserfilter(String userfilter) {
		this.userfilter = userfilter;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	public SysUser getSysUser() {
		return sysUser;
	}

	public DualListModel<SysUser> getUserlist() {
		return userlist;
	}

	public void setUserlist(DualListModel<SysUser> userlist) {
		this.userlist = userlist;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
