package com.cps.balance.controller;

import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.data.FilterEvent;

import com.cps.configuracion.locator.EJBServiceLocator;
import com.cps.entity.bean.SysUser;
import com.cps.entity.bean.UserTransactionReport;
import com.cps.remote.SysUserBeanRemote;
import com.cps.util.DateHelper;
import com.cps.util.JasperReportHelper;
import com.cps.util.JsfUtil;
import com.mysema.query.alias.Alias;
//needed, if you use the $-invocations    
import com.mysema.query.collections.CollQueryFactory;

@ManagedBean
@ViewScoped
public class BalanceTransactionController {

	private Logger LOGGER = Logger
			.getLogger(BalanceTransactionController.class);

	private List<UserTransactionReport> userTransList = new ArrayList<UserTransactionReport>();
	private SysUserBeanRemote sysUserBeanRemote;
	private SysUser sysUser;
	private Date fechaIni;
	private Date fechaFin;
	private boolean history = false;
	private String useremail;
	private boolean isadmin = false;
	private double totalAmount;
	private int totalItems;
	private List<UserTransactionReport> userTransListFiltered;
	private UserTransactionReport usre = Alias.alias(
			UserTransactionReport.class, "ur");

	public BalanceTransactionController() {
		sysUser = (SysUser) JsfUtil.getSessionAtribute("userSession");
		isadmin = sysUser.getIdsysUserType() == 5;
		if (!isadmin) {
			useremail = sysUser.getEmail();
		}

	}

	public void onCompleteFilter(FilterEvent e) {

		DataTable dt = (DataTable) e.getSource();
		dt.getFilteredValue();
		Map<String, Object> filters = e.getFilters();

		String filterUserDest = (filters.containsKey("idsysUserDest")) ? filters
				.get("idsysUserDest").toString() : "";
		String filterUserOrigin = (filters.containsKey("idsysUserOrigin")) ? filters
				.get("idsysUserOrigin").toString() : "";

		List<UserTransactionReport> result = CollQueryFactory
				.from(Alias.$(usre), userTransList)
				.where(Alias
						.$(usre.getIdsysUserOrigin())
						.contains(filterUserOrigin)
						.and(Alias.$(usre.getIdsysUserDest()).contains(
								filterUserDest))).list(Alias.$(usre));
		result.toString();

		totalAmount = 0;
		totalItems = 0;
		for (UserTransactionReport usert : result) {
			totalAmount += usert.getAmount().doubleValue();
			totalItems++;

		}

		RequestContext.getCurrentInstance().addCallbackParam(
				"totalcompletedamount",
				JsfUtil.getStringMessageFor("myHistory.totalamount") + " : "
						+ NumberFormat.getCurrencyInstance().format(totalAmount));
		RequestContext.getCurrentInstance().addCallbackParam(
				"totalcompleteditems",
				JsfUtil.getStringMessageFor("myHistory.totalitems") + " : "
						+ totalItems);

		// RequestContext.getCurrentInstance().update(":contact-form:tbl");

	}

	public void reportTransference(ActionEvent event) {

		try {

			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			sysUserBeanRemote = EJBServiceLocator.getInstace().getEjbService(
					SysUserBeanRemote.class);
			if (useremail != null && !useremail.equals("")) {
				sysUser = sysUserBeanRemote.getUserByEmail(useremail);
				setUserTransList(sysUserBeanRemote.getTransactionForUser(
						sysUser.getIdsysUser().toString(),
						fechaI + " 00:01:01", fechaF + " 23:59:59"));

			} else if (isadmin && useremail.equals("")) {

				totalAmount = 0;
				setUserTransList(sysUserBeanRemote.getTransactionForAllUsers(
						fechaI + " 00:01:01", fechaF + " 23:59:59"));

			}
			if (userTransList != null) {
				totalAmount = 0;
				for (UserTransactionReport utr : userTransList) {
					totalAmount += utr.getAmount().doubleValue();
				}
			}

			setHistory(userTransList.size() > 0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generarInforme() {
		try {

			String fechaI = DateHelper.format(getFechaIni(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);
			String fechaF = DateHelper.format(getFechaFin(),
					DateHelper.FORMATYYYYMMDD_HYPHEN);

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			HttpServletResponse response = (HttpServletResponse) externalContext
					.getResponse();

			String formato = (String) externalContext.getRequestParameterMap()
					.get("formato");

			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");

			String filename = "reportBalanceTransaction_"
					+ formatter.format(new java.util.Date());

			HashMap<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("fechaIni", fechaI + " 00:01:01");
			parameter.put("fechaFin", fechaF + " 23:59:59");
			parameter.put("userId", sysUser.getIdsysUser());
			String reportname = "reportBalanceTransaction";
			if (isadmin && useremail.equals("")) {
				reportname = "reportBalanceUsersTransaction";
			}

			JasperReportHelper jrh = new JasperReportHelper(reportname,
					parameter);
			JasperReportHelper.formatos format = formato.equals("xls") ? JasperReportHelper.formatos.XLS
					: JasperReportHelper.formatos.PDF;

			byte[] pdfStreamByteArray = jrh.obtenerStream(format).toByteArray();

			OutputStream os = response.getOutputStream();

			response.setContentType("application/" + formato); // fill in

			response.setContentLength(pdfStreamByteArray.length);

			response.setHeader("Content-disposition", "attachment; filename=\""
					+ filename + "." + formato + "\"");

			os.write(pdfStreamByteArray); // fill in bytes

			os.flush();
			os.close();
			facesContext.responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("No se pudo realizar el informe : ", e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_ERROR);
		}
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<UserTransactionReport> getUserTransList() {
		return userTransList;
	}

	public void setUserTransList(List<UserTransactionReport> userTransList) {
		this.userTransList = userTransList;
	}

	public boolean isHistory() {
		return history;
	}

	public void setHistory(boolean history) {
		this.history = history;
	}

	public String getUseremail() {
		return useremail;
	}

	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<UserTransactionReport> getUserTransListFiltered() {
		return userTransListFiltered;
	}

	public void setUserTransListFiltered(
			List<UserTransactionReport> userTransListFiltered) {
		this.userTransListFiltered = userTransListFiltered;
	}

}
