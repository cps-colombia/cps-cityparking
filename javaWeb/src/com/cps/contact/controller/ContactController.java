package com.cps.contact.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import com.cps.util.JsfUtil;
import com.cps.util.MailSender;

@ManagedBean
public class ContactController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(ContactController.class);
	private String name;
	private String email;
	private String phone;
	private String message;

	public void enviarEmail(ActionEvent event) {
		String sender = JsfUtil.getMessage("forgotMailFrom", FacesMessage.SEVERITY_INFO).getSummary();
		String body = "User Name ="+name+"\n"+
		              "Email :"+email+"\n"+
		              "Phone :"+phone+"\n"+
		              "Message"+message;
		try {
			MailSender.sendMail("CONTACTO CPS", body, sender, sender);
            
			JsfUtil.addMessage("contactForm.messageSent", FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			LOGGER.error("Error al enviar mail", e);
			JsfUtil.addMessage("serviceDisabled", FacesMessage.SEVERITY_INFO);
		}

	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
