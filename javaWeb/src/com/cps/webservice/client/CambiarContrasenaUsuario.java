
package com.cps.webservice.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pClaveVieja" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pClaveNueva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pUserName",
    "pClaveVieja",
    "pClaveNueva"
})
@XmlRootElement(name = "cambiarContrase\u00f1aUsuario")
public class CambiarContrasenaUsuario {

    @XmlElementRef(name = "pUserName", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pUserName;
    @XmlElementRef(name = "pClaveVieja", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pClaveVieja;
    @XmlElementRef(name = "pClaveNueva", namespace = "www.msj.go.cr:8090", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pClaveNueva;

    /**
     * Obtiene el valor de la propiedad pUserName.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPUserName() {
        return pUserName;
    }

    /**
     * Define el valor de la propiedad pUserName.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPUserName(JAXBElement<String> value) {
        this.pUserName = value;
    }

    /**
     * Obtiene el valor de la propiedad pClaveVieja.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPClaveVieja() {
        return pClaveVieja;
    }

    /**
     * Define el valor de la propiedad pClaveVieja.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPClaveVieja(JAXBElement<String> value) {
        this.pClaveVieja = value;
    }

    /**
     * Obtiene el valor de la propiedad pClaveNueva.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPClaveNueva() {
        return pClaveNueva;
    }

    /**
     * Define el valor de la propiedad pClaveNueva.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPClaveNueva(JAXBElement<String> value) {
        this.pClaveNueva = value;
    }

}
