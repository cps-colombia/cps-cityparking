DELIMITER $$
CREATE PROCEDURE updaterevision_log(_revision double , _author text, _description text)

begin 
DECLARE
  curRevision double ;
  select max(revision) from revision_log into curRevision;
  IF curRevision >= _revision THEN
  select 'no se pudo actualizar la versión';

   CALL EXCEPTION;
 ELSE
 insert into revision_log(revision,author,description) values (_revision, _author, _description);
  END IF;


END