-- MySQL dump 10.13  Distrib 5.5.39, for Linux (i686)
--
-- Host: localhost    Database: cps_city
-- ------------------------------------------------------
-- Server version	5.5.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_parking`
--

DROP TABLE IF EXISTS `active_parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_parking` (
  `idactiveParking` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` datetime DEFAULT NULL,
  `currentMins` int(11) DEFAULT NULL,
  `maxMins` int(11) DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `platform_idplatform` int(11) DEFAULT NULL,
  `enableSMSNotif` int(11) DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `place_idplace` varchar(80) DEFAULT NULL,
  `idhistory` bigint(20) DEFAULT NULL,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `zone_idzone` varchar(30) NOT NULL,
  `system_date` datetime DEFAULT NULL,
  PRIMARY KEY (`idactiveParking`),
  UNIQUE KEY `vehicle_plate_UNIQUE` (`vehicle_plate`),
  KEY `fk_activeParking_platform` (`platform_idplatform`),
  KEY `fk_active_parking_parking_history` (`idhistory`),
  KEY `FK_active_parking_user` (`idsys_user`),
  KEY `fk_activeParking_zone` (`place_idplace`) USING BTREE,
  KEY `FK_active_parking_zone` (`zone_idzone`),
  KEY `currentMinsIndex` (`currentMins`),
  CONSTRAINT `fk_activeParking_platform` FOREIGN KEY (`platform_idplatform`) REFERENCES `platform` (`idplatform`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_active_parking_parking_history` FOREIGN KEY (`idhistory`) REFERENCES `parking_history` (`idhistory`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_active_parking_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`),
  CONSTRAINT `FK_active_parking_zone` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_parking`
--

LOCK TABLES `active_parking` WRITE;
/*!40000 ALTER TABLE `active_parking` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_parking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `idagent` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `country_idcountry` bigint(20) DEFAULT NULL,
  `status` char(1) DEFAULT '',
  PRIMARY KEY (`idagent`),
  KEY `fk_agent_country` (`country_idcountry`),
  CONSTRAINT `fk_agent_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent`
--

LOCK TABLES `agent` WRITE;
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `agent_bi` BEFORE INSERT ON `agent` FOR EACH ROW begin 
set NEW.pass = SHA1(NEW.pass);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `agent_push_notification`
--

DROP TABLE IF EXISTS `agent_push_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_push_notification` (
  `idagent_push_notification` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `message` longtext NOT NULL,
  `idagent` bigint(20) NOT NULL,
  `dateNotif` datetime NOT NULL,
  `isSended` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idagent_push_notification`),
  KEY `FK_agent_push_notification_agent` (`idagent`),
  CONSTRAINT `FK_agent_push_notification_agent` FOREIGN KEY (`idagent`) REFERENCES `agent` (`idagent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_push_notification`
--

LOCK TABLES `agent_push_notification` WRITE;
/*!40000 ALTER TABLE `agent_push_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_push_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_subzone`
--

DROP TABLE IF EXISTS `agent_subzone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_subzone` (
  `idAgent` bigint(20) NOT NULL,
  `idSubzone` bigint(30) NOT NULL,
  PRIMARY KEY (`idAgent`,`idSubzone`),
  KEY `FK__idx` (`idAgent`),
  KEY `FK_zona_idzona_idx` (`idSubzone`),
  CONSTRAINT `FK_agent_idagent` FOREIGN KEY (`idAgent`) REFERENCES `agent` (`idagent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_subzona_idsubzona` FOREIGN KEY (`idSubzone`) REFERENCES `subzone` (`idsubzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_subzone`
--

LOCK TABLES `agent_subzone` WRITE;
/*!40000 ALTER TABLE `agent_subzone` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_subzone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_zone`
--

DROP TABLE IF EXISTS `agent_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_zone` (
  `idZone` varchar(30) NOT NULL,
  `idAgent` bigint(20) NOT NULL,
  PRIMARY KEY (`idZone`,`idAgent`),
  KEY `FK_agent_zone_user` (`idAgent`) USING BTREE,
  CONSTRAINT `FK_agent_zone_agent` FOREIGN KEY (`idAgent`) REFERENCES `agent` (`idagent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_zone`
--

LOCK TABLES `agent_zone` WRITE;
/*!40000 ALTER TABLE `agent_zone` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id_campaign` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `range_amount_start` decimal(15,2) DEFAULT NULL,
  `range_amount_end` decimal(15,2) DEFAULT NULL,
  `amount_percent` decimal(12,2) DEFAULT NULL,
  `campaign_state` int(11) NOT NULL,
  PRIMARY KEY (`id_campaign`),
  KEY `id_campaign_state_kf_idx` (`campaign_state`),
  CONSTRAINT `id_campaign_state_kf` FOREIGN KEY (`campaign_state`) REFERENCES `campaign_state` (`idcampaign_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign`
--

LOCK TABLES `campaign` WRITE;
/*!40000 ALTER TABLE `campaign` DISABLE KEYS */;
INSERT INTO `campaign` VALUES (2,'test','2014-08-14 13:32:09','2014-08-14 13:32:09',500.00,1000.00,50.00,2),(3,'Test andres para campaña de pruebas','2014-08-14 17:11:30','2014-08-21 17:11:30',10000.00,20000.00,10.00,2),(8,'test 100$','2014-08-28 18:10:39','2014-08-29 18:10:39',1.00,100.00,50.00,2),(9,'test','2014-09-25 19:12:43','2014-09-26 19:12:43',34.00,45.00,12.00,2),(10,'test sabado','2014-09-06 10:03:52','2014-09-09 10:03:52',10.00,20.00,1.00,2),(11,'test sabado 2','2014-09-24 10:47:48','2014-09-26 10:47:48',1.00,2.00,1.00,2),(13,'octubre prueba','2014-10-14 15:52:50','2014-10-15 15:52:50',1000.00,5000.00,20.00,2);
/*!40000 ALTER TABLE `campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_state`
--

DROP TABLE IF EXISTS `campaign_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_state` (
  `idcampaign_state` int(11) NOT NULL,
  `state_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idcampaign_state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_state`
--

LOCK TABLES `campaign_state` WRITE;
/*!40000 ALTER TABLE `campaign_state` DISABLE KEYS */;
INSERT INTO `campaign_state` VALUES (1,'activa'),(2,'finalizada'),(3,'suspendida');
/*!40000 ALTER TABLE `campaign_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_transaction_log`
--

DROP TABLE IF EXISTS `campaign_transaction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_transaction_log` (
  `idcampaign_transaction_log` varchar(20) NOT NULL,
  `id_sys_user` bigint(20) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `amount_percent` decimal(15,2) DEFAULT NULL,
  `total_amount` decimal(15,2) DEFAULT NULL,
  `log_date` datetime DEFAULT NULL,
  PRIMARY KEY (`idcampaign_transaction_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_transaction_log`
--

LOCK TABLES `campaign_transaction_log` WRITE;
/*!40000 ALTER TABLE `campaign_transaction_log` DISABLE KEYS */;
INSERT INTO `campaign_transaction_log` VALUES ('ID990900',885,2000.00,20.00,2400.00,'2014-10-14 16:23:25'),('TEST1',1114,200.00,0.00,200.00,'2014-08-28 18:15:50'),('TEST2',1114,100.00,50.00,150.00,'2014-08-28 18:17:05');
/*!40000 ALTER TABLE `campaign_transaction_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card` (
  `PIN` varchar(255) NOT NULL,
  `activationDate` datetime DEFAULT NULL,
  `price` decimal(7,2) NOT NULL,
  `used` int(11) NOT NULL DEFAULT '0',
  `idsys_user` bigint(20) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  PRIMARY KEY (`PIN`),
  KEY `fk_card_sys_user` (`idsys_user`),
  CONSTRAINT `fk_card_sys_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES ('8bb0b97698f489d41b6955a46383fa1f2d9001c5',NULL,20000.00,0,NULL,'COL');
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `passToSHA1` BEFORE INSERT ON `card` FOR EACH ROW begin 
set NEW.PIN = SHA1(NEW.PIN);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `card_virtual`
--

DROP TABLE IF EXISTS `card_virtual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_virtual` (
  `PIN` varchar(255) NOT NULL,
  `activationDate` datetime DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `used` int(11) NOT NULL DEFAULT '0',
  `idsys_user` bigint(20) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  PRIMARY KEY (`PIN`),
  KEY `fk_card_virtual_sys_user` (`idsys_user`),
  CONSTRAINT `fk_card_virtual_sys_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_virtual`
--

LOCK TABLES `card_virtual` WRITE;
/*!40000 ALTER TABLE `card_virtual` DISABLE KEYS */;
/*!40000 ALTER TABLE `card_virtual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `idcity` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `idcountry` bigint(20) NOT NULL,
  PRIMARY KEY (`idcity`),
  KEY `FK_city_country` (`idcountry`),
  CONSTRAINT `FK_city_country` FOREIGN KEY (`idcountry`) REFERENCES `country` (`idcountry`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (103,'Bogotá',472);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cityparking_transaction`
--

DROP TABLE IF EXISTS `cityparking_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cityparking_transaction` (
  `id_transaction` varchar(20) NOT NULL,
  `date_transaction` datetime NOT NULL,
  `id_sys_user` bigint(20) NOT NULL,
  `email_sys_user` varchar(70) DEFAULT NULL,
  `state_transaction` varchar(150) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `idzone` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cityparking_transaction`
--

LOCK TABLES `cityparking_transaction` WRITE;
/*!40000 ALTER TABLE `cityparking_transaction` DISABLE KEYS */;
INSERT INTO `cityparking_transaction` VALUES ('0','2014-06-06 13:05:02',881,'apereira@gmail.com','Rejected',2000.00,'CO',NULL),('10345775104','2014-06-05 09:35:16',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('1047590955666487393','2014-10-08 13:30:59',1136,'andres.pereirap@gmail.com','Rejected',10.00,'CO',NULL),('1048576','2014-04-14 08:16:26',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('1074266112','2014-04-14 08:33:38',936,'oswaldoaponte23@gmail.com','Completed',25000.00,'CO',NULL),('1082130432','2014-06-04 14:09:36',950,'operaciones11@city-parking.com','Rejected',25000.00,'CO',NULL),('1082654720','2014-04-14 07:58:35',893,'jharrylopez@gmail.com','Rejected',20000.00,'CO',NULL),('1099528404992','2014-03-27 14:08:45',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1099614388224','2014-05-30 18:15:33',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1099712954368','2014-04-04 07:50:03',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1099796840448','2014-01-16 09:41:47',881,'city1@mail.com','Rejected',15000.00,'CO',NULL),('1099832492032','2014-06-06 15:49:41',970,'gerenciadesarrollo@city-parking.com','Rejected',1000000.00,'CO',NULL),('1100050595840','2014-06-05 21:01:59',973,'osierrasoporte@gmail.com','Rejected',12345.00,'CO',NULL),('1100065275904','2014-01-17 11:59:04',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1100065800192','2014-04-02 21:53:34',917,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('1100141821952','2014-06-16 11:19:52',1076,'dianin0331@gmail.com','Completed',50000.00,'CO',NULL),('1100151914496','2014-02-12 14:54:46',881,'testmail@gmail.com','Completed',10000.00,'CO',NULL),('1101411123200','2014-06-06 16:08:04',970,'gerenciadesarrollo@city-parking.com','Rejected',1000000.00,'CO',NULL),('1101717831680','2014-01-16 09:42:25',881,'city1@mail.com','Completed',15000.00,'CO',NULL),('1103823372288','2013-12-30 17:31:04',885,'anubis_ex@hotmail.com','Rejected',20000.00,'CO',NULL),('1103843295232','2013-12-19 11:30:33',888,'andres.pereirap@gmail.com','Rejected',100000.00,'CO',NULL),('1103940812800','2014-05-01 14:37:09',929,'operaciones3@city-parking.com','Completed',25000.00,'CO',NULL),('1103957786624','2014-04-23 08:35:29',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1106013061120','2014-01-16 10:14:14',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1106147082240','2014-01-16 10:00:53',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('1108110082048','2014-01-16 10:10:23',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1108126728192','2013-12-18 16:05:21',888,'andres.pereirap@gmail.com','Rejected',100000.00,'CO',NULL),('1108253868032','2014-01-31 11:16:13',881,'gmail1@gmail.com','Rejected',10000.00,'CO',NULL),('1108272611328','2014-05-23 18:57:31',948,'adriana','Rejected',20000.00,'CO',NULL),('1108303020032','2014-05-30 15:21:58',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1108664713216','2014-06-06 08:26:48',974,'karolinafloyd@gmail.com','Rejected',30.00,'CO',NULL),('1110268641280','2014-06-16 11:13:39',1076,'dianin0331@gmail.com','Rejected',17366.00,'CO',NULL),('1110400106496','2014-06-14 18:53:22',948,'adriana','Rejected',10000.00,'CO',NULL),('1110520102912','2014-01-28 20:43:46',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1112398692352','2013-12-30 15:30:15',885,'anubis_ex@hotmail.com','Completed',20000.00,'CO',NULL),('1113473482752','2014-01-28 20:53:24',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('11148656640','2014-06-13 21:55:10',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('1116783771648','2014-06-16 11:14:34',1076,'dianin0331@gmail.com','Rejected',50000.00,'CO',NULL),('1116825714688','2014-03-04 06:14:37',881,'testmail@gmail.com','Rejected',50000.00,'CO',NULL),('1116963340288','2014-05-27 04:24:04',945,'johnamaya02@gmail.com','Rejected',10000.00,'CO',NULL),('1117329096704','2014-06-16 17:57:43',1095,'fabio_navarro@hotmail.com','Completed',30000.00,'CO',NULL),('1117397450752','2014-05-23 18:58:16',948,'adriana','Rejected',10000.00,'CO',NULL),('1117968924672','2014-05-05 10:34:26',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1118855757824','2014-06-11 18:58:53',1039,'pc.carlos.89@gmail.com','Rejected',10000.00,'CO',NULL),('1118856282112','2014-06-09 08:52:26',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1119069470720','2014-06-11 19:42:37',1042,'marinesluna@hotmail.com','Rejected',10000.00,'CO',NULL),('1119392825344','2014-03-25 08:31:58',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1125290868736','2014-05-28 10:08:32',909,'ebayon@city-parking.com','Rejected',22000.00,'CO',NULL),('1125602295808','2014-06-06 18:25:50',909,'ebayon@city-parking.com','Rejected',20000.00,'CO',NULL),('1125820071936','2014-05-23 19:01:45',948,'adriana','Rejected',10000.00,'CO',NULL),('1125836390400','2014-06-16 17:50:03',1096,'william.avila@gmail.com','Rejected',30000.00,'CO',NULL),('1149239296','2014-06-05 09:31:46',972,'alveiro1612@hotmail.com','Rejected',10.00,'CO',NULL),('1277689856','2014-06-08 10:05:02',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('12953780224','2014-01-16 12:31:18',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1345323008','2014-04-11 15:30:33',930,'operaciones5@city-parking.com','Rejected',25000.00,'CO',NULL),('134742016','2014-06-04 14:53:02',950,'operaciones11@city-parking.com','Completed',25000.00,'CO',NULL),('1374389534720','2014-01-30 17:49:47',881,'aljurado@gmail.com','Completed',5000.00,'CO',NULL),('1374418894848','2014-02-06 10:19:48',881,'testmail@gmail.com','Rejected',29000.00,'CO',NULL),('1374476566528','2014-06-14 20:51:43',1081,'rosamaliazadal@hotmail.com','Completed',50000.00,'CO',NULL),('1374628675584','2014-05-30 17:05:36',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1375484248064','2014-05-30 16:57:14',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1376828129280','2014-05-01 13:54:53',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1377074413568','2014-04-29 15:36:16',944,'misagars@hotmail.com','Rejected',25000.00,'CO',NULL),('1378684502016','2013-12-19 12:09:47',888,'andres.pereirap@gmail.com','Completed',10000.00,'CO',NULL),('1378726445056','2013-12-19 12:22:47',888,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('1378856140800','2014-05-05 04:43:31',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1379963764736','2014-05-05 11:06:02',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1381658263552','2014-05-02 08:41:08',946,'aaa@a.cl','Rejected',12222.00,'CO',NULL),('1381963923456','2014-05-05 04:42:07',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1383060865024','2014-06-13 18:50:13',1075,'etnarca@yahoo.com','Rejected',20000.00,'CO',NULL),('1383069253632','2013-12-18 15:51:48',885,'anubis_ex@hotmail.com','Completed',200.00,'CO',NULL),('1383174504448','2013-12-19 12:28:40',888,'andres.pereirap@gmail.com','Completed',10000.00,'CO',NULL),('1383248560128','2014-05-26 19:46:53',948,'adriana','Rejected',10000.00,'CO',NULL),('1383549042688','2014-06-17 07:10:39',948,'adrianag1807@hotmail.com','Rejected',10000.00,'CO',NULL),('1384069988352','2013-12-30 16:39:55',885,'anubis_ex@hotmail.com','Completed',20000.00,'CO',NULL),('1385162276864','2014-06-12 05:34:43',1048,'lachaves@gmail.com','Rejected',50000.00,'CO',NULL),('1385195175936','2014-06-13 07:12:38',1068,'ndmorame@hotmail.com','Rejected',50000.00,'CO',NULL),('1385213067264','2014-01-27 15:44:58',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('1385684795392','2014-01-30 17:43:56',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('1387316445184','2013-12-19 12:07:19',888,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('1387430739968','2013-12-19 12:07:18',888,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('1391573860352','2014-05-26 19:47:20',948,'adriana','Rejected',20000.00,'CO',NULL),('1391644901376','2014-06-16 11:40:46',1091,'helmancollante@gmail.com','Rejected',20.00,'CO',NULL),('1391651192832','2014-05-16 11:02:29',950,'operaciones11@city-parking.com','Rejected',5000.00,'CO',NULL),('1391860908032','2014-05-01 14:05:15',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1391902851072','2014-06-08 08:41:47',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1391948201984','2014-05-26 03:54:45',948,'adriana','Rejected',10000.00,'CO',NULL),('1392107651072','2014-02-11 09:59:26',893,'jharrylopez@gmail.com','Completed',50000.00,'CO',NULL),('1392124362752','2014-05-01 13:56:52',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1392732143616','2014-02-18 08:04:22',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('1393805230080','2014-04-23 01:22:07',941,'octavio.herrera@gmail.com','Rejected',50000.00,'CO',NULL),('1394256642048','2014-05-01 14:32:21',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1394606080000','2014-05-01 06:40:20',945,'johnamaya02@gmail.com','Rejected',25000.00,'CO',NULL),('1395881934848','2014-05-01 13:42:32',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1395885604864','2014-04-15 06:21:14',939,'edrojal777@gmail.com','Rejected',2000.00,'CO',NULL),('1396478574592','2014-04-03 07:53:28',917,'aljurado@gmail.com','Completed',10000.00,'CO',NULL),('1398034202624','2014-04-23 01:21:04',941,'octavio.herrera@gmail.com','Rejected',50000.00,'CO',NULL),('1398055305216','2014-04-23 12:27:25',909,'ebayon@city-parking.com','Completed',20000.00,'CO',NULL),('1400256790528','2014-06-16 11:52:49',1091,'helmancollante@gmail.com','Rejected',20.00,'CO',NULL),('1400495013888','2014-06-13 09:17:48',1049,'yestrigos@gmail.com','Rejected',8000.00,'CO',NULL),('1402349682688','2014-06-11 10:59:39',978,'sharly841022@hotmail.com','Rejected',25000.00,'CO',NULL),('1402447921152','2014-06-12 06:13:59',1049,'yestrigos@gmail.com','Rejected',50000.00,'CO',NULL),('14026604544','2013-12-30 10:46:36',885,'anubis_ex@hotmail.com','Completed',2000.00,'CO',NULL),('142606336','2014-06-04 14:10:27',948,'adriana','Rejected',10000.00,'CO',NULL),('1448870970756382997','2014-09-09 16:33:09',1121,'andres.pereirap@gmail.com','Rejected',1000.00,'CO',NULL),('15502213120','2014-01-16 12:31:57',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('1613234176','2014-06-05 09:33:35',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('1644691456','2014-06-03 10:39:19',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('17180786688','2014-04-14 08:22:55',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17180917760','2014-04-14 08:22:55',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17181048832','2014-04-14 08:22:56',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17183277056','2014-06-10 08:44:01',970,'gerenciadesarrollo@city-parking.com','Completed',1000000.00,'CO',NULL),('17189437440','2014-04-14 08:22:56',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17213489152','2014-05-28 05:09:46',945,'johnamaya02@gmail.com','Rejected',10000.00,'CO',NULL),('17213685760','2014-06-03 10:34:37',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('17223385088','2014-04-12 09:46:42',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('17257070592','2014-02-07 08:51:36',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('17281122304','2014-06-16 15:37:46',948,'operaciones10@city-parking.com','Rejected',10000.00,'CO',NULL),('17281908736','2014-06-16 15:28:45',948,'operaciones10@city-parking.com','Rejected',25000.00,'CO',NULL),('17315528704','2014-02-07 18:46:45',881,'testmail@gmail.com','Rejected',10.00,'CO',NULL),('17322475520','2014-02-07 08:52:18',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('17448566784','2014-04-12 09:44:41',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('17456955392','2014-04-12 09:53:19',934,'maxfutura1782@gmail.com','Completed',25000.00,'CO',NULL),('17583374336','2014-06-13 19:45:51',1075,'etnarca@yahoo.com','Rejected',50000.00,'CO',NULL),('17759993856','2014-06-16 15:27:51',948,'adriana','Rejected',25000.00,'CO',NULL),('17862033408','2014-06-05 09:37:11',971,'ggh@city-parking.com','Completed',25000.00,'CO',NULL),('18253938688','2014-05-31 08:00:55',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('18624151552','2014-04-12 09:47:10',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1879179264','2014-03-25 10:11:09',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('18798870528','2014-06-05 09:20:25',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('18824036352','2014-06-03 10:34:27',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('18858639360','2014-06-05 16:19:19',964,'operaciones4@city-parking.com','Rejected',25000.00,'CO',NULL),('18924961792','2014-06-04 14:14:44',948,'adriana','Rejected',20000.00,'CO',NULL),('19531038720','2014-06-11 12:17:19',1035,'andresnino@yahoo.com','Rejected',5000.00,'CO',NULL),('19976159232','2014-06-16 15:55:56',948,'operaciones10@city-parking.com','Rejected',25000.00,'CO',NULL),('1997787794420710570','2014-09-16 00:33:11',1135,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('2030552118907089409','2014-09-15 17:24:31',1111,'cquimbayo@cpsparking.ca','Rejected',2000.00,'CO',NULL),('2086347489597308093','2014-09-15 16:39:10',1111,'cquimbayo@cpsparking.ca','Rejected',2000.00,'CO',NULL),('21575761920','2014-04-21 15:00:52',940,'milenavw@hotmail.com','Completed',25000.00,'CO',NULL),('2157969408','2014-06-10 10:33:27',908,'caloman@hotmail.com','Completed',25000.00,'CO',NULL),('2214592512','2014-06-16 06:48:15',1086,'fernansala0526@gmail.com','Rejected',10000.00,'CO',NULL),('2217803776','2014-06-10 08:41:22',970,'gerenciadesarrollo@city-parking.com','Rejected',1000000.00,'CO',NULL),('22616539136','2014-04-11 22:26:38',935,'operaciones2@city-parking.com','Rejected',10000.00,'CO',NULL),('2316369920','2014-06-11 05:39:23',980,'stevenc_07@hotmail.com','Rejected',20.00,'CO',NULL),('2350383104','2014-06-14 15:12:36',1075,'etnarca@yahoo.com','Rejected',50000.00,'CO',NULL),('23622385664','2014-04-21 14:58:52',940,'milenavw@hotmail.com','Rejected',25000.00,'CO',NULL),('2383494344126646055','2014-09-15 16:43:22',1111,'cquimbayo@cpsparking.ca','Rejected',2000.00,'CO',NULL),('23894228992','2014-04-29 19:26:36',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('2415984640','2014-06-13 19:32:58',1075,'etnarca@yahoo.com','Rejected',50000.00,'CO',NULL),('2433750600104524200','2014-09-14 11:59:14',1129,'test15@gmail.com','Rejected',10000.00,'CO',NULL),('25769803776','2014-06-05 09:20:37',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('25769934848','2014-06-03 10:36:16',881,'apereira@gmail.com','Rejected',15000.00,'CO',NULL),('25778192384','2014-06-10 10:04:37',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('25912672256','2014-06-04 14:16:59',948,'adriana','Rejected',20000.00,'CO',NULL),('25971261440','2014-06-05 09:56:39',972,'alveiro1612@hotmail.com','Rejected',10.00,'CO',NULL),('26038763520','2014-06-12 18:27:28',1064,'constanza.guerrero@hotmail.com','Rejected',20000.00,'CO',NULL),('26316242944','2014-06-04 14:43:51',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('26316374016','2014-06-06 13:09:40',881,'apereira@gmail.com','Rejected',40.00,'CO',NULL),('26340229120','2014-06-08 18:37:07',948,'adriana','Rejected',10000.00,'CO',NULL),('268435456','2014-01-20 09:31:22',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('26843807744','2014-06-04 14:16:13',948,'adriana','Rejected',20000.00,'CO',NULL),('26852720640','2014-05-28 05:21:02',945,'johnamaya02@gmail.com','Completed',10000.00,'CO',NULL),('26911506432','2014-06-05 09:22:29',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('2693791744','2014-06-17 10:02:28',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('27380613120','2014-06-04 14:17:59',950,'operaciones11@city-parking.com','Rejected',25000.00,'CO',NULL),('27381465088','2014-06-04 14:44:12',950,'operaciones11@city-parking.com','Rejected',25000.00,'CO',NULL),('27389132800','2014-06-05 09:30:47',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('275158925312','2014-06-14 17:39:49',1043,'torradoj@hotmail.com','Rejected',20000.00,'CO',NULL),('275422117888','2014-06-16 14:48:29',948,'adriana','Rejected',25000.00,'CO',NULL),('275964362752','2014-04-12 10:52:37',935,'operaciones2@city-parking.com','Rejected',1000.00,'CO',NULL),('276289290240','2014-06-07 06:59:48',976,'city4@mail.com','Rejected',1000.00,'CO',NULL),('277058945024','2014-06-09 15:03:18',917,'aljurado@gmail.com','Rejected',25000.00,'CO',NULL),('277066285056','2014-06-10 09:49:57',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('277096693760','2014-04-21 08:46:32',909,'ebayon@city-parking.com','Rejected',50000.00,'CO',NULL),('277365391360','2014-01-27 10:15:13',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('278541631488','2014-01-28 17:07:58',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('279239983104','2013-12-18 10:09:42',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('279278780416','2013-12-18 11:02:17',890,'gerenciadesarrollo@city-parking.com','Rejected',200.00,'CO',NULL),('279621992448','2014-01-28 17:08:05',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('279710793728','2014-04-14 07:33:20',913,'soniayolima@gmail.com','Rejected',25000.00,'CO',NULL),('280559091712','2014-01-28 17:08:28',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('283468890112','2014-06-04 14:02:46',964,'operaciones4@city-parking.com','Rejected',25000.00,'CO',NULL),('283473805312','2014-06-07 06:44:07',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('283506245632','2014-06-09 14:55:49',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('283539144704','2013-12-18 10:10:52',881,'city1@mail.com','Rejected',300000.00,'CO',NULL),('283539275776','2013-12-18 10:10:53',881,'city1@mail.com','Rejected',300000.00,'CO',NULL),('283737849856','2014-06-07 06:43:38',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('284009955328','2014-01-31 07:55:26',881,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('284042592256','2014-01-08 09:43:15',891,'n_a_n_o84@hotmail.com','Completed',20000.00,'CO',NULL),('284140371968','2014-01-31 07:55:27',881,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('284277473280','2014-01-20 10:26:05',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('284541714432','2014-01-08 09:38:30',891,'n_a_n_o84@hotmail.com','Completed',50000.00,'CO',NULL),('284546957312','2014-06-04 15:39:13',965,'scabrera@cpsparking.ca','Rejected',20.00,'CO',NULL),('284684189696','2014-06-04 15:39:45',966,'gcabrera@cpsparking.ca','Rejected',20.00,'CO',NULL),('284790620160','2014-05-30 15:14:00',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('284847898624','2014-01-08 10:22:47',885,'anubis_ex@hotmail.com','Rejected',200.00,'CO',NULL),('285079633920','2014-06-04 15:39:22',965,'scabrera@cpsparking.ca','Rejected',20000.00,'CO',NULL),('285082648576','2014-06-07 06:51:40',976,'city4@mail.com','Rejected',10000.00,'CO',NULL),('285180166144','2014-06-07 06:43:30',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('285626859520','2014-06-17 11:59:08',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('285629087744','2014-06-17 11:58:19',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('285750329344','2014-01-31 07:55:51',881,'aljurado@gmail.com','Completed',100000.00,'CO',NULL),('287763398656','2013-12-18 10:20:54',888,'andres.pereirap@gmail.com','Rejected',120000.00,'CO',NULL),('288842579968','2014-01-31 07:56:35',881,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('290448408576','2014-01-20 10:45:10',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('2917470589074653039','2014-09-15 16:37:42',1111,'cquimbayo@cpsparking.ca','Rejected',2000.00,'CO',NULL),('292057907200','2014-03-20 10:32:20',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('292095787008','2014-06-09 14:58:47',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('292129341440','2014-02-12 10:45:33',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('292200382464','2014-06-11 06:09:29',980,'stevenc_07@hotmail.com','Rejected',5000.00,'CO',NULL),('292259299328','2014-02-19 16:32:55',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('292461871104','2014-03-31 08:07:03',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('292598841344','2014-05-23 12:08:21',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('292900831232','2014-06-07 06:55:17',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('293131583488','2014-06-04 16:00:26',917,'aljurado@gmail.com','Rejected',20000.00,'CO',NULL),('293165334528','2014-06-07 06:54:48',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('293436522496','2014-04-11 16:35:24',909,'ebayon@city-parking.com','Rejected',50000.00,'CO',NULL),('293539676160','2014-05-08 11:23:20',949,'joalex9226@yahoo.com','Rejected',10000.00,'CO',NULL),('294210568192','2014-06-09 14:42:02',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('294239141888','2014-06-10 08:54:26',948,'adriana','Rejected',10000.00,'CO',NULL),('294343737344','2014-06-11 06:01:21',980,'stevenc_07@hotmail.com','Rejected',10000.00,'CO',NULL),('294753140736','2014-06-16 07:46:20',1076,'dianin0331@gmail.com','Rejected',17366.00,'CO',NULL),('2966911005597676489','2014-09-15 16:45:40',1111,'cquimbayo@cpsparking.ca','Rejected',200000.00,'CO',NULL),('297461415936','2014-04-11 14:32:25',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('297473671168','2014-04-11 16:10:19',934,'maxfutura1782@gmail.com','Rejected',25000.00,'CO',NULL),('297574137856','2014-05-05 08:52:41',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('297734569984','2014-04-11 14:34:07',929,'operaciones3@city-parking.com','Rejected',250000.00,'CO',NULL),('298500292608','2014-04-23 14:29:38',909,'ebayon@city-parking.com','Rejected',50000.00,'CO',NULL),('299882577920','2014-05-06 10:46:25',936,'oswaldoaponte23@gmail.com','Rejected',40000.00,'CO',NULL),('300654133248','2014-06-13 21:22:57',1076,'dianin0331@gmail.com','Rejected',17366.00,'CO',NULL),('302834515968','2014-06-09 14:43:20',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('302897561600','2014-06-11 22:38:43',1047,'danielfelipecristobal@gmail.com','Rejected',1000.00,'CO',NULL),('3211264','2014-06-11 21:57:44',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('3289382912','2014-01-30 11:56:48',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('3497904239250132733','2014-09-15 16:55:53',1111,'cquimbayo@cpsparking.ca','Rejected',200.00,'CO',NULL),('3708892932230497425','2014-09-09 16:32:04',1121,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('4207515317562489681','2014-09-15 17:26:37',1111,'cquimbayo@cpsparking.ca','Rejected',2000.00,'CO',NULL),('4269658312269907102','2014-09-16 11:52:54',1136,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('5045501297856630706','2014-09-13 09:59:39',1129,'test15@gmail.com','Rejected',10000.00,'CO',NULL),('5084828710952257097','2014-09-16 11:52:54',1136,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('5104854464159499998','2014-10-03 15:49:00',1111,'cquimbayo@cpsparking.ca','Rejected',26000.00,'CO',NULL),('524288','2014-02-19 15:26:13',881,'testmail@gmail.com','Completed',15000.00,'CO',NULL),('5316721290905668235','2014-09-16 00:32:07',1135,'andres.pereirap@gmail.com','Rejected',100.00,'CO',NULL),('536870912','2014-06-16 15:23:24',948,'adriana','Rejected',25000.00,'CO',NULL),('5368774656','2013-12-30 10:31:08',885,'anubis_ex@hotmail.com','Rejected',200.00,'CO',NULL),('5378146304','2014-01-23 14:30:46',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('5453225914968919265','2014-10-03 19:27:13',1111,'cquimbayo@cpsparking.ca','Rejected',20000.00,'CO',NULL),('547356672','2014-06-03 10:55:31',881,'apereira@gmail.com','Rejected',1000.00,'CO',NULL),('5543352962125708269','2014-09-14 11:53:38',1129,'test15@gmail.com','Rejected',10000.00,'CO',NULL),('5645533184','2014-04-11 14:36:19',929,'operaciones3@city-parking.com','Rejected',25.00,'CO',NULL),('6094645130701718550','2014-09-14 11:59:35',1129,'test15@gmail.com','Rejected',1000.00,'CO',NULL),('6405065378176191746','2014-09-08 10:44:24',1118,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('7137753893416026703','2014-10-29 11:36:27',1136,'andres.pereirap@gmail.com','Rejected',1000.00,'CO',NULL),('7174973147248899221','2014-10-03 16:50:59',1111,'cquimbayo@cpsparking.ca','Rejected',20000.00,'CO',NULL),('7417711434653348452','2014-09-15 17:19:48',1111,'cquimbayo@cpsparking.ca','Rejected',2000.00,'CO',NULL),('7455319640410272101','2014-09-15 17:09:48',1111,'cquimbayo@cpsparking.ca','Rejected',2000.00,'CO',NULL),('7741171386094604018','2014-09-10 19:06:10',1111,'cquimbayo@cpsparking.ca','Rejected',3000.00,'CO',NULL),('8215188266122850709','2014-10-03 16:50:35',1111,'cquimbayo@cpsparking.ca','Rejected',20000.00,'CO',NULL),('8388608','2014-06-04 14:09:51',948,'adriana','Rejected',10000.00,'CO',NULL),('8590065664','2014-06-05 09:26:01',969,'wilmer.santana@gmail.com','Rejected',25000.00,'CO',NULL),('8590589952','2014-06-08 09:48:19',881,'apereira@gmail.com','Rejected',1000.00,'CO',NULL),('8591638528','2014-06-16 15:35:21',948,'operaciones10@city-parking.com','Rejected',20000.00,'CO',NULL),('8665432064','2014-06-16 16:00:25',948,'adrianag1807@hotmail.com','Rejected',25000.00,'CO',NULL),('8912896','2014-04-23 15:42:31',909,'ebayon@city-parking.com','Rejected',10000.00,'CO',NULL),('8971619248638966774','2014-09-16 11:53:14',1136,'andres.pereirap@gmail.com','Rejected',20000.00,'CO',NULL),('9035186176','2014-05-20 10:51:03',948,'adriana','Rejected',10000.00,'CO',NULL),('903910171292617109','2014-09-10 19:08:02',1111,'cquimbayo@cpsparking.ca','Rejected',23094.00,'CO',NULL),('9128902656','2014-06-16 16:09:35',881,'apereira@cpsparking.ca','Rejected',1.00,'CO',NULL),('ID990900','2014-10-14 16:23:25',885,'anubis_ex@hotmail.com','Completed',2000.00,'CO','COL21'),('TEST1','2014-08-28 16:09:35',1114,'test@mail.com','Completed',200.00,'CO',NULL),('TEST2','2014-08-28 16:09:35',1114,'test@mail.com','Completed',100.00,'CO',NULL);
/*!40000 ALTER TABLE `cityparking_transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cityparking_transaction_AINS` AFTER INSERT ON `cityparking_transaction`
FOR EACH ROW
BEGIN
 DECLARE percent  DECIMAL(12,2) default 0.0;
DECLARE amount_bonus  DECIMAL(12.2) default 0.0;
IF NEW.state_transaction = 'Completed' THEN
   select amount_percent into percent from campaign 
   where (now() between start_date and end_date) and
   (new.amount between range_amount_start and range_amount_end) and campaign_state=1 limit 1;
set amount_bonus := NEW.amount + ( NEW.amount * percent/100);
update sys_user set balance = balance +  amount_bonus  where idsys_user = NEW.id_sys_user;
INSERT INTO `cps_city`.`campaign_transaction_log`
(`idcampaign_transaction_log`,`id_sys_user`,`amount`,`amount_percent`,
`total_amount`,`log_date`)
VALUES(New.id_transaction,New.id_sys_user,New.amount,
percent,amount_bonus,now());
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cityparking_transaction_AUPD` AFTER UPDATE ON cityparking_transaction FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
 DECLARE percent  DECIMAL(12,2) default 0.0;
DECLARE amount_bonus  DECIMAL(12.2) default 0.0;
IF NEW.state_transaction = 'Completed' THEN
   select amount_percent into percent from campaign 
   where (now() between start_date and end_date) and
   (new.amount between range_amount_start and range_amount_end) and campaign_state=1 limit 1;
set amount_bonus := NEW.amount + ( NEW.amount * percent/100);
update sys_user set balance = balance +  amount_bonus  where idsys_user = NEW.id_sys_user;
INSERT INTO `cps_city`.`campaign_transaction_log`
(`idcampaign_transaction_log`,`id_sys_user`,`amount`,`amount_percent`,
`total_amount`,`log_date`)
VALUES(New.id_transaction,New.id_sys_user,New.amount,
percent,amount_bonus,now());
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `idclient` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `country_idcountry` bigint(20) DEFAULT NULL,
  `city_idcity` bigint(20) unsigned DEFAULT NULL,
  `status` char(1) DEFAULT '',
  PRIMARY KEY (`idclient`),
  KEY `fk_client_country` (`country_idcountry`),
  KEY `FK_client_city` (`city_idcity`),
  CONSTRAINT `FK_client_city` FOREIGN KEY (`city_idcity`) REFERENCES `city` (`idcity`),
  CONSTRAINT `fk_client_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (3,'CITYPARKING',472,103,'');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `idcountry` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `countryPrefix` varchar(4) NOT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `lang` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`idcountry`),
  UNIQUE KEY `prefix` (`countryPrefix`)
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (472,'Colombia','COL','4.0000','-72.0000','ENG');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_app_version`
--

DROP TABLE IF EXISTS `device_app_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_app_version` (
  `device_id` int(11) NOT NULL,
  `device_name` varchar(45) NOT NULL,
  `current_version` varchar(45) NOT NULL,
  `lastchange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_app_version`
--

LOCK TABLES `device_app_version` WRITE;
/*!40000 ALTER TABLE `device_app_version` DISABLE KEYS */;
INSERT INTO `device_app_version` VALUES (1,'android','1.8.5','2014-06-19 06:00:00'),(2,'iphone','1.8.5','2014-09-05 01:38:56'),(3,'windows','1.8.5','2014-09-10 01:38:56');
/*!40000 ALTER TABLE `device_app_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integrator`
--

DROP TABLE IF EXISTS `integrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integrator` (
  `idintegrator` bigint(20) NOT NULL AUTO_INCREMENT,
  `shortCode` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  `country_idcountry` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idintegrator`),
  KEY `fk_integrator_country` (`country_idcountry`),
  CONSTRAINT `fk_integrator_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integrator`
--

LOCK TABLES `integrator` WRITE;
/*!40000 ALTER TABLE `integrator` DISABLE KEYS */;
/*!40000 ALTER TABLE `integrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msisdn_user`
--

DROP TABLE IF EXISTS `msisdn_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msisdn_user` (
  `idmsisdn_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(15) DEFAULT NULL,
  `idsys_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idmsisdn_user`),
  UNIQUE KEY `msisdn_unique` (`msisdn`),
  KEY `fk_msisdn_user_sys_user` (`idsys_user`)
) ENGINE=InnoDB AUTO_INCREMENT=649 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msisdn_user`
--

LOCK TABLES `msisdn_user` WRITE;
/*!40000 ALTER TABLE `msisdn_user` DISABLE KEYS */;
INSERT INTO `msisdn_user` VALUES (463,'6210355',881),(466,NULL,883),(467,NULL,884),(468,'3163113240',885),(469,'3014302402',886),(471,'1223344566',887),(472,'3164976452',888),(475,'3164976456',889),(477,'3164976451',891),(478,'86487838',892),(479,'3105511906',893),(480,'42312431241',894),(483,'123123',906),(484,'7765',907),(485,'3168789072',908),(486,'3164230079',909),(489,'3143701492',912),(490,'3112011487',913),(491,'33333333',914),(492,'44444444',915),(493,'5555555',916),(494,'3116052106',917),(495,'956677088',918),(496,'3124645625',919),(497,'5263738383',920),(498,'1234567890',921),(499,'444444',922),(500,'1234213432',927),(501,'8447776',928),(502,'3174230074',929),(503,'3174230070',930),(504,'3188017826',934),(505,'3174230073',935),(506,'3168748323',936),(507,'3812369852',937),(508,'3118299490',938),(509,'3003012307',939),(510,'3108777269',940),(511,'3006136887',941),(512,'34532567',942),(513,'737373773',943),(514,'3186296890',944),(515,'3185159631',945),(516,'300333333',946),(517,'3134950053',947),(518,'3174316800',948),(519,'3007476111',949),(520,'3174230076',950),(521,'2332323',952),(522,'3102313437',953),(523,'3115806779',956),(524,'3002069082',957),(525,'3174230075',958),(526,' 50686487838',959),(528,'50670161744',959),(529,'3102826220',960),(531,'3174230072',964),(532,'4162067471',965),(533,'6477024530',966),(534,'6478810718',967),(535,'3006091656',969),(536,'3183530879',970),(537,'3186126551',971),(538,'3115667439',972),(539,'316579',973),(540,'3174589231',974),(541,'6479939089',975),(542,'83735252',976),(543,'3153929819',977),(544,'3164972576',978),(545,'3174025443',979),(546,'3173637079',980),(558,'3214857009',1032),(559,'3045230276',1033),(560,'3005712534',1034),(561,'3003590187',1035),(562,'3164708014',1036),(563,'3202785747',1037),(564,'3002143224',1038),(565,'3176805848',1039),(566,'3002645833',1040),(567,'3107998875',1041),(568,'3212009574',1042),(569,'3006009156',1043),(570,'3002046334',1044),(571,'3208594987',1045),(572,'3002004531',1046),(573,'3159267344',1047),(574,'3133945236',1048),(575,'3002086125',1049),(576,'3203081041',1050),(577,'3164976455',1051),(578,'3125278522',1052),(579,'3102224197',1053),(580,'3213150959',1062),(581,'3006199388',1063),(582,'3115905789',1064),(583,'3143251130',1065),(584,'3043773303',1066),(585,'3016735047',1067),(586,'3153609944',1068),(587,'3112081250',1069),(588,'3214907449',1070),(589,'3008029026',1071),(590,'3204325279',1072),(591,'3103244517',1073),(592,'3153481016',1074),(593,'3164814936',1075),(594,'3183833424',1076),(595,'3002006781',1077),(596,'3165219619',1078),(597,'3132776909',1079),(598,'3017465927',1080),(599,'3187076266',1081),(600,'3158995671',1082),(601,'3164739281',1083),(602,'3176607467',1084),(603,'3114621219',1085),(604,'  321',1086),(605,'3012302917',1091),(606,'3153452862',1095),(607,'3005504707',1096),(608,'3103486670',1097),(609,'3183592538',1098),(610,'3124358037',1099),(611,'3176436286',1100),(612,'2484664444',1101),(613,'346491',1102),(614,'9239382',1103),(615,'city7',1104),(616,'98383838383',1105),(622,'3178795959',1111),(623,'31938348',1112),(624,'328937436',1113),(625,'349183',1114),(626,'3939',4),(627,'1234543',5),(628,'33421111111',1115),(629,'123454312',1116),(630,'393823',1117),(631,'78982625',1118),(633,'82727626',1119),(634,'54848425',1121),(635,'3162582836',1122),(636,'3178895940',1123),(637,'586655555',1124),(638,'3178503478',1126),(639,'4567890980',1128),(640,'3152583625',1129),(643,'3164976254',1134),(644,'3152584758',1135),(645,'3158547252',1136),(646,'3002582838',1137),(647,'34124444444',1138),(648,'3006589076',14057);
/*!40000 ALTER TABLE `msisdn_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `idnotification` bigint(20) NOT NULL AUTO_INCREMENT,
  `message` varchar(160) DEFAULT NULL,
  `isread` int(11) DEFAULT NULL,
  `platform_idplatform` int(11) DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `dateNotif` datetime DEFAULT NULL,
  PRIMARY KEY (`idnotification`),
  KEY `fk_notification_platform` (`platform_idplatform`),
  KEY `FK_notification_user` (`idsys_user`),
  CONSTRAINT `fk_notification_platform` FOREIGN KEY (`platform_idplatform`) REFERENCES `platform` (`idplatform`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_notification_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parking_history`
--

DROP TABLE IF EXISTS `parking_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parking_history` (
  `idhistory` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `platform_idplatform` int(11) DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `place_idplace` varchar(80) DEFAULT NULL,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `zone_idzone` varchar(30) NOT NULL,
  `pay_id` varchar(45) DEFAULT NULL,
  `fee` double DEFAULT NULL,
  `confirm_number` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`idhistory`),
  KEY `fk_parking_history_platform` (`platform_idplatform`),
  KEY `FK_parking_history_user` (`idsys_user`),
  KEY `fk_parking_history_place` (`place_idplace`) USING BTREE,
  KEY `FK_parking_history_zone` (`zone_idzone`) USING BTREE,
  CONSTRAINT `fk_parking_history_platform` FOREIGN KEY (`platform_idplatform`) REFERENCES `platform` (`idplatform`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_parking_history_zones` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB AUTO_INCREMENT=1263 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parking_history`
--

LOCK TABLES `parking_history` WRITE;
/*!40000 ALTER TABLE `parking_history` DISABLE KEYS */;
INSERT INTO `parking_history` VALUES (1121,'2014-04-04 10:46:14','2014-04-04 10:46:22','3105511906',2,0.00,'0','',893,'COL71','15',100,'7115'),(1122,'2014-04-04 10:59:45','2014-04-04 10:59:52','3116052106',2,0.00,'0','',917,'COL71','16',100,'7116'),(1123,'2014-04-04 11:00:30','2014-04-04 11:00:37','3116052106',2,0.00,'0','',917,'COL71','17',100,'7117'),(1124,'2014-04-12 11:04:10','2014-04-12 11:07:40','3188017826',2,0.00,'0','',934,'COL22','2',300,'222'),(1125,'2014-04-12 11:21:56','2014-04-12 13:08:48','3188017826',2,0.00,'0','',934,'COL69','7',10200,'697'),(1126,'2014-04-15 08:40:50','2014-04-15 09:09:04','3188017826',2,0.00,'0','',934,'COL31','7',1900,'317'),(1127,'2014-04-16 15:51:13','2014-04-16 16:03:21','3116052106',2,0.00,'0','',917,'COL97','7',850,'977'),(1128,'2014-04-21 07:56:47','2014-04-21 10:57:02','3188017826',2,0.00,'0','',934,'COL71','19',9000,'7119'),(1129,'2014-04-25 11:12:06','2014-04-25 11:43:55','3105511906',2,0.00,'0','',893,'COL37','21',2150,'3721'),(1130,'2014-04-25 07:55:56','2014-04-25 11:50:47','3105511906',2,0.00,'0','',893,'COL37','22',2600,'3722'),(1131,'2014-04-25 12:21:13','2014-04-25 13:56:47','3164230079',2,0.00,'0','',909,'COL23','8',1000,'238'),(1132,'2014-04-30 08:37:32','2014-04-30 18:16:17','3164230079',2,0.00,'0','',909,'COL71','26',4000,'7126'),(1133,'2014-05-02 08:52:13','2014-05-02 08:52:26','3174230074',2,0.00,'0','',929,'COL25','3',100,'253'),(1134,'2014-05-02 08:56:49','2014-05-02 08:56:58','3174230074',2,0.00,'0','',929,'COL25','4',100,'254'),(1135,'2014-05-02 09:18:55','2014-05-02 09:19:54','3174230074',2,0.00,'0','',929,'COL33','1',100,'331'),(1136,'2014-05-02 09:22:57','2014-05-02 09:23:04','3174230074',2,0.00,'0','',929,'COL33','2',100,'332'),(1137,'2014-05-02 09:44:36','2014-05-02 09:51:24','3174230074',2,0.00,'0','',929,'COL102','1',100,'1021'),(1138,'2014-05-02 09:52:39','2014-05-02 09:52:48','3174230074',2,0.00,'0','',929,'COL102','2',100,'1022'),(1139,'2014-05-02 11:15:45','2014-05-02 11:43:10','3174230074',2,0.00,'0','',929,'COL37','23',1900,'3723'),(1140,'2014-05-03 09:59:14','2014-05-03 10:00:49','3174230074',2,0.00,'0','',929,'COL25','8',150,'258'),(1141,'2014-05-03 10:35:53','2014-05-03 11:11:33','3174230074',2,0.00,'0','',929,'COL37','24',2450,'3724'),(1142,'2014-05-05 09:19:57','2014-05-05 09:20:38','3174230074',2,0.00,'0','',929,'COL102','3',100,'1023'),(1143,'2014-05-06 09:59:40','2014-05-06 11:34:15','3164230079',2,0.00,'0','',909,'COL122','13',950,'12213'),(1144,'2014-05-06 12:59:57','2014-05-06 13:11:03','3174230074',2,0.00,'0','',929,'COL102','4',700,'1024'),(1145,'2014-05-07 09:05:12','2014-05-07 09:14:37','3174230074',2,0.00,'0','',929,'COL25','10',950,'2510'),(1146,'2014-05-07 12:47:41','2014-05-07 13:29:32','3174230074',2,0.00,'0','',929,'COL37','25',2850,'3725'),(1147,'2014-05-07 13:33:39','2014-05-07 13:40:50','3174230074',2,0.00,'0','',929,'COL37','26',550,'3726'),(1148,'2014-05-07 14:00:11','2014-05-07 14:00:45','3174230074',2,0.00,'0','',929,'COL37','27',100,'3727'),(1149,'2014-05-07 17:15:20','2014-05-07 17:15:40','3164230079',2,0.00,'0','',909,'COL90','3',100,'903'),(1150,'2014-05-09 08:21:52','2014-05-09 08:25:39','3164230079',2,0.00,'0','',909,'COL37','28',100,'3728'),(1151,'2014-05-11 10:51:33','2014-05-11 10:51:52','3174230074',2,0.00,'0','',929,'COL37','30',100,'3730'),(1152,'2014-05-16 07:49:17','2014-05-16 09:05:04','3164230079',2,0.00,'0','',909,'COL23','11',800,'2311'),(1153,'2014-05-20 10:28:30','2014-05-20 10:28:39','3174230074',2,0.00,'0','',929,'COL33','3',100,'333'),(1154,'2014-05-24 09:06:23','2014-05-24 12:02:27','3164230079',2,0.00,'0','',909,'COL21','10',1800,'2110'),(1155,'2014-05-28 09:40:32','2014-05-28 10:01:03','3185159631',2,0.00,'0','',945,'COL16','71',1350,'1671'),(1156,'2014-05-28 10:43:16','2014-05-28 10:55:25','3185159631',2,0.00,'0','',945,'COL18','7',900,'187'),(1157,'2014-05-28 11:20:21','2014-05-28 11:35:53','3185159631',2,0.00,'0','',945,'COL101','5',1100,'1015'),(1158,'2014-05-28 14:34:07','2014-05-28 14:47:10','3185159631',2,0.00,'0','',945,'COL70','9',900,'709'),(1159,'2014-05-28 19:10:48','2014-05-28 19:19:06','3186296890',2,0.00,'0','',944,'COL108','6',450,'1086'),(1160,'2014-05-29 07:26:13','2014-05-29 07:26:25','3174230070',2,0.00,'0','',930,'COL90','1',100,'901'),(1161,'2014-05-29 07:18:24','2014-05-29 07:38:01','3112011487',2,0.00,'0','',913,'COL103','4',350,'1034'),(1162,'2014-05-29 07:52:15','2014-05-29 07:53:31','3174230070',2,0.00,'0','',930,'COL11','10',150,'1110'),(1163,'2014-05-29 08:46:20','2014-05-29 08:49:43','3174230070',2,0.00,'0','',930,'COL29','32',300,'2932'),(1164,'2014-05-29 09:15:00','2014-05-29 09:15:10','3174230070',2,0.00,'0','',930,'COL22','6',100,'226'),(1165,'2014-05-29 09:38:58','2014-05-29 09:39:49','3174230070',2,0.00,'0','',930,'COL77','8',100,'778'),(1166,'2014-05-29 10:42:52','2014-05-29 10:46:59','3174230070',2,0.00,'0','',930,'COL46','19',400,'4619'),(1167,'2014-05-29 13:08:55','2014-05-29 13:12:59','3186296890',2,0.00,'0','',944,'COL49','6',400,'496'),(1168,'2014-05-30 09:48:48','2014-05-30 09:49:53','3174230074',2,0.00,'0','',929,'COL25','11',100,'2511'),(1169,'2014-05-30 10:33:07','2014-05-30 10:33:19','3174230074',2,0.00,'0','',929,'COL33','4',100,'334'),(1170,'2014-05-30 14:50:16','2014-05-30 14:53:23','3186296890',2,0.00,'0','',944,'COL118','1',150,'1181'),(1171,'2014-05-30 15:42:47','2014-05-30 16:05:38','3186296890',2,0.00,'0','',944,'COL24','2',1550,'242'),(1172,'2014-05-30 20:15:22','2014-05-30 20:18:07','3174230070',2,0.00,'0','',930,'COL92','10',300,'9210'),(1173,'2014-05-30 20:33:54','2014-05-30 20:34:17','3174230070',2,0.00,'0','',930,'COL33','5',100,'335'),(1174,'2014-05-30 20:37:42','2014-05-30 20:37:52','3174230070',2,0.00,'0','',930,'COL33','6',100,'336'),(1175,'2014-05-30 20:55:26','2014-05-30 20:58:15','3174230070',2,0.00,'0','',930,'COL95','10',250,'9510'),(1176,'2014-05-30 22:28:59','2014-05-30 22:30:53','3174230070',2,0.00,'0','',930,'COL37','37',200,'3737'),(1177,'2014-05-30 22:57:25','2014-05-30 22:57:35','3174230074',2,0.00,'0','',929,'COL102','5',100,'1025'),(1178,'2014-05-30 23:24:23','2014-05-30 23:24:33','3174230074',2,0.00,'0','',929,'COL46','20',100,'4620'),(1179,'2014-06-01 12:12:41','2014-06-01 12:13:12','3174230070',2,0.00,'0','',930,'COL9','7',100,'97'),(1180,'2014-06-01 18:03:49','2014-06-01 18:04:35','3174230070',2,0.00,'0','',930,'COL46','21',100,'4621'),(1181,'2014-06-04 09:00:16','2014-06-04 09:00:37','3185159631',2,0.00,'0','',945,'COL18','9',100,'189'),(1182,'2014-06-04 11:13:18','2014-06-04 11:24:05','3185159631',2,0.00,'0','',945,'COL70','11',750,'7011'),(1183,'2014-06-05 10:14:32','2014-06-05 10:16:21','3174230076',2,0.00,'0','',950,'COL109','32',150,'10932'),(1184,'2014-06-05 10:52:25','2014-06-05 10:54:18','3116052106',2,0.00,'0','',917,'COL71','34',200,'7134'),(1185,'2014-06-05 10:52:34','2014-06-05 11:09:15','3185159631',2,0.00,'0','',945,'COL101','8',1150,'1018'),(1186,'2014-06-05 12:11:58','2014-06-05 12:19:30','3174230076',2,0.00,'0','',950,'COL35','7',550,'357'),(1187,'2014-06-05 13:58:46','2014-06-05 14:00:00','3174230076',2,0.00,'0','',950,'COL4','6',200,'46'),(1190,'2014-06-06 12:17:44','2014-06-06 12:20:02','3112011487',2,0.00,'0','',913,'COL149','3',100,'1493'),(1192,'2014-06-06 21:39:16','2014-06-06 21:39:45','3186296890',2,0.00,'0','',944,'COL21','12',100,'2112'),(1193,'2014-01-16 10:25:23','2014-01-16 10:25:39','83735252',2,0.00,'0','',976,'COL77','16',100,'7716'),(1194,'2014-02-05 11:29:42','2014-02-05 11:29:49','83735252',2,0.00,'0','',976,'COL77','53',100,'7753'),(1197,'2014-06-10 09:09:22','2014-06-10 09:09:30','3174230076',2,0.00,'0','',950,'COL139','10',100,'13910'),(1198,'2014-06-10 09:10:55','2014-06-10 09:11:14','3174230076',2,0.00,'0','',950,'COL139','11',100,'13911'),(1199,'2014-06-10 09:30:44','2014-06-10 09:45:23','3174230076',2,0.00,'0','',950,'COL35','8',1050,'358'),(1200,'2014-06-10 10:36:11','2014-06-10 10:36:17','3105511906',2,0.00,'0','',893,'COL71','36',100,'7136'),(1201,'2014-06-11 07:47:18','2014-06-11 07:47:31','3112011487',2,0.00,'0','',913,'COL100','12',50,'10012'),(1202,'2014-06-11 08:10:08','2014-06-11 08:16:25','3112011487',2,0.00,'0','',913,'COL6','6',500,'66'),(1203,'2014-06-11 09:05:31','2014-06-11 09:23:58','3112011487',2,0.00,'0','',913,'COL42','124',1300,'42124'),(1204,'2014-06-11 09:49:17','2014-06-11 09:49:27','3174230074',2,0.00,'0','',929,'COL62','17',100,'6217'),(1205,'2014-06-11 10:13:48','2014-06-11 10:14:33','3174230074',2,0.00,'0','',929,'COL115','2',50,'1152'),(1206,'2014-06-11 10:14:20','2014-06-11 10:17:02','3112011487',2,0.00,'0','',913,'COL21','13',250,'2113'),(1207,'2014-06-11 10:17:35','2014-06-11 10:17:54','3174230076',2,0.00,'0','',950,'COL109','37',100,'10937'),(1208,'2014-06-11 10:19:11','2014-06-11 11:10:54','3174230076',2,0.00,'0','',950,'COL109','38',2900,'10938'),(1209,'2014-06-11 11:38:31','2014-06-11 11:48:51','3174230076',2,0.00,'0','',950,'COL139','12',850,'13912'),(1210,'2014-06-11 12:08:19','2014-06-11 12:22:06','3174230076',2,0.00,'0','',950,'COL35','11',950,'3511'),(1211,'2014-06-11 12:45:35','2014-06-11 12:46:59','3168789072',2,0.00,'0','',908,'COL122','25',200,'12225'),(1212,'2014-06-11 12:52:45','2014-06-11 12:53:29','3168789072',2,0.00,'0','',908,'COL29','42',100,'2942'),(1213,'2014-06-11 12:41:25','2014-06-11 14:29:53','3003590187',2,0.00,'0','',1035,'COL77','10',10400,'7710'),(1214,'2014-06-11 14:10:46','2014-06-11 14:34:29','3168748323',2,0.00,'0','',936,'COL37','39',1650,'3739'),(1215,'2014-06-11 12:28:51','2014-06-11 14:42:04','3102826220',2,0.00,'0','',960,'COL77','12',12650,'7712'),(1216,'2014-06-11 15:32:54','2014-06-11 15:33:37','3112011487',2,0.00,'0','',913,'COL116','2',100,'1162'),(1217,'2014-06-11 15:33:50','2014-06-11 15:35:07','3112011487',2,0.00,'0','',913,'COL38','145',150,'38145'),(1218,'2014-06-11 16:41:43','2014-06-11 16:47:42','3174230076',2,0.00,'0','',950,'COL29','43',600,'2943'),(1219,'2014-06-11 17:02:14','2014-06-11 17:02:25','3174230074',2,0.00,'0','',929,'COL102','6',100,'1026'),(1220,'2014-06-11 17:10:40','2014-06-11 17:11:59','3174230074',2,0.00,'0','',929,'COL62','18',200,'6218'),(1221,'2014-06-11 17:15:05','2014-06-11 17:15:59','3168748323',2,0.00,'0','',936,'COL129','7',100,'1297'),(1222,'2014-06-11 17:43:10','2014-06-11 17:43:55','3174230074',2,0.00,'0','',929,'COL25','13',100,'2513'),(1223,'2014-06-12 10:12:02','2014-06-12 10:17:26','3174230076',2,0.00,'0','',950,'COL129','8',450,'1298'),(1224,'2014-06-12 10:18:23','2014-06-12 10:18:30','3174230076',2,0.00,'0','',950,'COL129','9',100,'1299'),(1225,'2014-06-12 10:27:34','2014-06-12 10:27:42','3174230074',2,0.00,'0','',929,'COL33','8',100,'338'),(1226,'2014-06-12 11:11:28','2014-06-12 11:11:40','3174230074',2,0.00,'0','',929,'COL115','4',50,'1154'),(1227,'2014-06-12 11:44:51','2014-06-12 11:45:08','3174230074',2,0.00,'0','',929,'COL37','40',100,'3740'),(1228,'2014-06-12 09:51:17','2014-06-12 11:58:19','3108777269',2,0.00,'0','',940,'COL139','16',9550,'13916'),(1229,'2014-06-12 14:09:31','2014-06-12 14:11:20','3186296890',2,0.00,'0','',944,'COL71','37',150,'7137'),(1230,'2014-06-12 17:25:34','2014-06-12 17:26:44','3185159631',2,0.00,'0','',945,'COL71','38',100,'7138'),(1231,'2014-06-13 10:32:42','2014-06-13 10:32:58','3185159631',2,0.00,'0','',945,'COL18','12',100,'1812'),(1232,'2014-06-13 11:00:59','2014-06-13 11:27:35','3174230076',2,0.00,'0','',950,'COL139','17',1950,'13917'),(1233,'2014-06-13 11:49:53','2014-06-13 11:50:29','3174230076',2,0.00,'0','',950,'COL35','12',100,'3512'),(1234,'2014-06-13 14:16:38','2014-06-13 14:16:52','3185159631',2,0.00,'0','',945,'COL70','15',100,'7015'),(1235,'2014-06-14 14:13:20','2014-06-14 14:13:33','3185159631',2,0.00,'0','',945,'COL139','18',100,'13918'),(1236,'2014-06-16 06:08:16','2014-06-16 08:12:20','3108777269',2,0.00,'0','',940,'COL108','7',8350,'1087'),(1237,'2014-06-16 16:57:43','2014-06-16 16:58:28','3186126551',2,0.00,'0','',971,'COL71','40',100,'7140'),(1238,'2014-06-17 08:02:46','2014-06-17 08:02:54','3112011487',2,0.00,'0','',913,'COL45','2',50,'452'),(1239,'2014-06-17 10:00:12','2014-06-17 10:05:40','3174230076',2,0.00,'0','',950,'COL129','12',450,'12912'),(1240,'2014-06-17 13:02:08','2014-06-17 13:04:42','3174230076',2,0.00,'0','',950,'COL4','7',300,'47'),(1241,'2014-06-17 13:26:47','2014-06-17 13:27:03','3112011487',2,0.00,'0','',913,'COL123','9',100,'1239'),(1242,'2014-01-28 17:43:06','2014-01-30 15:03:53','78982625',2,0.00,'0','',1118,'COL77','46',100,'7746'),(1243,'2014-09-08 10:18:40','2014-09-08 10:28:28','78982625',2,0.00,'0','',1118,'COL77','85',100,'7785'),(1244,'2014-09-08 10:18:40','2014-09-08 10:28:34','78982625',2,0.00,'0','',1118,'COL77','86',100,'7786'),(1245,'2014-09-08 10:18:40','2014-09-08 10:28:37','78982625',2,0.00,'0','',1118,'COL77','87',100,'7787'),(1246,'2014-09-08 10:18:40','2014-09-08 10:28:40','78982625',2,0.00,'0','',1118,'COL77','88',100,'7788'),(1247,'2014-09-08 10:18:40','2014-09-08 10:31:04','3178795959',2,0.00,'0','',1111,'COL77','125',100,'77125'),(1248,'2014-09-08 10:18:40','2014-09-08 10:30:54','3178795959',2,0.00,'0','',1111,'COL77','120',100,'77120'),(1249,'2014-09-08 10:18:40','2014-09-08 10:30:02','3152584758',2,0.00,'0','',1135,'COL77','100',100,'77100'),(1250,'2014-09-08 10:18:40','2014-09-08 10:30:05','3152584758',2,0.00,'0','',1135,'COL77','101',100,'77101'),(1251,'2014-09-08 10:18:40','2014-09-08 10:30:08','3152584758',2,0.00,'0','',1135,'COL77','102',100,'77102'),(1252,'2014-09-08 10:18:40','2014-09-08 10:30:10','3152584758',2,0.00,'0','',1135,'COL77','103',100,'77103'),(1253,'2014-09-08 10:18:40','2014-09-08 10:30:56','3178795959',2,0.00,'0','',1111,'COL77','121',100,'77121'),(1254,'2014-09-08 10:18:40','2014-09-08 10:30:12','3158547252',2,0.00,'0','',1136,'COL77','104',100,'77104'),(1255,'2014-09-08 10:18:40','2014-09-08 10:30:41','3158547252',2,0.00,'0','',1136,'COL77','115',100,'77115'),(1256,'2014-09-08 10:18:40','2014-09-08 10:30:44','3158547252',2,0.00,'0','',1136,'COL77','116',100,'77116'),(1257,'2014-09-08 10:18:40','2014-09-08 10:31:15','3178795959',2,0.00,'0','',1111,'COL77','130',100,'77130'),(1258,'2014-09-08 10:18:40','2014-09-08 10:31:12','3178795959',2,0.00,'0','',1111,'COL77','129',100,'77129'),(1259,'2014-09-08 10:18:40','2014-09-08 10:31:08','3178795959',2,0.00,'0','',1111,'COL77','127',100,'77127'),(1260,'2014-09-08 10:18:40','2014-09-08 10:31:08','3178795959',2,0.00,'0','',1111,'COL77','126',100,'77126'),(1261,'2014-09-08 10:18:40','2014-09-08 10:31:02','3178795959',2,0.00,'0','',1111,'COL77','124',100,'77124'),(1262,'2014-09-08 10:18:40','2014-09-08 10:31:10','3178795959',2,0.00,'0','',1111,'COL77','128',100,'77128');
/*!40000 ALTER TABLE `parking_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `parking_history_view`
--

DROP TABLE IF EXISTS `parking_history_view`;
/*!50001 DROP VIEW IF EXISTS `parking_history_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `parking_history_view` (
  `idhistory` tinyint NOT NULL,
  `startTime` tinyint NOT NULL,
  `endTime` tinyint NOT NULL,
  `msisdn` tinyint NOT NULL,
  `platform_idplatform` tinyint NOT NULL,
  `minuteValue` tinyint NOT NULL,
  `zone_idzone` tinyint NOT NULL,
  `vehicle_plate` tinyint NOT NULL,
  `idsys_user` tinyint NOT NULL,
  `pay_id` tinyint NOT NULL,
  `fee` tinyint NOT NULL,
  `minuts` tinyint NOT NULL,
  `minutsRound` tinyint NOT NULL,
  `totalParking` tinyint NOT NULL,
  `place_idplace` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `paypal_transaction`
--

DROP TABLE IF EXISTS `paypal_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_transaction` (
  `id_transaction` varchar(20) NOT NULL,
  `date_transaction` datetime NOT NULL,
  `id_sys_user` bigint(20) NOT NULL,
  `email_sys_user` varchar(30) DEFAULT NULL,
  `state_transaction` varchar(15) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_transaction`
--

LOCK TABLES `paypal_transaction` WRITE;
/*!40000 ALTER TABLE `paypal_transaction` DISABLE KEYS */;
INSERT INTO `paypal_transaction` VALUES ('1099796840448','2014-01-16 09:41:47',881,'city1@mail.com','Rejected',9999.99,'CO'),('1100065275904','2014-01-17 11:59:04',881,'city1@mail.com','Rejected',9999.99,'CO'),('1101717831680','2014-01-16 09:42:25',881,'city1@mail.com','Completed',9999.99,'CO'),('1103823372288','2013-12-30 17:31:04',885,'anubis_ex@hotmail.com','Rejected',9999.99,'CO'),('1103843295232','2013-12-19 11:30:33',888,'andres.pereirap@gmail.com','Rejected',9999.99,'CO'),('1106013061120','2014-01-16 10:14:14',881,'city1@mail.com','Rejected',9999.99,'CO'),('1106147082240','2014-01-16 10:00:53',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO'),('1108110082048','2014-01-16 10:10:23',881,'city1@mail.com','Rejected',9999.99,'CO'),('1108126728192','2013-12-18 16:05:21',888,'andres.pereirap@gmail.com','Rejected',9999.99,'CO'),('1108253868032','2014-01-31 11:16:13',881,'gmail1@gmail.com','Rejected',9999.99,'CO'),('1110520102912','2014-01-28 20:43:46',881,'city1@mail.com','Rejected',9999.99,'CO'),('1112398692352','2013-12-30 15:30:15',885,'anubis_ex@hotmail.com','Completed',9999.99,'CO'),('1113473482752','2014-01-28 20:53:24',881,'city1@mail.com','Rejected',9999.99,'CO'),('12953780224','2014-01-16 12:31:18',881,'city1@mail.com','Rejected',9999.99,'CO'),('1374389534720','2014-01-30 17:49:47',881,'aljurado@gmail.com','Completed',5000.00,'CO'),('1374418894848','2014-02-06 10:19:48',881,'testmail@gmail.com','Rejected',9999.99,'CO'),('1378684502016','2013-12-19 12:09:47',888,'andres.pereirap@gmail.com','Completed',9999.99,'CO'),('1378726445056','2013-12-19 12:22:47',888,'andres.pereirap@gmail.com','Rejected',9999.99,'CO'),('1383069253632','2013-12-18 15:51:48',885,'anubis_ex@hotmail.com','Completed',200.00,'CO'),('1383174504448','2013-12-19 12:28:40',888,'andres.pereirap@gmail.com','Completed',9999.99,'CO'),('1384069988352','2013-12-30 16:39:55',885,'anubis_ex@hotmail.com','Completed',9999.99,'CO'),('1385213067264','2014-01-27 15:44:58',881,'city1@mail.com','Completed',9999.99,'CO'),('1385684795392','2014-01-30 17:43:56',881,'city1@mail.com','Completed',9999.99,'CO'),('1387316445184','2013-12-19 12:07:19',888,'andres.pereirap@gmail.com','Rejected',9999.99,'CO'),('1387430739968','2013-12-19 12:07:18',888,'andres.pereirap@gmail.com','Rejected',9999.99,'CO'),('14026604544','2013-12-30 10:46:36',885,'anubis_ex@hotmail.com','Completed',2000.00,'CO'),('15502213120','2014-01-16 12:31:57',881,'city1@mail.com','Completed',9999.99,'CO'),('268435456','2014-01-20 09:31:22',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO'),('277365391360','2014-01-27 10:15:13',881,'city1@mail.com','Rejected',9999.99,'CO'),('278541631488','2014-01-28 17:07:58',881,'city1@mail.com','Rejected',9999.99,'CO'),('279239983104','2013-12-18 10:09:42',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO'),('279278780416','2013-12-18 11:02:17',890,'gerenciadesarrollo@city-parkin','Rejected',200.00,'CO'),('279621992448','2014-01-28 17:08:05',881,'city1@mail.com','Rejected',9999.99,'CO'),('280559091712','2014-01-28 17:08:28',881,'city1@mail.com','Rejected',9999.99,'CO'),('283539144704','2013-12-18 10:10:52',881,'city1@mail.com','Rejected',9999.99,'CO'),('283539275776','2013-12-18 10:10:53',881,'city1@mail.com','Rejected',9999.99,'CO'),('284009955328','2014-01-31 07:55:26',881,'aljurado@gmail.com','Rejected',9999.99,'CO'),('284042592256','2014-01-08 09:43:15',891,'n_a_n_o84@hotmail.com','Completed',9999.99,'CO'),('284140371968','2014-01-31 07:55:27',881,'aljurado@gmail.com','Rejected',9999.99,'CO'),('284277473280','2014-01-20 10:26:05',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO'),('284541714432','2014-01-08 09:38:30',891,'n_a_n_o84@hotmail.com','Completed',9999.99,'CO'),('284847898624','2014-01-08 10:22:47',885,'anubis_ex@hotmail.com','Rejected',200.00,'CO'),('285750329344','2014-01-31 07:55:51',881,'aljurado@gmail.com','Completed',9999.99,'CO'),('287763398656','2013-12-18 10:20:54',888,'andres.pereirap@gmail.com','Rejected',9999.99,'CO'),('288842579968','2014-01-31 07:56:35',881,'aljurado@gmail.com','Rejected',9999.99,'CO'),('290448408576','2014-01-20 10:45:10',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO'),('3289382912','2014-01-30 11:56:48',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO'),('5368774656','2013-12-30 10:31:08',885,'anubis_ex@hotmail.com','Rejected',200.00,'CO'),('5378146304','2014-01-23 14:30:46',881,'city1@mail.com','Completed',9999.99,'CO');
/*!40000 ALTER TABLE `paypal_transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `updateBalanceUser` AFTER INSERT ON `paypal_transaction` FOR EACH ROW BEGIN
IF NEW.state_transaction = 'Completed' THEN
   update sys_user set balance = balance +  NEW.amount where idsys_user = NEW.id_sys_user;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `idplace` varchar(80) NOT NULL,
  `idzone` varchar(30) NOT NULL,
  PRIMARY KEY (`idplace`),
  KEY `FK_place_zone` (`idzone`),
  CONSTRAINT `FK_place_zone` FOREIGN KEY (`idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place`
--

LOCK TABLES `place` WRITE;
/*!40000 ALTER TABLE `place` DISABLE KEYS */;
/*!40000 ALTER TABLE `place` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `generatePlaceId` BEFORE INSERT ON `place` FOR EACH ROW BEGIN
DECLARE paisPrefix VARCHAR(3);
SELECT c.countryPrefix INTO paisPrefix
       FROM country c
       INNER JOIN client cl ON(c.idcountry = cl.country_idcountry) 
       INNER JOIN zone z ON(z.client_idclient = cl.idclient)
       WHERE z.idzone = NEW.idzone;
SET NEW.idplace = CONCAT(paisPrefix,NEW.idplace);
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `place_subzone`
--

DROP TABLE IF EXISTS `place_subzone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place_subzone` (
  `idplace` varchar(80) NOT NULL,
  `idsubzone` bigint(20) NOT NULL,
  PRIMARY KEY (`idplace`),
  KEY `fk_place_idx` (`idplace`),
  KEY `fk_subzone_idx` (`idsubzone`),
  KEY `FK_place_subzona_idx` (`idsubzone`),
  KEY `IDX_IDSUBZONE` (`idsubzone`) USING BTREE,
  CONSTRAINT `FK_place_subzona` FOREIGN KEY (`idsubzone`) REFERENCES `subzone` (`idsubzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place_subzone`
--

LOCK TABLES `place_subzone` WRITE;
/*!40000 ALTER TABLE `place_subzone` DISABLE KEYS */;
/*!40000 ALTER TABLE `place_subzone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `idplatform` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`idplatform`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform`
--

LOCK TABLES `platform` WRITE;
/*!40000 ALTER TABLE `platform` DISABLE KEYS */;
INSERT INTO `platform` VALUES (1,'SMS'),(2,'MOBILE'),(3,'WEB');
/*!40000 ALTER TABLE `platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_restrict`
--

DROP TABLE IF EXISTS `price_restrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_restrict` (
  `idtimePriceRestriction` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `zone_idzone` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idtimePriceRestriction`),
  KEY `fk_price_restrict_zone` (`zone_idzone`),
  CONSTRAINT `fk_price_restrict_zone` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_restrict`
--

LOCK TABLES `price_restrict` WRITE;
/*!40000 ALTER TABLE `price_restrict` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_restrict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revision_log`
--

DROP TABLE IF EXISTS `revision_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revision_log` (
  `revision` double NOT NULL,
  `author` varchar(145) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revision_log`
--

LOCK TABLES `revision_log` WRITE;
/*!40000 ALTER TABLE `revision_log` DISABLE KEYS */;
INSERT INTO `revision_log` VALUES (0.303,'juan otero','agregada nueva columna de envio de emails'),(0.379,'juan otero','creada nueva tabla user_transaction'),(0.402,'juan otero','creada nuevas tablas user_transaction campaign_state y campaign, agregado trigger de actualización de porcentaje');
/*!40000 ALTER TABLE `revision_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shared_account`
--

DROP TABLE IF EXISTS `shared_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_account` (
  `idsys_user_acc1` bigint(20) NOT NULL,
  `idsys_user_acc2` bigint(20) NOT NULL,
  `state` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idsys_user_acc1`,`idsys_user_acc2`),
  KEY `fk_shared_account_sys_user` (`idsys_user_acc1`),
  KEY `fk_shared_account_sys_user1` (`idsys_user_acc2`),
  CONSTRAINT `fk_shared_account_sys_user` FOREIGN KEY (`idsys_user_acc1`) REFERENCES `sys_user` (`idsys_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shared_account_sys_user1` FOREIGN KEY (`idsys_user_acc2`) REFERENCES `sys_user` (`idsys_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shared_account`
--

LOCK TABLES `shared_account` WRITE;
/*!40000 ALTER TABLE `shared_account` DISABLE KEYS */;
INSERT INTO `shared_account` VALUES (917,893,0),(1118,1051,0);
/*!40000 ALTER TABLE `shared_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_received`
--

DROP TABLE IF EXISTS `sms_received`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_received` (
  `smsID` bigint(20) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(15) DEFAULT NULL,
  `min` varchar(12) DEFAULT NULL,
  `shortCode` varchar(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `opt` varchar(10) DEFAULT NULL,
  `operator` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`smsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_received`
--

LOCK TABLES `sms_received` WRITE;
/*!40000 ALTER TABLE `sms_received` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_received` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sent`
--

DROP TABLE IF EXISTS `sms_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sent` (
  `smsID` bigint(20) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(15) DEFAULT NULL,
  `min` varchar(12) DEFAULT NULL,
  `shortCode` varchar(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `opt` varchar(10) DEFAULT NULL,
  `operator` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`smsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sent`
--

LOCK TABLES `sms_sent` WRITE;
/*!40000 ALTER TABLE `sms_sent` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_networks`
--

DROP TABLE IF EXISTS `social_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_networks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_networks`
--

LOCK TABLES `social_networks` WRITE;
/*!40000 ALTER TABLE `social_networks` DISABLE KEYS */;
INSERT INTO `social_networks` VALUES (1,'fbm','http://m.facebook.com/pages/Canadian-Parking-Systems-Technology-Inc/105167772946251?sk=wall','Pagina social de CPS en facebook'),(2,'twm','http://twitter.com/cps_parking','Cuenta CPS de twiter');
/*!40000 ALTER TABLE `social_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subzone`
--

DROP TABLE IF EXISTS `subzone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subzone` (
  `idsubzone` bigint(20) NOT NULL AUTO_INCREMENT,
  `idzone` varchar(30) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`idsubzone`),
  UNIQUE KEY `idsubzone_UNIQUE` (`idsubzone`),
  KEY `fk_zona_idx` (`idzone`),
  KEY `FK_zone_idzone_idx` (`idzone`),
  CONSTRAINT `FK_zone_idzone` FOREIGN KEY (`idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subzone`
--

LOCK TABLES `subzone` WRITE;
/*!40000 ALTER TABLE `subzone` DISABLE KEYS */;
/*!40000 ALTER TABLE `subzone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggestion`
--

DROP TABLE IF EXISTS `suggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suggestion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `description` longtext NOT NULL,
  `sys_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggestion`
--

LOCK TABLES `suggestion` WRITE;
/*!40000 ALTER TABLE `suggestion` DISABLE KEYS */;
INSERT INTO `suggestion` VALUES (55,'2014-06-06 08:38:57','Creo que la idea es muy buena pero seria importante que city parking logre abrir mas espacios en BogotÃ¡. Para asÃ­ tener mas posibilidades de parqueo',974),(57,'2014-06-11 10:56:47','Debería exisrir una parte para consultar tarifas',1034),(58,'2014-06-11 12:26:37','Excelente aplicacion',1035),(59,'2014-09-09 16:20:36','Tes',1121),(60,'2014-09-16 11:50:16','Esta es una sugerencia desde iphone',1136);
/*!40000 ALTER TABLE `suggestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `idsys_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `idsys_user_type` int(11) DEFAULT NULL,
  `country_idcountry` bigint(20) NOT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `cu` varchar(12) DEFAULT NULL,
  `favoriteMsisdn` varchar(15) DEFAULT NULL,
  `city_idcity` bigint(20) unsigned NOT NULL,
  `dateReg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `origin` varchar(10) DEFAULT NULL,
  `notificationmsj` smallint(6) DEFAULT '0',
  `identification` varchar(20) DEFAULT NULL,
  `passtemp` varchar(255) DEFAULT NULL,
  `email_notification` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idsys_user`),
  UNIQUE KEY `favMsisdnUnique` (`favoriteMsisdn`),
  UNIQUE KEY `login_unique` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_sys_user_sys_user_type` (`idsys_user_type`),
  KEY `fk_sys_user_country` (`country_idcountry`),
  KEY `FK_sys_user_city` (`city_idcity`),
  CONSTRAINT `FK_sys_user_city` FOREIGN KEY (`city_idcity`) REFERENCES `city` (`idcity`),
  CONSTRAINT `fk_sys_user_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sys_user_sys_user_type` FOREIGN KEY (`idsys_user_type`) REFERENCES `sys_user_type` (`idsys_user_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25254 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (4,'test123','a148fb8e309db53830d30211d5ad9d1b57d4ffdc','Juan Carlos','maul','CRA','andres.pereirap@gmail.comasw','2014-04-09',2,472,0.00,'98593','3939',103,'2014-09-06 16:21:25',NULL,0,NULL,NULL,1),(5,'atest','c2d48a1ede77619511dd45f46630d9654fde65ff','calos','andres','cra','mail@testrpurbe.com','2014-09-06',2,472,0.00,'36526','1234543',103,'2014-09-06 16:22:13',NULL,0,NULL,NULL,1),(881,'city1','a36fffdea20502826b16628eeed3119d434e0982','Andres','Pereira Pereira','cll103 #14a-53','apereira@cpsparking.ca','1986-08-03',2,472,900.00,'27499','6210355',103,'2013-11-12 20:38:46',NULL,0,NULL,NULL,0),(883,'financiera','446f7cd076b90d54bf3312a3687a7f076f47c74a','Financiera','City','Bogota','financiera@mail.com','1984-06-02',6,472,NULL,'PARK',NULL,103,'2013-11-13 14:22:17',NULL,0,NULL,NULL,0),(884,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','Admin','City','Bogota','admin@mail.com','1984-06-02',5,472,NULL,'PARK1','34123121',103,'2013-11-13 14:38:21',NULL,0,NULL,NULL,0),(885,'anubis_ex','7c222fb2927d828af22f592134e8932480637c0d','juan','otero castellar','cll 52 52 54','anubis_ex@hotmail.com','1996-11-15',2,472,2004162.00,'62506','3163113240',103,'2013-11-14 18:03:29',NULL,0,NULL,NULL,0),(886,'soporte7','fe8ea1bdce0cee2ff444daa94d25c0c3398badbb','Daimler','Vanegas Berrio','Mall Ventura','soporte7@zonavirtual.com.co','1987-02-06',2,472,25000.00,'97121','3014302402',103,'2013-11-14 19:23:18',NULL,0,NULL,NULL,0),(887,'test4044','7c222fb2927d828af22f592134e8932480637c0d','test4044','test4044','call 45 46','test4044@mail.com','1998-03-12',5,472,838.00,'94777','1223344566',103,'2013-12-17 21:21:08',NULL,0,NULL,NULL,0),(893,'hlopez','90386b11353c423c34dc29362b1e22a3272e56de','Harry','LÃ³pez','Cr 111a no 16 h 02','jharrylopez@gmail.com','1979-01-16',2,472,150.00,'10279','3105511906',103,'2014-02-11 14:58:30',NULL,0,NULL,NULL,0),(908,'CALOMAN','6cb6d3a6a92f3fec8fe22ee1ba890b3cc68bd1d1','CARLOS GERMAN','RAMIREZ LONDONO','CR 51 N. 122 55 APT 403','caloman@hotmail.com','1971-07-17',2,472,24700.00,'61086','3168789072',103,'2014-03-06 16:05:44',NULL,0,NULL,NULL,0),(909,'ebayon','5e15ffd7a65e9aa2cd877abfd2d895733a86c882','Eduardo','Bayon','cll103 #14a-53','ebayon@city-parking.com','1968-10-21',2,472,11250.00,'62716','3164230079',103,'2014-03-06 20:54:06',NULL,0,NULL,NULL,0),(912,'rbtfutura','04130d5c257b0ba5ad9ead0b2da32f09f6ce5c51','oscar','quiroga','cr 71 # 52 - 44','rbtfutura@hotmail.com','1982-02-17',2,472,0.00,'35784','3143701492',103,'2014-03-08 18:01:19',NULL,0,NULL,NULL,0),(913,'soniayolima','28a314b0e7b1e24423812908f6079c46e0d3b14a','sonia','parra','cll 103 14a-53','soniayolima@gmail.com','1985-09-23',2,472,2050.00,'33938','3112011487',103,'2014-03-10 00:44:30',NULL,0,NULL,NULL,0),(917,'aljurado','711383a59fda05336fd2ccf70c8059d1523eb41a','Ana LucÃ­a','Jurado','cll149#53-45 ','aljurado@gmail.com','1981-08-14',2,472,4750.00,'78513','3116052106',103,'2014-04-02 20:00:14',NULL,0,NULL,NULL,0),(918,'dpinho','0ec863c1f081cf0b6126f9942d0cfed790dd6d81','luli','depinho diaz','los olivos mz165 lote15 aa hh san martin','dpinho_7612@hotmail.com','1976-02-08',2,472,0.00,'33567','956677088',103,'2014-04-04 01:04:00',NULL,0,NULL,NULL,0),(919,'anders8877','a8b444be663ae81bf005c7faa8bcc6a172a26108','John Anderson ','Osorio','calle 66  19-22','anders8877@gmail.com','1988-08-20',2,472,0.00,'73323','3124645625',103,'2014-04-04 15:15:53',NULL,0,NULL,NULL,0),(920,'agata22','b1b3773a05c0ed0176787a4f1574ff0075f7521e','damian','hernandez','gxfjgyfj gdjfhg','micorreo@gmail.com','1980-07-05',2,472,0.00,'56083','5263738383',103,'2014-04-06 17:40:36',NULL,0,NULL,NULL,0),(921,'asael2','b1b3773a05c0ed0176787a4f1574ff0075f7521e','asael','arenas','k 100 n 100','asael2@gmail.com','1980-03-01',2,472,0.00,'88921','1234567890',103,'2014-04-06 22:56:38',NULL,0,NULL,NULL,0),(922,'test3','3ebfa301dc59196f18593c45e519287a23297589','test2','test2','cra','test3@mail.com','2002-12-02',2,472,0.00,'06361','444444',103,'2014-04-07 15:34:34',NULL,0,NULL,NULL,0),(927,'jc3000','7c222fb2927d828af22f592134e8932480637c0d','jc','jc','cale 45','jc3000@gmail.com','1969-02-15',2,472,0.00,'60127','1234213432',103,'2014-04-07 20:00:35',NULL,0,NULL,NULL,1),(928,'jc2','7c222fb2927d828af22f592134e8932480637c0d','jc','jc','cc','jc2@mai.com','1992-04-05',2,472,0.00,'65453','8447776',103,'2014-04-07 20:40:57',NULL,0,NULL,NULL,1),(929,'edwin','0fada73eee63a53a2e8fd68d07da28878f436576','edwin','vargas','calle 103 No 14 a 53','operaciones3@city-parking.com','1980-03-06',2,472,8400.00,'43964','3174230074',103,'2014-04-11 20:22:41',NULL,0,NULL,NULL,0),(930,'wilsonv','b770e3ee5112a1a8c1407ee2d96df49321ec582e','wilson ','vargas portilla','clle 103 #103a-25','operaciones5@city-parking.com','1974-09-17',2,472,2550.00,'12496','3174230070',103,'2014-04-11 21:28:07',NULL,0,NULL,NULL,0),(934,'silviablan','af5c3065cd6e443215de0a3af51810ccb27944e2','silvia','blanco','carrera 71#52-44','maxfutura1782@gmail.com','1985-11-10',2,472,3600.00,'65164','3188017826',103,'2014-04-11 22:09:04',NULL,0,NULL,NULL,0),(935,'alejandrob','0a01332440166914a72924b0003030f8fc8fa69f','Alejandro','Bermudez','cra 11 a #191-28 torre 1 apto 802','operaciones2@city-parking.com','1983-12-19',2,472,0.00,'39437','3174230073',103,'2014-04-12 04:23:40',NULL,0,NULL,NULL,0),(936,'oaponte','3c3cca3e3e574434357c744dee3d46c94a30cac1','Oswaldo','Aponte','cll103#14a-53','oswaldoaponte23@gmail.com','1982-04-23',2,472,13250.00,'99619','3168748323',103,'2014-04-14 13:42:08',NULL,0,NULL,NULL,0),(937,'jdiaz','8789bfe9741db45fb0d9652ac3bb04a624b90a1c','Juan','Diaz Perez','Calle 1 No 2-3','jperez@gmail.com','1984-02-04',2,472,0.00,'60632','3812369852',103,'2014-04-14 14:16:12',NULL,0,NULL,NULL,1),(938,'Karmenzo','42e5156847a2f977f6596351776049967f1a50b5','Kevin','SÃ¡nchez','av.38 30-40','karmenzo@hotmail.com','1982-09-26',2,472,0.00,'43211','3118299490',103,'2014-04-14 17:39:11',NULL,0,NULL,NULL,0),(939,'edrojal','b78af6726fe112b61e68fb5e95ed52729e63c863','edwin','rojas alarcon','calle 63b 25-25','edrojal777@gmail.com','1982-05-09',2,472,0.00,'36406','3003012307',103,'2014-04-14 18:30:54',NULL,0,NULL,NULL,1),(940,'milenavw','76020219ccf8a7b08363fdfac44f7d097a97f6df','Milena','Vergara Wilches','Carrera 29 No 40 56 sur','milenavw@hotmail.com','1980-02-11',2,472,7100.00,'97712','3108777269',103,'2014-04-21 20:56:54',NULL,0,NULL,NULL,1),(941,'tavioto','350ceca89c0043e964b6d87dc7d5a8bf92c16a96','octavio','herrera','calle 97 70C-49','octavio.herrera@gmail.com','1981-11-01',2,472,0.00,'47886','3006136887',103,'2014-04-23 07:19:58',NULL,0,NULL,NULL,0),(942,'test55','360fb530a7bde6cab3c7a8004aa7fe9739df85cf','test4','test4','bta','test55@mail.com','1996-07-05',2,472,0.00,'53505','34532567',103,'2014-04-23 21:32:06',NULL,0,NULL,NULL,1),(943,'test555','ded5c87f64e04c1aae9b08ebb74f36af803351c4','test5','test5','cra','test5@mail.com','2014-04-23',2,472,0.00,'16188','737373773',103,'2014-04-23 21:35:24',NULL,0,NULL,NULL,1),(944,'misagars','b3061b7765962465c39c0cd94ed2d88d1966643b','misael','macias corredor','calle 103 14-53','misagars@hotmail.com','1984-10-06',2,472,2200.00,'20371','3186296890',103,'2014-04-29 21:31:35',NULL,0,NULL,NULL,1),(945,'jaas2014','bb0ca3c21c93d649dfe132260fbd53868fb7db25','John Alexander','Amaya Sanchez','Bogota','johnamaya02@gmail.com','1981-03-12',2,472,1350.00,'43373','3185159631',103,'2014-05-01 12:39:08',NULL,0,NULL,NULL,1),(946,'tty','895b317c76b8e504c2fb32dbb4420178f60ce321','saaa','hjgyh','kuj','aaa@a.cl','1996-04-13',2,472,0.00,'55167','300333333',103,'2014-05-02 14:38:22',NULL,0,NULL,NULL,1),(947,'lauraome','1b30089424699a05bd72d49626f8b383fefa9aa6','laura','ome','cll 119a #56a 97','l.ome197@uniandes.edu.co','1992-04-09',2,472,0.00,'41465','3134950053',103,'2014-05-04 18:36:43',NULL,0,NULL,NULL,0),(948,'adri1807','9da33634ea7414a71e2a01680ff05f77b08d5637','Adriana Maria','Guzman Segura','calle 156 # 92-64','adrianag1807@hotmail.com','1974-09-04',2,472,0.00,'29015','3174316800',103,'2014-05-07 13:30:23',NULL,0,NULL,NULL,1),(949,'joalex9226','63b6ad61fd776dfdf6007a60b5418ae9aba0403c','Alexis','Cano','cra 1 no 32-52','joalex9226@yahoo.com','1994-02-06',2,472,0.00,'28216','3007476111',103,'2014-05-08 17:04:06',NULL,0,NULL,NULL,1),(950,'andiaz73','93578b6f52f5f9743c4586d4bde56180c1fa15cd','yimi uriel','diaz peralta','trav 5a este No 21-11 sur','operaciones11@city-parking.com','1974-07-04',2,472,16100.00,'69230','3174230076',103,'2014-05-16 17:01:36',NULL,0,NULL,NULL,1),(952,'city3','e54c8ffa5665fdb629113b5304589c64e37b2645','city3','city3','bta','city3@mail.com','2003-02-02',2,472,9000.00,'59076','2332323',103,'2014-05-22 12:55:44',NULL,0,NULL,NULL,1),(953,'olgapardo ','765bb5d5e39445c7f56b5554b9ba0d431132f818','olga','pardo ','cra 7 no 156_68','olgapardodiaz4@gmail.com','1969-06-27',2,472,0.00,'37932','3102313437',103,'2014-05-22 18:27:57',NULL,0,NULL,NULL,0),(956,'Richarsasuke','93ca835df9b335cc6f3df9acbc0771b18e4d8083','Richard','marquez','clle 75 c# 113 a 53','kiubey17@hotmail.com','1989-07-17',2,472,0.00,'08693','3115806779',103,'2014-05-23 17:47:23',NULL,0,NULL,NULL,1),(957,'david','8fea20de47033745e891e53ecceb021cb094931c','david ricardo','buendia benito-revollo','calle 142 # 13-33','davidricardo04@gmail.com','1984-08-04',2,472,0.00,'57744','3002069082',103,'2014-05-27 14:32:15',NULL,0,NULL,NULL,1),(958,'adrianam','0c4b28598eacf68ae83182e22b3ccf3e9a030c58','ADRIANA ','mateus','cll 57#19a56','operaciones9@city-parking.com','1986-01-08',2,472,0.00,'23514','3174230075',103,'2014-05-27 15:28:53',NULL,0,NULL,NULL,1),(959,'cbrenes','c2877182dbd90b618f2c4c4699f2f70315736bd2','christhian','brenes','san jose','cbrenes@cpsparking.ca','1974-06-11',2,472,0.00,'84780',' 50686487838',103,'2014-05-27 17:38:57',NULL,0,NULL,NULL,0),(960,'alcastillo','18e1426b30cacb9c7502c9852364d95b74b95cfd','Ana','Castillo','cra 21 # 134 -61','analuciacastillo76@gmail.com','1976-12-19',2,472,87350.00,'65239','3102826220',103,'2014-05-29 14:21:10',NULL,0,NULL,NULL,1),(964,'santiago','7cf3ed2bd31750372364718432972a9cf5569304','ricardo','Sarmiento','k 7d 145-30 apt 501','operaciones4@city-parking.com','1974-10-26',2,472,0.00,'22985','3174230072',103,'2014-06-04 20:01:18',NULL,0,NULL,NULL,0),(965,'scabrera','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Steven','Cabrera','235 sherway gardens rd','scabrera@cpsparking.ca','1991-07-15',2,472,100000.00,'52570','4162067471',103,'2014-06-04 21:30:51',NULL,0,NULL,NULL,0),(966,'gerardo','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Gerardo','Cabrera','Sherway','gcabrera@cpsparking.ca','1964-07-23',2,472,0.00,'70123','6477024530',103,'2014-06-04 21:31:32',NULL,0,NULL,NULL,1),(967,'Adriana','8cf23826cc294d2f86c16818dc32dab165a32fc7','Adriana','Molinares','33 Sherway ','adria145@hotmIl.czyui','1964-09-26',2,472,0.00,'10999','6478810718',103,'2014-06-04 21:51:41',NULL,0,NULL,NULL,0),(969,'Wass','00af7f71c7fbfa070e05965c5598bfecfcca7509','Wilmer','Santana','Calle 149 53 45','wilmer.santana@gmail.com','1977-09-16',2,472,0.00,'11206','3006091656',103,'2014-06-05 01:57:05',NULL,0,NULL,NULL,0),(970,'cityparking','f1133678598de883993f289504bba598d289dc99','City Parking','S.A.S.','calle 103 14a 53 of 206','gerenciadesarrollo@city-parking.com','1998-01-01',2,472,170000.00,'83466','3183530879',103,'2014-06-05 14:59:57',NULL,0,NULL,NULL,1),(971,'Guianro7','792d28329de7ed446c01b83f731a071248ffeaf4','Guillermo','Rodriguez','Calle 103 # 14a - 53','ggh@city-parking.com','1965-03-14',2,472,23900.00,'12026','3186126551',103,'2014-06-05 15:01:15',NULL,0,NULL,NULL,0),(972,'alveiro','89c33fd80342c77dcc2ea2f0065b40212f7e64e4','alveiro','rodriguez','chuniza','alveiro1612@hotmail.com','1985-08-16',2,472,0.00,'58299','3115667439',103,'2014-06-05 15:23:50',NULL,0,NULL,NULL,1),(973,'osierra','455cecb5050f540887f603eeb0a956d2e290829b','Oliver','Sierra','Direction','osierrasoporte@gmail.com','1985-02-11',2,472,0.00,'77475','316579',103,'2014-06-06 02:28:15',NULL,0,NULL,NULL,0),(974,'karitofloyd','295725eef3af46a31256a7c07d00f3eca9d9c97f','Carolina','Peralta','calle 63 n 78j - 38','karolinafloyd@gmail.com','1983-11-14',2,472,0.00,'40450','3174589231',103,'2014-06-06 14:25:45',NULL,0,NULL,NULL,1),(975,'Hbuitrago','1b40ee0d2eb3a1d94fb921880fc1718db257c4d2','Hector','Buitrago','47 brookwood dr','hbuitragol@gmail.com','1954-04-09',2,472,0.00,'84801','6479939089',103,'2014-06-06 16:39:07',NULL,0,NULL,NULL,0),(976,'City4','673fa18f78b12ceadc1c4a12da20f6cdf262eb10','Test','4','Bogota','city4@mail.com','2003-02-03',2,472,800.00,'41566','83735252',103,'2014-06-07 11:40:30',NULL,0,NULL,NULL,0),(977,'hop2014','01675056e74e38c4b456526775f104375294002c','hernan','ortiz','ak 45 147 12','hernan7806@yahoo.com','1978-02-06',2,472,0.00,'63848','3153929819',103,'2014-06-10 14:30:57',NULL,0,NULL,NULL,0),(978,'sharly','7c34716f3e28f3430c76d6321147b9e339b91b6d','sharly','heredia','calle 103 no 14a-53','sharly841022@hotmail.com','1984-10-22',2,472,0.00,'74264','3164972576',103,'2014-06-10 20:40:40',NULL,0,NULL,NULL,0),(979,'ivan0143','f383bfacec983a93d6a5e5974b24977add6475f3','ivan','beltran','calle 103 No 14-53','ivan001433@gmail.com','1979-11-23',2,472,0.00,'13491','3174025443',103,'2014-06-10 22:31:29',NULL,0,NULL,NULL,0),(980,'davidmoli','455f62df88b1ff352b1150a2d2f20a89e4fea6c7','David','Molinares','cra 15 #87-26','stevenc_07@hotmail.com','2014-03-06',2,472,0.00,'51575','3173637079',103,'2014-06-11 00:19:10',NULL,0,NULL,NULL,0),(1032,'mary','8cb2237d0679ca88db6464eac60da96345513964','maria antonieta','garcia ducuara','calle 200 n 109 78','mary@hotmail.com','1980-06-01',2,472,0.00,'88975','3214857009',103,'2014-06-11 03:26:52',NULL,0,NULL,NULL,0),(1033,'knauj20','643fec50e79c69bc6bbb7616afd3904acf40867c','juan ','rodriguez','20 de julio','knauj20@gmail.com','1991-12-11',2,472,0.00,'99858','3045230276',103,'2014-06-11 16:15:27',NULL,0,NULL,NULL,0),(1034,'jarsoff','06d0aa0f942ab250a150c5b0847b3e811d78d58a','Julio Roberto','Arenas Rincón','CL 23C 69F 65 BL 33 AP 401','jarsoff@gmail.com','1975-08-18',2,472,0.00,'90374','3005712534',103,'2014-06-11 16:45:08',NULL,0,NULL,NULL,0),(1035,'andresnino','c7fdc520e5575e7673a7deba4806f4c00af6fcf1','andres','niño narvaez','cra 18c # 122 - 09','andresnino@yahoo.com','1976-08-03',2,472,89600.00,'80499','3003590187',103,'2014-06-11 18:14:39',NULL,0,NULL,NULL,0),(1036,'julianacoral','5ed88ab988ead445ad62abc76f57fa97dbbb2763','juliana','coral','cra 18c # 121-09','jcoral98@hotmal.com','1971-10-17',2,472,100000.00,'12701','3164708014',103,'2014-06-11 18:16:09',NULL,0,NULL,NULL,0),(1037,'macastro','95c0198316ea78e5086bef633bbf29753804da39','marlon alejandro',' castro ','carrera 83 número 82 a 30','macastrocani@gmail.com','1979-11-18',2,472,0.00,'99601','3202785747',103,'2014-06-11 22:22:33',NULL,0,NULL,NULL,0),(1038,'leonardoco','4f13c27712a0b7fa9aa86bf63f53cb909477c05f','jorge','hernandez','calle131z25.46','jorge.hernandez@gmail.com','1976-01-11',2,472,0.00,'61708','3002143224',103,'2014-06-11 23:13:06',NULL,0,NULL,NULL,0),(1039,'capc1989','841362213388372e07ac1c1403b7c36170210573','Carlos Andrés ','Peña Cárdenas ','Calle 19A # 120-09','pc.carlos.89@gmail.com','1989-10-11',2,472,0.00,'91517','3176805848',103,'2014-06-12 00:56:06',NULL,0,NULL,NULL,0),(1040,'rickdazam','1e9e67c91cb531ea65b54bdc9b40e02f57276f41','Ricardo Alfonso','Daza Martinez','Calle 127 #51A-45. Torre A. Apto 501.','rickdazam@gmail.com','1980-01-09',2,472,0.00,'51691','3002645833',103,'2014-06-12 00:57:46',NULL,0,NULL,NULL,0),(1041,'Gusmor0607','7afac3c9320ed65eb935aab0f9a891e5e518def3','Gustavo ','Moreno ','Cra 56a # 130a 61 Int 2 Apt 201 ','gusmor@hotmail.com','1972-06-07',2,472,0.00,'98443','3107998875',103,'2014-06-12 01:12:35',NULL,0,NULL,NULL,0),(1042,'marinesluna','30cbfedac62dddbcb25650a402897dd2eb101f86','María Inés',' Luna Carrizosa','Calle 109A 18 50 Apto. 205','marinesluna@hotmail.com','1965-04-23',2,472,0.00,'41094','3212009574',103,'2014-06-12 01:38:55',NULL,0,NULL,NULL,0),(1043,'torradoj','bd60facea6da70344fd9ec95afcbdad50f2a2e29','javier','torrado','trv 4 este # 61 - 05 t1 apt 1301','torradoj@hotmail.com','1975-12-29',2,472,0.00,'36021','3006009156',103,'2014-06-12 02:03:07',NULL,0,NULL,NULL,0),(1044,'julorj','3a8b30d53dc352c661e361980904fbeae2021e5f','Julie','Orjuela','av cll 26 # 68B - 70','jorjuelap@bt.unal.edu.co','1987-06-18',2,472,0.00,'27550','3002046334',103,'2014-06-12 02:08:46',NULL,0,NULL,NULL,0),(1045,'berndsign','09afe8623c37dc1bae66e174736277668e8ea272','bernardo','gomez','cra 60 #79b-46 apt401','koopa_troop@hotmail.com','1983-05-04',2,472,0.00,'77157','3208594987',103,'2014-06-12 03:06:01',NULL,0,NULL,NULL,0),(1046,'aguirres','ddfbc8cd6ef9223f1fd9d57eebc0e994e203a8e4','Santiago ','Aguirre ','Calle 103A # 16-90 ap 202','aguirres1@gmail.com','1980-11-08',2,472,0.00,'34142','3002004531',103,'2014-06-12 03:56:48',NULL,0,NULL,NULL,0),(1047,'garcenho','73b5bc775c1c99f33f45df8956859ce9a0f64aa2','Daniel Felipe Cristobal','García Clavijo','calle 131 #76A-85','danielfelipecristobal@gmail.com','1989-01-24',2,472,0.00,'08215','3159267344',103,'2014-06-12 04:31:45',NULL,0,NULL,NULL,0),(1048,'lachaves','1b4c4352f52e05e0b53239e0a0f7bbf2386d4130','Luis Adolfo ','Chaves Caicedo ','Calle 155 # 9-45 Apto 1212 Torre 1','lachaves@gmail.com','1970-12-26',2,472,0.00,'03098','3133945236',103,'2014-06-12 11:33:50',NULL,0,NULL,NULL,0),(1049,'yestrigos','cfcc3618c540f9477b571fdd04618cad4ad82bc0','Yesenia','Trigos','Cra 7 35 40','yestrigos@gmail.com','1980-02-02',2,472,0.00,'47541','3002086125',103,'2014-06-12 12:13:19',NULL,0,NULL,NULL,0),(1050,'faro13','a83716ee84b36bcb4d635f81052da37186e0ff7c','Felipe','Rugen Ortiz','calle 64 j 72 a 58 apto 201','feliperugenortiz@gmail.com','1983-11-13',2,472,0.00,'82181','3203081041',103,'2014-06-12 16:45:36',NULL,0,NULL,NULL,0),(1051,'apereira','67525f46c6b9f1fb8df61d12bb97548b1fa40bd0','Andres','Pereira','bogota','andres.apereirap@gmail.com12','1984-06-03',2,472,99000.00,'16483','3164976455',103,'2014-06-12 16:58:41',NULL,0,NULL,NULL,0),(1052,'victor','88fa846e5f8aa198848be76e1abdcb7d7a42d292','victor','londono','calle 98 #7a 08','vlondonoa@gmail.com','1947-05-01',2,472,100000.00,'13022','3125278522',103,'2014-06-12 19:54:19',NULL,0,NULL,NULL,0),(1053,'fjmjbog','ea995adaac5478f4eb81b5e90f0307823d4ee434','Francisco Javier','Mora Jaramillo','Calle 98 No. 7A - 08','jvrmora@gmail.com','1954-01-12',2,472,0.00,'30651','3102224197',103,'2014-06-12 20:02:23',NULL,0,NULL,NULL,0),(1062,'Adlc','6bf923730ee2c944ef34c7c775fdd4daa4c9e17b','Andres','De las casas','Calle 100 # 13-21','Andres.delascasas@oracle.com','1977-03-06',2,472,0.00,'91773','3213150959',103,'2014-06-12 20:14:00',NULL,0,NULL,NULL,0),(1063,'sospina','06ce3ba6bbf3d1303c1389a39a57119040d13b94','Santiago','Ospina','Tv 74 #11A - 15','santiago.ospina@gmail.com','1984-06-10',2,472,100000.00,'68979','3006199388',103,'2014-06-12 20:30:15',NULL,0,NULL,NULL,0),(1064,'connygn','9b7c3e5ff349c910dc35bb682a534a5310ae3f8d','constanza','guerrero','clle 116 16 89','constanza.guerrero@hotmail.com','1976-02-12',2,472,0.00,'35954','3115905789',103,'2014-06-12 21:29:06',NULL,0,NULL,NULL,0),(1065,'balaguera','561aaa115f8277b84b8ddf3303b5f3f53f4a5055','jorge alejandro','balaguera lopez','Carrera 12 N 22 - 63 Apt 403','balaguerajorge@yahoo.com','1978-07-06',2,472,0.00,'19358','3143251130',103,'2014-06-12 21:52:10',NULL,0,NULL,NULL,1),(1066,'maurogudi','d5d0ab0e7d67b306b8767a7cc75fe800ae66fdc0','Mauricio',' Gutiérrez Díaz','Calle 126 # 52A - 28','maogd@hotmail.com','1969-11-12',2,472,0.00,'60643','3043773303',103,'2014-06-13 09:19:44',NULL,0,NULL,NULL,0),(1067,'yeison79','d0de7949414fa4c058d0c299d20e97b1d1b5b698','yeison alexander','trujillo diaz','cll6 b 80 g 95','jeisontdiaz@gmail.com','1979-09-04',2,472,0.00,'14548','3016735047',103,'2014-06-13 12:12:34',NULL,0,NULL,NULL,0),(1068,'ndmorame','75e2107ce7e8072f6dd5c8d0b9a9ba723f1ae281','Nelson Dario','Mora Mendoza','Cra 13 # 155 - 51 casa 9','ndmorame@hotmail.com','1973-04-06',2,472,0.00,'62901','3153609944',103,'2014-06-13 13:10:48',NULL,0,NULL,NULL,0),(1069,'joango79','0d8b77a36cc3794017c2eb4d6eebe36a516fa330','Jorge','Angel','calle 166#9-45','joango79@hotmail.com','1979-06-27',2,472,0.00,'37388','3112081250',103,'2014-06-13 14:11:44',NULL,0,NULL,NULL,0),(1070,'carolina','3cb9d28c5ae0853c2f4fe357f3e20ab4acc6f70e','carolina','garcia','transversal 54 # 114-51 apto 402','garciacarito@hotmail.com','1980-04-22',2,472,0.00,'16088','3214907449',103,'2014-06-13 14:36:26',NULL,0,NULL,NULL,0),(1071,'jjgf13','1ef84247fd20effb53c42c28e80ef2d34da9d7c5','Jhonnatan','Galindo','Cra 58 # 125b-96 B3 Apto512','jjgf13@gmail.com','1985-04-05',2,472,0.00,'11741','3008029026',103,'2014-06-13 15:01:51',NULL,0,NULL,NULL,0),(1072,'maocar','664d4e9e161bc176db60d285e3f6cfee3a5cbfa7','mauricio','caro','cll131b ~ 91 a 26','maurocar78.mc@gmail.com','1978-12-31',2,472,0.00,'44888','3204325279',103,'2014-06-13 15:31:12',NULL,0,NULL,NULL,0),(1073,'ricatov','4f39e71860fbae8e0ffd28ce55c6c624f7526b4a','Ricardo','Tovar','calle 146 7-64','ricatov@hotmail.com','1974-05-28',2,472,0.00,'17478','3103244517',103,'2014-06-13 23:28:00',NULL,0,NULL,NULL,0),(1074,'eunice','888795ee1c62a23630f4619061cf4312c457c3ce','Eunice','Ñañez','calle 100 # 8a55','eunicenanezm@gmail.com','1966-03-16',2,472,0.00,'40054','3153481016',103,'2014-06-13 23:47:34',NULL,0,NULL,NULL,0),(1075,'etnarca','5864aa36a2b012367fd6189095de30f5041228c5','carlos','rivas','calle 142 #6-69','etnarca@yahoo.com','1964-03-28',2,472,0.00,'29883','3164814936',103,'2014-06-14 00:37:28',NULL,0,NULL,NULL,0),(1076,'dianin0331','cac8d2618173f475dea11dff4cb78d781b4d74e9','Diana Carolina ','Mancilla Bautista','Cr 101 B 141-35','dianin0331@gmail.com','1984-03-31',2,472,100000.00,'99942','3183833424',103,'2014-06-14 03:18:07',NULL,0,NULL,NULL,0),(1077,'lmlanzam','5b6fd88081867dc28db1fab25e9426e1d484e3d5','laura','lanza','carrera 54 #64a - 45','lmlanzam@gmail.com','1983-04-29',2,472,0.00,'35203','3002006781',103,'2014-06-14 12:33:52',NULL,0,NULL,NULL,0),(1078,'CarlosE.','a480beb6cc0296af6881e8c0a23fbaeba26323ab','Carlos E.','Romero V.','cra. 20 # 122-58','carlos.e.romero@telmex.net.co','1954-04-07',2,472,0.00,'41405','3165219619',103,'2014-06-14 21:27:02',NULL,0,NULL,NULL,0),(1079,'janson79','380875d8ac05dd53771222ccccaf7586e6d03745','yeison','Ramirez','cll82 n 94-61','yandresramirez979@gmail.com','1978-09-11',2,472,0.00,'47660','3132776909',103,'2014-06-14 22:47:49',NULL,0,NULL,NULL,0),(1080,'cgmf89','a8c17c52eb747da790a8d285015ea9fd21ffc40e','Cristian Gerardo ','Maldonado Florez','Carrera 44 # 58c-45 sur','cgmf89@gmail.com','1989-12-01',2,472,0.00,'59502','3017465927',103,'2014-06-15 01:52:06',NULL,0,NULL,NULL,0),(1081,'RosaAmalia','ccac53a604dad151c792645f3236fe958a4a5c66','Rosa Amalia','Daza Lozada','Cr 70 C No. 118 -30','rosamaliadazal@hotmail.com','2014-07-07',2,472,100000.00,'20989','3187076266',103,'2014-06-15 02:51:16',NULL,0,NULL,NULL,1),(1082,'oscar_baez85','b9207aa9074569ea66a7401f408a64164fb72a6c','oscar javier','baez riveros','tv 71 f  5a 54 apt 205','oscar_baez85@yahoo.com','1985-12-05',2,472,0.00,'26484','3158995671',103,'2014-06-15 13:56:19',NULL,0,NULL,NULL,0),(1083,'diana84','615c01ddda9e03629d9c6026cb12ee45c60d9880','diana','castillo','Cra 71#117-29','d.castillo@hillebrandgroup.com','1984-10-18',2,472,0.00,'95096','3164739281',103,'2014-06-15 18:12:21',NULL,0,NULL,NULL,0),(1084,'josesu85','956341e1bee55489a70b0b67b1b78621be2710f7','Jose Ernesto','Rueda Caro','kr 14b 146 33 ','josesu85@gmail.com','1985-06-18',2,472,0.00,'72265','3176607467',103,'2014-06-15 22:13:59',NULL,0,NULL,NULL,0),(1085,'jorst2','a091dcc5d260faa1f0d0df53fa976cd6f6f3ddec','Jorge Andres','Torres','Calle 127 C # 9-45','jorst2@hotmail.com','1980-02-02',2,472,0.00,'23502','3114621219',103,'2014-06-16 01:42:36',NULL,0,NULL,NULL,0),(1086,'fernansala','6f883eabf047abd4997343a3b7ff39fe85dcb719','jimmy','salazar','carrera 6 este n 12-14','fernansala0526@gmail.com','1981-10-22',2,472,0.00,'36547','  321',103,'2014-06-16 12:46:55',NULL,0,NULL,NULL,0),(1091,'helman','a2ad098ace6f8890cac95ea377f9a7c4400d552f','Helman','Collante','Cr. 2a #66 - 52','helmancollante@gmail.com','1991-04-05',2,472,0.00,'55790','3012302917',103,'2014-06-16 17:23:29',NULL,0,NULL,NULL,0),(1095,'Fnavarros','fa9fe1e5968e2d880ac05ce643aa266b3f8fc1e9','Fabio Augusto','Navarro Sabogal','Cra 17 No 106-30 ','fabio_navarro@hotmail.com','1974-05-10',2,472,60000.00,'26869','3153452862',103,'2014-06-16 22:39:53',NULL,0,NULL,NULL,0),(1096,'willy_avila','95a351a4beb74af8c8e9ebe8b129185c8885b810','William','Avila','cra 56 # 153 -84','william.avila@gmail.com','1977-11-08',2,472,0.00,'65185','3005504707',103,'2014-06-16 23:48:56',NULL,0,NULL,NULL,0),(1097,'Dwalls','d241dc7b77766095a489008996c4c16ac3b9e5b3','Diego','Lopez','Cll 144 9-54','diegolopezparedes4@hotmail.com','1976-06-23',2,472,0.00,'28843','3103486670',103,'2014-06-17 01:55:52',NULL,0,NULL,NULL,0),(1098,'elrolo3000','8e7b08e507db2995c310784ff30708132ebc554e','Cristian','Ramirez','Carrera 46 # 22B - 20 oficina 309','cramirez@ravagoamericas.com','1985-05-07',2,472,0.00,'08071','3183592538',103,'2014-06-17 11:08:43',NULL,0,NULL,NULL,0),(1099,'andre5rg','c3560941621c3fad4482b11a0512e32637dd754b','Andres','Roa','Calle 22B 43B 56','roa.efrain@ur.edu.co','1993-11-29',2,472,0.00,'38931','3124358037',103,'2014-06-17 19:02:47',NULL,0,NULL,NULL,0),(1100,'jp1986','e4f88bf4b0c64b69a4393648335f5aa828e322fa','José','Pérez','Cra 56 no 7a-56','jp1986@hotmail.com','1986-06-04',2,472,0.00,'97227','3176436286',103,'2014-06-17 21:26:47',NULL,0,NULL,NULL,0),(1101,'tintin','7c222fb2927d828af22f592134e8932480637c0d','tijtin','tintin','col 56','tintin','1996-05-04',2,472,0.00,'48154','2484664444',103,'2014-06-18 15:58:33',NULL,0,NULL,NULL,1),(1102,'city5','4fbf40f9b601e56665518a821e0dc43bea62fc63','city5','city5','cra','city5','2001-03-03',2,472,0.00,'93885','346491',103,'2014-06-18 16:22:02',NULL,0,NULL,NULL,1),(1103,'city6','4cb2a14c334bbe9e6ae2b951a6f69226050cd5be','city6','city6','cra','as','2003-02-03',2,472,100.00,'64553','9239382',103,'2014-06-18 17:09:21',NULL,0,NULL,NULL,0),(1104,'city7','7f67cf99fb0891027c38e55256383367ec262504','city7','city7','city7','city7@mail.com','2003-01-01',2,472,0.00,'09523','city7',103,'2014-06-18 17:13:20',NULL,0,NULL,NULL,0),(1105,'city10','d9c760e3188fda70aab30552a66587bc06814ed7','city10','city10','city10','city10@mail.com','2014-04-23',2,472,0.00,'55954','98383838383',103,'2014-07-25 19:22:17',NULL,0,NULL,NULL,0),(1111,'cquimbayo','4ff88aaddbd209d8026924c2cc2836b408698823','Camilo','Prueba','Calle siempre viva 123','cquimbayo@cpsparking.ca','2014-08-26',2,472,16096.00,'47707','3178795959',103,'2014-08-25 14:08:59',NULL,0,NULL,NULL,1),(1112,'apereira12','109f4b3c50d7b0df729d299bc6f8e9ef9066971f','Andres','Pereira','cra','andress.pereirap@gmail.com','2014-08-25',2,472,0.00,'53875','31938348',103,'2014-08-25 16:33:32',NULL,0,NULL,NULL,0),(1113,'test12','1d1e15655d36c0bbf9347857a571a61d99879b45','test','maul','cra','n_a_n_o84@hotmail.com','2014-08-28',2,472,0.00,'93749','328937436',103,'2014-08-28 22:02:28',NULL,0,NULL,NULL,1),(1114,'tes25','709cd900908544bb06f326fe9fa19281dd3f534e','test','maul','city11','andres.pereirap@gmail.comsas','2014-08-28',2,472,350.00,'00449','349183',103,'2014-08-28 22:04:50',NULL,0,NULL,NULL,1),(1115,'tintin3','7c222fb2927d828af22f592134e8932480637c0d','tintin','tin','call 45 46','tintin3@xmail.com','1985-09-18',2,472,0.00,'51776','33421111111',103,'2014-09-06 16:33:58',NULL,0,NULL,NULL,0),(1116,'atest12','68d63fe28929bea343782fc1b9b74eddf3ad1791','calos','andres','cra','mai12l@testrpurbe.com','2014-09-06',2,472,0.00,'80023','123454312',103,'2014-09-06 16:36:03',NULL,0,NULL,NULL,1),(1117,'carlosmail','6e19dc421d93175dbafc9a4db1e170531ee2ed03','Carlos','Suarez','Cra','carlostest@mail.com','2014-09-01',2,472,0.00,'64032','393823',103,'2014-09-06 16:57:25',NULL,0,NULL,NULL,1),(1118,'apereira10','bc3deabb56fd60fb70e591a4764f41475f5927a7','Andres','Pereira','cra 100 12 12','andres.pereirap@gmail.com988','2014-09-02',2,472,690.00,'89325','78982625',103,'2014-09-06 17:05:59',NULL,0,NULL,NULL,1),(1119,'Lsuarez','a00708d763dca61f92a5b911c65b6321785c6f56','Luís','Suarez','Cra','lsuarez2@mail.com','2014-09-08',2,472,10.00,'39327','82727626',103,'2014-09-08 15:05:27',NULL,0,NULL,NULL,1),(1121,'test14','4d39836f0f1d34f2984e221fdd670dc84d0b7ccf','andrws perez','pereipereira syares','cra. ','andres.pereirap@gmail.com34','2014-09-09',2,472,1000.00,'89663','54848425',103,'2014-09-09 20:13:19',NULL,0,NULL,NULL,1),(1122,'camilocps','4ff88aaddbd209d8026924c2cc2836b408698823','camilo','test','Calle siempre viva 123','321media@gmail.com','2014-09-10',2,472,0.00,'68035','3162582836',103,'2014-09-10 23:46:29',NULL,0,NULL,NULL,1),(1123,'333333333333','4ff88aaddbd209d8026924c2cc2836b408698823','camilo','test23333333333333333333333333','Calle siempre viva 124','camiloquimbayo@gmx.com','2014-09-11',2,472,0.00,'52974','3178895940',103,'2014-09-11 21:39:16',NULL,0,NULL,NULL,1),(1124,'tilin','7c222fb2927d828af22f592134e8932480637c0d','tilin','tilin','y 78','tilin@xmail.com','1974-09-11',2,472,0.00,'15365','586655555',103,'2014-09-11 22:49:54',NULL,0,NULL,NULL,0),(1126,'camiloq_2','4ff88aaddbd209d8026924c2cc2836b408698823','camilo','test2','calle siempre viva 123','xrockartex@gmx.com','2014-09-13',2,472,0.00,'05301','3178503478',103,'2014-09-13 05:01:00',NULL,0,NULL,NULL,1),(1128,'cquimbayo2','4ff88aaddbd209d8026924c2cc2836b408698823','camilo','test3','calle siempre viva 123','camilo.quimbayo@c3visual.com','2014-09-13',2,472,3004.00,'71279','4567890980',103,'2014-09-13 05:21:13',NULL,0,NULL,NULL,1),(1129,'test15','1a34d5a0c97babaa6e6ed96ee5ffaf041c99a71d','luis','test','cra','test15@gmail.com','2014-09-13',2,472,0.00,'04609','3152583625',103,'2014-09-13 13:40:34',NULL,0,NULL,NULL,1),(1134,'city123','bd548103787a11e544f511a4de1ff60e0eed2913','Luis carlos galán suare','perez suarwsbde alborboa ','colé 106 no idjdn jdjdjdjd bb ','email@mail.comqwe','2014-09-15',2,472,0.00,'18907','3164976254',103,'2014-09-16 03:47:48',NULL,0,NULL,NULL,1),(1135,'apereira1','aae918e77712721f9290d2355e5bd8d53f8d873a','Andrés giovanni','pereira pereiea','cora 14 no 15 tt','andres.pereirap@gmail.com2','2014-09-14',2,472,50.00,'93678','3152584758',103,'2014-09-16 04:26:16',NULL,0,NULL,NULL,1),(1136,'Apereira2','9804031f9287d413437277b836f06c89b61451af','Andres Giovanniggggggggggggggg','Pereira Pereiraggggggggggggggg','Cra 14 numero áv','andres.pereirap@gmail.com','1984-06-16',2,472,0.00,'63643','3158547252',103,'2014-09-16 15:49:19',NULL,0,NULL,NULL,1),(1137,'cquimbayo3','4ff88aaddbd209d8026924c2cc2836b408698823','camilo','test2','Calle siempre viva 123','cquimbayo@gmx.com','2014-09-16',2,472,0.00,'10475','3002582838',103,'2014-09-16 19:51:49',NULL,0,NULL,NULL,1),(1138,'testjmeter','7c222fb2927d828af22f592134e8932480637c0d','test','jmeter','jm 11','jmeter@xmail.com','1977-09-14',2,472,0.00,'63659','34124444444',103,'2014-09-25 19:49:35',NULL,0,NULL,NULL,0),(14057,'csd 345','4ff88aaddbd209d8026924c2cc2836b408698823','camilo','test2','Calle siempre viva 123','cquimbayo4@cpsparking.ca','2014-10-06',2,472,0.00,'45551','3006589076',103,'2014-10-06 16:05:57',NULL,0,NULL,NULL,1);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `sysuser_hashpasswd` BEFORE INSERT ON `sys_user` FOR EACH ROW begin 
if NEW.origin ='MSJ' then
set NEW.pass = NEW.pass;
else
set NEW.pass = SHA1(NEW.pass);
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `registerPhoneTrigger` AFTER INSERT ON `sys_user` FOR EACH ROW BEGIN
INSERT INTO msisdn_user values(null,NEW.favoriteMsisdn,NEW.idsys_user);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `updatePhoneTrigger` AFTER UPDATE ON `sys_user` FOR EACH ROW BEGIN
if NEW.favoriteMsisdn != OLD.favoriteMsisdn then
  
   UPDATE msisdn_user SET msisdn = NEW.favoriteMsisdn WHERE idsys_user = NEW.idsys_user AND msisdn = OLD.favoriteMsisdn;
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sys_user_client`
--

DROP TABLE IF EXISTS `sys_user_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_client` (
  `idsys_user` bigint(20) NOT NULL,
  `idclient` bigint(20) NOT NULL,
  PRIMARY KEY (`idsys_user`,`idclient`),
  KEY `FK_sys_user_client` (`idclient`),
  CONSTRAINT `FK_sys_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`),
  CONSTRAINT `FK_sys_user_client` FOREIGN KEY (`idclient`) REFERENCES `client` (`idclient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_client`
--

LOCK TABLES `sys_user_client` WRITE;
/*!40000 ALTER TABLE `sys_user_client` DISABLE KEYS */;
INSERT INTO `sys_user_client` VALUES (884,3);
/*!40000 ALTER TABLE `sys_user_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_type`
--

DROP TABLE IF EXISTS `sys_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_type` (
  `idsys_user_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idsys_user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_type`
--

LOCK TABLES `sys_user_type` WRITE;
/*!40000 ALTER TABLE `sys_user_type` DISABLE KEYS */;
INSERT INTO `sys_user_type` VALUES (1,'ADMIN'),(2,'USER'),(3,'TEMP_USER'),(4,'PARK_ADMIN'),(5,'CTRL_VIAL'),(6,'TESORERIA'),(7,'SETEX');
/*!40000 ALTER TABLE `sys_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_vehicle`
--

DROP TABLE IF EXISTS `sys_user_vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_vehicle` (
  `idsys_user_vehicle` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `owner` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idsys_user_vehicle`),
  KEY `FK_sys_user_vehicle_plate` (`vehicle_plate`),
  KEY `FK_sys_user_vehicle_user` (`idsys_user`),
  CONSTRAINT `FK_sys_user_vehicle_plate` FOREIGN KEY (`vehicle_plate`) REFERENCES `vehicle` (`plate`)
) ENGINE=InnoDB AUTO_INCREMENT=347 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_vehicle`
--

LOCK TABLES `sys_user_vehicle` WRITE;
/*!40000 ALTER TABLE `sys_user_vehicle` DISABLE KEYS */;
INSERT INTO `sys_user_vehicle` VALUES (220,'CITY1',881,1),(221,'CITY2',882,1),(222,'CITY2',881,0),(223,'KJM860',885,1),(224,'ZVP999',886,1),(226,'test4044',887,1),(227,'AP55',888,1),(229,'BWE-345',890,1),(234,'AP23',894,1),(235,'CYM537',908,1),(236,'AP2',915,1),(237,'RDY064',917,1),(239,'NDW726',935,1),(240,'QWE-896',937,1),(241,'mrz326',939,1),(242,'MKT880',940,1),(247,'YLA05A',945,1),(248,'xxxxxx',949,1),(249,'BKT30',929,1),(251,'BKK879',950,1),(253,'aaa567',958,1),(254,'CBS123',959,1),(255,'cbs9876',959,1),(258,'469ABC',966,1),(260,'469ACD',966,1),(262,'KXS88D',972,1),(265,'4321',976,1),(268,'wza300',980,1),(269,'city10',1023,1),(270,'city11',1024,1),(271,'SOS123',1032,1),(272,'RAL318',1034,1),(273,'BAK370',1035,1),(274,'BFK909',1035,1),(275,'MAZ393',1038,1),(276,'BZQ42',1039,1),(277,'BHG337',1039,1),(278,'DCH423',1040,1),(279,'KLQ207',1041,1),(280,'BWD743',1042,1),(281,'UUF24C',1045,1),(282,'MBT884',1046,1),(283,'REV529',1048,1),(284,'MNP659',1049,1),(285,'HVK315',1050,1),(286,'Czg208',1062,1),(287,'BOL818',1065,1),(288,'MBS183',1071,1),(289,'RLU518',1075,1),(290,'MJY883',1076,1),(291,'NBS513',1078,1),(292,'MKO328',1078,1),(293,'AWG03D',1080,1),(294,'CFN020',1081,1),(295,'GGR87B',979,1),(296,'BFS959',979,1),(297,'RMY458',1084,1),(298,'MCL520',1084,1),(299,'RLW215',1084,1),(300,'BIG923',1084,1),(301,'BSW162',1052,1),(302,'DCB562',1051,1),(303,'CCY447',948,1),(305,'AP22',1112,1),(306,'AP83',1113,1),(307,'AP831',1114,1),(308,'tintin3',1115,1),(309,'ap123',1116,1),(310,'AP292',1117,1),(313,'KJM456',885,1),(315,'TEST123',1118,1),(316,'TEST124',1118,1),(338,'SQL-234',1111,1),(346,'DFG345',1111,1);
/*!40000 ALTER TABLE `sys_user_vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_restrict`
--

DROP TABLE IF EXISTS `time_restrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_restrict` (
  `idtime_restrict` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `maxMinutes` int(11) DEFAULT NULL,
  `zone_idzone` varchar(10) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtime_restrict`),
  KEY `fk_time_restrict_zone` (`zone_idzone`),
  CONSTRAINT `fk_time_restrict_zone` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_restrict`
--

LOCK TABLES `time_restrict` WRITE;
/*!40000 ALTER TABLE `time_restrict` DISABLE KEYS */;
/*!40000 ALTER TABLE `time_restrict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tower`
--

DROP TABLE IF EXISTS `tower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tower` (
  `towerId` varchar(255) NOT NULL,
  `idZone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`towerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tower`
--

LOCK TABLES `tower` WRITE;
/*!40000 ALTER TABLE `tower` DISABLE KEYS */;
INSERT INTO `tower` VALUES ('fba7a8dbe02e11f7459533923dfdb7cd22843ec1',NULL);
/*!40000 ALTER TABLE `tower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_transaction`
--

DROP TABLE IF EXISTS `user_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_transaction` (
  `idsysuser_origen` bigint(20) NOT NULL,
  `idsysuser_dest` bigint(20) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`idsysuser_origen`,`idsysuser_dest`,`transaction_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_transaction`
--

LOCK TABLES `user_transaction` WRITE;
/*!40000 ALTER TABLE `user_transaction` DISABLE KEYS */;
INSERT INTO `user_transaction` VALUES (885,887,'2014-06-20 10:01:52',250),(885,887,'2014-06-20 10:41:37',388),(1051,1103,'2014-07-01 17:59:04',1000),(1103,881,'2014-07-01 18:08:21',900),(1111,1128,'2014-10-06 13:26:58',2),(1111,1128,'2014-10-06 13:29:51',1000),(1111,1128,'2014-10-06 13:49:56',2000),(1111,1128,'2014-10-06 20:01:17',2),(1118,1119,'2014-09-08 11:11:00',10),(1135,1118,'2014-09-16 00:52:33',100),(1136,1135,'2014-09-16 11:56:42',50);
/*!40000 ALTER TABLE `user_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle` (
  `plate` varchar(12) NOT NULL,
  `state` int(10) unsigned NOT NULL DEFAULT '1',
  `dateReg` datetime NOT NULL,
  PRIMARY KEY (`plate`),
  UNIQUE KEY `plateUnique` (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle`
--

LOCK TABLES `vehicle` WRITE;
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` VALUES ('4321',1,'2014-06-07 05:50:54'),('469ABC',1,'2014-06-04 15:32:49'),('469ACD',1,'2014-06-04 15:43:18'),('aaa567',1,'2014-05-27 09:28:53'),('ap123',1,'2014-09-06 12:36:03'),('AP2',1,'2014-04-01 07:10:11'),('AP22',1,'2014-08-25 12:33:32'),('AP23',1,'2014-02-11 17:09:36'),('AP292',1,'2014-09-06 12:57:25'),('AP55',1,'2013-12-17 16:37:30'),('AP83',1,'2014-08-28 18:02:28'),('AP831',1,'2014-08-28 18:04:50'),('AWG03D',1,'2014-06-14 19:53:18'),('BAK370',1,'2014-06-11 12:21:20'),('BFK909',1,'2014-06-11 12:21:48'),('BFS959',1,'2014-06-15 08:33:42'),('BHG337',1,'2014-06-11 18:56:53'),('BIG923',1,'2014-06-15 18:51:57'),('BKK879',1,'2014-05-16 11:01:36'),('BKT30',1,'2014-05-08 18:34:49'),('BOL818',1,'2014-06-12 15:52:10'),('BSW162',1,'2014-06-16 15:01:43'),('BWD743',1,'2014-06-11 19:40:22'),('BWE-345',1,'2013-12-18 09:45:03'),('BZQ42',1,'2014-06-11 18:56:36'),('CBS123',1,'2014-05-27 11:44:27'),('cbs9876',1,'2014-05-28 16:08:52'),('CCY447',1,'2014-06-17 05:25:02'),('CFN020',1,'2014-06-14 20:51:16'),('CITY1',1,'2013-11-12 22:25:29'),('city10',1,'2014-06-10 20:57:32'),('city11',1,'2014-06-10 21:00:28'),('CITY2',1,'2013-11-12 22:26:50'),('CYM537',1,'2014-03-06 10:29:23'),('Czg208',1,'2014-06-12 14:14:00'),('DCB562',1,'2014-06-16 15:02:48'),('DCH423',1,'2014-06-11 18:58:48'),('DFG345',1,'2014-10-21 18:25:19'),('GGR87B',1,'2014-06-15 08:33:12'),('HVK315',1,'2014-06-12 10:46:48'),('KJM456',1,'2014-09-06 13:30:46'),('KJM860',1,'2013-11-14 13:03:29'),('KLQ207',1,'2014-06-11 19:15:12'),('KXS88D',1,'2014-06-05 09:30:49'),('MAZ393',1,'2014-06-11 17:13:06'),('MBS183',1,'2014-06-13 09:02:29'),('MBT884',1,'2014-06-11 22:31:14'),('MCL520',1,'2014-06-15 18:51:12'),('MJY883',1,'2014-06-13 21:19:59'),('MKO328',1,'2014-06-14 15:28:30'),('MKT880',1,'2014-04-21 14:56:54'),('MNP659',1,'2014-06-12 06:15:09'),('mrz326',1,'2014-04-14 12:30:54'),('NBS513',1,'2014-06-14 15:28:09'),('NDW726',1,'2014-04-11 22:28:18'),('QWE-896',1,'2014-04-14 08:16:12'),('RAL318',1,'2014-06-11 10:47:44'),('RDY064',1,'2014-04-02 22:06:34'),('REV529',1,'2014-06-12 05:38:14'),('RLU518',1,'2014-06-13 18:49:07'),('RLW215',1,'2014-06-15 18:51:31'),('RMY458',1,'2014-06-15 18:50:41'),('SOS123',1,'2014-06-10 21:29:33'),('SQL-234',1,'2014-09-15 21:25:24'),('TEST123',1,'2014-09-08 10:38:19'),('TEST124',1,'2014-09-08 10:38:37'),('test4044',1,'2013-12-17 16:21:08'),('tintin3',1,'2014-09-06 12:33:58'),('UUF24C',1,'2014-06-11 21:06:35'),('wza300',1,'2014-06-10 18:19:10'),('xxxxxx',1,'2014-05-08 11:04:06'),('YLA05A',1,'2014-05-05 04:40:00'),('ZVP999',1,'2013-11-14 14:23:18');
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `idzone` varchar(30) NOT NULL,
  `name` varchar(45) NOT NULL,
  `client_idclient` bigint(20) DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `openTime` time DEFAULT NULL,
  `closeTime` time DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `gmt` varchar(200) DEFAULT 'America/Toronto',
  `url` varchar(512) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `schedule` text,
  `online` smallint(6) DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idzone`),
  KEY `fk_zone_client` (`client_idclient`),
  KEY `nameIndex` (`name`),
  CONSTRAINT `fk_zone_client` FOREIGN KEY (`client_idclient`) REFERENCES `client` (`idclient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone`
--

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `zone` DISABLE KEYS */;
INSERT INTO `zone` VALUES ('COL10','CL 97 #15 -21',3,89.00,'06:00:00','23:00:00',4.6821821,-74.049151,'America/Colombia','http://200.119.7.194:70/wsPagoXCelular','calle 97 No 15-21','De Lunes de 6:00am. A 10:00pm. Martes a Sábado de 6:00am. A 11:00pm. Domingos y Festivos de 11:00am A 6:00pm.',1,'calle97',1),('COL100','CR 15 A # 124 -44',3,89.00,'06:00:00','23:00:00',4.7026963,-74.0436682,'America/Colombia','http://201.245.114.30:70/wsPagoXCelular','Cra 15 A No. 124-40','LUNES A SABADO 6 - 22',1,'calle124',1),('COL101','CL 59A BIS NO. 5-53 LINK 7-60',3,89.00,'06:00:00','23:00:00',4.644907,-74.061064,'America/Colombia','http://201.245.63.50:70/wsPagoXCelular','Calle 59 A Bis 5-53','LUNES A VIERNES 6 - 22, SABADO 8 - 18',1,'link760',1),('COL102','CL 116 # 15-80',3,89.00,'06:00:00','23:00:00',4.6969234,-74.0442163,'America/Colombia','http://200.93.136.194:70/wsPagoXCelular','Cale 116 No 15-80','LUNES A JUEVES 6 - 22, VIERNES Y SABADO 7 - 1, DOMINGO 14 - 18',1,'calle116',1),('COL103','(AV68)',3,89.00,'06:00:00','23:00:00',4.687276,-74.073441,'America/Colombia','http://65.167.49.38:70/wsPagoXCelular','Calle 94 A No. 67 A 74','LUNES A VIERNES 6 - 23, SABADO Y DOMINGO 6:30 - 24',1,'mall68',1),('COL105','TC1',3,89.00,'06:00:00','23:00:00',4.693736,-74.138748,'America/Colombia','http://190.24.242.6:70/wsPagoXCelular','Calle 26 No.106 - 39 TERMINAL DE CARGA','24 HORAS',0,'tc1',1),('COL108','SURTIFRUVER 76',3,89.00,'06:00:00','23:00:00',4.661112,-74.0564589,'America/Colombia','http://201.245.104.194:70/wsPagoXCelular','Calle 76 No. 11 15','De Lunes a Domingo de 6:00am a 10:00pm.',1,'surtifruver76',1),('COL109','CANDELARIA',3,89.00,'06:00:00','23:00:00',4.598781,-74.07045,'America/Colombia','http://190.24.249.71:70/wsPagoXCelular','Calle 12 C 3 -32','LUNES A JUEVES 5:30 - 22, VIERNES Y SABADOS 5:30 - 24',1,'candelaria',1),('COL11','CREPES',3,89.00,'06:00:00','23:00:00',4.6576878,-74.0559868,'America/Colombia','http://201.244.178.18:70/wsPagoXCelular','calle 74 No 9-21','LUNES A SABADO 6 - 23, DOMINGO 11 - 21',1,'crepes',1),('COL114','CASTELLANA',3,89.00,'06:00:00','23:00:00',4.6831482,-74.0587158,'America/Colombia','http://201.245.152.74:70/wsPagoXCelular','Cra. 45 A No. 94-66','LUNES A SABADO 6 - 23, DOMINGO 6 - 22',1,'castellana',1),('COL115','CRA 7 NO. 117 - 14',3,89.00,'06:00:00','23:00:00',4.694879,-74.032451,'America/Colombia','http://190.26.247.95:70/wsPagoXCelular','Cra 7 No. 117 - 44','DOMINGO A MARTES 6 - 22, MIERCOLES A SABADO 24 HORAS',1,'calle117',1),('COL116','TORRE ZIMMA',3,89.00,'06:00:00','23:00:00',4.674343,-74.052876,'America/Colombia','http://190.25.181.160:70/wsPagoXCelular','Cra 15 No. 88 - 64','LUNES A VIERNES 6 - 21:30, SABADO 6 - 17',1,'torrezimma',1),('COL118','SAN JOSE',3,89.00,'06:00:00','23:00:00',4.60556,-74.089865,'America/Colombia','http://201.244.73.202:70/wsPagoXCelular','CR 22 #9- 35 SAN JOSE','De Lunes a Sábado de 7:00 a.m. hasta 8:00 p.m. Domingos y Festivos de 8:00am a 6:00pm.',1,'sanjose',1),('COL12','PARQUE 93',3,89.00,'06:00:00','23:00:00',4.6756759,-74.0488112,'America/Colombia','http://201.245.114.238:70/wsPagoXCelular','CRA 12  # 93-43  PARQUE 93','LUNES A MIERCOLES 6 - 23, JUEVES A SABADO 6 - 3, DOMINGO 10 - 19',1,'parque93',1),('COL122','BAKERS',3,89.00,'06:00:00','23:00:00',4.6682435,-74.0520778,'America/Colombia','http://190.24.226.234:70/wsPagoXCelular','Calle 85 No. 11-64','LUNES A MARTES 6 - 22, MIERCOLES A SABADO 6 - 3, DOMINGO 8 - 20',1,'bakers',1),('COL123','BODY 134',3,89.00,'06:00:00','23:00:00',4.719523,-74.049456,'America/Colombia','http://186.30.243.70:70/wsPagoXCelular','Calle 134 A No. 21-08','De Lunes a Miércoles de 5:00 am. A 11:00 pm. Jueves a Viernes de 5:00 am. A 2:00 am. Sábados de 7:00 am. A 2:00 am. Domingos y Festivos de 7:00 am. A 4:00 p.m.',0,'body134',1),('COL124','BICENTENARIO',3,89.00,'06:00:00','23:00:00',4.6009636,-74.0700435,'America/Colombia','http://190.26.83.116:70/wsPagoXCelular','Carrera 4a No. 16-03','LUNES A VIERNES 6 - 22, SABADOS 7 - 17',1,'bicentenario',1),('COL128','CL 100 NO. 8-70',3,89.00,'06:00:00','23:00:00',4.680886,-74.039721,'America/Colombia',NULL,'calle 100 No. 8 - 70','De Lunes a Viernes de 6:00am hasta 11:00pm; Y Sábados 7:00am hasta 3:00pm',0,'umilitar',1),('COL129','JOCKEY CLUB',3,89.00,'06:00:00','23:00:00',4.6014951,-74.0709349,'America/Colombia','http://190.24.213.49:70/wsPagoXCelular','CL 16  # 04 - 68 PROCURADURIA','LUNES A VIERNES 6 - 22, SABADOS 7 - 17',0,NULL,1),('COL13','CL 85 # 12-46',3,89.00,'06:00:00','23:00:00',4.668798,-74.053042,'America/Colombia','http://200.93.139.142:70/wsPagoXCelular','calle 85 No 12-46','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 11 - 20',0,'calle85',1),('COL133','cra. 8 64-10',3,89.00,'06:00:00','23:00:00',4.649249,-74.060418,'America/Colombia',NULL,'CRA 8 #64-30 CALLE 64','LUNES A SABADO 6 - 22',0,'calle64',1),('COL134','Calle 93B',3,89.00,'06:00:00','23:00:00',4.679009,-74.051744,'America/Colombia','http://190.26.107.11:70/wsPagoXCelular','CL 93B # 15-80','LUNES A VIERNES 6 - 22, SABADO 7 - 15',1,'cl93b15',1),('COL135','Cra30 #48-38',3,89.00,'06:00:00','23:00:00',4.638333,-74.07902,'America/Colombia',NULL,'CR 30 #48-38 UNIVERSIDAD NACIONAL','LUNES A DOMINGO 6 - 22',0,'cr3048',1),('COL137','BUFFALO WINGS',3,89.00,'06:00:00','23:00:00',4.697837,-74.046125,'America/Colombia','http://168.28.226.73:70/wsPagoXCelular','CR  17 #116-15  BUFFALO WINGS','LUNES A JUEVES 11 - 23, VIERNES Y SABADO 12 - 1, DOMINGO 11 - 22',0,NULL,1),('COL138','Av. Americas 62- 84',3,89.00,'06:00:00','23:00:00',4.631126,-74.117431,'America/Colombia',NULL,'Av. AMERICS 62-84 OUTLET AMERICAS','De Lunes a Sabado de 08:00am hasta 10:00pm; Y Domingos y Festivos 8:00am hasta 8:00pm.',0,'outamericas',1),('COL139','Intercentro',3,89.00,'06:00:00','23:00:00',4.6102,-74.071016,'America/Colombia','http://201.244.83.44:70/wsPagoXCelular','CR 9 # 23 - 75 INTERCENTRO','Lunes a Sabado de 5:30am hasta 10:00pm; Y Domingos y Festivos 7:00am hasta 5:00pm.',1,'cr9cl24',1),('COL14','BODY TECH KENNEDY',3,89.00,'06:00:00','23:00:00',4.618721,-74.159597,'America/Colombia','http://200.119.20.122:70/wsPagoXCelular','transv. 78j No 41f-05  sur','De Lunes a Jueves de 5:00 am. A 11:00 pm. Viernes de 5:00 am. A 9:00 pm. Sábados de 7:00 am. A 6:00 pm. Domingos y festivos de 7:00 am. A 4:00 p.m.',1,'kennedy',1),('COL146','CR 13 # 25 - 31 FONADE',3,89.00,'06:00:00','23:00:00',4.612559,-74.071627,'America/Colombia','http://190.27.217.197:70/wsPagoXCelular','CR 13 # 25 - 31 FONADE','De Lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Sábados 7:00am hasta 06:00pm; Domingos y Festivos de 8:00am a 5:00pm.',1,'Fonade',1),('COL147','Av. Ciudad de Cali No. 51 – 66',3,89.00,'06:00:00','23:00:00',4.680198,-74.116803,'America/Colombia',NULL,'AV CIUDAD DE CALI 51 66','LUNES A VIERNES 6 - 20',0,'wbc',1),('COL148','Calle 25B Sur No. 5-87 20 de Julio',3,89.00,'06:00:00','23:00:00',4.569286,-74.093218,'America/Colombia','http://201.244.72.237:70/wsPagoXCelular','CALLE  25B SUR No. 5-87 20 de julio','24 HORAS',1,'20julio',1),('COL149','Calle 100 7 A- 81 ',3,89.00,'06:00:00','23:00:00',4.680343,-74.039531,'America/Colombia','http://190.24.151.80:70/wsPagoXCelular','Calle 100 7 A- 81 ','LUNES A VIERNES 6 - 22, SABADO 6 - 14',0,NULL,1),('COL151','CR 33 No 28 41',3,89.00,'06:00:00','23:00:00',4.631214,-74.081431,'America/Colombia',NULL,'CR 33 No 28 41','LUNES A VIERNES 6 - 22, SABADO 6 - 14',0,NULL,1),('COL152','BIBLIOTECA JULIO MSD ',3,89.00,'06:00:00','23:00:00',4.757577,-74.063455,'America/Colombia',NULL,'CL 170 No 67-51 centro cultural y biblioteca JMSD  ','LUNES A DOMINGO 7 - 19',0,NULL,1),('COL153','SALITRE OFICCE',3,89.00,'06:00:00','23:00:00',4.634933,-74.09591,'America/Colombia',NULL,'Cra. 46 # 22 B – 20                                             ','lunes a sábado 6:00 a 22:00',0,NULL,1),('COL154','El Alcazar Tennis',3,89.00,'06:00:00','23:00:00',4.810804,-74.036216,'America/Colombia',NULL,'Autopista Norte  235 Costado Oriental','sábado 7:00 a 19:00, domingo 7:00 19:00 (depende de las actividades programadas)',0,NULL,1),('COL155','Jungla Kumba ',3,89.00,'06:00:00','23:00:00',4.723717,-74.07226,'America/Colombia',NULL,'Cra. 59D 131-45','LUNES N.A, MARTES A JUEVES 12 - 21, VIERNES 12 - 23, SABADO Y DOMINGO 8 - 23',0,NULL,1),('COL156','Green Oficce',3,89.00,'06:00:00','23:00:00',4.681511,-74.044056,'America/Colombia',NULL,'Cr 11  No 98-07',NULL,0,NULL,1),('COL157','CRA 18 136A – 14',3,89.00,'06:00:00','23:00:00',4.720956,-74.045097,'America/Colombia',NULL,'CRA 18 136ª – 14',NULL,0,NULL,1),('COL158','AC 13 NO 66-70',3,89.00,'06:00:00','23:00:00',4.637033,-74.117473,'America/Colombia',NULL,'AC 13 NO 66-70',NULL,0,NULL,1),('COL159','12 de Octubre',3,89.00,'06:00:00','23:00:00',4.669375,-74.074916,'America/Colombia',NULL,'52 # 72-1 a 72-99','24 HORAS',0,NULL,1),('COL16','SULTANA',3,89.00,'06:00:00','23:00:00',4.597565,-74.108341,'America/Colombia','http://186.30.249.166:70/wsPagoXCelular','calle 12 sur No 31-37*33','LUNES A JUEVES 5 - 24, VIERNES 5 - 22, SABADO 6:30 - 19, DOMINGO 6:30 - 17',1,'sultana',1),('COL161','Alcazar Futbol',3,89.00,'06:00:00','23:00:00',4.81234,-74.035974,'America/Colombia',NULL,'Autopista Norte  236 Costado Oriental','sábado 7:00 a 19:00, domingo 7:00 19:00 (depende de las actividades programadas)',0,NULL,1),('COL18','BLOCKBUSTER-CHAPINERO',3,89.00,'06:00:00','23:00:00',4.643608,-74.0616881,'America/Colombia','http://201.245.63.158:70/wsPagoXCelular','Cra.  7 No  58-18','Jueves 7 a 1AM y Sábado 7 a 3AM',1,'blockbuster58',1),('COL19','HOTEL BEST WESTERN',3,89.00,'06:00:00','23:00:00',4.676082,-74.050733,'America/Colombia',NULL,'Calle 93 13-71','LUNES A VIERNES 6 - 22, SABADO 6 - 23',0,'bestwestern',1),('COL21','CL 93B # 13- 55',3,89.00,'06:00:00','23:00:00',4.6778841,-74.0495929,'America/Colombia','http://200.93.170.242:70/wsPagoXCelular','calle 93B No 13-55','LUNES A MIERCOLES 6 - 23, JUEVES A SABADO 6 - 3, DOMINGO 9 - 23',1,'calle93b',1),('COL22','CL 81 #11-34/44',3,89.00,'06:00:00','23:00:00',4.6652432,-74.0536932,'America/Colombia','http://201.244.183.138:70/wsPagoXCelular','calle 81 No 11-34-44','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 11 - 19',1,'calle81',1),('COL23','CL 95 # 11A-67',3,89.00,'06:00:00','23:00:00',4.679475,-74.047211,'America/Colombia','http://200.69.114.250:70/wsPagoXCelular','calle 95 No 11a -67','De lunes a miércoles de 6:00 a.m. hasta 11:00 p.m. Los días jueves a sábados de 6:00 a.m. hasta 3:00 a.m. Los días domingo y festivos de 9:00 a.m. hasta 5:00 p.m.',1,'calle95',1),('COL24','CL 100 # 13-67',3,89.00,'06:00:00','23:00:00',4.6840487,-74.046752,'America/Colombia','http://201.245.150.10:70/wsPagoXCelular','calle 100 No 13-67','LUNES A SABADO 6 - 23, DOMINGO 10 - 21',1,'calle100',1),('COL25','AUTO NORTE JAVESALUD',3,89.00,'06:00:00','23:00:00',4.704524,-74.053939,'America/Colombia','http://201.245.100.42:70/wsPagoXCelular','autp.nte No 124-02','LUNES A VIERNES 6 - 22, SABADO 6 - 15, DOMINGO 8 - 16',1,'javesalud',1),('COL28','(CHICO - 2)',3,89.00,'06:00:00','23:00:00',4.6910752,-74.0510944,'America/Colombia',NULL,'avda.19 No 104-52','LUNES A VIERNES 5 - 22, SABADO 7 - 22',0,'calle104',1),('COL29','CARR 11 -  84-50',3,89.00,'06:00:00','23:00:00',4.6672451,-74.0518061,'America/Colombia','http://201.244.188.26:70/wsPagoXCelular',' cra, 11 No 84 -50','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 10 - 20',1,'calle84',1),('COL31','CL 99 # 10-47',3,89.00,'06:00:00','23:00:00',4.681572,-74.043385,'America/Colombia','http://65.167.51.126:70/wsPagoXCelular','Calle 99 No. 10-47','De Lunes a Viernes de 6:00am a 11:00pm; Sábados 7:00am a 7:00pm; Domingos y Festivos de 11:00am hasta 7:00pm.',1,'calle99',1),('COL33','CL 122 # 15A- 64',3,89.00,'06:00:00','23:00:00',4.70111,-74.04472,'America/Colombia','http://65.167.80.86:70/wsPagoXCelular','Calle 122 No 15A -64','LUNES A MIERCOLES 7 - 22, JUEVES A SABADO 7 - 3, DOMINGO 10 - 19',1,'calle122',1),('COL34','MEGATOWER',3,89.00,'06:00:00','23:00:00',4.6837911,-74.0462276,'America/Colombia','http://201.245.64.154:70/wsPagoXCelular','calle 100 No 13-21','LUNES A VIERNES 5:30 - 22, SABADO 6 - 14',1,'megatower',1),('COL35','PLAZA DE TOROS',3,89.00,'06:00:00','23:00:00',4.614369,-74.067343,'America/Colombia','http://200.75.40.238:70/wsPagoXCelular','Calle 27 No. 5 44/cl 28 No. 5 23','24 HORAS',1,'plazadetoros',1),('COL37','VALMARIA',3,89.00,'06:00:00','23:00:00',4.69566,-74.02926,'America/Colombia','http://201.245.82.69:70/wsPagoXCelular','calle 119 No 3-00','24 HORAS',0,'valmaria',1),('COL38','SHOW PLACE',3,89.00,'06:00:00','23:00:00',4.723221,-74.026933,'America/Colombia','http://190.24.205.202:70/wsPagoXCelular','avda 147 No 7-70','LUNES A VIERNES 4 - 24, SABADO 6 - 23, DOMINGO 6 - 22',1,'showplace',1),('COL4','CU. EXTERNADO',3,89.00,'06:00:00','23:00:00',4.6768521,-74.0518781,'America/Colombia','http://190.24.224.126:70/wsPagoXCelular','CRA 15 # 93-07/09  (U. EXTERNADO)','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3',1,'calle93',1),('COL40','PISO 6 NORTH POINT PRIVAD',3,89.00,'06:00:00','23:00:00',4.733126,-74.024053,'America/Colombia','http://65.167.83.42:70/wsPagoXCelular','cra. 7 No. 156-80','24 horas',1,'northpoint',1),('COL42','BODY TECH 102',3,89.00,'06:00:00','23:00:00',4.687791,-74.051854,'America/Colombia','http://200.93.139.38:70/wsPagoXCelular','avda. 19 No 102 -31','LUNES A VIERNES 24 HORAS, SABADO 6 - 22, DOMINGO 6 19',0,'bodytech',1),('COL43','CALLE 94',3,89.00,'06:00:00','23:00:00',4.680353,-74.049984,'America/Colombia','http://200.93.188.22:70/wsPagoXCelular','CRA 15 # 94-72/78 CALLE 94','De Lunes a Sábados de 6:00 am a 10:00 pm.',1,'calle94',1),('COL44','JARDINES',3,89.00,'06:00:00','23:00:00',4.784422,-74.041253,'America/Colombia','http://200.93.159.230:70/wsPagoXCelular','Autopista Norte No 207-47','De Domingo a Domingo de 6:30 am a 10:00 pm.',1,'jardines',1),('COL45','PAS. LOS LIBERT',3,89.00,'06:00:00','23:00:00',4.783444,-74.041499,'America/Colombia','http://190.25.132.132:70/wsPagoXCelular','AV CRA 45 (PAS. LOS LIBERT)#207- 41 ECI','De lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Y los sábados de 6:00 a.m. hasta 06:00 p.m.',1,'eci',1),('COL46','SAN RAFAEL',3,89.00,'06:00:00','23:00:00',4.723923,-74.06199,'America/Colombia','http://200.93.139.106:70/wsPagoXCelular','calle 134 No-55-30','De Lunes a miércoles de 7:00 am a 01:00 am; jueves, Viernes y Sábados de 7:00 am a 5:00 am; Domingos y Festivos de 7:30 am. A 01:00 am. ',1,'sanrafael',1),('COL49','PLAZA 39',3,89.00,'06:00:00','23:00:00',4.626375,-74.066177,'America/Colombia','http://201.244.178.90:70/wsPagoXCelular','Diagonal  40A No  7-84 PLAZA 39','De lunes a miércoles de 6:00 a.m. hasta 12:00 p.m., Los días jueves a sábados las 24 horas. Y los domingos y festivos de 11:00 a.m. hasta 7:00 p.m.',1,'plaza39',1),('COL54','CENTENARIO',3,89.00,'06:00:00','23:00:00',4.634877,-74.115724,'America/Colombia',NULL,'calle 13 No 65-71','De lunes a viernes de 6:00 a.m. hasta 8:00 p.m., Los días sábados de 8:00 a.m. hasta 8:00 p.m. Domingos y festivos de 9:00 a.m. hasta 7:00 ',0,'centenario',1),('COL59','BANCOLOMBIA',3,89.00,'06:00:00','23:00:00',4.6165921,-74.067804,'America/Colombia','http://200.118.112.12:70/wsPagoXCelular','Calle 30 No 6-38','LUNES A VIERNES 5:30 - 22',1,'bancolombia',1),('COL6','CHICO 104',3,89.00,'06:00:00','23:00:00',4.690437,-74.051528,'America/Colombia','http://200.93.171.226:70/wsPagoXCelular','avd. 19 No 104 -37','LUNES A VIERNES 5 - 22, SABADO 7 - 22',1,'chico104',1),('COL62','PEPE SIERRA',3,89.00,'06:00:00','23:00:00',4.697839,-74.046093,'America/Colombia','http://201.244.71.76:70/wsPagoXCelular','Cra 17 No 116-14','LUNES A VIERNES 7 - 3, SABADO 8 - 3, DOMINGO 12 - 3',1,'pepesierra',1),('COL69','BTC',3,89.00,'06:00:00','23:00:00',4.680436,-74.043937,'America/Colombia','http://201.245.74.38:70/wsPagoXCelular','Cra 10 No 97A-13','De lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Los días sábados de 6:00 a.m. hasta 5:00 p.m.',1,'btc',1),('COL70','UMB',3,89.00,'06:00:00','23:00:00',4.642882,-74.054299,'America/Colombia','http://201.245.132.242:70/wsPagoXCelular','Avda. Circunvalar No 60-00','LUNES A VIERNES 6 - 22, SABADO 6 - 19',1,'manuelabeltran',1),('COL71','CL 103 # 14A- 53 BBC',3,89.00,'06:00:00','23:00:00',4.687714,-74.046992,'America/Colombia','http://201.244.58.70:70/wsPagoXCelular','Calle 103No 14 a  -53','De lunes a sábados de 6:00 a.m. hasta 10:00 p.m., Los días domingos 6:00 a.m. hasta 3:00 p.m. y Festivos 7:00 a.m. hasta 2:00 p.m',1,'bbc',1),('COL74','ESTE UNIV. EXTERNADO PPAL',3,89.00,'06:00:00','23:00:00',4.595314,-74.069066,'America/Colombia','http://201.244.183.174:70/wsPagoXCelular','Calle 12 No 1-17 este','LUNES A VIERNES 5:30 - 22, SABADOS 6 - 18',1,'uexternado',1),('COL77','SURTIFRUVER 85',3,89.00,'06:00:00','23:00:00',4.669686,-74.054654,'America/Colombia','http://190.145.101.3:70/wsPagoXCelular','Calle 85 No 14-05','LUNES A MIERCOLES 6 - 21, JUEVES A SABADO 6 - 3, DOMINGO 8 - 21',1,'surtifruver85',1),('COL8','CAPITAL TOWER',3,89.00,'06:00:00','23:00:00',4.680051,-74.038948,'America/Colombia','http://200.71.45.20:70/wsPagoXCelular','Calle 100 No 7 - 33','LUNES A VIERNES 5:30 - 22, SABADO 6 - 14',0,'capitaltower',1),('COL82','BODY COLINA',3,89.00,'06:00:00','23:00:00',4.728943,-74.066127,'America/Colombia','http://201.244.58.54:70/wsPagoXCelular','Calle 138 No 58-74','De Lunes a Jueves de 4:00 am a 10:00 pm; Viernes, Sábados, Domingos y Festivos de 6:00 am a 10:00 pm.',1,'bodytechcolina',1),('COL85','BODY 166',3,89.00,'06:00:00','23:00:00',4.748339,-74.046548,'America/Colombia','http://201.245.150.198:70/wsPagoXCelular','CR 23 # 166-59 BODY AUTO-NORTE','De Lunes a Jueves de 5:00 am a 11:00 pm; Viernes 5:00 am a 9:00 pm; Sábados de 7:00 am a 6:00 pm; Domingos y Festivos de 7:00 am a 4:00 pm.',1,'bodytechautopista',1),('COL86','BODY CHAPINERO',3,89.00,'06:00:00','23:00:00',4.646627,-74.060495,'America/Colombia',NULL,'CR 7  No 61- 47 BODYTECH  CHAPINERO','LUNES A VIERNES 4 - 24, SABADO 6 - 24, DOMINGO 6 - 20',0,'bodytech63',1),('COL9','FRISBY',3,89.00,'06:00:00','23:00:00',4.693958,-74.050839,'America/Colombia','http://201.244.187.250:70/wsPagoXCelular','avd.19 No. 106 a -45','LUNES A SABADO 6 - 22, DOMINGO 11- 20',1,'frisbv',1),('COL90','ROSALES',3,89.00,'06:00:00','23:00:00',4.653001,-74.054378,'America/Colombia','http://200.93.185.54:70/wsPagoXCelular','carrera 5 No. 70A -74','De Lunes a Jueves de 6:00am a 10:00pm; Viernes y Sábado de 6:00am a 3:00am; Domingos y Festivos de 9:00am a 9:00pm.',1,'rosales',1),('COL91','DAN CARLTON',3,89.00,'06:00:00','23:00:00',4.680179,-74.052948,'America/Colombia','http://200.93.172.10:70/wsPagoXCelular','Cra. 18  93B-49','LUNES A VIERNES 6 - 22, SABADO 7 - 15',1,'carrera18',1),('COL92','METRO 127',3,89.00,'06:00:00','23:00:00',4.7037,-74.043628,'America/Colombia','http://190.146.236.21:70/wsPagoXCelular','Calle 127 15 A 03','24 HORAS',1,'metro127',1),('COL95','MEDICAL CENTER',3,89.00,'06:00:00','23:00:00',4.6960488,-74.0322167,'America/Colombia','http://200.119.9.42:70/wsPagoXCelular','Calle 119 No. 7-14','24 HORAS',1,'medicalcenter',1),('COL97','CARGO PORT',3,89.00,'06:00:00','23:00:00',4.692501,-74.13579,'America/Colombia','http://190.26.99.54:70/wsPagoXCelular','Calle 26 106 - 39','24 HORAS',1,'cargoport',1);
/*!40000 ALTER TABLE `zone` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cps_city`.`generateZoneId`
BEFORE INSERT ON `cps_city`.`zone`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
DECLARE paisPrefix VARCHAR(3);
DECLARE zoneCnt VARCHAR(5);
SELECT countryPrefix INTO paisPrefix
       FROM country c
       INNER JOIN client cl ON(c.idcountry = cl.country_idcountry) 
       WHERE cl.idclient = NEW.client_idclient;
SELECT (COUNT(*) + 1) INTO zoneCnt 
       FROM zone WHERE idzone like concat(paisPrefix,'%');
IF zoneCnt < 10 THEN
 SET zoneCnt = concat('0',zoneCnt);
END IF;
SET NEW.idzone = TRIM(concat(paisPrefix,zoneCnt));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `zone_temp`
--

DROP TABLE IF EXISTS `zone_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone_temp` (
  `idzone` varchar(30) NOT NULL,
  `name` varchar(45) NOT NULL,
  `client_idclient` bigint(20) DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `openTime` time DEFAULT NULL,
  `closeTime` time DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `gmt` varchar(200) DEFAULT 'America/Toronto',
  `url` varchar(512) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `schedule` text,
  `online` smallint(6) DEFAULT '0',
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone_temp`
--

LOCK TABLES `zone_temp` WRITE;
/*!40000 ALTER TABLE `zone_temp` DISABLE KEYS */;
INSERT INTO `zone_temp` VALUES ('COL10','CL 97 #15 -21',3,89.00,'06:00:00','23:00:00',4.682182,-74.049151,'America/Colombia',NULL,'calle 97 No 15-21','De Lunes de 6:00am. A 10:00pm. Martes a Sábado de 6:00am. A 11:00pm. Domingos y Festivos de 11:00am A 6:00pm.',0,'calle97'),('COL100','CR 15 A # 124 -44',3,89.00,'06:00:00','23:00:00',4.7026963,-74.0436682,'America/Colombia','http://201.245.114.30:70/wsPagoXCelular','Cra 15 A No. 124-40','LUNES A SABADO 6 - 22',1,'calle124'),('COL101','CL 59A BIS NO. 5-53 LINK 7-60',3,89.00,'06:00:00','23:00:00',4.644907,-74.061064,'America/Colombia','http://201.245.63.50:70/wsPagoXCelular','Calle 59 A Bis 5-53','LUNES A VIERNES 6 - 22, SABADO 8 - 18',1,'link760'),('COL102','CL 116 # 15-80',3,89.00,'06:00:00','23:00:00',4.6969234,-74.0442163,'America/Colombia','http://200.93.136.194:70/wsPagoXCelular','Cale 116 No 15-80','LUNES A JUEVES 6 - 22, VIERNES Y SABADO 7 - 1, DOMINGO 14 - 18',1,'calle116'),('COL103','(AV68)',3,89.00,'06:00:00','23:00:00',4.687276,-74.073441,'America/Colombia','http://65.167.49.38:70/wsPagoXCelular','Calle 94 A No. 67 A 74','LUNES A VIERNES 6 - 23, SABADO Y DOMINGO 6:30 - 24',1,'mall68'),('COL105','TC1',3,89.00,'06:00:00','23:00:00',4.693736,-74.138748,'America/Colombia','http://190.24.242.6:70/wsPagoXCelular','Calle 26 No.106 - 39 TERMINAL DE CARGA','24 HORAS',1,'tc1'),('COL108','SURTIFRUVER 76',3,89.00,'06:00:00','23:00:00',4.661112,-74.0564589,'America/Colombia','http://201.245.104.194:70/wsPagoXCelular','Calle 76 No. 11 15','De Lunes a Domingo de 6:00am a 10:00pm.',1,'surtifruver76'),('COL109','CANDELARIA',3,89.00,'06:00:00','23:00:00',4.598781,-74.07045,'America/Colombia','http://190.24.249.71:70/wsPagoXCelular','Calle 12 C 3 -32','LUNES A JUEVES 5:30 - 22, VIERNES Y SABADOS 5:30 - 24',1,'candelaria'),('COL11','CREPES',3,89.00,'06:00:00','23:00:00',4.6576878,-74.0559868,'America/Colombia','http://201.244.178.18:70/wsPagoXCelular','calle 74 No 9-21','LUNES A SABADO 6 - 23, DOMINGO 11 - 21',1,'crepes'),('COL114','CASTELLANA',3,89.00,'06:00:00','23:00:00',4.6831482,-74.0587158,'America/Colombia','http://201.245.152.74:70/wsPagoXCelular','Cra. 45 A No. 94-66','LUNES A SABADO 6 - 23, DOMINGO 6 - 22',1,'castellana'),('COL115','CRA 7 NO. 117 - 14',3,89.00,'06:00:00','23:00:00',4.694879,-74.032451,'America/Colombia',NULL,'Cra 7 No. 117 - 44','DOMINGO A MARTES 6 - 22, MIERCOLES A SABADO 24 HORAS',0,'calle117'),('COL116','TORRE ZIMMA',3,89.00,'06:00:00','23:00:00',4.674343,-74.052876,'America/Colombia',NULL,'Cra 15 No. 88 - 64','LUNES A VIERNES 6 - 21:30, SABADO 6 - 17',0,'torrezimma'),('COL118','SAN JOSE',3,89.00,'06:00:00','23:00:00',4.60556,-74.089865,'America/Colombia','http://201.244.73.202:70/wsPagoXCelular','CR 22 #9- 35 SAN JOSE','De Lunes a Sábado de 7:00 a.m. hasta 8:00 p.m. Domingos y Festivos de 8:00am a 6:00pm.',1,'sanjose'),('COL12','PARQUE 93',3,89.00,'06:00:00','23:00:00',4.6756759,-74.0488112,'America/Colombia','http://201.245.114.238:70/wsPagoXCelular','CRA 12  # 93-43  PARQUE 93','LUNES A MIERCOLES 6 - 23, JUEVES A SABADO 6 - 3, DOMINGO 10 - 19',1,'parque93'),('COL122','BAKERS',3,89.00,'06:00:00','23:00:00',4.6682435,-74.0520778,'America/Colombia','http://190.24.226.234:70/wsPagoXCelular','Calle 85 No. 11-64','LUNES A MARTES 6 - 22, MIERCOLES A SABADO 6 - 3, DOMINGO 8 - 20',1,'bakers'),('COL123','BODY 134',3,89.00,'06:00:00','23:00:00',4.719523,-74.049456,'America/Colombia',NULL,'Calle 134 A No. 21-08','De Lunes a Miércoles de 5:00 am. A 11:00 pm. Jueves a Viernes de 5:00 am. A 2:00 am. Sábados de 7:00 am. A 2:00 am. Domingos y Festivos de 7:00 am. A 4:00 p.m.',0,'body134'),('COL124','BICENTENARIO',3,89.00,'06:00:00','23:00:00',4.6009636,-74.0700435,'America/Colombia','http://200.119.76.55:70/wsPagoXCelular','Carrera 4a No. 16-03','LUNES A VIERNES 6 - 22, SABADOS 7 - 17',0,'bicentenario'),('COL128','CL 100 NO. 8-70',3,89.00,'06:00:00','23:00:00',4.680886,-74.039721,'America/Colombia',NULL,'calle 100 No. 8 - 70','De Lunes a Viernes de 6:00am hasta 11:00pm; Y Sábados 7:00am hasta 3:00pm',0,'umilitar'),('COL129','PROCURADURIA',3,89.00,'06:00:00','23:00:00',4.6014951,-74.0709349,'America/Colombia',NULL,'CL 16  # 04 - 68 PROCURADURIA','LUNES A VIERNES 6 - 22, SABADOS 7 - 17',0,NULL),('COL13','CL 85 # 12-46',3,89.00,'06:00:00','23:00:00',4.668798,-74.053042,'America/Colombia','http://200.93.139.142:70/wsPagoXCelular','calle 85 No 12-46','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 11 - 20',0,'calle85'),('COL133','cra. 8 64-10',3,89.00,'06:00:00','23:00:00',4.649249,-74.060418,'America/Colombia',NULL,'CRA 8 #64-30 CALLE 64','LUNES A SABADO 6 - 22',0,'calle64'),('COL134','CL 93B # 15-80',3,89.00,'06:00:00','23:00:00',4.679009,-74.051744,'America/Colombia','http://200.93.170.242:70/wsPagoXCelular','CL 93B # 15-80','LUNES A VIERNES 6 - 22, SABADO 7 - 15',1,'cl93b15'),('COL135','Cra30 #48-38',3,89.00,'06:00:00','23:00:00',4.638333,-74.07902,'America/Colombia',NULL,'CR 30 #48-38 UNIVERSIDAD NACIONAL','LUNES A DOMINGO 6 - 22',0,'cr3048'),('COL137','BUFFALO WINGS',3,89.00,'06:00:00','23:00:00',4.697837,-74.046125,'America/Colombia','http://168.28.226.73:70/wsPagoXCelular','CR  17 #116-15  BUFFALO WINGS','LUNES A JUEVES 11 - 23, VIERNES Y SABADO 12 - 1, DOMINGO 11 - 22',0,NULL),('COL138','Av. Américas 62- 84',3,89.00,'06:00:00','23:00:00',4.631126,-74.117431,'America/Colombia',NULL,'Av. AMERICS 62-84 OUTLET AMERICAS','De Lunes a Sabado de 08:00am hasta 10:00pm; Y Domingos y Festivos 8:00am hasta 8:00pm.',0,'outamericas'),('COL139','Intercentro',3,89.00,'06:00:00','23:00:00',4.6102,-74.071016,'America/Colombia','http://201.244.83.44:70/wsPagoXCelular','CR 9 # 23 - 75 INTERCENTRO','Lunes a Sabado de 5:30am hasta 10:00pm; Y Domingos y Festivos 7:00am hasta 5:00pm.',1,'cr9cl24'),('COL14','BODY TECH KENNEDY',3,89.00,'06:00:00','23:00:00',4.618721,-74.159597,'America/Colombia','http://200.119.20.122:70/wsPagoXCelular','transv. 78j No 41f-05  sur','De Lunes a Jueves de 5:00 am. A 11:00 pm. Viernes de 5:00 am. A 9:00 pm. Sábados de 7:00 am. A 6:00 pm. Domingos y festivos de 7:00 am. A 4:00 p.m.',1,'kennedy'),('COL146','CR 13 # 25 - 31 FONADE',3,89.00,'06:00:00','23:00:00',4.612559,-74.071627,'America/Colombia',NULL,'CR 13 # 25 - 31 FONADE','De Lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Sábados 7:00am hasta 06:00pm; Domingos y Festivos de 8:00am a 5:00pm.',0,'Fonade'),('COL147','Av. Ciudad de Cali No. 51 – 66',3,89.00,'06:00:00','23:00:00',4.680198,-74.116803,'America/Colombia',NULL,'AV CIUDAD DE CALI 51 66','LUNES A VIERNES 6 - 20',0,'wbc'),('COL148','Calle 25B Sur No. 5-87 20 de Julio',3,89.00,'06:00:00','23:00:00',4.569286,-74.093218,'America/Colombia','http://201.244.72.237:70/wsPagoXCelular','CALLE  25B SUR No. 5-87 20 de julio','24 HORAS',0,'20julio'),('COL149','Calle 100 7 A- 81 ',3,89.00,'06:00:00','23:00:00',4.680343,-74.039531,'America/Colombia','http://190.24.151.80:70/wsPagoXCelular','Calle 100 7 A- 81 ','LUNES A VIERNES 6 - 22, SABADO 6 - 14',1,NULL),('COL151','CR 33 No 28 41',3,89.00,'06:00:00','23:00:00',4.631214,-74.081431,'America/Colombia',NULL,'CR 33 No 28 41','LUNES A VIERNES 6 - 22, SABADO 6 - 14',0,NULL),('COL152','BIBLIOTECA JULIO MSD ',3,89.00,'06:00:00','23:00:00',4.757577,-74.063455,'America/Colombia',NULL,'CL 170 No 67-51 centro cultural y biblioteca JMSD  ','LUNES A DOMINGO 7 - 19',0,NULL),('COL153','SALITRE OFICCE',3,89.00,'06:00:00','23:00:00',4.634933,-74.09591,'America/Colombia',NULL,'Cra. 46 # 22 B – 20                                             ','lunes a sábado 6:00 a 22:00',0,NULL),('COL154','El Alcazar Tennis',3,89.00,'06:00:00','23:00:00',4.810804,-74.036216,'America/Colombia',NULL,'Autopista Norte  235 Costado Oriental','sábado 7:00 a 19:00, domingo 7:00 19:00 (depende de las actividades programadas)',0,NULL),('COL155','Jungla Kumba ',3,89.00,'06:00:00','23:00:00',4.723717,-74.07226,'America/Colombia',NULL,'Cra. 59D 131-45','LUNES N.A, MARTES A JUEVES 12 - 21, VIERNES 12 - 23, SABADO Y DOMINGO 8 - 23',0,NULL),('COL156','Green Oficce',3,89.00,'06:00:00','23:00:00',4.681511,-74.044056,'America/Colombia',NULL,'Cr 11  No 98-07',NULL,0,NULL),('COL157','CRA 18 136A – 14',3,89.00,'06:00:00','23:00:00',4.720956,-74.045097,'America/Colombia',NULL,'CRA 18 136ª – 14',NULL,0,NULL),('COL158','AC 13 NO 66-70',3,89.00,'06:00:00','23:00:00',4.637033,-74.117473,'America/Colombia',NULL,'AC 13 NO 66-70',NULL,0,NULL),('COL159','12 de Octubre',3,89.00,'06:00:00','23:00:00',4.669375,-74.074916,'America/Colombia',NULL,'52 # 72-1 a 72-99','24 HORAS',0,NULL),('COL16','SULTANA',3,89.00,'06:00:00','23:00:00',4.597565,-74.108341,'America/Colombia','http://186.30.249.166:70/wsPagoXCelular','calle 12 sur No 31-37*33','LUNES A JUEVES 5 - 24, VIERNES 5 - 22, SABADO 6:30 - 19, DOMINGO 6:30 - 17',1,'sultana'),('COL161','Alcazar Futbol',3,89.00,'06:00:00','23:00:00',4.81234,-74.035974,'America/Colombia',NULL,'Autopista Norte  236 Costado Oriental','sábado 7:00 a 19:00, domingo 7:00 19:00 (depende de las actividades programadas)',0,NULL),('COL18','BLOCKBUSTER-CHAPINERO',3,89.00,'06:00:00','23:00:00',4.643608,-74.0616881,'America/Colombia','http://201.245.63.158:70/wsPagoXCelular','Cra.  7 No  58-18','Jueves 7 a 1AM y Sábado 7 a 3AM',1,'blockbuster58'),('COL19','HOTEL BEST WESTERN',3,89.00,'06:00:00','23:00:00',4.676082,-74.050733,'America/Colombia',NULL,'Calle 93 13-71','LUNES A VIERNES 6 - 22, SABADO 6 - 23',0,'bestwestern'),('COL21','CL 93B # 13- 55',3,89.00,'06:00:00','23:00:00',4.6778841,-74.0495929,'America/Colombia','http://200.93.170.242:70/wsPagoXCelular','calle 93B No 13-55','LUNES A MIERCOLES 6 - 23, JUEVES A SABADO 6 - 3, DOMINGO 9 - 23',1,'calle93b'),('COL22','CL 81 #11-34/44',3,89.00,'06:00:00','23:00:00',4.6652432,-74.0536932,'America/Colombia','http://201.244.183.138:70/wsPagoXCelular','calle 81 No 11-34-44','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 11 - 19',1,'calle81'),('COL23','CL 95 # 11A-67',3,89.00,'06:00:00','23:00:00',4.679475,-74.047211,'America/Colombia','http://200.69.114.250:70/wsPagoXCelular','calle 95 No 11a -67','De lunes a miércoles de 6:00 a.m. hasta 11:00 p.m. Los días jueves a sábados de 6:00 a.m. hasta 3:00 a.m. Los días domingo y festivos de 9:00 a.m. hasta 5:00 p.m.',1,'calle95'),('COL24','CL 100 # 13-67',3,89.00,'06:00:00','23:00:00',4.6840487,-74.046752,'America/Colombia','http://201.245.150.10:70/wsPagoXCelular','calle 100 No 13-67','LUNES A SABADO 6 - 23, DOMINGO 10 - 21',1,'calle100'),('COL25','AUTO NORTE  JAVESALUD',3,89.00,'06:00:00','23:00:00',4.704524,-74.053939,'America/Colombia','http://201.245.100.42:70/wsPagoXCelular','autp.nte No 124-02','LUNES A VIERNES 6 - 22, SABADO 6 - 15, DOMINGO 8 - 16',1,'javesalud'),('COL28','(CHICO - 2)',3,89.00,'06:00:00','23:00:00',4.6910752,-74.0510944,'America/Colombia',NULL,'avda.19 No 104-52','LUNES A VIERNES 5 - 22, SABADO 7 - 22',0,'calle104'),('COL29','CARR 11 -  84-50',3,89.00,'06:00:00','23:00:00',4.6672451,-74.0518061,'America/Colombia','http://201.244.188.26:70/wsPagoXCelular','cra','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 10 - 20',1,'calle84'),('COL31','CL 99 # 10-47',3,89.00,'06:00:00','23:00:00',4.681572,-74.043385,'America/Colombia','http://65.167.51.126:70/wsPagoXCelular','Calle 99 No. 10-47','De Lunes a Viernes de 6:00am a 11:00pm; Sábados 7:00am a 7:00pm; Domingos y Festivos de 11:00am hasta 7:00pm.',1,'calle99'),('COL33','CL 122 # 15A- 64',3,89.00,'06:00:00','23:00:00',4.70111,-74.04472,'America/Colombia','http://65.167.80.86:70/wsPagoXCelular','Calle 122 No 15A -64','LUNES A MIERCOLES 7 - 22, JUEVES A SABADO 7 - 3, DOMINGO 10 - 19',1,'calle122'),('COL34','MEGATOWER',3,89.00,'06:00:00','23:00:00',4.6837911,-74.0462276,'America/Colombia','http://201.245.64.154:70/wsPagoXCelular','calle 100 No 13-21','LUNES A VIERNES 5:30 - 22, SABADO 6 - 14',1,'megatower'),('COL35','PLAZA DE TOROS',3,89.00,'06:00:00','23:00:00',4.614369,-74.067343,'America/Colombia','http://200.75.40.238:70/wsPagoXCelular','Calle 27 No. 5 44/cl 28 No. 5 23','24 HORAS',1,'plazadetoros'),('COL37','VALMARIA',3,89.00,'06:00:00','23:00:00',4.69566,-74.02926,'America/Colombia','http://201.245.82.69:70/wsPagoXCelular','calle 119 No 3-00','24 HORAS',1,'valmaria'),('COL38','SHOW PLACE',3,89.00,'06:00:00','23:00:00',4.723221,-74.026933,'America/Colombia','http://190.24.205.202:70/wsPagoXCelular','avda 147 No 7-70','LUNES A VIERNES 4 - 24, SABADO 6 - 23, DOMINGO 6 - 22',1,'showplace'),('COL4','CU. EXTERNADO',3,89.00,'06:00:00','23:00:00',4.6768521,-74.0518781,'America/Colombia','http://190.24.224.126:70/wsPagoXCelular','CRA 15 # 93-07/09  (U. EXTERNADO)','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3',1,'calle93'),('COL40','PISO 6 NORTH POINT PRIVAD',3,89.00,'06:00:00','23:00:00',4.733126,-74.024053,'America/Colombia','http://65.167.83.42:70/wsPagoXCelular','cra. 7 No. 156-80','24 horas',1,'northpoint'),('COL42','BODY TECH 102',3,89.00,'06:00:00','23:00:00',4.687791,-74.051854,'America/Colombia','http://200.93.139.38:70/wsPagoXCelular','avda. 19 No 102 -31','LUNES A VIERNES 24 HORAS, SABADO 6 - 22, DOMINGO 6 19',1,'bodytech'),('COL43','CALLE 94',3,89.00,'06:00:00','23:00:00',4.680353,-74.049984,'America/Colombia','http://200.93.188.22:70/wsPagoXCelular','CRA 15 # 94-72/78 CALLE 94','De Lunes a Sábados de 6:00 am a 10:00 pm.',1,'calle94'),('COL44','JARDINES',3,89.00,'06:00:00','23:00:00',4.784422,-74.041253,'America/Colombia','http://200.93.159.230:70/wsPagoXCelular','Autopista Norte No 207-47','De Domingo a Domingo de 6:30 am a 10:00 pm.',1,'jardines'),('COL45','PAS. LOS LIBERT',3,89.00,'06:00:00','23:00:00',4.783444,-74.041499,'America/Colombia',NULL,'AV CRA 45 (PAS. LOS LIBERT)#207- 41 ECI','De lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Y los sábados de 6:00 a.m. hasta 06:00 p.m.',0,'eci'),('COL46','SAN RAFAEL',3,89.00,'06:00:00','23:00:00',4.723923,-74.06199,'America/Colombia','http://200.93.139.106:70/wsPagoXCelular','calle 134 No-55-30','De Lunes a miércoles de 7:00 am a 01:00 am; jueves, Viernes y Sábados de 7:00 am a 5:00 am; Domingos y Festivos de 7:30 am. A 01:00 am. ',1,'sanrafael'),('COL49','PLAZA 39',3,89.00,'06:00:00','23:00:00',4.626375,-74.066177,'America/Colombia','http://201.244.178.90:70/wsPagoXCelular','Diagonal  40A No  7-84 PLAZA 39','De lunes a miércoles de 6:00 a.m. hasta 12:00 p.m., Los días jueves a sábados las 24 horas. Y los domingos y festivos de 11:00 a.m. hasta 7:00 p.m.',1,'plaza39'),('COL54','CENTENARIO',3,89.00,'06:00:00','23:00:00',4.634877,-74.115724,'America/Colombia',NULL,'calle 13 No 65-71','De lunes a viernes de 6:00 a.m. hasta 8:00 p.m., Los días sábados de 8:00 a.m. hasta 8:00 p.m. Domingos y festivos de 9:00 a.m. hasta 7:00 ',0,'centenario'),('COL59','BANCOLOMBIA',3,89.00,'06:00:00','23:00:00',4.6165921,-74.067804,'America/Colombia','http://200.118.112.12:70/wsPagoXCelular','Calle 30 No 6-38','LUNES A VIERNES 5:30 - 22',1,'bancolombia'),('COL6','CHICO 104',3,89.00,'06:00:00','23:00:00',4.690437,-74.051528,'America/Colombia','http://200.93.171.226:70/wsPagoXCelular','avd. 19 No 104 -37','LUNES A VIERNES 5 - 22, SABADO 7 - 22',1,'chico104'),('COL62','PEPE SIERRA',3,89.00,'06:00:00','23:00:00',4.697839,-74.046093,'America/Colombia','http://190.24.250.118:70/wsPagoXCelular','Cra 17 No 116-14','LUNES A VIERNES 7 - 3, SABADO 8 - 3, DOMINGO 12 - 3',0,'pepesierra'),('COL69','BTC',3,89.00,'06:00:00','23:00:00',4.680436,-74.043937,'America/Colombia','http://201.245.74.38:70/wsPagoXCelular','Cra 10 No 97A-13','De lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Los días sábados de 6:00 a.m. hasta 5:00 p.m.',1,'btc'),('COL70','UMB',3,89.00,'06:00:00','23:00:00',4.642882,-74.054299,'America/Colombia','http://201.245.132.242:70/wsPagoXCelular','Avda. Circunvalar No 60-00','LUNES A VIERNES 6 - 22, SABADO 6 - 19',1,'manuelabeltran'),('COL71','CL 103 # 14A- 53 BBC',3,89.00,'06:00:00','23:00:00',4.687714,-74.046992,'America/Colombia','http://201.244.58.70:70/wsPagoXCelular','Calle 103No 14 a  -53','De lunes a sábados de 6:00 a.m. hasta 10:00 p.m., Los días domingos 6:00 a.m. hasta 3:00 p.m. y Festivos 7:00 a.m. hasta 2:00 p.m',1,'bbc'),('COL74','ESTE UNIV. EXTERNADO PPAL',3,89.00,'06:00:00','23:00:00',4.595314,-74.069066,'America/Colombia','http://201.244.183.174:70/wsPagoXCelular','Calle 12 No 1-17 este','LUNES A VIERNES 5:30 - 22, SABADOS 6 - 18',1,'uexternado'),('COL77','SURTIFRUVER 85',3,89.00,'06:00:00','23:00:00',4.669686,-74.054654,'America/Colombia','http://200.75.37.50:70/wsPagoXCelular','Calle 85 No 14-05','LUNES A MIERCOLES 6 - 21, JUEVES A SABADO 6 - 3, DOMINGO 8 - 21',1,'surtifruver85'),('COL8','CAPITAL TOWER',3,89.00,'06:00:00','23:00:00',4.680051,-74.038948,'America/Colombia','http://200.71.45.20:70/wsPagoXCelular','Calle 100 No 7 - 33','LUNES A VIERNES 5:30 - 22, SABADO 6 - 14',0,'capitaltower'),('COL82','BODY COLINA',3,89.00,'06:00:00','23:00:00',4.728943,-74.066127,'America/Colombia','http://201.244.58.54:70/wsPagoXCelular','Calle 138 No 58-74','De Lunes a Jueves de 4:00 am a 10:00 pm; Viernes, Sábados, Domingos y Festivos de 6:00 am a 10:00 pm.',1,'bodytechcolina'),('COL85','BODY 166',3,89.00,'06:00:00','23:00:00',4.748339,-74.046548,'America/Colombia',NULL,'CR 23 # 166-59 BODY AUTO-NORTE','De Lunes a Jueves de 5:00 am a 11:00 pm; Viernes 5:00 am a 9:00 pm; Sábados de 7:00 am a 6:00 pm; Domingos y Festivos de 7:00 am a 4:00 pm.',0,'bodytechautopista'),('COL86','BODY CHAPINERO',3,89.00,'06:00:00','23:00:00',4.646627,-74.060495,'America/Colombia',NULL,'CR 7  No 61- 47 BODYTECH  CHAPINERO','LUNES A VIERNES 4 - 24, SABADO 6 - 24, DOMINGO 6 - 20',0,'bodytech63'),('COL9','FRISBY',3,89.00,'06:00:00','23:00:00',4.693958,-74.050839,'America/Colombia','http://201.244.187.250:70/wsPagoXCelular','avd.19 No. 106 a -45','LUNES A SABADO 6 - 22, DOMINGO 11- 20',1,'frisbv'),('COL90','ROSALES',3,89.00,'06:00:00','23:00:00',4.653001,-74.054378,'America/Colombia','http://200.93.185.54:70/wsPagoXCelular','carrera 5 No. 70A -74','De Lunes a Jueves de 6:00am a 10:00pm; Viernes y Sábado de 6:00am a 3:00am; Domingos y Festivos de 9:00am a 9:00pm.',1,'rosales'),('COL91','DAN CARLTON',3,89.00,'06:00:00','23:00:00',4.680179,-74.052948,'America/Colombia','http://200.93.172.10:70/wsPagoXCelular','Cra. 18  93B-49','LUNES A VIERNES 6 - 22, SABADO 7 - 15',1,'carrera18'),('COL92','METRO 127',3,89.00,'06:00:00','23:00:00',4.7037,-74.043628,'America/Colombia','http://190.146.236.21:70/wsPagoXCelular','Calle 127 15 A 03','24 HORAS',1,'metro127'),('COL95','MEDICAL CENTER',3,89.00,'06:00:00','23:00:00',4.6960488,-74.0322167,'America/Colombia','http://200.119.9.42:70/wsPagoXCelular','Calle 119 No. 7-14','24 HORAS',1,'medicalcenter'),('COL97','CARGO PORT',3,89.00,'06:00:00','23:00:00',4.692501,-74.13579,'America/Colombia','http://190.26.99.54:70/wsPagoXCelular','Calle 26 106 - 39','24 HORAS',1,'cargoport');
/*!40000 ALTER TABLE `zone_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `parking_history_view`
--

/*!50001 DROP TABLE IF EXISTS `parking_history_view`*/;
/*!50001 DROP VIEW IF EXISTS `parking_history_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `parking_history_view` AS select `ph`.`idhistory` AS `idhistory`,`ph`.`startTime` AS `startTime`,`ph`.`endTime` AS `endTime`,`ph`.`msisdn` AS `msisdn`,`ph`.`platform_idplatform` AS `platform_idplatform`,`ph`.`minuteValue` AS `minuteValue`,`ph`.`zone_idzone` AS `zone_idzone`,`ph`.`vehicle_plate` AS `vehicle_plate`,`ph`.`idsys_user` AS `idsys_user`,`ph`.`pay_id` AS `pay_id`,`ph`.`fee` AS `fee`,(time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60) AS `minuts`,ceiling((time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60)) AS `minutsRound`,if(((time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60) > 30),(ceiling((time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60)) * `ph`.`minuteValue`),255) AS `totalParking`,`ph`.`place_idplace` AS `place_idplace` from `parking_history` `ph` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-29 16:00:40
