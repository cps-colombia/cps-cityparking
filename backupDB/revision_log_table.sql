delimiter $$

CREATE TABLE `revision_log` (
  `revision` double NOT NULL,
  `author` varchar(145) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

