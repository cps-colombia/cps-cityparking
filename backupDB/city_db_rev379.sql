call updateRevision_log(0.379, 'juan otero', 'creada nueva tabla user_transaction');

CREATE TABLE `user_transaction` (
  `idsysuser_origen` bigint(20) NOT NULL,
  `idsysuser_dest` bigint(20) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`idsysuser_origen`,`idsysuser_dest`,`transaction_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

