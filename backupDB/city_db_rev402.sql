USE `cps_city`;

CREATE TABLE `campaign_state` (
  `idcampaign_state` int(11) NOT NULL,
  `state_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idcampaign_state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `campaign_state` (`idcampaign_state`,`state_name`) VALUES (1,'activa');
INSERT INTO `campaign_state` (`idcampaign_state`,`state_name`) VALUES (2,'finalizada');
INSERT INTO `campaign_state` (`idcampaign_state`,`state_name`) VALUES (3,'suspendida');


CREATE TABLE `campaign` (
  `id_campaign` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `range_amount_start` decimal(15,2) DEFAULT NULL,
  `range_amount_end` decimal(15,2) DEFAULT NULL,
  `amount_percent` decimal(12,2) DEFAULT NULL,
  `campaign_state` int(11) NOT NULL,
  PRIMARY KEY (`id_campaign`),
  KEY `id_campaign_state_kf_idx` (`campaign_state`),
  CONSTRAINT `id_campaign_state_kf` FOREIGN KEY (`campaign_state`) REFERENCES `campaign_state` (`idcampaign_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

USE `cps_city`;
DROP TRIGGER IF EXISTS cityparking_transaction_AUPD;
DELIMITER $$

CREATE DEFINER=`root`@`localhost` 
TRIGGER `cityparking_transaction_AUPD` AFTER UPDATE ON cityparking_transaction FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
 DECLARE percent  DECIMAL(12,2) default 0.0;
DECLARE amount_bonus  DECIMAL(12.2) default 0.0;
IF NEW.state_transaction = 'Completed' THEN
   select amount_percent into percent from campaign 
   where (now() between start_date and end_date) and
   (new.amount between range_amount_start and range_amount_end) and campaign_state=1 limit 1;
set amount_bonus := NEW.amount + ( NEW.amount * percent/100);
update sys_user set balance = balance +  amount_bonus  where idsys_user = NEW.id_sys_user;
END IF;
END