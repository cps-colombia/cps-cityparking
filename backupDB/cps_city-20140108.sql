-- MySQL dump 10.13  Distrib 5.5.28, for Linux (x86_64)
--
-- Host: localhost    Database: cps_city
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_parking`
--

DROP TABLE IF EXISTS `active_parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_parking` (
  `idactiveParking` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` datetime DEFAULT NULL,
  `currentMins` int(11) DEFAULT NULL,
  `maxMins` int(11) DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `platform_idplatform` int(11) DEFAULT NULL,
  `enableSMSNotif` int(11) DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `place_idplace` varchar(80) DEFAULT NULL,
  `idhistory` bigint(20) DEFAULT NULL,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `zone_idzone` varchar(30) NOT NULL,
  `system_date` datetime DEFAULT NULL,
  PRIMARY KEY (`idactiveParking`),
  UNIQUE KEY `vehicle_plate_UNIQUE` (`vehicle_plate`),
  KEY `fk_activeParking_platform` (`platform_idplatform`),
  KEY `fk_active_parking_parking_history` (`idhistory`),
  KEY `FK_active_parking_user` (`idsys_user`),
  KEY `fk_activeParking_zone` (`place_idplace`) USING BTREE,
  KEY `FK_active_parking_zone` (`zone_idzone`),
  KEY `currentMinsIndex` (`currentMins`),
  CONSTRAINT `fk_activeParking_platform` FOREIGN KEY (`platform_idplatform`) REFERENCES `platform` (`idplatform`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_active_parking_parking_history` FOREIGN KEY (`idhistory`) REFERENCES `parking_history` (`idhistory`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_active_parking_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`),
  CONSTRAINT `FK_active_parking_zone` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_parking`
--

LOCK TABLES `active_parking` WRITE;
/*!40000 ALTER TABLE `active_parking` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_parking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `idagent` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `country_idcountry` bigint(20) DEFAULT NULL,
  `status` char(1) DEFAULT '',
  PRIMARY KEY (`idagent`),
  KEY `fk_agent_country` (`country_idcountry`),
  CONSTRAINT `fk_agent_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent`
--

LOCK TABLES `agent` WRITE;
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `agent_bi` BEFORE INSERT ON `agent` FOR EACH ROW begin 
set NEW.pass = SHA1(NEW.pass);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `agent_push_notification`
--

DROP TABLE IF EXISTS `agent_push_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_push_notification` (
  `idagent_push_notification` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `message` longtext NOT NULL,
  `idagent` bigint(20) NOT NULL,
  `dateNotif` datetime NOT NULL,
  `isSended` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idagent_push_notification`),
  KEY `FK_agent_push_notification_agent` (`idagent`),
  CONSTRAINT `FK_agent_push_notification_agent` FOREIGN KEY (`idagent`) REFERENCES `agent` (`idagent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_push_notification`
--

LOCK TABLES `agent_push_notification` WRITE;
/*!40000 ALTER TABLE `agent_push_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_push_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_subzone`
--

DROP TABLE IF EXISTS `agent_subzone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_subzone` (
  `idAgent` bigint(20) NOT NULL,
  `idSubzone` bigint(30) NOT NULL,
  PRIMARY KEY (`idAgent`,`idSubzone`),
  KEY `FK__idx` (`idAgent`),
  KEY `FK_zona_idzona_idx` (`idSubzone`),
  CONSTRAINT `FK_agent_idagent` FOREIGN KEY (`idAgent`) REFERENCES `agent` (`idagent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_subzona_idsubzona` FOREIGN KEY (`idSubzone`) REFERENCES `subzone` (`idsubzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_subzone`
--

LOCK TABLES `agent_subzone` WRITE;
/*!40000 ALTER TABLE `agent_subzone` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_subzone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_zone`
--

DROP TABLE IF EXISTS `agent_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_zone` (
  `idZone` varchar(30) NOT NULL,
  `idAgent` bigint(20) NOT NULL,
  PRIMARY KEY (`idZone`,`idAgent`),
  KEY `FK_agent_zone_user` (`idAgent`) USING BTREE,
  CONSTRAINT `FK_agent_zone_agent` FOREIGN KEY (`idAgent`) REFERENCES `agent` (`idagent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_zone`
--

LOCK TABLES `agent_zone` WRITE;
/*!40000 ALTER TABLE `agent_zone` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card` (
  `PIN` varchar(255) NOT NULL,
  `activationDate` datetime DEFAULT NULL,
  `price` decimal(7,2) NOT NULL,
  `used` int(11) NOT NULL DEFAULT '0',
  `idsys_user` bigint(20) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  PRIMARY KEY (`PIN`),
  KEY `fk_card_sys_user` (`idsys_user`),
  CONSTRAINT `fk_card_sys_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES ('8bb0b97698f489d41b6955a46383fa1f2d9001c5',NULL,20000.00,0,NULL,'COL');
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `passToSHA1` BEFORE INSERT ON `card` FOR EACH ROW begin 
set NEW.PIN = SHA1(NEW.PIN);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `card_virtual`
--

DROP TABLE IF EXISTS `card_virtual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_virtual` (
  `PIN` varchar(255) NOT NULL,
  `activationDate` datetime DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `used` int(11) NOT NULL DEFAULT '0',
  `idsys_user` bigint(20) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  PRIMARY KEY (`PIN`),
  KEY `fk_card_virtual_sys_user` (`idsys_user`),
  CONSTRAINT `fk_card_virtual_sys_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_virtual`
--

LOCK TABLES `card_virtual` WRITE;
/*!40000 ALTER TABLE `card_virtual` DISABLE KEYS */;
/*!40000 ALTER TABLE `card_virtual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `idcity` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `idcountry` bigint(20) NOT NULL,
  PRIMARY KEY (`idcity`),
  KEY `FK_city_country` (`idcountry`),
  CONSTRAINT `FK_city_country` FOREIGN KEY (`idcountry`) REFERENCES `country` (`idcountry`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (103,'Bogotá',472),(104,'Leticia',472),(105,'Medellin',472),(106,'Arauca',472),(107,'Barranquilla',472),(108,'Cartagena',472),(109,'Tunja',472),(110,'Manizales',472),(111,'Florencia',472),(112,'Yopal',472),(113,'Popayán',472),(114,'Valledupar',472),(115,'Quibdó',472),(116,'Monteria',472),(117,'Inírida',472),(118,'Guaviare',472),(119,'Neiva',472),(120,'Rioacha',472),(121,'Santa Marta',472),(122,'Villavicencio',472),(123,'Pasto',472),(124,'Cúcuta',472),(125,'Mocoa',472),(126,'Armenia',472),(127,'Pereira',472),(128,'San Andrés',472),(129,'Bucaramanga',472),(130,'Sincelejo',472),(131,'Ibagué',472),(132,'Cali',472),(133,'Mitú',472),(134,'Puerto Carreño',472);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cityparking_transaction`
--

DROP TABLE IF EXISTS `cityparking_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cityparking_transaction` (
  `id_transaction` varchar(20) NOT NULL,
  `date_transaction` datetime NOT NULL,
  `id_sys_user` bigint(20) NOT NULL,
  `email_sys_user` varchar(70) DEFAULT NULL,
  `state_transaction` varchar(150) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `idzone` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cityparking_transaction`
--

LOCK TABLES `cityparking_transaction` WRITE;
/*!40000 ALTER TABLE `cityparking_transaction` DISABLE KEYS */;
INSERT INTO `cityparking_transaction` VALUES ('0','2014-06-06 13:05:02',881,'apereira@gmail.com','Rejected',2000.00,'CO',NULL),('10345775104','2014-06-05 09:35:16',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('1048576','2014-04-14 08:16:26',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('1074266112','2014-04-14 08:33:38',936,'oswaldoaponte23@gmail.com','Completed',25000.00,'CO',NULL),('1076887552','2014-06-30 22:39:20',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('1077333155027204535','2014-07-22 19:15:28',1349,'drywallbogota@gmail.com','Rejected',50000.00,'CO',NULL),('1082130432','2014-06-04 14:09:36',950,'operaciones11@city-parking.com','Rejected',25000.00,'CO',NULL),('1082654720','2014-04-14 07:58:35',893,'jharrylopez@gmail.com','Rejected',20000.00,'CO',NULL),('1099528404992','2014-03-27 14:08:45',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1099614388224','2014-05-30 18:15:33',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1099712954368','2014-04-04 07:50:03',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1099796840448','2014-01-16 09:41:47',881,'city1@mail.com','Rejected',15000.00,'CO',NULL),('1099832492032','2014-06-06 15:49:41',970,'gerenciadesarrollo@city-parking.com','Rejected',1000000.00,'CO',NULL),('1100050595840','2014-06-05 21:01:59',973,'osierrasoporte@gmail.com','Rejected',12345.00,'CO',NULL),('1100065275904','2014-01-17 11:59:04',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1100065800192','2014-04-02 21:53:34',917,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('1100141821952','2014-06-16 11:19:52',1076,'dianin0331@gmail.com','Completed',50000.00,'CO',NULL),('1100151914496','2014-02-12 14:54:46',881,'testmail@gmail.com','Completed',10000.00,'CO',NULL),('1100803538944','2014-06-27 14:25:43',1169,'vdiazmoreno@hotmail.com','Rejected',200000.00,'CO',NULL),('1100937691136','2014-06-25 16:23:33',1158,'yecidfuentes@hotmail.com','Rejected',30000.00,'CO',NULL),('1101411123200','2014-06-06 16:08:04',970,'gerenciadesarrollo@city-parking.com','Rejected',1000000.00,'CO',NULL),('1101677002752','2014-06-25 09:42:53',1046,'aguirres1@gmail.com','Rejected',100000.00,'CO',NULL),('1101717831680','2014-01-16 09:42:25',881,'city1@mail.com','Completed',15000.00,'CO',NULL),('1101793329152','2014-06-23 17:57:24',1144,'mateoaristi@gmail.com','Rejected',20000.00,'CO',NULL),('1102196572160','2014-06-29 13:46:04',1138,'ramirezfabian@gmail.com','Rejected',100.00,'CO',NULL),('1102758543360','2014-06-25 09:50:48',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('1103816032256','2014-07-05 20:46:54',979,'ivan001433@gmail.com','Rejected',25000.00,'CO',NULL),('1103823372288','2013-12-30 17:31:04',885,'anubis_ex@hotmail.com','Rejected',20000.00,'CO',NULL),('1103825010688','2014-07-04 07:38:58',1051,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('1103843295232','2013-12-19 11:30:33',888,'andres.pereirap@gmail.com','Rejected',100000.00,'CO',NULL),('1103940812800','2014-05-01 14:37:09',929,'operaciones3@city-parking.com','Completed',25000.00,'CO',NULL),('1103957786624','2014-04-23 08:35:29',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1106013061120','2014-01-16 10:14:14',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1106147082240','2014-01-16 10:00:53',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('1108110082048','2014-01-16 10:10:23',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1108126728192','2013-12-18 16:05:21',888,'andres.pereirap@gmail.com','Rejected',100000.00,'CO',NULL),('1108235911168','2014-06-18 06:53:45',1083,'d.castillo@hillebrandgroup.com','Rejected',50000.00,'CO',NULL),('1108253868032','2014-01-31 11:16:13',881,'gmail1@gmail.com','Rejected',10000.00,'CO',NULL),('1108272611328','2014-05-23 18:57:31',948,'adriana','Rejected',20000.00,'CO',NULL),('1108303020032','2014-05-30 15:21:58',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1108664713216','2014-06-06 08:26:48',974,'karolinafloyd@gmail.com','Rejected',30.00,'CO',NULL),('1108673101824','2014-06-18 09:32:03',1113,'gondulamarina84@hotmail.com','Rejected',100000.00,'CO',NULL),('1108680572928','2014-06-29 06:04:33',1155,'laurajpinzonr@gmail.com','Completed',100000.00,'CO',NULL),('1109343666176','2014-06-27 17:13:55',1169,'vdiazmoreno@hotmail.com','Rejected',300000.00,'CO',NULL),('1110268641280','2014-06-16 11:13:39',1076,'dianin0331@gmail.com','Rejected',17366.00,'CO',NULL),('1110400106496','2014-06-14 18:53:22',948,'adriana','Rejected',10000.00,'CO',NULL),('1110520102912','2014-01-28 20:43:46',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1111323901952','2014-06-26 13:07:58',1155,'laurajpinzonr@gmail.com','Rejected',100000.00,'CO',NULL),('1111474962432','2014-07-03 12:54:20',1051,'andres.pereirap@gmail.com','Rejected',22.00,'CO',NULL),('1112398692352','2013-12-30 15:30:15',885,'anubis_ex@hotmail.com','Completed',20000.00,'CO',NULL),('1112667062272','2014-07-08 12:19:22',965,'scabrera@cpsparking.ca','Rejected',1000.00,'CO',NULL),('1113473482752','2014-01-28 20:53:24',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('11148656640','2014-06-13 21:55:10',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('1116783771648','2014-06-16 11:14:34',1076,'dianin0331@gmail.com','Rejected',50000.00,'CO',NULL),('1116825714688','2014-03-04 06:14:37',881,'testmail@gmail.com','Rejected',50000.00,'CO',NULL),('1116961767424','2014-06-18 19:28:18',1113,'gondulamarina84@hotmail.com','Completed',20000.00,'CO',NULL),('1116963340288','2014-05-27 04:24:04',945,'johnamaya02@gmail.com','Rejected',10000.00,'CO',NULL),('1117329096704','2014-06-16 17:57:43',1095,'fabio_navarro@hotmail.com','Completed',30000.00,'CO',NULL),('1117397450752','2014-05-23 18:58:16',948,'adriana','Rejected',10000.00,'CO',NULL),('1117765566464','2014-06-21 19:16:37',1139,'mauricio.clavijob@gmail.com','Rejected',30000.00,'CO',NULL),('1117832347648','2014-06-25 16:28:49',1158,'yecidfuentes@hotmail.com','Completed',30000.00,'CO',NULL),('1117924884480','2014-06-27 17:06:16',1169,'vdiazmoreno@hotmail.com','Rejected',300000.00,'CO',NULL),('1117968924672','2014-05-05 10:34:26',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1118059364352','2014-06-25 09:38:11',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('1118841602048','2014-06-25 09:38:33',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('1118855757824','2014-06-11 18:58:53',1039,'pc.carlos.89@gmail.com','Rejected',10000.00,'CO',NULL),('1118856282112','2014-06-09 08:52:26',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1119069470720','2014-06-11 19:42:37',1042,'marinesluna@hotmail.com','Rejected',10000.00,'CO',NULL),('1119293276160','2014-06-27 17:06:45',1169,'vdiazmoreno@hotmail.com','Rejected',300000.00,'CO',NULL),('1119392825344','2014-03-25 08:31:58',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1119654772736','2014-06-20 17:46:52',1132,'camilo.ruiz.diaz@gmail.com','Rejected',15000.00,'CO',NULL),('1120098385920','2014-06-24 12:14:15',1135,'javiersn79@gmail.com','Rejected',20000.00,'CO',NULL),('1125290868736','2014-05-28 10:08:32',909,'ebayon@city-parking.com','Rejected',22000.00,'CO',NULL),('1125593120768','2014-06-25 09:49:00',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('1125602295808','2014-06-06 18:25:50',909,'ebayon@city-parking.com','Rejected',20000.00,'CO',NULL),('1125610815488','2014-06-25 09:47:44',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('1125684215808','2014-06-20 05:26:46',935,'operaciones2@city-parking.com','Rejected',10000.00,'CO',NULL),('1125701124096','2014-06-26 22:21:03',1165,'nectar.jhon@hotmail.com','Rejected',50000.00,'CO',NULL),('1125820071936','2014-05-23 19:01:45',948,'adriana','Rejected',10000.00,'CO',NULL),('1125836390400','2014-06-16 17:50:03',1096,'william.avila@gmail.com','Rejected',30000.00,'CO',NULL),('1128787869696','2014-06-26 12:53:52',1167,'fedfercar@hotmail.com','Rejected',10000.00,'CO',NULL),('1149239296','2014-06-05 09:31:46',972,'alveiro1612@hotmail.com','Rejected',10.00,'CO',NULL),('11542724608','2014-06-30 21:50:38',1048,'lachaves@gmail.com','Rejected',50000.00,'CO',NULL),('11577327616','2014-06-20 10:55:07',948,'adrianag1807@hotmail.com','Rejected',50000.00,'CO',NULL),('1159972160907986813','2014-07-16 13:18:02',1297,'mimoralesmo@yahoo.com','Rejected',50000.00,'CO',NULL),('1277689856','2014-06-08 10:05:02',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('12953780224','2014-01-16 12:31:18',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('1337298451623031328','2014-07-13 22:29:23',1302,'jorge.arenas@etb.net.co','Completed',50000.00,'CO',NULL),('1345323008','2014-04-11 15:30:33',930,'operaciones5@city-parking.com','Rejected',25000.00,'CO',NULL),('134742016','2014-06-04 14:53:02',950,'operaciones11@city-parking.com','Completed',25000.00,'CO',NULL),('1374389534720','2014-01-30 17:49:47',881,'aljurado@gmail.com','Completed',5000.00,'CO',NULL),('1374418894848','2014-02-06 10:19:48',881,'testmail@gmail.com','Rejected',29000.00,'CO',NULL),('1374445699072','2014-06-26 21:23:02',1046,'aguirres1@gmail.com','Completed',50000.00,'CO',NULL),('1374476566528','2014-06-14 20:51:43',1081,'rosamaliazadal@hotmail.com','Completed',50000.00,'CO',NULL),('1374563663872','2014-06-20 18:52:47',1132,'camilo.ruiz.diaz@gmail.com','Rejected',20000.00,'CO',NULL),('1374628675584','2014-05-30 17:05:36',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1374968414208','2014-06-28 12:42:35',1178,'matteogonzato@hotmail.com','Rejected',20.00,'CO',NULL),('1375484248064','2014-05-30 16:57:14',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('1376016924672','2014-06-29 14:38:09',1138,'ramirezfabian@gmail.com','Completed',100000.00,'CO',NULL),('1376828129280','2014-05-01 13:54:53',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1377074413568','2014-04-29 15:36:16',944,'misagars@hotmail.com','Rejected',25000.00,'CO',NULL),('1377250050048','2014-06-20 18:52:22',1132,'camilo.ruiz.diaz@gmail.com','Rejected',15000.00,'CO',NULL),('1377632256000','2014-06-26 21:22:46',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('1378175942656','2014-06-28 13:25:59',1113,'gondulamarina84@hotmail.com','Completed',30000.00,'CO',NULL),('1378684502016','2013-12-19 12:09:47',888,'andres.pereirap@gmail.com','Completed',10000.00,'CO',NULL),('1378726445056','2013-12-19 12:22:47',888,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('1378856140800','2014-05-05 04:43:31',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1379963764736','2014-05-05 11:06:02',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1381658263552','2014-05-02 08:41:08',946,'aaa@a.cl','Rejected',12222.00,'CO',NULL),('1381963923456','2014-05-05 04:42:07',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('1383060865024','2014-06-13 18:50:13',1075,'etnarca@yahoo.com','Rejected',20000.00,'CO',NULL),('1383069253632','2013-12-18 15:51:48',885,'anubis_ex@hotmail.com','Completed',200.00,'CO',NULL),('1383174504448','2013-12-19 12:28:40',888,'andres.pereirap@gmail.com','Completed',10000.00,'CO',NULL),('1383248560128','2014-05-26 19:46:53',948,'adriana','Rejected',10000.00,'CO',NULL),('1383319339008','2014-06-21 10:28:54',1135,'javiersn79@gmail.com','Rejected',10000.00,'CO',NULL),('1383549042688','2014-06-17 07:10:39',948,'adrianag1807@hotmail.com','Rejected',10000.00,'CO',NULL),('1383603372032','2014-07-02 08:19:52',965,'scabrera@cpsparking.ca','Rejected',10000.00,'CO',NULL),('1383932755968','2014-06-20 18:27:35',1114,'dujandroid@gmail.com','Rejected',2748.00,'CO',NULL),('1384069988352','2013-12-30 16:39:55',885,'anubis_ex@hotmail.com','Completed',20000.00,'CO',NULL),('1385162276864','2014-06-12 05:34:43',1048,'lachaves@gmail.com','Rejected',50000.00,'CO',NULL),('1385195175936','2014-06-13 07:12:38',1068,'ndmorame@hotmail.com','Rejected',50000.00,'CO',NULL),('1385213067264','2014-01-27 15:44:58',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('1385684795392','2014-01-30 17:43:56',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('1386473324544','2014-06-25 17:53:31',1159,'ridiazba3@gmail.com','Rejected',50000.00,'CO',NULL),('1387316445184','2013-12-19 12:07:19',888,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('1387430739968','2013-12-19 12:07:18',888,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('1391573860352','2014-05-26 19:47:20',948,'adriana','Rejected',20000.00,'CO',NULL),('1391599550464','2014-07-04 08:34:15',1051,'andres.pereirap@gmail.com','Rejected',10.00,'CO',NULL),('1391644901376','2014-06-16 11:40:46',1091,'helmancollante@gmail.com','Rejected',20.00,'CO',NULL),('1391651192832','2014-05-16 11:02:29',950,'operaciones11@city-parking.com','Rejected',5000.00,'CO',NULL),('1391860908032','2014-05-01 14:05:15',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1391902851072','2014-06-08 08:41:47',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1391948201984','2014-05-26 03:54:45',948,'adriana','Rejected',10000.00,'CO',NULL),('1392107651072','2014-02-11 09:59:26',893,'jharrylopez@gmail.com','Completed',50000.00,'CO',NULL),('1392124362752','2014-05-01 13:56:52',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1392732143616','2014-02-18 08:04:22',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('1393805230080','2014-04-23 01:22:07',941,'octavio.herrera@gmail.com','Rejected',50000.00,'CO',NULL),('1393968545792','2014-06-27 15:49:09',1169,'vdiazmoreno@hotmail.com','Rejected',200000.00,'CO',NULL),('1394256642048','2014-05-01 14:32:21',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1394606080000','2014-05-01 06:40:20',945,'johnamaya02@gmail.com','Rejected',25000.00,'CO',NULL),('1394858000384','2014-06-22 15:22:54',1043,'torradoj@hotmail.com','Rejected',50000.00,'CO',NULL),('1395881934848','2014-05-01 13:42:32',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('1395885604864','2014-04-15 06:21:14',939,'edrojal777@gmail.com','Rejected',2000.00,'CO',NULL),('1396132806656','2014-07-07 09:42:39',1241,'cfeliper@hotmail.com','Rejected',6000.00,'CO',NULL),('1396478574592','2014-04-03 07:53:28',917,'aljurado@gmail.com','Completed',10000.00,'CO',NULL),('1398034202624','2014-04-23 01:21:04',941,'octavio.herrera@gmail.com','Rejected',50000.00,'CO',NULL),('1398055305216','2014-04-23 12:27:25',909,'ebayon@city-parking.com','Completed',20000.00,'CO',NULL),('1400256790528','2014-06-16 11:52:49',1091,'helmancollante@gmail.com','Rejected',20.00,'CO',NULL),('1400457920512','2014-06-25 10:14:06',1155,'laurajpinzonr@gmail.com','Rejected',50000.00,'CO',NULL),('1400495013888','2014-06-13 09:17:48',1049,'yestrigos@gmail.com','Rejected',8000.00,'CO',NULL),('1402349682688','2014-06-11 10:59:39',978,'sharly841022@hotmail.com','Rejected',25000.00,'CO',NULL),('1402447921152','2014-06-12 06:13:59',1049,'yestrigos@gmail.com','Rejected',50000.00,'CO',NULL),('14026604544','2013-12-30 10:46:36',885,'anubis_ex@hotmail.com','Completed',2000.00,'CO',NULL),('142606336','2014-06-04 14:10:27',948,'adriana','Rejected',10000.00,'CO',NULL),('143982740012093049','2014-07-14 19:47:16',1169,'vdiazmoreno@hotmail.com','Completed',300000.00,'CO',NULL),('146895618934094303','2014-07-16 12:52:54',1297,'mimoralesmo@yahoo.com','Rejected',30000.00,'CO',NULL),('1480550239434355732','2014-07-31 11:13:52',1611,'freddy.leon@unimilitar.edu.co','Rejected',10000.00,'CO',NULL),('1491853519014246558','2014-07-30 15:13:13',1348,'mapinz@gmail.com','Rejected',5000.00,'CO',NULL),('1532501264374871892','2014-07-28 19:44:37',1613,'emendez21@hotmail.com','Rejected',20000.00,'CO',NULL),('15502213120','2014-01-16 12:31:57',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('1613234176','2014-06-05 09:33:35',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('1617214489424840646','2014-07-25 08:40:02',1349,'drywallbogota@gmail.com','Rejected',50000.00,'CO',NULL),('1644691456','2014-06-03 10:39:19',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1682087782219233424','2014-07-29 23:04:57',1617,'ferbarbosa27@gmail.com','Started',5000.00,'CO',NULL),('1713746246029165835','2014-07-22 20:36:01',1353,'fachs2007@hotmail.com','Rejected',30000.00,'CO',NULL),('17180786688','2014-04-14 08:22:55',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17180917760','2014-04-14 08:22:55',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17181048832','2014-04-14 08:22:56',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17183277056','2014-06-10 08:44:01',970,'gerenciadesarrollo@city-parking.com','Completed',1000000.00,'CO',NULL),('17189437440','2014-04-14 08:22:56',937,'jperez@gmail.com','Rejected',20000.00,'CO',NULL),('17213489152','2014-05-28 05:09:46',945,'johnamaya02@gmail.com','Rejected',10000.00,'CO',NULL),('17213685760','2014-06-03 10:34:37',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('17223385088','2014-04-12 09:46:42',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1723496287474859826','2014-07-24 11:13:51',1140,'cesarortiz77@gmail.com','Completed',50000.00,'CO',NULL),('17257070592','2014-02-07 08:51:36',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('17281122304','2014-06-16 15:37:46',948,'operaciones10@city-parking.com','Rejected',10000.00,'CO',NULL),('17281908736','2014-06-16 15:28:45',948,'operaciones10@city-parking.com','Rejected',25000.00,'CO',NULL),('17315528704','2014-02-07 18:46:45',881,'testmail@gmail.com','Rejected',10.00,'CO',NULL),('17322475520','2014-02-07 08:52:18',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('17448304640','2014-07-07 12:18:49',950,'operaciones11@city-parking.com','Rejected',25000.00,'CO',NULL),('17448566784','2014-04-12 09:44:41',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('17456955392','2014-04-12 09:53:19',934,'maxfutura1782@gmail.com','Completed',25000.00,'CO',NULL),('17583374336','2014-06-13 19:45:51',1075,'etnarca@yahoo.com','Rejected',50000.00,'CO',NULL),('17759993856','2014-06-16 15:27:51',948,'adriana','Rejected',25000.00,'CO',NULL),('17862033408','2014-06-05 09:37:11',971,'ggh@city-parking.com','Completed',25000.00,'CO',NULL),('18021613568','2014-07-01 07:45:21',970,'gerenciadesarrollo@city-parking.com','Completed',500000.00,'CO',NULL),('18253938688','2014-05-31 08:00:55',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('183176904698184008','2014-07-31 11:08:27',1611,'freddy.leon@unimilitar.edu.co','Completed',30000.00,'CO',NULL),('1837596251887912029','2014-07-30 14:23:11',1623,'andreifu@gmail.com','Started',1000.00,'CO',NULL),('1851987188961491023','2014-07-11 09:11:59',1284,'heyderandreachamorro@gmail.com','Rejected',150000.00,'CO',NULL),('18624151552','2014-04-12 09:47:10',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('1867742397997075463','2014-07-30 08:59:14',1051,'andres.pereirap@gmail.com','Started',10000.00,'CO',NULL),('1879179264','2014-03-25 10:11:09',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('18798870528','2014-06-05 09:20:25',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('18824036352','2014-06-03 10:34:27',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('18858639360','2014-06-05 16:19:19',964,'operaciones4@city-parking.com','Rejected',25000.00,'CO',NULL),('18924961792','2014-06-04 14:14:44',948,'adriana','Rejected',20000.00,'CO',NULL),('19531038720','2014-06-11 12:17:19',1035,'andresnino@yahoo.com','Rejected',5000.00,'CO',NULL),('19976159232','2014-06-16 15:55:56',948,'operaciones10@city-parking.com','Rejected',25000.00,'CO',NULL),('20140721116000005','2014-07-21 03:40:13',893,'jharrylopez@gmail.com','Completed',1000.00,'COL','COL116'),('20140721116000006','2014-07-21 05:25:00',893,'jharrylopez@gmail.com','Completed',100.00,'COL','COL116'),('2014072171000001','2014-07-21 04:42:38',893,'jharrylopez@gmail.com','Completed',100.00,'COL','COL71'),('2014072171000002','2014-07-21 04:47:42',893,'jharrylopez@gmail.com','Completed',100.00,'COL','COL71'),('2014072171000003','2014-07-21 05:05:50',893,'jharrylopez@gmail.com','Completed',10.00,'COL','COL71'),('2014072177000002','2014-07-21 03:41:33',893,'jharrylopez@gmail.com','Completed',1100.00,'COL','COL77'),('20140722157000001','2014-07-22 11:41:57',893,'jharrylopez@gmail.com','Completed',100.00,'COL','COL157'),('2014072337000001','2014-07-23 01:56:36',893,'jharrylopez@gmail.com','Completed',100.00,'COL','COL37'),('2014072337000002','2014-07-23 01:57:47',929,'operaciones3@city-parking.com','Completed',1000.00,'COL','COL37'),('2014072370000001','2014-07-23 04:53:24',893,'jharrylopez@gmail.com','Completed',110.00,'COL','COL70'),('2014072377000001','2014-07-23 10:34:04',940,'milenavw@hotmail.com','Completed',1100.00,'COL','COL77'),('2014072377000004','2014-07-23 10:38:03',940,'milenavw@hotmail.com','Completed',1100.00,'COL','COL77'),('2014072577000002','2014-07-25 10:02:39',913,'soniayolima@gmail.com','Completed',110.00,'COL','COL77'),('2014072577000003','2014-07-25 03:20:54',893,'jharrylopez@gmail.com','Completed',150.00,'COL','COL77'),('2014072577000004','2014-07-25 04:01:24',893,'jharrylopez@gmail.com','Completed',150.00,'COL','COL77'),('2014072877000001','2014-07-28 09:24:17',893,'jharrylopez@gmail.com','Completed',150.00,'COL','COL77'),('2014072877000002','2014-07-28 03:17:59',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014072877000003','2014-07-28 03:21:07',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014072977000001','2014-07-29 09:19:58',940,'milenavw@hotmail.com','Completed',150.00,'COL','COL77'),('2014072977000002','2014-07-29 09:51:37',940,'milenavw@hotmail.com','Completed',150.00,'COL','COL77'),('2014072977000003','2014-07-29 03:22:28',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014072977000004','2014-07-29 03:49:32',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014073077000001','2014-07-30 09:25:12',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014073077000002','2014-07-30 09:29:47',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014073077000003','2014-07-30 10:29:04',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014073077000004','2014-07-30 03:46:17',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2014073177000002','2014-07-31 05:25:19',950,'operaciones11@city-parking.com','Completed',150.00,'COL','COL77'),('2014073177000003','2014-07-31 05:38:30',917,'aljurado@gmail.com','Completed',150.00,'COL','COL77'),('2075374081750973729','2014-07-12 18:55:27',1169,'vdiazmoreno@hotmail.com','Rejected',300000.00,'CO',NULL),('21575761920','2014-04-21 15:00:52',940,'milenavw@hotmail.com','Completed',25000.00,'CO',NULL),('2157969408','2014-06-10 10:33:27',908,'caloman@hotmail.com','Completed',25000.00,'CO',NULL),('2166578696993421277','2014-07-11 19:29:36',1293,'Fyquis@gmail.com','Rejected',50000.00,'CO',NULL),('2183135232','2014-07-01 07:42:25',970,'gerenciadesarrollo@city-parking.com','Rejected',500000.00,'CO',NULL),('2214592512','2014-06-16 06:48:15',1086,'fernansala0526@gmail.com','Rejected',10000.00,'CO',NULL),('2217803776','2014-06-10 08:41:22',970,'gerenciadesarrollo@city-parking.com','Rejected',1000000.00,'CO',NULL),('2219785089090537953','2014-07-22 20:30:53',1353,'fachs2007@hotmail.com','Rejected',50000.00,'CO',NULL),('22616539136','2014-04-11 22:26:38',935,'operaciones2@city-parking.com','Rejected',10000.00,'CO',NULL),('2316369920','2014-06-11 05:39:23',980,'stevenc_07@hotmail.com','Rejected',20.00,'CO',NULL),('2328360223612780509','2014-07-30 11:20:27',1622,'oromenu@gmail.com','Started',20000.00,'CO',NULL),('2350383104','2014-06-14 15:12:36',1075,'etnarca@yahoo.com','Rejected',50000.00,'CO',NULL),('23622385664','2014-04-21 14:58:52',940,'milenavw@hotmail.com','Rejected',25000.00,'CO',NULL),('23894228992','2014-04-29 19:26:36',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('2415984640','2014-06-13 19:32:58',1075,'etnarca@yahoo.com','Rejected',50000.00,'CO',NULL),('2417073150219731686','2014-07-28 08:56:25',1051,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('2572040150036136666','2014-07-30 15:15:07',1348,'mapinz@gmail.com','Started',1200.00,'CO',NULL),('25769803776','2014-06-05 09:20:37',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('25769934848','2014-06-03 10:36:16',881,'apereira@gmail.com','Rejected',15000.00,'CO',NULL),('25772097536','2014-06-24 17:12:38',1043,'torradoj@hotmail.com','Completed',50000.00,'CO',NULL),('25778192384','2014-06-10 10:04:37',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('25804931072','2014-06-18 21:15:33',1119,'acostaf0510@Gmail.com','Rejected',10000.00,'CO',NULL),('25912672256','2014-06-04 14:16:59',948,'adriana','Rejected',20000.00,'CO',NULL),('25971261440','2014-06-05 09:56:39',972,'alveiro1612@hotmail.com','Rejected',10.00,'CO',NULL),('26038763520','2014-06-12 18:27:28',1064,'constanza.guerrero@hotmail.com','Rejected',20000.00,'CO',NULL),('26105872384','2014-06-25 14:18:38',1140,'cesarortiz77@gmail.com','Rejected',100000.00,'CO',NULL),('2619415402267624066','2014-07-23 08:30:15',1364,'dmedinap@bancolombia.com.co','Completed',50000.00,'CO',NULL),('26316242944','2014-06-04 14:43:51',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('26316374016','2014-06-06 13:09:40',881,'apereira@gmail.com','Rejected',40.00,'CO',NULL),('26340229120','2014-06-08 18:37:07',948,'adriana','Rejected',10000.00,'CO',NULL),('265867326799431952','2014-07-31 16:54:23',1634,'carloshhincapie@yahoo.es','Rejected',2000.00,'CO',NULL),('268435456','2014-01-20 09:31:22',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('26843807744','2014-06-04 14:16:13',948,'adriana','Rejected',20000.00,'CO',NULL),('26852720640','2014-05-28 05:21:02',945,'johnamaya02@gmail.com','Completed',10000.00,'CO',NULL),('2686976000','2014-06-30 22:40:51',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('26911506432','2014-06-05 09:22:29',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('2693791744','2014-06-17 10:02:28',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('27380613120','2014-06-04 14:17:59',950,'operaciones11@city-parking.com','Rejected',25000.00,'CO',NULL),('27381465088','2014-06-04 14:44:12',950,'operaciones11@city-parking.com','Rejected',25000.00,'CO',NULL),('27389132800','2014-06-05 09:30:47',971,'ggh@city-parking.com','Rejected',25000.00,'CO',NULL),('274877906944','2014-06-26 16:16:06',1140,'cesarortiz77@gmail.com','Completed',50000.00,'CO',NULL),('274953404416','2014-06-17 20:13:45',1039,'pc.carlos.89@gmail.com','Rejected',10000.00,'CO',NULL),('275158925312','2014-06-14 17:39:49',1043,'torradoj@hotmail.com','Rejected',20000.00,'CO',NULL),('275422117888','2014-06-16 14:48:29',948,'adriana','Rejected',25000.00,'CO',NULL),('275956367360','2014-06-21 15:33:37',1137,'augustorojas@yahoo.com','Rejected',10000.00,'CO',NULL),('275964362752','2014-04-12 10:52:37',935,'operaciones2@city-parking.com','Rejected',1000.00,'CO',NULL),('276289290240','2014-06-07 06:59:48',976,'city4@mail.com','Rejected',1000.00,'CO',NULL),('277058945024','2014-06-09 15:03:18',917,'aljurado@gmail.com','Rejected',25000.00,'CO',NULL),('277066285056','2014-06-10 09:49:57',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('277096693760','2014-04-21 08:46:32',909,'ebayon@city-parking.com','Rejected',50000.00,'CO',NULL),('277365391360','2014-01-27 10:15:13',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('277436432384','2014-06-26 09:27:09',1164,'jeyson.jimenez@gmail.com','Rejected',50000.00,'CO',NULL),('278541631488','2014-01-28 17:07:58',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('278669623296','2014-06-29 18:52:09',1139,'mauricio.clavijob@gmail.com','Rejected',50000.00,'CO',NULL),('279239983104','2013-12-18 10:09:42',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('279278780416','2013-12-18 11:02:17',890,'gerenciadesarrollo@city-parking.com','Rejected',200.00,'CO',NULL),('279451860992','2014-07-08 10:50:06',1051,'andres.pereirap@gmail.com','Rejected',1000.00,'CO',NULL),('279621992448','2014-01-28 17:08:05',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('279710793728','2014-04-14 07:33:20',913,'soniayolima@gmail.com','Rejected',25000.00,'CO',NULL),('280559091712','2014-01-28 17:08:28',881,'city1@mail.com','Rejected',10000.00,'CO',NULL),('28185722880','2014-06-24 17:12:16',1043,'torradoj@hotmail.com','Rejected',50000.00,'CO',NULL),('283468890112','2014-06-04 14:02:46',964,'operaciones4@city-parking.com','Rejected',25000.00,'CO',NULL),('283473805312','2014-06-07 06:44:07',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('283501527040','2014-06-26 00:00:51',1162,'odslovera@hotmail.com','Rejected',20000.00,'CO',NULL),('283506245632','2014-06-09 14:55:49',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('283539144704','2013-12-18 10:10:52',881,'city1@mail.com','Rejected',300000.00,'CO',NULL),('283539275776','2013-12-18 10:10:53',881,'city1@mail.com','Rejected',300000.00,'CO',NULL),('283736276992','2014-07-08 10:50:37',1051,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('283737849856','2014-06-07 06:43:38',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('283742699520','2014-07-07 06:52:40',949,'joalex9226@yahoo.com','Rejected',200000.00,'CO',NULL),('283870625792','2014-07-08 20:36:00',1248,'tuliocarrerosoto@hotmail.com','Rejected',50000.00,'CO',NULL),('284009955328','2014-01-31 07:55:26',881,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('284042592256','2014-01-08 09:43:15',891,'n_a_n_o84@hotmail.com','Completed',20000.00,'CO',NULL),('284140371968','2014-01-31 07:55:27',881,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('284277473280','2014-01-20 10:26:05',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('284541714432','2014-01-08 09:38:30',891,'n_a_n_o84@hotmail.com','Completed',50000.00,'CO',NULL),('284546957312','2014-06-04 15:39:13',965,'scabrera@cpsparking.ca','Rejected',20.00,'CO',NULL),('284684189696','2014-06-04 15:39:45',966,'gcabrera@cpsparking.ca','Rejected',20.00,'CO',NULL),('284790620160','2014-05-30 15:14:00',950,'operaciones11@city-parking.com','Rejected',25.00,'CO',NULL),('284847898624','2014-01-08 10:22:47',885,'anubis_ex@hotmail.com','Rejected',200.00,'CO',NULL),('285079633920','2014-06-04 15:39:22',965,'scabrera@cpsparking.ca','Rejected',20000.00,'CO',NULL),('285082648576','2014-06-07 06:51:40',976,'city4@mail.com','Rejected',10000.00,'CO',NULL),('285180166144','2014-06-07 06:43:30',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('285626859520','2014-06-17 11:59:08',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('285629087744','2014-06-17 11:58:19',1063,'santiago.ospina@gmail.com','Rejected',20000.00,'CO',NULL),('285750329344','2014-01-31 07:55:51',881,'aljurado@gmail.com','Completed',100000.00,'CO',NULL),('286120869888','2014-06-21 08:44:38',1134,'svega4536@hotmail.com','Completed',10000.00,'CO',NULL),('286185881600','2014-06-29 18:52:58',1139,'mauricio.clavijob@gmail.com','Rejected',50000.00,'CO',NULL),('2877154703404453393','2014-07-31 07:35:55',1049,'yestrigos@gmail.com','Rejected',30000.00,'CO',NULL),('287763398656','2013-12-18 10:20:54',888,'andres.pereirap@gmail.com','Rejected',120000.00,'CO',NULL),('288077578240','2014-07-08 10:34:00',1195,'city8@mail.com','Rejected',10000.00,'CO',NULL),('288142458880','2014-07-08 10:52:00',965,'scabrera@cpsparking.ca','Completed',5000.00,'CO',NULL),('2884481165631507042','2014-07-31 13:17:29',1633,'cristinasalazarceballos@yahoo.com','Completed',50000.00,'CO',NULL),('288842579968','2014-01-31 07:56:35',881,'aljurado@gmail.com','Rejected',10000.00,'CO',NULL),('290448408576','2014-01-20 10:45:10',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('292057907200','2014-03-20 10:32:20',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('292095787008','2014-06-09 14:58:47',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('292129341440','2014-02-12 10:45:33',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('292200382464','2014-06-11 06:09:29',980,'stevenc_07@hotmail.com','Rejected',5000.00,'CO',NULL),('292259299328','2014-02-19 16:32:55',881,'testmail@gmail.com','Rejected',10000.00,'CO',NULL),('292461871104','2014-03-31 08:07:03',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('292598841344','2014-05-23 12:08:21',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('292900831232','2014-06-07 06:55:17',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('293131583488','2014-06-04 16:00:26',917,'aljurado@gmail.com','Rejected',20000.00,'CO',NULL),('293165334528','2014-06-07 06:54:48',881,'apereira@gmail.com','Rejected',10000.00,'CO',NULL),('293436522496','2014-04-11 16:35:24',909,'ebayon@city-parking.com','Rejected',50000.00,'CO',NULL),('293480431616','2014-06-25 15:27:43',1051,'andres.pereirap@gmail.com','Rejected',20000.00,'CO',NULL),('293539676160','2014-05-08 11:23:20',949,'joalex9226@yahoo.com','Rejected',10000.00,'CO',NULL),('294210568192','2014-06-09 14:42:02',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('294239141888','2014-06-10 08:54:26',948,'adriana','Rejected',10000.00,'CO',NULL),('294343737344','2014-06-11 06:01:21',980,'stevenc_07@hotmail.com','Rejected',10000.00,'CO',NULL),('294556336128','2014-06-25 15:27:59',1140,'cesarortiz77@gmail.com','Rejected',100000.00,'CO',NULL),('294753140736','2014-06-16 07:46:20',1076,'dianin0331@gmail.com','Rejected',17366.00,'CO',NULL),('297461415936','2014-04-11 14:32:25',929,'operaciones3@city-parking.com','Rejected',25000.00,'CO',NULL),('297473671168','2014-04-11 16:10:19',934,'maxfutura1782@gmail.com','Rejected',25000.00,'CO',NULL),('297574137856','2014-05-05 08:52:41',945,'johnamaya02@gmail.com','Rejected',20000.00,'CO',NULL),('297734569984','2014-04-11 14:34:07',929,'operaciones3@city-parking.com','Rejected',250000.00,'CO',NULL),('2980203816557132988','2014-07-24 14:37:00',1597,'alexblandon@gmail.com','Rejected',10.00,'CO',NULL),('298500292608','2014-04-23 14:29:38',909,'ebayon@city-parking.com','Rejected',50000.00,'CO',NULL),('2987464695485284186','2014-07-09 18:19:02',1256,'herneyder@gmail.com','Rejected',1000.00,'CO',NULL),('299882577920','2014-05-06 10:46:25',936,'oswaldoaponte23@gmail.com','Rejected',40000.00,'CO',NULL),('300652560384','2014-07-08 10:28:44',965,'scabrera@cpsparking.ca','Rejected',5000.00,'CO',NULL),('300654133248','2014-06-13 21:22:57',1076,'dianin0331@gmail.com','Rejected',17366.00,'CO',NULL),('300656361472','2014-07-08 10:29:00',965,'scabrera@cpsparking.ca','Rejected',5000.00,'CO',NULL),('301318995968','2014-06-18 05:57:59',1105,'andresdemalo@hotmail.com','Rejected',10000.00,'CO',NULL),('301989888000','2014-06-24 18:21:54',1146,'alejotatan@gmail.com','Rejected',50000.00,'CO',NULL),('302834515968','2014-06-09 14:43:20',908,'caloman@hotmail.com','Rejected',25000.00,'CO',NULL),('302897561600','2014-06-11 22:38:43',1047,'danielfelipecristobal@gmail.com','Rejected',1000.00,'CO',NULL),('303101902848','2014-06-27 12:03:04',1173,'mquinonesvilla@gmail.com','Rejected',100.00,'CO',NULL),('303514517504','2014-06-29 22:19:24',1178,'matteogonzato@hotmail.com','Rejected',20.00,'CO',NULL),('303873916928','2014-07-02 21:40:04',1190,'cnd@cable.net.co','Rejected',25000.00,'CO',NULL),('304209920000','2014-06-25 15:04:24',1157,'alanbrito@gmail.com','Rejected',10000.00,'CO',NULL),('304982458368','2014-07-08 10:47:08',965,'scabrera@cpsparking.ca','Completed',5000.00,'CO',NULL),('3211264','2014-06-11 21:57:44',1046,'aguirres1@gmail.com','Rejected',50000.00,'CO',NULL),('3252863448355484303','2014-07-31 11:15:44',1611,'freddy.leon@unimilitar.edu.co','Rejected',10000.00,'CO',NULL),('328650264296929124','2014-07-31 11:17:36',1611,'freddy.leon@unimilitar.edu.co','Started',10000.00,'CO',NULL),('3289382912','2014-01-30 11:56:48',885,'anubis_ex@hotmail.com','Rejected',2000.00,'CO',NULL),('3575629773494305686','2014-07-31 07:33:58',1049,'yestrigos@gmail.com','Rejected',30000.00,'CO',NULL),('3646083973121949464','2014-07-24 14:06:28',929,'operaciones3@city-parking.com','Rejected',500000.00,'CO',NULL),('3719341684320941734','2014-07-28 13:47:06',1611,'freddy.leon@unimilitar.edu.co','Rejected',50000.00,'CO',NULL),('3732930560','2014-06-27 12:05:34',1173,'mquinonesvilla@gmail.com','Rejected',100.00,'CO',NULL),('3756448091898492610','2014-07-29 23:06:23',1617,'ferbarbosa27@gmail.com','Rejected',3859.00,'CO',NULL),('3961354513260721755','2014-07-15 15:44:30',970,'gerenciadesarrollo@city-parking.com','Completed',500000.00,'CO',NULL),('3987576193700152570','2014-07-09 06:44:00',949,'joalex9226@yahoo.com','Rejected',200000.00,'CO',NULL),('4076119530102345322','2014-07-28 13:51:57',1611,'freddy.leon@unimilitar.edu.co','Rejected',40000.00,'CO',NULL),('4122582181980881853','2014-07-29 18:12:05',1615,'jcmoralesc@cable.net.co','Completed',8000.00,'CO',NULL),('4224328303730145170','2014-07-31 21:01:05',1641,'jfjativa@gmail.com','Started',50000.00,'CO',NULL),('4235514894318550348','2014-07-29 22:22:29',1616,'david10_36@hotmail.com','Started',50000.00,'CO',NULL),('4330693300769473933','2014-07-10 22:16:02',1279,'mauricioreyestoro@gmail.com','Rejected',50000.00,'CO',NULL),('4333366980214935531','2014-07-22 19:11:02',1349,'drywallbogota@gmail.com','Rejected',50000.00,'CO',NULL),('4440306292510478','2014-07-31 20:04:26',1348,'mapinz@gmail.com','Started',100000.00,'CO',NULL),('4550125065088905869','2014-07-24 14:32:59',1597,'alexblandon@gmail.com','Completed',10.00,'CO',NULL),('4620566807235480480','2014-07-14 12:16:11',1169,'vdiazmoreno@hotmail.com','Rejected',300000.00,'CO',NULL),('4632673533424979055','2014-07-25 20:27:08',1605,'marpaty155@hotmail.com','Rejected',100000.00,'CO',NULL),('4648249812351759924','2014-07-24 11:07:53',1140,'cesarortiz77@gmail.com','Rejected',75000.00,'CO',NULL),('4769866703690249968','2014-07-09 06:44:55',1250,'cbszajowicz@gmail.com','Rejected',100000.00,'CO',NULL),('4777377184200606641','2014-07-29 18:08:36',1615,'jcmoralesc@cable.net.co','Rejected',10000.00,'CO',NULL),('4870146632146112177','2014-07-29 16:20:51',1051,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('5074119680328028016','2014-07-25 20:05:07',1605,'marpaty155@hotmail.com','Rejected',50000.00,'CO',NULL),('5106549328845191012','2014-07-25 19:59:32',1605,'marpaty155@hotmail.com','Rejected',50000.00,'CO',NULL),('524288','2014-02-19 15:26:13',881,'testmail@gmail.com','Completed',15000.00,'CO',NULL),('5317096849019747815','2014-07-24 11:11:20',1140,'cesarortiz77@gmail.com','Rejected',50000.00,'CO',NULL),('536870912','2014-06-16 15:23:24',948,'adriana','Rejected',25000.00,'CO',NULL),('5368774656','2013-12-30 10:31:08',885,'anubis_ex@hotmail.com','Rejected',200.00,'CO',NULL),('5378146304','2014-01-23 14:30:46',881,'city1@mail.com','Completed',10000.00,'CO',NULL),('5399146904008937318','2014-07-10 06:54:53',1250,'cbszajowicz@gmail.com','Completed',150000.00,'CO',NULL),('547356672','2014-06-03 10:55:31',881,'apereira@gmail.com','Rejected',1000.00,'CO',NULL),('5645533184','2014-04-11 14:36:19',929,'operaciones3@city-parking.com','Rejected',25.00,'CO',NULL),('5728928252810839747','2014-07-21 15:12:08',949,'joalex9226@yahoo.com','Rejected',150000.00,'CO',NULL),('5765521158076543449','2014-07-31 17:06:15',1634,'carloshhincapie@yahoo.es','Rejected',2000.00,'CO',NULL),('5936475459833674102','2014-07-31 07:35:01',1049,'yestrigos@gmail.com','Started',30000.00,'CO',NULL),('5936683066035356155','2014-07-23 08:24:14',1364,'dmedinap@bancolombia.com.co','Rejected',50000.00,'CO',NULL),('6010821990180304272','2014-07-10 21:58:00',1278,'galuquetta@gmail.com','Rejected',10000.00,'CO',NULL),('6111083485870210213','2014-07-31 16:58:26',1634,'carloshhincapie@yahoo.es','Rejected',2000.00,'CO',NULL),('6156872101047350440','2014-07-11 04:08:40',1280,'dariobeltrangomez@gmail.com','Rejected',50000.00,'CO',NULL),('6309064841361376349','2014-08-01 09:27:46',1644,'kanadi_99@hotmail.com','Rejected',100.00,'CO',NULL),('6568268897727827673','2014-07-24 15:08:31',1597,'alexblandon@gmail.com','Rejected',10.00,'CO',NULL),('6641714255212360645','2014-07-31 17:00:49',1634,'carloshhincapie@yahoo.es','Rejected',2000.00,'CO',NULL),('727389544787559131','2014-07-16 13:15:53',1297,'mimoralesmo@yahoo.com','Rejected',50000.00,'CO',NULL),('742918895886876755','2014-07-22 19:19:22',1335,'felodiaz@msn.com','Rejected',50000.00,'CO',NULL),('7604413431848846793','2014-07-09 06:44:31',1250,'cbszajowicz@gmail.com','Rejected',100000.00,'CO',NULL),('7961191236016226358','2014-07-23 17:42:16',1349,'drywallbogota@gmail.com','Rejected',50000.00,'CO',NULL),('7993474065187451478','2014-07-10 07:09:42',1265,'jorge@systegic.com','Rejected',20000.00,'CO',NULL),('8194049405104666570','2014-07-21 08:44:55',970,'gerenciadesarrollo@city-parking.com','Completed',500000.00,'CO',NULL),('8225427952995447544','2014-07-10 14:29:13',1268,'juandrp@gnail.com','Rejected',10000.00,'CO',NULL),('8267723339200904633','2014-07-17 17:41:53',1297,'mimoralesmo@yahoo.com','Completed',50000.00,'CO',NULL),('8269857142022650494','2014-07-23 21:19:15',1596,'ppfernan@bancolombia.com.co','Rejected',100.00,'CO',NULL),('8388608','2014-06-04 14:09:51',948,'adriana','Rejected',10000.00,'CO',NULL),('8429550365332189306','2014-07-23 17:39:16',1349,'drywallbogota@gmail.com','Rejected',50000.00,'CO',NULL),('8460183594701439345','2014-07-27 19:21:56',1608,'jaime.duque.tejeiro@hotmail.com','Rejected',25000.00,'CO',NULL),('8487274235979320522','2014-07-25 08:39:41',1349,'drywallbogota@gmail.com','Rejected',50000.00,'CO',NULL),('8545341376435077445','2014-07-24 09:19:25',1140,'cesarortiz77@gmail.com','Rejected',50000.00,'CO',NULL),('8590065664','2014-06-05 09:26:01',969,'wilmer.santana@gmail.com','Rejected',25000.00,'CO',NULL),('8590589952','2014-06-08 09:48:19',881,'apereira@gmail.com','Rejected',1000.00,'CO',NULL),('8591638528','2014-06-16 15:35:21',948,'operaciones10@city-parking.com','Rejected',20000.00,'CO',NULL),('861107759884289918','2014-07-23 08:07:29',1364,'dmedinap@bancolombia.com.co','Rejected',50000.00,'CO',NULL),('8613968086788389200','2014-07-30 06:22:40',1136,'jgomez_12lier@hotmail.com','Started',2000.00,'CO',NULL),('8623620096','2014-07-04 10:08:56',1051,'andres.pereirap@gmail.com','Rejected',10000.00,'CO',NULL),('8665432064','2014-06-16 16:00:25',948,'adrianag1807@hotmail.com','Rejected',25000.00,'CO',NULL),('8751408953369186059','2014-07-09 13:05:09',1250,'cbszajowicz@gmail.com','Rejected',100000.00,'CO',NULL),('8853415013880644070','2014-07-30 15:10:22',1630,'sandramvera@gmail.com','Started',50000.00,'CO',NULL),('8863080600401132862','2014-07-11 07:45:28',1283,'sachemas@gmail.com','Rejected',300.00,'CO',NULL),('8912896','2014-04-23 15:42:31',909,'ebayon@city-parking.com','Rejected',10000.00,'CO',NULL),('8961463909969906476','2014-07-16 21:34:53',909,'ebayon@city-parking.com','Completed',50000.00,'CO',NULL),('9008281099965546323','2014-07-13 19:05:18',1169,'vdiazmoreno@hotmail.com','Rejected',300000.00,'CO',NULL),('9035186176','2014-05-20 10:51:03',948,'adriana','Rejected',10000.00,'CO',NULL),('9071960643062706768','2014-07-29 21:49:43',1614,'adriana_tejada@hotmail.com','Completed',400000.00,'CO',NULL),('9085757061959626807','2014-07-15 20:34:35',1307,'andre_tero@hotmail.com','Completed',50000.00,'CO',NULL),('9128902656','2014-06-16 16:09:35',881,'apereira@cpsparking.ca','Rejected',1.00,'CO',NULL),('9188097719150529462','2014-07-09 06:44:52',949,'joalex9226@yahoo.com','Rejected',200000.00,'CO',NULL);
/*!40000 ALTER TABLE `cityparking_transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cps_city`.`cityparking_updateBalanceUser1`
AFTER INSERT ON `cps_city`.`cityparking_transaction`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
IF NEW.state_transaction = 'Completed' THEN
   update sys_user set balance = balance +  NEW.amount where idsys_user = NEW.id_sys_user;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cps_city`.`cityparking_updateBalanceUser2`
AFTER UPDATE ON `cps_city`.`cityparking_transaction`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
#INSERT INTO audition_log (description) VALUES( NEW.state_transaction );
IF NEW.state_transaction = 'Completed' THEN
#INSERT INTO audition_log (description) VALUES( 'Actualizacion' );

   update sys_user set balance = balance +  NEW.amount where idsys_user = NEW.id_sys_user;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `idclient` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `country_idcountry` bigint(20) DEFAULT NULL,
  `city_idcity` bigint(20) unsigned DEFAULT NULL,
  `status` char(1) DEFAULT '',
  PRIMARY KEY (`idclient`),
  KEY `fk_client_country` (`country_idcountry`),
  KEY `FK_client_city` (`city_idcity`),
  CONSTRAINT `FK_client_city` FOREIGN KEY (`city_idcity`) REFERENCES `city` (`idcity`),
  CONSTRAINT `fk_client_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (3,'CITYPARKING',472,103,'');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `idcountry` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `countryPrefix` varchar(4) NOT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `lang` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`idcountry`),
  UNIQUE KEY `prefix` (`countryPrefix`)
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (472,'Colombia','COL','4.0000','-72.0000','ENG');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_app_version`
--

DROP TABLE IF EXISTS `device_app_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_app_version` (
  `device_id` int(11) NOT NULL,
  `device_name` varchar(45) NOT NULL,
  `current_version` varchar(45) NOT NULL,
  `lastchange` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_app_version`
--

LOCK TABLES `device_app_version` WRITE;
/*!40000 ALTER TABLE `device_app_version` DISABLE KEYS */;
INSERT INTO `device_app_version` VALUES (1,'android','1.6','2014-06-19 02:00:00'),(2,'iphone','1.4','2014-06-03 16:01:56');
/*!40000 ALTER TABLE `device_app_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integrator`
--

DROP TABLE IF EXISTS `integrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integrator` (
  `idintegrator` bigint(20) NOT NULL AUTO_INCREMENT,
  `shortCode` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  `country_idcountry` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idintegrator`),
  KEY `fk_integrator_country` (`country_idcountry`),
  CONSTRAINT `fk_integrator_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integrator`
--

LOCK TABLES `integrator` WRITE;
/*!40000 ALTER TABLE `integrator` DISABLE KEYS */;
/*!40000 ALTER TABLE `integrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msisdn_user`
--

DROP TABLE IF EXISTS `msisdn_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msisdn_user` (
  `idmsisdn_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(15) DEFAULT NULL,
  `idsys_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idmsisdn_user`),
  UNIQUE KEY `msisdn_unique` (`msisdn`),
  KEY `fk_msisdn_user_sys_user` (`idsys_user`)
) ENGINE=InnoDB AUTO_INCREMENT=842 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msisdn_user`
--

LOCK TABLES `msisdn_user` WRITE;
/*!40000 ALTER TABLE `msisdn_user` DISABLE KEYS */;
INSERT INTO `msisdn_user` VALUES (463,'6210355',881),(466,NULL,883),(467,NULL,884),(468,'3163113240',885),(469,'3014302402',886),(471,'1223344566',887),(472,'3164976452',888),(475,'3164976456',889),(477,'3164976451',891),(478,'86487838',892),(479,'3105511906',893),(480,'42312431241',894),(483,'123123',906),(484,'7765',907),(485,'3168789072',908),(486,'3164230079',909),(489,'3143701492',912),(490,'3112011487',913),(491,'33333333',914),(492,'44444444',915),(493,'5555555',916),(494,'3116052106',917),(495,'956677088',918),(496,'3124645625',919),(497,'5263738383',920),(498,'1234567890',921),(502,'3174230074',929),(503,'3174230070',930),(504,'3188017826',934),(505,'3174230073',935),(506,'3168748323',936),(507,'3812369852',937),(508,'3118299490',938),(509,'3003012307',939),(510,'3108777269',940),(511,'3006136887',941),(514,'3186296890',944),(515,'3185159631',945),(516,'300333333',946),(517,'3134950053',947),(518,'3174316800',948),(519,'3007476111',949),(520,'3174230076',950),(522,'3102313437',953),(523,'3115806779',956),(524,'3002069082',957),(525,'3174230075',958),(526,' 50686487838',959),(528,'50670161744',959),(529,'3102826220',960),(531,'3174230072',964),(532,'4162067471',965),(533,'6477024530',966),(534,'6478810718',967),(535,'3006091656',969),(536,'3183530879',970),(537,'3186126551',971),(538,'3115667439',972),(539,'316579',973),(540,'3174589231',974),(541,'6479939089',975),(543,'3153929819',977),(544,'3164972576',978),(545,'3174025443',979),(546,'3173637079',980),(558,'3214857009',1032),(559,'3045230276',1033),(560,'3005712534',1034),(561,'3003590187',1035),(562,'3164708014',1036),(563,'3202785747',1037),(564,'3002143224',1038),(565,'3176805848',1039),(566,'3002645833',1040),(567,'3107998875',1041),(568,'3212009574',1042),(569,'3006009156',1043),(570,'3002046334',1044),(571,'3208594987',1045),(572,'3002004531',1046),(573,'3159267344',1047),(574,'3133945236',1048),(575,'3002086125',1049),(576,'3203081041',1050),(577,'3164976455',1051),(578,'3125278522',1052),(579,'3102224197',1053),(580,'3213150959',1062),(581,'3006199388',1063),(582,'3115905789',1064),(583,'3143251130',1065),(584,'3043773303',1066),(585,'3016735047',1067),(586,'3153609944',1068),(587,'3112081250',1069),(588,'3214907449',1070),(589,'3008029026',1071),(590,'3204325279',1072),(591,'3103244517',1073),(592,'3153481016',1074),(593,'3164814936',1075),(594,'3183833424',1076),(595,'3002006781',1077),(596,'3165219619',1078),(597,'3132776909',1079),(598,'3017465927',1080),(599,'3187076266',1081),(600,'3158995671',1082),(601,'3164739281',1083),(602,'3176607467',1084),(603,'3114621219',1085),(604,'  321',1086),(605,'3012302917',1091),(606,'3153452862',1095),(607,'3005504707',1096),(608,'3103486670',1097),(609,'3183592538',1098),(610,'3124358037',1099),(611,'3176436286',1100),(612,'3204773399',1101),(613,'3204038228',1102),(614,'3005681495',1103),(615,'3175734047',1104),(616,'3217005012',1105),(617,'3123069722',1106),(618,'3142968929',1113),(619,'3143317085',1114),(620,'3115238931',1115),(621,'3182668902',1116),(623,'3008164285',1118),(624,'3003185566',1119),(627,'3203394845',1122),(628,'3153924670',1123),(629,'3104807760',1124),(630,'3185481147',1130),(631,'3212560286',1131),(632,'3142951822',1132),(633,'3107770881',1133),(634,'3133334868',1134),(635,'3005522295',1135),(636,'3143555909',1136),(637,'3134673163',1137),(638,'3182696027',1138),(639,'3182711370',1139),(640,'3212320276',1140),(641,'3164726386',1141),(642,'3102627762',1142),(643,'3133824312',1143),(644,'3138059480',1144),(645,'3157661767',1145),(646,'3002171687',1146),(647,'3166300154',1147),(648,'3112389099',1148),(649,'3212130143',1149),(650,'3005645280',1150),(651,'3102326693',1151),(652,'3183970157',1152),(653,'3017681756',1153),(654,'3212134985',1154),(655,'3162438050',1155),(656,'3153597167',1156),(657,'3132859764',1157),(658,'3006155596',1158),(659,'3133761887',1159),(660,'3108619030',1160),(661,'3203028060',1161),(662,'3103167875',1162),(663,NULL,1163),(664,'3012352125',1164),(665,'3133807932',1165),(666,'3153150573',1166),(667,'3214492400',1167),(668,'3103044754',1168),(669,'3103345870',1169),(670,'3114925880',1172),(671,'3144205628',1173),(672,'3015565124',1174),(673,'3182988903',1175),(674,'3173004080',1176),(675,'3005907487',1177),(676,'3212440383',1178),(677,'3134838210',1179),(678,'3134213715',1180),(679,'3108146287',1181),(680,'3168755957',1182),(681,'3166558902',1183),(682,'3144703648',1184),(683,'3103204072',1185),(684,'3174314682',1186),(685,'3157782890',1187),(686,'3192026993',1188),(687,'3132661635',1189),(688,'3104841452',1190),(689,'3114756223',1191),(690,'3112104705',1192),(691,'3182194666',1193),(692,'3258445',1195),(693,'3114503362',1201),(694,'3153065597',1202),(695,'3165755568',1203),(696,'3125211299',1204),(697,'3005548526',1205),(698,'3008484848',1206),(699,'304-380-3125',1241),(700,'3005535506',1242),(701,'3212139283',1243),(702,'3125121785',1244),(703,'3107875441',1245),(704,'3003602885',1246),(705,'3015112904',1247),(706,'3174345720',1248),(707,'3212328036',1249),(708,'3112082477',1250),(709,'3118511014',1251),(710,'3112242367',1252),(711,'3187945670',1253),(712,'3002136538',1254),(713,'3166901820',1255),(714,'3143674026',1256),(715,'3163219844',1257),(716,'3183640482',1258),(717,'3115747659',1259),(718,'3102833945',1260),(719,'3203897723',1261),(720,'3013707923',1262),(721,'3153315720',1263),(722,'3187080796',1264),(723,'3124318053',1265),(724,'3207978003',1266),(725,'3108033069',1267),(726,'3153627612',1268),(727,'3003599155',1269),(728,'3185334021',1270),(729,'3212054074',1271),(730,'3214903652',1272),(731,'3174323014',1273),(732,'3012016301',1274),(733,'3158853391',1275),(734,'3144885141',1276),(735,'3184344934',1277),(736,'3012858477',1278),(737,'3112313478',1279),(738,'3176486990',1280),(739,'3102531895',1281),(740,'3108154016',1282),(741,'3118472009',1283),(742,'3105705240',1284),(743,'3184560125',1285),(744,'3164688242',1286),(745,'3006583957',1287),(746,'3002250416',1288),(747,'3208505771',1289),(748,'3186960459',1290),(749,'300000000',1291),(750,'3164694273',1292),(751,'3043924959',1293),(752,'3132021363',1294),(753,'3212013823',1295),(754,'3184455324',1296),(755,'3006099292',1297),(756,'3002896481',1299),(757,'3108091419',1300),(758,'3014228511',1301),(759,'3164735991',1302),(760,'3188390888',1303),(761,'3107583268',1304),(762,'3214682319',1305),(763,'3184559436',1306),(764,'3002919704',1307),(765,'3004446530',1308),(766,'3016594289',1309),(767,'3138885905',1315),(768,'3115323799',1316),(769,'3202359388',1317),(770,'3138295074',1318),(771,'3208541834',1330),(772,'3202302720',1331),(773,'3153017048',1332),(774,'3102109048 ',1333),(775,'3114456997',1334),(776,'3176450213',1335),(777,'3043838291',1336),(778,'3164783849',1337),(779,'3124515250',1338),(780,'3125598236',1339),(781,'3183061485',1340),(782,'3177258828',1341),(783,'3114708804',1342),(784,'3176486266',1343),(785,'3015109819',1344),(786,'3013701676',1345),(787,'3012098960',1346),(788,'3005631953',1347),(789,'3214497776',1348),(790,'3165287993',1349),(791,'3132107259',1350),(792,'3102472048',1351),(793,'3165215915',1352),(794,'3114524086',1353),(795,'3212710144',1354),(796,'3165242450',1363),(797,'3156322365',1364),(798,'3204991836',1365),(799,'3153628779',1366),(800,'3115017742',1367),(801,'asdfasfasfdasfa',1368),(806,'3153972117',1594),(807,'3052252461',1595),(808,'3176350729',1596),(809,'3014738438',1597),(810,'3166924550',1598),(811,'3102098296',1602),(812,'3003201034',1603),(813,'3186228856',1604),(814,'3143352282',1605),(815,'3115654097',1606),(816,'3103100948',1607),(817,'3168334106',1608),(818,'3125695215',1609),(819,'3003651553',1610),(820,'3107966487',1611),(821,'3188292476',1612),(822,'3017860036',1613),(823,'3108584605',1614),(824,'3208082690',1615),(825,'3166963864',1616),(826,'3182164767',1617),(827,'3204770632',1618),(828,'3016580329',1619),(829,'3203406823',1620),(830,'3102963512',1621),(831,'3102363178',1622),(832,'87132236',1623),(833,'3112891829',1624),(834,'3138206029',1630),(835,'3192223407',1631),(836,'3106975262',1632),(837,'3166901944',1633),(838,'3168746377',1634),(839,' 573163391914',1641),(840,'3132945447',1643),(841,'3138131428',1644);
/*!40000 ALTER TABLE `msisdn_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `idnotification` bigint(20) NOT NULL AUTO_INCREMENT,
  `message` varchar(160) DEFAULT NULL,
  `isread` int(11) DEFAULT NULL,
  `platform_idplatform` int(11) DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `dateNotif` datetime DEFAULT NULL,
  PRIMARY KEY (`idnotification`),
  KEY `fk_notification_platform` (`platform_idplatform`),
  KEY `FK_notification_user` (`idsys_user`),
  CONSTRAINT `fk_notification_platform` FOREIGN KEY (`platform_idplatform`) REFERENCES `platform` (`idplatform`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_notification_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parking_history`
--

DROP TABLE IF EXISTS `parking_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parking_history` (
  `idhistory` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `msisdn` varchar(15) DEFAULT NULL,
  `platform_idplatform` int(11) DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `place_idplace` varchar(80) DEFAULT NULL,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `zone_idzone` varchar(30) NOT NULL,
  `pay_id` varchar(45) DEFAULT NULL,
  `fee` double DEFAULT NULL,
  `confirm_number` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`idhistory`),
  KEY `fk_parking_history_platform` (`platform_idplatform`),
  KEY `FK_parking_history_user` (`idsys_user`),
  KEY `fk_parking_history_place` (`place_idplace`) USING BTREE,
  KEY `FK_parking_history_zone` (`zone_idzone`) USING BTREE,
  CONSTRAINT `fk_parking_history_platform` FOREIGN KEY (`platform_idplatform`) REFERENCES `platform` (`idplatform`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_parking_history_zones` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB AUTO_INCREMENT=1408 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parking_history`
--

LOCK TABLES `parking_history` WRITE;
/*!40000 ALTER TABLE `parking_history` DISABLE KEYS */;
INSERT INTO `parking_history` VALUES (1121,'2014-04-04 10:46:14','2014-04-04 10:46:22','3105511906',2,0.00,'0','',893,'COL71','15',100,'7115'),(1122,'2014-04-04 10:59:45','2014-04-04 10:59:52','3116052106',2,0.00,'0','',917,'COL71','16',100,'7116'),(1123,'2014-04-04 11:00:30','2014-04-04 11:00:37','3116052106',2,0.00,'0','',917,'COL71','17',100,'7117'),(1124,'2014-04-12 11:04:10','2014-04-12 11:07:40','3188017826',2,0.00,'0','',934,'COL22','2',300,'222'),(1125,'2014-04-12 11:21:56','2014-04-12 13:08:48','3188017826',2,0.00,'0','',934,'COL69','7',10200,'697'),(1126,'2014-04-15 08:40:50','2014-04-15 09:09:04','3188017826',2,0.00,'0','',934,'COL31','7',1900,'317'),(1127,'2014-04-16 15:51:13','2014-04-16 16:03:21','3116052106',2,0.00,'0','',917,'COL97','7',850,'977'),(1128,'2014-04-21 07:56:47','2014-04-21 10:57:02','3188017826',2,0.00,'0','',934,'COL71','19',9000,'7119'),(1129,'2014-04-25 11:12:06','2014-04-25 11:43:55','3105511906',2,0.00,'0','',893,'COL37','21',2150,'3721'),(1130,'2014-04-25 07:55:56','2014-04-25 11:50:47','3105511906',2,0.00,'0','',893,'COL37','22',2600,'3722'),(1131,'2014-04-25 12:21:13','2014-04-25 13:56:47','3164230079',2,0.00,'0','',909,'COL23','8',1000,'238'),(1132,'2014-04-30 08:37:32','2014-04-30 18:16:17','3164230079',2,0.00,'0','',909,'COL71','26',4000,'7126'),(1133,'2014-05-02 08:52:13','2014-05-02 08:52:26','3174230074',2,0.00,'0','',929,'COL25','3',100,'253'),(1134,'2014-05-02 08:56:49','2014-05-02 08:56:58','3174230074',2,0.00,'0','',929,'COL25','4',100,'254'),(1135,'2014-05-02 09:18:55','2014-05-02 09:19:54','3174230074',2,0.00,'0','',929,'COL33','1',100,'331'),(1136,'2014-05-02 09:22:57','2014-05-02 09:23:04','3174230074',2,0.00,'0','',929,'COL33','2',100,'332'),(1137,'2014-05-02 09:44:36','2014-05-02 09:51:24','3174230074',2,0.00,'0','',929,'COL102','1',100,'1021'),(1138,'2014-05-02 09:52:39','2014-05-02 09:52:48','3174230074',2,0.00,'0','',929,'COL102','2',100,'1022'),(1139,'2014-05-02 11:15:45','2014-05-02 11:43:10','3174230074',2,0.00,'0','',929,'COL37','23',1900,'3723'),(1140,'2014-05-03 09:59:14','2014-05-03 10:00:49','3174230074',2,0.00,'0','',929,'COL25','8',150,'258'),(1141,'2014-05-03 10:35:53','2014-05-03 11:11:33','3174230074',2,0.00,'0','',929,'COL37','24',2450,'3724'),(1142,'2014-05-05 09:19:57','2014-05-05 09:20:38','3174230074',2,0.00,'0','',929,'COL102','3',100,'1023'),(1143,'2014-05-06 09:59:40','2014-05-06 11:34:15','3164230079',2,0.00,'0','',909,'COL122','13',950,'12213'),(1144,'2014-05-06 12:59:57','2014-05-06 13:11:03','3174230074',2,0.00,'0','',929,'COL102','4',700,'1024'),(1145,'2014-05-07 09:05:12','2014-05-07 09:14:37','3174230074',2,0.00,'0','',929,'COL25','10',950,'2510'),(1146,'2014-05-07 12:47:41','2014-05-07 13:29:32','3174230074',2,0.00,'0','',929,'COL37','25',2850,'3725'),(1147,'2014-05-07 13:33:39','2014-05-07 13:40:50','3174230074',2,0.00,'0','',929,'COL37','26',550,'3726'),(1148,'2014-05-07 14:00:11','2014-05-07 14:00:45','3174230074',2,0.00,'0','',929,'COL37','27',100,'3727'),(1149,'2014-05-07 17:15:20','2014-05-07 17:15:40','3164230079',2,0.00,'0','',909,'COL90','3',100,'903'),(1150,'2014-05-09 08:21:52','2014-05-09 08:25:39','3164230079',2,0.00,'0','',909,'COL37','28',100,'3728'),(1151,'2014-05-11 10:51:33','2014-05-11 10:51:52','3174230074',2,0.00,'0','',929,'COL37','30',100,'3730'),(1152,'2014-05-16 07:49:17','2014-05-16 09:05:04','3164230079',2,0.00,'0','',909,'COL23','11',800,'2311'),(1153,'2014-05-20 10:28:30','2014-05-20 10:28:39','3174230074',2,0.00,'0','',929,'COL33','3',100,'333'),(1154,'2014-05-24 09:06:23','2014-05-24 12:02:27','3164230079',2,0.00,'0','',909,'COL21','10',1800,'2110'),(1155,'2014-05-28 09:40:32','2014-05-28 10:01:03','3185159631',2,0.00,'0','',945,'COL16','71',1350,'1671'),(1156,'2014-05-28 10:43:16','2014-05-28 10:55:25','3185159631',2,0.00,'0','',945,'COL18','7',900,'187'),(1157,'2014-05-28 11:20:21','2014-05-28 11:35:53','3185159631',2,0.00,'0','',945,'COL101','5',1100,'1015'),(1158,'2014-05-28 14:34:07','2014-05-28 14:47:10','3185159631',2,0.00,'0','',945,'COL70','9',900,'709'),(1159,'2014-05-28 19:10:48','2014-05-28 19:19:06','3186296890',2,0.00,'0','',944,'COL108','6',450,'1086'),(1160,'2014-05-29 07:26:13','2014-05-29 07:26:25','3174230070',2,0.00,'0','',930,'COL90','1',100,'901'),(1161,'2014-05-29 07:18:24','2014-05-29 07:38:01','3112011487',2,0.00,'0','',913,'COL103','4',350,'1034'),(1162,'2014-05-29 07:52:15','2014-05-29 07:53:31','3174230070',2,0.00,'0','',930,'COL11','10',150,'1110'),(1163,'2014-05-29 08:46:20','2014-05-29 08:49:43','3174230070',2,0.00,'0','',930,'COL29','32',300,'2932'),(1164,'2014-05-29 09:15:00','2014-05-29 09:15:10','3174230070',2,0.00,'0','',930,'COL22','6',100,'226'),(1165,'2014-05-29 09:38:58','2014-05-29 09:39:49','3174230070',2,0.00,'0','',930,'COL77','8',100,'778'),(1166,'2014-05-29 10:42:52','2014-05-29 10:46:59','3174230070',2,0.00,'0','',930,'COL46','19',400,'4619'),(1167,'2014-05-29 13:08:55','2014-05-29 13:12:59','3186296890',2,0.00,'0','',944,'COL49','6',400,'496'),(1168,'2014-05-30 09:48:48','2014-05-30 09:49:53','3174230074',2,0.00,'0','',929,'COL25','11',100,'2511'),(1169,'2014-05-30 10:33:07','2014-05-30 10:33:19','3174230074',2,0.00,'0','',929,'COL33','4',100,'334'),(1170,'2014-05-30 14:50:16','2014-05-30 14:53:23','3186296890',2,0.00,'0','',944,'COL118','1',150,'1181'),(1171,'2014-05-30 15:42:47','2014-05-30 16:05:38','3186296890',2,0.00,'0','',944,'COL24','2',1550,'242'),(1172,'2014-05-30 20:15:22','2014-05-30 20:18:07','3174230070',2,0.00,'0','',930,'COL92','10',300,'9210'),(1173,'2014-05-30 20:33:54','2014-05-30 20:34:17','3174230070',2,0.00,'0','',930,'COL33','5',100,'335'),(1174,'2014-05-30 20:37:42','2014-05-30 20:37:52','3174230070',2,0.00,'0','',930,'COL33','6',100,'336'),(1175,'2014-05-30 20:55:26','2014-05-30 20:58:15','3174230070',2,0.00,'0','',930,'COL95','10',250,'9510'),(1176,'2014-05-30 22:28:59','2014-05-30 22:30:53','3174230070',2,0.00,'0','',930,'COL37','37',200,'3737'),(1177,'2014-05-30 22:57:25','2014-05-30 22:57:35','3174230074',2,0.00,'0','',929,'COL102','5',100,'1025'),(1178,'2014-05-30 23:24:23','2014-05-30 23:24:33','3174230074',2,0.00,'0','',929,'COL46','20',100,'4620'),(1179,'2014-06-01 12:12:41','2014-06-01 12:13:12','3174230070',2,0.00,'0','',930,'COL9','7',100,'97'),(1180,'2014-06-01 18:03:49','2014-06-01 18:04:35','3174230070',2,0.00,'0','',930,'COL46','21',100,'4621'),(1181,'2014-06-04 09:00:16','2014-06-04 09:00:37','3185159631',2,0.00,'0','',945,'COL18','9',100,'189'),(1182,'2014-06-04 11:13:18','2014-06-04 11:24:05','3185159631',2,0.00,'0','',945,'COL70','11',750,'7011'),(1183,'2014-06-05 10:14:32','2014-06-05 10:16:21','3174230076',2,0.00,'0','',950,'COL109','32',150,'10932'),(1184,'2014-06-05 10:52:25','2014-06-05 10:54:18','3116052106',2,0.00,'0','',917,'COL71','34',200,'7134'),(1185,'2014-06-05 10:52:34','2014-06-05 11:09:15','3185159631',2,0.00,'0','',945,'COL101','8',1150,'1018'),(1186,'2014-06-05 12:11:58','2014-06-05 12:19:30','3174230076',2,0.00,'0','',950,'COL35','7',550,'357'),(1187,'2014-06-05 13:58:46','2014-06-05 14:00:00','3174230076',2,0.00,'0','',950,'COL4','6',200,'46'),(1190,'2014-06-06 12:17:44','2014-06-06 12:20:02','3112011487',2,0.00,'0','',913,'COL149','3',100,'1493'),(1192,'2014-06-06 21:39:16','2014-06-06 21:39:45','3186296890',2,0.00,'0','',944,'COL21','12',100,'2112'),(1193,'2014-01-16 10:25:23','2014-01-16 10:25:39','83735252',2,0.00,'0','',976,'COL77','16',100,'7716'),(1194,'2014-02-05 11:29:42','2014-02-05 11:29:49','83735252',2,0.00,'0','',976,'COL77','53',100,'7753'),(1197,'2014-06-10 09:09:22','2014-06-10 09:09:30','3174230076',2,0.00,'0','',950,'COL139','10',100,'13910'),(1198,'2014-06-10 09:10:55','2014-06-10 09:11:14','3174230076',2,0.00,'0','',950,'COL139','11',100,'13911'),(1199,'2014-06-10 09:30:44','2014-06-10 09:45:23','3174230076',2,0.00,'0','',950,'COL35','8',1050,'358'),(1200,'2014-06-10 10:36:11','2014-06-10 10:36:17','3105511906',2,0.00,'0','',893,'COL71','36',100,'7136'),(1201,'2014-06-11 07:47:18','2014-06-11 07:47:31','3112011487',2,0.00,'0','',913,'COL100','12',50,'10012'),(1202,'2014-06-11 08:10:08','2014-06-11 08:16:25','3112011487',2,0.00,'0','',913,'COL6','6',500,'66'),(1203,'2014-06-11 09:05:31','2014-06-11 09:23:58','3112011487',2,0.00,'0','',913,'COL42','124',1300,'42124'),(1204,'2014-06-11 09:49:17','2014-06-11 09:49:27','3174230074',2,0.00,'0','',929,'COL62','17',100,'6217'),(1205,'2014-06-11 10:13:48','2014-06-11 10:14:33','3174230074',2,0.00,'0','',929,'COL115','2',50,'1152'),(1206,'2014-06-11 10:14:20','2014-06-11 10:17:02','3112011487',2,0.00,'0','',913,'COL21','13',250,'2113'),(1207,'2014-06-11 10:17:35','2014-06-11 10:17:54','3174230076',2,0.00,'0','',950,'COL109','37',100,'10937'),(1208,'2014-06-11 10:19:11','2014-06-11 11:10:54','3174230076',2,0.00,'0','',950,'COL109','38',2900,'10938'),(1209,'2014-06-11 11:38:31','2014-06-11 11:48:51','3174230076',2,0.00,'0','',950,'COL139','12',850,'13912'),(1210,'2014-06-11 12:08:19','2014-06-11 12:22:06','3174230076',2,0.00,'0','',950,'COL35','11',950,'3511'),(1211,'2014-06-11 12:45:35','2014-06-11 12:46:59','3168789072',2,0.00,'0','',908,'COL122','25',200,'12225'),(1212,'2014-06-11 12:52:45','2014-06-11 12:53:29','3168789072',2,0.00,'0','',908,'COL29','42',100,'2942'),(1213,'2014-06-11 12:41:25','2014-06-11 14:29:53','3003590187',2,0.00,'0','',1035,'COL77','10',10400,'7710'),(1214,'2014-06-11 14:10:46','2014-06-11 14:34:29','3168748323',2,0.00,'0','',936,'COL37','39',1650,'3739'),(1215,'2014-06-11 12:28:51','2014-06-11 14:42:04','3102826220',2,0.00,'0','',960,'COL77','12',12650,'7712'),(1216,'2014-06-11 15:32:54','2014-06-11 15:33:37','3112011487',2,0.00,'0','',913,'COL116','2',100,'1162'),(1217,'2014-06-11 15:33:50','2014-06-11 15:35:07','3112011487',2,0.00,'0','',913,'COL38','145',150,'38145'),(1218,'2014-06-11 16:41:43','2014-06-11 16:47:42','3174230076',2,0.00,'0','',950,'COL29','43',600,'2943'),(1219,'2014-06-11 17:02:14','2014-06-11 17:02:25','3174230074',2,0.00,'0','',929,'COL102','6',100,'1026'),(1220,'2014-06-11 17:10:40','2014-06-11 17:11:59','3174230074',2,0.00,'0','',929,'COL62','18',200,'6218'),(1221,'2014-06-11 17:15:05','2014-06-11 17:15:59','3168748323',2,0.00,'0','',936,'COL129','7',100,'1297'),(1222,'2014-06-11 17:43:10','2014-06-11 17:43:55','3174230074',2,0.00,'0','',929,'COL25','13',100,'2513'),(1223,'2014-06-12 10:12:02','2014-06-12 10:17:26','3174230076',2,0.00,'0','',950,'COL129','8',450,'1298'),(1224,'2014-06-12 10:18:23','2014-06-12 10:18:30','3174230076',2,0.00,'0','',950,'COL129','9',100,'1299'),(1225,'2014-06-12 10:27:34','2014-06-12 10:27:42','3174230074',2,0.00,'0','',929,'COL33','8',100,'338'),(1226,'2014-06-12 11:11:28','2014-06-12 11:11:40','3174230074',2,0.00,'0','',929,'COL115','4',50,'1154'),(1227,'2014-06-12 11:44:51','2014-06-12 11:45:08','3174230074',2,0.00,'0','',929,'COL37','40',100,'3740'),(1228,'2014-06-12 09:51:17','2014-06-12 11:58:19','3108777269',2,0.00,'0','',940,'COL139','16',9550,'13916'),(1229,'2014-06-12 14:09:31','2014-06-12 14:11:20','3186296890',2,0.00,'0','',944,'COL71','37',150,'7137'),(1230,'2014-06-12 17:25:34','2014-06-12 17:26:44','3185159631',2,0.00,'0','',945,'COL71','38',100,'7138'),(1231,'2014-06-13 10:32:42','2014-06-13 10:32:58','3185159631',2,0.00,'0','',945,'COL18','12',100,'1812'),(1232,'2014-06-13 11:00:59','2014-06-13 11:27:35','3174230076',2,0.00,'0','',950,'COL139','17',1950,'13917'),(1233,'2014-06-13 11:49:53','2014-06-13 11:50:29','3174230076',2,0.00,'0','',950,'COL35','12',100,'3512'),(1234,'2014-06-13 14:16:38','2014-06-13 14:16:52','3185159631',2,0.00,'0','',945,'COL70','15',100,'7015'),(1235,'2014-06-14 14:13:20','2014-06-14 14:13:33','3185159631',2,0.00,'0','',945,'COL139','18',100,'13918'),(1236,'2014-06-16 06:08:16','2014-06-16 08:12:20','3108777269',2,0.00,'0','',940,'COL108','7',8350,'1087'),(1237,'2014-06-16 16:57:43','2014-06-16 16:58:28','3186126551',2,0.00,'0','',971,'COL71','40',100,'7140'),(1238,'2014-06-17 08:02:46','2014-06-17 08:02:54','3112011487',2,0.00,'0','',913,'COL45','2',50,'452'),(1239,'2014-06-17 10:00:12','2014-06-17 10:05:40','3174230076',2,0.00,'0','',950,'COL129','12',450,'12912'),(1240,'2014-06-17 13:02:08','2014-06-17 13:04:42','3174230076',2,0.00,'0','',950,'COL4','7',300,'47'),(1241,'2014-06-17 13:26:47','2014-06-17 13:27:03','3112011487',2,0.00,'0','',913,'COL123','9',100,'1239'),(1242,'2014-06-18 10:37:26','2014-06-18 10:38:39','3174230074',2,0.00,'0','',929,'COL62','20',100,'6220'),(1243,'2014-06-19 10:09:14','2014-06-19 10:09:59','3174316800',2,0.00,'0','',948,'COL85','30',100,'8530'),(1244,'2014-06-20 09:57:15','2014-06-20 09:57:24','3174316800',2,0.00,'0','',948,'COL85','34',100,'8534'),(1245,'2014-06-20 11:36:00','2014-06-20 11:36:09','3174316800',2,0.00,'0','',948,'COL157','2',100,'1572'),(1246,'2014-06-20 12:08:29','2014-06-20 12:08:39','3112011487',2,0.00,'0','',913,'COL24','3',100,'243'),(1247,'2014-06-20 14:01:04','2014-06-20 14:02:04','3116052106',2,0.00,'0','',917,'COL74','214',50,'74214'),(1248,'2014-06-20 12:11:50','2014-06-20 14:03:59','3164708014',2,0.00,'0','',1036,'COL77','13',10650,'7713'),(1249,'2014-06-20 17:03:53','2014-06-20 17:08:15','3105511906',2,0.00,'0','',893,'COL16','96',500,'1696'),(1250,'2014-06-21 07:46:27','2014-06-21 08:00:36','3174230076',2,0.00,'0','',950,'COL74','216',500,'74216'),(1251,'2014-06-21 08:02:01','2014-06-21 08:02:23','3174230076',2,0.00,'0','',950,'COL74','217',50,'74217'),(1252,'2014-06-21 07:11:14','2014-06-21 10:58:02','3133334868',2,0.00,'0','',1134,'COL74','218',7300,'74218'),(1253,'2014-06-22 11:19:55','2014-06-22 13:09:35','3142968929',2,0.00,'0','',1113,'COL85','35',1500,'8535'),(1254,'2014-06-25 17:22:01','2014-06-25 18:41:46','3142968929',2,0.00,'0','',1113,'COL85','39',1500,'8539'),(1255,'2014-06-26 10:27:04','2014-06-26 10:28:50','3168789072',2,0.00,'0','',908,'COL123','10',150,'12310'),(1256,'2014-06-26 14:46:23','2014-06-26 19:52:18','3212320276',2,0.00,'0','',1140,'COL74','227',9800,'74227'),(1257,'2014-06-27 10:36:36','2014-06-27 10:37:06','3183530879',2,0.00,'0','',970,'COL71','43',100,'7143'),(1258,'2014-06-27 09:26:05','2014-06-27 11:23:28','3187076266',2,0.00,'0','',1081,'COL43','22',11050,'4322'),(1259,'2014-06-27 11:00:14','2014-06-27 12:04:57','3006009156',2,0.00,'0','',1043,'COL115','7',4400,'1157'),(1260,'2014-06-27 10:02:01','2014-06-27 15:04:51','3212320276',2,0.00,'0','',1140,'COL74','231',9650,'74231'),(1261,'2014-06-28 11:11:34','2014-06-28 11:14:23','3185159631',2,0.00,'0','',945,'COL18','15',250,'1815'),(1262,'2014-06-28 11:37:53','2014-06-28 11:38:51','3185159631',2,0.00,'0','',945,'COL101','15',100,'10115'),(1263,'2014-06-28 11:42:31','2014-06-28 11:42:42','3185159631',2,0.00,'0','',945,'COL101','16',100,'10116'),(1264,'2014-06-28 10:27:07','2014-06-28 12:13:39','3142968929',2,0.00,'0','',1113,'COL85','40',1500,'8540'),(1265,'2014-06-28 14:19:44','2014-06-28 14:20:16','3185159631',2,0.00,'0','',945,'COL70','26',100,'7026'),(1266,'2014-07-01 08:21:26','2014-07-01 08:21:54','3174230070',2,0.00,'0','',930,'COL11','14',100,'1114'),(1267,'2014-07-01 08:28:02','2014-07-01 08:28:19','3174230070',2,0.00,'0','',930,'COL11','15',100,'1115'),(1268,'2014-07-01 09:11:42','2014-07-01 09:12:06','3174230070',2,0.00,'0','',930,'COL29','47',100,'2947'),(1269,'2014-07-01 09:36:13','2014-07-01 09:36:51','3174230070',2,0.00,'0','',930,'COL22','7',100,'227'),(1270,'2014-07-01 09:47:51','2014-07-01 09:48:16','3174230070',2,0.00,'0','',930,'COL77','14',100,'7714'),(1271,'2014-07-01 08:46:00','2014-07-01 14:17:46','3212320276',2,0.00,'0','',1140,'COL74','232',10650,'74232'),(1272,'2014-07-01 16:34:04','2014-07-01 16:34:18','3174230070',2,0.00,'0','',930,'COL46','27',100,'4627'),(1273,'2014-07-02 09:17:41','2014-07-02 09:17:48','3105511906',2,0.00,'0','',893,'COL71','47',100,'7147'),(1274,'2014-07-02 09:26:39','2014-07-02 09:26:51','3116052106',2,0.00,'0','',917,'COL71','48',100,'7148'),(1275,'2014-07-02 09:27:32','2014-07-02 09:29:14','3116052106',2,0.00,'0','',917,'COL71','49',200,'7149'),(1276,'2014-07-02 09:52:52','2014-07-02 09:53:33','3105511906',2,0.00,'0','',893,'COL71','53',100,'7153'),(1277,'2014-07-02 09:56:09','2014-07-02 09:56:34','3116052106',2,0.00,'0','',917,'COL71','54',100,'7154'),(1278,'2014-07-02 10:47:25','2014-07-02 10:48:08','3174230074',2,0.00,'0','',929,'COL37','42',100,'3742'),(1279,'2014-07-02 11:59:41','2014-07-02 12:00:15','3168789072',2,0.00,'0','',908,'COL116','7',100,'1167'),(1280,'2014-07-02 10:55:18','2014-07-02 12:43:18','3164230079',2,0.00,'0','',909,'COL37','43',10300,'3743'),(1281,'2014-07-02 12:21:53','2014-07-02 13:03:39','3168789072',2,0.00,'0','',908,'COL12','7',400,'127'),(1282,'2014-07-02 10:07:32','2014-07-02 14:10:56','3212320276',2,0.00,'0','',1140,'COL74','233',7850,'74233'),(1283,'2014-07-03 12:33:31','2014-07-03 12:34:31','3174230074',2,0.00,'0','',929,'COL102','10',100,'10210'),(1284,'2014-07-03 13:09:36','2014-07-03 13:09:50','3174230074',2,0.00,'0','',929,'COL62','21',100,'6221'),(1285,'2014-07-06 10:01:18','2014-07-06 11:04:26','3182696027',2,0.00,'0','',1138,'COL71','55',6000,'7155'),(1286,'2014-07-06 11:26:50','2014-07-06 13:19:45','3142968929',2,0.00,'0','',1113,'COL85','41',1500,'8541'),(1287,'2014-07-06 14:29:19','2014-07-06 14:34:52','3174230074',2,0.00,'0','',929,'COL71','56',450,'7156'),(1288,'2014-07-07 14:54:24','2014-07-07 16:22:03','3142968929',2,0.00,'0','',1113,'COL85','42',1500,'8542'),(1289,'2014-07-07 09:27:16','2014-07-07 17:26:09','3212320276',2,0.00,'0','',1140,'COL74','241',15350,'74241'),(1290,'2014-07-08 09:23:49','2014-07-08 09:23:58','3174230076',2,0.00,'0','',950,'COL109','42',100,'10942'),(1291,'2014-07-08 11:14:49','2014-07-08 11:15:04','3185159631',2,0.00,'0','',945,'COL101','17',100,'10117'),(1292,'2014-07-08 11:51:51','2014-07-08 11:51:59','3174230076',2,0.00,'0','',950,'COL139','20',100,'13920'),(1293,'2014-07-08 09:02:24','2014-07-08 12:12:22','3168789072',2,0.00,'0','',908,'COL22','10',1900,'2210'),(1294,'2014-07-08 12:12:52','2014-07-08 12:15:13','3174230076',2,0.00,'0','',950,'COL35','15',250,'3515'),(1295,'2014-07-08 12:16:58','2014-07-08 12:17:12','3174230076',2,0.00,'0','',950,'COL35','16',100,'3516'),(1296,'2014-07-08 15:03:36','2014-07-08 15:03:56','3174230076',2,0.00,'0','',950,'COL4','12',100,'412'),(1297,'2014-07-08 17:43:42','2014-07-08 17:59:14','3002004531',2,0.00,'0','',1046,'COL21','17',1550,'2117'),(1298,'2014-07-08 15:51:03','2014-07-08 18:09:08','3168789072',2,0.00,'0','',908,'COL102','11',1400,'10211'),(1299,'2014-07-09 07:26:54','2014-07-09 10:05:04','3006199388',2,0.00,'0','',1063,'COL31','19',10600,'3119'),(1300,'2014-07-09 11:12:17','2014-07-09 13:17:22','3142968929',2,0.00,'0','',1113,'COL85','43',3700,'8543'),(1301,'2014-07-09 10:45:56','2014-07-09 15:21:55','3212320276',2,0.00,'0','',1140,'COL74','246',8850,'74246'),(1302,'2014-07-10 06:36:26','2014-07-10 06:36:44','3174230070',2,0.00,'0','',930,'COL9','8',100,'98'),(1303,'2014-07-10 09:51:17','2014-07-10 09:51:59','3174230076',2,0.00,'0','',950,'COL109','43',100,'10943'),(1304,'2014-07-10 10:20:40','2014-07-10 10:20:49','3174230076',2,0.00,'0','',950,'COL124','8',100,'1248'),(1305,'2014-07-10 11:29:48','2014-07-10 11:29:57','3116052106',2,0.00,'0','',917,'COL108','8',100,'1088'),(1306,'2014-07-10 11:35:09','2014-07-10 11:35:27','3174230074',2,0.00,'0','',929,'COL37','44',100,'3744'),(1307,'2014-07-10 12:28:50','2014-07-10 14:18:35','3142968929',2,0.00,'0','',1113,'COL85','44',1500,'8544'),(1308,'2014-07-10 15:56:07','2014-07-10 15:58:09','3174230074',2,0.00,'0','',929,'COL71','59',150,'7159'),(1309,'2014-07-10 15:59:39','2014-07-10 15:59:56','3116052106',2,0.00,'0','',917,'COL95','19',100,'9519'),(1310,'2014-07-10 16:56:12','2014-07-10 17:01:08','3002004531',2,0.00,'0','',1046,'COL4','13',500,'413'),(1311,'2014-07-11 11:29:30','2014-07-11 11:29:49','3185159631',2,0.00,'0','',945,'COL101','18',100,'10118'),(1312,'2014-07-11 13:33:29','2014-07-11 15:02:59','3142968929',2,0.00,'0','',1113,'COL85','45',1500,'8545'),(1313,'2014-07-11 15:36:44','2014-07-11 15:36:56','3164230079',2,0.00,'0','',909,'COL18','16',100,'1816'),(1314,'2014-07-11 07:17:47','2014-07-11 17:01:05','3112082477',2,0.00,'0','',1250,'COL49','11',19000,'4911'),(1315,'2014-07-11 17:09:14','2014-07-11 17:10:52','3174230070',2,0.00,'0','',930,'COL11','20',100,'1120'),(1316,'2014-07-11 09:58:48','2014-07-11 17:25:48','3212320276',2,0.00,'0','',1140,'COL74','251',14350,'74251'),(1317,'2014-07-11 18:38:31','2014-07-11 18:38:40','3174230074',2,0.00,'0','',929,'COL155','4',100,'1554'),(1318,'2014-07-12 10:22:10','2014-07-12 10:57:11','3168789072',2,0.00,'0','',908,'COL103','5',3350,'1035'),(1319,'2014-07-14 12:34:39','2014-07-14 14:25:22','3142968929',2,0.00,'0','',1113,'COL85','46',1500,'8546'),(1320,'2014-07-14 15:18:40','2014-07-14 15:19:46','3174230070',2,0.00,'0','',930,'COL11','21',100,'1121'),(1321,'2014-07-15 08:32:22','2014-07-15 10:09:03','3162438050',2,0.00,'0','',1155,'COL82','60',1500,'8260'),(1322,'2014-07-15 12:16:05','2014-07-15 14:12:55','3142968929',2,0.00,'0','',1113,'COL85','47',1500,'8547'),(1323,'2014-07-15 07:51:01','2014-07-15 15:42:07','3103345870',2,0.00,'0','',1169,'COL116','8',11150,'1168'),(1324,'2014-07-15 07:07:14','2014-07-15 16:50:15','3112082477',2,0.00,'0','',1250,'COL49','12',19000,'4912'),(1325,'2014-07-16 12:11:10','2014-07-16 12:12:54','3185159631',2,0.00,'0','',945,'COL70','29',200,'7029'),(1326,'2014-07-16 07:44:25','2014-07-16 13:44:03','3103345870',2,0.00,'0','',1169,'COL116','9',11150,'1169'),(1327,'2014-07-16 15:54:31','2014-07-16 16:47:53','3006009156',2,0.00,'0','',1043,'COL115','8',3600,'1158'),(1328,'2014-07-16 16:59:40','2014-07-16 17:30:31','3006009156',2,0.00,'0','',1043,'COL24','4',2950,'244'),(1329,'2014-07-16 18:07:57','2014-07-16 19:18:21','3006009156',2,0.00,'0','',1043,'COL95','21',6650,'9521'),(1330,'2014-07-17 08:18:22','2014-07-17 10:29:20','3162438050',2,0.00,'0','',1155,'COL82','61',3700,'8261'),(1331,'2014-07-17 07:48:57','2014-07-17 14:19:48','3103345870',2,0.00,'0','',1169,'COL116','10',11150,'11610'),(1332,'2014-07-17 07:11:23','2014-07-17 17:02:23','3112082477',2,0.00,'0','',1250,'COL49','13',19000,'4913'),(1333,'2014-07-18 11:46:14','2014-07-18 13:36:38','3002004531',2,0.00,'0','',1046,'COL115','10',7450,'11510'),(1334,'2014-07-19 00:57:42','2014-07-19 01:35:31','3138295074',2,0.00,'0','',1318,'COL21','18',3650,'2118'),(1335,'2014-07-19 14:12:55','2014-07-19 15:33:33','3006199388',2,0.00,'0','',1063,'COL62','23',4850,'6223'),(1336,'2014-07-19 17:19:34','2014-07-19 18:39:36','3102109048 ',2,0.00,'0','',1333,'COL11','22',7600,'1122'),(1337,'2014-07-20 11:39:47','2014-07-20 12:59:55','3142968929',2,0.00,'0','',1113,'COL85','50',800,'8550'),(1338,'2014-07-20 13:47:54','2014-07-20 16:18:35','3186126551',2,0.00,'0','',971,'COL62','25',7000,'6225'),(1339,'2014-07-20 16:21:45','2014-07-20 17:01:24','3043838291',2,0.00,'0','',1336,'COL22','11',3800,'2211'),(1340,'2014-07-21 09:39:53','2014-07-21 09:49:36','3174230076',2,0.00,'0','',950,'COL129','22',100,'12922'),(1341,'2014-07-21 09:49:07','2014-07-21 09:53:32','3174230076',2,0.00,'0','',950,'COL129','23',300,'12923'),(1342,'2014-07-21 09:10:20','2014-07-21 13:09:03','3103345870',2,0.00,'0','',1169,'COL116','12',11150,'11612'),(1343,'2014-07-21 14:07:33','2014-07-21 14:07:42','3105511906',2,0.00,'0','',893,'COL137','10',100,'13710'),(1344,'2014-07-21 07:49:25','2014-07-21 15:44:59','3103345870',2,0.00,'0','',1169,'COL116','13',11150,'11613'),(1345,'2014-07-21 16:28:48','2014-07-21 18:05:00','3142968929',2,0.00,'0','',1113,'COL85','51',1500,'8551'),(1346,'2014-07-21 09:10:21','2014-07-21 18:51:09','3212320276',2,0.00,'0','',1140,'COL74','258',18600,'74258'),(1347,'2014-07-21 20:27:32','2014-07-21 21:54:21','3138295074',2,0.00,'0','',1318,'COL37','52',8300,'3752'),(1348,'2014-07-22 07:25:01','2014-07-22 09:25:47','3162438050',2,0.00,'0','',1155,'COL82','62',1500,'8262'),(1349,'2014-07-22 10:03:34','2014-07-22 13:03:43','3006199388',2,0.00,'0','',1063,'COL43','24',17100,'4324'),(1350,'2014-07-22 12:09:01','2014-07-22 13:15:19','3164976455',2,0.00,'0','',1051,'COL4','14',6400,'414'),(1351,'2014-07-22 11:56:29','2014-07-22 13:19:54','3142968929',2,0.00,'0','',1113,'COL85','52',1500,'8552'),(1352,'2014-07-22 07:59:18','2014-07-22 13:33:51','3103345870',2,0.00,'0','',1169,'COL116','14',11150,'11614'),(1353,'2014-07-22 14:37:51','2014-07-22 15:34:01','3103345870',2,0.00,'0','',1169,'COL116','15',3500,'11615'),(1354,'2014-07-22 20:39:17','2014-07-22 21:45:48','3002919704',2,0.00,'0','',1307,'COL38','198',2300,'38198'),(1355,'2014-07-23 07:54:46','2014-07-23 09:37:37','3006099292',2,0.00,'0','',1297,'COL25','14',9800,'2514'),(1356,'2014-07-23 08:32:17','2014-07-23 11:46:27','3162438050',2,0.00,'0','',1155,'COL82','63',5150,'8263'),(1357,'2014-07-23 08:40:54','2014-07-23 12:37:29','3012098960',2,0.00,'0','',1346,'COL49','14',19000,'4914'),(1358,'2014-07-23 10:02:57','2014-07-23 13:03:26','3103345870',2,0.00,'0','',1169,'COL116','17',11150,'11617'),(1359,'2014-07-23 15:05:22','2014-07-23 15:06:29','3186296890',2,0.00,'0','',944,'COL146','5',150,'1465'),(1360,'2014-07-23 17:50:28','2014-07-23 19:32:50','3142968929',2,0.00,'0','',1113,'COL85','53',1500,'8553'),(1361,'2014-07-24 10:51:22','2014-07-24 11:44:25','3187076266',2,0.00,'0','',1081,'COL43','25',4950,'4325'),(1362,'2014-07-24 10:15:58','2014-07-24 12:08:30','3212320276',2,0.00,'0','',1140,'COL74','261',3650,'74261'),(1363,'2014-07-24 12:16:04','2014-07-24 13:59:28','3162438050',2,0.00,'0','',1155,'COL24','5',9900,'245'),(1364,'2014-07-24 07:51:58','2014-07-24 14:26:32','3103345870',2,0.00,'0','',1169,'COL116','18',11150,'11618'),(1365,'2014-07-24 16:20:52','2014-07-24 17:54:59','3142968929',2,0.00,'0','',1113,'COL85','54',950,'8554'),(1366,'2014-07-24 14:13:55','2014-07-24 18:54:45','3006099292',2,0.00,'0','',1297,'COL42','144',13400,'42144'),(1367,'2014-07-24 20:00:39','2014-07-24 21:31:25','3002919704',2,0.00,'0','',1307,'COL38','201',2300,'38201'),(1368,'2014-07-25 07:40:14','2014-07-25 08:24:53','3212320276',2,0.00,'0','',1140,'COL24','7',4000,'247'),(1369,'2014-07-25 07:39:30','2014-07-25 13:03:54','3103345870',2,0.00,'0','',1169,'COL116','19',11150,'11619'),(1370,'2014-07-25 12:17:44','2014-07-25 15:33:23','3212320276',2,0.00,'0','',1140,'COL74','263',6250,'74263'),(1371,'2014-07-25 15:31:52','2014-07-25 16:19:54','3138295074',2,0.00,'0','',1318,'COL24','8',4600,'248'),(1372,'2014-07-25 16:27:35','2014-07-25 17:06:00','3002004531',2,0.00,'0','',1046,'COL4','15',3750,'415'),(1373,'2014-07-25 16:44:10','2014-07-25 19:39:28','3138295074',2,0.00,'0','',1318,'COL77','16',16650,'7716'),(1374,'2014-07-26 13:56:18','2014-07-26 17:37:01','3182696027',2,0.00,'0','',1138,'COL37','55',15300,'3755'),(1375,'2014-07-27 10:52:06','2014-07-27 12:45:18','3142968929',2,0.00,'0','',1113,'COL85','55',1500,'8555'),(1376,'2014-07-27 10:11:19','2014-07-27 14:19:46','3182696027',2,0.00,'0','',1138,'COL71','72',16350,'7172'),(1377,'2014-07-27 15:19:50','2014-07-27 16:41:09','3182696027',2,0.00,'0','',1138,'COL37','56',7800,'3756'),(1378,'2014-07-28 08:28:08','2014-07-28 11:02:44','3162438050',2,0.00,'0','',1155,'COL82','66',3700,'8266'),(1379,'2014-07-28 09:19:30','2014-07-28 12:34:32','3212320276',2,0.00,'0','',1140,'COL74','267',6250,'74267'),(1380,'2014-07-28 07:41:34','2014-07-28 12:58:50','3103345870',2,0.00,'0','',1169,'COL116','21',11150,'11621'),(1381,'2014-07-28 13:20:41','2014-07-28 13:21:27','3174230070',2,0.00,'0','',930,'COL90','4',100,'904'),(1382,'2014-07-28 18:52:46','2014-07-28 20:13:05','3182696027',2,0.00,'0','',1138,'COL71','75',7000,'7175'),(1383,'2014-07-29 08:13:46','2014-07-29 10:46:52','3162438050',2,0.00,'0','',1155,'COL82','68',3700,'8268'),(1384,'2014-07-29 10:00:20','2014-07-29 11:21:35','3142968929',2,0.00,'0','',1113,'COL85','56',1500,'8556'),(1385,'2014-07-29 11:09:21','2014-07-29 11:27:17','3187076266',2,0.00,'0','',1081,'COL43','27',1750,'4327'),(1386,'2014-07-29 13:25:50','2014-07-29 13:26:52','3164230079',2,0.00,'0','',909,'COL29','52',100,'2952'),(1387,'2014-07-29 18:59:24','2014-07-29 19:24:03','3003651553',2,0.00,'0','',1610,'COL21','19',2400,'2119'),(1388,'2014-07-29 18:46:26','2014-07-29 20:47:25','3102109048 ',2,0.00,'0','',1333,'COL12','11',11500,'1211'),(1389,'2014-07-29 19:50:57','2014-07-29 21:33:31','3002919704',2,0.00,'0','',1307,'COL38','208',2300,'38208'),(1390,'2014-07-30 10:09:53','2014-07-30 10:10:03','3174230074',2,0.00,'0','',929,'COL155','5',50,'1555'),(1391,'2014-07-30 07:57:57','2014-07-30 13:36:25','3108584605',2,0.00,'0','',1614,'COL116','26',11150,'11626'),(1392,'2014-07-30 11:50:13','2014-07-30 14:12:04','3212320276',2,0.00,'0','',1140,'COL74','272',4550,'74272'),(1393,'2014-07-30 14:35:26','2014-07-30 15:44:04','3142968929',2,0.00,'0','',1113,'COL85','57',600,'8557'),(1394,'2014-07-30 07:57:33','2014-07-30 15:52:04','3103345870',2,0.00,'0','',1169,'COL116','27',11150,'11627'),(1395,'2014-07-30 18:33:12','2014-07-30 19:39:37','3164708014',2,0.00,'0','',1036,'COL22','12',6400,'2212'),(1396,'2014-07-30 18:56:44','2014-07-30 20:04:14','3182696027',2,0.00,'0','',1138,'COL71','79',6500,'7179'),(1397,'2014-07-30 20:08:20','2014-07-30 21:26:24','3102109048 ',2,0.00,'0','',1333,'COL21','20',7450,'2120'),(1398,'2014-07-30 20:05:27','2014-07-30 21:57:50','3002919704',2,0.00,'0','',1307,'COL38','209',2300,'38209'),(1399,'2014-07-31 07:03:53','2014-07-31 10:41:59','3162438050',2,0.00,'0','',1155,'COL82','70',7450,'8270'),(1400,'2014-07-31 06:58:28','2014-07-31 11:44:19','3212320276',2,0.00,'0','',1140,'COL74','275',9200,'74275'),(1401,'2014-07-31 10:23:18','2014-07-31 13:06:19','3182696027',2,0.00,'0','',1138,'COL71','80',8000,'7180'),(1402,'2014-07-31 07:45:03','2014-07-31 13:19:08','3103345870',2,0.00,'0','',1169,'COL116','28',11150,'11628'),(1403,'2014-07-31 11:49:15','2014-07-31 13:31:21','3006199388',2,0.00,'0','',1063,'COL35','17',6850,'3517'),(1404,'2014-07-31 15:48:00','2014-07-31 17:50:13','3168748323',2,0.00,'0','',936,'COL103','6',2650,'1036'),(1405,'2014-07-31 13:03:43','2014-07-31 19:08:46','3006099292',2,0.00,'0','',1297,'COL42','150',21400,'42150'),(1406,'2014-07-31 16:33:23','2014-07-31 20:10:09','3212320276',2,0.00,'0','',1140,'COL74','276',6950,'74276'),(1407,'2014-07-31 20:45:12','2014-07-31 22:10:21','3212320276',2,0.00,'0','',1140,'COL37','58',8200,'3758');
/*!40000 ALTER TABLE `parking_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `parking_history_view`
--

DROP TABLE IF EXISTS `parking_history_view`;
/*!50001 DROP VIEW IF EXISTS `parking_history_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `parking_history_view` (
  `idhistory` tinyint NOT NULL,
  `startTime` tinyint NOT NULL,
  `endTime` tinyint NOT NULL,
  `msisdn` tinyint NOT NULL,
  `platform_idplatform` tinyint NOT NULL,
  `minuteValue` tinyint NOT NULL,
  `zone_idzone` tinyint NOT NULL,
  `vehicle_plate` tinyint NOT NULL,
  `idsys_user` tinyint NOT NULL,
  `pay_id` tinyint NOT NULL,
  `fee` tinyint NOT NULL,
  `minuts` tinyint NOT NULL,
  `minutsRound` tinyint NOT NULL,
  `totalParking` tinyint NOT NULL,
  `place_idplace` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `paypal_transaction`
--

DROP TABLE IF EXISTS `paypal_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_transaction` (
  `id_transaction` varchar(20) NOT NULL,
  `date_transaction` datetime NOT NULL,
  `id_sys_user` bigint(20) NOT NULL,
  `email_sys_user` varchar(30) DEFAULT NULL,
  `state_transaction` varchar(15) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_transaction`
--

LOCK TABLES `paypal_transaction` WRITE;
/*!40000 ALTER TABLE `paypal_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `updateBalanceUser` AFTER INSERT ON `paypal_transaction` FOR EACH ROW BEGIN
IF NEW.state_transaction = 'Completed' THEN
   update sys_user set balance = balance +  NEW.amount where idsys_user = NEW.id_sys_user;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `idplace` varchar(80) NOT NULL,
  `idzone` varchar(30) NOT NULL,
  PRIMARY KEY (`idplace`),
  KEY `FK_place_zone` (`idzone`),
  CONSTRAINT `FK_place_zone` FOREIGN KEY (`idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place`
--

LOCK TABLES `place` WRITE;
/*!40000 ALTER TABLE `place` DISABLE KEYS */;
/*!40000 ALTER TABLE `place` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `generatePlaceId` BEFORE INSERT ON `place` FOR EACH ROW BEGIN
DECLARE paisPrefix VARCHAR(3);
SELECT c.countryPrefix INTO paisPrefix
       FROM country c
       INNER JOIN client cl ON(c.idcountry = cl.country_idcountry) 
       INNER JOIN zone z ON(z.client_idclient = cl.idclient)
       WHERE z.idzone = NEW.idzone;
SET NEW.idplace = CONCAT(paisPrefix,NEW.idplace);
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `place_subzone`
--

DROP TABLE IF EXISTS `place_subzone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place_subzone` (
  `idplace` varchar(80) NOT NULL,
  `idsubzone` bigint(20) NOT NULL,
  PRIMARY KEY (`idplace`),
  KEY `fk_place_idx` (`idplace`),
  KEY `fk_subzone_idx` (`idsubzone`),
  KEY `FK_place_subzona_idx` (`idsubzone`),
  KEY `IDX_IDSUBZONE` (`idsubzone`) USING BTREE,
  CONSTRAINT `FK_place_subzona` FOREIGN KEY (`idsubzone`) REFERENCES `subzone` (`idsubzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place_subzone`
--

LOCK TABLES `place_subzone` WRITE;
/*!40000 ALTER TABLE `place_subzone` DISABLE KEYS */;
/*!40000 ALTER TABLE `place_subzone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `idplatform` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`idplatform`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform`
--

LOCK TABLES `platform` WRITE;
/*!40000 ALTER TABLE `platform` DISABLE KEYS */;
INSERT INTO `platform` VALUES (1,'SMS'),(2,'MOBILE'),(3,'WEB');
/*!40000 ALTER TABLE `platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_restrict`
--

DROP TABLE IF EXISTS `price_restrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_restrict` (
  `idtimePriceRestriction` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `zone_idzone` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idtimePriceRestriction`),
  KEY `fk_price_restrict_zone` (`zone_idzone`),
  CONSTRAINT `fk_price_restrict_zone` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_restrict`
--

LOCK TABLES `price_restrict` WRITE;
/*!40000 ALTER TABLE `price_restrict` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_restrict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revision_log`
--

DROP TABLE IF EXISTS `revision_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revision_log` (
  `revision` double NOT NULL,
  `author` varchar(145) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revision_log`
--

LOCK TABLES `revision_log` WRITE;
/*!40000 ALTER TABLE `revision_log` DISABLE KEYS */;
INSERT INTO `revision_log` VALUES (0.303,'juan otero','agregada nueva columna de envio de emails'),(0.379,'juan otero','creada nueva tabla user_transaction');
/*!40000 ALTER TABLE `revision_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shared_account`
--

DROP TABLE IF EXISTS `shared_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_account` (
  `idsys_user_acc1` bigint(20) NOT NULL,
  `idsys_user_acc2` bigint(20) NOT NULL,
  `state` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idsys_user_acc1`,`idsys_user_acc2`),
  KEY `fk_shared_account_sys_user` (`idsys_user_acc1`),
  KEY `fk_shared_account_sys_user1` (`idsys_user_acc2`),
  CONSTRAINT `fk_shared_account_sys_user` FOREIGN KEY (`idsys_user_acc1`) REFERENCES `sys_user` (`idsys_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shared_account_sys_user1` FOREIGN KEY (`idsys_user_acc2`) REFERENCES `sys_user` (`idsys_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shared_account`
--

LOCK TABLES `shared_account` WRITE;
/*!40000 ALTER TABLE `shared_account` DISABLE KEYS */;
INSERT INTO `shared_account` VALUES (917,893,0);
/*!40000 ALTER TABLE `shared_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_received`
--

DROP TABLE IF EXISTS `sms_received`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_received` (
  `smsID` bigint(20) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(15) DEFAULT NULL,
  `min` varchar(12) DEFAULT NULL,
  `shortCode` varchar(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `opt` varchar(10) DEFAULT NULL,
  `operator` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`smsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_received`
--

LOCK TABLES `sms_received` WRITE;
/*!40000 ALTER TABLE `sms_received` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_received` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_sent`
--

DROP TABLE IF EXISTS `sms_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_sent` (
  `smsID` bigint(20) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(15) DEFAULT NULL,
  `min` varchar(12) DEFAULT NULL,
  `shortCode` varchar(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `opt` varchar(10) DEFAULT NULL,
  `operator` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`smsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_sent`
--

LOCK TABLES `sms_sent` WRITE;
/*!40000 ALTER TABLE `sms_sent` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_sent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_networks`
--

DROP TABLE IF EXISTS `social_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_networks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_networks`
--

LOCK TABLES `social_networks` WRITE;
/*!40000 ALTER TABLE `social_networks` DISABLE KEYS */;
INSERT INTO `social_networks` VALUES (1,'fbm','http://m.facebook.com/pages/Canadian-Parking-Systems-Technology-Inc/105167772946251?sk=wall','Pagina social de CPS en facebook'),(2,'twm','http://twitter.com/cps_parking','Cuenta CPS de twiter');
/*!40000 ALTER TABLE `social_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subzone`
--

DROP TABLE IF EXISTS `subzone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subzone` (
  `idsubzone` bigint(20) NOT NULL AUTO_INCREMENT,
  `idzone` varchar(30) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`idsubzone`),
  UNIQUE KEY `idsubzone_UNIQUE` (`idsubzone`),
  KEY `fk_zona_idx` (`idzone`),
  KEY `FK_zone_idzone_idx` (`idzone`),
  CONSTRAINT `FK_zone_idzone` FOREIGN KEY (`idzone`) REFERENCES `zone` (`idzone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subzone`
--

LOCK TABLES `subzone` WRITE;
/*!40000 ALTER TABLE `subzone` DISABLE KEYS */;
/*!40000 ALTER TABLE `subzone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggestion`
--

DROP TABLE IF EXISTS `suggestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suggestion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `description` longtext NOT NULL,
  `sys_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggestion`
--

LOCK TABLES `suggestion` WRITE;
/*!40000 ALTER TABLE `suggestion` DISABLE KEYS */;
INSERT INTO `suggestion` VALUES (55,'2014-06-06 08:38:57','Creo que la idea es muy buena pero seria importante que city parking logre abrir mas espacios en BogotÃ¡. Para asÃ­ tener mas posibilidades de parqueo',974),(57,'2014-06-11 10:56:47','Debería exisrir una parte para consultar tarifas',1034),(58,'2014-06-11 12:26:37','Excelente aplicacion',1035),(59,'2014-06-20 09:02:49','Probando el envío de sugerencias',917),(60,'2014-06-20 10:13:43','Pésimo...quiero que me devuelvan la plata. En la universidad externado no sirve. Al momento de pagar no deja sale error y pierdo tiempo y plata',1076),(63,'2014-07-11 14:44:37','el parqueadero de la cra 15 con 98 deben mejorar el cicloparqueadero, y no tienen candado como los otros parqueaderos, y si se parquea con un solo candado se deberia cobrar como una sola bicicleta ',1287),(64,'2014-07-16 12:33:33','Los funcionarios no conocen el sistema ni la aplicación, no ha sido posible realizar pagos por medio de la misma, no le encuentro utilidad ',1297),(65,'2014-07-17 18:00:25','Buenas noches, recargue la aplicación desde mi cuenta, pero no veo reflejado el beneficio de la promoción de julio en mi saldo, agradeZco la colaboración. Mimoralesmo@yahoo.com',1297),(66,'2014-07-22 19:18:28','Estoy tratando de cargar la nueva app con dinero pero no pasa de la página donde uno escoge el medio e pago y el banco.\nNicolás ',1349),(67,'2014-07-25 07:16:13','En el día de ayer hice recargo y no me han incluido la promoción del doble. ',1140);
/*!40000 ALTER TABLE `suggestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `idsys_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `idsys_user_type` int(11) DEFAULT NULL,
  `country_idcountry` bigint(20) NOT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `cu` varchar(12) DEFAULT NULL,
  `favoriteMsisdn` varchar(15) DEFAULT NULL,
  `city_idcity` bigint(20) unsigned NOT NULL,
  `dateReg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `origin` varchar(10) DEFAULT NULL,
  `notificationmsj` smallint(6) DEFAULT '0',
  `identification` varchar(20) DEFAULT NULL,
  `passtemp` varchar(255) DEFAULT NULL,
  `email_notification` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idsys_user`),
  UNIQUE KEY `favMsisdnUnique` (`favoriteMsisdn`),
  UNIQUE KEY `login_unique` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_sys_user_sys_user_type` (`idsys_user_type`),
  KEY `fk_sys_user_country` (`country_idcountry`),
  KEY `FK_sys_user_city` (`city_idcity`),
  CONSTRAINT `FK_sys_user_city` FOREIGN KEY (`city_idcity`) REFERENCES `city` (`idcity`),
  CONSTRAINT `fk_sys_user_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`idcountry`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sys_user_sys_user_type` FOREIGN KEY (`idsys_user_type`) REFERENCES `sys_user_type` (`idsys_user_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1645 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (881,'city1','a36fffdea20502826b16628eeed3119d434e0982','Andres','Pereira Pereira','cll103 #14a-53','apereira@cpsparking.ca','1986-08-03',2,472,0.00,'27499','6210355',103,'2013-11-12 20:38:46',NULL,0,NULL,NULL,0),(883,'financiera','446f7cd076b90d54bf3312a3687a7f076f47c74a','Financiera','City','Bogota','financiera@mail.com','1984-06-02',6,472,NULL,'PARK',NULL,103,'2013-11-13 14:22:17',NULL,0,NULL,NULL,0),(884,'admin','97aef0296afb29245645ad56bbf90e571ce34d8d','Admin','City','Bogota','admin@mail.com','1984-06-02',5,472,NULL,'PARK1',NULL,103,'2013-11-13 14:38:21',NULL,0,NULL,NULL,0),(885,'anubis_ex','7c222fb2927d828af22f592134e8932480637c0d','juan','otero castellar','cll 52 52 55','anubis_ex@hotmail.com','1996-11-06',2,472,0.00,'62506','3163113240',103,'2013-11-14 18:03:29',NULL,0,NULL,NULL,0),(886,'soporte7','fe8ea1bdce0cee2ff444daa94d25c0c3398badbb','Daimler','Vanegas Berrio','Mall Ventura','soporte7@zonavirtual.com.co','1987-02-06',2,472,25000.00,'97121','3014302402',103,'2013-11-14 19:23:18',NULL,0,NULL,NULL,0),(887,'test4044','7c222fb2927d828af22f592134e8932480637c0d','juan','test4044','call 45 46','test4044@mail.com','1998-03-12',5,472,200.00,'94777','1223344566',103,'2013-12-17 21:21:08',NULL,0,NULL,NULL,0),(893,'hlopez','90386b11353c423c34dc29362b1e22a3272e56de','Harry','LÃ³pez','Cr 111a no 16 h 02','jharrylopez@gmail.com','1979-01-16',2,472,3520.00,'10279','3105511906',103,'2014-02-11 14:58:30',NULL,0,NULL,NULL,0),(908,'CALOMAN','6cb6d3a6a92f3fec8fe22ee1ba890b3cc68bd1d1','CARLOS GERMAN','RAMIREZ LONDONO','CR 51 N. 122 55 APT 403','caloman@hotmail.com','1971-07-17',2,472,7200.00,'61086','3168789072',103,'2014-03-06 16:05:44',NULL,0,NULL,NULL,0),(909,'ebayon','5e15ffd7a65e9aa2cd877abfd2d895733a86c882','Eduardo','Bayon','cll103 #14a-53','ebayon@city-parking.com','1968-10-21',2,472,50750.00,'62716','3164230079',103,'2014-03-06 20:54:06',NULL,0,NULL,NULL,0),(912,'rbtfutura','04130d5c257b0ba5ad9ead0b2da32f09f6ce5c51','oscar','quiroga','cr 71 # 52 - 44','rbtfutura@hotmail.com','1982-02-17',2,472,0.00,'35784','3143701492',103,'2014-03-08 18:01:19',NULL,0,NULL,NULL,0),(913,'soniayolima','28a314b0e7b1e24423812908f6079c46e0d3b14a','sonia','parra','cll 103 14a-53','soniayolima@gmail.com','1985-09-23',2,472,2060.00,'33938','3112011487',103,'2014-03-10 00:44:30',NULL,0,NULL,NULL,0),(917,'aljurado','0271736cc3d2f5ba3f905cf09f2c3bdc85f0ac31','Ana Luca','Jurado N','cll149#53-45 ','aljurado@gmail.com','1981-08-14',2,472,2050.00,'78513','3116052106',103,'2014-04-02 20:00:14',NULL,0,NULL,NULL,0),(918,'dpinho','0ec863c1f081cf0b6126f9942d0cfed790dd6d81','luli','depinho diaz','los olivos mz165 lote15 aa hh san martin','dpinho_7612@hotmail.com','1976-02-08',2,472,0.00,'33567','956677088',103,'2014-04-04 01:04:00',NULL,0,NULL,NULL,0),(919,'anders8877','a8b444be663ae81bf005c7faa8bcc6a172a26108','John Anderson ','Osorio','calle 66  19-22','anders8877@gmail.com','1988-08-20',2,472,0.00,'73323','3124645625',103,'2014-04-04 15:15:53',NULL,0,NULL,NULL,0),(920,'agata22','b1b3773a05c0ed0176787a4f1574ff0075f7521e','damian','hernandez','gxfjgyfj gdjfhg','micorreo@gmail.com','1980-07-05',2,472,0.00,'56083','5263738383',103,'2014-04-06 17:40:36',NULL,0,NULL,NULL,0),(921,'asael2','b1b3773a05c0ed0176787a4f1574ff0075f7521e','asael','arenas','k 100 n 100','asael2@gmail.com','1980-03-01',2,472,0.00,'88921','1234567890',103,'2014-04-06 22:56:38',NULL,0,NULL,NULL,0),(929,'edwin','0fada73eee63a53a2e8fd68d07da28878f436576','edwin','vargas','calle 103 No 14 a 53','operaciones3@city-parking.com','1980-03-06',2,472,8150.00,'43964','3174230074',103,'2014-04-11 20:22:41',NULL,0,NULL,NULL,0),(930,'wilsonv','b770e3ee5112a1a8c1407ee2d96df49321ec582e','wilson ','vargas portilla','clle 103 #103a-25','operaciones5@city-parking.com','1974-09-17',2,472,1550.00,'12496','3174230070',103,'2014-04-11 21:28:07',NULL,0,NULL,NULL,0),(934,'silviablan','af5c3065cd6e443215de0a3af51810ccb27944e2','silvia','blanco','carrera 71#52-44','maxfutura1782@gmail.com','1985-11-10',2,472,3600.00,'65164','3188017826',103,'2014-04-11 22:09:04',NULL,0,NULL,NULL,0),(935,'alejandrob','0a01332440166914a72924b0003030f8fc8fa69f','Alejandro','Bermudez','cra 11 a #191-28 torre 1 apto 802','operaciones2@city-parking.com','1983-12-19',2,472,0.00,'39437','3174230073',103,'2014-04-12 04:23:40',NULL,0,NULL,NULL,0),(936,'oaponte','3c3cca3e3e574434357c744dee3d46c94a30cac1','Oswaldo','Aponte','cll103#14a-53','oswaldoaponte23@gmail.com','1982-04-23',2,472,10600.00,'99619','3168748323',103,'2014-04-14 13:42:08',NULL,0,NULL,NULL,0),(937,'jdiaz','8789bfe9741db45fb0d9652ac3bb04a624b90a1c','Juan','Diaz Perez','Calle 1 No 2-3','jperez@gmail.com','1984-02-04',2,472,0.00,'60632','3812369852',103,'2014-04-14 14:16:12',NULL,0,NULL,NULL,1),(938,'Karmenzo','42e5156847a2f977f6596351776049967f1a50b5','Kevin','SÃ¡nchez','av.38 30-40','karmenzo@hotmail.com','1982-09-26',2,472,0.00,'43211','3118299490',103,'2014-04-14 17:39:11',NULL,0,NULL,NULL,0),(939,'edrojal','b78af6726fe112b61e68fb5e95ed52729e63c863','edwin','rojas alarcon','calle 63b 25-25','edrojal777@gmail.com','1982-05-09',2,472,0.00,'36406','3003012307',103,'2014-04-14 18:30:54',NULL,0,NULL,NULL,1),(940,'milenavw','76020219ccf8a7b08363fdfac44f7d097a97f6df','Milena','Vergara Wilches','Carrera 29 No 40 56 sur','milenavw@hotmail.com','1980-02-11',2,472,9600.00,'97712','3108777269',103,'2014-04-21 20:56:54',NULL,0,NULL,NULL,1),(941,'tavioto','350ceca89c0043e964b6d87dc7d5a8bf92c16a96','octavio','herrera','calle 97 70C-49','octavio.herrera@gmail.com','1981-11-01',2,472,0.00,'47886','3006136887',103,'2014-04-23 07:19:58',NULL,0,NULL,NULL,0),(944,'misagars','b3061b7765962465c39c0cd94ed2d88d1966643b','misael','macias corredor','calle 103 14-53','misagars@hotmail.com','1984-10-06',2,472,2050.00,'20371','3186296890',103,'2014-04-29 21:31:35',NULL,0,NULL,NULL,1),(945,'jaas2014','7d047c6dace1de2e693b0aa236ddd3dc82734cd8','John Alexander','Amaya Sanchez','Bogota','johnamaya02@gmail.com','1981-03-12',2,472,400.00,'43373','3185159631',103,'2014-05-01 12:39:08',NULL,0,NULL,NULL,1),(947,'lauraome','1b30089424699a05bd72d49626f8b383fefa9aa6','laura','ome','cll 119a #56a 97','l.ome197@uniandes.edu.co','1992-04-09',2,472,0.00,'41465','3134950053',103,'2014-05-04 18:36:43',NULL,0,NULL,NULL,0),(948,'adri1807','9da33634ea7414a71e2a01680ff05f77b08d5637','Adriana Maria','Guzman Segura','calle 156 # 92-64','adrianag1807@hotmail.com','1974-09-04',2,472,1700.00,'29015','3174316800',103,'2014-05-07 13:30:23',NULL,0,NULL,NULL,1),(949,'joalex9226','63b6ad61fd776dfdf6007a60b5418ae9aba0403c','Alexis','Cano','cra 1 no 32-52','joalex9226@yahoo.com','1994-02-06',2,472,0.00,'28216','3007476111',103,'2014-05-08 17:04:06',NULL,0,NULL,NULL,1),(950,'andiaz73','217d33dad956dae8025ca456af0c223a7983ac22','yimi uriel','diaz peralta','trav 5a este No 21-11 sur','operaciones11@city-parking.com','1974-07-04',2,472,14450.00,'69230','3174230076',103,'2014-05-16 17:01:36',NULL,0,NULL,NULL,1),(953,'olgapardo ','765bb5d5e39445c7f56b5554b9ba0d431132f818','olga','pardo ','cra 7 no 156_68','olgapardodiaz4@gmail.com','1969-06-27',2,472,0.00,'37932','3102313437',103,'2014-05-22 18:27:57',NULL,0,NULL,NULL,0),(956,'Richarsasuke','93ca835df9b335cc6f3df9acbc0771b18e4d8083','Richard','marquez','clle 75 c# 113 a 53','kiubey17@hotmail.com','1989-07-17',2,472,0.00,'08693','3115806779',103,'2014-05-23 17:47:23',NULL,0,NULL,NULL,1),(957,'david','8fea20de47033745e891e53ecceb021cb094931c','david ricardo','buendia benito-revollo','calle 142 # 13-33','davidricardo04@gmail.com','1984-08-04',2,472,0.00,'57744','3002069082',103,'2014-05-27 14:32:15',NULL,0,NULL,NULL,1),(958,'adrianam','5a9bd919e34833bbde395c6d595b776a73b1913a','ADRIANA ','mateus','cll 57#19a56','operaciones9@city-parking.com','1986-01-08',2,472,0.00,'23514','3174230075',103,'2014-05-27 15:28:53',NULL,0,NULL,NULL,1),(959,'cbrenes','c2877182dbd90b618f2c4c4699f2f70315736bd2','christhian','brenes','san jose','cbrenes@cpsparking.ca','1974-06-11',2,472,0.00,'84780',' 50686487838',103,'2014-05-27 17:38:57',NULL,0,NULL,NULL,0),(960,'alcastillo','18e1426b30cacb9c7502c9852364d95b74b95cfd','Ana','Castillo','cra 21 # 134 -61','analuciacastillo76@gmail.com','1976-12-19',2,472,87350.00,'65239','3102826220',103,'2014-05-29 14:21:10',NULL,0,NULL,NULL,1),(964,'santiago','7cf3ed2bd31750372364718432972a9cf5569304','ricardo','Sarmiento','k 7d 145-30 apt 501','operaciones4@city-parking.com','1974-10-26',2,472,0.00,'22985','3174230072',103,'2014-06-04 20:01:18',NULL,0,NULL,NULL,0),(965,'scabrera','8cb2237d0679ca88db6464eac60da96345513964','Steven','Cabrera','235 sherway gardens rd','scabrera@cpsparking.ca','1991-07-15',2,472,89980.00,'52570','4162067471',103,'2014-06-04 21:30:51',NULL,0,NULL,NULL,0),(966,'gerardo','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Gerardo','Cabrera','Sherway','gcabrera@cpsparking.ca','1964-07-23',2,472,0.00,'70123','6477024530',103,'2014-06-04 21:31:32',NULL,0,NULL,NULL,1),(967,'Adriana','8cf23826cc294d2f86c16818dc32dab165a32fc7','Adriana','Molinares','33 Sherway ','adria145@hotmIl.czyui','1964-09-26',2,472,0.00,'10999','6478810718',103,'2014-06-04 21:51:41',NULL,0,NULL,NULL,0),(969,'Wass','00af7f71c7fbfa070e05965c5598bfecfcca7509','Wilmer','Santana','Calle 149 53 45','wilmer.santana@gmail.com','1977-09-16',2,472,0.00,'11206','3006091656',103,'2014-06-05 01:57:05',NULL,0,NULL,NULL,0),(970,'cityparking','f36bee06f4d4e299e6362f2295c3d155ac52a7fe','City Parking','S.A.S.','calle 103 14a 53 of 206','gerenciadesarrollo@city-parking.com','1998-01-01',2,472,136000.00,'83466','3183530879',103,'2014-06-05 14:59:57',NULL,0,NULL,NULL,1),(971,'Guianro7','792d28329de7ed446c01b83f731a071248ffeaf4','Guillermo','Rodriguez','Calle 103 # 14a - 53','ggh@city-parking.com','1965-03-14',2,472,16900.00,'12026','3186126551',103,'2014-06-05 15:01:15',NULL,0,NULL,NULL,0),(972,'alveiro','89c33fd80342c77dcc2ea2f0065b40212f7e64e4','alveiro','rodriguez','chuniza','alveiro1612@hotmail.com','1985-08-16',2,472,0.00,'58299','3115667439',103,'2014-06-05 15:23:50',NULL,0,NULL,NULL,1),(973,'osierra','455cecb5050f540887f603eeb0a956d2e290829b','Oliver','Sierra','Direction','osierrasoporte@gmail.com','1985-02-11',2,472,0.00,'77475','316579',103,'2014-06-06 02:28:15',NULL,0,NULL,NULL,0),(974,'karitofloyd','295725eef3af46a31256a7c07d00f3eca9d9c97f','Carolina','Peralta','calle 63 n 78j - 38','karolinafloyd@gmail.com','1983-11-14',2,472,10200.00,'40450','3174589231',103,'2014-06-06 14:25:45',NULL,0,NULL,NULL,1),(975,'Hbuitrago','1b40ee0d2eb3a1d94fb921880fc1718db257c4d2','Hector','Buitrago','47 brookwood dr','hbuitragol@gmail.com','1954-04-09',2,472,0.00,'84801','6479939089',103,'2014-06-06 16:39:07',NULL,0,NULL,NULL,0),(977,'hop2014','01675056e74e38c4b456526775f104375294002c','hernan','ortiz','ak 45 147 12','hernan7806@yahoo.com','1978-02-06',2,472,0.00,'63848','3153929819',103,'2014-06-10 14:30:57',NULL,0,NULL,NULL,0),(978,'sharly','7c34716f3e28f3430c76d6321147b9e339b91b6d','sharly','heredia','calle 103 no 14a-53','sharly841022@hotmail.com','1984-10-22',2,472,0.00,'74264','3164972576',103,'2014-06-10 20:40:40',NULL,0,NULL,NULL,0),(979,'ivan0143','f383bfacec983a93d6a5e5974b24977add6475f3','ivan','beltran','calle 103 No 14-53','ivan001433@gmail.com','1979-11-23',2,472,0.00,'13491','3174025443',103,'2014-06-10 22:31:29',NULL,0,NULL,NULL,0),(980,'davidmoli','c49fb673ba0a8bf31e7b0a65de64b9e834867938','David','Molinares','cra 15 #87-26','stevenc_07@hotmail.com','2014-03-06',2,472,20.00,'51575','3173637079',103,'2014-06-11 00:19:10',NULL,0,NULL,NULL,0),(1032,'mary','8cb2237d0679ca88db6464eac60da96345513964','maria antonieta','garcia ducuara','calle 200 n 109 78','mary@hotmail.com','1980-06-01',2,472,0.00,'88975','3214857009',103,'2014-06-11 03:26:52',NULL,0,NULL,NULL,0),(1033,'knauj20','643fec50e79c69bc6bbb7616afd3904acf40867c','juan ','rodriguez','20 de julio','knauj20@gmail.com','1991-12-11',2,472,0.00,'99858','3045230276',103,'2014-06-11 16:15:27',NULL,0,NULL,NULL,0),(1034,'jarsoff','06d0aa0f942ab250a150c5b0847b3e811d78d58a','Julio Roberto','Arenas Rincón','CL 23C 69F 65 BL 33 AP 401','jarsoff@gmail.com','1975-08-18',2,472,0.00,'90374','3005712534',103,'2014-06-11 16:45:08',NULL,0,NULL,NULL,0),(1035,'andresnino','c7fdc520e5575e7673a7deba4806f4c00af6fcf1','andres','niño narvaez','cra 18c # 122 - 09','andresnino@yahoo.com','1976-08-03',2,472,89600.00,'80499','3003590187',103,'2014-06-11 18:14:39',NULL,0,NULL,NULL,0),(1036,'julianacoral','5ed88ab988ead445ad62abc76f57fa97dbbb2763','juliana','coral','cra 18c # 121-09','jcoral98@hotmal.com','1971-10-17',2,472,82950.00,'12701','3164708014',103,'2014-06-11 18:16:09',NULL,0,NULL,NULL,0),(1037,'macastro','95c0198316ea78e5086bef633bbf29753804da39','marlon alejandro',' castro ','carrera 83 número 82 a 30','macastrocani@gmail.com','1979-11-18',2,472,0.00,'99601','3202785747',103,'2014-06-11 22:22:33',NULL,0,NULL,NULL,0),(1038,'leonardoco','4f13c27712a0b7fa9aa86bf63f53cb909477c05f','jorge','hernandez','calle131z25.46','jorge.hernandez@gmail.com','1976-01-11',2,472,0.00,'61708','3002143224',103,'2014-06-11 23:13:06',NULL,0,NULL,NULL,0),(1039,'capc1989','841362213388372e07ac1c1403b7c36170210573','Carlos Andrés ','Peña Cárdenas ','Calle 19A # 120-09','pc.carlos.89@gmail.com','1989-10-11',2,472,0.00,'91517','3176805848',103,'2014-06-12 00:56:06',NULL,0,NULL,NULL,0),(1040,'rickdazam','1e9e67c91cb531ea65b54bdc9b40e02f57276f41','Ricardo Alfonso','Daza Martinez','Calle 127 #51A-45. Torre A. Apto 501.','rickdazam@gmail.com','1980-01-09',2,472,0.00,'51691','3002645833',103,'2014-06-12 00:57:46',NULL,0,NULL,NULL,0),(1041,'Gusmor0607','7afac3c9320ed65eb935aab0f9a891e5e518def3','Gustavo ','Moreno ','Cra 56a # 130a 61 Int 2 Apt 201 ','gusmor@hotmail.com','1972-06-07',2,472,0.00,'98443','3107998875',103,'2014-06-12 01:12:35',NULL,0,NULL,NULL,0),(1042,'marinesluna','30cbfedac62dddbcb25650a402897dd2eb101f86','María Inés',' Luna Carrizosa','Calle 109A 18 50 Apto. 205','marinesluna@hotmail.com','1965-04-23',2,472,0.00,'41094','3212009574',103,'2014-06-12 01:38:55',NULL,0,NULL,NULL,0),(1043,'torradoj','bd60facea6da70344fd9ec95afcbdad50f2a2e29','javier','torrado','trv 4 este # 61 - 05 t1 apt 1301','torradoj@hotmail.com','1975-12-29',2,472,82400.00,'36021','3006009156',103,'2014-06-12 02:03:07',NULL,0,NULL,NULL,0),(1044,'julorj','3a8b30d53dc352c661e361980904fbeae2021e5f','Julie','Orjuela','av cll 26 # 68B - 70','jorjuelap@bt.unal.edu.co','1987-06-18',2,472,0.00,'27550','3002046334',103,'2014-06-12 02:08:46',NULL,0,NULL,NULL,0),(1045,'berndsign','09afe8623c37dc1bae66e174736277668e8ea272','bernardo','gomez','cra 60 #79b-46 apt401','koopa_troop@hotmail.com','1983-05-04',2,472,0.00,'77157','3208594987',103,'2014-06-12 03:06:01',NULL,0,NULL,NULL,0),(1046,'aguirres','ddfbc8cd6ef9223f1fd9d57eebc0e994e203a8e4','Santiago ','Aguirre ','Calle 103A # 16-90 ap 202','aguirres1@gmail.com','1980-11-08',2,472,86750.00,'34142','3002004531',103,'2014-06-12 03:56:48',NULL,0,NULL,NULL,0),(1047,'garcenho','73b5bc775c1c99f33f45df8956859ce9a0f64aa2','Daniel Felipe Cristobal','García Clavijo','calle 131 #76A-85','danielfelipecristobal@gmail.com','1989-01-24',2,472,0.00,'08215','3159267344',103,'2014-06-12 04:31:45',NULL,0,NULL,NULL,0),(1048,'lachaves','1b4c4352f52e05e0b53239e0a0f7bbf2386d4130','Luis Adolfo ','Chaves Caicedo ','Calle 155 # 9-45 Apto 1212 Torre 1','lachaves@gmail.com','1970-12-26',2,472,0.00,'03098','3133945236',103,'2014-06-12 11:33:50',NULL,0,NULL,NULL,0),(1049,'yestrigos','9d9697f82c9b48bc25d9abf54695957dc3f83286','Yesenia','Trigos','Cra 7 35 40','yestrigos@gmail.com','1980-02-02',2,472,0.00,'47541','3002086125',103,'2014-06-12 12:13:19',NULL,0,NULL,NULL,0),(1050,'faro13','a83716ee84b36bcb4d635f81052da37186e0ff7c','Felipe','Rugen Ortiz','calle 64 j 72 a 58 apto 201','feliperugenortiz@gmail.com','1983-11-13',2,472,0.00,'82181','3203081041',103,'2014-06-12 16:45:36',NULL,0,NULL,NULL,0),(1051,'apereira','49453d64ae230fa29064b7a275eafd5d9bd14384','Andres','Pereira','bogota','andres.pereirap@gmail.com','1984-06-07',2,472,83400.00,'16483','3164976455',103,'2014-06-12 16:58:41',NULL,0,NULL,NULL,0),(1052,'victor','88fa846e5f8aa198848be76e1abdcb7d7a42d292','victor','londono','calle 98 #7a 08','vlondonoa@gmail.com','1947-05-01',2,472,100000.00,'13022','3125278522',103,'2014-06-12 19:54:19',NULL,0,NULL,NULL,0),(1053,'fjmjbog','ea995adaac5478f4eb81b5e90f0307823d4ee434','Francisco Javier','Mora Jaramillo','Calle 98 No. 7A - 08','jvrmora@gmail.com','1954-01-12',2,472,0.00,'30651','3102224197',103,'2014-06-12 20:02:23',NULL,0,NULL,NULL,0),(1062,'Adlc','6bf923730ee2c944ef34c7c775fdd4daa4c9e17b','Andres','De las casas','Calle 100 # 13-21','Andres.delascasas@oracle.com','1977-03-06',2,472,0.00,'91773','3213150959',103,'2014-06-12 20:14:00',NULL,0,NULL,NULL,0),(1063,'sospina','06ce3ba6bbf3d1303c1389a39a57119040d13b94','Santiago','Ospina','Tv 74 #11A - 15','santiago.ospina@gmail.com','1984-06-10',2,472,60600.00,'68979','3006199388',103,'2014-06-12 20:30:15',NULL,0,NULL,NULL,0),(1064,'connygn','9b7c3e5ff349c910dc35bb682a534a5310ae3f8d','constanza','guerrero','clle 116 16 89','constanza.guerrero@hotmail.com','1976-02-12',2,472,0.00,'35954','3115905789',103,'2014-06-12 21:29:06',NULL,0,NULL,NULL,0),(1065,'balaguera','561aaa115f8277b84b8ddf3303b5f3f53f4a5055','jorge alejandro','balaguera lopez','Carrera 12 N 22 - 63 Apt 403','balaguerajorge@yahoo.com','1978-07-06',2,472,0.00,'19358','3143251130',103,'2014-06-12 21:52:10',NULL,0,NULL,NULL,1),(1066,'maurogudi','d5d0ab0e7d67b306b8767a7cc75fe800ae66fdc0','Mauricio',' Gutiérrez Díaz','Calle 126 # 52A - 28','maogd@hotmail.com','1969-11-12',2,472,0.00,'60643','3043773303',103,'2014-06-13 09:19:44',NULL,0,NULL,NULL,0),(1067,'yeison79','d0de7949414fa4c058d0c299d20e97b1d1b5b698','yeison alexander','trujillo diaz','cll6 b 80 g 95','jeisontdiaz@gmail.com','1979-09-04',2,472,0.00,'14548','3016735047',103,'2014-06-13 12:12:34',NULL,0,NULL,NULL,0),(1068,'ndmorame','75e2107ce7e8072f6dd5c8d0b9a9ba723f1ae281','Nelson Dario','Mora Mendoza','Cra 13 # 155 - 51 casa 9','ndmorame@hotmail.com','1973-04-06',2,472,0.00,'62901','3153609944',103,'2014-06-13 13:10:48',NULL,0,NULL,NULL,0),(1069,'joango79','0d8b77a36cc3794017c2eb4d6eebe36a516fa330','Jorge','Angel','calle 166#9-45','joango79@hotmail.com','1979-06-27',2,472,0.00,'37388','3112081250',103,'2014-06-13 14:11:44',NULL,0,NULL,NULL,0),(1070,'carolina','3cb9d28c5ae0853c2f4fe357f3e20ab4acc6f70e','carolina','garcia','transversal 54 # 114-51 apto 402','garciacarito@hotmail.com','1980-04-22',2,472,0.00,'16088','3214907449',103,'2014-06-13 14:36:26',NULL,0,NULL,NULL,0),(1071,'jjgf13','1ef84247fd20effb53c42c28e80ef2d34da9d7c5','Jhonnatan','Galindo','Cra 58 # 125b-96 B3 Apto512','jjgf13@gmail.com','1985-04-05',2,472,0.00,'11741','3008029026',103,'2014-06-13 15:01:51',NULL,0,NULL,NULL,0),(1072,'maocar','664d4e9e161bc176db60d285e3f6cfee3a5cbfa7','mauricio','caro','cll131b ~ 91 a 26','maurocar78.mc@gmail.com','1978-12-31',2,472,0.00,'44888','3204325279',103,'2014-06-13 15:31:12',NULL,0,NULL,NULL,0),(1073,'ricatov','4f39e71860fbae8e0ffd28ce55c6c624f7526b4a','Ricardo','Tovar','calle 146 7-64','ricatov@hotmail.com','1974-05-28',2,472,0.00,'17478','3103244517',103,'2014-06-13 23:28:00',NULL,0,NULL,NULL,0),(1074,'eunice','888795ee1c62a23630f4619061cf4312c457c3ce','Eunice','Ñañez','calle 100 # 8a55','eunicenanezm@gmail.com','1966-03-16',2,472,0.00,'40054','3153481016',103,'2014-06-13 23:47:34',NULL,0,NULL,NULL,0),(1075,'etnarca','5864aa36a2b012367fd6189095de30f5041228c5','carlos','rivas','calle 142 #6-69','etnarca@yahoo.com','1964-03-28',2,472,0.00,'29883','3164814936',103,'2014-06-14 00:37:28',NULL,0,NULL,NULL,0),(1076,'dianin0331','cac8d2618173f475dea11dff4cb78d781b4d74e9','Diana Carolina ','Mancilla Bautista','Cr 101 B 141-35','dianin0331@gmail.com','1984-03-31',2,472,500.00,'99942','3183833424',103,'2014-06-14 03:18:07',NULL,0,NULL,NULL,0),(1077,'lmlanzam','5b6fd88081867dc28db1fab25e9426e1d484e3d5','laura','lanza','carrera 54 #64a - 45','lmlanzam@gmail.com','1983-04-29',2,472,0.00,'35203','3002006781',103,'2014-06-14 12:33:52',NULL,0,NULL,NULL,0),(1078,'CarlosE.','a480beb6cc0296af6881e8c0a23fbaeba26323ab','Carlos E.','Romero V.','cra. 20 # 122-58','carlos.e.romero@telmex.net.co','1954-04-07',2,472,0.00,'41405','3165219619',103,'2014-06-14 21:27:02',NULL,0,NULL,NULL,0),(1079,'janson79','380875d8ac05dd53771222ccccaf7586e6d03745','yeison','Ramirez','cll82 n 94-61','yandresramirez979@gmail.com','1978-09-11',2,472,0.00,'47660','3132776909',103,'2014-06-14 22:47:49',NULL,0,NULL,NULL,0),(1080,'cgmf89','a8c17c52eb747da790a8d285015ea9fd21ffc40e','Cristian Gerardo ','Maldonado Florez','Carrera 44 # 58c-45 sur','cgmf89@gmail.com','1989-12-01',2,472,0.00,'59502','3017465927',103,'2014-06-15 01:52:06',NULL,0,NULL,NULL,0),(1081,'RosaAmalia','ccac53a604dad151c792645f3236fe958a4a5c66','Rosa Amalia','Daza Lozada','Cr 70 C No. 118 -30','rosamaliadazal@hotmail.com','2014-07-07',2,472,82250.00,'20989','3187076266',103,'2014-06-15 02:51:16',NULL,0,NULL,NULL,1),(1082,'oscar_baez85','b9207aa9074569ea66a7401f408a64164fb72a6c','oscar javier','baez riveros','tv 71 f  5a 54 apt 205','oscar_baez85@yahoo.com','1985-12-05',2,472,0.00,'26484','3158995671',103,'2014-06-15 13:56:19',NULL,0,NULL,NULL,0),(1083,'diana84','615c01ddda9e03629d9c6026cb12ee45c60d9880','diana','castillo','Cra 71#117-29','d.castillo@hillebrandgroup.com','1984-10-18',2,472,0.00,'95096','3164739281',103,'2014-06-15 18:12:21',NULL,0,NULL,NULL,0),(1084,'josesu85','956341e1bee55489a70b0b67b1b78621be2710f7','Jose Ernesto','Rueda Caro','kr 14b 146 33 ','josesu85@gmail.com','1985-06-18',2,472,0.00,'72265','3176607467',103,'2014-06-15 22:13:59',NULL,0,NULL,NULL,0),(1085,'jorst2','a091dcc5d260faa1f0d0df53fa976cd6f6f3ddec','Jorge Andres','Torres','Calle 127 C # 9-45','jorst2@hotmail.com','1980-02-02',2,472,0.00,'23502','3114621219',103,'2014-06-16 01:42:36',NULL,0,NULL,NULL,0),(1086,'fernansala','6f883eabf047abd4997343a3b7ff39fe85dcb719','jimmy','salazar','carrera 6 este n 12-14','fernansala0526@gmail.com','1981-10-22',2,472,0.00,'36547','  321',103,'2014-06-16 12:46:55',NULL,0,NULL,NULL,0),(1091,'helman','a2ad098ace6f8890cac95ea377f9a7c4400d552f','Helman','Collante','Cr. 2a #66 - 52','helmancollante@gmail.com','1991-04-05',2,472,0.00,'55790','3012302917',103,'2014-06-16 17:23:29',NULL,0,NULL,NULL,0),(1095,'Fnavarros','fa9fe1e5968e2d880ac05ce643aa266b3f8fc1e9','Fabio Augusto','Navarro Sabogal','Cra 17 No 106-30 ','fabio_navarro@hotmail.com','1974-05-10',2,472,60000.00,'26869','3153452862',103,'2014-06-16 22:39:53',NULL,0,NULL,NULL,0),(1096,'willy_avila','95a351a4beb74af8c8e9ebe8b129185c8885b810','William','Avila','cra 56 # 153 -84','william.avila@gmail.com','1977-11-08',2,472,0.00,'65185','3005504707',103,'2014-06-16 23:48:56',NULL,0,NULL,NULL,0),(1097,'Dwalls','d241dc7b77766095a489008996c4c16ac3b9e5b3','Diego','Lopez','Cll 144 9-54','diegolopezparedes4@hotmail.com','1976-06-23',2,472,0.00,'28843','3103486670',103,'2014-06-17 01:55:52',NULL,0,NULL,NULL,0),(1098,'elrolo3000','8e7b08e507db2995c310784ff30708132ebc554e','Cristian','Ramirez','Carrera 46 # 22B - 20 oficina 309','cramirez@ravagoamericas.com','1985-05-07',2,472,0.00,'08071','3183592538',103,'2014-06-17 11:08:43',NULL,0,NULL,NULL,0),(1099,'andre5rg','c3560941621c3fad4482b11a0512e32637dd754b','Andres','Roa','Calle 22B 43B 56','roa.efrain@ur.edu.co','1993-11-29',2,472,0.00,'38931','3124358037',103,'2014-06-17 19:02:47',NULL,0,NULL,NULL,0),(1100,'jp1986','e4f88bf4b0c64b69a4393648335f5aa828e322fa','José','Pérez','Cra 56 no 7a-56','jp1986@hotmail.com','1986-06-04',2,472,0.00,'97227','3176436286',103,'2014-06-17 21:26:47',NULL,0,NULL,NULL,0),(1101,'jhonjairo8 ','27038379cbcc9373d4ac3984dcec048c670e90b1','jhon jairo','martinez gonzalez','Creo 73 # 36-32 sur','ba.nba.n@hotmail.com','1988-11-17',2,472,0.00,'93527','3204773399',103,'2014-06-18 00:47:08',NULL,0,NULL,NULL,0),(1102,'Dicasama','b96cc13e29bae67b284cb3ba1206c0909ec5b1fc','Diana','Sarmiento','Av 68','dianacsarmiento@gmail.com','1985-04-09',2,472,0.00,'31986','3204038228',103,'2014-06-18 01:13:02',NULL,0,NULL,NULL,0),(1103,'plutarco','2dab10b5fee7e94a3665f2f2be39aefb44c20897','plutarco','pedreros','carrera 99 bis 23h 49 int 4 apto 201','plutarco.pedreros@gmail.com','1957-12-21',2,472,0.00,'18010','3005681495',103,'2014-06-18 02:47:40',NULL,0,NULL,NULL,0),(1104,'nicolasmun','7d8a9cc4e566d9e2b77a5adbe3a25199f8e15054','Nicolás','Muñoz','Cra 55 # 22-38','nicolasmun@gmail.com','1982-12-07',2,472,0.00,'19194','3175734047',103,'2014-06-18 04:20:07',NULL,0,NULL,NULL,0),(1105,'DeMalo ','c5f3fcfa4d99e337d4e10c8050989ca0b1299172','Andrés ','Delgado ','Av 19 # 104  - 08','andresdemalo@hotmail.com','1985-12-02',2,472,0.00,'82431','3217005012',103,'2014-06-18 11:34:14',NULL,0,NULL,NULL,0),(1106,'claparca','39f27aaf0d4bb87334ed70df9d610002e7e6cf9c','Claudia','Arbelaez','Autopista Medellín kilos 2,2','claparca@gmail.com','1967-11-14',2,472,0.00,'43810','3123069722',103,'2014-06-18 12:17:15',NULL,0,NULL,NULL,0),(1113,'Victimor','fa076a53af2ad05d37e4c9334035b2aefdf88619','Javier fernando','Mariño molina','Cr 11a 191-28','gondulamarina84@hotmail.com','1984-09-29',2,472,72950.00,'12820','3142968929',103,'2014-06-18 15:30:51',NULL,0,NULL,NULL,0),(1114,'dujavi','fffd91fff2e4d46f3cfd782b963b327b0e385fb8','Javier','Duarte','cra 54d 135-15','dujandroid@gmail.com','1980-10-16',2,472,0.00,'55776','3143317085',103,'2014-06-18 15:48:31',NULL,0,NULL,NULL,0),(1115,'john9102','5c36d257ca3e367b2db87291d4f7614938162928','John','Amaya','cll 52 a sur n. 24 c-41','j.h.a.v@hotmail.com','1991-02-04',2,472,0.00,'08117','3115238931',103,'2014-06-18 20:04:51',NULL,0,NULL,NULL,0),(1116,'fermillo','df7c0aa5f98e05de3839c161ac96de98cf704fea','ferney','rozo','calle41 sur N1-08','adrianfelipegs@hotmail.com','1984-09-19',2,472,0.00,'75468','3182668902',103,'2014-06-18 23:41:20',NULL,0,NULL,NULL,0),(1118,'jlgomezf','379fc9b8ea702c734b1ea6db5cd00adc53bc7426','Jorge Luis','Gomez Florez','Calle 12B #71D-31','jlgomezf@gmail.com','1978-08-22',2,472,0.00,'14360','3008164285',103,'2014-06-19 01:39:00',NULL,0,NULL,NULL,0),(1119,'acosta-juan','734a213d2ae4a62a7e43e5e59839809ef1488c09','Juan','acosta','cll 45 # 4 27','acostaf0510@Gmail.com','1989-05-10',2,472,0.00,'58722','3003185566',103,'2014-06-19 03:14:36',NULL,0,NULL,NULL,0),(1122,'Javierpco','e07b6d37fdd0a7630ff5ca922ef9b7b9054a9788','Javier ','Perlaza','Cra 21# 9a31 local 74','javierpco@hotmail.com','1969-12-05',2,472,0.00,'24473','3203394845',103,'2014-06-19 14:48:41',NULL,0,NULL,NULL,0),(1123,'Cmolano','2796b256e1cb819cc1044368a2ca5e7213197680','Carlos','Molano ','Cra.12 n.144-48','cabeto4026@gmail.com','1973-07-11',2,472,0.00,'42346','3153924670',103,'2014-06-19 19:05:51',NULL,0,NULL,NULL,0),(1124,'Hquintero','ea90199a375bbe4416d54f871ebd9e2d665222b4','Hernando','Quintero M','Cl 165 A 58-62 Int 12 Apto 224','quinteromahecha@gmail.com','1964-02-02',2,472,0.00,'80493','3104807760',103,'2014-06-20 01:39:59',NULL,0,NULL,NULL,0),(1130,'Megas','b1c448fbafe82a81ea4a9757352a39b59ba21ece','Juan Camilo','Rios','cra 55#22-38','megastinger@hotmail.com','1991-10-04',2,472,0.00,'55746','3185481147',103,'2014-06-20 13:12:54',NULL,0,NULL,NULL,0),(1131,'Mrojas','b97181e12559c377a0ee34f73dc810fad0885e61','Manuel','Rojas','Cr 106 # 16 - 86','morrison57@gmail.com','1980-04-22',2,472,0.00,'56956','3212560286',103,'2014-06-20 17:50:42',NULL,0,NULL,NULL,1),(1132,'camruiz','d593bf8990aef2b423b3191860813713996ef465','Camilo Andres','Ruiz Diaz','Calle 160 # 73-47 Int 3 Apto 1103','camilo.ruiz.diaz@gmail.com','1986-05-08',2,472,0.00,'96675','3142951822',103,'2014-06-20 23:34:04',NULL,0,NULL,NULL,1),(1133,'tattan00','87a01bbdaa25a98a3d61585ebabb49d9a3e09243','jonathan','lara','calle 71#  93 59','joyape@hotmail.com','1994-03-19',2,472,0.00,'84085','3107770881',103,'2014-06-21 04:39:40',NULL,0,NULL,NULL,0),(1134,'svega4536','ef9b6b7c89127d753275cb02cdd14e7b9f08b1e9','Sergio Xavier ','Vega Laverde ','Calle 129a 56b 34','svega4536@hotmail.com','1983-04-18',2,472,12700.00,'32718','3133334868',103,'2014-06-21 14:43:47',NULL,0,NULL,NULL,0),(1135,'Javiersn','75cdc903dd9ae3689b0e9327e6e8e3cceeed1674','Javier','Sandoval','Calle 69 5 32 apt 703','javiersn79@gmail.com','1979-10-23',2,472,0.00,'93762','3005522295',103,'2014-06-21 15:02:05',NULL,0,NULL,NULL,0),(1136,'Andresito','3e4d38e93044b3e483244ee59e994ac6a30741e2','Andrés','Gómez','Calle37 a','jgomez_12lier@hotmail.com','1991-04-07',2,472,0.00,'53994','3143555909',103,'2014-06-21 15:21:04',NULL,0,NULL,NULL,0),(1137,'augustorojas','0e5e3139e06dd9482cf46d2c0177daa8eb505f43','Jose ','Rojas','Kra 7 48 03','augustorojas@yahoo.com','1980-08-23',2,472,0.00,'29853','3134673163',103,'2014-06-21 21:32:29',NULL,0,NULL,NULL,0),(1138,'fabian73','c45519c65b72e3f606512071b342d14e4282c112','fabian','ramirez','cra102 151-15','ramirezfabian@gmail.com','1973-10-06',2,472,133050.00,'31300','3182696027',103,'2014-06-21 23:22:42',NULL,0,NULL,NULL,0),(1139,'Javiclab','0504ac09f3ef5ef40f840d946ebbffaeaffc7175','Mauricio ','Clavijo Barbosa ','Calle 134a 53-82 ','mauricio.clavijob@gmail.com','1976-08-10',2,472,0.00,'08222','3182711370',103,'2014-06-22 00:58:05',NULL,0,NULL,NULL,0),(1140,'Cesarortiz77','66186ac7277e5e2bfe55c793bc5b0a9a09340c74','Cesar Augusto ','Ortiz Ruano','Cra 65 #103-01 Int 4 Apto 501 ','cesarortiz77@gmail.com','1975-09-16',2,472,30850.00,'87567','3212320276',103,'2014-06-22 16:59:18',NULL,0,NULL,NULL,1),(1141,'aceromora','f4d375289680432aa3f91c43f917bbb112edf3ee','JESS DAVID','Acero Mora','Cl 57 # 5-26 apto 302','aceromora@yahoo.com','1972-06-03',2,472,0.00,'73351','3164726386',103,'2014-06-22 17:30:37',NULL,0,NULL,NULL,0),(1142,'astrobymaro','f5c564f26b0e4d6009433904c0b166e4b7806cdd','Bibiana','Martínez Ortiz','Clle 44 D 45-86','astrobymaro@hotmail.com','1975-08-01',2,472,0.00,'94935','3102627762',103,'2014-06-23 01:58:32',NULL,0,NULL,NULL,0),(1143,'Monica','60b843896ecd9ddd3ff8d60af65e93248afdf8d0','Monica','Marquez','Cra 11A No191 28','monikamarquez71@msn.com','1979-05-28',2,472,0.00,'26088','3133824312',103,'2014-06-23 04:46:28',NULL,0,NULL,NULL,0),(1144,'mateoaristi','d48ff3bea6a0bdafac997fcc51dbf7f059a8a47c','Mateo','Aristizabal','Cr16a 100a','mateoaristi@gmail.com','1992-10-03',2,472,0.00,'76900','3138059480',103,'2014-06-23 23:55:15',NULL,0,NULL,NULL,0),(1145,'mariamoreno','79112e90afe26a95a4f4e127c3150bbadd581c04','maria','moreno','calle 142 #6-69','info@mariamoreno.com','1962-11-14',2,472,0.00,'76279','3157661767',103,'2014-06-25 00:16:58',NULL,0,NULL,NULL,0),(1146,'lsahrh','404c444d0934fe1d497314eb251fdd4401fbd4bb','liliana','sandoval','calle 147 no 11 86','alejotatan@gmail.com','2015-01-08',2,472,0.00,'94190','3002171687',103,'2014-06-25 00:18:55',NULL,0,NULL,NULL,1),(1147,'Mendivelso','13c2cdf55ed4f75ca3949eebb13421b71b529966','Nelson','Mendivelso','Calle 31 #6-39','nelson.mendivelso@gmail.com','1977-09-19',2,472,0.00,'91764','3166300154',103,'2014-06-25 01:09:56',NULL,0,NULL,NULL,1),(1148,'Liliq15','53d4ba1f08c9969f5723c6d10c2ad99bf3d25549','Liliana','Quiñones','Calle 150a #50-67','liliq1990@hotmail.com','1990-04-25',2,472,0.00,'69008','3112389099',103,'2014-06-25 02:29:09',NULL,0,NULL,NULL,0),(1149,'80109588','e104e6e03d8d6dc42887548a0f01fff896c0e4d4','Mario Andrés','Ramón','Cll 146 12','mario.andres.ramon@gmail.com','1982-03-17',2,472,0.00,'97292','3212130143',103,'2014-06-25 03:11:56',NULL,0,NULL,NULL,0),(1150,'ULTRAOXA','fb169127c753efa6ab71c0a39fdd9d756bd65663','Luis Felipe','Zaraza ','Calle 13no. 65 83','zarazafelipe@hotmail.com','1980-12-21',2,472,0.00,'25205','3005645280',103,'2014-06-25 03:14:05',NULL,0,NULL,NULL,0),(1151,'waguilarv','25d860779f201370eba49aaa6d9f0a0ab37d782d','Wilson','Aguilar Valencia','cll 77 a bis # 105 b 45','waguilarv@hotmail.com','1980-10-12',2,472,0.00,'24603','3102326693',103,'2014-06-25 12:47:57',NULL,0,NULL,NULL,1),(1152,'Gustangu','1c5b9186d5a3a6ff1f63751a6f59d08a5bfb7d54','Gustavo','Angulo','CLl 173a n 58-62','gustangu@icloud.com','1981-01-04',2,472,0.00,'89706','3183970157',103,'2014-06-25 14:04:33',NULL,0,NULL,NULL,0),(1153,'hbonillac','95f407c1ffa7689a0a3f2f21b1bf582fe8359351','henry','bonilla castro','carrera 102 b # 148 - 40 ','hbonillac@gmail.com','1979-08-29',2,472,0.00,'95218','3017681756',103,'2014-06-25 14:27:03',NULL,0,NULL,NULL,0),(1154,'Diegobrinez','df2bbed01360774eedfb44df1dad748b7b80de5c','Diego Fernando','Briñez Hernandez','Calle175 # 15-20 torre 2 apto 591','dfbrinez@hotmail.con','1979-02-28',2,472,0.00,'80717','3212134985',103,'2014-06-25 14:41:42',NULL,0,NULL,NULL,0),(1155,'lpinzon3','4fb4edd8fcc91da0168b1460496bc8249cc073ec','Laura Jimena','Pinzón','Calle 152B #58C-49 apto 701','laurajpinzonr@gmail.com','1978-09-21',2,472,163400.00,'97401','3162438050',103,'2014-06-25 15:02:46',NULL,0,NULL,NULL,0),(1156,'oscard138','e29a04569540fe025e3d7bb3c11fb11041d6f6d1','Óscar','Ramos','Calle 135 # 90 - 19','oscard138@hotmail.com','1989-09-13',2,472,0.00,'00601','3153597167',103,'2014-06-25 20:39:40',NULL,0,NULL,NULL,0),(1157,'alanbrito','7a27eeceb46275deaf7fbc7aac038298c9a53a4a','Alan','Gordon ','Carrera 7 # 57 - 75','alanbrito@gmail.com','1981-06-18',2,472,0.00,'61488','3132859764',103,'2014-06-25 20:53:10',NULL,0,NULL,NULL,0),(1158,'Yeso','3751d247493039b964ddd24585934e15aaf753bd','Yecid','Fuentes','Kr 55a N 167c-08','yecidfuentes@hotmail.com','1977-01-29',2,472,60000.00,'78783','3006155596',103,'2014-06-25 22:22:06',NULL,0,NULL,NULL,0),(1159,'ridiazba','0e92c7c3c9f121181d5edad1d2c375394bb0a9ac','Andrés Ricardo','Díaz Barrero','Cr21#120-53','ridiazba3@gmail.com','1992-11-27',2,472,0.00,'15137','3133761887',103,'2014-06-25 23:50:35',NULL,0,NULL,NULL,1),(1160,'alejo5904','64c2fa6483176ee7335e6244746d5e8be41f0b66','alexander','aristtizabal montilla','cra 58b #165a-71','alexander_am79@hotmail.com','1979-09-17',2,472,0.00,'45992','3108619030',103,'2014-06-26 00:47:15',NULL,0,NULL,NULL,0),(1161,'gimar','0ec7ed2554f238ea24789eebf1afacf5333bd124','yina','ochoa','carrera 64 #23a-10','gimar8a@hotmail.com','1982-02-12',2,472,0.00,'05730','3203028060',103,'2014-06-26 03:16:11',NULL,0,NULL,NULL,1),(1162,'Odslovera','3afe654c20037266d0b6ab63298ce35a8e566e61','Sergio','Lovera','Carrera 9 4a 29','odslovera@hotmail.com','1979-09-26',2,472,0.00,'24366','3103167875',103,'2014-06-26 05:36:40',NULL,0,NULL,NULL,0),(1163,'admincps','76f0e5489119c63986fc3c9e6b538deebdd5112f','Admin','CPS','Bogota','pcure@cpsparking.ca','1084-06-07',5,472,NULL,'PARK2',NULL,103,'2014-06-26 13:05:57',NULL,0,NULL,NULL,0),(1164,'cracksonj','3ae640f68f5e5ad732f16a4ef4d22e6a58e13068','Jeyson','Jimenez','kr 81 b # 19 b 50','jeyson.jimenez@gmail.com','1988-02-20',2,472,0.00,'50585','3012352125',103,'2014-06-26 15:16:13',NULL,0,NULL,NULL,0),(1165,'johnagudelo','1e3518da00378d8fb9f8d9130aa483d4f02f73e0','john andres','agudelo ardila','cll 22a sur 7 10','nectar.jhon@hotmail.com','1980-06-03',2,472,0.00,'46250','3133807932',103,'2014-06-26 17:41:18',NULL,0,NULL,NULL,0),(1166,'ceb683','8059cdaca7f832ca0f6b1664412ebd99968fc234','CARLOS EDUARDO','BURGOS VÁSQUEZ','CARRERA 10 No. 91-10','ceb683@gmail.com','1969-05-25',2,472,0.00,'75815','3153150573',103,'2014-06-26 17:45:56',NULL,0,NULL,NULL,0),(1167,'fedfercar','616b06dddb58e888a2ad2b644210e9e60220ba9b','Federico','Fernández','cll 96 #45a-40','fedfercar@hotmail.com','2014-08-12',2,472,0.00,'52113','3214492400',103,'2014-06-26 18:52:56',NULL,0,NULL,NULL,0),(1168,'Oliverosjair','16504cd5cd32a0574b4b73538ecceadcfb9820a0','Jair','Oliveros','Calle 94 # 72A 99 torre 3 apto 504','jair.oliveros@samsonite.com','1972-11-22',2,472,0.00,'80502','3103044754',103,'2014-06-26 20:44:16',NULL,0,NULL,NULL,0),(1169,'victorw','3a5ff3cc9806198a043000e5ca31d1bbbcb097a1','victor','diaz moreno','k 15 88-64','vdiazmoreno@hotmail.com','2000-06-26',2,472,462700.00,'85457','3103345870',103,'2014-06-27 02:05:13',NULL,0,NULL,NULL,0),(1172,'Jcmlfcit','d97e15cea7c3e225b2a0eef856c3db87e92ff833','Juan ','Medina','Cll127b bis # 53c - 28 ap 211','jcmlfcit@hotmail.com','1986-10-03',2,472,0.00,'20847','3114925880',103,'2014-06-27 16:00:15',NULL,0,NULL,NULL,0),(1173,'MarcosVilla','074fe681c9742d991dc00dc287aba5094ff8c678','Marcos','Quiñones Villa','calle 36 l Sur # 1 h 33 ','mquinonesvilla@gmail.com','1985-11-10',2,472,0.00,'64319','3144205628',103,'2014-06-27 18:02:19',NULL,0,NULL,NULL,0),(1174,'Lexs','30e47246e2ba175fc320a48af9c6196d1a42b163','alexander','sichaca','kra 79b #50-43 sur','lexs06@gmail.com','1984-11-14',2,472,0.00,'54070','3015565124',103,'2014-06-27 18:59:31',NULL,0,NULL,NULL,1),(1175,'asgiurew','574ece77ce10e113d9e4685fd844981d5c5c189f','Ligia','Pastran Reina','calle 152','ligiapastran@gmail.com','2014-11-11',2,472,0.00,'79812','3182988903',103,'2014-06-27 20:29:11',NULL,0,NULL,NULL,0),(1176,'cporras','5b3f6674e3d40423e0f2027af9e58c8a7472e060','Cesar Augusto','Porras Tibata','Calle 116 N 15-59','cesarporras12@hotmail.com','1978-06-12',2,472,0.00,'27472','3173004080',103,'2014-06-28 14:41:36',NULL,0,NULL,NULL,1),(1177,'pedro.torres','60c2c0f93d440893d4673afb2343ad0590901a03','Pedro','Torres Quiroga','Día 5A # 37 B 60','patqthebest@gmail.com','1985-03-12',2,472,0.00,'40735','3005907487',103,'2014-06-28 17:01:21',NULL,0,NULL,NULL,1),(1178,'mg1999','937bfaea6b875d17a48b0e4b499c346e56c4ca1c','Matteo','Gonzato ','Calle 128 #7-C 35 apto 410','matteogonzato@hotmail.com','1994-10-09',2,472,0.00,'51878','3212440383',103,'2014-06-28 18:41:47',NULL,0,NULL,NULL,0),(1179,'diielgado','c4b069ae9228208abdaee4e20b30d02f2bbc7184','Diego ','Delgado ','Calle 10 #10b38','diielgado1993@gmail.com','1993-02-17',2,472,0.00,'39472','3134838210',103,'2014-06-29 13:07:42',NULL,0,NULL,NULL,0),(1180,'juan01','ef3ce21c6dfe8f7442c8bdf7b75cc3667527f0d9','Sandra','Puerta Brand','Av 15 # 122 73','sandra.brand6501@hotmail.com','1965-05-08',2,472,0.00,'46639','3134213715',103,'2014-06-29 20:49:11',NULL,0,NULL,NULL,0),(1181,'Jafc08','42270a091768e80e1e02f81ee767f7292c348831','Julin Andrs ','Falla Castilli','Calle 97 # 70-90 int 5 apt 104','jafc08@gmail.com','1992-05-04',2,472,0.00,'89207','3108146287',103,'2014-06-30 18:05:28',NULL,0,NULL,NULL,0),(1182,'Hentuna','ab0444198e404e95da9c928f8ea7bcb7c2788721','Héctor Andrés','Pereira Vesga','Calle 147 # 7B-92, torre 2, apto 403','hector.andres.pereira@gmail.com','1977-02-02',2,472,0.00,'67198','3168755957',103,'2014-07-01 00:27:14',NULL,0,NULL,NULL,0),(1183,'jeny mart','921713da96740bec7028aef4ca4548ee5c03f8e4','jeny','martinez','crra 88 62#65','jekaju15@hotmail.com','1986-06-30',2,472,0.00,'19263','3166558902',103,'2014-07-01 15:40:25',NULL,0,NULL,NULL,1),(1184,'Mariboterob','51cffbe765b9f1acac3fa6751b13a717b024fcdb','Mariana ','Botero ','Carrera 16 número 85 - 12','mariboterob@hotmail.com','1991-03-28',2,472,0.00,'00761','3144703648',103,'2014-07-01 21:20:16',NULL,0,NULL,NULL,0),(1185,'omarpe','679061cce519dddcf212262cb80c19dc840b3750','omar','pena','cll 26 # 69 d - 91','omarpe_@hotmail.com','1974-10-02',2,472,0.00,'20710','3103204072',103,'2014-07-01 23:46:44',NULL,0,NULL,NULL,0),(1186,'maoromero','40a2515249febef1b455ee603b0c08ba0b984c5b','Mauricio','Romero','Cll 23C 72B 51 Apto 212','maoromero@yahoo.com','1974-12-20',2,472,0.00,'01405','3174314682',103,'2014-07-02 01:32:17',NULL,0,NULL,NULL,0),(1187,'amacias','1092ce9f5503d7d84846c9e316f464b73974a24f','Andrés','Macías','Cra 19A No. 107-41','andresmaciastolosa@gmail.com','1982-01-27',2,472,0.00,'88359','3157782890',103,'2014-07-02 18:55:39',NULL,0,NULL,NULL,0),(1188,'david624','cca060eab0ce87b297c9af653a8f6bff3d10aa4f','cristian david','fajardo villamil','cll 167 bbis # 16b -44','zharadavid@hotmail.com','1991-10-31',2,472,0.00,'82481','3192026993',103,'2014-07-02 19:38:27',NULL,0,NULL,NULL,0),(1189,'Alexagarci','5b18bcd0ddef6f6dfc709c65041e75e27da40b9d','Alexandra','García','Calle 12 # 1 17','alexagarci@gmail.com','1973-10-31',2,472,0.00,'25462','3132661635',103,'2014-07-03 01:46:15',NULL,0,NULL,NULL,0),(1190,'cnino','948608819e7821ac2a69b3871a34b4d9b6a881e8','Carlos ','Niño Duran ','Cra 9A # 96-21 Apto 206','cnd@cable.net.co','1969-09-11',2,472,0.00,'73188','3104841452',103,'2014-07-03 03:38:57',NULL,0,NULL,NULL,0),(1191,'Olgiraldo','9df8ea843c0ac364aabd67b610caa3ef302b89b5','Olga','Giraldo','Carrera 14C # 157-91','ogir100@gmail.com','1970-10-30',2,472,0.00,'46252','3114756223',103,'2014-07-03 06:28:45',NULL,0,NULL,NULL,0),(1192,'Ray219','99e7c7ddffa920ff93a49bc3694b38da8f399773','Betty yaneth','Caycedo parada','Cl 167 no. 56 -25 int 13 apto 403','bcaycedo@hotmail.com','1965-12-14',2,472,0.00,'51266','3112104705',103,'2014-07-03 15:24:16',NULL,0,NULL,NULL,0),(1193,'Helyrueda','e49849c72511572f5db70c08806ebe1009093c92','Helibeth','Rueda Quijano','Calle 93 bis no 19 - 85','helibethrueda91@gmail.com','1991-05-04',2,472,20000.00,'01392','3182194666',103,'2014-07-03 18:06:34',NULL,0,NULL,NULL,0),(1195,'City8','5b253c0a9425ded74174abea2eb9a7fc233034d0','City8','City8','Bogota','city8@mail.com','1980-07-04',2,472,0.00,'64334','3258445',103,'2014-07-04 15:51:25',NULL,0,NULL,NULL,1),(1201,'amea','e7498daaf59c9bcbdb1c4d8c3aa0e94098c08d53','Ana Maria','Escobar','cra 46 94-72 ','ana.escobar.alvarez@gmail.com','1992-02-21',2,472,10000.00,'57834','3114503362',103,'2014-07-04 21:16:22',NULL,0,NULL,NULL,1),(1202,'Juank-gonz','cdcf0824c0d127cb6d38a2ab4382e64cd874805e','Juan Carlos','González G.','Cra 27# 45A-25','gonzlez.juancarlos@gmail.com','2014-07-24',2,472,0.00,'08302','3153065597',103,'2014-07-05 01:41:54',NULL,0,NULL,NULL,0),(1203,'faco14','890979c38c48c20229ecf187f4d3151ab4d226b0','Fabian Andrés ','Cordero Villalba ','Cra 8a 151-95 ','faco14@hotmail.com','1981-05-17',2,472,0.00,'71237','3165755568',103,'2014-07-05 17:53:54',NULL,0,NULL,NULL,0),(1204,'urungeler','23d5b9e7c23b446f9d37d8672aed100cd4ef8222','Udo','Rungeler','Calle 119 #72B - 60','urungeler@gmail.com','1941-05-19',2,472,0.00,'19416','3125211299',103,'2014-07-05 21:08:37',NULL,0,NULL,NULL,0),(1205,'Djordonezc','6a336772f9af64a44a0559dd7f9dfc0551542c47','Diana','Ordonez','11','diana.ordonez@gmail.com','1980-04-27',2,472,0.00,'06057','3005548526',103,'2014-07-06 05:08:35',NULL,0,NULL,NULL,0),(1206,'jaja','cc61a0f386a2877e3e27587fa086d2945b97c811','jaja','jeje','jaja','jaja@jaja.cl','2014-07-06',2,472,0.00,'76842','3008484848',103,'2014-07-06 12:05:41',NULL,0,NULL,NULL,0),(1241,'Cfeliper','268e9f680c250eece9420e268fb0aa4021a5afaf','Carlos Felipe','Rocha Arango','Cra 55 153 15 ap 1101 int 1','cfeliper@hotmail.com','1980-05-20',2,472,0.00,'61513','304-380-3125',103,'2014-07-07 15:40:36',NULL,0,NULL,NULL,0),(1242,'91525254','1583579c8c6a01fa9842fe27ecf3e8f221639071','Fabian','Piedrahita','PabloVI bloque 84 interior 4 apto 502','fabianpiedrahita@gmail.com','1984-01-11',2,472,0.00,'08188','3005535506',103,'2014-07-07 18:31:44',NULL,0,NULL,NULL,0),(1243,'Adrimj28','4a63a7047c2b5e0d3fd0402aac77255283f54673','Adriana','Mejia','Calle 64n #4n-35 apto 801c','adrimj28@gmail.com','1986-04-10',2,472,0.00,'39582','3212139283',103,'2014-07-08 02:02:43',NULL,0,NULL,NULL,0),(1244,'duvianlopez','5694f78c674acd43dcea0a4616e968df5f59a456','Duvian','Lopez','calle','duvianlopez@gmail.com','1985-03-22',2,472,0.00,'42217','3125121785',103,'2014-07-08 13:34:05',NULL,0,NULL,NULL,1),(1245,'mirandage','ed32dfacd6aa5b9f7863c890372a08bc0938de67','Gustavo Enrique','Miranda Romero','carrera 14a #119-83 apto 301','mirandage@mail.com','1967-08-13',2,472,0.00,'46882','3107875441',103,'2014-07-08 18:17:27',NULL,0,NULL,NULL,1),(1246,'diedmapo','828da273e6733d35d0fcb95b73813f19cac8a04c','Diego','Malagon','cra 0#0-0','diedmapo@yahoo.com','1979-01-13',2,472,0.00,'67702','3003602885',103,'2014-07-08 19:46:25',NULL,0,NULL,NULL,0),(1247,'sebastiangc8','2f08ad41d6eca4f037f50ae7e06785bfcbbf7e3e','Juan Sebastian','Guerrero','Calle 155 No. 14-10 Casa 62','sebastian_guerrero8@hotmail.com','1990-12-27',2,472,0.00,'66827','3015112904',103,'2014-07-09 02:13:42',NULL,0,NULL,NULL,1),(1248,'tucaso','424dbb6ee482d403880c57e077a1af53cb928e53','Tulio','Carrero Soto','Cll 107 A # 11A-50 Apto 1105','tuliocarrerosoto@hotmail.com','1964-02-16',2,472,0.00,'27582','3174345720',103,'2014-07-09 02:17:18',NULL,0,NULL,NULL,0),(1249,'Akenaton71','1088340c027359281e5352d334f376c866b8af05','Luis Fernando ','León López ','Calle 160 No. 72-51','lleonlf@gmail.com','1971-12-15',2,472,0.00,'33597','3212328036',103,'2014-07-09 03:57:35',NULL,0,NULL,NULL,0),(1250,'clauszajo','ebe05cfd3bbf81f4b71de6216d619f289a4fa193','Claudia','Szajowicz','Carrera 11#140-41','cbszajowicz@gmail.com','1984-01-03',2,472,168000.00,'28318','3112082477',103,'2014-07-09 12:21:59',NULL,0,NULL,NULL,0),(1251,'Danielph','6857b8e82c5e03a4963e16557d285a4b97c69a44','Daniel','Pinto','Bogotá ','danielpinto20@gmail.com','1989-03-04',2,472,0.00,'94957','3118511014',103,'2014-07-09 20:47:22',NULL,0,NULL,NULL,0),(1252,'Jbazzani','af88919f782349c8c8c8f39a674a18c33ee744cb','Juan Pablo ','Bazzani','Carrera 18a # 112 04','jbazzani@yahoo.com','1981-02-19',2,472,0.00,'71977','3112242367',103,'2014-07-09 22:01:25',NULL,0,NULL,NULL,0),(1253,'OrlyMedina','c55f710ad4fa8195c5c2cec2059dc3a869f6f8e0','Orlando ','Medina ','Calle 80 # 8-41','orlymedina@gmail.com','1951-01-28',2,472,0.00,'79155','3187945670',103,'2014-07-09 22:24:11',NULL,0,NULL,NULL,0),(1254,'jcrueda','f97c87110822d371096ea0513badfc4a48c11db6','Juan','Rueda ','KR 6a # 51 A 27','juanc.rueda@gmail.com','1959-11-06',2,472,0.00,'98960','3002136538',103,'2014-07-09 23:00:17',NULL,0,NULL,NULL,0),(1255,'Lomelin','b7a531898d0f77dac8b69eb33deef5a780b0c718','Diego','Lomelin','Cr 7f 155 32 apto 401','diegolomelin5@hotmail.com','1980-06-12',2,472,0.00,'49496','3166901820',103,'2014-07-09 23:50:48',NULL,0,NULL,NULL,0),(1256,'herneyder','214aa53a46ec9fe87f65d4470f2e281cf851ad5c','Herneyder','Rosales Rodriguez','Crr 11 No 45C 54 ','herneyder@gmail.com','1982-10-16',2,472,0.00,'80504','3143674026',103,'2014-07-09 23:58:27',NULL,0,NULL,NULL,1),(1257,'kellycifu','f0f85163a91a859e3f079232171b05926f18d032','kelly','cifuentes','cll 55 7-51','kchaux@hotmial.com','1987-03-12',2,472,0.00,'27955','3163219844',103,'2014-07-10 00:01:24',NULL,0,NULL,NULL,0),(1258,'roguzman','30d11ace501b01284fdf45df2877593d217dea26','Roberto','Guzmán','Calle 75 ','guzmangomezroberto@yahoo.com','1975-05-03',2,472,0.00,'13092','3183640482',103,'2014-07-10 00:27:12',NULL,0,NULL,NULL,0),(1259,'lizarazojm','d71a0b2f93ccfd70724e7249dd39be2c10177b30','Jorge','Lizarazo','calle 183 16-71','jorge.lizarazo@outlook.com','1979-10-22',2,472,0.00,'93966','3115747659',103,'2014-07-10 02:01:18',NULL,0,NULL,NULL,0),(1260,'gyllermo','8a80a1dc82b9137d7698125b4a5e3f77d89fd367','Guillermo','Parra','kra 11b #123-71 apto 904','gylle1984@gmail.com','1984-10-09',2,472,0.00,'25391','3102833945',103,'2014-07-10 02:38:29',NULL,0,NULL,NULL,0),(1261,'egiovanni6','2daed95cc2a15d38b25533d931a4949167b85fa1','Edgar Giovany','Zuleta Parra','calle164 # 16 c 15 interior 1 apartamento 102','egiovanni6@gmail.com','1967-07-11',2,472,0.00,'53913','3203897723',103,'2014-07-10 03:01:10',NULL,0,NULL,NULL,1),(1262,'ceerar','262933473662d73b2c4a6e9c1fbad872a2170e47','César','Rodríguez','Carrera 28A # 39A-25','ceerar@gmail.com','1986-06-12',2,472,0.00,'74861','3013707923',103,'2014-07-10 04:41:27',NULL,0,NULL,NULL,0),(1263,'fego1805','d00b7f27256c942edda2d5ae87a5968201442ea8','fernando','gomez','calle 99 # 99 - 45','fego1805@hotmail.com','1960-05-18',2,472,0.00,'72696','3153315720',103,'2014-07-10 11:44:06',NULL,0,NULL,NULL,1),(1264,'ca_suar','dc8f9b0a118ca2c049074c0268940bb5d2201e58','Carlos','Suarez','Diag 92 # 16A - 42','ca_suar@hotmail.com','1989-12-08',2,472,0.00,'72703','3187080796',103,'2014-07-10 12:39:19',NULL,0,NULL,NULL,0),(1265,'jorpena','6c216effb50bb3a665dda61212731a98556878a3','Jorge','Peña','CL 92 11A-36','jorge@systegic.com','1976-11-29',2,472,0.00,'22823','3124318053',103,'2014-07-10 12:51:08',NULL,0,NULL,NULL,0),(1266,'sevaso','82fb69e278f2932f5335244907864163ef77d02e','Sergio','Valencia Solano','Calle 116','sevaso@yahoo.com','1963-03-11',2,472,0.00,'39760','3207978003',103,'2014-07-10 13:20:12',NULL,0,NULL,NULL,0),(1267,'cmontealegrb','20327e25db11d316bdaefb08d2256c6de2a0fe09','camilo andres','montealegre badillo','calle 8 bis a # 79c - 40','camilo.montealegre@hotmail.com','1985-02-17',2,472,0.00,'95864','3108033069',103,'2014-07-10 13:44:42',NULL,0,NULL,NULL,1),(1268,'juandrp','ec41621409a9ff4e52ee57492f59ffac7cba808b','Juan Diego','Ruiz','Av Cl 82 # 12 - 18','juandrp@gnail.com','1987-09-08',2,472,0.00,'71229','3153627612',103,'2014-07-10 20:08:54',NULL,0,NULL,NULL,0),(1269,'gabriel.p16','a6f83442001f63e0cbdc5b81f9628008239bfeed','Gabriel','Peña','Calle 173 # 19-75','gabriel.p16@gmail.com','1988-01-15',2,472,0.00,'30644','3003599155',103,'2014-07-10 22:22:41',NULL,0,NULL,NULL,0),(1270,'Pilar','af1db97096cc099570b9a3344bd0647337ff880f','Pilar','Franco','Cra 87 d bis # 54 - 16 sur','nana144@gmail.com','1985-10-12',2,472,0.00,'83453','3185334021',103,'2014-07-10 23:26:27',NULL,0,NULL,NULL,0),(1271,'a82un','b4385e549e3ccde9ef26a9bbb53e3ebe3266e717','Angel','Vargas','Cra 13 A # 29 - 24','angel82un@gmail.com','1982-06-27',2,472,0.00,'01359','3212054074',103,'2014-07-10 23:27:17',NULL,0,NULL,NULL,0),(1272,'Cesar','b9081666b345bec8228cb23dc3ad21ebf40aada5','Cesar ','Zuluaga','Calle 111 12-34','cesarzulu@hotmail.com','1964-08-23',2,472,0.00,'28846','3214903652',103,'2014-07-11 01:07:26',NULL,0,NULL,NULL,0),(1273,'Vicko','8cb2237d0679ca88db6464eac60da96345513964','Victor','Sua','Calle 128 No 94f 14','vicsua80@hotmail.com','1980-01-05',2,472,0.00,'31974','3174323014',103,'2014-07-11 01:24:44',NULL,0,NULL,NULL,0),(1274,'Jguerrerog','88256c400841ba3e9276be93cd1b11dfbe4bc6ec','Juan','Guerrero','Carrera 82 # 9a sur. 28','pablojuanguerrero@hotmail.com','1979-06-03',2,472,0.00,'50429','3012016301',103,'2014-07-11 01:32:05',NULL,0,NULL,NULL,0),(1275,'Yagorubio','966d8e682bee03c76771e5923276e457548a863d','Jaime Andres','Rubio Carvajalino','Cll 107a 7a 81','yagorubio@gmail.com','1977-06-22',2,472,0.00,'93968','3158853391',103,'2014-07-11 01:49:54',NULL,0,NULL,NULL,0),(1276,'FAENCARA','08e84072f08d7cca475740b13f88d1da91f5b464','FABIAN ENRIQUE','CAMPO RAMIREZ','Calle 11a # 79a -60 ','fabianramirez_21@hotmail.com','1986-07-18',2,472,0.00,'45278','3144885141',103,'2014-07-11 01:53:18',NULL,0,NULL,NULL,1),(1277,'dicorder','09ab076e1eb4eaf402fce06c7ff272b54864ca79','Diego','Cordero','cra 123 no 13c - 75 ','diego.cordero.516@gmail.com','1986-05-16',2,472,0.00,'21222','3184344934',103,'2014-07-11 02:14:00',NULL,0,NULL,NULL,1),(1278,'tuxnet','5c2f3b604837d97547eacf661463dc5855d31e3a','Andres','Luquetta','Cra 28A No. 4A-21','galuquetta@gmail.com','1982-11-08',2,472,0.00,'69587','3012858477',103,'2014-07-11 03:34:07',NULL,0,NULL,NULL,1),(1279,'mauricio08','d233d920120404e325ca9e33cdf66726603bd664','javier mauricio','reyes toro','CARRERA 3 ESTE # 46- 31 SUR','mauricioreyestoro@gmail.com','1975-11-08',2,472,0.00,'99711','3112313478',103,'2014-07-11 03:56:16',NULL,0,NULL,NULL,1),(1280,'Dario','5954703537f5c0f97302832ebbee313c70d6a985','Dario',' Beltran','calle 140 # 6-10 apto 201 torre 15','dariobeltrangomez@gmail.com','1969-02-27',2,472,0.00,'24032','3176486990',103,'2014-07-11 09:49:56',NULL,0,NULL,NULL,1),(1281,'Marianohdez','86ccc968d8c35a0b368ef7302a480d7b8d2c50cc','Mariano','Hernández','Cra 14 90 65','mariano.hernandez@mac.com','1971-08-15',2,472,0.00,'36958','3102531895',103,'2014-07-11 12:08:49',NULL,0,NULL,NULL,0),(1282,'bpatarroyo','5ce1c39a0cd4b451b78094a12a484501473b644e','Baudilio','Patarroyo Patiño','K 48 174b 67 IN 5 AP 518','bpatarroyo@gmail.com','1956-06-25',2,472,0.00,'78938','3108154016',103,'2014-07-11 12:51:02',NULL,0,NULL,NULL,0),(1283,'sachemas','3c9e20a0517243cfe5dbe05db19758103b5ae545','Sergio Alejandro','Chemás Vélez','Calle 82 # 9 - 79','sachemas@gmail.com','1992-01-31',2,472,0.00,'05021','3118472009',103,'2014-07-11 13:26:17',NULL,0,NULL,NULL,0),(1284,'andreacham','ed6cc5e87d0a5fca51412100d280540b0fa735dd','andrea','chamorro ladino','carrera 58 n 119a - 98 torre 11 apt 534','heyderandreachamorro@gmail.com','1986-03-07',2,472,0.00,'00437','3105705240',103,'2014-07-11 14:50:13',NULL,0,NULL,NULL,1),(1285,'tutomania','48be9328713780fd10caa4ef83a52c1c22ede34d','humberto','solorzano chaux','bogota','tutomania@hotmail.com','1989-09-29',2,472,0.00,'93272','3184560125',103,'2014-07-11 15:39:12',NULL,0,NULL,NULL,0),(1286,'Alexblanco','737af81e4be5629366224fd5e396705061507a9c','Alex','Blanco','Cr 8 No 67-51','alblancor@gmail.com','1970-02-15',2,472,0.00,'63755','3164688242',103,'2014-07-11 19:47:42',NULL,0,NULL,NULL,0),(1287,'valenpaez','dedc16355159651fd2d346f0410ca1b5a0d6e2df','Valentina','Páez','pablo VI','valenpaez12@hotmail.com','1987-09-12',2,472,0.00,'30495','3006583957',103,'2014-07-11 20:14:18',NULL,0,NULL,NULL,0),(1288,'caritocuervo','04f0bdd0c53be0293697a4c3404a6b531b246456','Carolina','Cuervo','Cra 54D # 135 - 15 Apto 603 Int 1','cariticuervo@gmail.com','1984-06-07',2,472,0.00,'47270','3002250416',103,'2014-07-11 20:43:56',NULL,0,NULL,NULL,1),(1289,'Ximena','a4a8415267205adb01478e82ceebb39cd9778951','Sandra Ximena','Olaya Florez','Cra 5, 60-91','ximenaolaya@hotmail.com','1972-09-19',2,472,0.00,'82894','3208505771',103,'2014-07-11 21:28:45',NULL,0,NULL,NULL,0),(1290,'Ruizlmc','c931bf4fe2fabcb128747cbe1c1dc87812d9ad53','Jorge','Ruiz','K7 72 64','ruizlmc@yahoo.com','1958-01-16',2,472,0.00,'79724','3186960459',103,'2014-07-11 21:29:52',NULL,0,NULL,NULL,0),(1291,'cjimcol','126ae95715160ac551c05b45e0c66da722e2c331','C.','J.','..','cjimcol@yahoo.com','2013-07-11',2,472,0.00,'54306','300000000',103,'2014-07-11 23:25:39',NULL,0,NULL,NULL,0),(1292,'Albesand','2b81c8ad257822ad70448283aaeaa4491e242833','Albeiro ','Sandoval g','Calle 50 sur 78 h 36','albe.sand@gmail.com','1983-08-12',2,472,0.00,'47144','3164694273',103,'2014-07-11 23:53:59',NULL,0,NULL,NULL,0),(1293,'Fyquis','39cabcd77c65c672ba734a36eda5c6adbf2ee898','Fanny','Cuesta','Carrera 68K #39-52 sur','Fyquis@gmail.com','1980-04-12',2,472,0.00,'27222','3043924959',103,'2014-07-12 00:38:56',NULL,0,NULL,NULL,0),(1294,'maomar88','fb2267df0203342eafcfc20df8da43bf856d50f3','mauricio','ardila','calle 74 d n 70 g 73','oscardila@gmail.com','1988-06-15',2,472,0.00,'75213','3132021363',103,'2014-07-12 16:15:17',NULL,0,NULL,NULL,0),(1295,'clizarazo','ab93eba9830bc4c143ee8fab21eb2150f3dc2b37','Carolina','Lizarazo ','kr 44 22 48','clizarazo@gmail.com','1982-11-04',2,472,0.00,'78920','3212013823',103,'2014-07-12 20:07:34',NULL,0,NULL,NULL,1),(1296,'jariasruiz','a6860cdcbbf0cb7c3d4de6afa807afe83a2cea81','Jairo','Arias','Calle 24f #85b-85 int 2 apto 411','jariasruiz@gmail.com','1972-10-18',2,472,0.00,'73098','3184455324',103,'2014-07-13 01:52:49',NULL,0,NULL,NULL,0),(1297,'Mimoralesmo','812a8b38a4d480611fed35b674e5e30b90db7785','Martha ','Morales','Calle 155 # 9-45 apto 1103-3','mimoralesmo@yahoo.com','1977-04-26',2,472,30400.00,'12495','3006099292',103,'2014-07-13 18:36:15',NULL,0,NULL,NULL,0),(1299,'nsanabria','d5c438a3cfc5ad73466c6a79c8ee67731579e43b','Nataly Vivian','Sanabria Mancipe','Calle 72 # 88-44','natysanabria.ma@gmail.com','1988-04-19',2,472,0.00,'39987','3002896481',103,'2014-07-13 22:46:42',NULL,0,NULL,NULL,1),(1300,'ChikenFast','226d19cc2da9260d531a52ec412e783acf8d3eb8','Jaime','Quintero','Calle 39 # 29 - 86','quinterojaime@gmail.com','1979-09-02',2,472,0.00,'33068','3108091419',103,'2014-07-14 02:01:11',NULL,0,NULL,NULL,0),(1301,'hernancruz','176b7e288c04b7801543d26dc3379579cc325d45','hernan','cruz','calle80#46 a 10 ','hernancruz119@gmail.com','1991-12-12',2,472,0.00,'80808','3014228511',103,'2014-07-14 02:50:23',NULL,0,NULL,NULL,0),(1302,'jorgearenasa','53e288df1b032020eeee934171bb5b05142cfe7a','Jorge','Arenas','Calle 111 No 45 A 70','jorge.arenas@etb.net.co','1960-07-07',2,472,75000.00,'15507','3164735991',103,'2014-07-14 04:08:49',NULL,0,NULL,NULL,0),(1303,'alvaromos','1e41c981637834caec149b4d33f7f8566076ddfa','Álvaro','Mosquera','Diagonal 74 No. 5-28','adelicus@live.com','1980-11-12',2,472,0.00,'33307','3188390888',103,'2014-07-14 21:18:46',NULL,0,NULL,NULL,0),(1304,'Jhon575','d8216354e82b679b49f022fcd121ae41b3e765f5','John ','Orduna ','Calle 1 e # 13-10','jhon575@hotmail.com','1979-05-20',2,472,0.00,'77688','3107583268',103,'2014-07-15 03:50:52',NULL,0,NULL,NULL,0),(1305,'guschapa','cf35b508311fa2d4071c5c6e59e04ef2388d17ea','Gustavo','Chaparro','Av. Calle 82 No. 12 - 18','guschapa@gmail.com','1984-05-09',2,472,0.00,'68448','3214682319',103,'2014-07-15 18:11:28',NULL,0,NULL,NULL,0),(1306,'joseisaac','3cc253739cc1f62166e4fa9cafff1c939047b55f','jose isaac','serrano','cra 54d# 134-50 torre5 apto 701','joseisaac@terra.com','1969-09-18',2,472,0.00,'90989','3184559436',103,'2014-07-15 19:31:16',NULL,0,NULL,NULL,1),(1307,'pilinq','5636a9f1406fae2352d18be8126ec0af2198ad42','Pilin Andrea','Quintero Molina','calle 146 # 12a -70 apto 303','andre_tero@hotmail.com','1983-06-17',2,472,65800.00,'45434','3002919704',103,'2014-07-16 02:15:41',NULL,0,NULL,NULL,1),(1308,'carrirri','7c4a8d09ca3762af61e59520943dc26494f8941b','cristian','carrillo perez','calle 73b #45 -21sur','carrirri@gmail.com','1993-04-05',2,472,0.00,'67587','3004446530',103,'2014-07-16 03:06:14',NULL,0,NULL,NULL,0),(1309,'melo6287','a8751534270d1cb11d697b0da8d980c418c2953f','jorge enrique','melo vargas','carrera 15 135-41','jorgemv_77@hotmail.com','1977-07-07',2,472,0.00,'32287','3016594289',103,'2014-07-16 06:50:59',NULL,0,NULL,NULL,0),(1315,'Guollep','8759e25fb9d3b37bdb82b50ab0739f7e7a8a3c95','Guillermo','Arboleda','Ave 82 No. 8-61','garboledav@gmail.com','1950-01-09',2,472,0.00,'54260','3138885905',103,'2014-07-16 06:58:29',NULL,0,NULL,NULL,0),(1316,'Fabro','77557f228e850157daa17ff690c0dbd03577593b','Fabricio','Alarcon','Calle 58a 52a 27 apto 402','fabro.alarcon@gmail.com','1974-10-26',2,472,0.00,'86575','3115323799',103,'2014-07-16 15:59:00',NULL,0,NULL,NULL,0),(1317,'Cbarraquer','b109b7bde1bd44afc9e11da5553442f4e2f9815f','Camila','Barraquer','Calle 101 no 16-26','camilabarraquer@3minds.com.co','1978-03-15',2,472,0.00,'14138','3202359388',103,'2014-07-16 16:18:09',NULL,0,NULL,NULL,0),(1318,'JennLeibo','896f579d268645a3fdca77467f4208a47677e9d1','Jennifer ','Leibovici Caro','Cll 126a # 7c - 88 ','galvanica2009@hotmail.com','1981-03-15',2,472,66800.00,'96763','3138295074',103,'2014-07-16 17:44:12',NULL,0,NULL,NULL,0),(1330,'Leofderdz','61edfd1a0b93c8f4a694c210d93b4f151d71b339','Leonardo Federico','Rodríguez ','Cra 4A 29-18 201','leo.fde.rod@me.com','1978-04-29',2,472,0.00,'56671','3208541834',103,'2014-07-17 01:26:28',NULL,0,NULL,NULL,0),(1331,'Andyv91','cb0bd4bafd90263294e7af2709204c54242f2d03','Andrés ','Valderrama ','Calle 163 # 62-71 ','avalde_91@hotmail.com','1991-12-17',2,472,0.00,'12052','3202302720',103,'2014-07-17 05:23:40',NULL,0,NULL,NULL,0),(1332,'mgaleano','4ef3202fbb86aa924cdac6b7c1647e06b49d0644','Marcello','Galeano','Cra 13 #79-30','mgaleano45@gmail.com','1980-08-23',2,472,0.00,'98607','3153017048',103,'2014-07-17 19:42:32',NULL,0,NULL,NULL,0),(1333,'Robertocano','485a8bd25c376823a0cf2bf47292218677fb9f03','Roberto','Cano Vejarano','Avenida carrera 20 # 83-72 ap. 401 ','contacto.cano@gmail.com','1973-02-05',2,472,73450.00,'18454','3102109048 ',103,'2014-07-17 23:32:25',NULL,0,NULL,NULL,0),(1334,'jovannyr84','ccffb2e4d96d9bfeb03a45b4a1e14bd389f4b039','Jovanny','Romero','cr 15 # 28b','astarothjra@hotmail.com','1984-02-24',2,472,0.00,'38367','3114456997',103,'2014-07-18 04:49:34',NULL,0,NULL,NULL,0),(1335,'felodiaz','07757e4b9ea4a13a94ee1a09c69e815031242603','Felipe','Diaz Castellar','Calle 55 6-17 ap 602','felodiaz@msn.com','1986-08-25',2,472,0.00,'45339','3176450213',103,'2014-07-18 19:04:03',NULL,0,NULL,NULL,0),(1336,'Lariza ','6d03a08780168d62fc9adde0707d079d9868c10c','Lariza','Pizano','Calle 70A 4 27 ','pizanolariza@hotmail.com','1977-11-07',2,472,96200.00,'76278','3043838291',103,'2014-07-18 19:21:31',NULL,0,NULL,NULL,0),(1337,'monitoreo','5a074a130d230a3a80f4a69f03272f08e8ea8331','cctv','city-parking','calle 103#14a53','monitoreo@city-parking.com','2000-08-20',2,472,0.00,'21865','3164783849',103,'2014-07-19 16:35:25',NULL,0,NULL,NULL,0),(1338,'Omnipecas','e7c0e4873fdccf3f2e17f2c9e84500f8de336771','Eduardo','Urrego','Cll 155 #9 45 ','edo289@hotmail.com','1967-10-17',2,472,0.00,'54999','3124515250',103,'2014-07-20 16:07:25',NULL,0,NULL,NULL,0),(1339,'Joselito8511','4607e65e121acd49a620372857306ad338d43f21','Jose David ','Rojas Amaya ','calle 12C # 71C - 31 Torre 6 apto 202','josedavid8511@hotmail.com','1986-11-02',2,472,0.00,'93795','3125598236',103,'2014-07-20 23:18:07',NULL,0,NULL,NULL,1),(1340,'Willser','73de1fc984f49a6233985f5aea572ad85c650fbe','Willser Fernando','González Calderón','Bogotá','willser@gmail.com','1974-10-17',2,472,0.00,'53809','3183061485',103,'2014-07-21 05:30:20',NULL,0,NULL,NULL,1),(1341,'AndreaVale','f0aa168561c7c294d4d1271de02269164b4b9c59','Andrea','Valencia','Cll 65 no. 4A - 01','andreavalencia10@gmail.com','1983-10-09',2,472,50000.00,'21924','3177258828',103,'2014-07-21 20:06:38',NULL,0,NULL,NULL,1),(1342,'ariel','2ec93778b927447afbde07a6b5db740b8c5980eb','Ariel','polania','cll23d8651','arielrp2000@yahoo.com','1975-01-06',2,472,0.00,'64127','3114708804',103,'2014-07-21 21:46:35',NULL,0,NULL,NULL,1),(1343,'daeral','530f17f1e01ef91fc6b2f45f59002554b4205725','Daniel Ernesto','Alfonso Montanez','Calle 100 cra 9','daeral2002@yaho.com','1977-07-06',2,472,0.00,'58537','3176486266',103,'2014-07-21 22:48:49',NULL,0,NULL,NULL,1),(1344,'monigarcia ','615923d86676636fc71d02a42c09350eb61e9948','Monica ','Garcia ','Calle 74#4-59','monic_sg@hotmail.com','1993-10-22',2,472,0.00,'33556','3015109819',103,'2014-07-21 23:56:34',NULL,0,NULL,NULL,0),(1345,'luchoraman','e2a1a159b8fb42a1fd874f5edcbb08707df02a92','john','ramos','cr 11 No 99-14','johnfre9@gmail.com','1977-12-08',2,472,0.00,'49657','3013701676',103,'2014-07-22 01:31:51',NULL,0,NULL,NULL,0),(1346,'alvaromontes','1592f782f5e56ef2a99c1cc7e78b8d7f75b02e35','Alvaro','Montes','Calle 9 A 14 F 28 Chia','alvaro.montes@yahoo.com','1964-11-13',2,472,31000.00,'75355','3012098960',103,'2014-07-22 14:17:04',NULL,0,NULL,NULL,1),(1347,'aleogp','94c99ed2bb2dd3549137bf5973ecc1fe9411ef3a','Andres','Guerrero','Av callé 116 # 54 - 34 apt 103','aleogp@hotmail.com','1980-07-12',2,472,0.00,'64105','3005631953',103,'2014-07-22 14:50:08',NULL,0,NULL,NULL,0),(1348,'mapinz','01f3f5c87126ebc925d770f8ebd3327f140e8c7a','Martha Patricia','Pinzon Torres','Cra.15 B No. 114 A-50 Oficina 101','mapinz@gmail.com','1967-01-07',2,472,0.00,'03811','3214497776',103,'2014-07-22 17:51:14',NULL,0,NULL,NULL,1),(1349,'nikolasg','24a962afb0ec9d9572e0cb5562a143ae6eb2316e','Nicolás Alejandro ','Gordillo Sanchez ','cra 54 d 134-50','drywallbogota@gmail.com','1982-12-18',2,472,0.00,'61746','3165287993',103,'2014-07-23 00:51:09',NULL,0,NULL,NULL,1),(1350,'Dianacoralg ','5d1051bc66ae321676146b83c5ee7cb0039ae280','Diana','Coral Guerrero','Calle 113 # 17A - 58','dicoguerrero@hotmail.com','1987-01-05',2,472,0.00,'96255','3132107259',103,'2014-07-23 01:09:52',NULL,0,NULL,NULL,0),(1351,'paulasopo','91a12b3f9b90ed9c4a3fe577e5420deb69582420','Paula','Sopó Quintero','Calle 119A # 57 - 60','paulasopo@gmail.com','1990-07-30',2,472,0.00,'58169','3102472048',103,'2014-07-23 01:31:12',NULL,0,NULL,NULL,0),(1352,'aast93','5173ebda74083c8047103d5a00824d4eae6ef361','andres Alberto','sales Tovar','dg 89A #115-50 int 9 ap 204','aast93@gmail.com','1993-09-09',2,472,0.00,'60180','3165215915',103,'2014-07-23 02:09:47',NULL,0,NULL,NULL,0),(1353,'fachs','749df17720995c4453bc3edecd8d743c1d567eb5','fredy','chillon','cr 23 c No 31 - 61 sur','fachs2007@hotmail.com','1982-05-07',2,472,0.00,'55522','3114524086',103,'2014-07-23 02:09:57',NULL,0,NULL,NULL,1),(1354,'rubenmonroy','2891baceeef1652ee698294da0e71ba78a2a4064','Ruben','Monroy','cra','rubashxc@Hotmail.com','1989-03-12',2,472,0.00,'80386','3212710144',103,'2014-07-23 02:39:36',NULL,0,NULL,NULL,0),(1363,'Jquirogab','070db63063fd92aae6c3d4aaf4949c1155e0c38d','Jairo','Quiroga','Cra 79 44 08 sur','jaiquiro@hotmail.com','1978-02-16',2,472,0.00,'52291','3165242450',103,'2014-07-23 13:08:18',NULL,0,NULL,NULL,0),(1364,'danimedy','97423f01c154452e6b75fcbad9a72b5ac7fa5e08','Danilo','Medina','Calle 31 6-39 piso 14','dmedinap@bancolombia.com.co','1976-04-09',2,472,75000.00,'47484','3156322365',103,'2014-07-23 13:47:02',NULL,0,NULL,NULL,1),(1365,'kmbaquero','7ed59552d292d564cbf973fefc01979b0ae6dcc8','karen','Baquero','cra 13 # 33-01 apto 903 Torre 1 ','kmbaquero@hgotmail.com','1986-04-06',2,472,0.00,'53404','3204991836',103,'2014-07-23 15:07:19',NULL,0,NULL,NULL,0),(1366,'scarpini','10928b8bdd04687332f2dc4d7d112c961fdec6bf','Juan Guillermo','Scarpini','Carrera 23A #6-83','scarpini2000@hotmail.com','1983-07-02',2,472,0.00,'86269','3153628779',103,'2014-07-23 15:39:02',NULL,0,NULL,NULL,1),(1367,'Jeflengua ','dc1213f099afc03a05ac2623a4ae50e791765d81','Jeferson ','Lengua ','Cll 7 a bis #78 h 95 ','jef.lengua@hotmail.com','1986-12-12',2,472,0.00,'98695','3115017742',103,'2014-07-23 16:51:46',NULL,0,NULL,NULL,0),(1594,'malvarez437','9c158e68110dc382bc7945c4ff9a644f58fc2d07','Marlene','Alvarez Sanchez','Calle 148 no 7-77 Apto 103','blsa1979@gmail.com','1949-12-16',2,472,0.00,'19700','3153972117',103,'2014-07-23 22:54:40',NULL,0,NULL,NULL,1),(1595,'nelson16q','97cd36deebc8521b5437d09affe58ef94f382162','nelson','quirama','calle 26','nelson16q@hotmail.com','1991-04-12',2,472,0.00,'17669','3052252461',105,'2014-07-24 00:31:17',NULL,0,NULL,NULL,0),(1596,'pedrop','fedf5c34fe89f669977039e584e1a9d2fbb9d0c1','Pedro','Fernandez Limas','calle 31 #6-39','ppfernan@bancolombia.com.co','1957-04-01',2,472,0.00,'43851','3176350729',103,'2014-07-24 03:00:22',NULL,0,NULL,NULL,0),(1597,'Alexblop','1c08eb8f745b495fc8a71723c6142c57e8f796ee','Alexander','Blandón López ','bodegas box','alexblandon@gmail.com','1989-07-09',2,472,10.00,'04286','3014738438',103,'2014-07-24 20:14:48',NULL,0,NULL,NULL,0),(1598,'frmorale','22dcad5d873518374856a3fc3e8d23d8658e86ae','Francisco Javier','Morales Carvajal','Ak 72 24 B 34 apto 101 Torre 4','frmorale@bancolombia.com.co','1969-09-28',2,472,0.00,'70685','3166924550',103,'2014-07-24 20:16:29',NULL,0,NULL,NULL,0),(1602,'cfonseca83','c5c8066d458ef32d2d9d6c641cd90b1f5259ebed','carlos','fonseca','calle 74 # 4 - 28 ','cfonseca832002@yahoo.com','1959-10-31',2,472,0.00,'28805','3102098296',103,'2014-07-24 22:26:32',NULL,0,NULL,NULL,0),(1603,'jmgonzalezj','aaeedf7ff332ff12404d3eb660d4737ca638d3c7','Joan Manuel','Gonzalez Jimenez','Calle 147 No 13 - 67 Apto 319','jmgonzalezj25@hotmail.com','1984-12-15',2,472,0.00,'16215','3003201034',103,'2014-07-25 14:49:27',NULL,0,NULL,NULL,1),(1604,'Julvelto','0015d0367e2331d49b70580f12c5d72b0eaa842c','Julián ','Velásquez ','Calle 64 # 1 - 15 torre 2 apto 803','julvelto@yahoo.com','1982-09-08',2,472,0.00,'51805','3186228856',103,'2014-07-25 21:25:02',NULL,0,NULL,NULL,0),(1605,'misparking','71e152a745808a2f9349aa34ad54c8df8c9628c4','misael','angel','calle 45 A #14-67','marpaty155@hotmail.com','2014-02-07',2,472,0.00,'25739','3143352282',103,'2014-07-26 01:38:26',NULL,0,NULL,NULL,0),(1606,'andreakrol','e2c94897386b667856edf49f54dbc8e2a3043043','Andrea Carolina','Escobar Contreras','cra 50 A bis #41 B - 41 sur','u1801789@unimilitar.edu.co','1992-08-04',2,472,0.00,'44195','3115654097',103,'2014-07-26 11:57:23',NULL,0,NULL,NULL,1),(1607,'alejoypunto','a1b9d421d750a8eea0beab10cc927b644ed6f369','Alejandro','Rodriguez','carrera 110#76-17','alejoypunto@outlook.com','1988-03-17',2,472,0.00,'33379','3103100948',103,'2014-07-26 19:42:37',NULL,0,NULL,NULL,0),(1608,'James06DT','6163a607f08d9cd261e0b1c91f17ade88e131840','Jaime','Duque Tejeiro','Cra45A #104A-14','jaime.duque.tejeiro@hotmail.com','1985-09-06',2,472,0.00,'12740','3168334106',103,'2014-07-28 01:02:59',NULL,0,NULL,NULL,0),(1609,'andrea','9877a64e0a88d2cabd6a6851056cfcf83e4ad616','Andrea','gomez','calle53#46 76','andreagomez0306@hotmail.com','1984-09-15',2,472,0.00,'28828','3125695215',103,'2014-07-28 14:15:55',NULL,0,NULL,NULL,0),(1610,'mayorcae','6d70df01b67d3a2aadcf7a5318edaac966f0b4dc','Nicolas ','Mayorca Escobar','cra 11 #78-22','nicolas@nicolasmayorca.com','1985-12-08',2,472,47600.00,'51620','3003651553',103,'2014-07-28 15:10:39',NULL,0,NULL,NULL,0),(1611,'Freddyle','015ae1ff7ca687a15dce111920ac7db127c36fe6','Freddy','LeÃ³n reyes','Calle 23c No 69 f 65 int 34 apto 302','freddy.leon@unimilitar.edu.co','1970-05-21',2,472,45000.00,'96113','3107966487',103,'2014-07-28 19:19:46',NULL,0,NULL,NULL,0),(1612,'dianavasquez','f26ffc7994c730e62140e5a0fb4cf61100ecd029','Diana','vasquez valencia','cll 61 sur # 97 B66','dianavasquez.v@hotmail.com','1993-07-30',2,472,0.00,'65660','3188292476',103,'2014-07-28 19:24:45',NULL,0,NULL,NULL,1),(1613,'emendez21','426f66749fb07453bec3c17e319ea576dc531fa0','Eder','Mendez','calle 4 sur 12 b 18','emendez21@hotmail.com','1982-09-25',2,472,0.00,'93906','3017860036',103,'2014-07-29 01:26:12',NULL,0,NULL,NULL,0),(1614,'ATEJADA','0c0201243666c71476f165a238c6a95be8ddf8ca','ADRIANA','TEJADA TORRES','Cra 19a no 89-08 apt 201','adriana_tejada@hotmail.com','1972-01-08',2,472,588850.00,'74548','3108584605',103,'2014-07-29 15:36:44',NULL,0,NULL,NULL,0),(1615,'Jcmorales','7384d898840f79db831134d4e6bdfa3efacb402b','Juan Carlos ','Morales Correa','CR 54d 186 85 IN 8 AP 401','jcmoralesc@cable.net.co','1962-10-01',2,472,12000.00,'71367','3208082690',103,'2014-07-29 17:55:09',NULL,0,NULL,NULL,0),(1616,'fabianrojas','e549379b86a77b62592150360a555cd8a4d1bd07','Fabian David','Rojas Guevara','av boyaca # 152b - 90','david10_36@hotmail.com','1992-07-17',2,472,0.00,'64891','3166963864',103,'2014-07-30 04:04:03',NULL,0,NULL,NULL,1),(1617,'27ferbar','de8544acf1220266350a54930f119ad484bbacec','Fernando','Barbosa','Calle 2 a sur # 72b 66','ferbarbosa27@gmail.com','1986-06-27',2,472,0.00,'63176','3182164767',103,'2014-07-30 04:42:09',NULL,0,NULL,NULL,1),(1618,'Rdaranda ','e55464911adb5a667209e90df45ab96b658e5ff7','Rubén ','Aranda','Cr 7 31 10','rdaranda73@hotmail.com','1983-10-01',2,472,0.00,'85509','3204770632',103,'2014-07-30 10:29:25',NULL,0,NULL,NULL,0),(1619,'Gahr','850ad7382e968c5e7e3314db6e3700f588fccf87','Gonzalo','Hernandez','Carrera 5ta #131-90 Apto 1203, Bogotá ','gonzalo.ahr@gmail.com','1975-05-10',2,472,0.00,'01939','3016580329',103,'2014-07-30 14:23:52',NULL,0,NULL,NULL,0),(1620,'9 ','e8c205120a31abd16edd322578768276bfc0004b','Elizabeth','pulido','kra82 bis#71/12','caripooh15@hotmail.com','1987-05-09',2,472,0.00,'44510','3203406823',103,'2014-07-30 15:43:00',NULL,0,NULL,NULL,0),(1621,'natsviquerat','9014a64e6450961226fc19cdc4a039ffbeb4b10d','nathalie','viquerat valencia','cr 71 a # 128-82','nathisvq3@hotmail.com','1980-01-05',2,472,0.00,'43214','3102963512',103,'2014-07-30 16:13:46',NULL,0,NULL,NULL,1),(1622,'oromenu','1d39ad587747b9c15f27b294438dd4145b2a79a9','omar','mendoza','calle 23 carrera 65','oromenu@gmail.com','1995-11-20',2,472,0.00,'71609','3102363178',103,'2014-07-30 16:36:12',NULL,0,NULL,NULL,1),(1623,'andreifu','286c0f95c0615b9636e07e3dead94721f2ab5551','Juan','Perez','Avenida alcachofa, numero 25','andreifu@gmail.com','2003-01-01',2,472,0.00,'67033','87132236',103,'2014-07-30 20:04:04',NULL,0,NULL,NULL,0),(1624,'jucastillo68','7620acf4a667d9d307d7a1b63eda0bd243bffa3a','julio cesar','castillo','carrera 10 c # 36a - 58 sur','jucastillo68@hotmail.com','2014-05-01',2,472,0.00,'66667','3112891829',103,'2014-07-30 20:09:03',NULL,0,NULL,NULL,0),(1630,'Svera','2bbebfe6248117f9ae7717264ee495e8d1f01ab6','Sandra','Vera','Calle 81 8 51','sandramvera@gmail.com','1982-05-04',2,472,0.00,'01027','3138206029',103,'2014-07-30 20:49:06',NULL,0,NULL,NULL,0),(1631,'juanpablo','69dd0e064ea13199d53aa1843ee4d6dbcfa2483a','juan pablo','pedraza herreño','cll 122 b numero 112-40','juanpedraza001@gmail.com','1995-01-22',2,472,0.00,'56567','3192223407',103,'2014-07-30 20:59:48',NULL,0,NULL,NULL,1),(1632,'edwardneira ','feab1fdb66d887b1fd577432476f5509cf0d7f9f','Edward','Neira ','Cra 51 44 F 39','jjunnioor@hotmail.com','1980-11-10',2,472,0.00,'98213','3106975262',103,'2014-07-31 02:22:12',NULL,0,NULL,NULL,0),(1633,'Criss','63e531d07adc9e4e0bedd8e01d161c1e63da03ad','Cristina','Salazar','Carrera 47a #96-41','cristinasalazarceballos@yahoo.com','1977-10-04',2,472,75000.00,'80752','3166901944',103,'2014-07-31 18:58:24',NULL,0,NULL,NULL,0),(1634,'Carlos','07dd36a6f2b834208474ac8baa91321127fd8602','carlos','hincapié','carrera 69f #7A06','carloshhincapie@yahoo.es','2014-07-31',2,472,0.00,'81203','3168746377',103,'2014-07-31 22:30:53',NULL,0,NULL,NULL,1),(1641,'jfjativa','db43317e3256d81112297ae41d7b69f01795fcf3','Juan Felipe','Játiva Silva','Calle 96 #21-33 Edf. GOA Apto. 608','jfjativa@gmail.com','1983-06-12',2,472,0.00,'36647',' 573163391914',103,'2014-08-01 02:42:50',NULL,0,NULL,NULL,0),(1643,'Armax82','d12ea80ee55f2d031cdd9c45395116bb1b16fb2d','Andrs felipe ','Ruiz moreno','Calle 117d # 57-96','armax82@gmail.com','1982-11-03',2,472,0.00,'86296','3132945447',103,'2014-08-01 13:58:19',NULL,0,NULL,NULL,0),(1644,'NATALIEDIA','0a5683dcf514d3e9f363f02491982977f34cf594','NATALIE','DIAZ','Transversal 2 este No 22 a 41','kanadi_99@hotmail.com','1988-06-22',2,472,0.00,'69951','3138131428',103,'2014-08-01 15:06:22',NULL,0,NULL,NULL,1);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `sysuser_hashpasswd` BEFORE INSERT ON `sys_user` FOR EACH ROW begin 
if NEW.origin ='MSJ' then
set NEW.pass = NEW.pass;
else
set NEW.pass = SHA1(NEW.pass);
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `registerPhoneTrigger` AFTER INSERT ON `sys_user` FOR EACH ROW BEGIN
INSERT INTO msisdn_user values(null,NEW.favoriteMsisdn,NEW.idsys_user);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `updatePhoneTrigger` AFTER UPDATE ON `sys_user` FOR EACH ROW BEGIN
if NEW.favoriteMsisdn != OLD.favoriteMsisdn then
  
   UPDATE msisdn_user SET msisdn = NEW.favoriteMsisdn WHERE idsys_user = NEW.idsys_user AND msisdn = OLD.favoriteMsisdn;
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sys_user_client`
--

DROP TABLE IF EXISTS `sys_user_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_client` (
  `idsys_user` bigint(20) NOT NULL,
  `idclient` bigint(20) NOT NULL,
  PRIMARY KEY (`idsys_user`,`idclient`),
  KEY `FK_sys_user_client` (`idclient`),
  CONSTRAINT `FK_sys_user` FOREIGN KEY (`idsys_user`) REFERENCES `sys_user` (`idsys_user`),
  CONSTRAINT `FK_sys_user_client` FOREIGN KEY (`idclient`) REFERENCES `client` (`idclient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_client`
--

LOCK TABLES `sys_user_client` WRITE;
/*!40000 ALTER TABLE `sys_user_client` DISABLE KEYS */;
INSERT INTO `sys_user_client` VALUES (884,3),(1163,3);
/*!40000 ALTER TABLE `sys_user_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_type`
--

DROP TABLE IF EXISTS `sys_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_type` (
  `idsys_user_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idsys_user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_type`
--

LOCK TABLES `sys_user_type` WRITE;
/*!40000 ALTER TABLE `sys_user_type` DISABLE KEYS */;
INSERT INTO `sys_user_type` VALUES (1,'ADMIN'),(2,'USER'),(3,'TEMP_USER'),(4,'PARK_ADMIN'),(5,'CTRL_VIAL'),(6,'TESORERIA'),(7,'SETEX');
/*!40000 ALTER TABLE `sys_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_vehicle`
--

DROP TABLE IF EXISTS `sys_user_vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_vehicle` (
  `idsys_user_vehicle` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_plate` varchar(12) NOT NULL,
  `idsys_user` bigint(20) NOT NULL,
  `owner` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idsys_user_vehicle`),
  KEY `FK_sys_user_vehicle_plate` (`vehicle_plate`),
  KEY `FK_sys_user_vehicle_user` (`idsys_user`),
  CONSTRAINT `FK_sys_user_vehicle_plate` FOREIGN KEY (`vehicle_plate`) REFERENCES `vehicle` (`plate`)
) ENGINE=InnoDB AUTO_INCREMENT=428 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_vehicle`
--

LOCK TABLES `sys_user_vehicle` WRITE;
/*!40000 ALTER TABLE `sys_user_vehicle` DISABLE KEYS */;
INSERT INTO `sys_user_vehicle` VALUES (220,'CITY1',881,1),(221,'CITY2',882,1),(222,'CITY2',881,0),(223,'KJM860',885,1),(224,'ZVP999',886,1),(226,'test4044',887,1),(227,'AP55',888,1),(229,'BWE-345',890,1),(234,'AP23',894,1),(235,'CYM537',908,1),(236,'AP2',915,1),(237,'RDY064',917,1),(239,'NDW726',935,1),(240,'QWE-896',937,1),(241,'mrz326',939,1),(242,'MKT880',940,1),(247,'YLA05A',945,1),(248,'xxxxxx',949,1),(249,'BKT30',929,1),(251,'BKK879',950,1),(253,'aaa567',958,1),(254,'CBS123',959,1),(255,'cbs9876',959,1),(258,'469ABC',966,1),(260,'469ACD',966,1),(262,'KXS88D',972,1),(265,'4321',976,1),(268,'wza300',980,1),(269,'city10',1023,1),(270,'city11',1024,1),(271,'SOS123',1032,1),(272,'RAL318',1034,1),(273,'BAK370',1035,1),(274,'BFK909',1035,1),(275,'MAZ393',1038,1),(276,'BZQ42',1039,1),(277,'BHG337',1039,1),(278,'DCH423',1040,1),(279,'KLQ207',1041,1),(280,'BWD743',1042,1),(281,'UUF24C',1045,1),(282,'MBT884',1046,1),(283,'REV529',1048,1),(284,'MNP659',1049,1),(285,'HVK315',1050,1),(286,'Czg208',1062,1),(287,'BOL818',1065,1),(288,'MBS183',1071,1),(289,'RLU518',1075,1),(290,'MJY883',1076,1),(291,'NBS513',1078,1),(292,'MKO328',1078,1),(293,'AWG03D',1080,1),(294,'CFN020',1081,1),(295,'GGR87B',979,1),(296,'BFS959',979,1),(297,'RMY458',1084,1),(298,'MCL520',1084,1),(299,'RLW215',1084,1),(300,'BIG923',1084,1),(301,'BSW162',1052,1),(302,'DCB562',1052,1),(303,'CCY447',948,1),(304,'BML754',1101,1),(305,'DDT507',1104,1),(306,'HTT510',1104,1),(307,'MPS877',1105,1),(308,'HKJ115',1106,1),(309,'DBO880',1113,1),(310,'HJP306',1118,1),(312,'BZF008',1122,1),(313,'CYX763',1122,1),(314,'RBV675',1123,1),(315,'CYS269',1124,1),(316,'MPL184',1124,1),(317,'HTZ946',1132,1),(318,'RBM590',1135,1),(319,'HCS253',1135,1),(320,'MCX043',1137,1),(321,'ret823',1138,1),(322,'HSX060',1139,1),(323,'CYT332',1140,1),(324,'NEK584',1144,1),(325,'CYB013',1043,1),(326,'DCU290',1043,1),(327,'cpg 855',1146,1),(328,'NCX330',1148,1),(330,'RHR427',1149,1),(331,'RMQ396',1149,1),(332,'CVB622',1150,1),(333,'BRK610',1152,1),(334,'HDL053',1155,1),(335,'KMS058',1155,1),(336,'HCT628',1159,1),(337,'MCY014',1161,1),(338,'CYC514',1162,1),(339,'AZZ49D',1164,1),(340,'BIG121',1165,1),(341,'CIX930',1165,1),(342,'MKN862',1168,1),(343,'BBS019',1172,1),(344,'DBJ574',1172,1),(345,'BZD962',1172,1),(346,'HCO340',1177,1),(347,'MBP089',1166,1),(348,'TTO718',1166,1),(349,'QFW81B',1181,1),(350,'MAW438',1181,1),(351,'SGA312',1183,1),(352,'CZC330',1201,1),(353,'ROK386',1203,1),(354,'BRG691',1205,1),(355,'TTT565',1206,1),(356,'REM134',1241,1),(357,'BAT64D',1242,1),(358,'RJV669',1249,1),(359,'HJS926',1249,1),(360,'RKP188',1250,1),(361,'HCK281',1250,1),(362,'RMY978',1252,1),(363,'KAM818',1253,1),(364,'rzy348',1257,1),(365,'CVH390',1260,1),(366,'NAB195',1261,1),(367,'HBV877',1266,1),(368,'DAJ952',1266,1),(369,'BJF112',1267,1),(370,'YIW17C',1269,1),(371,'RBV998',1275,1),(372,'QNI84B',1276,1),(373,'MBM082',1278,1),(374,'HAY975',1279,1),(375,'CIW566',1281,1),(376,'AFN23C',1281,1),(377,'cym 887',1284,1),(378,'BLA272',1286,1),(379,'BRX333',1286,1),(380,'RZR469',1288,1),(381,'HKZ344',1293,1),(382,'EPF98D',1294,1),(383,'NBQ091',1297,1),(384,'HTM708',1302,1),(385,'BKT221',1302,1),(386,'CSO266',1304,1),(387,'OMD99C',1304,1),(388,'BJV487',1309,1),(389,'CFC904',1309,1),(390,'BMW121',1330,1),(391,'NEK546',1331,1),(392,'RAY219',1192,1),(393,'BSG997',1336,1),(394,'REQ479',1340,1),(395,'RMN627',1341,1),(396,'DAB797',1343,1),(397,'HSS890',1344,1),(398,'KEY040',1346,1),(399,'BJM227',1346,1),(400,'CIN597',1347,1),(401,'DBT652',1347,1),(402,'BNQ815',1348,1),(403,'CYJ962',1349,1),(404,'RNR327',1349,1),(405,'RZV818',1352,1),(406,'MCU808',1351,1),(407,'DCX143',1351,1),(408,'RMZ940',1364,1),(409,'BRP813',1353,1),(410,'djw908',1365,1),(411,'NDX092',1367,1),(412,'BHA945',1598,1),(413,'RIY102',1603,1),(414,'HJZ351',1280,1),(416,'BTL405',1605,1),(417,'DBL649',1608,1),(418,'BZQ937',1611,1),(419,'PXX01C',1611,1),(420,'NAE073',1613,1),(421,'CCU613',1615,1),(422,'CCU583',1624,1),(423,' TLO 168',1631,1),(424,'ZXX492',1333,1),(425,'REL592',1643,1),(426,'RJM855',1643,1),(427,'LAN635',1644,1);
/*!40000 ALTER TABLE `sys_user_vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_restrict`
--

DROP TABLE IF EXISTS `time_restrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_restrict` (
  `idtime_restrict` bigint(20) NOT NULL AUTO_INCREMENT,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `maxMinutes` int(11) DEFAULT NULL,
  `zone_idzone` varchar(10) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtime_restrict`),
  KEY `fk_time_restrict_zone` (`zone_idzone`),
  CONSTRAINT `fk_time_restrict_zone` FOREIGN KEY (`zone_idzone`) REFERENCES `zone` (`idzone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_restrict`
--

LOCK TABLES `time_restrict` WRITE;
/*!40000 ALTER TABLE `time_restrict` DISABLE KEYS */;
/*!40000 ALTER TABLE `time_restrict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tower`
--

DROP TABLE IF EXISTS `tower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tower` (
  `towerId` varchar(255) NOT NULL,
  `idZone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`towerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tower`
--

LOCK TABLES `tower` WRITE;
/*!40000 ALTER TABLE `tower` DISABLE KEYS */;
INSERT INTO `tower` VALUES ('fba7a8dbe02e11f7459533923dfdb7cd22843ec1',NULL);
/*!40000 ALTER TABLE `tower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_transaction`
--

DROP TABLE IF EXISTS `user_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_transaction` (
  `idsysuser_origen` bigint(20) NOT NULL,
  `idsysuser_dest` bigint(20) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`idsysuser_origen`,`idsysuser_dest`,`transaction_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_transaction`
--

LOCK TABLES `user_transaction` WRITE;
/*!40000 ALTER TABLE `user_transaction` DISABLE KEYS */;
INSERT INTO `user_transaction` VALUES (908,917,'2014-07-28 14:43:45',100),(908,917,'2014-07-28 15:30:30',100),(908,1201,'2014-07-07 07:17:44',10000),(917,970,'2014-07-15 13:36:13',100),(965,980,'2014-07-02 08:08:28',10),(965,980,'2014-07-02 08:12:56',10),(965,1193,'2014-07-03 12:09:12',20000),(970,960,'2014-06-11 08:08:19',100000),(970,965,'2014-06-17 08:08:19',100000),(970,1035,'2014-06-11 08:08:19',100000),(970,1036,'2014-06-11 08:08:19',100000),(970,1043,'2014-06-25 08:08:19',50000),(970,1046,'2014-06-27 08:08:19',50000),(970,1051,'2014-06-11 08:08:19',100000),(970,1052,'2014-06-16 08:08:19',100000),(970,1063,'2014-06-11 08:08:19',100000),(970,1081,'2014-06-16 08:08:19',50000),(970,1095,'2014-06-17 08:08:19',30000),(970,1113,'2014-06-20 08:08:19',20000),(970,1113,'2014-07-01 08:08:19',30000),(970,1134,'2014-06-24 08:08:19',10000),(970,1138,'2014-07-01 08:08:19',100000),(970,1140,'2014-06-27 08:08:19',50000),(970,1140,'2014-07-25 10:47:42',25000),(970,1155,'2014-07-01 08:08:19',100000),(970,1158,'2014-06-26 08:08:19',30000),(970,1169,'2014-07-15 15:56:35',300000),(970,1250,'2014-07-11 14:07:07',75000),(970,1297,'2014-07-18 10:15:35',25000),(970,1302,'2014-07-14 10:09:44',25000),(970,1307,'2014-07-16 13:20:31',25000),(970,1318,'2014-07-16 12:11:24',100000),(970,1333,'2014-07-18 12:44:36',100000),(970,1336,'2014-07-18 20:44:54',100000),(970,1341,'2014-07-22 11:54:42',50000),(970,1346,'2014-07-22 11:52:45',50000),(970,1364,'2014-07-23 13:15:04',25000),(970,1610,'2014-07-29 11:35:39',50000),(970,1611,'2014-07-31 18:03:28',15000),(970,1614,'2014-07-30 10:30:44',200000),(970,1615,'2014-07-30 10:33:09',4000),(970,1633,'2014-07-31 18:04:31',25000),(1051,974,'2014-07-02 08:08:19',100);
/*!40000 ALTER TABLE `user_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle` (
  `plate` varchar(12) NOT NULL,
  `state` int(10) unsigned NOT NULL DEFAULT '1',
  `dateReg` datetime NOT NULL,
  PRIMARY KEY (`plate`),
  UNIQUE KEY `plateUnique` (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle`
--

LOCK TABLES `vehicle` WRITE;
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` VALUES (' TLO 168',1,'2014-07-30 15:34:50'),('4321',1,'2014-06-07 05:50:54'),('469ABC',1,'2014-06-04 15:32:49'),('469ACD',1,'2014-06-04 15:43:18'),('aaa567',1,'2014-05-27 09:28:53'),('AFN23C',1,'2014-07-11 06:28:12'),('AP2',1,'2014-04-01 07:10:11'),('AP23',1,'2014-02-11 17:09:36'),('AP55',1,'2013-12-17 16:37:30'),('AWG03D',1,'2014-06-14 19:53:18'),('AZZ49D',1,'2014-06-26 09:17:28'),('BAK370',1,'2014-06-11 12:21:20'),('BAT64D',1,'2014-07-07 12:32:56'),('BBS019',1,'2014-06-27 10:01:09'),('BFK909',1,'2014-06-11 12:21:48'),('BFS959',1,'2014-06-15 08:33:42'),('BHA945',1,'2014-07-24 15:37:17'),('BHG337',1,'2014-06-11 18:56:53'),('BIG121',1,'2014-06-26 11:43:52'),('BIG923',1,'2014-06-15 18:51:57'),('BJF112',1,'2014-07-10 08:02:59'),('BJM227',1,'2014-07-22 08:39:53'),('BJV487',1,'2014-07-16 01:10:11'),('BKK879',1,'2014-05-16 11:01:36'),('BKT221',1,'2014-07-13 22:28:28'),('BKT30',1,'2014-05-08 18:34:49'),('BLA272',1,'2014-07-11 14:06:23'),('BML754',1,'2014-06-17 18:48:07'),('BMW121',1,'2014-07-16 19:44:47'),('BNQ815',1,'2014-07-22 12:11:24'),('BOL818',1,'2014-06-12 15:52:10'),('BRG691',1,'2014-07-05 23:11:54'),('BRK610',1,'2014-06-25 08:05:16'),('BRP813',1,'2014-07-23 08:42:20'),('BRX333',1,'2014-07-11 14:07:15'),('BSG997',1,'2014-07-18 13:39:58'),('BSW162',1,'2014-06-16 15:01:43'),('BTL405',1,'2014-07-25 20:25:20'),('BWD743',1,'2014-06-11 19:40:22'),('BWE-345',1,'2013-12-18 09:45:03'),('BZD962',1,'2014-06-27 10:01:47'),('BZF008',1,'2014-06-19 09:46:49'),('BZQ42',1,'2014-06-11 18:56:36'),('BZQ937',1,'2014-07-28 13:42:36'),('CBS123',1,'2014-05-27 11:44:27'),('cbs9876',1,'2014-05-28 16:08:52'),('CCU583',1,'2014-07-30 14:28:07'),('CCU613',1,'2014-07-29 12:20:57'),('CCY447',1,'2014-06-17 05:25:02'),('CFC904',1,'2014-07-16 01:10:39'),('CFN020',1,'2014-06-14 20:51:16'),('CIN597',1,'2014-07-22 09:11:11'),('CITY1',1,'2013-11-12 22:25:29'),('city10',1,'2014-06-10 20:57:32'),('city11',1,'2014-06-10 21:00:28'),('CITY2',1,'2013-11-12 22:26:50'),('CIW566',1,'2014-07-11 06:27:40'),('CIX930',1,'2014-06-26 11:44:31'),('cpg 855',1,'2014-06-24 18:18:55'),('CSO266',1,'2014-07-14 22:09:21'),('CVB622',1,'2014-06-25 04:08:14'),('CVH390',1,'2014-07-09 20:56:43'),('CYB013',1,'2014-06-24 17:17:33'),('CYC514',1,'2014-06-25 23:56:02'),('CYJ962',1,'2014-07-22 19:10:09'),('cym 887',1,'2014-07-11 09:07:49'),('CYM537',1,'2014-03-06 10:29:23'),('CYS269',1,'2014-06-19 19:41:06'),('CYT332',1,'2014-06-22 11:01:35'),('CYX763',1,'2014-06-19 09:47:19'),('CZC330',1,'2014-07-04 15:20:17'),('Czg208',1,'2014-06-12 14:14:00'),('DAB797',1,'2014-07-21 17:12:13'),('DAJ952',1,'2014-07-10 07:42:09'),('DBJ574',1,'2014-06-27 10:01:31'),('DBL649',1,'2014-07-27 19:21:18'),('DBO880',1,'2014-06-18 19:36:56'),('DBT652',1,'2014-07-22 09:11:54'),('DCB562',1,'2014-06-16 15:02:48'),('DCH423',1,'2014-06-11 18:58:48'),('DCU290',1,'2014-06-24 17:17:55'),('DCX143',1,'2014-07-22 20:31:24'),('DDT507',1,'2014-06-17 22:23:44'),('djw908',1,'2014-07-23 09:24:55'),('EPF98D',1,'2014-07-12 13:25:48'),('GGR87B',1,'2014-06-15 08:33:12'),('HAY975',1,'2014-07-10 22:13:52'),('HBV877',1,'2014-07-10 07:41:28'),('HCK281',1,'2014-07-09 06:44:00'),('HCO340',1,'2014-06-28 11:03:28'),('HCS253',1,'2014-06-21 10:26:36'),('HCT628',1,'2014-06-25 17:53:13'),('HDL053',1,'2014-06-25 09:03:58'),('HJP306',1,'2014-06-18 19:40:26'),('HJS926',1,'2014-07-08 22:16:56'),('HJZ351',1,'2014-07-25 15:06:16'),('HKJ115',1,'2014-06-18 06:19:31'),('HKZ344',1,'2014-07-11 18:56:33'),('HSS890',1,'2014-07-21 18:15:12'),('HSX060',1,'2014-06-21 19:13:59'),('HTM708',1,'2014-07-13 22:28:00'),('HTT510',1,'2014-06-17 22:24:13'),('HTZ946',1,'2014-06-20 17:42:51'),('HVK315',1,'2014-06-12 10:46:48'),('KAM818',1,'2014-07-09 16:47:41'),('KEY040',1,'2014-07-22 08:39:11'),('KJM860',1,'2013-11-14 13:03:29'),('KLQ207',1,'2014-06-11 19:15:12'),('KMS058',1,'2014-06-25 09:04:34'),('KXS88D',1,'2014-06-05 09:30:49'),('LAN635',1,'2014-08-01 09:23:58'),('MAW438',1,'2014-06-30 12:06:42'),('MAZ393',1,'2014-06-11 17:13:06'),('MBM082',1,'2014-07-10 21:51:44'),('MBP089',1,'2014-06-29 08:17:08'),('MBS183',1,'2014-06-13 09:02:29'),('MBT884',1,'2014-06-11 22:31:14'),('MCL520',1,'2014-06-15 18:51:12'),('MCU808',1,'2014-07-22 20:30:09'),('MCX043',1,'2014-06-21 15:41:33'),('MCY014',1,'2014-06-25 21:17:53'),('MJY883',1,'2014-06-13 21:19:59'),('MKN862',1,'2014-06-26 14:45:02'),('MKO328',1,'2014-06-14 15:28:30'),('MKT880',1,'2014-04-21 14:56:54'),('MNP659',1,'2014-06-12 06:15:09'),('MPL184',1,'2014-06-19 19:41:45'),('MPS877',1,'2014-06-18 05:57:28'),('mrz326',1,'2014-04-14 12:30:54'),('NAB195',1,'2014-07-09 21:20:31'),('NAE073',1,'2014-07-28 19:45:47'),('NBQ091',1,'2014-07-13 12:56:35'),('NBS513',1,'2014-06-14 15:28:09'),('NCX330',1,'2014-06-24 20:38:17'),('NDW726',1,'2014-04-11 22:28:18'),('NDX092',1,'2014-07-23 11:10:50'),('NEK546',1,'2014-07-16 23:42:03'),('NEK584',1,'2014-06-23 17:56:50'),('OMD99C',1,'2014-07-14 22:09:43'),('PXX01C',1,'2014-07-28 13:43:03'),('QFW81B',1,'2014-06-30 12:06:23'),('QNI84B',1,'2014-07-10 20:14:53'),('QWE-896',1,'2014-04-14 08:16:12'),('RAL318',1,'2014-06-11 10:47:44'),('RAY219',1,'2014-07-17 21:23:33'),('RBM590',1,'2014-06-21 10:25:46'),('RBV675',1,'2014-06-19 15:25:12'),('RBV998',1,'2014-07-10 20:08:27'),('RDY064',1,'2014-04-02 22:06:34'),('REL592',1,'2014-08-01 08:28:35'),('REM134',1,'2014-07-07 09:41:49'),('REQ479',1,'2014-07-20 23:49:56'),('ret823',1,'2014-06-21 17:22:42'),('REV529',1,'2014-06-12 05:38:14'),('RHR427',1,'2014-06-24 21:13:33'),('RIY102',1,'2014-07-25 09:12:08'),('RJM855',1,'2014-08-01 08:29:07'),('RJV669',1,'2014-07-08 22:16:38'),('RKP188',1,'2014-07-09 06:43:31'),('RLU518',1,'2014-06-13 18:49:07'),('RLW215',1,'2014-06-15 18:51:31'),('RMN627',1,'2014-07-21 14:24:14'),('RMQ396',1,'2014-06-24 21:13:52'),('RMY458',1,'2014-06-15 18:50:41'),('RMY978',1,'2014-07-09 16:19:35'),('RMZ940',1,'2014-07-23 08:12:39'),('RNR327',1,'2014-07-22 19:10:37'),('ROK386',1,'2014-07-05 11:55:29'),('RZR469',1,'2014-07-11 15:02:55'),('RZV818',1,'2014-07-22 20:28:52'),('rzy348',1,'2014-07-09 18:19:00'),('SGA312',1,'2014-07-01 09:45:43'),('SOS123',1,'2014-06-10 21:29:33'),('test4044',1,'2013-12-17 16:21:08'),('TTO718',1,'2014-06-29 08:18:26'),('TTT565',1,'2014-07-06 06:07:30'),('UUF24C',1,'2014-06-11 21:06:35'),('wza300',1,'2014-06-10 18:19:10'),('xxxxxx',1,'2014-05-08 11:04:06'),('YIW17C',1,'2014-07-10 16:42:21'),('YLA05A',1,'2014-05-05 04:40:00'),('ZVP999',1,'2013-11-14 14:23:18'),('ZXX492',1,'2014-07-30 20:45:54');
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `idzone` varchar(30) NOT NULL,
  `name` varchar(45) NOT NULL,
  `client_idclient` bigint(20) DEFAULT NULL,
  `minuteValue` decimal(6,2) DEFAULT NULL,
  `openTime` time DEFAULT NULL,
  `closeTime` time DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `gmt` varchar(200) DEFAULT 'America/Toronto',
  `url` varchar(512) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `schedule` text,
  `online` smallint(6) DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idzone`),
  KEY `fk_zone_client` (`client_idclient`),
  KEY `nameIndex` (`name`),
  CONSTRAINT `fk_zone_client` FOREIGN KEY (`client_idclient`) REFERENCES `client` (`idclient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zone`
--

LOCK TABLES `zone` WRITE;
/*!40000 ALTER TABLE `zone` DISABLE KEYS */;
INSERT INTO `zone` VALUES ('COL10','CL 97 #15 -21',3,89.00,'06:00:00','23:00:00',4.682182,-74.049151,'America/Colombia','http://200.119.7.194:70/wsPagoXCelular','calle 97 No 15-21','De Lunes de 6:00am. A 10:00pm. Martes a Sábado de 6:00am. A 11:00pm. Domingos y Festivos de 11:00am A 6:00pm.',0,'calle97'),('COL100','CR 15 A # 124 -44',3,89.00,'06:00:00','23:00:00',4.7026963,-74.0436682,'America/Colombia','http://201.245.114.30:70/wsPagoXCelular','Cra 15 A No. 124-40','LUNES A SABADO 6 - 22',0,'calle124'),('COL101','CL 59A BIS NO. 5-53 LINK 7-60',3,89.00,'06:00:00','23:00:00',4.644907,-74.061064,'America/Colombia','http://201.245.63.50:70/wsPagoXCelular','Calle 59 A Bis 5-53','LUNES A VIERNES 6 - 22, SABADO 8 - 18',0,'link760'),('COL102','CL 116 # 15-80',3,89.00,'06:00:00','23:00:00',4.6969234,-74.0442163,'America/Colombia','http://200.93.136.194:70/wsPagoXCelular','Cale 116 No 15-80','LUNES A JUEVES 6 - 22, VIERNES Y SABADO 7 - 1, DOMINGO 14 - 18',0,'calle116'),('COL103','(AV68)',3,89.00,'06:00:00','23:00:00',4.687276,-74.073441,'America/Colombia','http://65.167.49.38:70/wsPagoXCelular','Calle 94 A No. 67 A 74','LUNES A VIERNES 6 - 23, SABADO Y DOMINGO 6:30 - 24',0,'mall68'),('COL105','TC1',3,89.00,'06:00:00','23:00:00',4.693736,-74.138748,'America/Colombia','http://190.24.242.6:70/wsPagoXCelular','Calle 26 No.106 - 39 TERMINAL DE CARGA','24 HORAS',0,'tc1'),('COL108','SURTIFRUVER 76',3,89.00,'06:00:00','23:00:00',4.661112,-74.0564589,'America/Colombia','http://201.245.104.194:70/wsPagoXCelular','Calle 76 No. 11 15','De Lunes a Domingo de 6:00am a 10:00pm.',0,'surtifruver76'),('COL109','CANDELARIA',3,89.00,'06:00:00','23:00:00',4.598781,-74.07045,'America/Colombia','http://190.24.249.71:70/wsPagoXCelular','Calle 12 C 3 -32','LUNES A JUEVES 5:30 - 22, VIERNES Y SABADOS 5:30 - 24',0,'candelaria'),('COL11','CREPES',3,89.00,'06:00:00','23:00:00',4.6576878,-74.0559868,'America/Colombia','http://201.244.178.18:70/wsPagoXCelular','calle 74 No 9-21','LUNES A SABADO 6 - 23, DOMINGO 11 - 21',0,'crepes'),('COL114','CASTELLANA',3,89.00,'06:00:00','23:00:00',4.6831482,-74.0587158,'America/Colombia','http://201.245.152.74:70/wsPagoXCelular','Cra. 45 A No. 94-66','LUNES A SABADO 6 - 23, DOMINGO 6 - 22',0,'castellana'),('COL115','CRA 7 NO. 117 - 14',3,89.00,'06:00:00','23:00:00',4.694879,-74.032451,'America/Colombia','http://190.26.247.95:70/wsPagoXCelular','Cra 7 No. 117 - 44','DOMINGO A MARTES 6 - 22, MIERCOLES A SABADO 24 HORAS',0,'calle117'),('COL116','TORRE ZIMMA',3,89.00,'06:00:00','23:00:00',4.674343,-74.052876,'America/Colombia','http://190.25.181.160:70/wsPagoXCelular','Cra 15 No. 88 - 64','LUNES A VIERNES 6 - 21:30, SABADO 6 - 17',0,'torrezimma'),('COL118','SAN JOSE',3,89.00,'06:00:00','23:00:00',4.60556,-74.089865,'America/Colombia','http://201.244.73.202:70/wsPagoXCelular','CR 22 #9- 35 SAN JOSE','De Lunes a Sábado de 7:00 a.m. hasta 8:00 p.m. Domingos y Festivos de 8:00am a 6:00pm.',0,'sanjose'),('COL12','PARQUE 93',3,89.00,'06:00:00','23:00:00',4.6756759,-74.0488112,'America/Colombia','http://201.245.114.238:70/wsPagoXCelular','CRA 12  # 93-43  PARQUE 93','LUNES A MIERCOLES 6 - 23, JUEVES A SABADO 6 - 3, DOMINGO 10 - 19',0,'parque93'),('COL122','BAKERS',3,89.00,'06:00:00','23:00:00',4.6682435,-74.0520778,'America/Colombia','http://190.24.226.234:70/wsPagoXCelular','Calle 85 No. 11-64','LUNES A MARTES 6 - 22, MIERCOLES A SABADO 6 - 3, DOMINGO 8 - 20',0,'bakers'),('COL123','BODY 134',3,89.00,'06:00:00','23:00:00',4.719523,-74.049456,'America/Colombia','http://186.30.243.70:70/wsPagoXCelular','Calle 134 A No. 21-08','De Lunes a Miércoles de 5:00 am. A 11:00 pm. Jueves a Viernes de 5:00 am. A 2:00 am. Sábados de 7:00 am. A 2:00 am. Domingos y Festivos de 7:00 am. A 4:00 p.m.',0,'body134'),('COL124','BICENTENARIO',3,89.00,'06:00:00','23:00:00',4.6009636,-74.0700435,'America/Colombia','http://190.26.83.116:70/wsPagoXCelular','Carrera 4a No. 16-03','LUNES A VIERNES 6 - 22, SABADOS 7 - 17',0,'bicentenario'),('COL128','CL 100 NO. 8-70',3,89.00,'06:00:00','23:00:00',4.680886,-74.039721,'America/Colombia','http://190.26.67.246:70/wsPagoXCelular','calle 100 No. 8 - 70','De Lunes a Viernes de 6:00am hasta 11:00pm; Y Sábados 7:00am hasta 3:00pm',0,'umilitar'),('COL129','JOCKEY CLUB',3,89.00,'06:00:00','23:00:00',4.6014951,-74.0709349,'America/Colombia','http://201.244.53.23:70/wsPagoXCelular','CL 16  # 04 - 68 PROCURADURIA','LUNES A VIERNES 6 - 22, SABADOS 7 - 17',0,'calle16cr4'),('COL133','cra. 8 64-10',3,89.00,'06:00:00','23:00:00',4.649249,-74.060418,'America/Colombia',NULL,'CRA 8 #64-30 CALLE 64','LUNES A SABADO 6 - 22',0,'calle64'),('COL134','Calle 93B',3,89.00,'06:00:00','23:00:00',4.679009,-74.051744,'America/Colombia','http://190.26.107.11:70/wsPagoXCelular','CL 93B # 15-80','LUNES A VIERNES 6 - 22, SABADO 7 - 15',0,'cl93b15'),('COL135','Cra30 #48-38',3,89.00,'06:00:00','23:00:00',4.638333,-74.07902,'America/Colombia',NULL,'CR 30 #48-38 UNIVERSIDAD NACIONAL','LUNES A DOMINGO 6 - 22',0,'cr3048'),('COL137','BUFFALO WINGS',3,89.00,'06:00:00','23:00:00',4.697837,-74.046125,'America/Colombia','http://186.28.226.73:70/wsPagoXCelular','CR  17 #116-15  BUFFALO WINGS','LUNES A JUEVES 11 - 23, VIERNES Y SABADO 12 - 1, DOMINGO 11 - 22',0,NULL),('COL138','Av. Americas 62- 84',3,89.00,'06:00:00','23:00:00',4.631126,-74.117431,'America/Colombia',NULL,'Av. AMERICS 62-84 OUTLET AMERICAS','De Lunes a Sabado de 08:00am hasta 10:00pm; Y Domingos y Festivos 8:00am hasta 8:00pm.',0,'outamericas'),('COL139','Intercentro',3,89.00,'06:00:00','23:00:00',4.6102,-74.071016,'America/Colombia','http://201.244.83.44:70/wsPagoXCelular','CR 9 # 23 - 75 INTERCENTRO','Lunes a Sabado de 5:30am hasta 10:00pm; Y Domingos y Festivos 7:00am hasta 5:00pm.',0,'cr9cl24'),('COL14','BODY TECH KENNEDY',3,89.00,'06:00:00','23:00:00',4.618721,-74.159597,'America/Colombia','http://200.119.20.122:70/wsPagoXCelular','transv. 78j No 41f-05  sur','De Lunes a Jueves de 5:00 am. A 11:00 pm. Viernes de 5:00 am. A 9:00 pm. Sábados de 7:00 am. A 6:00 pm. Domingos y festivos de 7:00 am. A 4:00 p.m.',0,'kennedy'),('COL146','CR 13 # 25 - 31 FONADE',3,89.00,'06:00:00','23:00:00',4.612559,-74.071627,'America/Colombia','http://190.27.217.197:70/wsPagoXCelular','CR 13 # 25 - 31 FONADE','De Lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Sábados 7:00am hasta 06:00pm; Domingos y Festivos de 8:00am a 5:00pm.',0,'Fonade'),('COL147','AV CIUDAD DE CALI',3,89.00,'06:00:00','23:00:00',4.680198,-74.116803,'America/Colombia','http://190.25.229.130:70/wsPagoXCelular','AV CIUDAD DE CALI 51 66','LUNES A VIERNES 6 - 20',0,''),('COL148','Calle 25B Sur No. 5-87 20 de Julio',3,89.00,'06:00:00','23:00:00',4.569286,-74.093218,'America/Colombia','http://201.244.72.237:70/wsPagoXCelular','CALLE  25B SUR No. 5-87 20 de julio','24 HORAS',0,'20julio'),('COL149','Calle 100 7 A- 81 ',3,89.00,'06:00:00','23:00:00',4.680343,-74.039531,'America/Colombia','http://190.24.151.80:70/wsPagoXCelular','Calle 100 7 A- 81 ','LUNES A VIERNES 6 - 22, SABADO 6 - 14',0,NULL),('COL151','CR 33 No 28 41',3,89.00,'06:00:00','23:00:00',4.631214,-74.081431,'America/Colombia',NULL,'CR 33 No 28 41','LUNES A VIERNES 6 - 22, SABADO 6 - 14',0,NULL),('COL152','BIBLIOTECA JULIO MSD ',3,89.00,'06:00:00','23:00:00',4.757577,-74.063455,'America/Colombia',NULL,'CL 170 No 67-51 centro cultural y biblioteca JMSD  ','LUNES A DOMINGO 7 - 19',0,NULL),('COL153','SALITRE OFICCE',3,89.00,'06:00:00','23:00:00',4.634933,-74.09591,'America/Colombia',NULL,'Cra. 46 # 22 B – 20                                             ','lunes a sábado 6:00 a 22:00',0,NULL),('COL154','El Alcazar Tennis',3,89.00,'06:00:00','23:00:00',4.810804,-74.036216,'America/Colombia',NULL,'Autopista Norte  235 Costado Oriental','sábado 7:00 a 19:00, domingo 7:00 19:00 (depende de las actividades programadas)',0,NULL),('COL155','Jungla Kumba ',3,89.00,'06:00:00','23:00:00',4.723717,-74.07226,'America/Colombia','http://186.31.224.48:70/wsPagoXCelular','Cra. 59D 131-45','LUNES N.A, MARTES A JUEVES 12 - 21, VIERNES 12 - 23, SABADO Y DOMINGO 8 - 23',0,NULL),('COL156','Green Oficce',3,89.00,'06:00:00','23:00:00',4.681511,-74.044056,'America/Colombia',NULL,'Cr 11  No 98-07',NULL,0,NULL),('COL157','CRA 18 136A – 14',3,89.00,'06:00:00','23:00:00',4.720956,-74.045097,'America/Colombia','http://181.136.86.163:70/wsPagoXCelular','CRA 18 136A – 14','Lunes a Sabado de 6am a 10Pm, Domingos  de 6am a 3Pm y festivos 7am a 2pm',0,NULL),('COL158','AC 13 NO 66-70',3,89.00,'06:00:00','23:00:00',4.637033,-74.117473,'America/Colombia',NULL,'AC 13 NO 66-70',NULL,0,NULL),('COL159','12 de Octubre',3,89.00,'06:00:00','23:00:00',4.669375,-74.074916,'America/Colombia',NULL,'52 # 72-1 a 72-99','24 HORAS',0,NULL),('COL16','SULTANA',3,89.00,'06:00:00','23:00:00',4.597565,-74.108341,'America/Colombia','http://186.30.249.166:70/wsPagoXCelular','calle 12 sur No 31-37*33','LUNES A JUEVES 5 - 24, VIERNES 5 - 22, SABADO 6:30 - 19, DOMINGO 6:30 - 17',0,'sultana'),('COL161','Alcazar Futbol',3,89.00,'06:00:00','23:00:00',4.81234,-74.035974,'America/Colombia',NULL,'Autopista Norte  236 Costado Oriental','sábado 7:00 a 19:00, domingo 7:00 19:00 (depende de las actividades programadas)',0,NULL),('COL18','SURTIFRUVER CHAPINERO',3,89.00,'06:00:00','23:00:00',4.643608,-74.0616881,'America/Colombia','http://201.245.63.158:70/wsPagoXCelular','Cra.  7 No  58-18','Jueves 7 a 1AM y Sábado 7 a 3AM',0,'blockbuster58'),('COL19','HOTEL BEST WESTERN',3,89.00,'06:00:00','23:00:00',4.676082,-74.050733,'America/Colombia',NULL,'Calle 93 13-71','LUNES A VIERNES 6 - 22, SABADO 6 - 23',0,'bestwestern'),('COL21','CL 93B # 13- 55',3,89.00,'06:00:00','23:00:00',4.6778841,-74.0495929,'America/Colombia','http://200.93.170.242:70/wsPagoXCelular','calle 93B No 13-55','LUNES A MIERCOLES 6 - 23, JUEVES A SABADO 6 - 3, DOMINGO 9 - 23',0,'calle93b'),('COL22','CL 81 #11-34/44',3,89.00,'06:00:00','23:00:00',4.6652432,-74.0536932,'America/Colombia','http://201.244.183.138:70/wsPagoXCelular','calle 81 No 11-34-44','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 11 - 19',0,'calle81'),('COL23','CL 95 # 11A-67',3,89.00,'06:00:00','23:00:00',4.679475,-74.047211,'America/Colombia','http://200.69.114.250:70/wsPagoXCelular','calle 95 No 11a -67','De lunes a miércoles de 6:00 a.m. hasta 11:00 p.m. Los días jueves a sábados de 6:00 a.m. hasta 3:00 a.m. Los días domingo y festivos de 9:00 a.m. hasta 5:00 p.m.',0,'calle95'),('COL24','CL 100 # 13-67',3,89.00,'06:00:00','23:00:00',4.6840487,-74.046752,'America/Colombia','http://201.245.150.10:70/wsPagoXCelular','calle 100 No 13-67','LUNES A SABADO 6 - 23, DOMINGO 10 - 21',0,'calle100'),('COL25','AUTO NORTE JAVESALUD',3,89.00,'06:00:00','23:00:00',4.704524,-74.053939,'America/Colombia','http://201.245.100.42:70/wsPagoXCelular','autp.nte No 124-02','LUNES A VIERNES 6 - 22, SABADO 6 - 15, DOMINGO 8 - 16',0,'javesalud'),('COL28','(CHICO - 2)',3,89.00,'06:00:00','23:00:00',4.6910752,-74.0510944,'America/Colombia',NULL,'avda.19 No 104-52','LUNES A VIERNES 5 - 22, SABADO 7 - 22',0,'calle104'),('COL29','CARR 11 -  84-50',3,89.00,'06:00:00','23:00:00',4.6672451,-74.0518061,'America/Colombia','http://201.244.188.26:70/wsPagoXCelular',' cra, 11 No 84 -50','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3, DOMINGO 10 - 20',0,'calle84'),('COL31','CL 99 # 10-47',3,89.00,'06:00:00','23:00:00',4.681572,-74.043385,'America/Colombia','http://65.167.51.126:70/wsPagoXCelular','Calle 99 No. 10-47','De Lunes a Viernes de 6:00am a 11:00pm; Sábados 7:00am a 7:00pm; Domingos y Festivos de 11:00am hasta 7:00pm.',0,'calle99'),('COL33','CL 122 # 15A- 64',3,89.00,'06:00:00','23:00:00',4.70111,-74.04472,'America/Colombia','http://65.167.80.86:70/wsPagoXCelular','Calle 122 No 15A -64','LUNES A MIERCOLES 7 - 22, JUEVES A SABADO 7 - 3, DOMINGO 10 - 19',0,'calle122'),('COL34','MEGATOWER',3,89.00,'06:00:00','23:00:00',4.6837911,-74.0462276,'America/Colombia','http://201.245.64.154:70/wsPagoXCelular','calle 100 No 13-21','LUNES A VIERNES 5:30 - 22, SABADO 6 - 14',0,'megatower'),('COL35','PLAZA DE TOROS',3,89.00,'06:00:00','23:00:00',4.614369,-74.067343,'America/Colombia','http://200.75.40.238:70/wsPagoXCelular','Calle 27 No. 5 44/cl 28 No. 5 23','24 HORAS',0,'plazadetoros'),('COL37','VALMARIA',3,89.00,'06:00:00','23:00:00',4.69566,-74.02926,'America/Colombia','http://201.245.82.69:70/wsPagoXCelular','calle 119 No 3-00','24 HORAS',0,'valmaria'),('COL38','SHOW PLACE',3,89.00,'06:00:00','23:00:00',4.723221,-74.026933,'America/Colombia','http://190.24.205.202:70/wsPagoXCelular','avda 147 No 7-70','LUNES A VIERNES 4 - 24, SABADO 6 - 23, DOMINGO 6 - 22',0,'showplace'),('COL4','CU. EXTERNADO',3,89.00,'06:00:00','23:00:00',4.6768521,-74.0518781,'America/Colombia','http://190.24.224.126:70/wsPagoXCelular','CRA 15 # 93-07/09  (U. EXTERNADO)','LUNES A MIERCOLES 6 - 22, JUEVES A SABADO 6 - 3',0,'calle93'),('COL40','PISO 6 NORTH POINT PRIVAD',3,89.00,'06:00:00','23:00:00',4.733126,-74.024053,'America/Colombia','http://65.167.83.42:70/wsPagoXCelular','cra. 7 No. 156-80','24 horas',0,'northpoint'),('COL42','BODY TECH 102',3,89.00,'06:00:00','23:00:00',4.687791,-74.051854,'America/Colombia','http://200.93.139.38:70/wsPagoXCelular','avda. 19 No 102 -31','LUNES A VIERNES 24 HORAS, SABADO 6 - 22, DOMINGO 6 19',0,'bodytech'),('COL43','CALLE 94',3,89.00,'06:00:00','23:00:00',4.680353,-74.049984,'America/Colombia','http://200.93.188.22:70/wsPagoXCelular','CRA 15 # 94-72/78 CALLE 94','De Lunes a Sábados de 6:00 am a 10:00 pm.',0,'calle94'),('COL44','JARDINES',3,89.00,'06:00:00','23:00:00',4.784422,-74.041253,'America/Colombia','http://200.93.159.230:70/wsPagoXCelular','Autopista Norte No 207-47','De Domingo a Domingo de 6:30 am a 10:00 pm.',0,'jardines'),('COL45','PAS. LOS LIBERT',3,89.00,'06:00:00','23:00:00',4.783444,-74.041499,'America/Colombia','http://190.25.132.132:70/wsPagoXCelular','AV CRA 45 (PAS. LOS LIBERT)#207- 41 ECI','De lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Y los sábados de 6:00 a.m. hasta 06:00 p.m.',0,'eci'),('COL46','SAN RAFAEL',3,89.00,'06:00:00','23:00:00',4.723923,-74.06199,'America/Colombia','http://200.93.139.106:70/wsPagoXCelular','calle 134 No-55-30','De Lunes a miércoles de 7:00 am a 01:00 am; jueves, Viernes y Sábados de 7:00 am a 5:00 am; Domingos y Festivos de 7:30 am. A 01:00 am. ',0,'sanrafael'),('COL49','PLAZA 39',3,89.00,'06:00:00','23:00:00',4.626375,-74.066177,'America/Colombia','http://201.244.178.90:70/wsPagoXCelular','Diagonal  40A No  7-84 PLAZA 39','De lunes a miércoles de 6:00 a.m. hasta 12:00 p.m., Los días jueves a sábados las 24 horas. Y los domingos y festivos de 11:00 a.m. hasta 7:00 p.m.',0,'plaza39'),('COL54','CENTENARIO',3,89.00,'06:00:00','23:00:00',4.634877,-74.115724,'America/Colombia',NULL,'calle 13 No 65-71','De lunes a viernes de 6:00 a.m. hasta 8:00 p.m., Los días sábados de 8:00 a.m. hasta 8:00 p.m. Domingos y festivos de 9:00 a.m. hasta 7:00 ',0,'centenario'),('COL59','BANCOLOMBIA',3,89.00,'06:00:00','23:00:00',4.6165921,-74.067804,'America/Colombia','http://200.118.112.12:70/wsPagoXCelular','Calle 30 No 6-38','LUNES A VIERNES 5:30 - 22',0,'bancolombia'),('COL6','CHICO 104',3,89.00,'06:00:00','23:00:00',4.690437,-74.051528,'America/Colombia','http://200.93.171.226:70/wsPagoXCelular','avd. 19 No 104 -37','LUNES A VIERNES 5 - 22, SABADO 7 - 22',0,'chico104'),('COL62','PEPE SIERRA',3,89.00,'06:00:00','23:00:00',4.697839,-74.046093,'America/Colombia','http://201.244.71.76:70/wsPagoXCelular','Cra 17 No 116-14','LUNES A VIERNES 7 - 3, SABADO 8 - 3, DOMINGO 12 - 3',0,'pepesierra'),('COL69','BTC',3,89.00,'06:00:00','23:00:00',4.680436,-74.043937,'America/Colombia','http://201.245.74.38:70/wsPagoXCelular','Cra 10 No 97A-13','De lunes a viernes de 6:00 a.m. hasta 10:00 p.m. Los días sábados de 6:00 a.m. hasta 5:00 p.m.',0,'btc'),('COL70','UMB',3,89.00,'06:00:00','23:00:00',4.642882,-74.054299,'America/Colombia','http://201.245.132.242:70/wsPagoXCelular','Avda. Circunvalar No 60-00','LUNES A VIERNES 6 - 22, SABADO 6 - 19',0,'manuelabeltran'),('COL71','CL 103 # 14A- 53 BBC',3,89.00,'06:00:00','23:00:00',4.687714,-74.046992,'America/Colombia','http://201.244.58.70:70/wsPagoXCelular','Calle 103No 14 a  -53','De lunes a sábados de 6:00 a.m. hasta 10:00 p.m., Los días domingos 6:00 a.m. hasta 3:00 p.m. y Festivos 7:00 a.m. hasta 2:00 p.m',0,'bbc'),('COL74','ESTE UNIV. EXTERNADO PPAL',3,89.00,'06:00:00','23:00:00',4.595314,-74.069066,'America/Colombia','http://201.244.183.174:70/wsPagoXCelular','Calle 12 No 1-17 este','LUNES A VIERNES 5:30 - 22, SABADOS 6 - 18',0,'uexternado'),('COL77','SURTIFRUVER 85',3,89.00,'06:00:00','23:00:00',4.669686,-74.054654,'America/Colombia','http://200.75.37.50:70/wsPagoXCelular','Calle 85 No 14-05','LUNES A MIERCOLES 6 - 21, JUEVES A SABADO 6 - 3, DOMINGO 8 - 21',0,'surtifruver85'),('COL8','CAPITAL TOWER',3,89.00,'06:00:00','23:00:00',4.680051,-74.038948,'America/Colombia','http://200.71.45.20:70/wsPagoXCelular','Calle 100 No 7 - 33','LUNES A VIERNES 5:30 - 22, SABADO 6 - 14',0,'capitaltower'),('COL82','BODY COLINA',3,89.00,'06:00:00','23:00:00',4.728943,-74.066127,'America/Colombia','http://201.244.58.54:70/wsPagoXCelular','Calle 138 No 58-74','De Lunes a Jueves de 4:00 am a 10:00 pm; Viernes, Sábados, Domingos y Festivos de 6:00 am a 10:00 pm.',0,'bodytechcolina'),('COL84','Bodega - Floresta',3,89.00,'06:00:00','23:00:00',4.690797,-74.075526,'America/Colombia','http://181.136.83.151:70/wsPagoXCelular','CRA 69 No. 98 - 41 FLORESTA','	\n\n7am a 7pm de Lunes a Domingo',0,NULL),('COL85','BODY 166',3,89.00,'06:00:00','23:00:00',4.748339,-74.046548,'America/Colombia','http://201.245.150.198:70/wsPagoXCelular','CR 23 # 166-59 BODY AUTO-NORTE','De Lunes a Jueves de 5:00 am a 11:00 pm; Viernes 5:00 am a 9:00 pm; Sábados de 7:00 am a 6:00 pm; Domingos y Festivos de 7:00 am a 4:00 pm.',0,'bodytechautopista'),('COL86','BODY CHAPINERO',3,89.00,'06:00:00','23:00:00',4.646627,-74.060495,'America/Colombia',NULL,'CR 7  No 61- 47 BODYTECH  CHAPINERO','LUNES A VIERNES 4 - 24, SABADO 6 - 24, DOMINGO 6 - 20',0,'bodytech63'),('COL9','FRISBY',3,89.00,'06:00:00','23:00:00',4.693958,-74.050839,'America/Colombia','http://201.244.187.250:70/wsPagoXCelular','avd.19 No. 106 a -45','LUNES A SABADO 6 - 22, DOMINGO 11- 20',0,'frisbv'),('COL90','ROSALES',3,89.00,'06:00:00','23:00:00',4.653001,-74.054378,'America/Colombia','http://200.93.185.54:70/wsPagoXCelular','carrera 5 No. 70A -74','De Lunes a Jueves de 6:00am a 10:00pm; Viernes y Sábado de 6:00am a 3:00am; Domingos y Festivos de 9:00am a 9:00pm.',0,'rosales'),('COL91','DAN CARLTON',3,89.00,'06:00:00','23:00:00',4.680179,-74.052948,'America/Colombia','http://200.93.172.10:70/wsPagoXCelular','Cra. 18  93B-49','LUNES A VIERNES 6 - 22, SABADO 7 - 15',0,'carrera18'),('COL92','METRO 127',3,89.00,'06:00:00','23:00:00',4.7037,-74.043628,'America/Colombia','http://190.146.236.21:70/wsPagoXCelular','Calle 127 15 A 03','24 HORAS',0,'metro127'),('COL95','MEDICAL CENTER',3,89.00,'06:00:00','23:00:00',4.6960488,-74.0322167,'America/Colombia','http://200.119.9.42:70/wsPagoXCelular','Calle 119 No. 7-14','24 HORAS',0,'medicalcenter'),('COL97','CARGO PORT',3,89.00,'06:00:00','23:00:00',4.692501,-74.13579,'America/Colombia','http://190.26.99.54:70/wsPagoXCelular','Calle 26 106 - 39','24 HORAS',0,'cargoport');
/*!40000 ALTER TABLE `zone` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cps_city`.`generateZoneId`
BEFORE INSERT ON `cps_city`.`zone`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
DECLARE paisPrefix VARCHAR(3);
DECLARE zoneCnt VARCHAR(5);
SELECT countryPrefix INTO paisPrefix
       FROM country c
       INNER JOIN client cl ON(c.idcountry = cl.country_idcountry) 
       WHERE cl.idclient = NEW.client_idclient;
SELECT (COUNT(*) + 1) INTO zoneCnt 
       FROM zone WHERE idzone like concat(paisPrefix,'%');
IF zoneCnt < 10 THEN
 SET zoneCnt = concat('0',zoneCnt);
END IF;
SET NEW.idzone = TRIM(concat(paisPrefix,zoneCnt));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `parking_history_view`
--

/*!50001 DROP TABLE IF EXISTS `parking_history_view`*/;
/*!50001 DROP VIEW IF EXISTS `parking_history_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `parking_history_view` AS select `ph`.`idhistory` AS `idhistory`,`ph`.`startTime` AS `startTime`,`ph`.`endTime` AS `endTime`,`ph`.`msisdn` AS `msisdn`,`ph`.`platform_idplatform` AS `platform_idplatform`,`ph`.`minuteValue` AS `minuteValue`,`ph`.`zone_idzone` AS `zone_idzone`,`ph`.`vehicle_plate` AS `vehicle_plate`,`ph`.`idsys_user` AS `idsys_user`,`ph`.`pay_id` AS `pay_id`,`ph`.`fee` AS `fee`,(time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60) AS `minuts`,ceiling((time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60)) AS `minutsRound`,if(((time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60) > 30),(ceiling((time_to_sec(timediff(`ph`.`endTime`,`ph`.`startTime`)) / 60)) * `ph`.`minuteValue`),255) AS `totalParking`,`ph`.`place_idplace` AS `place_idplace` from `parking_history` `ph` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-01  9:40:39
