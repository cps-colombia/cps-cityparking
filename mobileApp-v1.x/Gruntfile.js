module.exports = function(grunt) {

    //Get time for optimizing build times
    require('time-grunt')(grunt);

    // run grunt config
    require('load-grunt-config')(grunt, {
        init: true,
        data: {
            libs: {
                src: "cpsApp/src/www/js",
                dist: "cpsApp/www/js"
            },
            static: {
                src: "cpsApp/src/www/",
                dist: "cpsApp/www/"
            },
            debug: false
        }
    });
};
