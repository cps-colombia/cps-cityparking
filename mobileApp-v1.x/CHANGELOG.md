Changelog
==============

## 1.9.3 - 2015/07/27
- [fix] Fix grunt uglify error in compress [App]
- [fix] Check ads in register [All]
- [fix] fix input's in tablets [All]
- [fix] tildes in profile and other text [All]
- [fix] Fix display popup price [Android]
- [fix] open share fb and twitter in apps [Andoid/IOS]
- [fix] Disable buttons for prevent double send petition [All]
- [fix] Fix header height [IOS]
- [fix] Fix error compress in uglify [All]
- [app] better concurrent grunt [All]
- [app] Add custom class by platform in body [All]
- [app] After register succes automatic send login [All]
- [app] Update cordova, social share and geolocation plugins [All]
- [app] better wey to get platform and add clas in body [All]

## 1.9.2 - 2015/01/08

- [stability] Better speed geolocation [WP]
- [ux] Get user data form device [Android]
- [ui] Info popup in reload balance [All]
- [ui] Add price in place map [All]
- [add] Initial profile of Operator Admin [All]
- [fix] Add platformId in parking payment [All]
- [fix] Better validation if exist email in view zone [All]
- [fix] Error code Confirm Parking [All]
- [fix] Ass error message if GPS is disabled
