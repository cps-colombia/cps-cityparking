var map;
var count = 0;
var marker;

function initializeMap(img) {	
	
	var lat = rampgap.getCurrLatitude();
	var lon = rampgap.getCurrLongitude();
	var zinfo = rampgap.getCurrentZoneInfo();
	var mapdiv = document.getElementById("map_canvas");
	
   	mapdiv.style.width = '100%';
    mapdiv.style.height = $(window).height();

    var latlng = new google.maps.LatLng(lat, lon);		
    var myOptions = {
      zoom: 15,
      center: latlng,
      mapTypeControl: false,
      navigationControl: true,
      navigationControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_RIGHT
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var mapdiv = document.getElementById("map_canvas");	    	   
    	    	    
    map = new google.maps.Map(mapdiv, myOptions);	
    var infoList= zinfo.split(",");
    console.info('infolist' + infoList);
    var info='';
    for(var i=0; i<infoList.length; i++)
    {
    	info+=infoList[i]+"<BR/>";
        console.info('info' + info);

    }
    
 
    
  /*  var contentString = '<div id="content">'+
    '<div id="siteNotice" class="phoneytext" style="text-align:center;width:200px;height:150px;-webkit-border-radius: 38px;-moz-border-radius: 38px;border-radius: 38px;background-color:#636260;"> <H1>'
    + info+
    '</H1></div> </div>';*/
    var contentString = 
    '<div class="phoneytext" > <H1>'
    + info+
    '</H1></div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var myLatLng = new google.maps.LatLng(lat, lon);
    marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: img
    });
    
    
   var infoBubble2 = new InfoBubble({
        map: map,
        maxWidth: 300,
        content: contentString,//'<div class="phoneytext">Some label</div>',
        position: new google.maps.LatLng(lat, lon),
        shadowStyle: 1,
        padding: 0,
        backgroundColor: 'rgb(57,57,57)',
        borderRadius: 4,
        arrowSize: 10,
        borderWidth: 1,
        borderColor: '#2c2c2c',
        disableAutoPan: true,
        hideCloseButton: false,
        arrowPosition: 40,
        backgroundClassName: 'phoney',
        arrowStyle: 2
      });
   //infoBubble2.open();

    google.maps.event.addListener(marker, 'click', function() {
        //infowindow.open(map,marker);
    	 infoBubble2.open(map,marker);
      });

    
}

function setCenter(lt, ln){
	var latlng = new google.maps.LatLng(lt, ln);
	map.panTo(latlng);
	marker.setPosition(latlng);
}