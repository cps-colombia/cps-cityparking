/*global $, LANG */

///// GLOBAL //////
/**
 * current server url
 */
//Server Pruebas Canada
var url = 'http://96.45.176.18/CAN-CPS-MOB/MobileServer';

//Produccion
//var url = 'http://190.0.226.91/CAN-CPS-MOB/MobileServer';

//Debug
//var url = 'http://localhost:8082/CAN-CPS-MOB/MobileServer';
//var url = 'http://steelxhome.no-ip.org:8082/CAN-CPS-MOB/MobileServer';

/**
 * Control variables
 */
var myData = [],
    logueado = 0,
    session = 0,
    countryGet,
    countryData,
    _hidesplash = false,
    _deviceReady = false,
    _initAnalytics = false,
    _mapsLoaded = false,
    _networkOnline = true,
    _getPlatform;


/**
 * initial and global ajax setup
 */
$.ajaxSetup({
    url: url,
    type: "POST",
    dataType: "json",
    timeout: 30000,
    error: function(response, textStatus, errorThrown) {
        var errorSend = response.status;
        if (textStatus == "timeout" || textStatus == 'abort'){
            generalAlert(LANG.alerttimeouterror.text);
            errorSend = textStatus;
        } else if (response.status === 0 || errorThrown == "NetworkError") {
            generalAlert(LANG.alertnetworkerror.text);
        } else if (response.status == 404) {
            generalAlert(LANG.alertnetworkerror.text);
            //generalAlert(LANG.alertnotfounderror.text);
        } else if (response.status == 500) {
            generalAlert(LANG.alertservererror.text);
        } else {
            errorSend = textStatus;
            generalAlert(LANG.alertgeneralerror.text);
        }
        if(_initAnalytics === true){
            //Send error network event register to Analytics
            rampgap.ga('send', 'event', 'error', errorSend);
            rampgap.ga('send', 'exception', errorSend, true);
        }
        $.mobile.loading('hide');
        return false;
    }
});
//// End GLOBAL /////

/**
 * Wait for device API libraries to load
 */

function onBodyLoad() {
    document.addEventListener("deviceready", onDeviceReady, false);
    document.addEventListener("offline", onOffline, false);
    document.addEventListener("online", onOnline, false);
}

/**
 * Check internet connectionx
 */
function onOffline() {
    console.log("Offline: "+navigator.connection.type);
    navigator.notification.alert(
        LANG.alertnetworkconnection.text, //'No network connection',
        alertDismissed,
        'CPS PARKING',
        'Ok'
    );
    _networkOnline = false;
}

function onOnline() {
    console.log("Online: "+navigator.connection.type);
    _networkOnline = true;
    if(_mapsLoaded === false && _deviceReady === true){
        initMapsIframe();
    }
}
/**
 * Device is ready to rock!!
 */
function onDeviceReady() {

    function getPlatform() {
        var platform,
            platformId,
            protocol = 'file://',
            devicePlatform = $.trim(device.platform);

        if (devicePlatform == 'Android') {
            platformId = '1';
        } else if (devicePlatform == 'iOS') {
            platformId = '2';
            if (navigator.userAgent.match(/(iPad.*|iPhone.*|iPod.*);.*CPU.*OS 7_\d/i)) {
                $('body').addClass('standalone');
                if ($('meta[name="apple-mobile-web-app-status-bar-style"]').attr('content') == 'black-translucent') {
                    $('body').addClass('translucent-statusbar');
                }
            }
        } else if (devicePlatform == 'Win32NT' || 'WinCE') {
            platformId = '3';
            protocol = 'ms-appx-web://';
        }
        platform = {
            platformId: platformId,
            platform: devicePlatform,
            protocol: protocol
        };
        window.localStorage.setItem('platform', JSON.stringify(platform));
        console.log(JSON.stringify(platform, null, 4));
        return platform;
    }

    _getPlatform = rampgap.getPlatform() || getPlatform();
    _deviceReady = true;

    //start basic app components
    checkAppVersion();
    getCountries();

    //Init Analytics  @params clientId
    if(_initAnalytics === false){
        rampgap.initAnalytics(device.uuid);
        _initAnalytics = true;
    }

    // If online create maps - wp crash when try to load google maps offline
    if(_networkOnline === true && _mapsLoaded === false && _deviceReady === true ){
        initMapsIframe();
    }

    //Event backbutton
    document.addEventListener("backbutton", onBackKeyDown, false);

    function onBackKeyDown(e) {
        var page = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;
        if (page == 'login' || page == 'menu') {
            e.preventDefault();

            navigator.notification.confirm(
                LANG.alertexit.text,
                function(buttonIndex){
                    if (buttonIndex == 2) {
                        return false;
                    }
                    navigator.app.exitApp();
                },
                'CPS PARKING', ['OK', 'Cancel']
            );
        } else {
            navigator.app.backHistory();
        }

    }

}

/**
 * Notifications
 */
function alertDismissed() {
    // do something
}

function generalAlert(msg) {
    var debug = 0;
    if (debug !== 0 && _deviceReady === false && _getPlatform.platformId != 3 ) {
        alert(msg);
    } else {
        navigator.notification.alert(
            msg,
            alertDismissed,
            'CPS PARKING',
            'OK'
        );
    }
}

/**
 * load login page
 */
function showLogin() {
    //$.mobile.changePage($("#login"));

}
/**
 * go to page back
 */
function historyBack() {
    window.history.back();
}

/**
 * Create inframe maps if state is online
 */
function initMapsIframe(){
    console.log("Maps create");
    _mapsLoaded = true;
    // Create iframe map according to platform
    var frame_link = 'maps.html',
        framemap_id = 'frame_mvz',
        framemaploc_id = 'frame_mvzl',
        framemap = rampgap.makeFrame(_getPlatform, framemap_id, frame_link),
        framemaploc = rampgap.makeFrame(_getPlatform, framemaploc_id, frame_link);
    $('#'+framemap_id+', #'+framemaploc_id).remove();
    $('#viewZonesMapResult [data-role=content]').append(framemap);
    $('#mapaZonesLocalization [data-role=content]').append(framemaploc);
}

/**
 * put all initial things here after html document finish to load.
 */
$(document).on('focusin', 'input, textArea', function() {
    $('div[data-role="footer"]').hide();
});

$(document).on('focusout', 'input, textArea', function() {
    window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
    $('div[data-role="footer"]').show();
});


$(document).ready(function(e) {

    //Version app in login
    $(".versionApp").html("v. "+rampgap.getAppVersion());

    // External popup
    $("#popPayParking").enhanceWithin().popup();

    // Persistent Footer
    $( "[data-role='footer']" ).toolbar({
        tapToggle: false,
        position: "fixed"
    });

    //Fix Footer Fixed Androdi 2.x
    $('body').on('touchstart', function(e) {});

    //Set language
    initialize();
    if (rampgap.getItem("lang") !== null) {
        setLanguage(rampgap.getItem("lang"));
        $('#langSelect').val(rampgap.getItem("lang"));
    }

    $('#settingsMenu').click(function() { //ajustes v1.0.1
        $('#languageSelect').find('form').find('span:first').html(LANG.selectlangtext.text);
    });


    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //ajustes v1.0.1
    $('#sendEmailReset').click(function(e) {
        var msg = $('#emailReset').val();
        if (!filter.test(msg)) {
            generalAlert(LANG.alerttypeemail.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=23&mail=' + msg,
            success: function(response) {
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == -117) {
                        generalAlert(LANG.alertresetsend.text);
                        $('#emailReset').val('');
                        //$.mobile.changePage($("#login"));
                        $("body").pagecontainer("change", "#login", {
                            transition: "slide"
                        });
                        return false;
                    }
                    if (response == -118) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertcheckemail.text);
                        return false;
                    }
                    $('#emailReset').val('');
                    $.mobile.loading('hide');
                    return false;
                }
            }

        });
    });

    $('#ingresar').click(function(e) {
        var returnCheckVersion = checkAppVersion();
        if (returnCheckVersion === true) {
            login($('#userName').val(), $('#userPass').val());
            $('#userName').val("");
            $('#userPass').val("");

        }
    });


    $('#btnRegister').click(function(e) {
        getCitiesRegister(1, 0);
        $("body").pagecontainer("change", "#registerPage", {
            transition: "slide"
        });

    });

    $('#registerCountry').change(function(e) {
        getCitiesRegister($('#registerCountry').val());
    });

    $('#citiesSelect').change(function(e) {
        $('#viewZones').find('form').find('span:first').html($("#citiesSelect option:selected").text());
    });


    $('#register-submit').click(function() {

        var user = $("#user").val();
        var pass = $("#pass").val();
        var pass1 = $("#pass1").val();
        var name = $("#name").val();
        var last = $("#last").val();
        var address = $("#address").val();
        var email = $("#email").val();
        var bd = $("#birthDate").val(); // (yyyy-MM-dd)
        var country = $('#registerCountry').val();
        var msisdn = $("input#msisdn").val();
        var city = $("#registerCity").val();
        var emailAd = $("#checkbox-mini-0").is(':checked');

        var checkInput = true;

        var selectorInput = '#registerPage input[type=tel], #registerPage input[type=text], #registerPage input[type=email], #registerPage input[type=password]';

        $(selectorInput).removeClass('formError');
        $('#registerPage select').parent().removeClass('formError');

        $(selectorInput).each(function() {
            if ($(this).val().length <= 2) {
                console.log($(this).attr('placeholder'));
                $('#' + $(this).attr('id')).addClass('formError');
                checkInput = false;
                console.log($(this).attr('maxlength'));
            }
        });

        if (checkInput === false) {
            $.mobile.loading('hide');
            generalAlert(LANG.alertfields.text);
            return false;
        } else {

            var filterUser = /^([a-zA-Z0-9ñÑ_\.\-])+$/;
            if (!filterUser.test(user)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertinvaliduser.text);
                $("#user").addClass('formError');
                return false;
            }
            if (!filter.test(email)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alerttypeemail.text);
                $("#email").addClass('formError');
                return false;
            }

            var filterPhone = /^([0-9])+$/;
            if (msisdn.length > 10 || msisdn.length < 10 || !filterPhone.test(msisdn)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertinvalidphone.text);
                $("#registerPage input#msisdn").addClass('formError');
                return false;
            }

            if (user.length > 12 || pass.length > 15 || name.length > 30 || last.length > 30 || address.length > 45 || email.length > 45) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertlongfield.text);

                if (user.length > 12) {
                    $("#user").addClass('formError');
                }
                if (pass.length > 15) {
                    $("#pass").addClass('formError');
                }
                if (name.length > 30) {
                    $("#name").addClass('formError');
                }
                if (last.length > 30) {
                    $("#last").addClass('formError');
                }
                if (address.length > 45) {
                    $("#address").addClass('formError');
                }
                if (email.length > 45) {
                    $("#email").addClass('formError');
                }
                return false;
            }

            if (country === 0 || country == "0" || country == "") {
                $.mobile.loading('hide');
                generalAlert(LANG.alertcountryfield.text);
                $('#registerCountry').parent().addClass('formError');
                return false;
            }
            if (city === 0 || city == "0" || city == "") {
                $.mobile.loading('hide');
                generalAlert(LANG.alertcityfield.text);
                $('#registerCity').parent().addClass('formError');
                return false;
            }

            if (pass == pass1) {
                var dataString = 'user=' + user + '&pass=' + encrypt(pass) + '&name=' + name + '&last_name=' + last + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city + '&emailad=' + emailAd;
                userRegister(dataString);
            } else {
                $.mobile.loading('hide');
                generalAlert(LANG.alertpasswords.text);
                $("#pass1").addClass('formError');
            }
        }
    });

    // PAGE EVENT
    $(document).on("pagecontainerbeforeshow", function(event, ui) {

        var imessage,
            page = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id,
            prevPage = typeof ui.prevPage[0] != 'undefined' ? ui.prevPage[0].id : null;

        // Debug
        console.log(prevPage);
        console.log(page);
        console.log(logueado);

        if($('#'+page).hasClass( "tutorial-page" )){
            $( ".footer-principal" ).toolbar( "hide" );
        }

        switch (page) {
        case 'addVehicle':
            $('#newPlate').val("");
            break;
        case 'login':
            if (logueado === 1) {
                $.mobile.loading('show');
                $("body").pagecontainer("change", "#menu", {
                    transition: "slide"
                });
            } else {
                setTimeout(function() {
                    navigator.splashscreen.hide();
                    _hidesplash = true;
                }, 2000);
            }
            break;
        case 'menu':
            if(_hidesplash === false){
                setTimeout(function() {
                    navigator.splashscreen.hide();
                    _hidesplash = true;
                }, 2000);
            }
            // If online create maps
            if(_mapsLoaded === false && _networkOnline === true){
                initMapsIframe();
            }
            break;
        case 'paycityparking':
            $('#parkingCode').val('');
            $('#payCode').val('');
            break;
        case 'mapaZonesLocalization':
            // If map not already created
            if(_mapsLoaded === false){
                initMapsIframe();
            }
            var mvzl = document.getElementById('frame_mvzl').contentWindow;

            imessage = {
                type: 'loadmap',
                info: {zones: rampgap.getCurrentZoneInfo(),
                       lat: rampgap.getCurrLatitude(),
                       log: rampgap.getCurrLongitude()
                      },
                data: myData,
                platform: _getPlatform,
                action: 'initializeMapPosition'
            };

            mvzl.postMessage(imessage, _getPlatform.protocol+document.location.host);
            break;
        case 'viewZonesMapResult':
            // If map not already created
            if(_mapsLoaded === false){
                initMapsIframe();
            }
            var mvz = document.getElementById('frame_mvz').contentWindow;

            imessage = {
                type: 'loadmap',
                info: {zones: rampgap.getCurrentZoneInfo()},
                data: myData,
                platform: _getPlatform,
                action: 'initializeMap'
            };

            mvz.postMessage(imessage, _getPlatform.protocol + document.location.host);
            break;
        case 'registerPage':
            getCountries();
            break;
        }

    });

    $(document).on("pagecontainershow", function(event, ui) {
        // Test fix Height Wp
        $.mobile.resetActivePageHeight();

        var page = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;
        switch (page) {
        case 'mapaZonesLocalization':
            var lheight = rampgap.getContentHeigth()-($('.locatezonesalerttxt').outerHeight( true )||0)+'px';
            $('#'+page+' iframe').height(lheight);
            //$('#'+page+' iframe').contents().find(".maps").height(lheight);
            break;
        case 'viewZonesMapResult':
            var wheight = rampgap.getContentHeigth();
            $('#'+page+' iframe').height(wheight);
            break;
        }

        // Send page visit to Analytics
        rampgap.ga('send', 'pageview', {'page': page});

    });

    $(document).on('pagecreate', '#viewAccountInfo', function() {
        $('#userInfoForm .ui-input-datebox .ui-btn').hide();
        $('#userInfoForm select').prop('disabled', true);
    });

    //SOCIAL BUTTONS

    //data for wp8 - socialsharing not support wp8.1
    function shareLinkHandler(e) {
        var request = e.request;
        request.data.properties.title = LANG.shareTitle.text;
        request.data.properties.description = LANG.shareMessage.text;
        request.data.setText(LANG.shareMessage.text+' - '+new Windows.Foundation.Uri(LANG.shareLink.text));
        //request.data.setWebLink(new Windows.Foundation.Uri(LANG.shareLink.text));
    }

    $('.cps-share').click(function (e) {

        if (_getPlatform.platformId == '3') {
            var dataTransferManager = Windows.ApplicationModel.DataTransfer.DataTransferManager.getForCurrentView();
            dataTransferManager.addEventListener("datarequested", shareLinkHandler);
            Windows.ApplicationModel.DataTransfer.DataTransferManager.showShareUI();
            rampgap.ga('send', 'event', 'share', 'wp');
        } else {
            window.plugins.socialsharing.share(
                LANG.shareMessage.text,
                LANG.shareTitle.text,
                null,
                LANG.shareLink.text,
                function () {
                    console.log('share ok');
                    // Send event share success to Analytics
                    rampgap.ga('send', 'event', 'share', 'success');
                },
                function (msg) {
                    console.log('error: ' + msg);
                    // Send event share error to Analytics
                    rampgap.ga('send', 'event', 'share', 'error');
                }
            );
        }
    });

    $('.btn-fb').click(function(e){
        console.log(e);
        rampgap.social('https://facebook.com/CityParking');
    });

    $('.btn-tw').click(function(e){
        rampgap.social('https://twitter.com/CityParkingsas');
    });

    //ZONES SUB BUTTONS
    $('#btnViewZones').click(function() {
        getCities(myData.country, "#viewZones");

    });
    $('#btnViewZonesMap').click(function() {
        getCities(myData.country, "#viewZonesMap");
    });

    $('#btnViewCityZones').click(function() {
        viewZones($('#viewZones #citiesSelect').val());
    });

    $('#btnViewCityZonesMap').click(function() {
        viewZonesMap($('#viewZonesMap #citiesSelect').val());
    });
    $('#btnLocateZones').click(function() {
        locateZones();
    });
    $('.viewZoneOptions').click(function() {
        viewZoneOptions($(this).attr('rel'), $(this).attr('title'));
    });

    $('.viewZoneSMenu').click(function() {
        viewZoneOptions($(this).attr('rel'), $(this).attr('title'));
    });

    $(document).on('click', '.viewZoneMap', function(e){
        var zone = $(this).attr('data-zone');
        viewZoneMap(zone);
    });

    //PARKING SUB BUTTONS
    $(document).on('click', '.parkingbutton', function() {
        $("#popPayParking").popup("open");
    });

    //DRIVER SUB BUTTONS

    $('#goDriverPage').click(function() {
        window.location.href = 'cityDriver.html';
    });

    $(document).on('click', '#btn-driverLocation', function() {
        locateDriver();
    });

    $(document).on('click', '#btn-driverCheck', function() {
        if ($('#driverAdrressPick').val().length > 3) {
            sendInputDriver();
        } else {
            generalAlert(LANG.alertnodriverorig.text);
        }
    });

    $(document).on('click', '#btn-driverCheckDest', function() {
        if ($('#driverAdrressPickDest').val().length > 3) {
            sendInputDriver();
        } else {
            generalAlert(LANG.alertnodriverdest.text);
        }
    });

    $(document).on('click', '#openDriverForm', function() {
        $('#driverFullName').val(myData.name + ' ' + myData.lastname);
        $('#driverMsisdn').val(myData.msisdn);
        $('#popOrigAddress').val($('#driverAdrressPick').val());
        $('#popDestAddress').val($('#driverAdrressPickDest').val());

        $("#driverForm").popup({
            afteropen: function(event, ui) {
                $('#driverContact').focus();
            }
        });
    });

    $(document).on("focusin", "#driverAdrressPick, #driverAdrressPickDest", function(event) {
        $("#popHelpAddress").fadeIn();
        setTimeout(function() {
            $("#popHelpAddress").fadeOut();
        }, 5000);
        event.preventDefault();
    });
    $(document).on("focusout", "#driverAdrressPick, #driverAdrressPickDest", function(event) {
        $("#popHelpAddress").fadeOut();
        event.preventDefault();
    });


    //VEHICLES SUB BUTTONS
    $('#btnViewVehicles').click(function() {
        myVehicles();
    });
    $('#btnAddCar').click(function() {
        if ($('#newPlate').val() == "") {
            generalAlert(LANG.alertcarplate.text);
        } else {
            var newPlate = $('#newPlate').val().toUpperCase();
            addVehicle(newPlate);
        }

    });

    //ACCOUNT BUTTONS
    $('#btnViewAccount').click(function() {
        if($('#infoCountry option').length <=1){
            getCountries();
        }
        viewAccountInfo();
    });

    $('#btnSavePass').click(function() {
        if ($('#oldPass').val() == "" || $('#newPass').val() == "" || $('#newPass').val() != $('#rePass').val()) {
            generalAlert(LANG.alertpassequals.text);
        } else {
            savePassword($('#oldPass').val(), $('#oldPass').val(), $('#newPass').val());
        }
    });
    $('#btnEditAccount').click(function() {
        $('#userInfoForm').find('input,select').each(function() {
            //$(this).removeAttr('disabled');
            //$(this).attr("disabled",false);
            // this.disabled = false;
            //console.info("item name="+ $(this).prop("id"));
            //$(this).removeAttr('disabled').removeClass('ui-state-disabled').parent().removeClass('ui-state-disabled');
            $(this).removeAttr('readonly');
        });

        $('#userInfoForm .ui-input-datebox .ui-btn').show();
        $('#userInfoForm select').prop('disabled', false);

        //Fix select city
        var currentCity = $('#infoCity').parent().find('span').text();
        $('#infoCity option').each(function() {
            if ($(this).text() == currentCity) {
                $(this).prop('selected', true).change();
            }
        });

        $('#infoCU').attr('disabled', 'disabled').addClass('ui-state-disabled');
        $('#infoLogin').attr('disabled', 'disabled').addClass('ui-state-disabled');
        $('#userInfoForm').trigger("refresh");
        $('#btnEditAccount,#btnBackAccount').hide();
        $('#btnSaveAccount,#btnCancelEdit').show();
    });
    $('#btnCancelEdit').click(function() {
        viewAccountInfo();
        $('#userInfoForm').find('input,select').each(function() {
            //$(this).attr("disabled","disabled").addClass('ui-state-disabled').parent().addClass('ui-state-disabled');
            $(this).attr('readonly', 'readonly');
        });
        $('#userInfoForm .ui-input-datebox .ui-btn').hide();
        $('#userInfoForm select').prop('disabled', true);
        $('#userInfoForm').trigger("refresh");
        $('#btnSaveAccount,#btnCancelEdit').hide();
        $('#btnEditAccount,#btnBackAccount').show();

        var selectorInput = '#userInfoForm input[type=text],#userInfoForm input[type=tel],#userInfoForm input[type=email], #userInfoForm input[type=password]';
        $(selectorInput).removeClass('formError');
    });
    $('#btnSaveAccount').click(function() {
        navigator.notification.confirm(
            LANG.alertconfirmmodifyinfo.text,
            saveUserUpdate,
            'CPS PARKING', ['OK', 'Cancel']
        );

        //rampgap.confirmDialog(LANG.alertconfirmmodifyinfo.text, "saveUserUpdate");
    });
    $('#changePass').click(function() {
        $('#userPass .campo').each(function() {
            $(this).val('');
        });
    });

    $('#btnChangeUser').click(function() {
        navigator.notification.confirm(
            LANG.alertlogout.text,
            changeUser,
            'CPS PARKING', ['OK', 'Cancel']
        );
    });

    //BALANCE
    $('#viewBalance').click(function() {
        $.mobile.loading('show');
        $.ajax({
            data: 'op=2&user=' + myData.login + '&pass=' + encrypt(myData.pass),
            success: function(response) {
                if (response == null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response === null) {
                        $.mobile.loading('hide');
                        return false;
                    }
                    $.mobile.loading('hide');
                    //NT - Actualizando saldo de usuario en SESSION
                    rampgap.updateBalance(response.balance);
                    var balance = '<p>' + LANG.balancetxt.text + ': $ ' + response.balance + '</p>';
                    //Fix error append wp8.1
                    balance = rampgap.toStatic(balance);

                    $('#infoBalanceContent').html(balance);
                    //$.mobile.changePage($("#infoBalance"));
                    $("body").pagecontainer("change", "#infoBalance", {
                        transition: "slide"
                    });

                }
            }
        });
    });

    $('#reloadBalancePin').click(function() {
        var pin = $('#pinNumber').val();
        pin = pin.replace(' ', '');
        if (pin == "") {
            generalAlert(LANG.alerrtypepin.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=13&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&pin=' + pin,
            success: function(response) {
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == "") {
                        $.mobile.loading('hide');
                        return false;
                    }
                    if (response.code == -101) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertinvalidpin.text);
                        return false;
                    }
                    $('#pinNumber').val('');
                    //NT - Actualizando saldo de usuario en SESSION
                    rampgap.updateBalance(response.obj.balance);
                    $.mobile.loading('hide');
                    generalAlert(LANG.alertreloadok.text);
                    //$.mobile.changePage($("#balance"));
                    $("body").pagecontainer("change", "#balance", {
                        transition: "slide"
                    });

                }
            }
        });
    });

    //--end reload balance pin


    $('#rbalancezplink').click(function() {
        //generalAlert("response");

        $.ajax({
            data: 'op=27&user=' + myData.login + '&pass=' + encrypt(myData.pass),
            success: function(response) {
                console.log(JSON.stringify(response));
                if (response === null) {
                    generalAlert("null");
                    $.mobile.loading('hide');
                    return false;
                } else {
                    $.mobile.loading('hide');
                    if (response == "") {

                        return false;
                    }
                    //generalAlert("code"+response.code);
                    if (response.code == -100) {
                        //rampgap.confirmDialog(response.obj, "javascript:confirmPayZp()");
                        navigator.notification.confirm(
                            response.obj,
                            confirmPayZp,
                            LANG.alertconfirmrbalnacetitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
                        );
                        $("body").pagecontainer("change", "#balance", {
                            transition: "slide"
                        });
                        return false;
                    }
                    if (response.code == -101) {
                        alert(response.obj);
                        $("body").pagecontainer("change", "#balance", {
                            transition: "slide"
                        });
                        return false;
                    }
                    $("body").pagecontainer("change", "#reloadBalanceZP", {
                        transition: "slide"
                    });

                }
            },
            error: function(response, textStatus, errorThrown) {
                generalAlert(textStatus);
            }


        });

    });



    // reload balance ZP
    $('#reloadBalanceZPButton').click(function() {

        var value = $('#payValue').val();

        //generalAlert('value='+value);

        value = value.replace(' ', '');
        if (isNaN($('#payValue').val())) {
            generalAlert(LANG.alertypenumber.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=26&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&value=' + value,
            success: function(response) {
                console.log(JSON.stringify(response));
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == "") {
                        $.mobile.loading('hide');
                        return false;
                    }
                    $.mobile.loading('hide');
                    if (response.code == -101) {

                        generalAlert(LANG.alertinvalidpin.text);
                        return false;
                    }
                    $('#payValue').val('');
                    //NT - Actualizando saldo de usuario en SESSION
                    //rampgap.updateBalance(response.obj.balance);
                    //alert(response.obj);
                    var myURL = encodeURI(response.obj);
                    var ref = window.open(myURL, '_system');

                    //rampgap.openURL(response.obj, "ZonaPagos");

                    //  generalAlert(LANG.alertreloadok.text);
                    $("body").pagecontainer("change", "#balance", {
                        transition: "slide"
                    });
                }
            },
            error: function(response, textStatus, errorThrown) {

                generalAlert(LANG.alertypenumber.text);
                $.mobile.loading('hide');
                return false;
            }


        });
    });
    //--end reload balance ZP

    /**
     * Button to fire give balance event
     */
    $('#btngiveBalance').click(function() {
        var repass = $('#givepassword').val();
        var cu = $('#cu').val();
        var recu = $('#recu').val();
        var amount = $('#amount').val();

        cu = cu.replace(' ', '');
        recu = recu.replace(' ', '');
        amount = amount.replace(' ', '');
        amount = amount.replace('.', '');
        amount = amount.replace(',', '');

        if (cu == "" || recu == "" || cu != recu) {
            generalAlert(LANG.alertcheckcu.text);
            return false;
        } else {
            if (amount == "") {
                generalAlert(LANG.alertamount.text);
                return false;
            }
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=14&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&cu=' + cu + '&amm=' + amount + '&repass=' + encrypt(repass),
            success: function(response) {

                console.log(response);

                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == "") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertcheckdata.text);
                        return false;
                    } else if(response.code == -1 ) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertinvalidcu.text);
                        return false;
                    } else if (response.code == -2) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertcheckdata.text);
                        return false;
                    } else if(response.code == -112){
                        //NT - Actualizando saldo de usuario en SESSION
                        rampgap.updateBalance(response.obj.balance);

                        $('#givepassword').val('');
                        $('#cu').val('');
                        $('#recu').val('');
                        $('#amount').val('');
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertgiveok.text);

                        $("body").pagecontainer("change", "#balance", {
                            transition: "slide"
                        });
                    }

                }
            }
        });
    });

    /**
     * pupulate city wheter country
     */
    $('#infoCountry').change(function(e) {
        getCitiesRegister($('#infoCountry').val());
    });

    $('#carPLate').change(function(e) {
        $('#startParking').find('form').find('span:first').html($(this).val());
    });

    /**
     * Button to change language
     */
    $('#saveLang').click(function(e) {

        e.preventDefault();
        $.mobile.loading('show');
        //$.cookie('lang', $('#langSelect').val(), { expires: 365 });
        rampgap.setItem("lang", $('#langSelect').val());
        setLanguage($('#langSelect').val());
        $('#langSelect').val($('#langSelect').val());
        initialize();
        $.mobile.loading('hide');
        //window.localStorage.setItem("lang", $('#langSelect').val());
        //$.mobile.changePage($("#menu"));
        $("body").pagecontainer("change", "#menu", {
            transition: "slide"
        });


        /*e.preventDefault();
         $.mobile.loading( 'show' );
         $.cookie('lang', $('#langSelect').val(), { expires: 365 });
         location.reload(true);
         rampgap.setItem("lang", $('#langSelect').val());
         rampgap.openLocation('file:///android_asset/www/index.html#settings');*/

    });
    /**
     * on click sends suggestion
     */
    $('#sendSug').click(function(e) {
        var msg = $('#suggestiontxt').val();
        if (msg == "" || msg == " " || msg == "  ") {
            generalAlert(LANG.alertmsgempty.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=19&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&sug=' + msg,
            success: function(response) {
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == "") {
                        $.mobile.loading('hide');
                        return false;
                    }
                    if (response == "[]") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertmsgempty.text);
                        return false;
                    }
                    $('#suggestiontxt').val('');
                    $.mobile.loading('hide');
                    generalAlert(LANG.suggestionsend.text);
                    //$.mobile.changePage($("#menu"));
                    $("body").pagecontainer("change", "#menu", {
                        transition: "slide"
                    });

                }
            }
        });
    });

    /*
     * Envio de Pago a  City Parking
     */
    $('#sendPayment, #sendPaymentPop').click(function(e) {
        var pcode,
            paycode;

        if(e.target.id == 'sendPaymentPop'){
            pcode = $('#parkingCodePop').val();
            paycode = $('#payCodePop').val();
        } else {
            pcode = $('#parkingCode').val();
            paycode = $('#payCode').val();
        }

        if (pcode == "" || pcode == " " || pcode == "  ") {
            generalAlert(LANG.alertparkingempty.text);
            return false;
        }
        if (paycode == "" || paycode == " " || paycode == "  ") {
            generalAlert(LANG.alertpayempty.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=24&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&parkcode=' + pcode + '&paycode=' + paycode,
            success: function(response) {
                console.log(response.value);
                console.log(JSON.stringify(response));
                console.log(parseInt(response.value));
                if (response === null) {
                    $.mobile.loading('hide');
                    generalAlert(LANG.alertconfirmpaymentiderror.text);
                    return false;
                } else {
                    if (response == "") {

                        $.mobile.loading('hide');
                        generalAlert(LANG.alertconfirmpaymentiderror.text);
                        return false;
                    }
                    if (response == "[]") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertmsgempty.text);
                        return false;
                    }
                    if (response == "-1") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertmsgempty.text);
                        return false;
                    }

                    $.mobile.loading('hide');
                    var res = LANG.alertpaidparkingerror.text;
                    //response=$.parseJSON(response);

                    if (parseInt(response.value) == 1 || response.value == 1) {
                        var msg = $.format(LANG.alertconfirmpayment.text, response.info.id, response.info.placeId, response.info.parkingInitDate, response.info.parkingEndDate, response.info.paymentValue, response.info.parkingTime);
                        navigator.notification.confirm(
                            msg,
                            function(buttonIndex) {
                                confirmParking(buttonIndex, response.info);
                            },
                            LANG.alertconfirmpaymenttitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
                        );
                        return;
                    } else if (parseInt(response.value) == 0 || response.value == 0) {
                        res = LANG.alertpaidparkingoffline.text;
                    } else if (parseInt(response.value) == 14) {
                        res = LANG.alertpaidparkingtokenerror.text;
                    } else if (parseInt(response.value) == 3) {
                        res = LANG.alertconfirmpaymentiderror.text;
                    } else if (response.value == "-1") {
                        res = LANG.alertpaycheck.text;
                    }
                    generalAlert(res);

                }
            }
        });
    });

    //****************************************************************************************************************************************************** END READY************************************************
});

function confirmPayZp(buttonIndex) {
    if (buttonIndex === 2) {
        return false;
    }
    $("body").pagecontainer("change", "#reloadBalanceZP", {
        transition: "slide"
    });


}

function confirmParking(buttonIndex, infopay) {

    if (buttonIndex === 2) {
        return false;
    }
    console.log(infopay.id);
    $.mobile.loading('show');

    $.ajax({
        data: 'op=25&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&id=' + infopay.id + '&validationNumber=' + infopay.validationNumber +
            '&operationResult=' + infopay.operationResult + '&token=' + infopay.token + '&value=' + infopay.paymentValue + '&placeid=' + infopay.placeId +
            '&initparking=' + infopay.parkingInitDate + '&endparking=' + infopay.parkingEndDate,
        success: function(response) {
            console.log(JSON.stringify(response));
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    $.mobile.loading('hide');
                    return false;
                }
                if (response == "[]") {
                    $.mobile.loading('hide');
                    generalAlert(LANG.alertmsgempty.text);
                    return false;
                }
                $.mobile.loading('hide');
                var res = LANG.alertpaidparkingerror.text;

                if (response == "12" || parseInt(response) == 12) {
                    res = LANG.alertpaidparkingok.text;
                    $('#parkingCode, #parkingCodePop').val('');
                    $('#payCode, #payCodePop').val('');
                    $("body").pagecontainer("change", "#menu", { transition: "slide"});
                } else if (parseInt(response) == 13)
                    res = LANG.alertpaidparkingnullerror.text;
                else if (parseInt(response) == 14)
                    res = LANG.alertpaidparkingtokenerror.text;
                else if (parseInt(response) == 9)
                    res = LANG.alertpaidparkingfail.text;
                generalAlert(res);

            }
        }
    });

}

// LOGIN -----------------------------------------------------------------
function changeUser(buttonIndex) {

    if (buttonIndex == 2) {
        return false;
    }

    rampgap.setItem('login', null);
    rampgap.setItem('pass', null);

    window.localStorage.setItem('logueado', "0");
    window.localStorage.removeItem('login');
    window.localStorage.removeItem('pass');

    session = 0;
    logueado = 0;

    //$.mobile.changePage($("#login"));
    $("body").pagecontainer("change", "#login", {
        transition: "slide"
    });
    $("#login").val('');
    rampgap.destroySession();
}


function checkAppVersion() {
    console.log(_getPlatform);
    var returnCheck = false,
        platformId = _getPlatform.platformId;

    $.mobile.loading('show');
    $.ajax({
        data: 'op=28&deviceid=' + platformId + '&version=' + rampgap.getAppVersion(),
        async: false,
        success: function(response) {
            console.log('response:' + response);
            console.log(JSON.stringify(response));
            if (response == false) {
                checkSession();
                returnCheck = true;
            } else if (response == "") {
                generalAlert(LANG.alerttimeouterror.text);
            } else {

                setTimeout(function() {
                    navigator.notification.alert(
                        LANG.alertnewupdate.text,
                        function() {
                            _storeLink(platformId);
                        },
                        'CPS PARKING',
                        'Ok'
                    );
                }, 100);
                returnCheck = false;
            }
            $.mobile.loading('hide');
        }

    });
    return returnCheck;
}

/**
 * Connect to APP Store
 */
function _storeLink(platformId) {
    var myURL,
        AProtocol;


    switch (platformId) {
    case "1":
        CanOpen('market://', function (isInstalled) {
            if (isInstalled) {
                myURL = encodeURI('market://details?id=appandroidcity.cps');
            } else {
                myURL = encodeURI('https://play.google.com/store/apps/details?id=appandroidcity.cps');
            }
            window.open(myURL, '_system');
        });
        break;
    case "2":
        myURL = encodeURI('https://itunes.apple.com/us/app/cityparking/id868140927?ls=1');
        window.open(myURL, '_system');
        break;
    case "3":
        myURL = encodeURI('zune:navigate?appid=e76a319a-665f-4d09-9e15-2eaab2f660d4');
        window.open(myURL, '_system');
        break;
    }
}

/**
 * verify if current user has a valid session
 */
function checkSession() {

    console.log(window.localStorage.getItem('logueado'));

    if (rampgap.getItem("login") !== null && rampgap.getItem("login") && rampgap.getItem("pass") !== null &&
        rampgap.getItem("login") != 'null' && rampgap.getItem("pass") != 'null') {
        session = 1;
        logueado = 1;

        login(rampgap.getItem("login"), rampgap.getItem("pass"));

    } else if (window.localStorage.getItem('logueado') == "1") {
        $.mobile.loading('show');
        session = 1;
        logueado = 1;

        var _loginStorage = window.localStorage.getItem('login');
        var _passStorage = window.localStorage.getItem('pass');
        login(_loginStorage, _passStorage);

    } else {
        $('#user-login').show();
    }
}
/**
 * Request for login from server. on success go to menu and keep a valid session
 * @param user valid username
 * @param pass valid password
 *
 */
function login(user, pass) {

    /*
     SUCCES
     {"idsysUser":"3","address":"Kra 41G #47-12","balance":"$57.925","birthDate":"1984-11-19","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
     */
    $.mobile.loading('show');

    if (user === '' || pass === '') {
        $.mobile.loading('hide');
        generalAlert(LANG.alertusername.text);
        return false;
    }

    $.ajax({
        data: 'op=0&user=' + user + '&pass=' + encrypt(pass) + '&device=' + _getPlatform.platform,
        success: function(response) {
            console.log('Success' + JSON.stringify(response));
            if (response === null) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertusername.text);
                return false;
            } else {
                //NT - Seteando values de usuario
                rampgap.setUserSession(response.login, pass, response.balance);
                //User Data
                myData['country'] = response.countryIdcountry;
                myData['city'] = response.city_idcity.idcity;
                myData['city_name'] = response.city_idcity.name;
                myData['msisdn'] = response.favoriteMsisdn;
                myData['idu'] = response.idsysUser;
                myData['address'] = response.address;
                myData['balance'] = response.balance;
                myData['cu'] = response.cu;
                myData['email'] = response.email;
                myData['usertype'] = response.idsysUserType;
                myData['name'] = response.name;
                myData['lastname'] = response.lastName;
                myData['login'] = response.login;
                myData['pass'] = pass;
                logueado = 1;
                $.mobile.loading('hide');
                $("body").pagecontainer("change", "#menu", {
                    transition: "pop"
                });

                if (session === 0) {
                    rampgap.setItem("login", response.login);
                    rampgap.setItem("pass", pass);

                }
                //Save login
                if (window.localStorage.getItem('logueado') != '1') {

                    window.localStorage.setItem('logueado', "1");
                    window.localStorage.setItem('login', response.login);
                    window.localStorage.setItem('pass', pass);
                }

                // Start Session and set user login in Analytics
                rampgap.ga('set', 'uid', response.login);

            }

        },
        error: function(response, textStatus, errorThrown) {
            console.log('ERROR' + JSON.stringify(response));
            if (response.status == 1001 || response.status === 0) {
                generalAlert(LANG.alertusername.text);
                if(_initAnalytics === true){
                    rampgap.ga('send', 'event', 'error', 'login'+response.status);
                    rampgap.ga('send', 'exception', 'Error login: '+response.status, true);
                }
            }
            $.mobile.loading('hide');
        }
    });

}

/**
 * get user info,
 * @param type contains info for get aditional data
 */
function getUserInfo(type) {

    var _loginStorage = window.localStorage.getItem('login');
    var _passStorage = window.localStorage.getItem('pass');

    $.mobile.loading('show');
    $.ajax({
        data: 'op=2&user=' + _loginStorage + '&pass=' + encrypt(_passStorage),
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                //User Data
                myData['country'] = response.countryIdcountry;
                myData['city'] = response.city_idcity.idcity;
                myData['city_name'] = response.city_idcity.name;
                myData['msisdn'] = response.favoriteMsisdn;
                myData['idu'] = response.idsysUser;
                myData['address'] = response.address;
                myData['balance'] = response.balance;
                myData['cu'] = response.cu;
                myData['email'] = response.email;
                myData['usertype'] = response.idsysUserType;
                myData['name'] = response.name;
                myData['lastname'] = response.lastName;
                myData['login'] = response.login;
                myData['pass'] = _passStorage;
                logueado = 1;
                $.mobile.loading('hide');

                if (type == 'driver') {
                    // Load plate and init locate driver
                    var cars = '';
                    $.mobile.loading('show');
                    $.ajax({
                        data: 'op=15&user=' + myData.login + '&pass=' + encrypt(myData.pass),
                        success: function(response) {
                            if (response === null) {
                                $.mobile.loading('hide');
                                return false;
                            } else {
                                if (response == "") {
                                    generalAlert(LANG.alertnocars.text);
                                    $.mobile.loading('hide');
                                    return false;
                                } else {
                                    for (var i = 0; i < response.length; i++) {
                                        if (i === 0) {
                                            cars += '<option value="0" selected="selected">---</option> ';
                                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                                        } else {
                                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                                        }
                                    }

                                    //Fix error append wp8.1
                                    cars = rampgap.toStatic(cars);

                                    $('#driverPlate').html('');
                                    $('#driverPlate').html(cars);
                                    $.mobile.loading('hide');

                                }
                            }
                        }
                    });

                    sendInputDriver();
                    setTimeout(function() {
                        locateDriver();
                    }, 4000);
                } // End if driver

            }
        }
    });
}
/**
 * register a new user in the System,
 * @param dataString contains different parameter information
 */
function userRegister(dataString) {

    /*DATOS PARA REGISTRO
     user
     pass
     name
     last_name
     address
     email
     bd (yyyy-MM-dd)
     country
     msisdn
     city
     */

    $.mobile.loading('show');
    $.ajax({
        data: 'op=1&' + dataString,

        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertalreadyregister.text);
                //Send error event register to Analytics
                rampgap.ga('send', 'event', 'register', 'error');

                return false;
            } else {
                $.mobile.loading('hide');
                generalAlert(LANG.alertregisterok.text);
                $('#registerPage input[type=tel], #registerPage input[type=text], #registerPage input[type=email], #registerPage input[type=password]').val('');
                $('#registerCity [value="0"]').prop('selected', true).change();
                $("body").pagecontainer("change", "#login", {
                    transition: "slide"
                });

                //Send success event register to Analytics
                rampgap.ga('send', 'event', 'register', 'success');
            }
        },
        error: function(response, textStatus, errorThrown) {
            //if(response.status==1001 || response.status==0)
            //{
            generalAlert(LANG.alertalreadyregister.text);
            //}
            $.mobile.loading('hide');
        }
    });
}
// ------------------------------------------------------------------------------ ZONES ------------------------------------------------------------------------------------------
/**
 * return cities by country code
 * @param countryCode, the country code.
 */
function getCities(countryCode, page) {
    $.mobile.loading('show');
    var cities = '';
    $.ajax({
        data: 'op=8&idc=' + countryCode,
        success: function(response) {
            if (response == null) {
                $.mobile.loading('hide');
                return false
            } else {
                for (var i = 0; i < response.length; i++) {
                    if (i == 0) {
                        cities += '<option value="' + response[i].idcity + '" selected="selected">' + response[i].name + '</option> ';
                        var currentCity = response[i].name; //ajustes v1.0.1
                    } else {
                        cities += '<option value="' + response[i].idcity + '">' + response[i].name + '</option> ';
                    }
                }

                //$(page).find('form').find('span:first').html(currentCity); //ajustes v1.0.1
                $.mobile.loading('hide');

                //Fix error append wp8.1
                cities = rampgap.toStatic(cities);

                $(page + ' #citiesSelect').html(cities);
                $(page + ' #citiesSelect [value='+myData.city+']').prop('selected', true).change()

                $("body").pagecontainer("change", page, {
                    transition: "slide"
                });

            }
        }
    });
}

/**
 *
 * @param cityCode
 */
var zones = new Array();

function viewZones(cityCode) {
    $.mobile.loading('show');
    var results = '';
    $.ajax({
        data: 'op=7&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&idc=' + myData.country + '&idcity=' + cityCode,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {

                console.log(response);
                for (var i = 0; i < response.length; i++) {
                    zones[i] = new Array(response[i]);

                    results += '<li class="ui-li-has-count ui-first-child" >';
                    results += '    <a href="#" rel="' + i + '" title="' + response[i].name + '" class="ui-link-inherit viewZoneMap ui-btn ui-btn-icon-right ui-icon-carat-r" data-zone="' + i + '">';
                    results += '        <h3 class="ui-li-heading">' + response[i].name + '</h3>';
                    results += '        <p>' + LANG.placestxt.text + '</p>';
                    if (response[i].places < 0) {
                        response[i].places = 0;
                    }
                    if (response[i].places == '(--)') {
                        results += '        <span class="ui-li-count ui-li-nophone ui-body-b">' + '</span>';
                    } else {
                        results += '        <span class="ui-li-count ui-body-b">' + response[i].places + '</span>';
                    }
                    results += '    </a>';
                    results += '</li>';
                }

                //Fix error append wp8.1
                results = rampgap.toStatic(results);

                $('#zoneResultList').html(results);
                $.mobile.loading('hide');
                $("body").pagecontainer("change", "#viewZonesResult", {
                    transition: "slide"
                });

                $('.viewZoneOptions').click(function() {
                    viewZoneOptions($(this).attr('rel'), $(this).attr('title'));
                });
            }
        }
    });
}

function viewZonesMap(cityCode) {
    $.mobile.loading('show');
    var results = '';
    $.ajax({
        data: 'op=7&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&idc=' + myData.country + '&idcity=' + cityCode,
        success: function(response) {
            //generalAlert(response);
            if (response === null) {
                $.mobile.loading('hide');
                return false
            } else {

                $.mobile.loading('hide');
                //alert(JSON.stringify(response));
                showZonesMap(response);

            }
        },
        error: function(response) {
            generalAlert('error');
        }
    });
}
var ActualzoneArrayIndex;
//if(LANG==LANG_EN){
var weekDays = new Array('', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
//}else{
//var weekDays = new Array ('','Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');

//}
function viewZoneOptions(zoneArrayIndex, zoneName) {
    var zOptions = '';
    ActualzoneArrayIndex = zoneArrayIndex;
    zOptions += '<li data-theme="a" class="ui-first-child ui-last-child ui-li-has-thumb">';
    zOptions += '           <a href="#" rel="' + zoneArrayIndex + '" class="ui-btn ui-btn-icon-right ui-icon-carat-r viewZoneMap" data-zone="' + zoneArrayIndex + '">';
    zOptions += '                <img src="img/ico-zones.png" class="ui-li-thumb">';
    zOptions += '                <h3 class="ui-li-heading">' + LANG.viewmaptxt.text + '</h3>';
    zOptions += '         </a>';
    zOptions += ' </li>';

    //Fix error append wp8.1
    zOptions = rampgap.toStatic(zOptions);

    $('#zoneOptionsList').html(zOptions);
    //$.mobile.changePage($("#viewZonesOptions"));
    $("body").pagecontainer("change", "#viewZonesOptions", {
        transition: "slide"
    });

}

function viewZoneMap(zoneArrayIndex) {
    showZonesMap(zones[zoneArrayIndex]);
}

function showZonesMap(zonemap) {
    rampgap.showZoneSMap(JSON.stringify(zonemap));
}

function showZonesMapLocation(zonemap) {
    rampgap.showZoneSMapLocation(JSON.stringify(zonemap));
}

function viewZoneMapCar(carlat, carlong) {
    rampgap.setCurrentLatLong(carlat + "," + carlong);
    rampgap.showCarMap();
}

function locateZones() {
    //rampgap.verifyGPSIsActive();
    getPosition('locateZonesCoords');
}

function locateZonesCoords(position) {
    console.log(position.coords.latitude);
    var clat = position.coords.latitude;
    var clong = position.coords.longitude;
    rampgap.setCurrLatitude(clat);
    rampgap.setCurrLongitude(clong);
    afterGPSGetLocation(clat, clong);
};

function afterGPSGetLocation(clat, clong) {
    var results = '';
    $.ajax({
        data: 'op=16&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&lat=' + clat + '&lon=' + clong,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    generalAlert(LANG.alertnozone.text);
                    $.mobile.loading('hide');
                    return false;
                } else {
                    showZonesMapLocation(response);
                }
            }
        }
    });
}


// ------------------------------------------------------------------------------ /ZONES ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------ /DRVER ------------------------------------------------------------------------------------------
var driverPosition;

function initDriverPage() {

    $('.driverPickmeup_header .ui-input-text').width($('.origAddress .ui-input-text').width() - $('.btn-driverLocation').width() - $('.btn-driverCheck').width() - 2);
    driverPosition = {
        origen: {
            type: '',
            place: '',
            input: '#driverAdrressPick'
        },
        destino: {
            type: 'address',
            place: '',
            input: '#driverAdrressPickDest'
        }
    };

    initialize();
    getCountries();
    getUserInfo('driver');
}

function sendInputDriver() {
    var address,
        addressDest;

    if ($('#driverAdrressPick').val() != '') {
        address = $('#driverAdrressPick').val();
    } else {
        address = myData.city_name + ',' + countryData[myData.country];
    }
    addressDest = $('#driverAdrressPickDest').val();

    console.log(address);
    var updatePosition = {
        origen: {
            type: 'address',
            place: address
        },
        destino: {
            place: addressDest
        }
    };
    $.extend(true, driverPosition, updatePosition);

    initializeMapDriver('img/flag.png', driverPosition);
}

function locateDriver() {
    console.log('locate driver');
    getPosition('locateDriverCoords');
}

function locateDriverCoords(position) {
    console.log(position.coords.latitude);
    console.log(position.coords.longitude);
    var clat = position.coords.latitude;
    var clong = position.coords.longitude;
    rampgap.setCurrLatitude(clat);
    rampgap.setCurrLongitude(clong);

    var addressDest = $('#driverAdrressPickDest').val();

    var updatePosition = {
        origen: {
            type: 'location',
            place: {
                lat: clat,
                lng: clong
            }
        },
        destino: {
            place: addressDest
        }
    };
    $.extend(true, driverPosition, updatePosition);

    initializeMapDriver('img/flag.png', driverPosition);
};
// ------------------------------------------------------------------------------ /DRIVER ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------ PARKING ------------------------------------------------------------------------------------------

function startParking(plate, num, idp, sms) {
    /*
     SUCCES
     {"code":0,"description":"OK","obj":{"totalMins":0,"maxMins":30,"minuteValue":"0.03","msisdn":"3006658900","startTime":"2011-11-08 02:36:06","endTime":"2011-11-08 02:36:06","idPlace":"01","vehiclePlate":"BJJ118","total":"0.00","userBalance":"204.52","balance":"204.52","minValue":"0.03"}}

     PARQUEADO
     {"code":-113,"description":"Error"}

     VEHICULO NO EXISTE
     {"code":-121,"description":"Error"}

     ZONA NO EXISTE
     {"code":-106,"description":"Error"}

     */


    plate = plate.replace(' ', '');
    $.mobile.loading('show');
    $.ajax({
        data: 'op=24&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&plate=' + plate + '&num=' + num + '&idc=' + myData.country + '&idp=' + idp + '&idu=' + myData.idu + '&sms=' + sms,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {

                $.mobile.loading('hide');
                if (response.code === 0) {
                    //NT - Agregando ActiveParking para procesamiento de Notificaciones
                    rampgap.addActiveParking(
                        response.obj.vehiclePlate,
                        response.obj.minuteValue,
                        response.obj.maxMins
                    );
                    var parkDoneResult = '<div cellspacing="0" cellpadding="0">';
                    parkDoneResult += '<p class="title">' + LANG.parkinginfotxt.text + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.vehicletxt.text + ': </strong>' + response.obj.vehiclePlate + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.starttimetxt.text + ': </strong>' + response.obj.startTime + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.placetxt.text + ': </strong>' + response.obj.idPlace + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.balancetxt.text + ': </strong>CRC ' + response.obj.userBalance + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.minutepricetxt.text + ': </strong>CRC ' + response.obj.minuteValue + '</p>';
                    parkDoneResult += '</div>';
                    //<a data-inline="true" data-role="button" href="#" data-theme="c" class="ui-btn ui-btn-inline ui-btn-corner-all ui-shadow ui-btn-up-c Home"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text hometxt">Home</span></span></a>
                    //$('#parkingDoneInfo').html(parkDoneResult);
                    //$.mobile.changePage($("#parkingDone"));
                    //$( "body" ).pagecontainer( "change", "#parkingDone", { transition: "slide" });
                    //rampgap.confirmDialog(parkDoneResult,
                    //                     "javascript:confirmParking('"+plate+"','"+num+ "','"+ idp+"','"+sms+ "')");
                    navigator.notification.confirm(
                        parkDoneResult,
                        function(buttonIndex) {
                            confirmParking(plate, num, idp, sms);
                        },
                        LANG.alertconfirmgeneraltitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
                    );

                    /*$('.Home').click(function(){
                     //$.mobile.changePage($("#menu"));
                     $( "body" ).pagecontainer( "change", "#menu", { transition: "slide" });

                     });*/
                }
                if (response.code == -113) {
                    generalAlert(LANG.alertalreadyparked.text);
                    return false;
                } else if (response.code == -121) {
                    generalAlert(LANG.alertvehiclenoregistered.text);
                    return false;
                } else if (response.code == -120) {
                    generalAlert(LANG.alertplaceclosed.text);
                    return false;
                } else if (response.code == -106) {
                    generalAlert(LANG.alertplacewrong.text);
                    return false;
                } else if (response.code == -127) {
                    generalAlert(LANG.alertplacenotexist.text);
                    return false;
                } else if (response.code == -128) {
                    generalAlert(LANG.alertplacenot.text);
                    return false;
                } else if (response.code == -103) {
                    generalAlert(LANG.alertnobalance.text);
                    return false;
                }
            }
        }
    });
}


// ------------------------------------------------------------------------------ VEHICLES ------------------------------------------------------------------------------------------

var myCars = new Array();

function myVehicles() {
    /*
     http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
     op = 4
     SUCCES
     [{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

     */
    myCars = '';
    var resultCars = '';
    $.mobile.loading('show');
    $.ajax({
        data: 'op=15&user=' + myData.login + '&pass=' + encrypt(myData.pass),
        success: function(response) {
            if (response == null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    generalAlert(LANG.alertnocars.text);
                    $.mobile.loading('hide');
                    $("body").pagecontainer("change", "#vehicles", {
                        transition: "slide"
                    });
                    return false;
                } else {
                    for (var i = 0; i < response.length; i++) {
                        myCars[i] = new Array(response[i].plate, response[i].dateReg, response[i].owner);
                        //resultCars += '<li><span>'+response[i].plate+' - '+response[i].dateReg+'</span><a href="#" rel="'+response[i].plate+'" class="deleteItem">Delete</a></li>';

                        resultCars += '<li data-theme="a" class="ui-first-child ui-last-child">';
                        if (response[i].owner == '1' || response[i].owner == 1) {
                            resultCars += '    <a href="#" class="ui-btn ui-btn-icon-right ui-icon-delete deleteItem"  rel="' + response[i].plate + '" >';
                        } else {
                            resultCars += '    <a href="#" class="ui-btn">';

                        }
                        resultCars += '     <h3 class="ui-li-heading">' + response[i].plate + '</h3>';
                        resultCars += ' </a>';


                    }

                    //Fix error append wp8.1
                    resultCars = rampgap.toStatic(resultCars);

                    $('#vehiclesList').html(resultCars);
                    $.mobile.loading('hide');
                    //$.mobile.changePage($("#viewVehicles"));
                    $("body").pagecontainer("change", "#viewVehicles", {
                        transition: "slide"
                    });


                    $('.deleteItem').click(function() {
                        deleteCar($(this).attr('rel'));
                    });
                }
            }
        }
    });
}

function loadVehicles() {
    /*
     http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
     op = 4
     SUCCES
     [{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

     */
    var cars = '';
    $.mobile.loading('show');
    $.ajax({
        data: 'op=15&user=' + myData.login + '&pass=' + encrypt(myData.pass),
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    generalAlert(LANG.alertnocars.text);
                    $.mobile.loading('hide');
                    return false;
                } else {
                    for (var i = 0; i < response.length; i++) {
                        if (i === 0) {
                            cars += '<option value="0" selected="selected">---</option> ';
                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                        } else {
                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                        }
                    }
                    //Fix error append wp8.1
                    cars = rampgap.toStatic(cars);

                    $('#startParking').find('form').find('span:first').html(LANG.selectplatetxt.text);
                    $('#carPLate').html('');
                    $('#carPLate').html(cars);
                    $.mobile.loading('hide');
                    //$.mobile.changePage($("#startParking"));
                    $("body").pagecontainer("change", "#startParking", {
                        transition: "slide"
                    });

                }
            }
        }
    });
}


function deleteCar(plate) {
    /*
     op = 17
     idu, plate

     SUCCES
     {"code":-126,"description":"OK"}
     */
    //rampgap.confirmDialog(LANG.alertconfirmcardelete.text, "javascript:_deleteCar('"+plate+"')");
    navigator.notification.confirm(
        LANG.alertconfirmcardelete.text,
        function(buttonIndex) {
            _deleteCar(buttonIndex, plate);
        },
        LANG.alertconfirmcardeletetitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
    );

}

function _deleteCar(buttonIndex, plate) {
    if (buttonIndex === 2) {
        return false;
    }
    $.ajax({
        data: 'op=17&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&plate=' + plate,
        success: function(response) {
            if (response === null) {
                generalAlert(LANG.alertsorry.text);
            } else {
                myVehicles();
            }
        }
    });
}

function addVehicle(plate) {
    /*
     op = 12
     idu, pass, plate
     IDU = LOGIN // OJO CORREGIR
     SUCCES
     {"code":-100,"description":"OK"}

     FAILED
     {"code":1,"description":"Failed"}

     */
    plate = plate.replace(' ', '');
    $.mobile.loading('show');
    $.ajax({
        data: 'op=12&user=' + myData.login + '&pass=' + encrypt(myData.pass) + '&plate=' + plate,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response.code == -102) {
                    generalAlert(LANG.alertvehicleexist.text);
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response.code != -100) {
                        generalAlert(LANG.alertsorry.text);
                        $.mobile.loading('hide');
                        return false;
                    } else {
                        generalAlert(LANG.alertvehicleadded.text);
                        $.mobile.loading('hide');
                        $('#newPlate').val("");
                        myVehicles();
                    }
                }
            }
        }
    });
}

// USER ACCOUNT -----------------------------------------------------------------


function viewAccountInfo() {
    /*
     OP=2 & USER

     SUCCES
     {"idsysUser":"3","address":"Kra 41G #47-12","balance":57.9,"birthDate":"Nov 19, 1984","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
     */
    $.mobile.loading('show');
    $.ajax({
        data: 'op=2&user=' + myData.login + '&pass=' + encrypt(myData.pass),
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                //Update Array myData
                myData['country'] = response.countryIdcountry;
                myData['city'] = response.city_idcity.idcity;
                myData['city_name'] = response.city_idcity.name;
                myData['msisdn'] = response.favoriteMsisdn;
                myData['idu'] = response.idsysUser;
                myData['address'] = response.address;
                myData['balance'] = response.balance;
                myData['cu'] = response.cu;
                myData['email'] = response.email;
                myData['usertype'] = response.idsysUserType;
                myData['name'] = response.name;
                myData['lastname'] = response.lastName;

                //User Data
                var d = new Date(response.birthDate);
                var year = d.getFullYear();
                var monthFormat = d.getMonth() + 1;
                var month = monthFormat < 10 ? "0" + monthFormat : monthFormat;
                var date = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();

                var formatted = year + "-" + month + "-" + date;

                $('#infoCU').val(response.cu);
                $('#infoLogin').val(response.login);
                $('#infoName').val(response.name);
                $('#infoLast').val(response.lastName);
                $('#infoAddress').val(response.address);
                $('#infoBirthDate').val(formatted);
                $('#infoMobile').val(response.favoriteMsisdn);
                $('#infoEmail').val(response.email);

                getCitiesRegister(response.city_idcity.idcountry, response.city_idcity.idcity);

                $('#infoCountry [value="' + response.city_idcity.idcountry + '"]').prop('selected', true).change();

                //generalAlert(LANG.alertregisterinfo.text);

                $.mobile.loading('hide');
                $("body").pagecontainer("change", "#viewAccountInfo", {
                    transition: "slide"
                });

            }
        }
    });
}

function getCountries() {
    /*
     SUCCFES
     [{"idcountry":"1","countryPrefix":"CAN","name":"Canada","latitude":"12365478","longitude":"12365445","lang":"en"}]
     */
    var today = new Date();

    countryGet = window.localStorage.getItem('countryGet');

    $.mobile.loading('show');
    if(!countryGet || today > new Date(countryGet)){

        console.log('GET country');

        $.ajax({
            data: 'op=3',
            success: function(response) {
                console.log(response);
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {

                    var dateSave = new Date();
                    dateSave.setDate(dateSave.getDate() + 15);

                    window.localStorage.setItem('countryGet', dateSave);
                    window.localStorage.setItem('countryData', JSON.stringify(response));

                    countryData = JSON.parse(window.localStorage.getItem('countryData'));
                    pushCountryList(countryData);
                    console.log(countryData);
                }
            }
        });

    } else {
        countryData = JSON.parse(window.localStorage.getItem('countryData'));
        pushCountryList(countryData);
        console.log(countryData);
    }



}

/**
 *
 * @param countryData - append country data in select list
 */
function pushCountryList(countryData){
    var countries = "";
    for (var i = 0; i < countryData.length; i++) {

        if (i === 0) {
            countries += '<option value="0" selected="selected">---</option> ';
            countries += '<option value="' + countryData[i].idcountry + '">' + countryData[i].name + '</option> ';
        } else {
            countries += '<option value="' + countryData[i].idcountry + '">' + countryData[i].name + '</option> ';
        }

    }

    //Fix error append wp8.1
    countries = rampgap.toStatic(countries);

    $('#infoCountry,#registerCountry').html(countries);
    if(myData.country){
        $('#infoCountry [value="' + myData.country + '"]').prop('selected', true).change();
    }
    $.mobile.loading('hide');
}

function getCitiesRegister(countryCode, cityCode) {
    if (countryCode === 0)
        return false;

    $.mobile.loading('show');
    var cities = '';
    $.ajax({
        data: 'op=8&idc=' + countryCode,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                for (var i = 0; i < response.length; i++) {
                    if (i === 0) {
                        cities += '<option value="0" selected="selected">---</option> ';
                        cities += '<option value="' + response[i].idcity + '">' + response[i].name + '</option> ';
                    } else {
                        cities += '<option value="' + response[i].idcity + '">' + response[i].name + '</option> ';
                    }
                }
                $.mobile.loading('hide');

                //Fix error append wp8.1
                cities = rampgap.toStatic(cities);

                $('#registerCity,#infoCity').html(cities);

                if (cityCode !== 0 && cityCode != undefined && cityCode != 'undefined') {
                    $('#infoCity [value="' + cityCode + '"]').prop('selected', true).change();
                }
            }

        }
    });
}

function saveUserUpdate(buttonIndex) {

    if (buttonIndex == 2) {
        return false;
    }
    /*
     OP=10
     name = request.getParameter("name");
     last_name = request.getParameter("last_name");
     address = request.getParameter("address");
     email = request.getParameter("email");
     birthdate = request.getParameter("bd");
     country = request.getParameter("country");
     msisdn = request.getParameter("msisdn");
     city = request.getParameter("city");

     SUCCES
     */
    $.mobile.loading('show');

    var address = $("#infoAddress").val();
    var email = $("#infoEmail").val();
    var bd = $("#infoBirthDate").val(); // (yyyy-MM-dd)
    var country = $("#infoCountry").val();
    myData.country = country; //ajustes v1.0.1
    var msisdn = $("#infoMobile").val();
    var city = $("#infoCity").val();
    var name = $("#infoName").val();
    var last_name = $("#infoLast").val();

    var checkInput = true;
    var selectorInput = '#userInfoForm input[type=text],#userInfoForm input[type=tel],#userInfoForm input[type=email], #userInfoForm input[type=password]';

    $(selectorInput).removeClass('formError');
    $('#userInfoForm select').parent().removeClass('formError');

    $(selectorInput).each(function() {
        if ($(this).val().length <= 2) {
            console.log($(this).attr('placeholder'));
            $('#' + $(this).attr('id')).addClass('formError');
            checkInput = false;
        }
    });

    if (checkInput === false) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertfields.text);
        return false;
    }

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //ajustes v1.0.1
    if (!filter.test(email)) {
        $.mobile.loading('hide');
        generalAlert(LANG.alerttypeemail.text);
        $("#infoEmail").addClass('formError');
        return false;
    }

    var filterPhone = /^([0-9])+$/;
    if (msisdn.length > 10 || msisdn.length < 10 || !filterPhone.test(msisdn)) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertinvalidphone.text);
        $("#infoMobile").addClass('formError');
        return false;
    }

    if (name.length > 30 || last_name.length > 30 || address.length > 45 || email.length > 45) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertlongfield.text);

        if (name.length > 30) {
            $("#infoName").addClass('formError');
        }
        if (last_name.length > 30) {
            $("#infoLast").addClass('formError');
        }
        if (address.length > 45) {
            $("#infoAddress").addClass('formError');
        }
        if (email.length > 45) {
            $("#infoEmail").addClass('formError');
        }
        return false;
    }

    if (country === 0 || country == "0" || country == "") {
        $.mobile.loading('hide');
        generalAlert(LANG.alertcountryfield.text);
        $('#infoCountry').parent().addClass('formError');
        return false;
    }
    if (city === 0 || city == "0" || city == "") {
        $.mobile.loading('hide');
        generalAlert(LANG.alertcityfield.text);
        $('#infoCity').parent().addClass('formError');
        return false;
    }

    var dataString = '&name=' + name + '&last_name=' + last_name + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city;
    //var dataString = '&name=Camilo&last_name=Prueba&address=Calle siempre viva 123&email=cquimbayo@cpsparking.ca&bd=2014-08-26&country=472&msisdn=3178795959&city=103';
    console.log(city);
    console.log(url + '?op=10&user=' + myData.login + '&pass=' + encrypt(myData.pass) + dataString);

    $.ajax({
        data: 'op=10&user=' + myData.login + '&pass=' + encrypt(myData.pass) + dataString,
        success: function(response) {
            console.log(JSON.stringify(response));
            if (response === null) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertcheckdata.text);
                return false;
            } else {
                $.mobile.loading('hide');
                generalAlert(LANG.alertregisterok.text);
                $('#btnCancelEdit').trigger('click');
                viewAccountInfo();
            }
        }
    });
}


function savePassword(pass, oldPass, newPass) {
    $.mobile.loading('show');
    $.ajax({
        data: 'op=11&user=' + myData.login + '&pass=' + encrypt(pass) + '&oldpass=' + encrypt(oldPass) + '&npass=' + encrypt(newPass),
        success: function(response) {
            if (response === null) {
                generalAlert(LANG.alertcheckpassword.text);
                $.mobile.loading('hide');
                return false;
            } else {
                if (response.code == -117) {
                    myData['pass'] = newPass;
                    //$.cookie('pass', newPass, { expires: 7 });
                    rampgap.setItem("pass", newPass);
                    window.localStorage.setItem('pass', newPass);
                    console.log(window.localStorage.getItem('pass'));

                    generalAlert(LANG.alertpasschangeok.text);
                    $.mobile.loading('hide');
                    //$.mobile.changePage($("#userAccount"));
                    $("body").pagecontainer("change", "#userAccount", {
                        transition: "slide"
                    });

                }
            }
        }
    });
}

//---------------------------------------------------------------------------//

function getPosition(onSucces) {
    $.mobile.loading('show');
    if (!navigator.geolocation) {
        navigator.notification.alert(
            LANG.alertgeonotsupported.text,
            alertDismissed,
            'CPS Notification',
            'Ok'
        );
        $.mobile.loading('hide');
        return;
    } else {
        navigator.geolocation.getCurrentPosition(eval(onSucces), function(error) {
            onErrorZone(error, onSucces);
        }, {
            timeout: 27000,
            enableHighAccuracy: true
        });
    }
}

function onErrorZone(error, onSucces) {
    console.log(JSON.stringify(error));
    navigator.geolocation.getCurrentPosition(eval(onSucces), onErrorZone_lowAccuracy, {
        timeout: 27000,
        enableHighAccuracy: false
    });
}

function onErrorZone_lowAccuracy(error) {
    navigator.notification.alert(
        LANG.alertsorry.text + ' ' + error.message,
        alertDismissed,
        'CPS Notification',
        'Ok'
    );

    $.mobile.loading('hide');
    return false;
}
