var SPANISH = 0;
var ENGLISH = 1;

var defaultLang = SPANISH;

var LANG_EN = {
    viewmapcar: {clase: "viewmapcar", text: "Find Car"},
    alertnewupdate:{text: "There is an available update, please update your application before continue."},
    alertbirthday: {text: "Please check your birthday"},
    resetPassword:{clase: "resetPassword", text: "Forgot your password?"},
    selectlangtext:{clase: "English"},
    alertcheckemail: {text: "Please check your email"},
    alerttypeemail: {text: "Please type your email"},
    alertinvaliduser:{text: "Please check your user, without special characters or spaces"},
    alertinvalidphone: {text: "Please type a valid mobile number"},
    alertresetsend: {text: "Your password request has been sent"},
    alertmsgempty: {text: "Please write a message"},
    alertparkingempty: {text: "Cannot send an empty parking code, please type a valid parking code"},
    alertpayempty: {text: "Cannot send an empty payment code, please type a valid payment code"},
    alertpaycheck: {text: "Cannot send an invalid code."},
    alertcheckcountry: {text: "Please check your Country/City selection"},
    alertfields: {text: "Please fill all fields"},
    alertpasswords: {text: "Passwords must be equals"},
    alertcarplate: {text: "Please enter the car plate"},
    alertpassequals: {text: "Check that the new passwords are equal"},
    alertconfirmmodifyinfo: {text: "Are you sure you want to modify your info?"},
    alertlogout: {text: "Are you sure you want to logout?"},
    alertexit: {text: "Are you sure you want to exit?"},
    alerrtypepin: {text: "Please type a PIN number"},
    alertinvalidpin: {text: "Invalid"},
    alertreloadok: {text: "Reload Succesfull"},
    alertcheckcu: {text: "Please check CU and CU Retype"},
    alertamount: {text: "Please type an amount"},
    alertcheckdata: {text: "Please check your data"},
    alertinvalidcu: {text: "Invalid destination unique code (UC), Please check it."},
    alertgiveok: {text: "Give Succesfull"},
    alertusername: {text: "Invalid user or password"},
    alertalreadyregister: {text: "Some of these data have already been registered"},
    alertregisterok: {text: "Register Succesfully"},
    alertnozone: {text: "You are not close to any zone"},
    alertalreadyparked: {text: "This vehicle it's already parked"},
    alertvehiclenoregistered: {text: "This vehicle it's not registered"},
    alertplacewrong: {text: "This place code it's wrong"},
    alertplacenot: {text: "This place it's not available"},
    alertnotparking: {text: "You don't have active parkings"},
    alertnocars: {text: "you don't have registereds cars"},
    alertnobalance: {text: "you don't have balance"},
    alertconfirmcardelete: {text: "Are you sure you want to delete this vehicle?"},
    alertconfirmcardeletetitle: {text: "Delete vehicle"},
    alertsorry: {text: "Sorry, please try again later"},
    alertvehicleexist: {text: "Sorry, this car it's already registered"},
    alertvehicleadded: {text: "Vehicle successfully added"},
    alertcheckpassword: {text: "Please check your typed passwords"},
    alertpasschangeok: {text: "Your password was changed successfully"},
    alertfiveleft: {text: "Just 5 minutes to remove your vehicle"},
    alertoneleft: {text: "Just 1 minute to remove your vehicle"},
    alertvehiclefined: {text: "From this point you will be fined for not removing your vehicle"},
    alertpaidparkingok: {text: "Payment success"},
    alertpaidparkingfail: {text: "Not enough balance to complete operation"},
    alertpaidparkingoffline: {text: "Could not connect to the parking spot. Please try again "},
    alertpaidparkingtokenerror: {text: "Payment token error"},
    alertpaidparkingerror: {text: "Error trying to make parking operation"},
    alertpaidparkingnullerror: {text: "Error due  null field values"},
    alertpaidparkingstoreerror: {text: " Error recording parking operation"},
    alertconfirmpaymentiderror: {text: " Invalid payment code"},
    alerttimeouterror: {text: "Exceeded timeout, please try again"},
    alertnetworkerror: {text: "There is a communication error, check your network connection and try again"},
    alertnotfounderror: {text: "Your request could not be completed, please try again"},
    alertservererror: {text: "There was an internal error, please try again"},
    alertgeneralerror: {text: "There was an unexpected error, please try again"},
    alertnetworkconnection: {text: "The application requires internet for proper operation, check your connection"},
    alertgeonotsupported: {text: "The geolocation is not active, please turn it on."},
    alertconfirmpaymenttitle: {text: "Payment Confirmation"},
    alertconfirmgeneraltitle: {text: "CPS PARKING"},
    alertconfirmpayment: {text: "Information:\nParking Id: %s\nPlace id: %s\nBegin time: %s\nEnd time: %s.\nParking amount: $%s" +
                          "\nParking time: %d"},
    alertroutefail: {text: "We could not get the route, please try again"},

    btnOk: {text: "Ok"},
    btnCancel: {text: "Cancel"},
    backBtn: {clase: "backBtn", text: "Back"},
    edittxt: {clase: "edittxt", text: "Edit"},
    savetxt: {clase: "savetxt", text: "Save"},
    sendtxt: {clase: "sendtxt", text: "Send"},
    /*LOGIN*/
    userName: {clase: "userName", text: "Username"},
    password: {clase: "password", text: "Password"},
    login: {clase: "login", text: "Login"},
    register: {clase: "register", text: "Register"},
    /*END LOGIN*/

    /*REGISTER*/
    name: {clase: "name", text: "Name"},
    lastName: {clase: "lastName", text: "Last Name"},
    mobile: {clase: "mobile", text: "Mobile No."},
    login: {clase: "login", text: "Login"},
    address: {clase: "address", text: "Address"},
    birthday: {clase: "birthday", text: "BirthDay"},
    year: {clase: "year", text: "Year"},
    month: {clase: "month", text: "Month"},
    day: {clase: "day", text: "Day"},
    repassword: {clase: "repassword", text: "Re-Password"},
    email: {clase: "email", text: "E-Mail"},
    country: {clase: "country", text: "Country"},
    city: {clase: "city", text: "City"},
    cancel: {clase: "cancel", text: "Cancel"},
    loginUs: {clase: "loginUs", text: "Login"},
    pageNameRegister: {clase: "pnreg", text: "Register"},

    january:{clase: "january", text: "January"},
    february:{clase: "february", text: "February"},
    march:{clase: "march", text: "March"},
    april:{clase: "april", text: "April"},
    may:{clase: "may", text: "May"},
    june:{clase: "june", text: "June"},
    july:{clase: "july", text: "July"},
    august:{clase: "august", text: "August"},
    september:{clase: "september", text: "September"},
    october:{clase: "october", text: "October"},
    november:{clase: "november", text: "November"},
    december:{clase: "december", text: "December"},

    alertlongfield: {text: "Check the length of the fields"},
    alertcityfield: {text: "Please check your City"},
    alertcountryfield: {text: "Please check your Country"},

    /*END REGISTER*/

    /* MAIN MENU*/
    zonestxt: {clase: "zonestxt", text: "Zones"},
    parkingtxt: {clase: "parkingtxt", text: "Parking"},
    vehiclestxt: {clase: "vehiclestxt", text: "Vehicles"},
    usertxt: {clase: "usertxt", text: "User Account"},
    balancetxt: {clase: "balancetxt", text: "Balance"},
    pnmain: {clase: "pnmain", text: "Main Menu"},
    settingstxt: {clase: "settingstxt", text: "Settings"},
    languagetxt: {clase: "languagetxt", text: "Language"},
    pnsetlang: {clase: "pnsetlang", text: "Set Language"},
    suggestionstxt: {clase: "suggestionstxt", text: "Suggestions"},
    suggestionsend: {text: "Suggestion sent"},
    payparkingtxt: {clase: "payparkingtxt", text: "Pay Parking"},
    logouttxt: {clase: "logouttxt", text: "Logout"},


    /* ZONES*/
    viewzonestxt: {clase: "viewzonestxt", text: "View Zones"},
    locatezonestxt: {clase: "locatezonestxt", text: "Locate Zones"},
    pnzones: {clase: "pnzones", text: "Zones"},
    viewtxt: {clase: "viewtxt", text: "View"},
    canceltxt: {clase: "canceltxt", text: "Cancel"},
    pnviewzones: {clase: "pnviewzones", text: "View Zones"},
    pnzoneresult: {clase: "pnzoneresult", text: "Zones Result"},
    viewdaystxt: {clase: "viewdaystxt", text: "View Days"},
    zoneoptionstxt: {clase: "zoneoptionstxt", text: "Zone Options"},
    pnzonemap: {clase: "pnzonemap", text: "Zone Map"},
    pnzonedays: {clase: "pnzonedays", text: "Zone Days"},
    pnzoneinfo: {clase: "pnzoneinfo", text: "Zone Info"},
    pricetxt: {clase: "price", text: "Price:"},
    placestxt: {clase: "placestxt", text: "Places"},
    viewmaptxt: {clase: "viewmaptxt", text: "View Map"},
    viewpricestxt: {clase: "viewpricestxt", text: "View Prices"},
    maxtimetxt: {clase: "maxtimetxt", text: "Max Time"},
    minutepricetxt: {clase: "minutepricetxt", text: "Minute Price"},
    currenttimetxt: {clase: "currenttimetxt", text: "Current Time"},
    zonetxt: {clase: "zonetxt", text: "Zone"},
    opentimetxt: {clase: "opentimetxt", text: "Open Time"},
    closetimetxt: {clase: "closetimetxt", text: "Close Time"},
    locatezonesalerttxt: {clase: "locatezonesalerttxt", text: "Routes are suggestions, check traffic laws."},

    /* PARKING*/
    startparkingtxt: {clase: "startparkingtxt", text: "Start Parking"},
    activeparkingtxt: {clase: "activeparkingtxt", text: "Active Parking"},
    pnparking: {clase: "pnparking", text: "Parking"},
    zoneplacetxt: {clase: "zoneplacetxt", text: "Zone Place"},
    smsnotitxt: {clase: "smsnotitxt", text: "SMS Notification"},
    parktxt: {clase: "parktxt", text: "Park"},
    pnstartparking: {clase: "pnstartparking", text: "Start Parking"},
    pnparkingdone: {clase: "pnparkingdone", text: "Parking Done"},
    pnparkingoptions: {clase: "pnparkingoptions", text: "Parking Options"},
    pnendparking: {clase: "pnendparking", text: "End Parking"},
    endparkingtxt: {clase: "endparkingtxt", text: "End Parking"},
    endtimetxt: {clase: "endtimetxt", text: "End Time"},
    totaltimetxt: {clase: "totaltimetxt", text: "Total Time"},
    selectplatetxt: {clase: "selectplatetxt", text: "Select Plate"},
    payparkingcodetxt:{clase:"payparkingcodetxt", text:"Parking code:"},
    paycodetxt:{clase:"paycodetxt", text:"Pay code:"},
    payparkingmsgtxt: {clase:"payparkingmsgtxt", text: "Only pay for occasional service"},

    /* VEHICLES */
    addvechicletxt: {clase: "addvechicletxt", text: "Add Vehicle"},
    viewvehiclestxt: {clase: "viewvehiclestxt", text: "View Vehicles"},
    addtxt: {clase: "addtxt", text: "Add"},
    carplatetxt: {clase: "carplatetxt", text: "Car Plate:"},
    pnaddvehicles: {clase: "pnaddvehicles", text: "Add Vechicles:"},
    vehicletxt: {clase: "vehicletxt", text: "Vehicle"},
    starttimetxt: {clase: "starttimetxt", text: "Start Time"},
    placetxt: {clase: "placetxt", text: "Place"},

    /* USER ACCOUNT*/
    viewinfotxt: {clase: "viewinfotxt", text: "View Info"},
    changepasstxt: {clase: "changepasstxt", text: "Change Password"},
    pnuserinfo: {clase: "pnuserinfo", text: "User Info"},
    oldpasstxt: {clase: "oldpasstxt", text: "Old Password"},
    newpasstxt: {clase: "newpasstxt", text: "New Password"},

    /* BALANCE*/
    reloadbalancetxt: {clase: "reloadbalancetxt", text: "Reload PIN Balance"},
    reloadbalancezptxt: {clase: "reloadbalancezptxt", text: "Reload Balance"},
    viewbalancetxt: {clase: "viewbalancetxt", text: "View Balance"},
    givebalancetxt: {clase: "givebalancetxt", text: "Transfer"},
    paypalreloadtxt: {clase: "paypalreloadtxt", text: "Credit card"},
    pinnumbertxt: {clase: "pinnumbertxt", text: "Pin Number"},
    payvaluetxt: {clase: "payvaluetxt", text: "Amount"},
    reloadtxt: {clase: "reloadtxt", text: "Reload"},
    pnbalanceinfo: {clase: "pnbalanceinfo", text: "Balance Info"},
    amounttxt: {clase: "amounttxt", text: "Amount"},
    givetxt: {clase: "givetxt", text: "Give"},
    pngivebalance: {clase: "pngivebalance", text: "Transfer"},
    cudestinationtxt: {clase: "cudestinationtxt", text: "UC Destination user"},
    recudestinationtxt: {clase: "recudestinationtext", text: "Re-UC Destination user"},
    alertconfirmrbalnacetitle: {text: "Reload Confirmation"},

    menuZones: {clase: "zones", text: "Zones"},
    menuParking: {clase: "park", text: "Parking"},
    alertError: {text: "xx"},

    // SHARE
    shareMessage: {text: "The best #parking app  City Parking ¡DOWNLOAD HERE! #Android #Iphone #Wp"},
    shareTitle: {text: "CityParking - Cellular Parking Systems"},
    shareLink: {text: "http://goo.gl/W26B8H"}
};

var LANG_ES = {
    viewmapcar: {clase: "viewmapcar", text: "Buscar Carro"},
    alertnewupdate:{text: "Hay una actualización pendiente, para poder continuar usted debe actualizar la aplicación."},
    alertbirthday: {text: "Por favor revisa tu fecha de nacimiento"},
    selectlangtext:{clase: "Español"},
    resetPassword: {clase: "resetPassword", text: "Olvidaste tu clave?"},
    alertcheckemail: {text: "Por favor revisa tu correo"},
    alerttypeemail: {text: "Por favor ingresa tu correo"},
    alertinvaliduser: {text: "Por favor revisa tu usuario que no tenga caracteres especiales o espacios"},
    alertinvalidphone: {text: "Por favor ingresa un numero de celular valido"},
    alertresetsend: {text: "Se ha enviado tu contraseña al correo registrado"},
    alertregisterinfo: {text: "Si desea cambiar información, oprima sobre el botón Editar."},
    alertmsgempty: {text: "Por favor escribe un mensaje"},
    alertparkingempty: {text: "El código de parqueo no puede estar vacio," +
                        " Por favor introduzca un código de parqueo válido"},
    alertpayempty: {text: "El código de pago no puede estar vacio," +
                    " por favor escriba un código válido"},
    alertpaycheck: {text: "Los códigos ingresados son invalidos."},
    alertcheckcountry: {text: "Por favor revisa la ciudad y/o pais"},
    alertfields: {text: "Por favor llena todos los campos"},
    alertpasswords: {text: "Las claves deben ser iguales"},
    alertcarplate: {text: "Por favor ingresa la placa"},
    alertpassequals: {text: "Revisa que las claves sean iguales"},
    alertconfirmmodifyinfo: {text: "Deseas modificar tu informacion?"},
    alertlogout: {text: "¿Deseas cerrar sesión?"},
    alertexit: {text: "¿Deseas salir?"},
    alerrtypepin: {text: "Por favor ingresa un numero PIN"},
    alertinvalidpin: {text: "Pin Invalido"},
    alertreloadok: {text: "Recarga realizada"},
    alertcheckcu: {text: "Por favor revisa el CU y Re-CU"},
    alertamount: {text: "Por favor ingresa una cantidad"},
    alertcheckdata: {text: "Por favor revisa tus datos"},
    alertinvalidcu: {text: "Codigo unico (CU) de destino invalido, por favor verifiquelo."},
    alertgiveok: {text: "Saldo otorgado correctamente"},
    alertusername: {text: "Usuario o pasword invalido"},
    alertalreadyregister: {text: "Algunos de estos datos ya fueron registrados: Usuario, Movil o Correo"},
    alertregisterok: {text: "Registro realizado"},
    alertnozone: {text: "No estas cerca de ninguna zona"},
    alertalreadyparked: {text: "Este vehículo ya fue parqueado"},
    alertvehiclenoregistered: {text: "Este vehículo no esta registrado"},
    alertplacewrong: {text: "Placa invalida"},
    alertplacenot: {text: "Este lugar no esta disponible"},
    alertplaceclosed: {text: "Zona cerrada por Horario"},
    alertnotparking: {text: "No tienes parqueos activos"},
    alertnocars: {text: "No tienes vehículos registrados"},
    alertnobalance: {text: "No tienes saldo suficiente"},
    alertconfirmcardelete: {text: "Deseas borrar este vehículo?"},
    alertconfirmcardeletetitle: {text: "Borrar vehículo"},
    alertsorry: {text: "Lo sentimos, intenta nuevamente"},
    alertvehicleexist: {text: "Este vehículo ya existe"},
    alertvehicleadded: {text: "Vehículo agregado"},
    alertcheckpassword: {text: "Por favor revisa tu clave"},
    alertpasschangeok: {text: "Clave actualizada"},
    alertfiveleft: {text: "Te quedan 5 minutos para retirar tu vehículo"},
    alertoneleft: {text: "Te quedan 1 minuto para retirar tu vehículo"},
    alertvehiclefined: {text: "Tu vehículo recibira una multa por no retirarlo"},
    alertpaidparkingok: {text: "El pago ha sido reportado satisfactoriamente"},
    alertpaidparkingfail: {text: "Error, no hay suficiente saldo para completar la operación"},
    alertpaidparkingoffline: {text: "No se pudo establecer conexión con el punto de parqueo. Favor intente nuevamente"},
    alertpaidparkingtokenerror: {text: "Error en el token de pago"},
    alertpaidparkingerror: {text: "Ha ocurrido un error en la solicitud del servicio"},
    alertpaidparkingnullerror: {text: "Error debido a valores nulos en los campos"},
    alertpaidparkingstoreerror: {text: " Error guardando la operación de parqueo"},
    alertconfirmpaymentiderror: {text: "Código de pago invalido"},
    alerttimeouterror: {text: "Supero el tiempo de espera, por favor intente de nuevo"},
    alertnetworkerror: {text: "Ocurrido un error en la solicitud, revise su conexión e intente de nuevo"},
    alertnotfounderror: {text: "Su solicitud no se pudo completar, por favor intente de nuevo"},
    alertservererror: {text: "Hubo un error interno, por favor intente de nuevo"},
    alertgeneralerror: {text: "Hubo un error inesperado, por favor intente de nuevo"},
    alertnetworkconnection: {text: "La aplicación necesita internet para su correcto funcionamiento, revise su conexión"},
    alertgeonotsupported: {text: "La Geolocalización no está activa, por favor enciendalo."},
    alertconfirmpaymenttitle: {text: "Confirmar Pago"},
    alertconfirmgeneraltitle: {text: "CPS PARKING"},
    alertconfirmpayment: {text: "Información de parqueo:\nCod. Pago: %s\nCod. Parqueadero: %s" +
                          "\nInicio de parqueo: %s\nFin de parqueo: %s.\nValor: $%s" +
                          "\nMinutos de parqueo: %d"},
    alertroutefail: {text: "No pudimos obtener la ruta, por favor intenta de nuevo"},

    btnOk: {text: "Ok"},
    btnCancel: {text: "Cancelar"},
    backBtn: {clase: "backBtn", text: "Atras"},
    edittxt: {clase: "edittxt", text: "Editar"},
    savetxt: {clase: "savetxt", text: "Guardar"},
    sendtxt: {clase: "sendtxt", text: "Enviar"},

    /*LOGIN*/
    userName: {clase: "userName", text: "Usuario"},
    password: {clase: "password", text: "Clave"},
    login: {clase: "login", text: "Iniciar"},
    ingresar: {clase: "ingresar", text: "Iniciar"},
    register: {clase: "register", text: "Registro"},
    /*END LOGIN*/

    /*REGISTRO*/
    name: {clase: "name", text: "Nombres"},
    lastName: {clase: "lastName", text: "Apellidos"},
    mobile: {clase: "mobile", text: "Movil"},
    address: {clase: "address", text: "Direccion"},
    birthday: {clase: "birthday", text: "Fecha Nacimiento"},
    year: {clase: "year", text: "Año"},
    month: {clase: "month", text: "Mes"},
    day: {clase: "day", text: "Dia"},
    repassword: {clase: "repassword", text: "Reingresa Clave"},
    email: {clase: "email", text: "Correo"},
    country: {clase: "country", text: "Pais"},
    city: {clase: "city", text: "Ciudad"},
    cancel: {clase: "cancel", text: "Cancelar"},
    loginUs: {clase: "loginUs", text: "Usuario"},
    pageNameRegister: {clase: "pnreg", text: "Registro"},
    notificationMail: {clase: "notificationMail", text: "Desea recibir mas informacion sobre los productos y servicios de City Parking S.A.S y sus empresas asociadas y aliadas."},

    january:{clase: "january", text: "Enero"},
    february:{clase: "february", text: "Febrero"},
    march:{clase: "march", text: "Marzo"},
    april:{clase: "april", text: "Abril"},
    may:{clase: "may", text: "Mayo"},
    june:{clase: "june", text: "Junio"},
    july:{clase: "july", text: "Julio"},
    august:{clase: "august", text: "Agosto"},
    september:{clase: "september", text: "Septiembre"},
    october:{clase: "october", text: "Octubre"},
    november:{clase: "november", text: "Noviembre"},
    december:{clase: "december", text: "Diciembre"},

    alertlongfield: {text: "Compruebe la longitud de los campos"},
    alertcountryfield: {text: "Por favor revisa el País"},
    alertcityfield: {text: "Por favor revisa la Ciudad"},

    /*END REGISTER*/

    /* MENU PRINCIPAL*/
    zonestxt: {clase: "zonestxt", text: "Parqueaderos"},
    parkingtxt: {clase: "parkingtxt", text: "Parqueo"},
    vehiclestxt: {clase: "vehiclestxt", text: "Vehículos"},
    usertxt: {clase: "usertxt", text: "Cuenta de Usuario"},
    pnmain: {clase: "pnmain", text: "Menu"},
    settingstxt: {clase: "settingstxt", text: "Configuración"},
    languagetxt: {clase: "languagetxt", text: "Idioma"},
    pnsetlang: {clase: "pnsetlang", text: "Cambiar Idioma"},
    suggestionstxt: {clase: "suggestionstxt", text: "Sugerencias"},
    suggestionsend: {text: "Sugerencia enviada"},
    payparkingtxt: {clase: "payparkingtxt", text: "Pagar Parqueo"},


    /* ZONAS*/
    viewzonestxt: {clase: "viewzonestxt", text: "Ver Parqueaderos"},
    locatezonestxt: {clase: "locatezonestxt", text: "Localizar Parqueadero"},
    pnzones: {clase: "pnzones", text: "Parqueaderos"},
    viewtxt: {clase: "viewtxt", text: "Ver"},
    canceltxt: {clase: "canceltxt", text: "Cancelar"},
    pnviewzones: {clase: "pnviewzones", text: "Ver Parqueaderos"},
    pnzoneresult: {clase: "pnzoneresult", text: "Zonas"},
    viewdaystxt: {clase: "viewdaystxt", text: "Ver Horarios"},
    zoneoptionstxt: {clase: "zoneoptionstxt", text: "Opciones de Zona"},
    pnzonemap: {clase: "pnzonemap", text: "Mapa de Zona"},
    pnzonedays: {clase: "pnzonedays", text: "Horarios"},
    pnzoneinfo: {clase: "pnzoneinfo", text: "Info Zona"},
    pricetxt: {clase: "price", text: "Precio:"},
    placestxt: {clase: "placestxt", text: "Cupos Disponibles"},
    viewmaptxt: {clase: "viewmaptxt", text: "Ver Mapa"},
    viewpricestxt: {clase: "viewpricestxt", text: "Ver Precios"},
    maxtimetxt: {clase: "maxtimetxt", text: "Tiempo Maximo"},
    minutepricetxt: {clase: "minutepricetxt", text: "Precio Minuto"},
    currenttimetxt: {clase: "currenttimetxt", text: "Tiempo Actual"},
    zonetxt: {clase: "zonetxt", text: "Zona"},
    opentimetxt: {clase: "opentimetxt", text: "Apertura"},
    closetimetxt: {clase: "closetimetxt", text: "Cierre"},
    sundaytxt: {clase: "sundaytxt", text: "Domingo"},
    mondaytxt: {clase: "mondaytxt", text: "Lunes"},
    tuesdaytxt: {clase: "tuesdaytxt", text: "Martes"},
    wednesdaytxt: {clase: "wednesdaytxt", text: "Miercoles"},
    thursdaytxt: {clase: "thursdaytxt", text: "Jueves"},
    fridaytxt: {clase: "fridaytxt", text: "Viernes"},
    saturdaytxt: {clase: "saturdaytxt", text: "Sabado"},
    hometxt: {clase: "hometxt", text: "Volver"},
    locatezonesalerttxt: {clase: "locatezonesalerttxt", text: "Las rutas son sugerencias, verifica las normas de transito."},


    /* PARQUEO*/
    startparkingtxt: {clase: "startparkingtxt", text: "Parquear"},
    activeparkingtxt: {clase: "activeparkingtxt", text: "Parqueos Activos"},
    pnparking: {clase: "pnparking", text: "Parqueo"},
    zoneplacetxt: {clase: "zoneplacetxt", text: "Lugar/Espacio"},
    smsnotitxt: {clase: "smsnotitxt", text: "Notificacion SMS"},
    parktxt: {clase: "parktxt", text: "Parquear"},
    pnstartparking: {clase: "pnstartparking", text: "Parquear"},
    pnparkingdone: {clase: "pnparkingdone", text: "Parqueo Hecho"},
    pnparkingoptions: {clase: "pnparkingoptions", text: "Opciones de Parqueo"},
    pnendparking: {clase: "pnendparking", text: "Fin de Parqueo"},
    endparkingtxt: {clase: "endparkingtxt", text: "Fin Parqueo"},
    endtimetxt: {clase: "endtimetxt", text: "Hora Final"},
    totaltimetxt: {clase: "totaltimetxt", text: "Tiempo Total"},
    selectplatetxt: {clase: "selectplatetxt", text: "Selecciona Placa"},
    payparkingcodetxt:{clase:"payparkingcodetxt", text:"Código parqueo:"},
    paycodetxt:{clase:"paycodetxt", text:"Código de pago:"},
    paycodeinputtxt:{clase:"paycodeinputtxt", text:"Indicado por el punto de pago."},
    payparkingmsgtxt: {clase:"payparkingmsgtxt",text: "Solo para pago de servicio ocasional"},


    /* VEHICULOS*/
    addvechicletxt: {clase: "addvechicletxt", text: "Agregar Vehículo"},
    viewvehiclestxt: {clase: "viewvehiclestxt", text: "Ver Vehículos"},
    addtxt: {clase: "addtxt", text: "Agregar"},
    carplatetxt: {clase: "carplatetxt", text: "Placa:"},
    pnaddvehicles: {clase: "pnaddvehicles", text: "Agregar Vehículo"},
    vehicletxt: {clase:"vehicletxt", text: "Vehículo"},
    starttimetxt: {clase: "starttimetxt", text: "Hora Inicio"},
    placetxt: {clase: "placetxt", text: "Lugar"},
    balancetxt: {clase: "balancetxt", text: "Saldo"},


    /* USER ACCOUNT*/
    viewinfotxt: {clase: "viewinfotxt", text: "Ver Info"},
    changepasstxt: {clase: "changepasstxt", text: "Cambiar Clave"},
    pnuserinfo: {clase: "pnuserinfo", text: "Info de Usuario"},
    oldpasstxt: {clase: "oldpasstxt", text: "Clave Anterior"},
    newpasstxt: {clase: "newpasstxt", text: "Clave Nueva"},
    logouttxt: {clase: "logouttxt", text: "Cerrar Sesión"},

    /* BALANCE*/
    reloadbalancetxt: {clase: "reloadbalancetxt", text: "Recargar Saldo PIN"},
    reloadbalancezptxt: {clase: "reloadbalancezptxt", text: "Recargar Saldo"},
    viewbalancetxt: {clase: "viewbalancetxt", text: "Ver Saldo"},
    givebalancetxt: {clase: "givebalancetxt", text: "Transferir"},
    paypalreloadtxt: {clase: "paypalpaymenttxt", text: "Tarjeta de crédito"},
    pinnumbertxt: {clase: "pinnumbertxt", text: "Numero Pin"},
    payvaluetxt: {clase: "payvaluetxt", text: "Valor"},
    reloadtxt: {clase: "reloadtxt", text: "Recargar"},
    pnbalanceinfo: {clase: "pnbalanceinfo", text: "Saldo"},
    amounttxt: {clase: "amounttxt", text: "Cantidad"},
    givetxt: {clase: "givetxt", text: "Transferir"},
    pngivebalance: {clase: "pngivebalance", text: "Transferir"},
    cudestinationtxt: {clase: "cudestinationtxt", text: "CU Usuario destino"},
    recudestinationtxt: {clase: "recudestinationtxt", text: "Re-CU Usuario destino"},
    alertconfirmrbalnacetitle: {text: "Confirmar Recargar"},

    menuZones: {clase: "zones", text: "Zonas"},
    menuParking: {clase: "park", text: "Parqueo"},
    alertError: {text: "xx"},

    // SHARE
    shareMessage: {text: "La mejor aplicación de #Parqueo City Parking, que esperas? ¡DESCÁRGALA YA! #Android #Iphone #Wp"},
    shareTitle: {text: "CityParking - Cellular Parking Systems"},
    shareLink: {text: "http://goo.gl/W26B8H"}
};

var LANG = defaultLang == SPANISH ? LANG_ES : LANG_EN;

function initialize(){
    for(var key in LANG){
        if( LANG[key].clase != null ){
            var elem = $("."+LANG[key].clase);
            elem.text(LANG[key].text);
            var ell = elem.attr('placeholder');
            if( typeof ell != undefined ){
                elem.attr('placeholder', LANG[key].text);
            }

            ell = elem.attr('title');
            if( typeof ell != 'undefined' ){
                elem.attr('title', LANG[key].text);
            }
        }
    }
}


function initializeForTutorial(){
    for(var key in LANG){
        if( LANG[key].clase != null ){
            var elem = $("."+LANG[key].clase);
            var ell = elem.attr('title');
            if( typeof ell != 'undefined' ){
                elem.attr('title', LANG[key].text);
            }
        }
    }
}

function setLanguage(lang){
    defaultLang = lang;
    LANG = defaultLang == SPANISH ? LANG_ES : LANG_EN;
}

function cambiarIngles(){
    setLanguage(ENGLISH);
    initialize();
}

function cambiarEspaniol(){
    setLanguage(SPANISH);
    initialize();
}
