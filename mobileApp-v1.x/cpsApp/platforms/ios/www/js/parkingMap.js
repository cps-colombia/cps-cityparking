var map,
    lat,
    lon,
    count = 0,
    flightPath,
    infoBox,
    backpath,
    carImage = 'img/xcar.png',
    platform = rampgap.getPlatform(),
    protocol;

function initializeMap(img) {

    var myLatLng,
        zinfo = rampgap.getCurrentZoneInfo(),
        zoneArray = $.parseJSON(zinfo),
        mapdiv = document.getElementById("map_view_zones"),
        bounds = new google.maps.LatLngBounds(),
        platform = rampgap.getPlatform();

    var myOptions = {
        zoom : 17,
        mapTypeControl : false,
        navigationControl : true,
        navigationControlOptions : {
            style : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position : google.maps.ControlPosition.TOP_RIGHT
        },
        mapTypeId : google.maps.MapTypeId.ROADMAP
    };

    var infoOptions = {
        alignBottom: true,
        disableAutoPan: false,
        maxWidth: 280,
        pixelOffset: new google.maps.Size(-160, -16),
        zIndex: null,
        boxClass: "infoBox animated pulse",
        closeBoxMargin: "7px 7px 0 0",
        closeBoxURL: "img/close.gif",
        infoBoxClearance: new google.maps.Size(1, 1),
        pane: "floatPane"
    };

    mapdiv.style.width = '100%';
    mapdiv.style.height =  rampgap.getContentHeigth()+'px';

    map = new google.maps.Map(mapdiv, myOptions);
    infoBox = new InfoBox(infoOptions);


    for ( var i = 0; i < zoneArray.length; i++) {

        if (zoneArray[i].latitude == 0 && zoneArray[i].longitude == 0) {
            console.warn(zoneArray[i].idzone + "," + zoneArray[i].name + ","
                          + zoneArray[i].latitude + "," + zoneArray[i].longitude);
        } else {

            myLatLng = new google.maps.LatLng(zoneArray[i].latitude,
                                              zoneArray[i].longitude);

            bounds.extend(myLatLng);

            var marker = new google.maps.Marker({
                position : myLatLng,
                map : map,
                icon : img
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {

                    var idzone = zoneArray[i].idzone.replace(/[^0-9]/g, '');
                    var email = zoneArray[i].email+"@city-parking.com";
                    if (zoneArray[i].name.indexOf("@") != -1)
                        email = zoneArray[i].name;

                    var head = zoneArray[i].name.toUpperCase();
                    var info = "<p>Codigo Parqueo: "+idzone+"</p>"+
                            "<p>Cupos: "+ zoneArray[i].places+"</p>"+
                            "<p>Dirección: "+ zoneArray[i].address+"</p>"+
                            "<p> correo: " + email+"</p>"+
                            "<p>Horario: "+zoneArray[i].schedule+"</p>";
                    var contentString = '<div><div class="phoneytext" >'+
                            '<h1>'+head+'</h1></div>'+
                            '<div  class="infotext">'+info+ '</div>'+
                            '<div style="position:relative; text-align:center;">'+
                            '<button  class="ui-btn-up-c ui-btn-hover-c ui-btn-down-c ui-corner-all"  id="parkingbutton" type="button" onclick="javascript:popPayParking('+idzone+');">Pagar</button >';
                    var boxContent = '<div class="content-box">'+contentString+'</div>';

                    infoBox.setContent(boxContent);
                    //$('#map_view_zones').trigger('create');
                    infoBox.open(map, marker);
                };
            })(marker, i));

        }

    }

    if(platform.platformId != '3'){
        google.maps.event.addListener(map, "click", function() {
            infoBox.close();
        });
    }

    map.fitBounds(bounds);
}

function initializeMapPosition(img) {

    lat = rampgap.getCurrLatitude();
    lon = rampgap.getCurrLongitude();

    var myLatLng,
        zinfo = rampgap.getCurrentZoneInfo(),
        zoneArray = $.parseJSON(zinfo),
        mapdiv = document.getElementById("map_zones_localization"),
        bounds = new google.maps.LatLngBounds(),
        platform = rampgap.getPlatform();

    console.log('lat:' + lat + ' lon:' + lon);

    var myOptions = {
        zoom : 25,
        mapTypeControl : false,
        navigationControl : true,
        navigationControlOptions : {
            style : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position : google.maps.ControlPosition.TOP_RIGHT
        },
        mapTypeId : google.maps.MapTypeId.ROADMAP
    };

    var infoOptions = {
        alignBottom: true,
        disableAutoPan: false,
        maxWidth: 280,
        pixelOffset: new google.maps.Size(-160, -16),
        zIndex: null,
        boxClass: "infoBox animated pulse",
        closeBoxMargin: "7px 7px 0 0",
        closeBoxURL: "img/close.gif",
        infoBoxClearance: new google.maps.Size(1, 1),
        pane: "floatPane"
    };

    mapdiv.style.width = '100%';
    mapdiv.style.height = rampgap.getContentHeigth()-($('.locatezonesalerttxt').outerHeight( true ) || 0)+'px';

    map = new google.maps.Map(mapdiv, myOptions);
    infoBox = new InfoBox(infoOptions);

    var carmarker = new google.maps.Marker({
        position : new google.maps.LatLng(lat, lon),
        map : map,
        icon : carImage
    });

    console.log(JSON.stringify(zoneArray[0]));
    for ( var i = 0; i < zoneArray.length; i++) {

        if (zoneArray[i].latitude === 0 && zoneArray[i].longitude === 0) {
            console.warn(zoneArray[i].idzone + "," + zoneArray[i].name + "," +
                          zoneArray[i].latitude + "," + zoneArray[i].longitude);
        } else {

            myLatLng = new google.maps.LatLng(zoneArray[i].latitude,
                                              zoneArray[i].longitude);

            bounds.extend(myLatLng);

            var marker = new google.maps.Marker({
                position : myLatLng,
                map : map,
                icon : img
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {

                    var idzone = zoneArray[i].idzone.replace(/[^0-9]/g, '');
                    var email = zoneArray[i].name+
                            "@city-parking.com";
                    if (zoneArray[i].name.indexOf("@") != -1)
                        email = zoneArray[i].name;

                    var head = zoneArray[i].name.toUpperCase();
                    var info = "<p>Codigo Parqueo: "+idzone+"</p>"+
                            "<p>Cupos: "+zoneArray[i].places+"</p>"+
                            "<p>Dirección: "+zoneArray[i].address+"</p>"+
                            "<p> correo: "+email+"</p>"+
                            "<p>Horario: "+zoneArray[i].schedule+"</p>";
                    var contentString = '<div><div class="phoneytext" >'+
                            '<h1>'+head+'</h1></div>'+
                            '<div class="infotext">'+info+'</div>'+
                            '<div style="position:relative; text-align:center;">'+
                            '<button  class="ui-btn-up-c ui-btn-hover-c ui-btn-down-c ui-corner-all"  id="parkingbutton" type="button" onclick="javascript:popPayParking('+zoneArray[i].idzone+');">Pagar</button >'+
                            '<button  class="ui-btn-up-c ui-btn-hover-c ui-btn-down-c ui-corner-all"  id="closebutton" type="button" onclick="javascript:routeTo('+
                            marker.getPosition().lat()+','+ marker.getPosition().lng()+');">Ruta hasta aquí</button>'+
                            '</div></div>';

                    var boxContent = '<div class="content-box">'+contentString+'</div>';

                    infoBox.setContent(boxContent);
                    infoBox.open(map, marker);

                };
            })(marker, i));

        }

    }

    if(platform.platformId != '3'){
        google.maps.event.addListener(map, "click", function() {
            infoBox.close();
        });
    }

    map.fitBounds(bounds);

}

function initializeMapDriver(img, location){

    var geocoder,
        geoOptions,
        myLatLng,
        pointCount = 0,
        mapdiv = document.getElementById("map_canvas"),
        bounds = new google.maps.LatLngBounds(),
        platform = rampgap.getPlatform();

    var myOptions = {
        zoom: 15,
        panControl: false,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
        navigationControl : true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        navigationControlOptions : {
            style : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position : google.maps.ControlPosition.TOP_RIGHT
        },
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    };

    var infoOptions = {
        alignBottom: true,
        disableAutoPan: false,
        maxWidth: 280,
        pixelOffset: new google.maps.Size(-160, -16),
        zIndex: null,
        boxClass: "infoBox animated pulse",
        closeBoxMargin: "7px 7px 0 0",
        closeBoxURL: "img/close.gif",
        infoBoxClearance: new google.maps.Size(1, 1),
        pane: "floatPane"
    };

    mapdiv.style.width = '100%';
    mapdiv.style.height = rampgap.getContentHeigth()-$('.driverPickmeup_header').outerHeight( true )-$('.driverPickmeup_footer a').outerHeight( true )-20;

    map = new google.maps.Map(mapdiv, myOptions);
    infoBox = new InfoBox(infoOptions);
    geocoder = new google.maps.Geocoder();

    google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        //map.setCenter(center);
        myLatLng = new google.maps.LatLng(center.lat(), center.lng());
        bounds.extend(myLatLng);
    });

    $.each( location, function( key, value ) {

        if(location[key].place != ''){
            if(location[key].type == 'location'){
                geoOptions = {
                    location : location[key].place
                };
            } else {
                geoOptions = {
                    address : location[key].place
                };
            }

            geocoder.geocode(geoOptions, function(results, status) {
                console.log(results[0].geometry.location);

                if(status == google.maps.GeocoderStatus.OK) {
                    // var InfoPosition = {position : results[0].geometry.location};
                    // $.extend(InfoOptions, InfoPosition);
                    // infoBubble = new InfoBubble(InfoOptions);

                    // map.setCenter(results[0].geometry.location);

                    myLatLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

                    var carmarker = new google.maps.Marker({
                        position : myLatLng,
                        map : map,
                        icon : carImage
                    });

                    google.maps.event.addListener( carmarker, 'click', (function(marker, key) {
                        return function() {
                            var contentString = '<div><div class="ibox-header" > <h3>'+
                                    results[0].formatted_address+
                                    '</h3></div>'+
                                    '<div style="position:relative; text-align:center;">'+
                                    '</div></div>';
                            var boxContent = '<div class="content-box">'+contentString+'</div>';

                            infoBox.setContent(boxContent);
                            infoBox.open(map, marker);
                        };
                    })(carmarker, key));

                    var rawAddreess,
                        fullAddress = false;

                    $.each(results[0].address_components, function (i, address_component) {
                        if (address_component.types[0] == "route"){
                            fullAddress = true;
                            return false;
                        }
                    });

                    rawAddreess = results[0].formatted_address;

                    bounds.extend(myLatLng);

                    pointCount++;
                    console.log(pointCount);
                    if(fullAddress === false){
                        if(pointCount <= 1){
                            map.panTo(bounds.getCenter());
                        }
                        var oldAddress = results[1].formatted_address;
                        oldAddress = oldAddress.replace(/[ ]/g,"").split(",");
                        rawAddreess = [];
                        for(var i =0; i < oldAddress.length ; i++){
                            if(rawAddreess.indexOf(oldAddress[i]) == -1) rawAddreess.push(oldAddress[i]);
                        }
                        rawAddreess=rawAddreess.join(", ");
                    } else {
                        map.fitBounds(bounds);
                        var zoom = google.maps.event.addListenerOnce(map, "idle", function() {
                            if (map.getZoom() > 19) smoothZoom(map, 19, map.getZoom(), false);
                            google.maps.event.removeListener(zoom);
                        });

                    }

                    $(location[key].input).val(rawAddreess);

                    $.mobile.loading( 'hide' );

                }
            });

        }
    });

    if(platform.platformId != '3'){
        google.maps.event.addListener(map, "click", function() {
            infoBox.close();
        });
    }

}

function routeTo( latEnd, lonEnd )
{
    $.mobile.loading( 'show' );
    //routeToMapquest(latEnd,lonEnd);
    //routeToYours(latEnd,lonEnd); //Diable for iframe Access-Control-Allow-Origin in Wp 8.1
    platform = rampgap.getPlatform();
    protocol = platform.platformId == '3' ? 'ms-appx://' : platform.protocol;

    var imessage = {
        type: 'routemap',
        info: {lat: lat, latEnd: latEnd, lon: lon, lonEnd: lonEnd},
        data: '',
        platform: platform,
        action: 'routeToYoursGet'
    };

    parent.postMessage(imessage, protocol+document.location.host);
}

//YOURS api
function routeToYoursSuccess(response) {
    $.mobile.loading('hide');

    var coordinates = response.coordinates;
    var arrayPoints = [];
    for ( var i = 0; i < coordinates.length; i++) {
        var coordinate=coordinates[i];
        arrayPoints.push(new google.maps.LatLng(
            coordinate[1], coordinate[0]));

    }
    if (flightPath) {// If flightPath is already defined
        // (already a polyline)
        backpath.setPath(arrayPoints);

        flightPath.setPath(arrayPoints);
    } else {

        backpath = new google.maps.Polyline({
            path : arrayPoints,
            geodesic : true,
            strokeColor : '#000000',
            strokeOpacity : 1.0,
            strokeWeight : 4
        });
        backpath.setMap(null);
        backpath.setMap(map);
        flightPath = new google.maps.Polyline({
            path : arrayPoints,
            geodesic : true,
            strokeColor : '#FF0000',
            strokeOpacity : 1.0,
            strokeWeight : 2
        });

        flightPath.setMap(null);
        flightPath.setMap(map);

    }

    infoBox.close();
}

//Fallback route
function routeToFallbackSuccess(response){
    $.mobile.loading('hide');

    var shapePoints = response.route.shape.shapePoints;
    var arrayPoints = [];
    for ( var i = 0; i < shapePoints.length; i += 2) {

        arrayPoints.push(new google.maps.LatLng(
            shapePoints[i], shapePoints[i + 1]));

    }
    if (flightPath) {// If flightPath is already defined
        // (already a polyline)
        backpath.setPath(arrayPoints);
        flightPath.setPath(arrayPoints);
    } else {

        backpath = new google.maps.Polyline({
            path : arrayPoints,
            geodesic : true,
            strokeColor : '#000000',
            strokeOpacity : 1.0,
            strokeWeight : 4
        });
        backpath.setMap(null);
        backpath.setMap(map);
        flightPath = new google.maps.Polyline({
            path : arrayPoints,
            geodesic : true,
            strokeColor : '#FF0000',
            strokeOpacity : 1.0,
            strokeWeight : 2
        });

        flightPath.setMap(null);
        flightPath.setMap(map);

    }

    infoBox.close();
}

//Diable for iframe Access-Control-Allow-Origin in Wp 8.1 use postMessage

function routeToYours(latEnd, lonEnd) {
    var surl='http://www.yournavigation.org/api/1.0/gosmore.php?';
    var sdata='format=geojson&v=motorcar&fast=1&layer=mapnik&instructions=1&lang=es&flat='+
            lat+
            '&flon='+
            lon+
            '&tlat='+
            latEnd+
            '&tlon='+
            lonEnd;

    $.ajax({
        url:surl+sdata,
        dataType: "json",
        success : function(response) {

            // console.info("response info "+response);

            if (response == null) {
                $.mobile.loading('hide');
                mapAlert(LANG.alertalreadyregister.text);
                return false;
            } else {
                $.mobile.loading('hide');

                var coordinates = response.coordinates;
                var arrayPoints = new Array();
                for ( var i = 0; i < coordinates.length; i++) {
                    var coordinate=coordinates[i];
                    arrayPoints.push(new google.maps.LatLng(
                        coordinate[1], coordinate[0]));

                }
                if (flightPath) {// If flightPath is already defined
                    // (already a polyline)
                    backpath.setPath(arrayPoints);

                    flightPath.setPath(arrayPoints);
                } else {

                    backpath = new google.maps.Polyline({
                        path : arrayPoints,
                        geodesic : true,
                        strokeColor : '#000000',
                        strokeOpacity : 1.0,
                        strokeWeight : 4
                    });
                    backpath.setMap(null);
                    backpath.setMap(map);
                    flightPath = new google.maps.Polyline({
                        path : arrayPoints,
                        geodesic : true,
                        strokeColor : '#FF0000',
                        strokeOpacity : 1.0,
                        strokeWeight : 2
                    });

                    flightPath.setMap(null);
                    flightPath.setMap(map);

                }

                infoBox.close();
            }
        },
        error: function(response, textStatus, errorThrown ){
            mapAlert(errorThrown);
            console.log(textStatus + ' - ' + JSON.stringify(response) + ' - ' + errorThrown);
        }

    });

}
function routeToMapquest(latEnd, lonEnd) {
    $.ajax({
        //maquest
        url : 'http://open.mapquestapi.com/directions/v2/route',
        data : 'key=Fmjtd%7Cluur290tn0%2C70%3Do5-90zll4&outFormat=json&timeType=1&shapeFormat=raw&generalize=0&locale=es_MX&unit=k&from='+
            lat+
            ','+
            lon+
            '&to='+
            latEnd+
            ','+
            lonEnd,
        success : function(response) {

            // console.info("response info "+response);

            if (response == null) {
                $.mobile.loading('hide');
                mapAlert(LANG.alertalreadyregister.text);
                return false;
            } else {
                $.mobile.loading('hide');

                var shapePoints = response.route.shape.shapePoints;
                var arrayPoints = new Array();
                for ( var i = 0; i < shapePoints.length; i += 2) {

                    arrayPoints.push(new google.maps.LatLng(
                        shapePoints[i], shapePoints[i + 1]));

                }
                if (flightPath) {// If flightPath is already defined
                    // (already a polyline)
                    backpath.setPath(arrayPoints);

                    flightPath.setPath(arrayPoints);
                } else {

                    backpath = new google.maps.Polyline({
                        path : arrayPoints,
                        geodesic : true,
                        strokeColor : '#000000',
                        strokeOpacity : 1.0,
                        strokeWeight : 4
                    });
                    backpath.setMap(null);
                    backpath.setMap(map);
                    flightPath = new google.maps.Polyline({
                        path : arrayPoints,
                        geodesic : true,
                        strokeColor : '#FF0000',
                        strokeOpacity : 1.0,
                        strokeWeight : 2
                    });

                    flightPath.setMap(null);
                    flightPath.setMap(map);

                }

                infoBox.close();
            }
        },
        jsonpCallback : function(response) {

        }
    });

}

function setCenter(lt, ln) {
    var latlng = new google.maps.LatLng(lt, ln);
    map.panTo(latlng);
    marker.setPosition(latlng);
}
function smoothZoom (map, level, cnt, mode) {
    // If mode is zoom in
    var z;
    if(mode === true) {

        if (cnt >= level) {
            return;
        }
        else {
            z = google.maps.event.addListener(map, 'zoom_changed', function(event){
                google.maps.event.removeListener(z);
                smoothZoom(map, level, cnt + 1, true);
            });
            setTimeout(function(){map.setZoom(cnt);}, 80);
        }
    } else {
        if (cnt <= level) {
            return;
        }
        else {
            z = google.maps.event.addListener(map, 'zoom_changed', function(event) {
                google.maps.event.removeListener(z);
                smoothZoom(map, level, cnt - 1, false);
            });
            setTimeout(function(){map.setZoom(cnt);}, 80);
        }
    }
}

function mapAlert(text) {
    platform = rampgap.getPlatform();
    protocol = platform.platformId == '3' ? 'ms-appx://' : platform.protocol;
    var imessage = {
        type: 'notify',
        info: '',
        data: text,
        platform: platform,
        action: 'alert'
    };
    parent.postMessage(imessage, protocol+ document.location.host);
}

function popPayParking(idzone){
    platform = rampgap.getPlatform();
    protocol = platform.platformId == '3' ? 'ms-appx://' : platform.protocol;
    var imessage = {
        type: 'popup',
        info: idzone,
        data: '',
        platform: platform,
        action: 'popPayParking'
    };
    parent.postMessage(imessage, protocol+document.location.host);
}
