var coordss,
    znsInfo,
    clat,
    clong,
    debug=0;

var rampgap = new function() {

    this.zns="";
    this.alert =function ( msg) {
        $.mobile.loading('hide');

        if(debug===0){
            navigator.notification.alert(
                msg,
                alertDismissed,
                'CPS PARKING',
                'OK'
            );
        }else{
            alert(msg);
        }
    };

    this.notifySoundAndVibrate =function (){};

    this.confirmDialog=function ( text,  action){
        var r = confirm(text);
        if (r === true){
            eval(action);
        }
    };

    this.openURL=function ( url,  fn){ window.location.href=url;};
    this.reloadUrl =function (){};
    this.openLocation=function ( url){window.open(url,"_system","location=no");};

    this.setCurrLatitude =function (clat){  sessionStorage.setItem("clat",clat);};
    this.getCurrLatitude =function (){ return sessionStorage.getItem("clat",clat);};

    this.setCurrLongitude=function (clong){sessionStorage.setItem("clong",clong);};
    this.getCurrLongitude=function (){return sessionStorage.getItem("clong",clong);};

    this.setCurrentLatLong =function ( coords){coordss=coords;};

    this.getCurrMarkerImage =function (){return 'img/flag.png';};

    this.getCurrentZoneInfo =function (){
        // return this.zns;
        return sessionStorage.getItem("zones");
    };

    this.getPlatform = function(){
        var platform = JSON.parse(window.localStorage.getItem('platform'));
        return platform;
    };
    this.setPlatform = function(platform){
        window.localStorage.setItem('platform', JSON.stringify(platform));
    };

    this.showZoneMap =function (){};

    this.showZoneSMap=function ( zones){
        this.zns=zones;
        sessionStorage.setItem("zones",zones);
        $("body").pagecontainer("change", "#viewZonesMapResult", { transition: "slide"});
    };

    this.showZoneSMapLocation=function ( zones){
        this.zns=zones;
        sessionStorage.setItem("zones",zones);
        $("body").pagecontainer("change","#mapaZonesLocalization", { transition: "slide" });
    };
    this.viewZoneSMenu=function (){
        $("body").pagecontainer("change","#menu", { transition: "slide" });
    };


    //function showZoneSMap( Object  zones );

    this. showCarMap=function (){};


    this.verifyGPSIsActive=function (){afterGPSGetLocation(this.getCurrLatitude(),this.getCurrLongitude());};


    this.setUserSession=function( idu,  pass,  balance){};

    this.setZoneInfo=function(  zinfo ){
        // znsInfo=zinfo;

    };

    this.updateBalance =function( balance){};

    this.synchActiveParking=function(){};

    this.addActiveParking=function( id,  mv,  mm){};

    this.removeActiveParking=function( id){};

    this.destroySession=function(){};

    this.setItem=function(key,  value){
        // $.cookie(key,value, { expires: 365 });
        sessionStorage.setItem(key,value);
    };
    this.getItem=function(  key){
        return sessionStorage.getItem(key);
    };
    this.getHashLocation=function(){};
    this.getContentHeigth=function(){
        var docHeight = $(window).height(),
            cpsHeader = $('.ui-header').outerHeight( true ) || 0,
            cpsFooter = $('.ui-footer').outerHeight( true ) || 0,
            cpsContent = docHeight - (cpsHeader + cpsFooter);
        return cpsContent;
    };
    // Analytics
    this.initAnalytics = function(clientId){
        var analytics = navigator.analytics,
            trackingId = "UA-52539991-1";

        if(this.getPlatform().platformId == 3){
            console.log("Analytics WP Init");
            var config = new GoogleAnalytics.EasyTrackerConfig();
            config.trackingId = trackingId;
            config.appName = "CityParking";
            config.appVersion = this.getAppVersion();
            GoogleAnalytics.EasyTracker.current.config = config;
        } else {
            analytics.setTrackingId(
                trackingId,
                function(){ console.log("Analytics General Init"); },
                function(){ console.log("Error Analytics General Init"); }
            );
        }
    };
    this.ga = function(action, type, content, label){
        var platform = this.getPlatform().platformId,
            analytics = platform == "3" ? GoogleAnalytics.EasyTracker.getTracker() : navigator.analytics,
            Fields    = analytics.Fields,
            HitTypes  = analytics.HitTypes,
            LogLevel  = analytics.LogLevel,
            params    = {};

        if(action == 'send'){
            if(type == 'pageview'){
                switch(platform){
                case '3':
                    analytics.sendView(content.page);
                    break;
                default:
                    analytics.sendAppView(content.page);
                }
            } else if (type == 'event'){
                analytics.sendEvent(content, label, content, 0);
                switch(content){
                case 'social':
                case 'share':
                    switch(platform){
                    case '3':
                        analytics.sendSocial(label, content, label);
                        break;
                    default:
                        params[Fields.HIT_TYPE] = HitTypes.SOCIAL;
                        params[Fields.SOCIAL_ACTION] = content;
                        params[Fields.SOCIAL_NETWORK] = label;
                        params[Fields.SOCIAL_TARGET] = label;
                        analytics.send(params);
                    }
                    break;
                }
            } else if (type == 'exception'){
                analytics.sendException(content, label);
            }
        } else if (action == 'set'){
            if(type == 'uid'){
                switch(platform){
                case '3':
                    //wp not send Useid
                    break;
                default:
                    analytics.set('uid', content);
                    analytics.set('&in', content);
                }
            }
        }
    };
    this.makeFrame = function(platform, id, link){
        var hframe = this.getContentHeigth(),
            ilink = platform.platformId == '3' ? platform.protocol+'/www/'+link : link;

        //var frame = '<iframe id="'+id+'" src="'+ilink+'" style="width:100%; height:'+hframe+'px" seamless></iframe>';
        var iframe = document.createElement('iframe');
        iframe.id = id;
        iframe.src = ilink;
        iframe.style.width = '100%';
        iframe.style.height = hframe+'px';
        return iframe;
    };
    this.toStatic = function(data){
        var result = this.getPlatform().platformId == '3' ? toStaticHTML(data) : data;
        return result;
    };
    this.social = function (link) {
        console.log(link);
        var surl = link.split('/');
        var schema = surl[2] == 'facebook.com' ? 'fb' : 'twitter';
        console.log(this.getPlatform().platformId);
        if (this.getPlatform().platformId != '3') {
            CanOpen(schema+'://', function (isInstalled) {
                if (isInstalled) {
                    window.open(schema + '://' + surl[3], '_system', 'location=no');
                } else {
                    window.open(link, '_system', 'location=no');
                }
            });
        } else {
            console.log('wp: ' + schema + ':' + surl[3]);
            link = schema == 'fb' ? schema+':pages?id=398654126812091' : link;
            window.open(link, '_system', 'location=no');
        }
        // Send event to Analytics
        this.ga('send', 'event', 'social', surl[2]);
    };
    this.getAppVersion=function(){
        return "1.8.8";
    };
};
