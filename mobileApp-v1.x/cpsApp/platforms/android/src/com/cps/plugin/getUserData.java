package com.cps.plugin;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.util.ArrayList;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.provider.ContactsContract;
import android.content.ContentResolver;
import android.telephony.TelephonyManager;
import android.database.Cursor;
import android.content.Context;
//import android.content.Intent;
import android.content.ContentUris;
//import android.util.Log;
import android.net.Uri;

public class getUserData extends CordovaPlugin {
    private static final String TAG = "getUserData";

    /**
     * Constructor.
     */
    public getUserData() {
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    @Override
    public boolean execute(String action, JSONArray args,
                           CallbackContext callbackContext) throws JSONException {

        if (action.equals("getUserInfo")) {
            JSONObject r = new JSONObject();
            r.put("email", this.getEmailAccount());
            r.put("phone", this.getPhoneAccount());
            r.put("displayname", this.getProfileAccount().get(0));
            r.put("profilephone", this.getProfileAccount().get(1));
            callbackContext.success(r);

            return true;
        }
        return false;
    }

    //--------------------------------------------------------------------------
    // LOCAL METHODS
    //--------------------------------------------------------------------------

    /**
     * Get email
     *
     * @return
     */
    public String getEmailAccount(){
        AccountManager accountManager = AccountManager.get(cordova.getActivity());
        Account account = getAccount(accountManager);

        String aEmail = null;
        if (account == null) {
            aEmail = null;
        } else {
            aEmail = account.name;
        }
        return aEmail;
    }

    /**
     * Get phone owner.
     *
     * @return
     */
    public String getPhoneAccount(){
        String aPhone = null;
        try {
            TelephonyManager tm = (TelephonyManager) cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            aPhone = tm.getLine1Number();
        } catch (Exception e) {
            aPhone = e.getMessage();
        }
        return aPhone;
    }

    /**
     * Get user data from profile.
     * Android 4+ api 14
     * @return ArrayList<String>
     * [0] aName
     * [1] aPhone
     */
    public ArrayList<String> getProfileAccount(){
        ArrayList<String> profileData = new ArrayList<String>();
        String aId = null;
        String aName = null;
        String aPhone = null;
        String aHasPhone = "0";
        String osversion = android.os.Build.VERSION.RELEASE;
        @SuppressWarnings("deprecation")
            String sdkversion = android.os.Build.VERSION.SDK;

        if(Integer.parseInt(sdkversion) >= 14){
            try {
                String[] columnNames = new String[] {ContactsContract.Profile._ID, ContactsContract.Profile.DISPLAY_NAME, ContactsContract.Profile.HAS_PHONE_NUMBER, ContactsContract.Profile.PHOTO_ID};
                ContentResolver cr = cordova.getActivity().getContentResolver();

                Cursor c = cr.query(ContactsContract.Profile.CONTENT_URI, columnNames, null, null, null);
                boolean b = c.moveToFirst();
                if (c.getCount() == 1 && c.getPosition() == 0) {
                    aId = c.getString(0);
                    aName = c.getString(1);
                    aHasPhone = c.getString(2);
                }
                c.close();

                if (aId != null) {
                    if (aHasPhone.equals("1")) {
                        // get the phone number from profile
                        Uri pUri = ContentUris.withAppendedId(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, Long.parseLong(aId));
                        c = cr.query(pUri, null, null, null, null);

                        b = c.moveToFirst();
                        if (c.getCount() == 1 && c.getPosition() == 0) {
                            aPhone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        }
                        c.close();
                    }
                }
            } catch (Exception e) {
                aName = e.getMessage();
            }
        }

        profileData.add(aName);
        profileData.add(aPhone);

        return (profileData);
    }


    /**
     * Get accounts from acountManager.
     *
     * @return
     */
    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

}
