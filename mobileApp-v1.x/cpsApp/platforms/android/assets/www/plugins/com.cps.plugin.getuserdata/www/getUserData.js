cordova.define("com.cps.plugin.getuserdata.getuserdata", function(require, exports, module) { var utils = require('cordova/utils'),
    exec = require('cordova/exec'),
    cordova = require('cordova');

/**
 * This represents the user data and provides user info
 * @constructor
 */
function getUserData() {
    this.available = false;
    this.email = null;
    this.phone = null;
    this.displayname = null;
    this.profilephone = null;
}

/**
 * Init plugin and get data
 */
getUserData.prototype.init = function(){
    var me = this;

    me.getUserInfo(function(info) {
        me.available = true;
        me.email = info.email;
        me.phone = info.phone;
        me.displayname = info.displayname;
        me.profilephone = info.profilephone;
    },function(e) {
        me.available = false;
        console.log("[ERROR] Error initializing Cordova: " + e);
    });
};

/**
 * Get user info
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
getUserData.prototype.getUserInfo = function (successCallback, errorCallback) {
    exec(successCallback, errorCallback, "getUserData", "getUserInfo", []);
};

module.exports = new getUserData();

});
