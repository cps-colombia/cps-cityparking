var errorSend,
    imessage,
    mvzl;

function routeToYoursGet(lat, latEnd, lon, lonEnd) {
    var surl='http://www.yournavigation.org/api/1.0/gosmore.php?';
    var sdata='format=geojson&v=motorcar&fast=1&layer=mapnik&instructions=1&lang=es&flat='+
            lat+
            '&flon='+
            lon+
            '&tlat='+
            latEnd+
            '&tlon='+
            lonEnd;

    $.ajax({
        url:surl+sdata,
        dataType: "json",
        success : function(response) {

            // console.info("response info "+response);

            if (response === null) {
                //if error call fallback route
                routeToFallBack(lat, latEnd, lon, lonEnd);
            } else {
                mvzl = document.getElementById('frame_mvzl').contentWindow;
                imessage = {
                    type: 'routemap',
                    info: '',
                    data: response,
                    platform: rampgap.getPlatform(),
                    action: 'routeToYoursSuccess'
                };

                mvzl.postMessage(imessage, rampgap.getPlatform().protocol+document.location.host);
            }
        },
        error: function(response, textStatus, errorThrown ){
            //if error call fallback route
            routeToFallBack(lat, latEnd, lon, lonEnd);

            errorSend = 'routeToYours: '+textStatus+' - '+response.status;
            console.log(errorSend);
            //Send error route event register to Analytics
            rampgap.ga('send', 'event', 'error', errorSend);
            rampgap.ga('send', 'exception', errorSend, true);
        }

    });

}


function routeToFallBack(lat, latEnd, lon, lonEnd) {
    $.ajax({
        //maquest
        url : 'http://open.mapquestapi.com/directions/v2/route',
        data : 'key=Fmjtd%7Cluur290tn0%2C70%3Do5-90zll4&outFormat=json&timeType=1&shapeFormat=raw&generalize=0&locale=es_MX&unit=k&from='+
            lat+
            ','+
            lon+
            '&to='+
            latEnd+
            ','+
            lonEnd,
        success : function(response) {
            // console.info("response info "+response);
            if (response === null) {
                $.mobile.loading('hide');
                rampgap.alert(LANG.alertroutefail.text);
                return false;
            } else {
                mvzl = document.getElementById('frame_mvzl').contentWindow;
                imessage = {
                    type: 'routemap',
                    info: '',
                    data: response,
                    platform: rampgap.getPlatform(),
                    action: 'routeToFallbackSuccess'
                };

                mvzl.postMessage(imessage, rampgap.getPlatform().protocol+document.location.host);
            }
        },
        error: function(response, textStatus, errorThrown ){
            rampgap.alert(errorThrown);

            errorSend = 'routeFallback: '+textStatus+' - '+response.status;
            console.log(errorSend);
            //Send error route event register to Analytics
            rampgap.ga('send', 'event', 'error', errorSend);
            rampgap.ga('send', 'exception', errorSend, true);
        }
    });

}
