$( document ).one( "pagecreate", ".tutorial-page", function() {

    // Handler for navigating to the next page
    function navnext( next ) {
        $( ":mobile-pagecontainer" ).pagecontainer( "change", "#"+next, {
            transition: "slide"
        });
    }

    // Handler for navigating to the previous page
    function navprev( prev ) {
        $( ":mobile-pagecontainer" ).pagecontainer( "change", "#"+prev, {
            transition: "slide",
            reverse: true
        });
    }

    // Navigate to the next page on swipeleft
    $( document ).on( "swipeleft", ".ui-page.tutorial-page", function( event ) {

        var next = $( this ).jqmData( "next" );

        if ( next && ( event.target === $( this )[ 0 ] ) ) {
            navnext( next );
        }
    });

    // Navigate to the next page when the "next" button in the footer is clicked
    $( document ).on( "click", ".next", function() {
        var next = $( ".ui-page-active" ).jqmData( "next" );

        // Check if there is a next page
        if ( next ) {
            navnext( next );
        }
    });

    // The same for the navigating to the previous page
    $( document ).on( "swiperight", ".ui-page.tutorial-page", function( event ) {
        var prev = $( this ).jqmData( "prev" );

        if ( prev && ( event.target === $( this )[ 0 ] ) ) {
            navprev( prev );
        }
    });

    $( document ).on( "click", ".prev", function() {
        var prev = $( ".ui-page-active" ).jqmData( "prev" );

        if ( prev ) {
            navprev( prev );
        }
    });
});

$( document ).on( "pageshow", ".tutorial-page", function() {
    var thePage = $( this ),
        title = thePage.jqmData( "title" ),
        next = thePage.jqmData( "next" ),
        prev = thePage.jqmData( "prev" );

    $(".tutorial-page .ui-content").height($(window).height()-$(".tutorial-page .ui-header").outerHeight(true));

    if ( next ) {
        $( ":mobile-pagecontainer" ).pagecontainer( "load", "#"+next );
    }

    $( ".next.ui-state-disabled, .prev.ui-state-disabled" ).removeClass( "ui-state-disabled" );

    if ( !next ) {
        $( ".next" ).addClass( "ui-state-disabled" );
    }
    if ( !prev ) {
        $( ".prev" ).addClass( "ui-state-disabled" );
    }
});

//Close and enable default footer
$('.tutorial-exit').on('click', function(){
    $( ".footer-principal" ).show();
});
