﻿
/**
 * load login page
 */
function showLogin(){
	//$.mobile.changePage($("#login"));

}
/**
 * go to page back
 */
function historyBack(){
	window.history.back();
}
/**
 * put all initial things here after html document finish to load.
 */
 $(document).on('focus', 'input, textArea', function () {
        $('div[data-role="footer"]').hide();
    })

    $(document).on('blur', 'input, textarea', function() {
        setTimeout(function() {
            window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
            $('div[data-role="footer"]').show();
        }, 0);
    });

$(document).ready(function(e) {


	if (rampgap.getItem("lang")!= null){
		setLanguage(rampgap.getItem("lang"));
		$('#langSelect').val(rampgap.getItem("lang"));
	}

	checkAppVersion();

	$('#settingsMenu').click(function(){//ajustes v1.0.1
		$('#languageSelect').find('form').find('span:first').html(LANG.selectlangtext.text);
	});


	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;//ajustes v1.0.1
	$('#sendEmailReset').click(function(e) {
		var msg = $('#emailReset').val();
		if (!filter.test(msg)) {
			rampgap.alert(LANG.alerttypeemail.text);
			return false
		}

		$.mobile.loading( 'show' );
		$.ajax({
			data: 'op=23&mail='+msg,
			dataType: "html",
			success: function(response){
				if (response == null){
					$.mobile.loading( 'hide' );
					return false
				}else{
					if (response == -117){
						rampgap.alert(LANG.alertresetsend.text);
						$('#emailReset').val('');
	  					//$.mobile.changePage($("#login"));
						$( "body" ).pagecontainer( "change", "#login", { transition: "slide" });
						return false
					}
					if (response == -118){
						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertcheckemail.text);
						return false
					}
					$('#emailReset').val('');
					$.mobile.loading( 'hide' );
					return false
				}
			}

		});
    });

$('#ingresar').click(function(e) {
	   var returnCheckVersion=checkAppVersion();
	   if(returnCheckVersion==true){
		   login($('#userName').val(),$('#userPass').val());
		   $('#userName').val("");
		    $('#userPass').val("");

	   }
    });


	$('#btnRegister').click(function(e) {
		getCitiesRegister(1,0);
		$( "body" ).pagecontainer( "change", "#registerPage", { transition: "slide" });

    });

	$('#registerCountry').change(function(e) {
		getCitiesRegister($('#registerCountry').val());
    });

	$('#citiesSelect').change(function(e) {
		$('#viewZones').find('form').find('span:first').html(  $("#citiesSelect option:selected").text()  );
    });


	$('#register-submit').click(function(){
		if ($('#registerCountry').val() == 0 || $('#registerCity').val() == 0){
			rampgap.alert(LANG.alertcheckcountry.text);
			return false
		}
		var user = $("#user").val();
		var pass = $("#pass").val();
		var pass1 = $("#pass1").val();
		var name = $("#name").val();
		var last = $("#last").val();
		var address = $("#address").val();
		var email = $("#email").val();
		var bd = $("#birthDate").val(); // (yyyy-MM-dd)
		var country = $('#registerCountry').val();
		var msisdn = $("input#msisdn").val();
		var city = $("#registerCity").val();
		var emailAd= $("#checkbox-mini-0").is(':checked');
		if (user == '' || pass == ''|| name == ''|| last == ''|| address == ''|| email == ''|| msisdn == '' || bd==''){
			rampgap.alert(LANG.alertfields.text);
			return false
		}else{

			if (!filter.test(email)) {
				rampgap.alert(LANG.alerttypeemail.text);
				return false
			}
			if(pass == pass1){
			var dataString = 'user='+ user + '&pass=' + encrypt(pass) + '&name=' + name + '&last_name=' + last + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city +'&emailad='+emailAd;
			userRegister(dataString);
			}else{
				rampgap.alert(LANG.alertpasswords.text);
			}
		}
	});

	//ZONES SUB BUTTONS
	$('#btnViewZones').click(function() {
	  getCities(myData.country, "#viewZones");

	});
	$('#btnViewZonesMap').click(function() {
		  getCities(myData.country, "#viewZonesMap");
		});

	$('#btnViewCityZones').click(function() {
	  viewZones($('#viewZones #citiesSelect').val());
	});

	$('#btnViewCityZonesMap').click(function() {
		  viewZonesMap($('#viewZonesMap #citiesSelect').val());
		});
	$('#btnLocateZones').click(function() {
	  locateZones();
	});
	$('.viewZoneOptions').click(function() {
	  viewZoneOptions($(this).attr('rel'),$(this).attr('title'));
	});

	$('.viewZoneSMenu').click(function() {
	  viewZoneOptions($(this).attr('rel'),$(this).attr('title'));
	});



	//VEHICLES SUB BUTTONS
	$('#btnViewVehicles').click(function(){
		myVehicles();
	});
	$('#btnAddCar').click(function(){
		if ($('#newPlate').val() == ""){
			rampgap.alert(LANG.alertcarplate.text);
		}else{
			var newPlate =$('#newPlate').val().toUpperCase();
			addVehicle(newPlate);
		}

	});

	//ACCOUNT BUTTONS
	$('#btnViewAccount').click(function(){
		viewAccountInfo();
	});

	$('#btnSavePass').click(function(){
		if($('#oldPass').val()=="" || $('#newPass').val()== "" || $('#newPass').val()!=$('#rePass').val()){
			rampgap.alert(LANG.alertpassequals.text);
		}else{
			savePassword($('#oldPass').val(),$('#oldPass').val(),$('#newPass').val());
		}
	});
	$('#btnEditAccount').click(function(){
		$('#userInfoForm').find('input,select').each(function() {
			//$(this).removeAttr('disabled');
			//$(this).attr("disabled",false);
			// this.disabled = false;
			//console.info("item name="+ $(this).prop("id"));
			//$(this).removeAttr('disabled').removeClass('ui-state-disabled').parent().removeClass('ui-state-disabled');
			$(this).removeAttr('readonly');
		});

		$('#infoCU').attr('disabled','disabled').addClass('ui-state-disabled');
		$('#userInfoForm').trigger("refresh");
		$('#btnEditAccount,#btnBackAccount').hide();
		$('#btnSaveAccount,#btnCancelEdit').show();
	});
	$('#btnCancelEdit').click(function(){
		$('#userInfoForm').find('input,select').each(function() {
			//$(this).attr("disabled","disabled").addClass('ui-state-disabled').parent().addClass('ui-state-disabled');
			$(this).attr('readonly','readonly');
		});
		$('#userInfoForm').trigger("refresh");
		$('#btnSaveAccount,#btnCancelEdit').hide();
		$('#btnEditAccount,#btnBackAccount').show();
	});
	$('#btnSaveAccount').click(function(){
				 navigator.notification.confirm(LANG.alertconfirmmodifyinfo.text,
		 								saveUserUpdate,
		 								'CPS PARKING',
                                                               ['OK','Cancel']
                                                              );

		//rampgap.confirmDialog(LANG.alertconfirmmodifyinfo.text, "saveUserUpdate");
	});
	$('#changePass').click(function(){
		$('#userPass .campo').each(function(){
			$(this).val('');
		});
	});

	$('#btnChangeUser').click(function(){
 			navigator.notification.confirm(LANG.alertlogout.text,
		 								changeUser,
		 								'CPS PARKING',
                                                               ['OK','Cancel']
                                                              );
		//rampgap.confirmDialog(LANG.alertlogout.text, "javascript:changeUser()");
	});

	//BALANCE
	$('#viewBalance').click(function() {
		$.mobile.loading( 'show' );
		$.ajax({
			data: 'op=2&user='+myData.login+'&pass='+encrypt(myData.pass),
			success: function(response){
				if (response == null){
					$.mobile.loading( 'hide' );
					return false
				}else{
					if (response == null){
						$.mobile.loading( 'hide' );
						return false
					}
					$.mobile.loading( 'hide' );
					//NT - Actualizando saldo de usuario en SESSION
					rampgap.updateBalance(response.balance);
					var balance = '<p>'+LANG.balancetxt.text+': $ '+response.balance+'</p>';
					$('#infoBalanceContent').html(balance);
	  				//$.mobile.changePage($("#infoBalance"));
	  				$( "body" ).pagecontainer( "change", "#infoBalance", { transition: "slide" });

				}
			}
		});
	});

	$('#reloadBalancePin').click(function() {
		var pin = $('#pinNumber').val();
		pin = pin.replace(' ','');
		if (pin == "" ){
			rampgap.alert(LANG.alerrtypepin.text);
			return false
		}

		$.mobile.loading( 'show' );
		$.ajax({
			data: 'op=13&user='+myData.login+'&pass='+encrypt(myData.pass)+'&pin='+pin,
			success: function(response){
				if (response == null){
					$.mobile.loading( 'hide' );
					return false
				}else{
					if (response == ""){
						$.mobile.loading( 'hide' );
						return false
					}
					if (response.code == -101){
						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertinvalidpin.text);
						return false
					}
					$('#pinNumber').val('');
					//NT - Actualizando saldo de usuario en SESSION
					rampgap.updateBalance(response.obj.balance);
					$.mobile.loading( 'hide' );
					rampgap.alert(LANG.alertreloadok.text);
	  				//$.mobile.changePage($("#balance"));
	  				$( "body" ).pagecontainer( "change", "#balance", { transition: "slide" });

				}
			}
		});
	});

	//--end reload balance pin


	$('#rbalancezplink').click( function() {


		//rampgap.alert("response");

		$.ajax({
			data: 'op=27&user='+myData.login+'&pass='+encrypt(myData.pass),
			success: function(response){


				if (response == null){
					rampgap.alert("null");
					$.mobile.loading( 'hide' );
					return false;
				}else{
					$.mobile.loading( 'hide' );
					if (response == ""){

						return false;
					}
					//rampgap.alert("code"+response.code);
					if (response.code == -100){
						rampgap.confirmDialog(response.obj, "javascript:confirmPayZp()");
						$( "body" ).pagecontainer( "change", "#balance", { transition: "slide" });
						return false;
					}
					if (response.code == -101){
						alert(response.obj);
						$( "body" ).pagecontainer( "change", "#balance", { transition: "slide" });
						return false;
					}
					$( "body" ).pagecontainer( "change", "#reloadBalanceZP", { transition: "slide" });



				}
			},
			error: function(response, textStatus, errorThrown ){
				rampgap.alert(textStatus);
			}


		});

	});



	// reload balance ZP
	$('#reloadBalanceZPButton').click(function() {




		var value = $('#payValue').val();


		//rampgap.alert('value='+value);

		value = value.replace(' ','');
		if (isNaN($('#payValue').val())){
			rampgap.alert(LANG.alertypenumber.text);
			return false
		}

		$.mobile.loading( 'show' );
		$.ajax({
			data: 'op=26&user='+myData.login+'&pass='+encrypt(myData.pass)+'&value='+value,
			success: function(response){


				if (response == null){
					$.mobile.loading( 'hide' );
					return false
				}else{
					if (response == ""){
						$.mobile.loading( 'hide' );
						return false
					}
					$.mobile.loading( 'hide' );
					if (response.code == -101){

						rampgap.alert(LANG.alertinvalidpin.text);
						return false
					}
					$('#payValue').val('');
					//NT - Actualizando saldo de usuario en SESSION
					//rampgap.updateBalance(response.obj.balance);
					//alert(response.obj);
					myURL =encodeURI(response.obj);
					var ref = window.open(myURL, '_system');

					//rampgap.openURL(response.obj, "ZonaPagos");

				//	rampgap.alert(LANG.alertreloadok.text);
	  				$( "body" ).pagecontainer( "change", "#balance", { transition: "slide" });
				}
			},
			error: function(response, textStatus, errorThrown ){

				rampgap.alert(LANG.alertypenumber.text);
				$.mobile.loading( 'hide' );
				return false
			}


		});
	});
	//--end reload balance ZP

	/**
	 * Button to fire give balance event
	 */
	$('#btngiveBalance').click(function() {
		var repass = $('#givepassword').val();
		var cu = $('#cu').val();
		var recu = $('#recu').val();
		var amount = $('#amount').val();

		cu = cu.replace(' ','');
		recu = recu.replace(' ','');
		amount = amount.replace(' ','');
		amount = amount.replace('.','');
		amount = amount.replace(',','');

		if (cu == "" || recu == "" || cu != recu ){
			rampgap.alert(LANG.alertcheckcu.text);
			return false
		}else{
			if (amount == ""){
				rampgap.alert(LANG.alertamount.text);
				return false
			}
		}

		$.mobile.loading( 'show' );
		$.ajax({
			data: 'op=14&user='+myData.login+'&pass='+encrypt(myData.pass)+'&cu='+cu+'&amm='+amount+'&repass='+encrypt(repass),
			success: function(response){
				if (response == null){
					$.mobile.loading( 'hide' );
					return false
				}else{
					if (response == ""){
						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertcheckdata.text);
						return false
					}
					if (response.code == -2){
						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertcheckdata	.text);
						return false
					}

					//NT - Actualizando saldo de usuario en SESSION
					rampgap.updateBalance(response.obj.balance);

					$('#givepassword').val('');
					$('#cu').val('');
					$('#recu').val('');
					$('#amount').val('');
					$.mobile.loading( 'hide' );
					rampgap.alert(LANG.alertgiveok.text);
	  				//$.mobile.changePage($("#balance"));
	  				$( "body" ).pagecontainer( "change", "#balance", { transition: "slide" });

				}
			}
		});
	});

	/**
	 * pupulate city wheter country
	 */
	$('#infoCountry').change(function(e) {
		getCitiesRegister($('#infoCountry').val());
    });

	$('#carPLate').change(function(e) {
		$('#startParking').find('form').find('span:first').html($(this).val());
    });

	/**
	 * Button to change language
	 */
	$('#saveLang').click(function(e){

		e.preventDefault();
		$.mobile.loading( 'show' );
		//$.cookie('lang', $('#langSelect').val(), { expires: 365 });
		rampgap.setItem("lang", $('#langSelect').val());
		setLanguage($('#langSelect').val());
		$('#langSelect').val($('#langSelect').val());
		 initialize();
		 $.mobile.loading( 'hide' );
		//window.localStorage.setItem("lang", $('#langSelect').val());
		//$.mobile.changePage($("#menu"));
		$( "body" ).pagecontainer( "change", "#menu", { transition: "slide" });


		/*e.preventDefault();
		$.mobile.loading( 'show' );
		$.cookie('lang', $('#langSelect').val(), { expires: 365 });
		location.reload(true);
		rampgap.setItem("lang", $('#langSelect').val());
		rampgap.openLocation('file:///android_asset/www/index.html#settings');*/

	});
	/**
	 * on click sends suggestion
	 */
	$('#sendSug').click(function(e) {
		var msg = $('#suggestiontxt').val();
		if (msg == "" || msg == " " || msg == "  "){
			rampgap.alert(LANG.alertmsgempty.text);
			return false
		}

		$.mobile.loading( 'show' );
		$.ajax({
			data: 'op=19&user='+myData.login+'&pass='+encrypt(myData.pass)+'&sug='+msg,
			dataType: "html",
			success: function(response){
				if (response == null){
					$.mobile.loading( 'hide' );
					return false
				}else{
					if (response == ""){
						$.mobile.loading( 'hide' );
						return false
					}
					if (response == "[]"){
						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertmsgempty.text);
						return false
					}
					$('#suggestiontxt').val('');
					$.mobile.loading( 'hide' );
					rampgap.alert(LANG.suggestionsend.text);
	  				//$.mobile.changePage($("#menu"));
	  				$( "body" ).pagecontainer( "change", "#menu", { transition: "slide" });

				}
			}
		});
    });

    /*
	 * Envio de Pago a  City Parking
	 */
	$('#sendPayment').click(function(e) {
		var pcode = $('#parkingCode').val();
		var paycode = $('#payCode').val();
		if (pcode == "" || pcode == " " || pcode == "  "){
			rampgap.alert(LANG.alertparkingempty.text);
			return false;
		}
		if (paycode == "" || paycode == " " || paycode == "  "){
			rampgap.alert(LANG.alertpayempty.text);
			return false;
		}

	$.mobile.loading( 'show' );
		$.ajax({
			data: 'op=24&user='+myData.login+'&pass='+encrypt(myData.pass)+'&parkcode='+pcode+'&paycode='+paycode,
			dataType: "html",
			success: function(response){
				if (response == null){
					$.mobile.loading( 'hide' );
					rampgap.alert(LANG.alertconfirmpaymentiderror.text);
					return false;
				}else{
					if (response == ""){

						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertconfirmpaymentiderror.text);
						return false;
					}
					if (response == "[]"){
						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertmsgempty.text);
						return false;
					}
					if ($.parseJSON(response)=="-1"){
						$.mobile.loading( 'hide' );
						rampgap.alert(LANG.alertmsgempty.text);
						return false;
					}
					$('#parkingCode').val('');
					$('#payCode').val('');
					$.mobile.loading( 'hide' );
					var res=LANG.alertpaidparkingerror.text;
					response=$.parseJSON(response);

					// console.info("response "+response.info);
					if(response.value == 1){
						var msg= $.format(LANG.alertconfirmpayment.text,response.info.id,response.info.placeId,response.info.parkingInitDate,response.info.parkingEndDate, response.info.paymentValue,response.info.parkingTime);
						rampgap.confirmDialog(msg, "javascript:confirmParking("+JSON.stringify(response.info)+")");
						return;
						//res=LANG.alertpaidparkingok.text;
					}
					else if(parseInt(response.value)== 14){
						res=LANG.alertpaidparkingtokenerror.text;
					}
					else if(parseInt(response.value)==3){
						res=LANG.alertconfirmpaymentiderror.text;
					}
					else if (response.value=="-1"){

						res=LANG.alertpaycheck.text
					}
					rampgap.alert(res);

	  				$( "body" ).pagecontainer( "change", "#menu", { transition: "slide" });
				}
			}
		});
    });

	getCountries();
	//****************************************************************************************************************************************************** END READY************************************************
});

function confirmPayZp()
{

	$( "body" ).pagecontainer( "change", "#reloadBalanceZP", { transition: "slide" });


}

function confirmParking(infopay){

	//alert(infopay.placeId);
	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=25&user='+myData.login+'&pass='+encrypt(myData.pass)+'&id='+infopay.id+'&validationNumber='+infopay.validationNumber
		+'&operationResult='+infopay.operationResult+'&token='+infopay.token+'&value='+infopay.paymentValue+'&placeid='+infopay.placeId+
		'&initparking='+infopay.parkingInitDate+'&endparking='+infopay.parkingEndDate,
		dataType: "html",
		success: function(response){
			if (response == null){
						$.mobile.loading( 'hide' );
				return false
			}else{
				if (response == ""){
						$.mobile.loading( 'hide' );
					return false
				}
				if (response == "[]"){
						$.mobile.loading( 'hide' );
					rampgap.alert(LANG.alertmsgempty.text);
					return false
				}
				$('#parkingCode').val('');
				$('#payCode').val('');
						$.mobile.loading( 'hide' );
				var res=LANG.alertpaidparkingerror.text;
				if(response == "12")
					{
					res=LANG.alertpaidparkingok.text;
					}else if(parseInt(response)== 13)
						res=LANG.alertpaidparkingnullerror.text;
				else if(parseInt(response)== 14)
					res=LANG.alertpaidparkingtokenerror.text;
				else if(parseInt(response)== 9)
					res=LANG.alertpaidparkingfail.text;
					rampgap.alert(res);
	  				$( "body" ).pagecontainer( "change", "#menu", { transition: "slide" });
			}
		}
	});

}

function alertDismissed() {
    // do something
}

var logueado=0;

/**
 * current server url
 */
//Server Pruebas Canada
var url='http://96.45.176.18/CAN-CPS-MOB/MobileServer';
//Produccion
//var url = 'http://190.0.226.91/CAN-CPS-MOB/MobileServer';

//var url = 'http://localhost:8082/CAN-CPS-MOB/MobileServer';
//var url = 'http://steelxhome.no-ip.org:8082/CAN-CPS-MOB/MobileServer';

//var url = 'http://localhost:8082/CAN-CPS-MOB/MobileServer';
//var url = 'http://steelxhome.no-ip.org:8082/CAN-CPS-MOB/MobileServer';
/**
 * initial  and global ajax setup
 */
$.ajaxSetup({
	url: url,
	type: "POST",
	dataType: "json",
	timeout: 10000,
	error: function(response, textStatus, errorThrown ){
		$.mobile.loading( 'hide' );
		 var name= errorThrown.name;
		  if(textStatus==="timeout" || name==="NetworkError"  ||  textStatus=="error") {
			  rampgap.alert(LANG.alerttimeouterror.text );
	        }
		  //console.error(textStatus);
		  $.mobile.loading( 'hide' );
		return false;
	}
});

var myData = new Array();
var countryGet = 0;

// LOGIN -----------------------------------------------------------------
var session=0;

function changeUser(buttonIndex){

	if(buttonIndex==2){
		return false;
	}

	rampgap.setItem('login', null);
	rampgap.setItem('pass', null);
	session=0;
	//$.mobile.changePage($("#login"));
	$( "body" ).pagecontainer( "change", "#login", { transition: "slide" });
$("#login").val('');
	rampgap.destroySession();
}

function  checkAppVersion()
{
	var returnCheck=false;
	 $.ajax({
		data: 'op=28&deviceid=2&version='+rampgap.getAppVersion(),
		dataType: "html",
		async: false,
		success: function(response){
			// //console.warn('response:'+response);
			// if (response == 'false'){
				initialize();
				checkSession();
				returnCheck= true;
		// 	}else{


		// 		setTimeout(function () {navigator.notification.alert(
        //              LANG.alertnewupdate.text,  // message
        //              _storeApple,         // callback
        //              'CPS PARKING',            // title
        //              'Ok'                  // buttonName
        //              );},100);
		// //		 rampgap.confirmDialog(LANG.alertnewupdate.text, "javascript:_storeApple()");


		// 		returnCheck= false;
		// 	}
		}

	});
	return returnCheck;
}


/**
 * Connect to APP Store
 */
function _storeApple(){
    myURL =encodeURI('https://itunes.apple.com/us/app/cityparking/id868140927?ls=1');
    var ref = window.open(myURL, '_system');
}

/**
 * verify if current user has a valid session
 */
function checkSession(){

	if (rampgap.getItem("login")!= null&&rampgap.getItem("login")&& rampgap.getItem("pass")!= null
			&&rampgap.getItem("login")!= 'null'&& rampgap.getItem("pass")!= 'null'){
		session=1;
		logueado=1;

		login(rampgap.getItem("login"),rampgap.getItem("pass"));

	}else{

		$('#user-login').show();
	}
}
/**
 * Request for login from server. on success go to menu and keep a valid session
 * @param user valid username
 * @param pass valid password
 *
 */
function login(user,pass){

	/*
	SUCCES
	{"idsysUser":"3","address":"Kra 41G #47-12","balance":"$57.925","birthDate":"1984-11-19","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
	*/
	$.mobile.loading( 'show' );

	if (user == '' || pass==''){
		$.mobile.loading( 'hide' );
		rampgap.alert(LANG.alertusername.text);
		return false;
	}

	$.ajax({
		data: 'op=0&user='+user+'&pass='+encrypt(pass)+'&device=Iphone',
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				rampgap.alert(LANG.alertusername.text);
				return false;
			}else{
				//NT - Seteando values de usuario
				rampgap.setUserSession(response.login, pass, response.balance);
				//User Data
				myData['country'] = response.countryIdcountry;
				myData['city'] = response.city_idcity.idcity;
				myData['msisdn'] = response.favoriteMsisdn;
				myData['idu'] = response.idsysUser;
				myData['address'] = response.address;
				myData['balance'] = response.balance;
				myData['cu'] = response.cu;
				myData['email'] = response.email;
				myData['usertype'] = response.idsysUserType;
				myData['name'] = response.name;
				myData['lastname'] = response.lastName;
				myData['login'] = response.login;
				myData['pass'] = pass;
				logueado=1;
				$.mobile.loading( 'hide' );
				$( "body" ).pagecontainer( "change", "#menu", { transition: "pop" });

				if (session==0){
					rampgap.setItem("login",  response.login);
					rampgap.setItem("pass",  pass);

				}
			}

		},
		error: function(response, textStatus, errorThrown ){
			if(response.status==1001 || response.status==0)
			{
				rampgap.alert(LANG.alertusername.text);
			}
			$.mobile.loading( 'hide' );
		}
	});

}
/**
 * register a new user in the System,
 * @param dataString contains different parameter information
 */
function userRegister(dataString){

	/*DATOS PARA REGISTRO
	user
	pass
	name
	last_name
	address
	email
	bd (yyyy-MM-dd)
	country
	msisdn
	city
	*/

	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=1&'+dataString,

		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				rampgap.alert(LANG.alertalreadyregister.text);
				return false;
			}else{
				$.mobile.loading( 'hide' );
				rampgap.alert(LANG.alertregisterok.text);
				$( "body" ).pagecontainer( "change", "#login", { transition: "slide" });

			}
		},
		error: function(response, textStatus, errorThrown ){
			//if(response.status==1001 || response.status==0)
			//{
				rampgap.alert(LANG.alertalreadyregister.text);
			//}
			$.mobile.loading( 'hide' );
		}
	});
}
// ------------------------------------------------------------------------------ ZONES ------------------------------------------------------------------------------------------
/**
 * return cities by country code
 * @param countryCode, the country code.
 */
function getCities(countryCode, page){
	$.mobile.loading( 'show' );
	var cities = '';
	$.ajax({
		data: 'op=8&idc='+countryCode,
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{
				for (var i = 0; i < response.length; i++) {
					if (i == 0){
						cities += '<option value="'+response[i].idcity+'" selected="selected">'+response[i].name+'</option> ';
						var currentCity=response[i].name;//ajustes v1.0.1
					}else{
						cities += '<option value="'+response[i].idcity+'">'+response[i].name+'</option> ';
					}
				}
				$(page).find('form').find('span:first').html(currentCity);//ajustes v1.0.1
				$.mobile.loading( 'hide' );
				$(page+' #citiesSelect').html(cities);
				$( "body" ).pagecontainer( "change", page, { transition: "slide" });

			}
		}
	});
}

var zones = new Array();
/**
 *
 * @param cityCode
 */
function viewZones(cityCode){
	$.mobile.loading( 'show' );
	var results = '';
	$.ajax({
		data: 'op=7&user='+myData.login+'&pass='+encrypt(myData.pass)+'&idc='+myData.country+'&idcity='+cityCode,
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{

				for (var i = 0; i < response.length; i++) {
					zones[i]=new Array(response[i]);

					results += '<li class="ui-li-has-count ui-first-child" >';
					results += '	<a href="#" rel="'+i+'" title="'+response[i].name+'" class="ui-link-inherit viewZoneMap ui-btn ui-btn-icon-right ui-icon-carat-r" onclick="viewZoneMap('+i+');">';
					results += '		<h3 class="ui-li-heading">'+response[i].name+'</h3>';
					results += '		<p>'+LANG.placestxt.text+'</p>';
					if(response[i].places < 0){
						response[i].places=0;
					}
					results += '		<span class="ui-li-count ui-body-b">'+response[i].places+'</span>';
					results += '	</a>';
					results += '</li>';
				}


				$('#zoneResultList').html(results);
				$.mobile.loading( 'hide' );
				$( "body" ).pagecontainer( "change", "#viewZonesResult", { transition: "slide" });

				$('.viewZoneOptions').click(function() {
				  viewZoneOptions($(this).attr('rel'),$(this).attr('title'));
				});
			}
		}
	});
}

function viewZonesMap(cityCode){
	$.mobile.loading( 'show' );
	var results = '';
	$.ajax({
		data: 'op=7&user='+myData.login+'&pass='+encrypt(myData.pass)+'&idc='+myData.country+'&idcity='+cityCode,
		success: function(response){
			//rampgap.alert(response);
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{

				$.mobile.loading( 'hide' );
				//alert(JSON.stringify(response));
				showZonesMap(response);

			}
		},
		error: function(response ){
			rampgap.alert('error');
		}
	});
}
var ActualzoneArrayIndex;
//if(LANG==LANG_EN){
	var weekDays = new Array ('','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
//}else{
	//var weekDays = new Array ('','Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');

//}
function viewZoneOptions(zoneArrayIndex,zoneName){
	var zOptions = '';
	ActualzoneArrayIndex = zoneArrayIndex;
	zOptions += '<li data-theme="a" class="ui-first-child ui-last-child ui-li-has-thumb">';
    zOptions += '        	<a href="#" rel="'+zoneArrayIndex+'" class="ui-btn ui-btn-icon-right ui-icon-carat-r viewZoneMap" onclick="viewZoneMap('+zoneArrayIndex+');">';
    zOptions += '                <img src="img/ico-zones.png" class="ui-li-thumb">';
    zOptions += '                <h3 class="ui-li-heading">'+LANG.viewmaptxt.text+'</h3>';
   	zOptions += '         </a>';
   	zOptions += ' </li>';



	$('#zoneOptionsList').html(zOptions);
	//$.mobile.changePage($("#viewZonesOptions"));
	$( "body" ).pagecontainer( "change", "#viewZonesOptions", { transition: "slide" });

}

function viewZoneMap(zoneArrayIndex){
	showZonesMap(zones[zoneArrayIndex]);
}
function viewZonesMenu(){
	rampgap.backToApp();
}
function showZonesMap(zonemap ){
	rampgap.showZoneSMap(JSON.stringify(zonemap));
}

function showZonesMapLocation(zonemap ){
	rampgap.showZoneSMapLocation(JSON.stringify(zonemap));
}


function viewZoneMapCar(carlat,carlong){
	rampgap.setCurrentLatLong(carlat+","+carlong);
	rampgap.showCarMap();
}





function locateZones(){
	//rampgap.verifyGPSIsActive();
	getPosition();
}

function getPosition(){
	$.mobile.loading( 'show' );
    navigator.geolocation.getCurrentPosition(locateZonesCoords, onErrorZone);
}


var locateZonesCoords = function(position){
    clat=position.coords.latitude;
    clong=position.coords.longitude;
    rampgap.setCurrLatitude(clat);
     rampgap.setCurrLongitude(clong);
    afterGPSGetLocation(clat,clong);
}

function onErrorZone(error) {
    navigator.notification.alert(
    LANG.alertsorry.text,  // message
    alertDismissed,         // callback
    'CPS Notification',            // title
    'Ok'                  // buttonName
    );
    return false
}

function afterGPSGetLocation(clat, clong){
	var results = '';
	$.ajax({
		data: 'op=16&user='+myData.login+'&pass='+encrypt(myData.pass)+'&lat='+clat+'&lon='+clong,
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{
				if (response == ""){
					rampgap.alert (LANG.alertnozone.text);
					$.mobile.loading( 'hide' );
					return false
				}else{
					showZonesMapLocation(response);
				}
			}
		}
	});
}


// ------------------------------------------------------------------------------ /ZONES ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------ PARKING ------------------------------------------------------------------------------------------

function startParking(plate,num,idp,sms){
	/*
	SUCCES
	{"code":0,"description":"OK","obj":{"totalMins":0,"maxMins":30,"minuteValue":"0.03","msisdn":"3006658900","startTime":"2011-11-08 02:36:06","endTime":"2011-11-08 02:36:06","idPlace":"01","vehiclePlate":"BJJ118","total":"0.00","userBalance":"204.52","balance":"204.52","minValue":"0.03"}}

	PARQUEADO
	{"code":-113,"description":"Error"}

	VEHICULO NO EXISTE
	{"code":-121,"description":"Error"}

	ZONA NO EXISTE
	{"code":-106,"description":"Error"}

	*/


	plate = plate.replace(' ','');
	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=24&user='+myData.login+'&pass='+encrypt(myData.pass)+'&plate='+plate+'&num='+num+'&idc='+myData.country+'&idp='+idp+'&idu='+myData.idu+'&sms='+sms,
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{

				$.mobile.loading( 'hide' );
				if (response.code == 0){
					//NT - Agregando ActiveParking para procesamiento de Notificaciones
					rampgap.addActiveParking(
							response.obj.vehiclePlate,
							response.obj.minuteValue,
							response.obj.maxMins
					);
					var parkDoneResult = '<div cellspacing="0" cellpadding="0">';
					parkDoneResult += '<p class="title">'+ LANG.parkinginfotxt.text +'</p>';
					parkDoneResult += '<p><strong>'+LANG.vehicletxt.text+': </strong>'+response.obj.vehiclePlate+'</p>';
					parkDoneResult += '<p><strong>'+LANG.starttimetxt.text+': </strong>'+response.obj.startTime+'</p>';
					parkDoneResult += '<p><strong>'+LANG.placetxt.text+': </strong>'+response.obj.idPlace+'</p>';
					parkDoneResult += '<p><strong>'+LANG.balancetxt.text+': </strong>CRC '+response.obj.userBalance+'</p>';
					parkDoneResult += '<p><strong>'+LANG.minutepricetxt.text+': </strong>CRC '+response.obj.minuteValue+'</p>';
					parkDoneResult += '</div>';
//<a data-inline="true" data-role="button" href="#" data-theme="c" class="ui-btn ui-btn-inline ui-btn-corner-all ui-shadow ui-btn-up-c Home"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text hometxt">Home</span></span></a>
					//$('#parkingDoneInfo').html(parkDoneResult);
					//$.mobile.changePage($("#parkingDone"));
					//$( "body" ).pagecontainer( "change", "#parkingDone", { transition: "slide" });
					rampgap.confirmDialog(parkDoneResult,
							"javascript:confirmParking('"+plate+"','"+num+ "','"+ idp+"','"+sms+ "')");

					/*$('.Home').click(function(){
						//$.mobile.changePage($("#menu"));
						$( "body" ).pagecontainer( "change", "#menu", { transition: "slide" });

					});*/
				}
				if (response.code == -113){
					rampgap.alert (LANG.alertalreadyparked.text);
					return false
				}
				else if (response.code == -121){
					rampgap.alert (LANG.alertvehiclenoregistered.text);
					return false
				}
				else if (response.code == -120){
					rampgap.alert (LANG.alertplaceclosed.text);
					return false
				}
				else if (response.code == -106){
					rampgap.alert (LANG.alertplacewrong.text);
					return false
				}
				else if (response.code == -127){
					rampgap.alert (LANG.alertplacenotexist.text);
					return false
				}
				else if (response.code == -128){
					rampgap.alert (LANG.alertplacenot.text);
					return false
				}
				else if(response.code == -103){
					rampgap.alert(LANG.alertnobalance.text);
					return false;
				}
			}
		}
	});
}


// ------------------------------------------------------------------------------ VEHICLES ------------------------------------------------------------------------------------------

var myCars = new Array();

function myVehicles(){
	/*
	http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
op = 4
SUCCES
	[{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

	*/
	myCars = '';
	var resultCars = '';
	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=15&user='+myData.login+'&pass='+encrypt(myData.pass),
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{
				if (response == ""){
					rampgap.alert (LANG.alertnocars.text);
					$.mobile.loading( 'hide' );
					return false
				}else {
					for (var i = 0; i < response.length; i++) {
						myCars[i]= new Array(response[i].plate,response[i].dateReg,response[i].owner);
						//resultCars += '<li><span>'+response[i].plate+' - '+response[i].dateReg+'</span><a href="#" rel="'+response[i].plate+'" class="deleteItem">Delete</a></li>';

						resultCars += '<li data-theme="a" class="ui-first-child ui-last-child">';
						if( response[i].owner == '1' || response[i].owner == 1 ){
            			 resultCars += '	<a href="#" class="ui-btn ui-btn-icon-right ui-icon-delete deleteItem"  rel="'+response[i].plate+'" >';
						}else
						{
	            		 resultCars += '	<a href="#" class="ui-btn">';

						}
                    	resultCars += '		<img src="img/ico-car.png" class="ui-li-thumb">';
                    	resultCars += '		<h3 class="ui-li-heading">'+response[i].plate+'</h3>';
                    	//resultCars += '		<p class="ui-li-desc">'+response[i].dateReg+'</p>';
                		resultCars += '	</a>';

                		/*if( response[i].owner == '1' || response[i].owner == 1 ){
                			resultCars +='	<a class="deleteItem ui-li-link-alt ui-btn  ui-icon-delete" rel="'+response[i].plate+'" href="#" title="" data-theme="a"> '+
							'<span class="ui-btn-inner"></span></a></li>';
                		}*/

					}

					$('#vehiclesList').html(resultCars);
					$.mobile.loading( 'hide' );
					//$.mobile.changePage($("#viewVehicles"));
					$( "body" ).pagecontainer( "change", "#viewVehicles", { transition: "slide" });


					$('.deleteItem').click(function() {
					  deleteCar($(this).attr('rel'));
					});
				}
			}
		}
	});
}

function loadVehicles(){
	/*
	http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
op = 4
SUCCES
	[{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

	*/
	cars = '';
	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=15&user='+myData.login+'&pass='+encrypt(myData.pass),
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{
				if (response == ""){
					rampgap.alert (LANG.alertnocars.text);
					$.mobile.loading( 'hide' );
					return false
				}else {
					for (var i = 0; i < response.length; i++) {
						if (i == 0){
							cars += '<option value="0" selected="selected">---</option> ';
							cars += '<option value="'+response[i].plate+'">'+response[i].plate+'</option> ';
						}else{
							cars += '<option value="'+response[i].plate+'">'+response[i].plate+'</option> ';
						}
					}
					$('#startParking').find('form').find('span:first').html(LANG.selectplatetxt.text);
					$('#carPLate').html('');
					$('#carPLate').html(cars);
					$.mobile.loading( 'hide' );
					//$.mobile.changePage($("#startParking"));
					$( "body" ).pagecontainer( "change", "#startParking", { transition: "slide" });

				}
			}
		}
	});
}


function deleteCar(plate){
	/*
	op = 17
	idu, plate

	SUCCES
	{"code":-126,"description":"OK"}
	*/
	rampgap.confirmDialog(LANG.alertconfirmcardelete.text, "javascript:_deleteCar('"+plate+"')");

}

function _deleteCar(plate){
	$.ajax({
		data: 'op=17&user='+myData.login+'&pass='+encrypt(myData.pass)+'&plate='+plate,
		success: function(response){
			if (response == null){
				rampgap.alert (LANG.alertsorry.text);
			}else{
				myVehicles();
			}
		}
	});
}

function addVehicle(plate){
	/*
	op = 12
idu, pass, plate
IDU = LOGIN // OJO CORREGIR
	SUCCES
	{"code":-100,"description":"OK"}

	FAILED
	{"code":1,"description":"Failed"}

	*/
	plate = plate.replace(' ','');
	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=12&user='+myData.login+'&pass='+encrypt(myData.pass)+'&plate='+plate,
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{
				if (response.code == -102){
					rampgap.alert (LANG.alertvehicleexist.text);
					$.mobile.loading( 'hide' );
					return false
				}else {
					if (response.code != -100){
						rampgap.alert (LANG.alertsorry.text);
						$.mobile.loading( 'hide' );
					return false
					}else{
						rampgap.alert (LANG.alertvehicleadded.text);
						$.mobile.loading( 'hide' );
						myVehicles();
					}
				}
			}
		}
	});
}

// USER ACCOUNT -----------------------------------------------------------------


function viewAccountInfo(){
	/*
	OP=2 & USER

	SUCCES
	{"idsysUser":"3","address":"Kra 41G #47-12","balance":57.9,"birthDate":"Nov 19, 1984","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
	*/
	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=2&user='+myData.login+'&pass='+encrypt(myData.pass),
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{
				//User Data
				var d= new Date(response.birthDate);
				var year= d.getFullYear();
				var monthFormat= d.getMonth()+1;
				var month= monthFormat<10?"0"+monthFormat:monthFormat;
				var date= d.getDate()<10?"0"+d.getDate():d.getDate();

				var formatted= year+"-"+month+"-"+date;

				$('#infoCU').val(response.cu);
				$('#infoName').val(response.name);
				$('#infoLast').val(response.lastName);
				$('#infoAddress').val(response.address);
				$('#infoBirthDate').val(formatted);
				$('#infoMobile').val(response.favoriteMsisdn);
				$('#infoEmail').val(response.email);

				getCitiesRegister(response.city_idcity.idcountry,response.city_idcity.idcity);
				$('#infoCountry').val(response.city_idcity.idcountry).attr('selected', true);
				//rampgap.alert(LANG.alertregisterinfo.text);
				$('#infoCity').val(response.city_idcity.idcity).attr('selected', true);

				$.mobile.loading( 'hide' );
				$( "body" ).pagecontainer( "change", "#viewAccountInfo", { transition: "slide" });

			}
		}
	});
}

var countryGet = 0;

function getCountries(){
	/*
	SUCCFES
	[{"idcountry":"1","countryPrefix":"CAN","name":"Canada","latitude":"12365478","longitude":"12365445","lang":"en"}]
	*/
	if (countryGet == 0){
		$.mobile.loading( 'show' );
		var countries = '';
		$.ajax({
			data: 'op=3',
			success: function(response){
				if (response == null){
					$.mobile.loading( 'hide' );
				    return false
				}else{
					for (var i = 0; i < response.length; i++) {
						if (i == 0){
							countries += '<option value="0" selected="selected">---</option> ';
							countries += '<option value="'+response[i].idcountry+'">'+response[i].name+'</option> ';
						}else{
							countries += '<option value="'+response[i].idcountry+'">'+response[i].name+'</option> ';
						}

					}
					$('#infoCountry,#registerCountry').html(countries);
					$.mobile.loading( 'hide' );
				}
			}
		});
		countryGet = 1;
	}
}

function getCitiesRegister(countryCode,cityCode){
	if (countryCode==0)
		return false

	$.mobile.loading( 'show' );
	var cities = '';
	$.ajax({
		data: 'op=8&idc='+countryCode,
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				return false
			}else{
				for (var i = 0; i < response.length; i++) {
					if (i == 0){
						cities += '<option value="0" selected="selected">---</option> ';
						cities += '<option value="'+response[i].idcity+'">'+response[i].name+'</option> ';
					}else{
						cities += '<option value="'+response[i].idcity+'">'+response[i].name+'</option> ';
					}
				}
				$.mobile.loading( 'hide' );
				$('#registerCity,#infoCity').html(cities);
				if (cityCode != 0){
					$('#infoCity').val(cityCode);
				}
			}
		}
	});
}

function saveUserUpdate(buttonIndex){

	if(buttonIndex==2){
		return false;
	}
	/*
	OP=10
	name = request.getParameter("name");
    last_name = request.getParameter("last_name");
    address = request.getParameter("address");
    email = request.getParameter("email");
    birthdate = request.getParameter("bd");
    country = request.getParameter("country");
    msisdn = request.getParameter("msisdn");
    city = request.getParameter("city");

	SUCCES
	*/
	$.mobile.loading( 'show' );

	var address = $("#infoAddress").val();
	var email = $("#infoEmail").val();
	var bd = $("#infoBirthDate").val();// (yyyy-MM-dd)
	var country = $("#infoCountry").val();
	myData.country=country;//ajustes v1.0.1
	var msisdn = $("#infoMobile").val();
	var city = $("#infoCity").val();
	var name = $("#infoName").val();
	var last_name = $("#infoLast").val();

	if (name == ''|| last_name == ''|| address == ''|| email == ''|| msisdn == '' || bd==''){
		$.mobile.loading( 'hide' );
		rampgap.alert(LANG.alertfields.text);
		return false
	}
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;//ajustes v1.0.1

	if (!filter.test(email)) {
		$.mobile.loading( 'hide' );
		rampgap.alert(LANG.alerttypeemail.text);
		return false
	}

	var dataString = '&name=' + name + '&last_name=' + last_name + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city;

    console.log(url+'?op=10&user='+myData.login+'&pass='+encrypt(myData.pass)+dataString);

	$.ajax({
		data: 'op=10&user='+myData.login+'&pass='+encrypt(myData.pass)+dataString,
		success: function(response){
			if (response == null){
				$.mobile.loading( 'hide' );
				rampgap.alert(LANG.alertcheckdata.text);
				return false
			}else{
				$.mobile.loading( 'hide' );
				rampgap.alert(LANG.alertregisterok.text);
				$('#btnCancelEdit').trigger('click');
				viewAccountInfo();
			}
		}
	});
}


function savePassword(pass,oldPass,newPass){
	$.mobile.loading( 'show' );
	$.ajax({
		data: 'op=11&user='+myData.login+'&pass='+encrypt(pass)+'&oldpass='+encrypt(oldPass)+'&npass='+encrypt(newPass),
		success: function(response){
			if (response == null){
				rampgap.alert (LANG.alertcheckpassword.text);
				$.mobile.loading( 'hide' );
				return false
			}else{
				if (response.code = -117){
					myData['pass'] = newPass;
					//$.cookie('pass', newPass, { expires: 7 });
					rampgap.setItem("pass", newPass);

					rampgap.alert (LANG.alertpasschangeok.text);
					$.mobile.loading( 'hide' );
					//$.mobile.changePage($("#userAccount"));
					$( "body" ).pagecontainer( "change", "#userAccount", { transition: "slide" });

				}
			}
		}
	});
}


//---------------------------------------------------------------------------//
