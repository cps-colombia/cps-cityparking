﻿// legacy.js
//
// provide support for google maps across all platforms,
// because IE not accept external script in the first instance
// and force to use iframe with your own protocol and your way.

/**
 * Message receive
 * @param type: type of action
 * @param info: data to be update
 * @param data: user data
 * @param platfom: info platform
 * @param action: action to be launch
 */

(function () {

    if (window.addEventListener) {
        // For standards-compliant web browsers
        window.addEventListener("message", receiveMessage, false);
    }
    else {
        window.attachEvent("onmessage", receiveMessage);
    }

    //receive messages
    function receiveMessage(e) {
        var origin = e.origin.split('://');

        if (origin[1] == document.location.host) {
            if(e.data.type == 'loadmap'){
                rampgap.setItem("zones", e.data.info.zones);
                var myData = e.data.data;
                var _getPlatform = e.data.platform;
                //Fix localStorage in iframe IE
                rampgap.setPlatform(e.data.platform);

                // eval can be harmful
                switch(e.data.action){
                case 'initializeMap':
                    $('#iframe-maps .maps').remove();
                    $('#iframe-maps .ui-page').append(rampgap.toStatic('<div id="map_view_zones" class="maps"></div>'));
                    initializeMap("img/flag.png");
                    break;
                case 'initializeMapPosition':
                    $('#iframe-maps .maps').remove();
                    $('#iframe-maps .ui-page').append(rampgap.toStatic('<div id="map_zones_localization" class="maps"></div>'));
                    rampgap.setCurrLatitude(e.data.info.lat);
                    rampgap.setCurrLongitude(e.data.info.log);
                    rampgap.setFlightPath("false");
                    initializeMapPosition('img/flag.png');
                }
            } else if (e.data.type == 'routemap') {
                switch(e.data.action){
                case 'routeToYoursGet':
                    routeToYoursGet(e.data.info.lat,
                                    e.data.info.latEnd,
                                    e.data.info.lon,
                                    e.data.info.lonEnd);
                    break;
                case 'routeToYoursSuccess':
                    routeToYoursSuccess(e.data.data);
                    break;
                case 'routeToFallbackSuccess':
                    routeToFallbackSuccess(e.data.data);
                    break;
                }

            } else if (e.data.type == 'popup'){
                switch(e.data.action){
                case 'popPayParking':
                    $("#popPayParking").popup({
                        afteropen: function( event, ui ) {
                            $('#parkingCodePop').val('');
                            $('#payCodePop').val('');
                            $("#parkingCodePop").val(e.data.info);
                        }
                    });
                    $("#popPayParking").popup( "open" );
                    break;
                case 'popPrice':
                    getPriceZone(e.data.info);
                    break;
                }
            } else if (e.data.type == 'notify') {
                switch (e.data.action) {
                case 'alert':
                    rampgap.alert(e.data.data);
                    break;
                }
            }
        }
    }

})();
