#!/usr/bin/env node

// This hook replaces package parameters by platform
// get params from config_hook.json

(function(){
    "use strict";

    /**
     * Dependencies
     */

    var fs = require('fs'),
        path = require('path');

    // Vars control
    var displayName,
        idPackage = "com.cps.cpsApp",
        filestoreplace = [],
        changeDebug = false,
        debugState = false,
        rootdir = process.argv[2],
        platform = process.env.CORDOVA_PLATFORMS.toLowerCase(),
        cmd = process.env.CORDOVA_CMDLINE.split(/\s+/);

    // reads the configuration file and starts
    if (rootdir) {
        var ourconfigfile = path.join(rootdir, "hooks", "config_hook.json");
        var configobj = JSON.parse(fs.readFileSync(ourconfigfile, 'utf8'));

        // Platform parameters
        if (platform == "android") {
            changeDebug = true;
            filestoreplace = [
                "platforms/android/AndroidManifest.xml"
            ];
        } else if (platform == "ios"){
            filestoreplace = [
                "platforms/ios/cpsApp/cpsApp-Info.plist"
            ];
        } else if (platform == "windows"){
            idPackage = null;
            displayName = '"cpsApp"';
            // filestoreplace = [
            //     "platforms/windows/package.phone.appxmanifest",
            //     "platforms/windows/package.windows.appxmanifest"
            // ];
        }

        // Replace for file
        filestoreplace.forEach(function(val, index, array) {
            var fullfilename = path.join(rootdir, val);
            if (fs.existsSync(fullfilename)) {

                if(idPackage !== null){
                    console.log("Apply Hook ID for: "+platform);
                    replace_in_file(fullfilename,
                                    idPackage,
                                    configobj[platform].package_id);
                }

                if(displayName !== undefined){
                    console.log("Change DisplayName for: "+platform);
                    replace_in_file(fullfilename,
                                    displayName,
                                    configobj.all.display_name);
                }

                // Replace debuggable
                if(changeDebug){
                    debugState = cmd.indexOf("--release") <= -1;
                    console.log("Change Debug "+debugState+" for: "+platform);
                    replace_in_file(fullfilename,
                                    "debuggable=\"(:?.*?)\"",
                                    "debuggable=\""+debugState+"\"");
                }

            } else {
                console.log("missing: "+fullfilename);
            }
        });
    }

    // Private
    /**
     * @param filename relative path to file will be changed
     * @param to_replace string or regexp to replace
     * @param replace_with string that will be replaced
     */
    function replace_in_file(filename, to_replace, replace_with) {
        var data = fs.readFileSync(filename, 'utf8');

        var result = data.replace(new RegExp(to_replace, "g"), replace_with);

        fs.writeFileSync(filename, result, 'utf8');
    }

})();
