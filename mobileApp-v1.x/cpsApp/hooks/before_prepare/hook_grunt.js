#!/usr/bin/env node

// hook_grunt
// This hook call grunt for compilign css, js and other optimizations
(function(){
    "use strict";

    /**
     * Dependencies
     */
    var os = require("os"),
        fs = require("fs"),
        path = require("path"),
        shell = require('shelljs');

    /**
     * Global vars
     */
    var rootdir = process.argv[2],
        grunt = path.resolve(rootdir, "../node_modules/grunt-cli/bin/grunt"),
        platform = process.env.CORDOVA_PLATFORMS;

    //Checkks if grunt-cli installed
    if (!shell.which(grunt)) {
        shell.echo('Sorry, please install grunt-cli');
        shell.exit(1);
    }

    //Run grunt.js
    shell.exec(grunt+" build", {async:true}, function(code, output) {
        if (code !== 0) {
            shell.echo('Error: Grunt CPS failed');
            shell.echo('Exit code:', code);
            shell.exit(1);
        }
        shell.echo('Grunt CPS Finish');
    });

})();
