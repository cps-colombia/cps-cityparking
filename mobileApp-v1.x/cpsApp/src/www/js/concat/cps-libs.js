/*global $, LANG, WinJS */

///// GLOBAL //////
/**
 * current server url
 */
//Server Pruebas Canada
var url = "http://96.45.176.18/mobilecity/MobileServer";
var wsdl = "http://190.0.226.97:8888/wservicecitytesting/CPS/puente.php";
var wsdlToken = "700f3d9e7d3a3bf3a123e8ee46391049";

//Produccion
//var url = "http://190.0.226.91/CAN-CPS-MOB/MobileServer";
//var wsdl = "http://190.0.226.97:8888/wservicecity/CPS/puente.php";
//var wsdlToken = "43697479252652652d5669727475616c";

//Debug
//var url = 'http://localhost:8082/CAN-CPS-MOB/MobileServer';
//var url = 'http://steelxhome.no-ip.org:8082/CAN-CPS-MOB/MobileServer';

/**
 * Control variables
 */
var myData = [],
    myCars = [],
    zones = [],
    debug = 0,
    logueado = 0,
    session = 0,
    countryGet,
    countryData,
    _adminId = "11",
    _isAdmin = false,
    _hidesplash = false,
    _deviceReady = false,
    _initAnalytics = false,
    _mapsLoaded = false,
    _enableAccuracy = false,
    _networkOnline = true,
    _currentPage,
    _networkInfo,
    _getPlatform;


/**
 * initial and global ajax setup
 */
$.ajaxSetup({
    url: url,
    type: "POST",
    dataType: "json",
    timeout: 30000,
    error: function(response, textStatus, errorThrown) {
        $("a.ui-btn[href=#], input[type=submit]").removeClass("ui-disabled");

        var errorSend = response.status;
        if (textStatus == "timeout" || textStatus == 'abort'){
            generalAlert(LANG.alerttimeouterror.text);
            errorSend = textStatus;
        } else if (response.status === 0 || errorThrown == "NetworkError") {
            generalAlert(LANG.alertnetworkerror.text);
        } else if (response.status == 404) {
            generalAlert(LANG.alertnetworkerror.text);
            //generalAlert(LANG.alertnotfounderror.text);
        } else if (response.status == 500) {
            generalAlert(LANG.alertservererror.text);
        } else {
            errorSend = textStatus;
            generalAlert(LANG.alertgeneralerror.text);
        }
        if(_initAnalytics === true){
            //Send error network event register to Analytics
            rampgap.ga('send', 'event', 'error', errorSend);
            rampgap.ga('send', 'exception', errorSend, true);
        }
        _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;
        if(_currentPage == "preloadPage"){
            $(":mobile-pagecontainer").pagecontainer("change", "#login", {
                transition: "fade"
            });
        }
        $.mobile.loading('hide');
        return false;
    }
});
//// End GLOBAL /////

/**
 * Wait for device API libraries to load
 */

function onBodyLoad() {
    console.log("Load body");
    if(debug === 0){
        document.addEventListener("deviceready", onDeviceReady, false);
        document.addEventListener("offline", onOffline, false);
        document.addEventListener("online", onOnline, false);
    }
}

/**
 * Check internet connectionx
 */
function onOffline() {
    console.log("Offline: "+navigator.connection.type);
    navigator.notification.alert(
        LANG.alertnetworkconnection.text, //'No network connection',
        alertDismissed,
        LANG.alerttitle.text,
        LANG.btnOk.text
    );
    $.mobile.loading('hide');
    _networkOnline = false;
}

function onOnline() {
    console.log("Online: "+navigator.connection.type);
    _networkOnline = true;
    if(_mapsLoaded === false && _deviceReady === true){
        initMapsIframe();
    }
}
/**
 * Device is ready to rock!!
 */
function onDeviceReady() {

    var getPlatform = function() {
        var ua = window.navigator.userAgent,
            cpsDevice = window.device,
            deviceVersion,
            platform,
            platformId,
            protocol = 'file://',
            platformVersion = 0,
            devicePlatform = (cpsDevice ? cpsDevice.platform : "android").toLowerCase().trim().split(/(\s+)/)[0];

        if (devicePlatform == 'android' ) {
            platformId = "1";
        } else if (devicePlatform == 'ios') {
            platformId = "2";
        } else if (devicePlatform == 'win32nt' || devicePlatform == 'wince' || devicePlatform == 'windows') {
            devicePlatform = "windows";
            platformId = "3";
            protocol = 'ms-appx-web://';
        } else {
            platformId = "1";
            devicePlatform = 'android';
        }


        if (cpsDevice) {
            deviceVersion = cpsDevice.version.split('.');
            deviceVersion = parseFloat(deviceVersion[0] + '.' + (deviceVersion.length > 1 ? deviceVersion[1] : 0));
            if (!isNaN(deviceVersion)) {
                platformVersion = deviceVersion;
            }
        } else {
            // fallback to user-agent checking
            var pName = devicePlatform,
                versionMatch = {
                    'android': /Android (\d+).(\d+)?/,
                    'ios': /OS (\d+)_(\d+)?/,
                    'windows': /Windows Phone (\d+).(\d+)?/
                };
            if (versionMatch[pName]) {
                deviceVersion = ua.match(versionMatch[pName]);
                if (deviceVersion &&  deviceVersion.length > 2) {
                    platformVersion = parseFloat(deviceVersion[1] + '.' + deviceVersion[2]);
                }
            }
        }


        platform = {
            version: platformVersion,
            platformId: platformId,
            platform: devicePlatform,
            protocol: protocol
        };
        window.localStorage.setItem('platform', JSON.stringify(platform));
        // Add to body
        $("body").addClass("platform-"+devicePlatform);
        if (platformVersion) {
            var v = platformVersion.toString();
            if (v.indexOf('.') > 0) {
                v = v.replace('.', '_');
            } else {
                v += '_0';
            }
            $("body").addClass(devicePlatform+v.split("_")[0]+" "+devicePlatform+v);
        }
        console.log("Get platform");
        return platform;
    }();

    _getPlatform = rampgap.getPlatform() || getPlatform();
    _deviceReady = true;

    //start basic app components
    checkAppVersion();
    getCountries();

    //Init Analytics  @params clientId
    if(_initAnalytics === false){
        rampgap.initAnalytics(device.uuid);
        _initAnalytics = true;
    }

    //Init the plugin getUserData when cordova ready to prevent not fire
    if(_getPlatform.platformId == "1"){
        window.getUserData.init();
    }

    // If online create maps - wp crash when try to load google maps offline
    if(_networkOnline === true && _mapsLoaded === false && _deviceReady === true ){
        initMapsIframe();
    }

    //Event backbutton
    if(_getPlatform.platformId != "3"){
        document.addEventListener("backbutton", onBackKeyDown, false);
    } else {
        WinJS.Navigation.addEventListener("navigating", onBackKeyDown);
    }
}

/**
 * Backbutton event callback
 */
function onBackKeyDown(e) {
    _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;

    if(_getPlatform.platformId != "3"){
        if (_currentPage == "login" || _currentPage == "menu" || _currentPage == "preloadPage") {
            e.preventDefault();

            navigator.notification.confirm(
                LANG.alertexit.text,
                function(buttonIndex){
                    if (buttonIndex == 2) {
                        return false;
                    }
                    navigator.app.exitApp();
                },
                LANG.alerttitle.text,
                [LANG.btnOk.text, LANG.btnCancel.text]
            );
        } else {
            navigator.app.backHistory();
        }
    } else {
        //wp historyBack
        if(_currentPage == 'login' || _currentPage == 'menu'){
            WinJS.Navigation.history.backStack = [];
        } else {
            if(e.detail.delta == -1){
                if(e.detail.location == "#menu"){
                    //prevent back to de login
                    if(_currentPage != "registerPage" && _currentPage != "resetPasswordPage"){
                        $(":mobile-pagecontainer").pagecontainer("change", e.detail.location, {
                            transition: "slide"
                        });
                    } else {
                        $(":mobile-pagecontainer").pagecontainer("change", "#login", {
                            transition: "slide"
                        });
                    }
                    WinJS.Navigation.history.backStack = [];
                } else {
                    historyBack();
                }
            }
        }

    }
}

/**
 * Notifications
 */
function alertDismissed() {
    // do something
}

function generalAlert(msg) {
    if (debug !== 0 && _deviceReady === false && _getPlatform.platformId != "3" ) {
        alert(msg);
    } else {
        navigator.notification.alert(
            msg,
            alertDismissed,
            LANG.alerttitle.text,
            LANG.btnOk.text
        );
    }
}

/**
 * load login page
 */
function showLogin() {
    //$.mobile.changePage($("#login"));

}
/**
 * go to page back
 */
function historyBack() {
    window.history.back();
}

/**
 * Create inframe maps if state is online
 */
function initMapsIframe(){
    console.log("Maps create");
    _mapsLoaded = true;
    // Create iframe map according to platform
    var frame_link = 'maps.html',
        framemap_id = 'frame_mvz',
        framemaploc_id = 'frame_mvzl',
        framemap = rampgap.makeFrame(_getPlatform, framemap_id, frame_link),
        framemaploc = rampgap.makeFrame(_getPlatform, framemaploc_id, frame_link);
    $('#'+framemap_id+', #'+framemaploc_id).remove();
    $('#viewZonesMapResult [data-role=content]').append(framemap);
    $('#mapaZonesLocalization [data-role=content]').append(framemaploc);
}

/**
 * put all initial things here after html document finish to load.
 */
$(document).on('focusin', 'input, textArea', function() {
    $('div[data-role="footer"]').hide();
});

$(document).on('focusout', 'input, textArea', function() {
    window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
    $('div[data-role="footer"]').show();
});


$(document).ready(function(e) {
    onBodyLoad();

    //Version app in login
    $(".versionApp").html("v. "+rampgap.getAppVersion());

    // External popup
    $(".ui-extarnal-popup").enhanceWithin().popup();
    // $("#resultGetPricePop").listview('refresh');
    // $("#popPrice").popup('open');

    // Persistent Footer
    $( "[data-role='footer']" ).toolbar({
        tapToggle: false,
        position: "fixed"
    });

    //Preload
    $("#preloadPage").height($(document).height()-$('.ui-header').outerHeight( true ) || 0);
    $("#preloadPage .ui-content").height(rampgap.getContentHeigth());
    $( ".footer-principal" ).addClass('invisible');

    //Loader text
    $("#preloadPage .cpsLoader_text").addClass("animated fadeIn");

    //Fix Footer Fixed Androdi 2.x
    $('body').on('touchstart', function(e) {});

    //Set language
    if (window.localStorage.getItem("lang")!== null){
        setLanguage(window.localStorage.getItem("lang"));
        $('#langSelect').val(window.localStorage.getItem("lang"));
        $('#langSelect [value='+window.localStorage.getItem("lang")+']').prop('selected', true).change();
    } else {
        window.localStorage.setItem("lang", "0");
    }

    //Init language
    initialize();


    // PAGE EVENT
    $(document).on("pagecontainerbeforeshow", function(event, ui) {

        _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;

        var imessage,
            prevPage = typeof ui.prevPage[0] != 'undefined' ? ui.prevPage[0].id : null;

        // Check if any bottom if disabled and enabled
        $("#"+_currentPage+" a.ui-btn[href=#], #"+_currentPage+"input[type=submit]").removeClass("ui-disabled");

        // Debug
        console.log("Pre page: "+prevPage);
        console.log("Current:"+ _currentPage);

        if($('#'+_currentPage).hasClass( "tutorial-page" )){
            $( ".footer-principal" ).hide();
        }

        switch (_currentPage) {
        case 'preloadPage':
            if (logueado === 1) {
                $.mobile.loading('show');
                $(":mobile-pagecontainer").pagecontainer("change", "#menu", {
                    transition: "slide"
                });
            } else {
                $.mobile.loading('show');
                $(":mobile-pagecontainer").pagecontainer("change", "#login", {
                    transition: "slide"
                });
            }
            break;
        case 'login':
            if (logueado === 1) {
                $.mobile.loading('show');
                $(":mobile-pagecontainer").pagecontainer("change", "#menu", {
                    transition: "slide"
                });
            } else {
                setTimeout(function() {
                    navigator.splashscreen.hide();
                    _hidesplash = true;
                }, 2000);
            }
            break;
        case 'menu':
            if (logueado != 1) {
                $.mobile.loading('show');
                $(":mobile-pagecontainer").pagecontainer("change", "#login", {
                    transition: "slide"
                });
            }
            if(_hidesplash === false){
                setTimeout(function() {
                    navigator.splashscreen.hide();
                    _hidesplash = true;
                }, 2000);
            }
            // If online create maps
            if(_mapsLoaded === false && _networkOnline === true){
                initMapsIframe();
            }
            //Enable content for user Admin
            rampgap.checkIsAdmin(_isAdmin);
            break;
        case 'adminPage':
            if(!_isAdmin){
                var pageRedirect = logueado === 1 ? "#menu" : "#login";
                $(":mobile-pagecontainer").pagecontainer("change", pageRedirect, {
                    transition: "slide"
                });
            }
            break;
        case 'addVehicle':
            $('#newPlate').val("");
            break;
        case 'paycityparking':
            $('#parkingCode').val('');
            $('#payCode').val('');
            break;
        case 'mapaZonesLocalization':
            // If map not already created
            if(_mapsLoaded === false){
                initMapsIframe();
            }
            var mvzl = document.getElementById('frame_mvzl').contentWindow;

            imessage = {
                type: 'loadmap',
                info: {zones: rampgap.getCurrentZoneInfo(),
                       lat: rampgap.getCurrLatitude(),
                       log: rampgap.getCurrLongitude()
                      },
                data: myData,
                platform: _getPlatform,
                action: 'initializeMapPosition'
            };

            mvzl.postMessage(imessage, _getPlatform.protocol+document.location.host);
            break;
        case 'viewZonesMapResult':
            // If map not already created
            if(_mapsLoaded === false){
                initMapsIframe();
            }
            var mvz = document.getElementById('frame_mvz').contentWindow;

            imessage = {
                type: 'loadmap',
                info: {zones: rampgap.getCurrentZoneInfo()},
                data: myData,
                platform: _getPlatform,
                action: 'initializeMap'
            };

            mvz.postMessage(imessage, _getPlatform.protocol + document.location.host);
            break;
        case 'registerPage':
            //Clear input's
            rampgap.clearInput(_currentPage);
            //Get data info from user phone
            if(_getPlatform.platformId == "1"){
                if(window.getUserData.available){
                    $("#email").val(window.getUserData.email);

                    if(window.getUserData.displayname){
                        var gdDisplayName = window.getUserData.displayname.trim().split(/\s+/);
                        if(gdDisplayName.length >= 3){
                            $("#name").val(gdDisplayName[0]);
                            $("#last").val(gdDisplayName[2]);
                        } else if (gdDisplayName.length == 2){
                            $("#name").val(gdDisplayName[0]);
                            $("#last").val(gdDisplayName[1]);
                        } else {
                            $("#name").val(window.getUserData.displayname);
                        }
                    }

                    if(window.getUserData.phone){
                        $("#msisdn").val(window.getUserData.phone);
                    } else {
                        $("#msisdn").val(window.getUserData.profilephone);
                    }
                }
            }
            getCountries();
            break;
        case 'tutorialRecarga':
            if(prevPage == "reloadBalanceZP"){
                $("#"+_currentPage+" .tutorial-exit").attr("href", "#"+prevPage);
            } else {
                $("#"+_currentPage+" .tutorial-exit").attr("href", "#menu");
            }
            break;
        }

    });

    $(document).on("pagecontainershow", function(event, ui) {
        _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;
        switch (_currentPage) {
        case 'login':
        case 'menu':
            //Enable Footer
            if($( ".footer-principal" ).hasClass('invisible')){
                $( ".footer-principal" ).removeClass('invisible');
            }
            break;
        case 'mapaZonesLocalization':
            var lheight = rampgap.getContentHeigth()-($('.locatezonesalerttxt').outerHeight( true )||0)+'px';
            $('#'+_currentPage+' iframe').height(lheight);
            //$('#'+page+' iframe').contents().find(".maps").height(lheight);
            break;
        case 'viewZonesMapResult':
            var wheight = rampgap.getContentHeigth();
            $('#'+_currentPage+' iframe').height(wheight);
            break;
        }

        // Send page visit to Analytics
        rampgap.ga('send', 'pageview', {"page": _currentPage});

        //Send navigation for wp
        if(_getPlatform.platformId == "3"){
            if(_currentPage != 'login'){
                WinJS.Navigation.navigate("#"+_currentPage);
            }
        }
    });

    $(document).on('pagecreate', '#viewAccountInfo', function() {
        $('#userInfoForm .ui-input-datebox .ui-btn').hide();
        $('#userInfoForm select').prop('disabled', true);
    });

    $( window ).on( "orientationchange", function( event ) {
        _currentPage = $(":mobile-pagecontainer").pagecontainer("getActivePage")[0].id;
        var wheight = rampgap.getContentHeigth();
        switch(_currentPage){
        case 'mapaZonesLocalization':
        case 'viewZonesMapResult':
            $('#'+_currentPage+' iframe').height(wheight);
            break;
        }
    });

    //SOCIAL BUTTONS

    //data for wp8 - socialsharing not support wp8.1
    function shareLinkHandler(e) {
        var request = e.request;
        request.data.properties.title = LANG.shareTitle.text;
        request.data.properties.description = LANG.shareMessage.text;
        request.data.setText(LANG.shareMessage.text+' - '+new Windows.Foundation.Uri(LANG.shareLink.text));
        //request.data.setWebLink(new Windows.Foundation.Uri(LANG.shareLink.text));
    }

    $(document).on('tap', ".cps-share", function (e) {

        if (_getPlatform.platformId == "3") {
            var dataTransferManager = Windows.ApplicationModel.DataTransfer.DataTransferManager.getForCurrentView();
            dataTransferManager.addEventListener("datarequested", shareLinkHandler);
            Windows.ApplicationModel.DataTransfer.DataTransferManager.showShareUI();
            rampgap.ga('send', 'event', 'share', 'wp');
        } else {
            window.plugins.socialsharing.share(
                LANG.shareMessage.text,
                LANG.shareTitle.text,
                null,
                LANG.shareLink.text,
                function(res) {
                    console.log('share ok');
                    // Send event share success to Analytics
                    rampgap.ga('send', 'event', 'share', 'success');
                },
                function(err) {
                    console.log('error: ' + err);
                    // Send event share error to Analytics
                    rampgap.ga('send', 'event', 'share', 'error');
                }
            );
        }
    });

    $(document).on('tap', ".btn-fb", function(e){
        console.log(e);
        rampgap.social('https://facebook.com/398654126812091');
    });

    $(document).on('tap', ".btn-tw", function(e){
        rampgap.social('https://twitter.com/CityParkingsas');
    });

    //SETTINGS EVENTS
    $('#settingsMenu').click(function() { //ajustes v1.0.1
        $('#languageSelect').find('form').find('span:first').html(LANG.selectlangtext.text);
    });


    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //ajustes v1.0.1
    $('#sendEmailReset').click(function(e) {
        var msg = $('#emailReset').val();
        if (!filter.test(msg)) {
            generalAlert(LANG.alerttypeemail.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=23&mail=' + msg,
            success: function(response) {
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == -117) {
                        generalAlert(LANG.alertresetsend.text);
                        $('#emailReset').val('');
                        //$.mobile.changePage($("#login"));
                        $("body").pagecontainer("change", "#login", {
                            transition: "slide"
                        });
                        return false;
                    }
                    if (response == -118) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertcheckemail.text);
                        return false;
                    }
                    $('#emailReset').val('');
                    $.mobile.loading('hide');
                    return false;
                }
            }

        });
    });

    //LOGIN / REGISTER EVENTS
    $('#ingresar').click(function(e) {
        var returnCheckVersion = checkAppVersion();
        if (returnCheckVersion === true) {
            login($('#userName').val(), encrypt($('#userPass').val()));
            $('#userName').val("");
            $('#userPass').val("");

        }
    });

    $('#btnRegister').click(function(e) {
        getCitiesRegister(1, 0);
        $("body").pagecontainer("change", "#registerPage", {
            transition: "slide"
        });

    });

    $('#registerCountry').change(function(e) {
        getCitiesRegister($('#registerCountry').val());
    });

    $('#citiesSelect').change(function(e) {
        $('#viewZones').find('form').find('span:first').html($("#citiesSelect option:selected").text());
    });


    $(document).on('tap', "#register-submit", function() {

        var name = $("#name").val(),
            last = $("#last").val(),
            email = $("#email").val(),
            msisdn = $("input#msisdn").val(),
            pass = $("#pass").val(),
            pass1 = $("#pass1").val(),
            user = $("#user").val(),
            // var address = $("#address").val();
            //bd = $("#registerYear").val()+'-'+$("#registerMonth").val()+'-'+$("#registerDay").val(), // (yyyy-MM-dd)
            bd = $("#registerPage #birthDate").val(),
            // bd = "2000-01-01", //Default date
            // var country = $('#registerCountry').val();
            // var city = $("#registerCity").val();
            country = 472, //Default Colombia
            city = 103, //Default Bogota
            emailAd = $("#checkbox-mini-0").is(':checked'),
            checkInput = true;

        var selectorInput = '#registerPage input[type=tel], #registerPage input[type=text], #registerPage input[type=email], #registerPage input[type=password]';

        $(selectorInput).removeClass('formError');
        $('#registerPage select').parent().removeClass('formError');

        $(selectorInput).each(function() {
            if ($(this).val().length <= 2) {
                $('#' + $(this).attr('id')).addClass('formError');
                checkInput = false;
            }
        });

        if (checkInput === false) {
            $.mobile.loading('hide');
            generalAlert(LANG.alertfields.text);
            return false;
        } else {

            var filterUser = /^([a-zA-Z0-9ñÑ_\.\-])+$/;
            if (!filterUser.test(user)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertinvaliduser.text);
                $("#user").addClass('formError');
                return false;
            }
            if (!filter.test(email)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alerttypeemail.text);
                $("#email").addClass('formError');
                return false;
            }

            var filterPhone = /^([0-9])+$/;
            if (msisdn.length > 10 || msisdn.length < 10 || !filterPhone.test(msisdn)) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertinvalidphone.text);
                $("#registerPage input#msisdn").addClass('formError');
                return false;
            }

            //address.length > 45
            if (user.length > 12 || pass.length > 15 || name.length > 30 || last.length > 30 || bd.length > 10 || email.length > 45) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertlongfield.text);

                if (user.length > 12) {
                    $("#user").addClass('formError');
                }
                if (pass.length > 15) {
                    $("#pass").addClass('formError');
                }
                if (name.length > 30) {
                    $("#name").addClass('formError');
                }
                if (last.length > 30) {
                    $("#last").addClass('formError');
                }
                if (bd.length > 10) {
                    $("#registerPage #birthDate").addClass('formError');
                }
                if (email.length > 45) {
                    $("#email").addClass('formError');
                }
                return false;
            }

            // if (country === 0 || country == "0" || country == "") {
            //     $.mobile.loading('hide');
            //     generalAlert(LANG.alertcountryfield.text);
            //     $('#registerCountry').parent().addClass('formError');
            //     return false;
            // }
            // if (city === 0 || city == "0" || city == "") {
            //     $.mobile.loading('hide');
            //     generalAlert(LANG.alertcityfield.text);
            //     $('#registerCity').parent().addClass('formError');
            //     return false;
            // }

            if (pass == pass1) {
                //var dataString = 'user=' + user + '&pass=' + encrypt(pass) + '&name=' + name + '&last_name=' + last + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city + '&emailad=' + emailAd;
                var dataString = 'user='+ user + '&pass=' + encrypt(pass) + '&name=' + name + '&last_name=' +
                        last + '&email=' + email + '&msisdn=' + msisdn + '&country=' + country + '&city=' + city + '&bd=' + bd + '&emailad=' + emailAd;

                userRegister(dataString);
            } else {
                $.mobile.loading('hide');
                generalAlert(LANG.alertpasswords.text);
                $("#pass1").addClass('formError');
            }
        }
    });

    //ZONES SUB BUTTONS
    $('#btnViewZones').click(function() {
        getCities(myData.country, "#viewZones");

    });
    $('#btnViewZonesMap').click(function() {
        getCities(myData.country, "#viewZonesMap");
    });

    $('#btnViewCityZones').click(function() {
        viewZones($('#viewZones #citiesSelect').val());
    });

    $('#btnViewCityZonesMap').click(function() {
        viewZonesMap($('#viewZonesMap #citiesSelect').val());
    });
    $('#btnLocateZones').click(function() {
        locateZones();
    });
    $('.viewZoneOptions').click(function() {
        viewZoneOptions($(this).attr('rel'), $(this).attr('title'));
    });

    $('.viewZoneSMenu').click(function() {
        viewZoneOptions($(this).attr('rel'), $(this).attr('title'));
    });

    $(document).on('click', '.viewZoneMap', function(e){
        var zone = $(this).attr('data-zone');
        viewZoneMap(zone);
    });

    //PARKING SUB BUTTONS
    $(document).on('click', '.parkingbutton', function() {
        $("#popPayParking").popup("open");
    });

    //DRIVER SUB BUTTONS

    $('#goDriverPage').click(function() {
        window.location.href = 'cityDriver.html';
    });

    $(document).on('click', '#btn-driverLocation', function() {
        locateDriver();
    });

    $(document).on('click', '#btn-driverCheck', function() {
        if ($('#driverAdrressPick').val().length > 3) {
            sendInputDriver();
        } else {
            generalAlert(LANG.alertnodriverorig.text);
        }
    });

    $(document).on('click', '#btn-driverCheckDest', function() {
        if ($('#driverAdrressPickDest').val().length > 3) {
            sendInputDriver();
        } else {
            generalAlert(LANG.alertnodriverdest.text);
        }
    });

    $(document).on('click', '#openDriverForm', function() {
        $('#driverFullName').val(myData.name + ' ' + myData.lastname);
        $('#driverMsisdn').val(myData.msisdn);
        $('#popOrigAddress').val($('#driverAdrressPick').val());
        $('#popDestAddress').val($('#driverAdrressPickDest').val());

        $("#driverForm").popup({
            afteropen: function(event, ui) {
                $('#driverContact').focus();
            }
        });
    });

    $(document).on("focusin", "#driverAdrressPick, #driverAdrressPickDest", function(event) {
        $("#popHelpAddress").fadeIn();
        setTimeout(function() {
            $("#popHelpAddress").fadeOut();
        }, 5000);
        event.preventDefault();
    });
    $(document).on("focusout", "#driverAdrressPick, #driverAdrressPickDest", function(event) {
        $("#popHelpAddress").fadeOut();
        event.preventDefault();
    });


    //VEHICLES SUB BUTTONS
    $('#btnViewVehicles').click(function() {
        myVehicles();
    });
    $('#btnAddCar').click(function() {
        if ($('#newPlate').val() == "") {
            generalAlert(LANG.alertcarplate.text);
        } else {
            var newPlate = $('#newPlate').val().toUpperCase();
            addVehicle(newPlate);
        }

    });

    //ACCOUNT BUTTONS
    $('#btnViewAccount').click(function() {
        if($('#infoCountry option').length <=1){
            getCountries();
        }
        viewAccountInfo();
    });

    $('#btnSavePass').click(function() {
        if ($('#oldPass').val() == "" || $('#newPass').val() == "" || $('#newPass').val() != $('#rePass').val()) {
            generalAlert(LANG.alertpassequals.text);
        } else {
            savePassword($('#oldPass').val(), $('#oldPass').val(), $('#newPass').val());
        }
    });
    $('#btnEditAccount').click(function() {
        $('#userInfoForm').find('input,select').each(function() {
            //$(this).removeAttr('disabled');
            //$(this).attr("disabled",false);
            // this.disabled = false;
            //console.info("item name="+ $(this).prop("id"));
            //$(this).removeAttr('disabled').removeClass('ui-state-disabled').parent().removeClass('ui-state-disabled');
            $(this).removeAttr('readonly');
        });

        $('#userInfoForm .ui-input-datebox .ui-btn').show();
        $('#userInfoForm select').prop('disabled', false);

        //Fix select city
        var currentCity = $('#infoCity').parent().find('span').text();
        $('#infoCity option').each(function() {
            if ($(this).text() == currentCity) {
                $(this).prop('selected', true).change();
            }
        });

        $('#infoCU').attr('disabled', 'disabled').addClass('ui-state-disabled');
        $('#infoLogin').attr('disabled', 'disabled').addClass('ui-state-disabled');
        $('#userInfoForm').trigger("refresh");
        $('#btnEditAccount,#btnBackAccount').hide();
        $('#btnSaveAccount,#btnCancelEdit').show();
    });
    $('#btnCancelEdit').click(function() {
        viewAccountInfo();
        $('#userInfoForm').find('input,select').each(function() {
            //$(this).attr("disabled","disabled").addClass('ui-state-disabled').parent().addClass('ui-state-disabled');
            $(this).attr('readonly', 'readonly');
        });
        $('#userInfoForm .ui-input-datebox .ui-btn').hide();
        $('#userInfoForm select').prop('disabled', true);
        $('#userInfoForm').trigger("refresh");
        $('#btnSaveAccount,#btnCancelEdit').hide();
        $('#btnEditAccount,#btnBackAccount').show();

        var selectorInput = '#userInfoForm input[type=text],#userInfoForm input[type=tel],#userInfoForm input[type=email], #userInfoForm input[type=password]';
        $(selectorInput).removeClass('formError');
    });
    $('#btnSaveAccount').click(function() {
        navigator.notification.confirm(
            LANG.alertconfirmmodifyinfo.text,
            saveUserUpdate,
            LANG.alerttitle.text,
            [LANG.btnOk.text, LANG.btnCancel.text]
        );

        //rampgap.confirmDialog(LANG.alertconfirmmodifyinfo.text, "saveUserUpdate");
    });
    $('#changePass').click(function() {
        $('#userPass .campo').each(function() {
            $(this).val('');
        });
    });

    $('#btnChangeUser').click(function() {
        navigator.notification.confirm(
            LANG.alertlogout.text,
            changeUser,
            LANG.alerttitle.text,
            [LANG.btnOk.text, LANG.btnCancel.text]
        );
    });

    //BALANCE
    $('#viewBalance').click(function() {
        $.mobile.loading('show');
        $.ajax({
            data: 'op=2&user=' + myData.login + '&pass=' + myData.pass,
            success: function(response) {
                if (response == null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response === null) {
                        $.mobile.loading('hide');
                        return false;
                    }
                    $.mobile.loading('hide');
                    //NT - Actualizando saldo de usuario en SESSION
                    rampgap.updateBalance(response.balance);
                    var balance = '<li class="ui-li-static ui-body-inherit ui-first-child ui-last-child"><p>' + LANG.balancetxt.text + ':</p> <h3 class="ui-li-aside"><strong>$ ' + response.balance + '</strong></h3></li>';
                    //Fix error append wp8.1
                    balance = rampgap.toStatic(balance);

                    $('#infoBalanceContent #infoBalanceList').html(balance);
                    //$.mobile.changePage($("#infoBalance"));
                    $("body").pagecontainer("change", "#infoBalance", {
                        transition: "slide"
                    });

                }
            }
        });
    });

    $('#reloadBalancePin').click(function() {
        var pin = $('#pinNumber').val();
        pin = pin.replace(' ', '');
        if (pin == "") {
            generalAlert(LANG.alerrtypepin.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=13&user=' + myData.login + '&pass=' + myData.pass + '&pin=' + pin,
            success: function(response) {
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == "") {
                        $.mobile.loading('hide');
                        return false;
                    }
                    if (response.code == -101) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertinvalidpin.text);
                        return false;
                    }
                    $('#pinNumber').val('');
                    //NT - Actualizando saldo de usuario en SESSION
                    rampgap.updateBalance(response.obj.balance);
                    $.mobile.loading('hide');
                    generalAlert(LANG.alertreloadok.text);
                    //$.mobile.changePage($("#balance"));
                    $("body").pagecontainer("change", "#balance", {
                        transition: "slide"
                    });

                }
            }
        });
    });

    //--end reload balance pin


    $('#rbalancezplink').click(function() {
        //generalAlert("response");

        $.ajax({
            data: 'op=27&user=' + myData.login + '&pass=' + myData.pass,
            success: function(response) {
                console.log(JSON.stringify(response));
                if (response === null) {
                    generalAlert("null");
                    $.mobile.loading('hide');
                    return false;
                } else {
                    $.mobile.loading('hide');
                    if (response == "") {
                        return false;
                    }
                    //generalAlert("code"+response.code);
                    if (response.code == -100) {
                        //rampgap.confirmDialog(response.obj, "javascript:confirmPayZp()");
                        navigator.notification.confirm(
                            response.obj,
                            confirmPayZp,
                            LANG.alertconfirmrbalnacetitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
                        );
                        $("body").pagecontainer("change", "#balance", {
                            transition: "slide"
                        });
                        return false;
                    }
                    if (response.code == -101) {
                        alert(response.obj);
                        $("body").pagecontainer("change", "#balance", {
                            transition: "slide"
                        });
                        return false;
                    }
                    $("body").pagecontainer("change", "#reloadBalanceZP", {
                        transition: "slide"
                    });

                }
            },
            error: function(response, textStatus, errorThrown) {
                generalAlert(textStatus);
            }


        });

    });



    // reload balance ZP
    $(document).on('tap', "#reloadBalanceZPButton", function() {
        var today = new Date(),
            value = $('#payValue').val().replace(' ', ''),
            storageHelp = window.localStorage.getItem("helpBalance");

        if (isNaN(value) || value === "") {
            generalAlert(LANG.alertypevalue.text);
            return false;
        }

        if(!storageHelp || today > new Date(storageHelp)){

            $("#popHelReload .ui-icon-delete").addClass("invisible");
            $("#popHelReload .toggleHidenHelp").removeClass("invisible");
            $("#popHelReload").popup('open', {
                transition: "pop"
            });
        } else {
            reloadBalanceZP(value);
        }
    });

    $(document).on('tap', "#continuePopReload", function(){
        var value = $('#payValue').val().replace(' ', ''),
            dateSave = new Date();

        dateSave.setDate(dateSave.getDate() + 15);
        if($("#checknoshowpop").is(":checked")){
            dateSave = "never";
        }
        window.localStorage.setItem("helpBalance", dateSave);
        reloadBalanceZP(value);
    });

    $(document).on('tap', "#infoHelReload", function(){
        $("#popHelReload .ui-icon-delete").removeClass("invisible");
        $("#popHelReload .toggleHidenHelp").addClass("invisible");
        $("#popHelReload").popup('open', {
            transition: "pop"
        });
    });

    function reloadBalanceZP(value){
        $("#reloadBalanceZPButton").addClass("ui-disabled");
        $.mobile.loading('show');
        $.ajax({
            data: 'op=26&user=' + myData.login + '&pass=' + myData.pass + '&value=' + value,
            success: function(response) {
                console.log(JSON.stringify(response));
                $("#reloadBalanceZPButton").removeClass("ui-disabled");
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == "") {
                        $.mobile.loading('hide');
                        return false;
                    }
                    $.mobile.loading('hide');
                    if (response.code == -101) {

                        generalAlert(LANG.alertinvalidpin.text);
                        return false;
                    }
                    $('#payValue').val('');
                    //NT - Actualizando saldo de usuario en SESSION
                    //rampgap.updateBalance(response.obj.balance);
                    //alert(response.obj);
                    var myURL = encodeURI(response.obj);
                    var ref = window.open(myURL, '_system');

                    //rampgap.openURL(response.obj, "ZonaPagos");

                    //  generalAlert(LANG.alertreloadok.text);
                    $("body").pagecontainer("change", "#balance", {
                        transition: "slide"
                    });
                }
            },
            error: function(response, textStatus, errorThrown) {
                generalAlert(LANG.alertgeneralerror.text);
                $("#reloadBalanceZPButton").removeClass("ui-disabled");
                $.mobile.loading('hide');
                return false;
            }


        });
    }
    //--end reload balance ZP

    /**
     * Button to fire give balance event
     */
    $('#btngiveBalance').click(function(e) {
        var repass = $('#givepassword').val();
        var cu = $('#cu').val();
        var recu = $('#recu').val();
        var amount = $('#amount').val();

        cu = cu.replace(' ', '');
        recu = recu.replace(' ', '');
        amount = amount.replace(' ', '');
        amount = amount.replace('.', '');
        amount = amount.replace(',', '');

        if (cu === "" || recu === "" || cu != recu) {
            generalAlert(LANG.alertcheckcu.text);
            return false;
        } else {
            if (amount === "") {
                generalAlert(LANG.alertamount.text);
                return false;
            }
        }

        $("#"+e.target.id).addClass("ui-disabled");
        $.mobile.loading('show');
        $.ajax({
            data: 'op=14&user=' + myData.login + '&pass=' + myData.pass + '&cu=' + cu + '&amm=' + amount + '&repass=' + encrypt(repass),
            success: function(response) {

                console.log(response);
                $("#"+e.target.id).removeClass("ui-disabled");
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response === "") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertcheckdata.text);
                        return false;
                    } else if(response.code == -1 ) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertinvalidcu.text);
                        return false;
                    } else if (response.code == -2) {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertcheckdata.text);
                        return false;
                    } else if(response.code == -112){
                        //NT - Actualizando saldo de usuario en SESSION
                        rampgap.updateBalance(response.obj.balance);

                        $('#givepassword').val('');
                        $('#cu').val('');
                        $('#recu').val('');
                        $('#amount').val('');
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertgiveok.text);

                        $("body").pagecontainer("change", "#balance", {
                            transition: "slide"
                        });
                    }

                }
            },
            error: function(response, textStatus, errorThrown){
                $("#"+e.target.id).removeClass("ui-disabled");
            }
        });
    });

    /**
     * pupulate city wheter country
     */
    $('#infoCountry').change(function(e) {
        getCitiesRegister($('#infoCountry').val());
    });

    $('#carPLate').change(function(e) {
        $('#startParking').find('form').find('span:first').html($(this).val());
    });

    /**
     * Button to change language
     */
    $('#saveLang').click(function(e) {

        e.preventDefault();
        $.mobile.loading( 'show' );
        window.localStorage.setItem("lang", $('#langSelect').val());
        setLanguage($('#langSelect').val());
        $('#langSelect [value='+window.localStorage.getItem("lang")+']').prop('selected', true).change();
        initialize();
        $.mobile.loading( 'hide' );

        $("body").pagecontainer("change", "#menu", {
            transition: "slide"
        });

    });
    /**
     * on click sends suggestion
     */
    $('#sendSug').click(function(e) {
        var msg = $('#suggestiontxt').val();
        if (msg == "" || msg == " " || msg == "  ") {
            generalAlert(LANG.alertmsgempty.text);
            return false;
        }

        $.mobile.loading('show');
        $.ajax({
            data: 'op=19&user=' + myData.login + '&pass=' + myData.pass + '&sug=' + msg,
            success: function(response) {
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response == "") {
                        $.mobile.loading('hide');
                        return false;
                    }
                    if (response == "[]") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertmsgempty.text);
                        return false;
                    }
                    $('#suggestiontxt').val('');
                    $.mobile.loading('hide');
                    generalAlert(LANG.suggestionsend.text);
                    //$.mobile.changePage($("#menu"));
                    $("body").pagecontainer("change", "#menu", {
                        transition: "slide"
                    });

                }
            }
        });
    });

    /*
     * Envio de Pago a  City Parking
     */
    $('#sendPayment, #sendPaymentPop').click(function(e) {
        var pcode,
            paycode;

        if(e.target.id == 'sendPaymentPop'){
            pcode = $('#parkingCodePop').val();
            paycode = $('#payCodePop').val();
        } else {
            pcode = $('#parkingCode').val();
            paycode = $('#payCode').val();
        }

        if (pcode == "" || pcode == " " || pcode == "  ") {
            generalAlert(LANG.alertparkingempty.text);
            return false;
        }
        if (paycode == "" || paycode == " " || paycode == "  ") {
            generalAlert(LANG.alertpayempty.text);
            return false;
        }

        $("#"+e.target.id).addClass("ui-disabled");
        $.mobile.loading('show');
        $.ajax({
            data: 'op=24&user=' + myData.login + '&pass=' + myData.pass + '&parkcode=' + pcode + '&paycode=' + paycode,
            success: function(response) {
                console.log("Response Pay: "+response.value);
                $("#"+e.target.id).removeClass("ui-disabled");
                if (response == null) {
                    $.mobile.loading('hide');
                    generalAlert(LANG.alertconfirmpaymentiderror.text);
                    return false;
                } else {
                    if (response == "") {

                        $.mobile.loading('hide');
                        generalAlert(LANG.alertconfirmpaymentiderror.text);
                        return false;
                    }
                    if (response == "[]") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertmsgempty.text);
                        return false;
                    }
                    if (response == "-1") {
                        $.mobile.loading('hide');
                        generalAlert(LANG.alertmsgempty.text);
                        return false;
                    }

                    $.mobile.loading('hide');
                    var res = LANG.alertpaidparkingerror.text;
                    //response=$.parseJSON(response);

                    if (parseInt(response.value) == 1 || response.value == 1) {
                        var msg = $.format(LANG.alertconfirmpayment.text, response.info.id, response.info.placeId, response.info.parkingInitDate, response.info.parkingEndDate, response.info.paymentValue, response.info.parkingTime);
                        navigator.notification.confirm(
                            msg,
                            function(buttonIndex) {
                                confirmParking(buttonIndex, response.info);
                            },
                            LANG.alertconfirmpaymenttitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
                        );
                        return;
                    } else if (parseInt(response.value) === 0 || response.value == "0" || parseInt(response.value) == -301 || response.value == "-301") {
                        res = LANG.alertpaidparkingoffline.text;
                    } else if (parseInt(response.value) == 14) {
                        res = LANG.alertpaidparkingtokenerror.text;
                    } else if (parseInt(response.value) == 3) {
                        res = LANG.alertconfirmpaymentiderror.text;
                    } else if (response.value == "-1") {
                        res = LANG.alertpaycheck.text;
                    }
                    generalAlert(res);

                }
            },
            error: function(response, textStatus, errorThrown){
                $("#"+e.target.id).removeClass("ui-disabled");
            }
        });
    });

    //****************************************************************************************************************************************************** END READY************************************************
});

function confirmPayZp(buttonIndex) {
    if (buttonIndex == 2) {
        return false;
    }
    $("body").pagecontainer("change", "#reloadBalanceZP", {
        transition: "slide"
    });


}

function confirmParking(buttonIndex, infopay) {

    if (buttonIndex == 2) {
        return false;
    }
    console.log(infopay.id);
    $.mobile.loading('show');

    $.ajax({
        data: 'op=25&user=' + myData.login + '&pass=' + myData.pass + '&id=' + infopay.id + '&validationNumber=' + infopay.validationNumber +
            '&operationResult=' + infopay.operationResult + '&token=' + infopay.token + '&value=' + infopay.paymentValue + '&placeid=' + infopay.placeId +
            '&initparking=' + infopay.parkingInitDate + '&endparking=' + infopay.parkingEndDate + '&device=' + _getPlatform.platformId,
        success: function(response) {
            console.log(JSON.stringify(response));
            if (response == null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    $.mobile.loading('hide');
                    return false;
                }
                if (response == "[]") {
                    $.mobile.loading('hide');
                    generalAlert(LANG.alertmsgempty.text);
                    return false;
                }
                $.mobile.loading('hide');
                var res = LANG.alertpaidparkingerror.text;

                if (response == "12" || parseInt(response) == 12) {
                    res = LANG.alertpaidparkingok.text;
                    $('#parkingCode, #parkingCodePop').val('');
                    $('#payCode, #payCodePop').val('');
                    $("body").pagecontainer("change", "#menu", { transition: "slide"});
                } else if (parseInt(response) == 13)
                    res = LANG.alertpaidparkingnullerror.text;
                else if (parseInt(response) == 14)
                    res = LANG.alertpaidparkingtokenerror.text;
                else if (parseInt(response) == 9)
                    res = LANG.alertpaidparkingfail.text;
                generalAlert(res);

            }
        }
    });

}

// LOGIN -----------------------------------------------------------------
function changeUser(buttonIndex) {

    if (buttonIndex == 2) {
        return false;
    }

    rampgap.setItem('login', null);
    rampgap.setItem('pass', null);

    window.localStorage.setItem('logueado', "0");
    window.localStorage.removeItem('login');
    window.localStorage.removeItem('pass');

    session = 0;
    logueado = 0;
    _isAdmin = false;

    //$.mobile.changePage($("#login"));
    $("body").pagecontainer("change", "#login", {
        transition: "slide"
    });
    $("#login").val('');
    rampgap.destroySession();
}


function checkAppVersion() {
    console.log(_getPlatform);
    var returnCheck = false,
        platformId = _getPlatform.platformId;

    $.mobile.loading('show');
    $.ajax({
        data: 'op=28&deviceid=' + platformId + '&version=' + rampgap.getAppVersion(),
        async: false,
        success: function(response) {
            console.log('response:' + response +' '+platformId+' '+rampgap.getAppVersion());
            console.log(JSON.stringify(response));
            if (response == false) {
                checkSession();
                returnCheck = true;
            } else if (response == "") {
                generalAlert(LANG.alerttimeouterror.text);
            } else {

                setTimeout(function() {
                    navigator.notification.alert(
                        LANG.alertnewupdate.text,
                        function() {
                            _storeLink(platformId);
                        },
                        LANG.alerttitle.text,
                        LANG.btnOk.text
                    );
                }, 100);
                returnCheck = false;
            }
            $.mobile.loading('hide');
        }

    });
    return returnCheck;
}

/**
 * Connect to APP Store
 */
function _storeLink(platformId) {
    var myURL;

    switch (platformId) {
    case "1":
        CanOpen('market://', function (isInstalled) {
            if (isInstalled) {
                myURL = encodeURI('market://details?id=appandroidcity.cps');
            } else {
                myURL = encodeURI('https://play.google.com/store/apps/details?id=appandroidcity.cps');
            }
            window.open(myURL, '_system');
        });
        break;
    case "2":
        myURL = encodeURI('https://itunes.apple.com/us/app/cityparking/id868140927?ls=1');
        window.open(myURL, '_system');
        break;
    case "3":
        myURL = encodeURI('zune:navigate?appid=e76a319a-665f-4d09-9e15-2eaab2f660d4');
        window.open(myURL, '_system');
        break;
    }
}

/**
 * verify if current user has a valid session
 */
function checkSession() {

    console.log("Logueado?: "+window.localStorage.getItem('logueado'));

    if (rampgap.getItem("login") !== null && rampgap.getItem("login") && rampgap.getItem("pass") !== null &&
        rampgap.getItem("login") != 'null' && rampgap.getItem("pass") != 'null') {

        var _passSession = rampgap.getItem("pass");
        //Migration to pass encrypted from the start
        if(_passSession.length < 29){
            _passSession = encrypt(_passStorage);
        }
        login(rampgap.getItem("login"),_passSession);

    } else if (window.localStorage.getItem('logueado') == "1") {

        var _loginStorage = window.localStorage.getItem('login'),
            _passStorage = window.localStorage.getItem('pass');
        //Migration to pass encrypted from the start
        if(_passStorage.length < 29){
            _passStorage = encrypt(_passStorage);
        }
        login(_loginStorage, _passStorage);

    } else {
        //Enable content for user Admin
        rampgap.checkIsAdmin(_isAdmin);
        $.mobile.loading('hide');
        $('#user-login').show();
        $("body").pagecontainer("change", "#login", {
            transition: "fade"
        });
    }
}
/**
 * Request for login from server. on success go to menu and keep a valid session
 * @param user valid username
 * @param pass valid password
 *
 */
function login(user, pass) {

    /*
     SUCCES
     {"idsysUser":"3","address":"Kra 41G #47-12","balance":"$57.925","birthDate":"1984-11-19","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
     */
    $("#ingresar").addClass("ui-disabled");
    $.mobile.loading('show');

    if (user === "" || pass.length == 14) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertusername.text);
        return false;
    }

    $.ajax({
        data: 'op=0&user=' + user + '&pass=' + pass + '&device=' + _getPlatform.platform,
        success: function(response) {
            $("#ingresar").removeClass("ui-disabled");
            if (response == null) {
                console.log("Login null");
                $.mobile.loading('hide');
                generalAlert(LANG.alertusername.text);

                window.localStorage.setItem('logueado', "0");
                session = 0;
                logueado = 0;

                $("body").pagecontainer("change", "#login", {
                    transition: "fade"
                });
                return false;
            } else {
                console.log("Login valid...");
                console.log('Success - Hi: ' + response.login);
                //NT - Seteando values de usuario
                rampgap.setUserSession(response.login, pass, response.balance);
                //User Data
                myData.country = response.countryIdcountry;
                myData.city = response.city_idcity.idcity;
                myData.city_name = response.city_idcity.name;
                myData.msisdn = response.favoriteMsisdn;
                myData.idu = response.idsysUser;
                myData.address = response.address;
                myData.balance = response.balance;
                myData.cu = response.cu;
                myData.email = response.email;
                myData.usertype = response.idsysUserType;
                myData.name = response.name;
                myData.lastname = response.lastName;
                myData.login = response.login;
                myData.pass = pass;

                $.mobile.loading('hide');

                // Save in session
                if (session === 0){
                    rampgap.setItem("login",  response.login);
                    rampgap.setItem("pass",  pass);
                    session = 1;
                }
                //Save login in localStorage
                if (window.localStorage.getItem('logueado') != '1') {
                    window.localStorage.setItem('logueado', "1");
                }

                if(logueado === 0){
                    window.localStorage.setItem('login', response.login);
                    window.localStorage.setItem('pass', pass);
                    logueado=1;
                }

                $("body").pagecontainer("change", "#menu", {
                    transition: "fade"
                });

                //Enable content for user Admin
                _isAdmin = myData.usertype == _adminId ? true : false;
                rampgap.checkIsAdmin(_isAdmin);

                // Start Session and set user login in Analytics
                rampgap.ga('set', 'uid', response.login);

            }

        },
        error: function(response, textStatus, errorThrown) {
            console.log('ERROR' + JSON.stringify(response));
            $("#ingresar").removeClass("ui-disabled");
            if (response.status == 1001 || response.status === 0) {
                generalAlert(LANG.alertusername.text);
                $("body").pagecontainer("change", "#login", {
                    transition: "fade"
                });
                window.localStorage.setItem('logueado', "0");
                session = 0;
                logueado = 0;
                //Enable content for user Admin
                rampgap.checkIsAdmin(_isAdmin);
                if(_initAnalytics === true){
                    rampgap.ga('send', 'event', 'error', 'login'+response.status);
                    rampgap.ga('send', 'exception', 'Error login: '+response.status, true);
                }
            }
            $.mobile.loading('hide');
        }
    });

}

/**
 * get user info,
 * @param type contains info for get aditional data
 */
function getUserInfo(type) {

    var _loginStorage = window.localStorage.getItem('login'),
        _passStorage = window.localStorage.getItem('pass');

    $.mobile.loading('show');
    $.ajax({
        data: 'op=2&user=' + _loginStorage + '&pass=' + _passStorage,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                //User Data
                myData.country = response.countryIdcountry;
                myData.city = response.city_idcity.idcity;
                myData.city_name = response.city_idcity.name;
                myData.msisdn = response.favoriteMsisdn;
                myData.idu = response.idsysUser;
                myData.address = response.address;
                myData.balance = response.balance;
                myData.cu = response.cu;
                myData.email = response.email;
                myData.usertype = response.idsysUserType;
                myData.name = response.name;
                myData.lastname = response.lastName;
                myData.login = response.login;
                myData.pass = _passStorage;

                logueado = 1;
                $.mobile.loading('hide');

                if (type == 'driver') {
                    // Load plate and init locate driver
                    var cars = '';
                    $.mobile.loading('show');
                    $.ajax({
                        data: 'op=15&user=' + myData.login + '&pass=' + myData.pass,
                        success: function(response) {
                            if (response === null) {
                                $.mobile.loading('hide');
                                return false;
                            } else {
                                if (response == "") {
                                    generalAlert(LANG.alertnocars.text);
                                    $.mobile.loading('hide');
                                    return false;
                                } else {
                                    for (var i = 0; i < response.length; i++) {
                                        if (i === 0) {
                                            cars += '<option value="0" selected="selected">---</option> ';
                                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                                        } else {
                                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                                        }
                                    }

                                    //Fix error append wp8.1
                                    cars = rampgap.toStatic(cars);

                                    $('#driverPlate').html('');
                                    $('#driverPlate').html(cars);
                                    $.mobile.loading('hide');

                                }
                            }
                        }
                    });

                    sendInputDriver();
                    setTimeout(function() {
                        locateDriver();
                    }, 4000);
                } // End if driver

            }
        }
    });
}
/**
 * register a new user in the System,
 * @param dataString contains different parameter information
 */
function userRegister(dataString) {

    /*DATOS PARA REGISTRO
     user
     pass
     name
     last_name
     address
     email
     bd (yyyy-MM-dd)
     country
     msisdn
     city
     */

    $("#register-submit").addClass("ui-disabled");
    $.mobile.loading('show');
    $.ajax({
        data: 'op=1&' + dataString,

        success: function(response) {
            $("#register-submit").removeClass("ui-disabled");
            if (response === null) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertalreadyregister.text);
                //Send error event register to Analytics
                rampgap.ga('send', 'event', 'register', 'error');

                return false;
            } else {
                $.mobile.loading('hide');
                generalAlert(LANG.alertregisterok.text);
                $('#registerPage input[type=tel], #registerPage input[type=text], #registerPage input[type=email], #registerPage input[type=password]').val('');
                $('#registerCity [value="0"]').prop('selected', true).change();
                $("body").pagecontainer("change", "#login", {
                    transition: "slide"
                });

                // Send Login
                login(response.obj.login, encrypt(response.obj.pass));

                //Send success event register to Analytics
                rampgap.ga('send', 'event', 'register', 'success');
            }
        },
        error: function(response, textStatus, errorThrown) {
            $("#register-submit").removeClass("ui-disabled");
            generalAlert(LANG.alertalreadyregister.text);
            $.mobile.loading('hide');
        }
    });
}
// ------------------------------------------------------------------------------ ZONES ------------------------------------------------------------------------------------------
/**
 * return cities by country code
 * @param countryCode, the country code.
 */
function getCities(countryCode, page) {
    $.mobile.loading('show');
    var cities = '';
    $.ajax({
        data: 'op=8&idc=' + countryCode,
        success: function(response) {
            if (response == null) {
                $.mobile.loading('hide');
                return false;
            } else {
                for (var i = 0; i < response.length; i++) {
                    if (i == 0) {
                        cities += '<option value="' + response[i].idcity + '" selected="selected">' + response[i].name + '</option> ';
                        var currentCity = response[i].name; //ajustes v1.0.1
                    } else {
                        cities += '<option value="' + response[i].idcity + '">' + response[i].name + '</option> ';
                    }
                }

                //$(page).find('form').find('span:first').html(currentCity); //ajustes v1.0.1
                $.mobile.loading('hide');

                //Fix error append wp8.1
                cities = rampgap.toStatic(cities);

                $(page + ' #citiesSelect').html(cities);
                $(page + ' #citiesSelect [value='+myData.city+']').prop('selected', true).change();

                $("body").pagecontainer("change", page, {
                    transition: "slide"
                });

            }
        }
    });
}

/**
 *
 * @param cityCode
 */

function viewZones(cityCode) {
    $.mobile.loading('show');
    var results = '';
    $.ajax({
        data: 'op=7&user=' + myData.login + '&pass=' + myData.pass + '&idc=' + myData.country + '&idcity=' + cityCode,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {

                console.log(response);
                for (var i = 0; i < response.length; i++) {
                    zones[i] = new Array(response[i]);

                    results += '<li class="ui-li-has-count ui-first-child" >';
                    results += '    <a href="#" rel="' + i + '" title="' + response[i].name + '" class="ui-link-inherit viewZoneMap ui-btn ui-btn-icon-right ui-icon-carat-r" data-zone="' + i + '">';
                    results += '        <h3 class="ui-li-heading">' + response[i].name + '</h3>';
                    results += '        <p>' + LANG.placestxt.text + '</p>';
                    if (response[i].places < 0) {
                        response[i].places = 0;
                    }
                    if (response[i].places == '(--)') {
                        results += '        <span class="ui-li-count ui-li-nophone ui-body-b">' + '</span>';
                    } else {
                        results += '        <span class="ui-li-count ui-body-b">' + response[i].places + '</span>';
                    }
                    results += '    </a>';
                    results += '</li>';
                }

                //Fix error append wp8.1
                results = rampgap.toStatic(results);

                $('#zoneResultList').html(results);
                $.mobile.loading('hide');
                $("body").pagecontainer("change", "#viewZonesResult", {
                    transition: "slide"
                });

                $('.viewZoneOptions').click(function() {
                    viewZoneOptions($(this).attr('rel'), $(this).attr('title'));
                });
            }
        }
    });
}

function viewZonesMap(cityCode) {
    $.mobile.loading('show');
    var results = '';
    $.ajax({
        data: 'op=7&user=' + myData.login + '&pass=' + myData.pass + '&idc=' + myData.country + '&idcity=' + cityCode,
        success: function(response) {
            //generalAlert(response);
            if (response === null) {
                $.mobile.loading('hide');
                return false
            } else {

                $.mobile.loading('hide');
                //alert(JSON.stringify(response));
                showZonesMap(response);

            }
        },
        error: function(response) {
            generalAlert('error');
        }
    });
}
var ActualzoneArrayIndex,
    weekDays = new Array('', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

function viewZoneOptions(zoneArrayIndex, zoneName) {
    var zOptions = '';
    ActualzoneArrayIndex = zoneArrayIndex;
    zOptions += '<li data-theme="a" class="ui-first-child ui-last-child ui-li-has-thumb">';
    zOptions += '           <a href="#" rel="' + zoneArrayIndex + '" class="ui-btn ui-btn-icon-right ui-icon-carat-r viewZoneMap" data-zone="' + zoneArrayIndex + '">';
    zOptions += '                <img src="img/ico-zones.png" class="ui-li-thumb">';
    zOptions += '                <h3 class="ui-li-heading">' + LANG.viewmaptxt.text + '</h3>';
    zOptions += '         </a>';
    zOptions += ' </li>';

    //Fix error append wp8.1
    zOptions = rampgap.toStatic(zOptions);

    $('#zoneOptionsList').html(zOptions);
    //$.mobile.changePage($("#viewZonesOptions"));
    $("body").pagecontainer("change", "#viewZonesOptions", {
        transition: "slide"
    });

}

function viewZoneMap(zoneArrayIndex) {
    showZonesMap(zones[zoneArrayIndex]);
}

function showZonesMap(zonemap) {
    rampgap.showZoneSMap(JSON.stringify(zonemap));
}

function showZonesMapLocation(zonemap) {
    rampgap.showZoneSMapLocation(JSON.stringify(zonemap));
}

function viewZoneMapCar(carlat, carlong) {
    rampgap.setCurrentLatLong(carlat + "," + carlong);
    rampgap.showCarMap();
}

function locateZones() {
    //rampgap.verifyGPSIsActive();
    getPosition('locateZonesCoords');
}

function locateZonesCoords(position) {
    console.log(position.coords.latitude);
    var clat = position.coords.latitude;
    var clong = position.coords.longitude;
    rampgap.setCurrLatitude(clat);
    rampgap.setCurrLongitude(clong);
    afterGPSGetLocation(clat, clong);
}

function afterGPSGetLocation(clat, clong) {
    var results = '';
    $.ajax({
        data: 'op=16&user=' + myData.login + '&pass=' + myData.pass + '&lat=' + clat + '&lon=' + clong,
        success: function(response) {
            if (response == null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    generalAlert(LANG.alertnozone.text);
                    $.mobile.loading('hide');
                    return false;
                } else {
                    showZonesMapLocation(response);
                }
            }
        }
    });
}

/**
 * @param zoneId
 */
function getPriceZone(zoneId){
    $.mobile.loading('show');
    console.log("Get Price");
    var dataResponse = "";
    $.ajax({
        //data:
        url: "https://www.cpsparking.co/cps_price/price.json",
        type: "GET",
        dataType: "json",
        success: function(response){
            if(response === null){
                $.mobile.loading('hide');
                console.log("Get Price Error");
                return false;
            } else {
                console.log("Get Price OK");
                $.each( response.obj, function( key, zone ) {
                    if(zone.idzone == "COL"+zoneId){
                        $.each(zone.price.vtype_id, function(vkey, vprice){
                            var frantion = vprice.fraction == "1" ? LANG.minutetxt.text : vprice.fraction+" min";
                            dataResponse += '<li data-role="list-divider">'+
                                '<span class="vehicletypetxt">'+LANG["vehicletype"+vkey+"txt"].text+'</span>'+
                                '</li>'+
                                '<li class="text-wrap"><span class="ratetxt bold">'+LANG.ratetxt.text+' '+frantion+'</span>: '+vprice.rate.toLowerCase()+'</li>'+
                                '<li class="text-wrap"><span class="discounttxt bold">'+LANG.discounttxt.text+'</span>: '+vprice.discount.toLowerCase()+'</li>';
                        });
                    }
                });
                $.mobile.loading('hide');

                if(dataResponse.length > 0){
                    dataResponse = rampgap.toStatic(dataResponse);
                    $("#resultGetPricePop").html(dataResponse);
                    $("#resultGetPricePop").listview('refresh');
                    $("#popPrice").popup({
                        afterclose: function( event, ui ) {
                            // Fix display popup in android  whe this has ui-btn class
                            $("#popPrice").popup('destroy');
                        }
                    });
                    $("#popPrice").popup('open');
                } else {
                    rampgap.alert(LANG.nopriceavailabletxt.text);
                    return false;
                }

            }
        },
        error: function(response, textStatus, errorThrown) {
            $.mobile.loading('hide');
            console.log("Error Get Price "+response+textStatus+errorThrown+url);
            rampgap.alert(LANG.nopriceavailabletxt.text);
        }
    });
}

// ------------------------------------------------------------------------------ /ZONES ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------ /DRVER ------------------------------------------------------------------------------------------
var driverPosition;

function initDriverPage() {

    $('.driverPickmeup_header .ui-input-text').width($('.origAddress .ui-input-text').width() - $('.btn-driverLocation').width() - $('.btn-driverCheck').width() - 2);
    driverPosition = {
        origen: {
            type: '',
            place: '',
            input: '#driverAdrressPick'
        },
        destino: {
            type: 'address',
            place: '',
            input: '#driverAdrressPickDest'
        }
    };

    initialize();
    getCountries();
    getUserInfo('driver');
}

function sendInputDriver() {
    var address,
        addressDest;

    if ($('#driverAdrressPick').val() != '') {
        address = $('#driverAdrressPick').val();
    } else {
        address = myData.city_name + ',' + countryData[myData.country];
    }
    addressDest = $('#driverAdrressPickDest').val();

    console.log(address);
    var updatePosition = {
        origen: {
            type: 'address',
            place: address
        },
        destino: {
            place: addressDest
        }
    };
    $.extend(true, driverPosition, updatePosition);

    initializeMapDriver('img/flag.png', driverPosition);
}

function locateDriver() {
    console.log('locate driver');
    getPosition('locateDriverCoords');
}

function locateDriverCoords(position) {
    console.log(position.coords.latitude);
    console.log(position.coords.longitude);
    var clat = position.coords.latitude;
    var clong = position.coords.longitude;
    rampgap.setCurrLatitude(clat);
    rampgap.setCurrLongitude(clong);

    var addressDest = $('#driverAdrressPickDest').val();

    var updatePosition = {
        origen: {
            type: 'location',
            place: {
                lat: clat,
                lng: clong
            }
        },
        destino: {
            place: addressDest
        }
    };
    $.extend(true, driverPosition, updatePosition);

    initializeMapDriver('img/flag.png', driverPosition);
};
// ------------------------------------------------------------------------------ /DRIVER ------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------ PARKING ------------------------------------------------------------------------------------------

function startParking(plate, num, idp, sms) {
    /*
     SUCCES
     {"code":0,"description":"OK","obj":{"totalMins":0,"maxMins":30,"minuteValue":"0.03","msisdn":"3006658900","startTime":"2011-11-08 02:36:06","endTime":"2011-11-08 02:36:06","idPlace":"01","vehiclePlate":"BJJ118","total":"0.00","userBalance":"204.52","balance":"204.52","minValue":"0.03"}}

     PARQUEADO
     {"code":-113,"description":"Error"}

     VEHICULO NO EXISTE
     {"code":-121,"description":"Error"}

     ZONA NO EXISTE
     {"code":-106,"description":"Error"}

     */


    plate = plate.replace(' ', '');
    $.mobile.loading('show');
    $.ajax({
        data: 'op=24&user=' + myData.login + '&pass=' + myData.pass + '&plate=' + plate + '&num=' + num + '&idc=' + myData.country + '&idp=' + idp + '&idu=' + myData.idu + '&sms=' + sms,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {

                $.mobile.loading('hide');
                if (response.code === 0) {
                    //NT - Agregando ActiveParking para procesamiento de Notificaciones
                    rampgap.addActiveParking(
                        response.obj.vehiclePlate,
                        response.obj.minuteValue,
                        response.obj.maxMins
                    );
                    var parkDoneResult = '<div cellspacing="0" cellpadding="0">';
                    parkDoneResult += '<p class="title">' + LANG.parkinginfotxt.text + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.vehicletxt.text + ': </strong>' + response.obj.vehiclePlate + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.starttimetxt.text + ': </strong>' + response.obj.startTime + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.placetxt.text + ': </strong>' + response.obj.idPlace + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.balancetxt.text + ': </strong>CRC ' + response.obj.userBalance + '</p>';
                    parkDoneResult += '<p><strong>' + LANG.minutepricetxt.text + ': </strong>CRC ' + response.obj.minuteValue + '</p>';
                    parkDoneResult += '</div>';
                    //<a data-inline="true" data-role="button" href="#" data-theme="c" class="ui-btn ui-btn-inline ui-btn-corner-all ui-shadow ui-btn-up-c Home"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text hometxt">Home</span></span></a>
                    //$('#parkingDoneInfo').html(parkDoneResult);
                    //$.mobile.changePage($("#parkingDone"));
                    //$( "body" ).pagecontainer( "change", "#parkingDone", { transition: "slide" });
                    //rampgap.confirmDialog(parkDoneResult,
                    //                     "javascript:confirmParking('"+plate+"','"+num+ "','"+ idp+"','"+sms+ "')");
                    navigator.notification.confirm(
                        parkDoneResult,
                        function(buttonIndex) {
                            confirmParking(plate, num, idp, sms);
                        },
                        LANG.alertconfirmgeneraltitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
                    );

                    /*$('.Home').click(function(){
                     //$.mobile.changePage($("#menu"));
                     $( "body" ).pagecontainer( "change", "#menu", { transition: "slide" });

                     });*/
                }
                if (response.code == -113) {
                    generalAlert(LANG.alertalreadyparked.text);
                    return false;
                } else if (response.code == -121) {
                    generalAlert(LANG.alertvehiclenoregistered.text);
                    return false;
                } else if (response.code == -120) {
                    generalAlert(LANG.alertplaceclosed.text);
                    return false;
                } else if (response.code == -106) {
                    generalAlert(LANG.alertplacewrong.text);
                    return false;
                } else if (response.code == -127) {
                    generalAlert(LANG.alertplacenotexist.text);
                    return false;
                } else if (response.code == -128) {
                    generalAlert(LANG.alertplacenot.text);
                    return false;
                } else if (response.code == -103) {
                    generalAlert(LANG.alertnobalance.text);
                    return false;
                }
            }
        }
    });
}


// ------------------------------------------------------------------------------ VEHICLES ------------------------------------------------------------------------------------------

function myVehicles() {
    /*
     http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
     op = 4
     SUCCES
     [{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

     */
    myCars = '';
    var resultCars = '';
    $.mobile.loading('show');
    $.ajax({
        data: 'op=15&user=' + myData.login + '&pass=' + myData.pass,
        success: function(response) {
            if (response == null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    generalAlert(LANG.alertnocars.text);
                    $.mobile.loading('hide');
                    $("body").pagecontainer("change", "#vehicles", {
                        transition: "slide"
                    });
                    return false;
                } else {
                    for (var i = 0; i < response.length; i++) {
                        myCars[i] = new Array(response[i].plate, response[i].dateReg, response[i].owner);
                        //resultCars += '<li><span>'+response[i].plate+' - '+response[i].dateReg+'</span><a href="#" rel="'+response[i].plate+'" class="deleteItem">Delete</a></li>';

                        resultCars += '<li data-theme="a" class="ui-first-child ui-last-child">';
                        if (response[i].owner == '1' || response[i].owner == 1) {
                            resultCars += '    <a href="#" class="ui-btn ui-btn-icon-right ui-icon-delete deleteItem"  rel="' + response[i].plate + '" >';
                        } else {
                            resultCars += '    <a href="#" class="ui-btn">';

                        }
                        resultCars += '     <h3 class="ui-li-heading">' + response[i].plate + '</h3>';
                        resultCars += ' </a>';


                    }

                    //Fix error append wp8.1
                    resultCars = rampgap.toStatic(resultCars);

                    $('#vehiclesList').html(resultCars);
                    $.mobile.loading('hide');
                    //$.mobile.changePage($("#viewVehicles"));
                    $("body").pagecontainer("change", "#viewVehicles", {
                        transition: "slide"
                    });


                    $('.deleteItem').click(function() {
                        deleteCar($(this).attr('rel'));
                    });
                }
            }
        }
    });
}

function loadVehicles() {
    /*
     http://96.45.176.18:8080/CAN-CPS-MOB/MobileServer?op=15&idu=ramp&pass=12345678
     op = 4
     SUCCES
     [{"plate":"BJJ118","dateReg":"2011-07-17","owner":true},{"plate":"PPP002","dateReg":"2011-07-23","owner":true},{"plate":"KJJ493","dateReg":"2011-08-12","owner":true}]

     */
    var cars = '';
    $.mobile.loading('show');
    $.ajax({
        data: 'op=15&user=' + myData.login + '&pass=' + myData.pass,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response == "") {
                    generalAlert(LANG.alertnocars.text);
                    $.mobile.loading('hide');
                    return false;
                } else {
                    for (var i = 0; i < response.length; i++) {
                        if (i === 0) {
                            cars += '<option value="0" selected="selected">---</option> ';
                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                        } else {
                            cars += '<option value="' + response[i].plate + '">' + response[i].plate + '</option> ';
                        }
                    }
                    //Fix error append wp8.1
                    cars = rampgap.toStatic(cars);

                    $('#startParking').find('form').find('span:first').html(LANG.selectplatetxt.text);
                    $('#carPLate').html('');
                    $('#carPLate').html(cars);
                    $.mobile.loading('hide');
                    //$.mobile.changePage($("#startParking"));
                    $("body").pagecontainer("change", "#startParking", {
                        transition: "slide"
                    });

                }
            }
        }
    });
}


function deleteCar(plate) {
    /*
     op = 17
     idu, plate

     SUCCES
     {"code":-126,"description":"OK"}
     */
    //rampgap.confirmDialog(LANG.alertconfirmcardelete.text, "javascript:_deleteCar('"+plate+"')");
    navigator.notification.confirm(
        LANG.alertconfirmcardelete.text,
        function(buttonIndex) {
            _deleteCar(buttonIndex, plate);
        },
        LANG.alertconfirmcardeletetitle.text, [LANG.btnOk.text, LANG.btnCancel.text]
    );

}

function _deleteCar(buttonIndex, plate) {
    if (buttonIndex === 2) {
        return false;
    }
    $.ajax({
        data: 'op=17&user=' + myData.login + '&pass=' + myData.pass + '&plate=' + plate,
        success: function(response) {
            if (response === null) {
                generalAlert(LANG.alertsorry.text);
            } else {
                myVehicles();
            }
        }
    });
}

function addVehicle(plate) {
    /*
     op = 12
     idu, pass, plate
     IDU = LOGIN // OJO CORREGIR
     SUCCES
     {"code":-100,"description":"OK"}

     FAILED
     {"code":1,"description":"Failed"}

     */
    plate = plate.replace(' ', '');
    $("#btnAddCar").addClass("ui-disabled");
    $.mobile.loading('show');
    $.ajax({
        data: 'op=12&user=' + myData.login + '&pass=' + myData.pass + '&plate=' + plate,
        success: function(response) {
            $("#btnAddCar").removeClass("ui-disabled");
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                if (response.code == -102) {
                    generalAlert(LANG.alertvehicleexist.text);
                    $.mobile.loading('hide');
                    return false;
                } else {
                    if (response.code != -100) {
                        generalAlert(LANG.alertsorry.text);
                        $.mobile.loading('hide');
                        return false;
                    } else {
                        generalAlert(LANG.alertvehicleadded.text);
                        $.mobile.loading('hide');
                        $('#newPlate').val("");
                        myVehicles();
                    }
                }
            }
        },
        error: function(){
            $("#btnAddCar").removeClass("ui-disabled");
        }
    });
}

// ADMIN -----------------------------------------------------------------------

/**
 * @param code - cu or mobile of user
 */
function adminGetBalance(code){
    $.mobile.loading('show');
    //For security re-check if user is admin
    if(myData.usertype == _adminId){
        var xmlhttp = new XMLHttpRequest(),
            wsdlSuccess = false,
            dataResponse = "";

        xmlhttp.open('POST', wsdl, true);

        // build SOAP request
        var sr ='<?xml version="1.0" encoding="utf-8"?>' +
                '<soapenv:Envelope ' +
                'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
                'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
                'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
                '<soapenv:Body>' +
                '<urn:consultarSaldo soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
                '<token xsi:type="xsd:string">'+wsdlToken+'</token>' +
                '<codigoCPS xsi:type="xsd:string">'+code+'</codigoCPS>' +
                '</urn:consultarSaldo>' +
                '</soapenv:Body>' +
                '</soapenv:Envelope>';

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                $.mobile.loading('hide');
                wsdlSuccess = true;

                var response = $.parseXML(xmlhttp.responseText),
                    getBSaldo = $(response).find("saldo").text(),
                    getBLogin = $(response).find("login").text(),
                    getBCu = $(response).find("cu").text(),
                    getBName = $(response).find("nombre").text(),
                    getBLast = $(response).find("apellido").text(),
                    getBPhone = $(response).find("telefono").text(),
                    getBEmail = $(response).find("email").text(),
                    codigoRespuesta = $(response).find("codigoRespuesta").text();

                switch(codigoRespuesta){
                case '11':
                    rampgap.alert(LANG.alertnoexistcumobile.text);
                    rampgap.clearInput(_currentPage);
                    break;
                case '12':
                case '13':
                    getBSaldo = getBSaldo === "" ? 0 : getBSaldo;
                    dataResponse = '<li data-role="list-divider">'+
                        '<span class="balancetxt">'+LANG.balancetxt.text+'</span>: <span class="data-balancce">'+getBSaldo+'</span>'+
                        '</li>'+
                        '<li><span class="cuxt bold">CU</span>: '+getBCu+'</li>'+
                        '<li><span class="loginUs bold">'+LANG.loginUs.text+'</span>: '+getBLogin+'</li>'+
                        '<li><span class="name bold">'+LANG.name.text+'</span>: '+getBName+'</li>'+
                        '<li><span class="lastName bold">'+LANG.lastName.text+'</span>: '+getBLast+'</li>'+
                        '<li><span class="mobile bold">'+LANG.mobile.text+'</span>: '+getBPhone+'</li>'+
                        '<li><span class="email bold">'+LANG.email.text+'</span>: '+getBEmail+'</li>';

                    $("#resultGetBalance").html(dataResponse);
                    $("#resultGetBalance").listview('refresh');
                    rampgap.clearInput(_currentPage);
                    break;
                case '51':
                case '52':
                case '53':
                case '54':
                    rampgap.alert(LANG.alertgeneralerror.text);
                    console.log("Response Code:"+$(response).find("codigoRespuesta").text());
                    break;
                }
            } else if (xmlhttp.status == 404){
                rampgap.alert(LANG.alertgeneralerror.text);
                console.log("Error Soap:"+xmlhttp.status);
                return false;
            }

        };
        // Send the POST request
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send(sr);
    }
}

//Admin actions
$(document).on('tap', "#btnAdminPage", function(e){
    if(_isAdmin){
        $(":mobile-pagecontainer").pagecontainer("change", "#adminPage", {
            transition: "slide"
        });
    }
});
$(document).on('tap', "#btnAdminGetBalance", function(){
    $.mobile.loading('show');
    var getBalenceInput = $("#cuMobileInput").val();
    if(getBalenceInput === ""){
        rampgap.alert(LANG.alertcumobile.text);
        $.mobile.loading('hide');
        return false;
    } else {
        adminGetBalance(getBalenceInput);
    }
});

// USER ACCOUNT -----------------------------------------------------------------


function viewAccountInfo() {
    /*
     OP=2 & USER

     SUCCES
     {"idsysUser":"3","address":"Kra 41G #47-12","balance":57.9,"birthDate":"Nov 19, 1984","countryIdcountry":"1","cu":"1G5698A","email":"rmesino@gmail.com","idsysUserType":2,"lastName":"Mesino Perdomo","login":"ramp","name":"Robinson Andres","pass":"12345678","favoriteMsisdn":"3006680286","city_idcity":{"idcity":"1","idcountry":"1","name":"Toronto"}}
     */
    $.mobile.loading('show');
    $.ajax({
        data: 'op=2&user=' + myData.login + '&pass=' + myData.pass,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                //Update Array myData
                myData.country = response.countryIdcountry;
                myData.city = response.city_idcity.idcity;
                myData.city_name = response.city_idcity.name;
                myData.msisdn = response.favoriteMsisdn;
                myData.idu = response.idsysUser;
                myData.address = response.address;
                myData.balance = response.balance;
                myData.cu = response.cu;
                myData.email = response.email;
                myData.usertype = response.idsysUserType;
                myData.name = response.name;
                myData.lastname = response.lastName;

                //User Data
                var d = new Date(response.birthDate);
                var year = d.getFullYear();
                var monthFormat = d.getMonth() + 1;
                var month = monthFormat < 10 ? "0" + monthFormat : monthFormat;
                var date = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();

                var formatted = year + "-" + month + "-" + date;

                $('#infoCU').val(response.cu);
                $('#infoLogin').val(response.login);
                $('#infoName').val(response.name);
                $('#infoLast').val(response.lastName);
                $('#infoAddress').val(response.address);
                $('#infoBirthDate').val(formatted);
                $('#infoMobile').val(response.favoriteMsisdn);
                $('#infoEmail').val(response.email);

                getCitiesRegister(response.city_idcity.idcountry, response.city_idcity.idcity);

                $('#infoCountry [value="' + response.city_idcity.idcountry + '"]').prop('selected', true).change();

                //generalAlert(LANG.alertregisterinfo.text);

                $.mobile.loading('hide');
                $("body").pagecontainer("change", "#viewAccountInfo", {
                    transition: "slide"
                });

            }
        }
    });
}

function getCountries() {
    /*
     SUCCFES
     [{"idcountry":"1","countryPrefix":"CAN","name":"Canada","latitude":"12365478","longitude":"12365445","lang":"en"}]
     */
    var today = new Date();

    countryGet = window.localStorage.getItem('countryGet');

    $.mobile.loading('show');
    if(!countryGet || today > new Date(countryGet)){

        console.log('GET country');

        $.ajax({
            data: 'op=3',
            success: function(response) {
                console.log(response);
                if (response === null) {
                    $.mobile.loading('hide');
                    return false;
                } else {

                    var dateSave = new Date();
                    dateSave.setDate(dateSave.getDate() + 15);

                    window.localStorage.setItem('countryGet', dateSave);
                    window.localStorage.setItem('countryData', JSON.stringify(response));

                    countryData = JSON.parse(window.localStorage.getItem('countryData'));
                    pushCountryList(countryData);
                    console.log(countryData);
                }
            }
        });

    } else {
        countryData = JSON.parse(window.localStorage.getItem('countryData'));
        pushCountryList(countryData);
        console.log(countryData);
    }



}

/**
 *
 * @param countryData - append country data in select list
 */
function pushCountryList(countryData){
    var countries = "";
    for (var i = 0; i < countryData.length; i++) {

        if (i === 0) {
            countries += '<option value="0" selected="selected">---</option> ';
            countries += '<option value="' + countryData[i].idcountry + '">' + countryData[i].name + '</option> ';
        } else {
            countries += '<option value="' + countryData[i].idcountry + '">' + countryData[i].name + '</option> ';
        }

    }

    //Fix error append wp8.1
    countries = rampgap.toStatic(countries);

    $('#infoCountry,#registerCountry').html(countries);
    if(myData.country){
        $('#infoCountry [value="' + myData.country + '"]').prop('selected', true).change();
    }
    $.mobile.loading('hide');
}

function getCitiesRegister(countryCode, cityCode) {
    if (countryCode === 0)
        return false;

    $.mobile.loading('show');
    var cities = '';
    $.ajax({
        data: 'op=8&idc=' + countryCode,
        success: function(response) {
            if (response === null) {
                $.mobile.loading('hide');
                return false;
            } else {
                for (var i = 0; i < response.length; i++) {
                    if (i === 0) {
                        cities += '<option value="0" selected="selected">---</option> ';
                        cities += '<option value="' + response[i].idcity + '">' + response[i].name + '</option> ';
                    } else {
                        cities += '<option value="' + response[i].idcity + '">' + response[i].name + '</option> ';
                    }
                }
                $.mobile.loading('hide');

                //Fix error append wp8.1
                cities = rampgap.toStatic(cities);

                $('#registerCity,#infoCity').html(cities);

                if (cityCode !== 0 && cityCode != undefined && cityCode != 'undefined') {
                    $('#infoCity [value="' + cityCode + '"]').prop('selected', true).change();
                }
            }

        }
    });
}

function saveUserUpdate(buttonIndex) {

    if (buttonIndex == 2) {
        return false;
    }
    /*
     OP=10
     name = request.getParameter("name");
     last_name = request.getParameter("last_name");
     address = request.getParameter("address");
     email = request.getParameter("email");
     birthdate = request.getParameter("bd");
     country = request.getParameter("country");
     msisdn = request.getParameter("msisdn");
     city = request.getParameter("city");

     SUCCES
     */

    $.mobile.loading('show');

    var address = $("#infoAddress").val();
    var email = $("#infoEmail").val();
    var bd = $("#infoBirthDate").val(); // (yyyy-MM-dd)
    var country = $("#infoCountry").val();
    myData.country = country; //ajustes v1.0.1
    var msisdn = $("#infoMobile").val();
    var city = $("#infoCity").val();
    var name = $("#infoName").val();
    var last_name = $("#infoLast").val();

    var checkInput = true;
    var selectorInput = '#userInfoForm input[type=text],#userInfoForm input[type=tel],#userInfoForm input[type=email], #userInfoForm input[type=password]';

    $(selectorInput).removeClass('formError');
    $('#userInfoForm select').parent().removeClass('formError');

    $(selectorInput).each(function() {
        if ($(this).val().length <= 2) {
            console.log($(this).attr('placeholder'));
            $('#' + $(this).attr('id')).addClass('formError');
            checkInput = false;
        }
    });

    if (checkInput === false) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertfields.text);
        return false;
    }

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; //ajustes v1.0.1
    if (!filter.test(email)) {
        $.mobile.loading('hide');
        generalAlert(LANG.alerttypeemail.text);
        $("#infoEmail").addClass('formError');
        return false;
    }

    var filterPhone = /^([0-9])+$/;
    if (msisdn.length > 10 || msisdn.length < 10 || !filterPhone.test(msisdn)) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertinvalidphone.text);
        $("#infoMobile").addClass('formError');
        return false;
    }

    if (name.length > 30 || last_name.length > 30 || address.length > 45 || email.length > 45) {
        $.mobile.loading('hide');
        generalAlert(LANG.alertlongfield.text);

        if (name.length > 30) {
            $("#infoName").addClass('formError');
        }
        if (last_name.length > 30) {
            $("#infoLast").addClass('formError');
        }
        if (address.length > 45) {
            $("#infoAddress").addClass('formError');
        }
        if (email.length > 45) {
            $("#infoEmail").addClass('formError');
        }
        return false;
    }

    if (country === 0 || country == "0" || country == "") {
        $.mobile.loading('hide');
        generalAlert(LANG.alertcountryfield.text);
        $('#infoCountry').parent().addClass('formError');
        return false;
    }
    if (city === 0 || city == "0" || city == "") {
        $.mobile.loading('hide');
        generalAlert(LANG.alertcityfield.text);
        $('#infoCity').parent().addClass('formError');
        return false;
    }

    $("#btnSaveAccount").addClass("ui-disabled");
    var dataString = '&name=' + name + '&last_name=' + last_name + '&address=' + address + '&email=' + email + '&bd=' + bd + '&country=' + country + '&msisdn=' + msisdn + '&city=' + city;
    //var dataString = '&name=Camilo&last_name=Prueba&address=Calle siempre viva 123&email=cquimbayo@cpsparking.ca&bd=2014-08-26&country=472&msisdn=3178795959&city=103';

    $.ajax({
        data: 'op=10&user=' + myData.login + '&pass=' + myData.pass + dataString,
        success: function(response) {
            console.log(JSON.stringify(response));
            $("#btnSaveAccount").removeClass("ui-disabled");
            if (response === null) {
                $.mobile.loading('hide');
                generalAlert(LANG.alertcheckdata.text);
                return false;
            } else {
                $.mobile.loading('hide');
                generalAlert(LANG.alertregisterok.text);
                $('#btnCancelEdit').trigger('click');
                viewAccountInfo();
            }
        },
        error: function(){
            $("#btnSaveAccount").removeClass("ui-disabled");
        }

    });
}


function savePassword(pass, oldPass, newPass) {
    $.mobile.loading('show');
    $.ajax({
        data: 'op=11&user=' + myData.login + '&pass=' + encrypt(pass) + '&oldpass=' + encrypt(oldPass) + '&npass=' + encrypt(newPass),
        success: function(response) {
            if (response === null) {
                generalAlert(LANG.alertcheckpassword.text);
                $.mobile.loading('hide');
                return false;
            } else {
                if (response.code == -117) {
                    myData.pass = encrypt(newPass);

                    rampgap.setItem("pass", encrypt(newPass));
                    window.localStorage.setItem("pass", encrypt(newPass));

                    generalAlert(LANG.alertpasschangeok.text);
                    $.mobile.loading('hide');

                    $("body").pagecontainer("change", "#userAccount", {
                        transition: "slide"
                    });

                }
            }
        }
    });
}

//---------------------------------------------------------------------------//

function getPosition(onSucces) {
    $.mobile.loading('show');
    _enableAccuracy = false;

    if (!navigator.geolocation) {
        navigator.notification.alert(
            LANG.alertgeonotsupported.text,
            alertDismissed,
            LANG.alerttitle.text,
            LANG.btnOk.text
        );
        $.mobile.loading('hide');
        return;
    } else {
        if(_getPlatform.platformId == "3"){
            _networkInfo = Windows.Networking.Connectivity.NetworkInformation.getInternetConnectionProfile();
            if(_networkInfo.isWlanConnectionProfile == false){
                _enableAccuracy = true;
            }
        } else {
            _networkInfo = navigator.network.connection.type;
        }
        navigator.geolocation.getCurrentPosition(eval(onSucces), function(error) {
            onErrorZone(error, onSucces);
        }, {
            timeout: 27000,
            enableHighAccuracy: _enableAccuracy
        });
    }
}

function onErrorZone(error, onSucces) {
    console.log(JSON.stringify(error));

    navigator.geolocation.getCurrentPosition(eval(onSucces), onErrorZone_fallback, {
        timeout: 27000,
        enableHighAccuracy:  _enableAccuracy == false ? true : false
    });
}

function onErrorZone_fallback(error) {
    var msgError = LANG.alertsorry.text;
    switch(parseInt(error.code)){
    case 1:
        msgError = LANG.alertgeopermission.text;
        break;
    case 2:
        msgError = LANG.alertgeounavailable.text;
        break;
    case 3:
        msgError = LANG.alertgeotimeout.text;
        break;
    }

    navigator.notification.alert(
        msgError,
        alertDismissed,
        LANG.alerttitle.text,
        LANG.btnOk.text
    );

    console.log(JSON.stringify(error));

    $.mobile.loading('hide');
    return false;
}
