var SPANISH = 0;
var ENGLISH = 1;

var defaultLang = SPANISH;

var LANG_EN = {
    viewmapcar: {clase: "viewmapcar", text: "Find Car"},
    alertnewupdate:{text: "There is an available update, please update your application before continue."},
    alertbirthday: {text: "Please check your birthday"},
    resetPassword:{clase: "resetPassword", text: "Forgot your password?"},
    selectlangtext:{clase: "English"},
    alertcheckemail: {text: "Please check your email"},
    alerttypeemail: {text: "Please type your email"},
    alertinvaliduser:{text: "Please check your user, without special characters or spaces"},
    alertinvalidphone: {text: "Please type a valid mobile number"},
    alertresetsend: {text: "Your password request has been sent"},
    alertmsgempty: {text: "Please write a message"},
    alertcumobile: {text: "Please enter the CU or cell number user"},
    alertnoexistcumobile: {text: "The CU or Cell Number does not exist"},
    alertparkingempty: {text: "Cannot send an empty parking code, please type a valid parking code"},
    alertpayempty: {text: "Cannot send an empty payment code, please type a valid payment code"},
    alertpaycheck: {text: "Cannot send an invalid code."},
    alertcheckcountry: {text: "Please check your Country/City selection"},
    alertfields: {text: "Please fill all fields"},
    alertpasswords: {text: "Passwords must be equals"},
    alertcarplate: {text: "Please enter the car plate"},
    alertpassequals: {text: "Check that the new passwords are equal"},
    alertconfirmmodifyinfo: {text: "Are you sure you want to modify your info?"},
    alertlogout: {text: "Are you sure you want to logout?"},
    alertexit: {text: "Are you sure you want to exit?"},
    alerrtypepin: {text: "Please type a PIN number"},
    alertypevalue: {text: "Please type the value to reload"},
    alertinvalidpin: {text: "Invalid"},
    alertreloadok: {text: "Reload Succesfull"},
    alertcheckcu: {text: "Please check CU and CU Retype"},
    alertamount: {text: "Please type an amount"},
    alertcheckdata: {text: "Please check your data"},
    alertinvalidcu: {text: "Invalid destination unique code (UC), Please check it."},
    alertgiveok: {text: "Give Succesfull"},
    alertusername: {text: "Invalid user or password"},
    alertalreadyregister: {text: "Some of these data have already been registered"},
    alertregisterok: {text: "Register Succesfully"},
    alertnozone: {text: "You are not close to any zone"},
    alertalreadyparked: {text: "This vehicle it's already parked"},
    alertvehiclenoregistered: {text: "This vehicle it's not registered"},
    alertplacewrong: {text: "This place code it's wrong"},
    alertplacenot: {text: "This place it's not available"},
    alertnotparking: {text: "You don't have active parkings"},
    alertnocars: {text: "you don't have registereds cars"},
    alertnobalance: {text: "you don't have balance"},
    alertconfirmcardelete: {text: "Are you sure you want to delete this vehicle?"},
    alertconfirmcardeletetitle: {text: "Delete vehicle"},
    alertsorry: {text: "Sorry, please try again later"},
    alertvehicleexist: {text: "Sorry, this car it's already registered"},
    alertvehicleadded: {text: "Vehicle successfully added"},
    alertcheckpassword: {text: "Please check your typed passwords"},
    alertpasschangeok: {text: "Your password was changed successfully"},
    alertfiveleft: {text: "Just 5 minutes to remove your vehicle"},
    alertoneleft: {text: "Just 1 minute to remove your vehicle"},
    alertvehiclefined: {text: "From this point you will be fined for not removing your vehicle"},
    alertpaidparkingok: {text: "Payment success"},
    alertpaidparkingfail: {text: "Not enough balance to complete operation"},
    alertpaidparkingoffline: {text: "Could not connect to the parking spot. Please try again "},
    alertpaidparkingtokenerror: {text: "Payment token error"},
    alertpaidparkingerror: {text: "Error trying to make parking operation"},
    alertpaidparkingnullerror: {text: "Error due  null field values"},
    alertpaidparkingstoreerror: {text: " Error recording parking operation"},
    alertconfirmpaymentiderror: {text: " Invalid payment code"},
    alerttimeouterror: {text: "Exceeded timeout, please try again"},
    alertnetworkerror: {text: "There is a communication error, please try again"},
    alertnotfounderror: {text: "Your request could not be completed, please try again"},
    alertservererror: {text: "There was an internal error, please try again"},
    alertgeneralerror: {text: "There was an unexpected error, please try again"},
    alertnetworkconnection: {text: "The application requires internet for proper operation, check your connection"},
    alertgeonotsupported: {text: "The geolocation is not active, please turn it on."},
    alertgeopermission: {text: "The geolocation is not active, please turn it on"},
    alertgeounavailable: {text: "Could not get the position, please try again"},
    alertgeotimeout: {text: "Could not get the position, timeout, please try again."},
    alertconfirmpaymenttitle: {text: "Payment Confirmation"},
    alertconfirmgeneraltitle: {text: "CPS PARKING"},
    alertconfirmpayment: {text: "Information:\nParking Id: %s\nPlace id: %s\nBegin time: %s\nEnd time: %s.\nParking amount: $%s" +
                          "\nParking time: %d"},
    alertroutefail: {text: "We could not get the route, please try again"},
    alertconfirmtitle: {text: "CPS PARKING"},
    alerttitle: {text: "CPS PARKING"},

    btnOk: {text: "Ok"},
    btnCancel: {text: "Cancel"},
    backBtn: {clase: "backBtn", text: "Back"},
    edittxt: {clase: "edittxt", text: "Edit"},
    savetxt: {clase: "savetxt", text: "Save"},
    sendtxt: {clase: "sendtxt", text: "Send"},
    continuetxt: {clase: "continuetxt", text: "Continue"},
    ckecksessiontxt: {clase: "ckecksessiontxt", text: "Checking user credentials"},

    /*LOGIN*/
    userName: {clase: "userName", text: "Username"},
    password: {clase: "password", text: "Password"},
    login: {clase: "login", text: "Login"},
    register: {clase: "register", text: "Register"},
    /*END LOGIN*/

    /*REGISTER*/
    name: {clase: "name", text: "Name"},
    lastName: {clase: "lastName", text: "Last Name"},
    mobile: {clase: "mobile", text: "Mobile No."},
    login: {clase: "login", text: "Login"},
    address: {clase: "address", text: "Address"},
    birthday: {clase: "birthday", text: "BirthDay"},
    year: {clase: "year", text: "Year"},
    month: {clase: "month", text: "Month"},
    day: {clase: "day", text: "Day"},
    repassword: {clase: "repassword", text: "Re-Password"},
    email: {clase: "email", text: "E-Mail"},
    country: {clase: "country", text: "Country"},
    city: {clase: "city", text: "City"},
    cancel: {clase: "cancel", text: "Cancel"},
    loginUs: {clase: "loginUs", text: "Login"},
    pageNameRegister: {clase: "pnreg", text: "Register"},

    january:{clase: "january", text: "January"},
    february:{clase: "february", text: "February"},
    march:{clase: "march", text: "March"},
    april:{clase: "april", text: "April"},
    may:{clase: "may", text: "May"},
    june:{clase: "june", text: "June"},
    july:{clase: "july", text: "July"},
    august:{clase: "august", text: "August"},
    september:{clase: "september", text: "September"},
    october:{clase: "october", text: "October"},
    november:{clase: "november", text: "November"},
    december:{clase: "december", text: "December"},

    alertlongfield: {text: "Check the length of the fields"},
    alertcityfield: {text: "Please check your City"},
    alertcountryfield: {text: "Please check your Country"},

    /*END REGISTER*/

    /* MAIN MENU*/
    zonestxt: {clase: "zonestxt", text: "Zones"},
    parkingtxt: {clase: "parkingtxt", text: "Parking"},
    vehiclestxt: {clase: "vehiclestxt", text: "Vehicles"},
    usertxt: {clase: "usertxt", text: "User Account"},
    balancetxt: {clase: "balancetxt", text: "Balance"},
    pnmain: {clase: "pnmain", text: "Main Menu"},
    settingstxt: {clase: "settingstxt", text: "Settings"},
    languagetxt: {clase: "languagetxt", text: "Language"},
    pnsetlang: {clase: "pnsetlang", text: "Set Language"},
    suggestionstxt: {clase: "suggestionstxt", text: "Suggestions"},
    suggestionsend: {text: "Suggestion sent"},
    payparkingtxt: {clase: "payparkingtxt", text: "Pay Parking"},
    logouttxt: {clase: "logouttxt", text: "Logout"},


    /* ZONES*/
    viewzonestxt: {clase: "viewzonestxt", text: "View Zones"},
    locatezonestxt: {clase: "locatezonestxt", text: "Locate Zones"},
    pnzones: {clase: "pnzones", text: "Zones"},
    viewtxt: {clase: "viewtxt", text: "View"},
    canceltxt: {clase: "canceltxt", text: "Cancel"},
    pnviewzones: {clase: "pnviewzones", text: "View Zones"},
    pnzoneresult: {clase: "pnzoneresult", text: "Zones Result"},
    viewdaystxt: {clase: "viewdaystxt", text: "View Days"},
    zoneoptionstxt: {clase: "zoneoptionstxt", text: "Zone Options"},
    pnzonemap: {clase: "pnzonemap", text: "Zone Map"},
    pnzonedays: {clase: "pnzonedays", text: "Zone Days"},
    pnzoneinfo: {clase: "pnzoneinfo", text: "Zone Info"},
    pricetxt: {clase: "price", text: "Price:"},
    placestxt: {clase: "placestxt", text: "Places"},
    viewmaptxt: {clase: "viewmaptxt", text: "View Map"},
    viewpricestxt: {clase: "viewpricestxt", text: "View Prices"},
    maxtimetxt: {clase: "maxtimetxt", text: "Max Time"},
    minutepricetxt: {clase: "minutepricetxt", text: "Minute Price"},
    currenttimetxt: {clase: "currenttimetxt", text: "Current Time"},
    zonetxt: {clase: "zonetxt", text: "Zone"},
    opentimetxt: {clase: "opentimetxt", text: "Open Time"},
    closetimetxt: {clase: "closetimetxt", text: "Close Time"},
    locatezonesalerttxt: {clase: "locatezonesalerttxt", text: "Routes are suggestions, check traffic laws."},
    noshowtxt: {clase: "noshowtxt", text: "Not show again"},

    minutetxt: {clase: "minutetxt", text: "Minute"},
    ratetxt: {clase: "ratetxt", text: "Rate"},
    discounttxt: {clase: "discounttxt", text: "Discount"},
    nopricetxt: {clase: "nopricetxt", text: "See point"},
    nopriceavailabletxt: {clase: "nopriceavailabletxt", text: "No price available, please consult in place."},

    vehicletype1txt: {clase: "vehicletype1txt", text: "Car"},
    vehicletype2txt: {clase: "vehicletype2txt", text: "Moto"},
    vehicletype3txt: {clase: "vehicletype3txt", text: "Bike"},

    /* PARKING*/
    startparkingtxt: {clase: "startparkingtxt", text: "Start Parking"},
    activeparkingtxt: {clase: "activeparkingtxt", text: "Active Parking"},
    pnparking: {clase: "pnparking", text: "Parking"},
    zoneplacetxt: {clase: "zoneplacetxt", text: "Zone Place"},
    smsnotitxt: {clase: "smsnotitxt", text: "SMS Notification"},
    parktxt: {clase: "parktxt", text: "Park"},
    pnstartparking: {clase: "pnstartparking", text: "Start Parking"},
    pnparkingdone: {clase: "pnparkingdone", text: "Parking Done"},
    pnparkingoptions: {clase: "pnparkingoptions", text: "Parking Options"},
    pnendparking: {clase: "pnendparking", text: "End Parking"},
    endparkingtxt: {clase: "endparkingtxt", text: "End Parking"},
    endtimetxt: {clase: "endtimetxt", text: "End Time"},
    totaltimetxt: {clase: "totaltimetxt", text: "Total Time"},
    selectplatetxt: {clase: "selectplatetxt", text: "Select Plate"},
    payparkingcodetxt:{clase:"payparkingcodetxt", text:"Parking code:"},
    paycodetxt:{clase:"paycodetxt", text:"Pay code:"},
    payparkingmsgtxt: {clase:"payparkingmsgtxt", text: "Only pay for occasional service"},

    /* VEHICLES */
    addvechicletxt: {clase: "addvechicletxt", text: "Add Vehicle"},
    viewvehiclestxt: {clase: "viewvehiclestxt", text: "View Vehicles"},
    addtxt: {clase: "addtxt", text: "Add"},
    carplatetxt: {clase: "carplatetxt", text: "Car Plate:"},
    pnaddvehicles: {clase: "pnaddvehicles", text: "Add Vechicles:"},
    vehicletxt: {clase: "vehicletxt", text: "Vehicle"},
    starttimetxt: {clase: "starttimetxt", text: "Start Time"},
    placetxt: {clase: "placetxt", text: "Place"},

    /* USER ACCOUNT*/
    viewinfotxt: {clase: "viewinfotxt", text: "View Info"},
    changepasstxt: {clase: "changepasstxt", text: "Change Password"},
    pnuserinfo: {clase: "pnuserinfo", text: "User Info"},
    oldpasstxt: {clase: "oldpasstxt", text: "Old Password"},
    newpasstxt: {clase: "newpasstxt", text: "New Password"},

    /* BALANCE*/
    reloadbalancetxt: {clase: "reloadbalancetxt", text: "Reload PIN Balance"},
    reloadbalancezptxt: {clase: "reloadbalancezptxt", text: "Reload Balance"},
    viewbalancetxt: {clase: "viewbalancetxt", text: "View Balance"},
    givebalancetxt: {clase: "givebalancetxt", text: "Transfer"},
    paypalreloadtxt: {clase: "paypalreloadtxt", text: "Credit card"},
    pinnumbertxt: {clase: "pinnumbertxt", text: "Pin Number"},
    payvaluetxt: {clase: "payvaluetxt", text: "Amount"},
    reloadtxt: {clase: "reloadtxt", text: "Reload"},
    pnbalanceinfo: {clase: "pnbalanceinfo", text: "Balance Info"},
    amounttxt: {clase: "amounttxt", text: "Amount"},
    transAmounttxt: {clase: "transAmounttxt", text: "Worth transfer"},
    givetxt: {clase: "givetxt", text: "Give"},
    pngivebalance: {clase: "pngivebalance", text: "Transfer"},
    cudestinationtxt: {clase: "cudestinationtxt", text: "Cell number or UC Destination user"},
    recudestinationtxt: {clase: "recudestinationtext", text: "Repeat Cell Number or UC Destination user"},
    alertconfirmrbalnacetitle: {text: "Reload Confirmation"},
    infopricelisttxt: {clase: "infopricelisttxt", text: "Rates are subject to change without notice."},

    menuZones: {clase: "zones", text: "Zones"},
    menuParking: {clase: "park", text: "Parking"},
    alertError: {text: "xx"},

    /* ADMIN */
    supervisortxt: {clase: "supervisortxt", text: "Supervisor"},
    cumobiletxt: {clase: "cumobiletxt", text: "CU/No. Cell"},
    getbalancetxt: {clase: "getbalancetxt", text: "Balance Check"},

    /* SHARE */
    shareMessage: {text: "The best #parking app  City Parking ¡DOWNLOAD HERE! #Android #Iphone #Wp"},
    shareTitle: {text: "CityParking - Cellular Parking Systems"},
    shareLink: {text: "https://www.cpsparking.co/service/faces/downloads.xhtml"}
};

var LANG_ES = {
    viewmapcar: {clase: "viewmapcar", text: "Buscar Carro"},
    alertnewupdate:{text: "Hay una actualización pendiente, para poder continuar usted debe actualizar la aplicación."},
    alertbirthday: {text: "Por favor revisa tu fecha de nacimiento"},
    selectlangtext:{clase: "Español"},
    resetPassword: {clase: "resetPassword", text: "Olvidaste tu clave?"},
    alertcheckemail: {text: "Por favor revisa tu correo"},
    alerttypeemail: {text: "Por favor ingresa tu correo"},
    alertinvaliduser: {text: "Por favor revisa tu usuario que no tenga caracteres especiales o espacios"},
    alertinvalidphone: {text: "Por favor ingresa un numero de celular valido"},
    alertresetsend: {text: "Se ha enviado tu contraseña al correo registrado"},
    alertregisterinfo: {text: "Si desea cambiar información, oprima sobre el botón Editar."},
    alertcumobile: {text: "Por favor ingresa el CU o Numero de celular del usuario"},
    alertnoexistcumobile: {text: "El CU o Nro Celular no existe"},
    alertmsgempty: {text: "Por favor escribe un mensaje"},
    alertparkingempty: {text: "El código de parqueo no puede estar vacio," +
                        " Por favor introduzca un código de parqueo válido"},
    alertpayempty: {text: "El código de pago no puede estar vacio," +
                    " por favor escriba un código válido"},
    alertpaycheck: {text: "Los códigos ingresados son invalidos."},
    alertcheckcountry: {text: "Por favor revisa la ciudad y/o país"},
    alertfields: {text: "Por favor llena todos los campos"},
    alertpasswords: {text: "Las claves deben ser iguales"},
    alertcarplate: {text: "Por favor ingresa la placa"},
    alertpassequals: {text: "Revisa que las claves sean iguales"},
    alertconfirmmodifyinfo: {text: "Deseas modificar tu informacion?"},
    alertlogout: {text: "¿Deseas cerrar sesión?"},
    alertexit: {text: "¿Deseas salir?"},
    alerrtypepin: {text: "Por favor ingresa un numero PIN"},
    alertypevalue: {text: "Por favor digite el valor a recargar"},
    alertinvalidpin: {text: "Pin Invalido"},
    alertreloadok: {text: "Recarga realizada"},
    alertcheckcu: {text: "Por favor revisa el CU y Re-CU"},
    alertamount: {text: "Por favor ingresa una cantidad"},
    alertcheckdata: {text: "Por favor revisa tus datos"},
    alertinvalidcu: {text: "Codigo unico (CU) de destino invalido, por favor verifiquelo."},
    alertgiveok: {text: "Saldo otorgado correctamente"},
    alertusername: {text: "Usuario o pasword invalido"},
    alertalreadyregister: {text: "Algunos de estos datos ya fueron registrados: Usuario, Movil o Correo"},
    alertregisterok: {text: "Registro realizado"},
    alertnozone: {text: "No estas cerca de ninguna zona"},
    alertalreadyparked: {text: "Este vehículo ya fue parqueado"},
    alertvehiclenoregistered: {text: "Este vehículo no esta registrado"},
    alertplacewrong: {text: "Placa invalida"},
    alertplacenot: {text: "Este lugar no esta disponible"},
    alertplaceclosed: {text: "Zona cerrada por Horario"},
    alertnotparking: {text: "No tienes parqueos activos"},
    alertnocars: {text: "No tienes vehículos registrados"},
    alertnobalance: {text: "No tienes saldo suficiente"},
    alertconfirmcardelete: {text: "Deseas borrar este vehículo?"},
    alertconfirmcardeletetitle: {text: "Borrar vehículo"},
    alertsorry: {text: "Lo sentimos, intenta nuevamente"},
    alertvehicleexist: {text: "Este vehículo ya existe"},
    alertvehicleadded: {text: "Vehículo agregado"},
    alertcheckpassword: {text: "Por favor revisa tu clave"},
    alertpasschangeok: {text: "Clave actualizada"},
    alertfiveleft: {text: "Te quedan 5 minutos para retirar tu vehículo"},
    alertoneleft: {text: "Te quedan 1 minuto para retirar tu vehículo"},
    alertvehiclefined: {text: "Tu vehículo recibira una multa por no retirarlo"},
    alertpaidparkingok: {text: "El pago ha sido reportado satisfactoriamente"},
    alertpaidparkingfail: {text: "Error, no hay suficiente saldo para completar la operación"},
    alertpaidparkingoffline: {text: "No se pudo establecer conexión con el punto de parqueo. Favor intente nuevamente"},
    alertpaidparkingtokenerror: {text: "Error en el token de pago"},
    alertpaidparkingerror: {text: "Ha ocurrido un error en la solicitud del servicio"},
    alertpaidparkingnullerror: {text: "Error debido a valores nulos en los campos"},
    alertpaidparkingstoreerror: {text: " Error guardando la operación de parqueo"},
    alertconfirmpaymentiderror: {text: "Código de pago invalido"},
    alerttimeouterror: {text: "Supero el tiempo de espera, por favor intente de nuevo"},
    alertnetworkerror: {text: "Ocurrido un error en la solicitud, por favor intente de nuevo"},
    alertnotfounderror: {text: "Su solicitud no se pudo completar, por favor intente de nuevo"},
    alertservererror: {text: "Hubo un error interno, por favor intente de nuevo"},
    alertgeneralerror: {text: "Hubo un error inesperado, por favor intente de nuevo"},
    alertnetworkconnection: {text: "La aplicación necesita internet para su correcto funcionamiento, revise su conexión"},
    alertgeonotsupported: {text: "La Geolocalización no está activa"},
    alertgeopermission: {text: "La geolocalización no está activa, por favor enciendela"},
    alertgeounavailable: {text: "No se pudo obtener la posición, intenta de nuevo"},
    alertgeotimeout: {text: "No se pudo obtener la posición, tiempo terminado, por favor intente de nuevo."},
    alertconfirmpaymenttitle: {text: "Confirmar Pago"},
    alertconfirmgeneraltitle: {text: "CPS PARKING"},
    alertconfirmpayment: {text: "Información de parqueo:\nCod. Pago: %s\nCod. Parqueadero: %s" +
                          "\nInicio de parqueo: %s\nFin de parqueo: %s.\nValor: $%s" +
                          "\nMinutos de parqueo: %d"},
    alertroutefail: {text: "No pudimos obtener la ruta, por favor intenta de nuevo"},
    alertconfirmtitle: {text: "CPS PARKING"},
    alerttitle: {text: "CPS PARKING"},

    btnOk: {text: "Ok"},
    btnCancel: {text: "Cancelar"},
    backBtn: {clase: "backBtn", text: "Atras"},
    edittxt: {clase: "edittxt", text: "Editar"},
    savetxt: {clase: "savetxt", text: "Guardar"},
    sendtxt: {clase: "sendtxt", text: "Enviar"},
    continuetxt: {clase: "continuetxt", text: "Continuar"},
    ckecksessiontxt: {clase: "ckecksessiontxt", text: "Comprobando credenciales de usuario"},

    /*LOGIN*/
    userName: {clase: "userName", text: "Usuario"},
    password: {clase: "password", text: "Clave"},
    login: {clase: "login", text: "Iniciar"},
    ingresar: {clase: "ingresar", text: "Iniciar"},
    register: {clase: "register", text: "Registro"},
    /*END LOGIN*/

    /*REGISTRO*/
    name: {clase: "name", text: "Nombres"},
    lastName: {clase: "lastName", text: "Apellidos"},
    mobile: {clase: "mobile", text: "Movil"},
    address: {clase: "address", text: "Direccion"},
    birthday: {clase: "birthday", text: "Fecha Nacimiento"},
    year: {clase: "year", text: "Año"},
    month: {clase: "month", text: "Mes"},
    day: {clase: "day", text: "Dia"},
    repassword: {clase: "repassword", text: "Reingresa Clave"},
    email: {clase: "email", text: "Correo"},
    country: {clase: "country", text: "País"},
    city: {clase: "city", text: "Ciudad"},
    cancel: {clase: "cancel", text: "Cancelar"},
    loginUs: {clase: "loginUs", text: "Usuario"},
    pageNameRegister: {clase: "pnreg", text: "Registro"},
    notificationMail: {clase: "notificationMail", text: "Desea recibir mas informacion sobre los productos y servicios de City Parking S.A.S y sus empresas asociadas y aliadas."},

    january:{clase: "january", text: "Enero"},
    february:{clase: "february", text: "Febrero"},
    march:{clase: "march", text: "Marzo"},
    april:{clase: "april", text: "Abril"},
    may:{clase: "may", text: "Mayo"},
    june:{clase: "june", text: "Junio"},
    july:{clase: "july", text: "Julio"},
    august:{clase: "august", text: "Agosto"},
    september:{clase: "september", text: "Septiembre"},
    october:{clase: "october", text: "Octubre"},
    november:{clase: "november", text: "Noviembre"},
    december:{clase: "december", text: "Diciembre"},

    alertlongfield: {text: "Compruebe la longitud de los campos"},
    alertcountryfield: {text: "Por favor revisa el País"},
    alertcityfield: {text: "Por favor revisa la Ciudad"},

    /*END REGISTER*/

    /* MENU PRINCIPAL*/
    zonestxt: {clase: "zonestxt", text: "Parqueaderos"},
    parkingtxt: {clase: "parkingtxt", text: "Parqueo"},
    vehiclestxt: {clase: "vehiclestxt", text: "Vehículos"},
    usertxt: {clase: "usertxt", text: "Cuenta de Usuario"},
    pnmain: {clase: "pnmain", text: "Menu"},
    settingstxt: {clase: "settingstxt", text: "Configuración"},
    languagetxt: {clase: "languagetxt", text: "Idioma"},
    pnsetlang: {clase: "pnsetlang", text: "Cambiar Idioma"},
    suggestionstxt: {clase: "suggestionstxt", text: "Sugerencias"},
    suggestionsend: {text: "Sugerencia enviada"},
    payparkingtxt: {clase: "payparkingtxt", text: "Pagar Parqueo"},


    /* ZONAS*/
    viewzonestxt: {clase: "viewzonestxt", text: "Ver Parqueaderos"},
    locatezonestxt: {clase: "locatezonestxt", text: "Localizar Parqueadero"},
    pnzones: {clase: "pnzones", text: "Parqueaderos"},
    viewtxt: {clase: "viewtxt", text: "Ver"},
    canceltxt: {clase: "canceltxt", text: "Cancelar"},
    pnviewzones: {clase: "pnviewzones", text: "Ver Parqueaderos"},
    pnzoneresult: {clase: "pnzoneresult", text: "Zonas"},
    viewdaystxt: {clase: "viewdaystxt", text: "Ver Horarios"},
    zoneoptionstxt: {clase: "zoneoptionstxt", text: "Opciones de Zona"},
    pnzonemap: {clase: "pnzonemap", text: "Mapa de Zona"},
    pnzonedays: {clase: "pnzonedays", text: "Horarios"},
    pnzoneinfo: {clase: "pnzoneinfo", text: "Info Zona"},
    pricetxt: {clase: "price", text: "Precio:"},
    placestxt: {clase: "placestxt", text: "Cupos Disponibles"},
    viewmaptxt: {clase: "viewmaptxt", text: "Ver Mapa"},
    viewpricestxt: {clase: "viewpricestxt", text: "Ver Precios"},
    maxtimetxt: {clase: "maxtimetxt", text: "Tiempo Maximo"},
    minutepricetxt: {clase: "minutepricetxt", text: "Precio Minuto"},
    currenttimetxt: {clase: "currenttimetxt", text: "Tiempo Actual"},
    zonetxt: {clase: "zonetxt", text: "Zona"},
    opentimetxt: {clase: "opentimetxt", text: "Apertura"},
    closetimetxt: {clase: "closetimetxt", text: "Cierre"},
    sundaytxt: {clase: "sundaytxt", text: "Domingo"},
    mondaytxt: {clase: "mondaytxt", text: "Lunes"},
    tuesdaytxt: {clase: "tuesdaytxt", text: "Martes"},
    wednesdaytxt: {clase: "wednesdaytxt", text: "Miercoles"},
    thursdaytxt: {clase: "thursdaytxt", text: "Jueves"},
    fridaytxt: {clase: "fridaytxt", text: "Viernes"},
    saturdaytxt: {clase: "saturdaytxt", text: "Sabado"},
    hometxt: {clase: "hometxt", text: "Volver"},
    locatezonesalerttxt: {clase: "locatezonesalerttxt", text: "Las rutas son sugerencias, verifica las normas de transito."},
    noshowtxt: {clase: "noshowtxt", text: "No volver a mostrar"},

    minutetxt: {clase: "minutetxt", text: "Minuto"},
    ratetxt: {clase: "ratetxt", text: "Tarifa"},
    discounttxt: {clase: "discounttxt", text: "Plena"},
    nopricetxt: {clase: "nopricetxt", text: "Consultar en punto"},
    nopriceavailabletxt: {clase: "nopriceavailabletxt", text: "No hay precio disponible, por favor consultar en punto."},

    vehicletype1txt: {clase: "vehicletype1txt", text: "Carro"},
    vehicletype2txt: {clase: "vehicletype2txt", text: "Moto"},
    vehicletype3txt: {clase: "vehicletype3txt", text: "Bicicleta"},


    /* PARQUEO*/
    startparkingtxt: {clase: "startparkingtxt", text: "Parquear"},
    activeparkingtxt: {clase: "activeparkingtxt", text: "Parqueos Activos"},
    pnparking: {clase: "pnparking", text: "Parqueo"},
    zoneplacetxt: {clase: "zoneplacetxt", text: "Lugar/Espacio"},
    smsnotitxt: {clase: "smsnotitxt", text: "Notificacion SMS"},
    parktxt: {clase: "parktxt", text: "Parquear"},
    pnstartparking: {clase: "pnstartparking", text: "Parquear"},
    pnparkingdone: {clase: "pnparkingdone", text: "Parqueo Hecho"},
    pnparkingoptions: {clase: "pnparkingoptions", text: "Opciones de Parqueo"},
    pnendparking: {clase: "pnendparking", text: "Fin de Parqueo"},
    endparkingtxt: {clase: "endparkingtxt", text: "Fin Parqueo"},
    endtimetxt: {clase: "endtimetxt", text: "Hora Final"},
    totaltimetxt: {clase: "totaltimetxt", text: "Tiempo Total"},
    selectplatetxt: {clase: "selectplatetxt", text: "Selecciona Placa"},
    payparkingcodetxt:{clase:"payparkingcodetxt", text:"Código parqueo:"},
    paycodetxt:{clase:"paycodetxt", text:"Código de pago:"},
    paycodeinputtxt:{clase:"paycodeinputtxt", text:"Indicado por el punto de pago."},
    payparkingmsgtxt: {clase:"payparkingmsgtxt",text: "Solo para pago de servicio ocasional"},


    /* VEHICULOS*/
    addvechicletxt: {clase: "addvechicletxt", text: "Agregar Vehículo"},
    viewvehiclestxt: {clase: "viewvehiclestxt", text: "Ver Vehículos"},
    addtxt: {clase: "addtxt", text: "Agregar"},
    carplatetxt: {clase: "carplatetxt", text: "Placa:"},
    pnaddvehicles: {clase: "pnaddvehicles", text: "Agregar Vehículo"},
    vehicletxt: {clase:"vehicletxt", text: "Vehículo"},
    starttimetxt: {clase: "starttimetxt", text: "Hora Inicio"},
    placetxt: {clase: "placetxt", text: "Lugar"},
    balancetxt: {clase: "balancetxt", text: "Saldo"},


    /* USER ACCOUNT*/
    viewinfotxt: {clase: "viewinfotxt", text: "Ver Info"},
    changepasstxt: {clase: "changepasstxt", text: "Cambiar Clave"},
    pnuserinfo: {clase: "pnuserinfo", text: "Info de Usuario"},
    oldpasstxt: {clase: "oldpasstxt", text: "Clave Anterior"},
    newpasstxt: {clase: "newpasstxt", text: "Clave Nueva"},
    logouttxt: {clase: "logouttxt", text: "Cerrar Sesión"},

    /* BALANCE*/
    reloadbalancetxt: {clase: "reloadbalancetxt", text: "Recargar Saldo PIN"},
    reloadbalancezptxt: {clase: "reloadbalancezptxt", text: "Recargar Saldo"},
    viewbalancetxt: {clase: "viewbalancetxt", text: "Ver Saldo"},
    givebalancetxt: {clase: "givebalancetxt", text: "Transferir"},
    paypalreloadtxt: {clase: "paypalpaymenttxt", text: "Tarjeta de crédito"},
    pinnumbertxt: {clase: "pinnumbertxt", text: "Numero Pin"},
    payvaluetxt: {clase: "payvaluetxt", text: "Valor"},
    reloadtxt: {clase: "reloadtxt", text: "Recargar"},
    pnbalanceinfo: {clase: "pnbalanceinfo", text: "Saldo"},
    amounttxt: {clase: "amounttxt", text: "Cantidad"},
    transAmounttxt: {clase: "transAmounttxt", text: "Valor a transferir"},
    givetxt: {clase: "givetxt", text: "Transferir"},
    pngivebalance: {clase: "pngivebalance", text: "Transferir"},
    cudestinationtxt: {clase: "cudestinationtxt", text: "Número Celular o CU Usuario destino"},
    recudestinationtxt: {clase: "recudestinationtxt", text: "Repetir Celular o CU Usuario destino"},
    alertconfirmrbalnacetitle: {text: "Confirmar Recargar"},
    infopricelisttxt: {clase: "infopricelisttxt", text: "Las tarifas se encuentran sujetas a cambios sin previo aviso."},

    menuZones: {clase: "zones", text: "Zonas"},
    menuParking: {clase: "park", text: "Parqueo"},
    alertError: {text: "xx"},

    /* ADMIN */
    supervisortxt: {clase: "supervisortxt", text: "Supervisor"},
    cumobiletxt: {clase: "cumobiletxt", text: "CU/Nro Celular"},
    getbalancetxt: {clase: "getbalancetxt", text: "Consultar Saldo"},

    /* SHARE */
    shareMessage: {text: "La mejor aplicación de #Parqueo City Parking, que esperas? ¡DESCÁRGALA YA! #Android #Iphone #Wp"},
    shareTitle: {text: "CityParking - Cellular Parking Systems"},
    shareLink: {text: "https://www.cpsparking.co/service/faces/downloads.xhtml"}
};

var LANG = defaultLang == SPANISH ? LANG_ES : LANG_EN;

function initialize(){
    for(var key in LANG){
        if( LANG[key].clase != null ){
            var elem = $("."+LANG[key].clase);
            elem.text(LANG[key].text);
            var ell = elem.attr('placeholder');
            if( typeof ell != undefined ){
                elem.attr('placeholder', LANG[key].text);
            }

            ell = elem.attr('title');
            if( typeof ell != 'undefined' ){
                elem.attr('title', LANG[key].text);
            }
        }
    }
}


function initializeForTutorial(){
    for(var key in LANG){
        if( LANG[key].clase != null ){
            var elem = $("."+LANG[key].clase);
            var ell = elem.attr('title');
            if( typeof ell != 'undefined' ){
                elem.attr('title', LANG[key].text);
            }
        }
    }
}

function setLanguage(lang){
    defaultLang = lang;
    LANG = defaultLang == SPANISH ? LANG_ES : LANG_EN;
}

function cambiarIngles(){
    setLanguage(ENGLISH);
    initialize();
}

function cambiarEspaniol(){
    setLanguage(SPANISH);
    initialize();
}
;var separators = "AEIOU";
var alpha = "BCDFGHJKLMNPQRSTVWXYZ";

function reverse(data){
	var dat = data.toString();
	var rev = "";
	for( i=dat.length-1;i>=0;i-- ){
		rev += dat.charAt(i); 
	}
	return rev;
}

function mask(txt){
	pr = txt.toString();
	mask2 = "";
	for( j=0;j<pr.length;j++ ){
		next = pr.charAt(j);
		if( j % 2 == 0 ){
			var index = pr.charAt(j);
			next = alpha.charAt(index);
			var rand = (Math.random()*separators.length);
			if( parseInt(rand) % 2 == 0 ){
				next = next.toLowerCase();
			}
		}
		mask2 += next; 
	}
	return mask2;
}

function encrypt(data){	
	key = new Date().getTime(); 
	var time = reverse(key)+""+separators.charAt( (Math.random()*separators.length) );
	var encrypted = mask(time);
	for( i=0;i<data.length;i++ ){		
		rk = data.charCodeAt(i)*key;		
		encrypted += mask(rk);	 
		encrypted += separators.charAt( (Math.random()*separators.length) );
	}
	return encrypted;		
};var coordss,
    znsInfo,
    clat,
    clong,
    debug=0;

var rampgap = {

    zns: "",
    alert: function ( msg) {
        $.mobile.loading('hide');

        if(debug===0){
            navigator.notification.alert(
                msg,
                alertDismissed,
                'CPS PARKING',
                'OK'
            );
        }else{
            alert(msg);
        }
    },

    notifySoundAndVibrate: function (){},

    confirmDialog: function ( text,  action){
        var r = confirm(text);
        if (r === true){
            eval(action);
        }
    },

    openURL: function ( url,  fn){ window.location.href=url;},
    reloadUrl: function (){},
    openLocation: function ( url){window.open(url,"_system","location=no");},

    setCurrLatitude: function (clat){  sessionStorage.setItem("clat",clat);},
    getCurrLatitude: function (){ return sessionStorage.getItem("clat",clat);},

    setCurrLongitude: function (clong){sessionStorage.setItem("clong",clong);},
    getCurrLongitude: function (){return sessionStorage.getItem("clong",clong);},

    setCurrentLatLong: function ( coords){coordss=coords;},

    getCurrMarkerImage: function (){return 'img/flag.png';},

    getCurrentZoneInfo: function (){
        // return zns;
        return sessionStorage.getItem("zones");
    },

    setFlightPath: function(flightPath){
        sessionStorage.setItem("flightPath", flightPath);
    },

    getFlightPath: function(){
        return sessionStorage.getItem("flightPath");
    },

    getPlatform: function(){
        var platform = JSON.parse(window.localStorage.getItem('platform'));
        return platform;
    },
    setPlatform: function(platform){
        window.localStorage.setItem('platform', JSON.stringify(platform));
    },

    showZoneMap: function (){},

    showZoneSMap: function ( zones){
        this.zns=zones;
        sessionStorage.setItem("zones",zones);
        $("body").pagecontainer("change", "#viewZonesMapResult", { transition: "slide"});
    },

    showZoneSMapLocation: function ( zones){
        this.zns=zones;
        sessionStorage.setItem("zones",zones);
        $("body").pagecontainer("change","#mapaZonesLocalization", { transition: "slide" });
    },
    viewZoneSMenu: function (){
        $("body").pagecontainer("change","#menu", { transition: "slide" });
    },

    //function showZoneSMap( Object  zones );

    showCarMap: function (){},


    verifyGPSIsActive: function (){afterGPSGetLocation(this.getCurrLatitude(),this.getCurrLongitude());},


    setUserSession: function( idu,  pass,  balance){},

    setZoneInfo: function(  zinfo ){
        // znsInfo=zinfo;
    },

    updateBalance: function( balance){},

    synchActiveParking: function(){},

    addActiveParking: function( id,  mv,  mm){},

    removeActiveParking: function( id){},

    destroySession: function(){},

    setItem: function(key,  value){
        sessionStorage.setItem(key,value);
    },
    getItem: function(  key){
        return sessionStorage.getItem(key);
    },
    getHashLocation: function(){},
    getContentHeigth: function(){
        var docHeight = $(window).height(),
            cpsHeader = $('.ui-header').outerHeight( true ) || 0,
            cpsFooter = $('.ui-footer').outerHeight( true ) || 0,
            cpsContent = docHeight - (cpsHeader + cpsFooter);
        return cpsContent;
    },
    clearInput: function(currentPage){
        //Clear input's
        $("#"+currentPage+" .ui-input-text input").each(function(){
            $(this).val("");
            $("#"+currentPage).parent().find(".ui-input-clear").addClass("ui-input-clear-hidden");
        });
    },
    checkIsAdmin: function(isAdmin){
        if(isAdmin){
            $(":mobile-pagecontainer").find("[data-owner='admin']").addClass("show-content").removeClass("hide-content");
        } else {
            $(":mobile-pagecontainer").find("[data-owner='admin']").addClass("hide-content").removeClass("show-content");
        }
    },
    // Analytics
    initAnalytics: function(clientId){
        var analytics = navigator.analytics,
            trackingId = "UA-52539991-1";

        if(this.getPlatform().platformId == "3"){
            console.log("Analytics WP Init");
            var config = new GoogleAnalytics.EasyTrackerConfig();
            config.trackingId = trackingId;
            config.appName = "CityParking";
            config.appVersion = this.getAppVersion();
            GoogleAnalytics.EasyTracker.current.config = config;
        } else {
            analytics.setTrackingId(
                trackingId,
                function(){ console.log("Analytics General Init"); },
                function(){ console.log("Error Analytics General Init"); }
            );
        }
    },
    ga: function(action, type, content, label){
        var platform = this.getPlatform().platformId,
            analytics = platform == "3" ? GoogleAnalytics.EasyTracker.getTracker() : navigator.analytics,
            Fields    = analytics.Fields,
            HitTypes  = analytics.HitTypes,
            LogLevel  = analytics.LogLevel,
            params    = {};

        if(action == 'send'){
            if(type == 'pageview'){
                switch(platform){
                case "3":
                    analytics.sendView(content.page);
                    break;
                default:
                    analytics.sendAppView(content.page);
                }
            } else if (type == 'event'){
                analytics.sendEvent(content, label, content, 0);
                switch(content){
                case 'social':
                case 'share':
                    switch(platform){
                    case "3":
                        analytics.sendSocial(label, content, label);
                        break;
                    default:
                        params[Fields.HIT_TYPE] = HitTypes.SOCIAL;
                        params[Fields.SOCIAL_ACTION] = content;
                        params[Fields.SOCIAL_NETWORK] = label;
                        params[Fields.SOCIAL_TARGET] = label;
                        analytics.send(params);
                    }
                    break;
                }
            } else if (type == 'exception'){
                analytics.sendException(content, label);
            }
        } else if (action == 'set'){
            if(type == 'uid'){
                switch(platform){
                case "3":
                    //wp not send Useid
                    break;
                default:
                    analytics.set('uid', content);
                    analytics.set('&in', content);
                }
            }
        }
    },
    makeFrame: function(platform, id, link){
        var hframe = this.getContentHeigth(),
            ilink = platform.platformId == "3" ? platform.protocol+'/www/'+link : link;

        //var frame = '<iframe id="'+id+'" src="'+ilink+'" style="width:100%; height:'+hframe+'px" seamless></iframe>';
        var iframe = document.createElement('iframe');
        iframe.id = id;
        iframe.src = ilink;
        iframe.style.width = '100%';
        iframe.style.height = hframe+'px';
        return iframe;
    },
    toStatic: function(data){
        var result = this.getPlatform().platformId == "3" ? toStaticHTML(data) : data;
        return result;
    },
    social: function (link) {
        var surl = link.split('/');
        var schema = surl[2].indexOf("facebook") > -1 ? 'fb' : 'twitter';
        if (this.getPlatform().platformId != "3") {
            schema = schema+(schema == 'fb' ? '://page/' : '://user?screen_name=')+surl[3];
            console.log(schema);
            CanOpen(schema, function (isInstalled) {
                console.log(isInstalled);
                link = isInstalled ? schema : link;
                window.open(link, '_system', 'location=no');
                console.log(link);
            });
        } else {
            link = schema == 'fb' ? schema+':pages?id=398654126812091' : link;
            window.open(link, '_system', 'location=no');
            console.log('wp: ' + link);
        }
        // Send event to Analytics
        this.ga('send', 'event', 'social', surl[2]);
    },
    getAppVersion: function(){
        return "1.9.0";
    }
};
