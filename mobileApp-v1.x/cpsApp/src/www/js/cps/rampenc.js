var separators = "AEIOU";
var alpha = "BCDFGHJKLMNPQRSTVWXYZ";

function reverse(data){
	var dat = data.toString();
	var rev = "";
	for( i=dat.length-1;i>=0;i-- ){
		rev += dat.charAt(i); 
	}
	return rev;
}

function mask(txt){
	pr = txt.toString();
	mask2 = "";
	for( j=0;j<pr.length;j++ ){
		next = pr.charAt(j);
		if( j % 2 == 0 ){
			var index = pr.charAt(j);
			next = alpha.charAt(index);
			var rand = (Math.random()*separators.length);
			if( parseInt(rand) % 2 == 0 ){
				next = next.toLowerCase();
			}
		}
		mask2 += next; 
	}
	return mask2;
}

function encrypt(data){	
	key = new Date().getTime(); 
	var time = reverse(key)+""+separators.charAt( (Math.random()*separators.length) );
	var encrypted = mask(time);
	for( i=0;i<data.length;i++ ){		
		rk = data.charCodeAt(i)*key;		
		encrypted += mask(rk);	 
		encrypted += separators.charAt( (Math.random()*separators.length) );
	}
	return encrypted;		
}