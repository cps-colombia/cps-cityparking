var coordss,
    znsInfo,
    clat,
    clong,
    debug=0;

var rampgap = {

    zns: "",
    alert: function ( msg) {
        $.mobile.loading('hide');

        if(debug===0){
            navigator.notification.alert(
                msg,
                alertDismissed,
                'CPS PARKING',
                'OK'
            );
        }else{
            alert(msg);
        }
    },

    notifySoundAndVibrate: function (){},

    confirmDialog: function ( text,  action){
        var r = confirm(text);
        if (r === true){
            eval(action);
        }
    },

    openURL: function ( url,  fn){ window.location.href=url;},
    reloadUrl: function (){},
    openLocation: function ( url){window.open(url,"_system","location=no");},

    setCurrLatitude: function (clat){  sessionStorage.setItem("clat",clat);},
    getCurrLatitude: function (){ return sessionStorage.getItem("clat",clat);},

    setCurrLongitude: function (clong){sessionStorage.setItem("clong",clong);},
    getCurrLongitude: function (){return sessionStorage.getItem("clong",clong);},

    setCurrentLatLong: function ( coords){coordss=coords;},

    getCurrMarkerImage: function (){return 'img/flag.png';},

    getCurrentZoneInfo: function (){
        // return zns;
        return sessionStorage.getItem("zones");
    },

    setFlightPath: function(flightPath){
        sessionStorage.setItem("flightPath", flightPath);
    },

    getFlightPath: function(){
        return sessionStorage.getItem("flightPath");
    },

    getPlatform: function(){
        var platform = JSON.parse(window.localStorage.getItem('platform'));
        return platform;
    },
    setPlatform: function(platform){
        window.localStorage.setItem('platform', JSON.stringify(platform));
    },

    showZoneMap: function (){},

    showZoneSMap: function ( zones){
        this.zns=zones;
        sessionStorage.setItem("zones",zones);
        $("body").pagecontainer("change", "#viewZonesMapResult", { transition: "slide"});
    },

    showZoneSMapLocation: function ( zones){
        this.zns=zones;
        sessionStorage.setItem("zones",zones);
        $("body").pagecontainer("change","#mapaZonesLocalization", { transition: "slide" });
    },
    viewZoneSMenu: function (){
        $("body").pagecontainer("change","#menu", { transition: "slide" });
    },

    //function showZoneSMap( Object  zones );

    showCarMap: function (){},


    verifyGPSIsActive: function (){afterGPSGetLocation(this.getCurrLatitude(),this.getCurrLongitude());},


    setUserSession: function( idu,  pass,  balance){},

    setZoneInfo: function(  zinfo ){
        // znsInfo=zinfo;
    },

    updateBalance: function( balance){},

    synchActiveParking: function(){},

    addActiveParking: function( id,  mv,  mm){},

    removeActiveParking: function( id){},

    destroySession: function(){},

    setItem: function(key,  value){
        sessionStorage.setItem(key,value);
    },
    getItem: function(  key){
        return sessionStorage.getItem(key);
    },
    getHashLocation: function(){},
    getContentHeigth: function(){
        var docHeight = $(window).height(),
            cpsHeader = $('.ui-header').outerHeight( true ) || 0,
            cpsFooter = $('.ui-footer').outerHeight( true ) || 0,
            cpsContent = docHeight - (cpsHeader + cpsFooter);
        return cpsContent;
    },
    clearInput: function(currentPage){
        //Clear input's
        $("#"+currentPage+" .ui-input-text input").each(function(){
            $(this).val("");
            $("#"+currentPage).parent().find(".ui-input-clear").addClass("ui-input-clear-hidden");
        });
    },
    checkIsAdmin: function(isAdmin){
        if(isAdmin){
            $(":mobile-pagecontainer").find("[data-owner='admin']").addClass("show-content").removeClass("hide-content");
        } else {
            $(":mobile-pagecontainer").find("[data-owner='admin']").addClass("hide-content").removeClass("show-content");
        }
    },
    // Analytics
    initAnalytics: function(clientId){
        var analytics = navigator.analytics,
            trackingId = "UA-52539991-1";

        if(this.getPlatform().platformId == "3"){
            console.log("Analytics WP Init");
            var config = new GoogleAnalytics.EasyTrackerConfig();
            config.trackingId = trackingId;
            config.appName = "CityParking";
            config.appVersion = this.getAppVersion();
            GoogleAnalytics.EasyTracker.current.config = config;
        } else {
            analytics.setTrackingId(
                trackingId,
                function(){ console.log("Analytics General Init"); },
                function(){ console.log("Error Analytics General Init"); }
            );
        }
    },
    ga: function(action, type, content, label){
        var platform = this.getPlatform().platformId,
            analytics = platform == "3" ? GoogleAnalytics.EasyTracker.getTracker() : navigator.analytics,
            Fields    = analytics.Fields,
            HitTypes  = analytics.HitTypes,
            LogLevel  = analytics.LogLevel,
            params    = {};

        if(action == 'send'){
            if(type == 'pageview'){
                switch(platform){
                case "3":
                    analytics.sendView(content.page);
                    break;
                default:
                    analytics.sendAppView(content.page);
                }
            } else if (type == 'event'){
                analytics.sendEvent(content, label, content, 0);
                switch(content){
                case 'social':
                case 'share':
                    switch(platform){
                    case "3":
                        analytics.sendSocial(label, content, label);
                        break;
                    default:
                        params[Fields.HIT_TYPE] = HitTypes.SOCIAL;
                        params[Fields.SOCIAL_ACTION] = content;
                        params[Fields.SOCIAL_NETWORK] = label;
                        params[Fields.SOCIAL_TARGET] = label;
                        analytics.send(params);
                    }
                    break;
                }
            } else if (type == 'exception'){
                analytics.sendException(content, label);
            }
        } else if (action == 'set'){
            if(type == 'uid'){
                switch(platform){
                case "3":
                    //wp not send Useid
                    break;
                default:
                    analytics.set('uid', content);
                    analytics.set('&in', content);
                }
            }
        }
    },
    makeFrame: function(platform, id, link){
        var hframe = this.getContentHeigth(),
            ilink = platform.platformId == "3" ? platform.protocol+'/www/'+link : link;

        //var frame = '<iframe id="'+id+'" src="'+ilink+'" style="width:100%; height:'+hframe+'px" seamless></iframe>';
        var iframe = document.createElement('iframe');
        iframe.id = id;
        iframe.src = ilink;
        iframe.style.width = '100%';
        iframe.style.height = hframe+'px';
        return iframe;
    },
    toStatic: function(data){
        var result = this.getPlatform().platformId == "3" ? toStaticHTML(data) : data;
        return result;
    },
    social: function (link) {
        var surl = link.split('/');
        var schema = surl[2].indexOf("facebook") > -1 ? 'fb' : 'twitter';
        if (this.getPlatform().platformId != "3") {
            schema = schema+(schema == 'fb' ? '://page/' : '://user?screen_name=')+surl[3];
            console.log(schema);
            CanOpen(schema, function (isInstalled) {
                console.log(isInstalled);
                link = isInstalled ? schema : link;
                window.open(link, '_system', 'location=no');
                console.log(link);
            });
        } else {
            link = schema == 'fb' ? schema+':pages?id=398654126812091' : link;
            window.open(link, '_system', 'location=no');
            console.log('wp: ' + link);
        }
        // Send event to Analytics
        this.ga('send', 'event', 'social', surl[2]);
    },
    getAppVersion: function(){
        return "1.9.0";
    }
};
