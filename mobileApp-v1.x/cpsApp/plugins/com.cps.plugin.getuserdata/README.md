cordova getUsetInfo
=====================

Cordova plugin to retrieve Android user info from device (account, profile and sim)

##Installing the plugin
To add this plugin just type:
```cordova plugin add cordova/com.cps.plugin.getuserdata```

To remove this plugin type:
```cordova plugin remove com.cps.plugin.getuserdata```

The plugin has the following method only:

##Usage
* [window.getUserData](#getUserData)
Retrieves if plugin is available and the email, phone, displayname with success callback

***

##Versions
Tested with Cordova 4.0
