
CityParking Mobile APP
===============================
Version 1.9.3

Usage
-------
Cps Mobile has its own way of starting and compiling, first concat and minfied js, css etc.
then starts a hook for assign id and display name by platform
and copy the file to platfom.

All the commands are inherited from cordova.

The Basic commands are:

```shell
./bin/cps run [platform]
```
### Commands ###

#### Prepare ####
`cps prepare [platform]`
Concat, minfied and copy file to platform

#### Build ####
`cps build [platform]`
Compile and buid platform and generates the package

#### Run ####
`cps run [platform]`
prepare, build and push app in device or emulator


### Rest Example ###
To see all the available queries and example see CPS.rest

Error Code
-------------
### Login [op=0] ###

```
op=0&user= +login+ &pass= +pass+ &device= platformName
```

| Description                | Code |
|----------------------------|------|
| + Usuario o clave invalida | 1001 |
| + Usuario o clave invalida | null |

### Give Balance [op=14] ###

```
op=14&user= +login+ &pass= +pass+ &cu= +cu+ &amm= +amount+ &repass= +repass
```

| Description              | Code |
|--------------------------|------|
| + Error CU invalido      |   -1 |
| + Error Usuario Invalido |   -2 |
| + Saldo tranferido       | -112 |

### Reload Balance [op=13] ###

```
op=13&user= +login+ &pass= +pass+ &pin= +pin
```

| Description           | Code |
|-----------------------|------|
| + Error recarga saldo | -101 |

### Reload balance link [op=27] ###

```
op=27&user= +login+ &pass= + pass
```

| Description            | Code |
|------------------------|------|
| + Recarga exitosa      | -100 |
| + Recarga no efectuada | -101 |

### Reload Balance ZP [op=26] ###

```
op=26&user= +login+ &pass= +pass+ &value= +value
```

| Description    | Code |
|----------------|------|
| + Pin invalido | -101 |

### Send payment [op=24] ###

```
op=24&user= +login+ &pass= +pass+ &parkcode= +parkcode+ &paycode= +paycode
```

| Description               | Code |
|---------------------------|------|
| + Respuesta vacia         |   -1 |
| + Repuesta a confirmar    |    1 |
| + paymentfline            | -301 |
| + Token error             |   14 |
| + Error en la confimación |    3 |

### Confirm Parking [op=25] ###

```
op=25&user= +login+ &pass= +pass+ &id= +id+ &validationNumber=+validationNumber+
&operationResult= +operationResult+ &token= +token+ &value= +paymentValue+
&placeid= +placeId+ &initparking= +parkingInitDate+
&endparking= +parkingEndDate+ &device= +platformId
```

| Description            | Code |
|------------------------|------|
| + Pago parqueo exitoso |   12 |
| + Pago nulo            |   13 |
| + Token parqueo error  |   14 |
| + Fallo pago parqueo   |    9 |

### Start Parking [op=24] ###

```
op=24&user= +login+ &pass= +pass+ &plate= +plate+ &num= +num+ &idc= +country+
&idp= +idp+ &idu= +idu+ &sms= +sms
```

| Description              | Code |
|--------------------------|------|
| + Parque iniciado        |    0 |
| + Vehiculo ya parqueado  | -113 |
| + vehiculo no registrado | -121 |
| + Zone cerrada           | -120 |
| + Lugar equivocado       | -106 |
| + Lugar no existe        | -127 |
| + Lugar no disponible    | -128 |
| + Sin saldo              | -103 |

### Add Vehicle [op=12] ###
```
op=12&user= +login+ &pass= +pass+ &plate=SDK234
```
| Description                 | Code |
|-----------------------------|------|
| + Vehiculo existe           | -102 |
| + Supero cantidad vehiculos | -103 |
| + Agregar exitosamente      | -100 |

### Delete Vehicle [op=17] ###
```
op=17&user= +login+ &pass= +pass+ &plate=SDK234
```
| Description                 | Code |
|-----------------------------|------|
| + Se elimino correcto       | -126 |
| + Error al eliminar         | -127 |

### Reset Pass [op=23] ###

```
op=23&mail= +msg
```

| Description              | Code |
|--------------------------|------|
| + Envio de solicitud     | -117 |
| + Clave enviada a correo | -118 |

### Change Pass [op=11] ###

```
op=11&user= +login+ &pass= +pass+ &oldpass= +oldPass+ &npass= +newPass
```

| Description       | Code |
|-------------------|------|
| + Cambio existoso | -117 |
