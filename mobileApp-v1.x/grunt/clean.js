module.exports = {
    all: {
        src: ["<%= libs.dist %>/*.map"]
    },
    debug: "<%= clean.all.src %>",
    dist: "<%= clean.all.src %>"
};
