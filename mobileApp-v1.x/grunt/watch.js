module.exports = {
    less: {
        files: '<%= staticPathSrc %>/less/**.less',
        tasks: [
            'less:debug'
        ]
    }
};
