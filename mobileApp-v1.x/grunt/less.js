module.exports = {
    debug: {
        files: {
            "<%= static.dist %>/css/main.min.css": "<%= static.src %>/less/main.less"
        },
        options: {
            compress: false
        }
    },
    dist: {
        files: {
            "<%= static.dist %>/css/main.min.css": "<%= static.src %>/less/main.less"
        },
        options: {
            cleancss: true,
            compress: true
        }
    }
};
