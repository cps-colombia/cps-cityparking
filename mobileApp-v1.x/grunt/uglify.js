module.exports = {
    options: {
        banner: "/*! <%= package.name %> v<%= package.version %> - <%= package.author %> - " +
            "<%= grunt.template.today('yyyy-mm-dd') %> */"
    },
    all: {
        files: {
            "<%= libs.dist %>/cps-utils.min.js": ["<%= libs.src %>/concat/cps-utils.js"],
            "<%= libs.dist %>/cps-libs.min.js": ["<%= libs.src %>/concat/cps-libs.js"]
        }
    },
    debug: {
        options: {
            beautify: true
        },
        files: "<%= uglify.all.files %>"
    },
    dist: {
        options: {
            compress: {
                join_vars: true,
                drop_console: false
            },
            mangle: true,
            sourceMap: "<%= debug %>",
            sourceMapIncludeSources: "<%= debug %>",
            preserveComments: "some"
        },
        files: "<%= uglify.all.files %>"
    }
};
