module.exports = {
    debug1: ["newer:clean:debug"],
    debug2: ["newer:concat:debug", "newer:less:debug"],
    debug3: ["newer:uglify:debug"],
    dist1: ["clean:dist"],
    dist2: ["concat:dist", "less:dist"],
    dist3: ["uglify:dist"]
};
