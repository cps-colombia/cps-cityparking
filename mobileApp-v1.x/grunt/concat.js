module.exports = {
    options: {
        separator: ";"
    },
    all: {
        files: {
            "<%= libs.src %>/concat/cps-utils.js": ["<%= libs.src %>/cps/languageUtil.js",
                                                    "<%= libs.src %>/cps/rampenc.js",
                                                    "<%= libs.src %>/cps/mockrampgap.js"],
            "<%= libs.src %>/concat/cps-libs.js": ["<%= libs.src %>/cps/base.js"]
        }
    },
    debug: {
        files: "<%= concat.all.files %>"
    },
    dist: {
        files: "<%= concat.all.files %>"
    }
};
